#!/usr/bin/env bash
source venv/bin/activate
set -e
if [ $CIRCLE_NODE_INDEX -eq 0 ]
then
    PATREON_ENV=test_circle nosetests -v patreon ./test/app/web/pages/test_walk.py
else
    i=0
    files=()
    declare x="$(PATREON_ENV=test_circle nosetests -v -s -a speed=toxic --collect-only --with-id patreon . 2>&1)"
    y=$(python3 circle_test.py "${x}")
    PATREON_ENV=test_circle nosetests -v -x --with-timer --with-id patreon ${y}

    i=0
    files=()
    declare x="$(PATREON_ENV=test_circle nosetests -v -s -a speed=slower --collect-only --with-id patreon . 2>&1)"
    y=$(python3 circle_test.py "${x}")
    PATREON_ENV=test_circle nosetests -v -x --with-timer --with-id patreon ${y}

    i=0
    files=()
    declare x="$(PATREON_ENV=test_circle nosetests -v -a '!speed' --collect-only --with-id patreon . 2>&1)"
    y=$(python3 circle_test.py "${x}")
    PATREON_ENV=test_circle nosetests -v -x --with-timer --with-id patreon ${y}
fi