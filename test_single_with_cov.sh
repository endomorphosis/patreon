source venv/bin/activate >/dev/null
pip3 install -r requirements.txt >/dev/null
cat database/schema.sql | mysql -uroot patreon_test
PATREON_ENV=test nosetests -v --with-cov --cov-report html --cov patreon $1
