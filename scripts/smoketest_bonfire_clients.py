import sys
import patreon
from patreon.util import datetime
import requests
import signal
import time
from socketIO_client import SocketIO
from nose.tools import *


POST_ID = 2506686
COMMENT_ID = 77777

def make_comment_json(post_id, text):
    return {'data': {
        'attributes': {'body': text},
        'relationships': {
            'parent': {'data': None},
            'post': {'data': {
                'id': post_id,
                'type': 'post'
            }}
        },
       'type': 'comment'
    }}


def make_comment_vote_json(post_id, comment_id):
    return {'data': {
        'relationships': {
            'post': {'data': {
                'id': post_id,
                'type': 'post'
            }},
            'comment': {'data': {
                'id': comment_id,
                'type': 'comment'
            }}
        },
        'type': 'comment_vote'
    }}


def make_typing_json():
    return {
        'typing': True
    }


def make_presence_json(post_id):
    last_seen = datetime.datetime.utcnow()
    expiration_time = last_seen + datetime.timedelta(minutes=5)
    room = 'post:{}'.format(post_id)
    return {'data': {
        'attributes': {
            'last_seen': last_seen.isoformat(),
            'expiration_time': expiration_time.isoformat(),
            'room': room
        }
    }}


class BonfireClient(object):

    def __init__(self, port, user_id):
        self.port = port
        self.socket = SocketIO('http://localhost:{}'.format(self.port))
        self.post_id = None
        self.user_id = user_id
        self.reset()
        self.socket.on('comment', self.handle_comment)
        self.socket.on('comment vote', self.handle_comment_vote)
        self.socket.on('typing', self.handle_typing)
        self.socket.on('room info', self.handle_room_info)

    def join_room(self, post_id, is_creator=False):
        self.post_id = post_id
        self.socket.emit('join room', {
            'activityId': self.post_id,
            'userId': self.user_id,
            'isCreator': is_creator
        })

    def reset(self):
        self.received_comments = []
        self.received_comment_votes = []
        self.received_typing = []
        self.room_info = []

    def handle_comment(self, info):
        self.received_comments.append(info['data']['attributes']['body'])

    def handle_comment_vote(self, info):
        self.received_comment_votes.append(info['data']['relationships']['comment']['data']['id'])

    def handle_typing(self, info):
        if 'activityId' in info:
            del info['activityId']
        self.received_typing.append(info)

    def handle_room_info(self, info):
        self.room_info = info

    def send_comment(self, comment_text):
        print("Sending comment:", comment_text)
        comment_json = make_comment_json(self.post_id, comment_text)
        requests.post('http://localhost:{}/comment_added'.format(self.port), json=comment_json)

    def send_comment_vote(self, comment_id):
        print("Sending comment vote:", comment_id)
        comment_json = make_comment_vote_json(self.post_id, comment_id)
        requests.post('http://localhost:{}/comment_vote'.format(self.port), json=comment_json)

    def send_creator_presence(self):
        print("Marking creator as present in room:", self.post_id)
        presence_json = make_presence_json(self.post_id)
        requests.post('http://localhost:{}/creator_presence'.format(self.port), json=presence_json)

    def send_typing(self, info):
        print("Sending typing indicator:", info)
        self.socket.emit('typing', info)

    def wait(self, seconds=0.1):
        return self.socket.wait(seconds)


def test_bonfire_standalone():
    client1 = BonfireClient(3000, 101)
    client2 = BonfireClient(3000, 102)
    client3 = BonfireClient(3001, 103)
    all_clients = [client1, client2, client3]

    def clients_wait(base_seconds=2, client_seconds=0.1):
        print("Waiting for response from clients.")
        for client in all_clients:
            client.wait(client_seconds)
        time.sleep(base_seconds)
        for client in all_clients:
            client.wait(client_seconds)

    def clients_reset():
        print("Resetting clients.")
        for client in all_clients:
            client.reset()

    print("Ensuring that all clients receive the room info after joining rooms.")
    client1.join_room(POST_ID)
    client2.join_room(POST_ID)
    clients_wait()
    assert_equal(client1.room_info, { 'numUsers': 2, 'creatorPresence': {} })
    assert_equal(client2.room_info, { 'numUsers': 2, 'creatorPresence': {} })

    client3.join_room(POST_ID)
    clients_wait()
    assert_equal(client1.room_info, { 'numUsers': 3, 'creatorPresence': {} })
    assert_equal(client2.room_info, { 'numUsers': 3, 'creatorPresence': {} })
    assert_equal(client3.room_info, { 'numUsers': 3, 'creatorPresence': {} })
    clients_reset()

    print("Ensuring that all clients can receive messages.")
    client1.send_comment('hey, listen')
    clients_wait()
    assert_equal(client1.received_comments, ['hey, listen'])
    assert_equal(client2.received_comments, ['hey, listen'])
    assert_equal(client3.received_comments, ['hey, listen'])
    clients_reset()

    print("Ensuring that joining different rooms affects the room membership.")
    client1.join_room(POST_ID + 1, is_creator=True)
    clients_wait()
    assert_equal(client1.room_info, { 'numUsers': 1, 'creatorPresence': {} })
    assert_equal(client2.room_info, { 'numUsers': 2, 'creatorPresence': {} })
    assert_equal(client3.room_info, { 'numUsers': 2, 'creatorPresence': {} })
    clients_reset()

    print("Ensuring that creator presence works.")
    client2.send_creator_presence()
    clients_wait()
    assert_equal(client1.room_info, { 'numUsers': 1, 'creatorPresence': {} })
    assert_equal(client2.room_info['numUsers'], 2)
    assert_in('lastSeen', client2.room_info['creatorPresence'])
    assert_in('expirationTime', client2.room_info['creatorPresence'])
    assert_equal(client3.room_info, client2.room_info)
    clients_reset()

    print("Ensuring that messages are passed between users in the same room.")
    client1.send_comment('comment in my own room')
    client2.send_comment('comment in original room')
    clients_wait()
    assert_equal(client1.received_comments, ['comment in my own room'])
    assert_equal(client2.received_comments, ['comment in original room'])
    assert_equal(client3.received_comments, ['comment in original room'])
    clients_reset()

    print("Ensuring that comment votes are passed between users.")
    client2.send_comment_vote(POST_ID)
    clients_wait()
    assert_equal(client1.received_comment_votes, [])
    assert_equal(client2.received_comment_votes, [POST_ID])
    assert_equal(client3.received_comment_votes, [POST_ID])
    clients_reset()

    print("Ensuring that typing info is passed between users.")
    client2.send_typing({'dummy': 'info'})
    clients_wait()
    assert_equal(client1.received_typing, [])
    assert_equal(client2.received_typing, [{'dummy': 'info'}])
    assert_equal(client3.received_typing, [{'dummy': 'info'}])
    clients_reset()


if __name__ == '__main__':
    # Automatically quit if there's a timeout.
    signal.signal(signal.SIGALRM, lambda signum, frame: sys.exit(1))
    signal.alarm(300)
    test_bonfire_standalone()
