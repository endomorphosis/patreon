import json
import time
import rollbar
import os

import patreon
from patreon.model.manager import activity_mgr, campaign_mgr, pledge_mgr, zoho_campaigns_mgr
from patreon.services.db import db_session
from patreon.services.logging.unsorted import log_json
from patreon.services.zoho import build_xml_doc, post_create, post_lead_convert, post_update, create_map


def get_campaign_updates_from_queue(number_elements=10):
    return_list = set()
    iterations = 0
    while len(return_list) < 15 and (iterations < 120 or len(return_list) < 15):
        iterations += 1
        messages = patreon.services.aws.sqs.get('prodweb-zoho_update', number_elements)
        if not messages:
            print('Waiting for at least 15 to go, have ' + str(len(return_list)))
            time.sleep(1)
            continue
        return_list = return_list.union(set([message.get('campaign_id') for message in messages]))
        if iterations > 120:
            print('Waiting for at least 15 to go, have ' + str(len(return_list)))
        if iterations > 9:
            time.sleep(1)

    return return_list


def estimate_per_month(campaign, pledge_total):
    if campaign.is_monthly == 1:
        return pledge_total / 100
    paid_posts_last_year = activity_mgr.get_count_paid_posts_in_last_year_by_campaign_id(campaign.campaign_id) or 0
    avg_monthly_cents = (paid_posts_last_year / 12 * float(pledge_total)) or 0
    return avg_monthly_cents / 100


def build_record_data(campaign):
    record_data = {
        'Last Name': campaign.users[0].last_name or 'No Last Name',
        'Account Name': campaign.users[0].full_name,
        'Email': campaign.users[0].email,
        'Patrons': pledge_mgr.get_patron_count_by_creator_id(campaign.users[0].UID),
        'Campaign': campaign,
        'Vanity': campaign.users[0].vanity,
        'User ID': campaign.users[0].user_id
    }
    if record_data['Patrons'] >= 2:
        record_data['Per Month'] = estimate_per_month(
            campaign,
            pledge_mgr.get_pledge_total_by_creator_id(campaign.users[0].UID)
        )
    return record_data


def zoho_iteration():
    patreon.services.clear_request_cache()
    db_session.commit()
    db_session.expire_all()
    campaign_ids = get_campaign_updates_from_queue()

    lead_updates = []
    lead_creates = []

    creator_updates = []
    creator_creates = []

    convert_ids = []

    if len(campaign_ids) == 0:
        print('Update queue empty! Sleeping!')
        time.sleep(10)
        return

    for campaign_id in campaign_ids:
        campaign = campaign_mgr.get(campaign_id=campaign_id)
        if campaign is None:
            continue

        record_data = build_record_data(campaign)

        if record_data['Patrons'] >= 2:
            if campaign.zoho_map is not None and campaign.zoho_map.is_converted == 0:
                convert_ids.append(campaign.zoho_id)
            elif campaign.zoho_map is None:
                creator_creates.append(record_data)
            else:
                record_data['Id'] = campaign.zoho_id
                creator_updates.append(record_data)
        else:
            if campaign.zoho_map is None:
                lead_creates.append(record_data)
            else:
                record_data['Id'] = campaign.zoho_id
                lead_updates.append(record_data)

    creates = [('Leads', lead_creates, 0), ('Accounts', creator_creates, 1)]
    updates = [('Leads', lead_updates), ('Accounts', creator_updates)]

    for create in creates:
        xml_doc = build_xml_doc(create[0], create[1])
        result = post_create(create[0], xml_doc)
        for _ in create[1]:
            log_json({
                'verb': 'zoho',
                'action': 'update',
                'campaign_id': _['Campaign'].campaign_id,
                'input': xml_doc.decode(),
                'output': result
            })
        create_map(result, create[1], create[2])

    for update in updates:
        xml_doc = build_xml_doc(update[0], update[1])
        result = post_update(update[0], xml_doc)
        for _ in update[1]:
            log_json({
                'verb': 'zoho',
                'action': 'create',
                'campaign_id': _['Campaign'].campaign_id,
                'input': xml_doc.decode(),
                'output': result
            })

    for id in convert_ids:
        campaign_id = zoho_campaigns_mgr.get(None, id).campaign_id
        post_lead_convert(id)
        data = {
            'campaign_id': campaign_id
        }

        patreon.services.aws.sqs.put('prodweb-zoho_update', json.dumps(data))

    print('Updating campaigns: ' + str(campaign_ids).strip('[]'))


try:
    while True:
        zoho_iteration()
except Exception as e:
    rollbar.init(
        patreon.config.rollbar_api_key,
        'production',
        root=os.path.dirname(os.path.realpath(__file__)),
        allow_logging_basic_config=False)
    rollbar.report_exc_info()
    print(e)
