from datetime import datetime
from patreon.model.table import RewardsGive, Payout
from patreon.services.db import db_session
from patreon.util.datetime import timezone_aware_sql_date_func
import pytz
from sqlalchemy import func

created_timezone = timezone_aware_sql_date_func(RewardsGive.Created, pytz.timezone('US/Pacific'))
created_timezone2 = timezone_aware_sql_date_func(Payout.Created, pytz.timezone('US/Pacific'))
now = datetime.now()

month_start_1 = datetime(year=now.year, month=(now.month-1 or 12), day=1, hour=0, minute=0, second=0)
month_start_2 = datetime(year=now.year, month=(now.month-1 or 12), day=31, hour=23, minute=59, second=59)

result1 = RewardsGive.query.with_entities(RewardsGive.CID.label('creator_id'),
                                          func.sum(RewardsGive.Pledge).label('pledge'),
                                          func.sum(RewardsGive.Fee).label('fee1'),
                                          func.sum(RewardsGive.PatreonFee).label('fee2')).filter_by(Status=1).\
                                          filter(created_timezone <= month_start_1).group_by(RewardsGive.CID)

result2 = Payout.query.with_entities(Payout.UID.label('creator_id'), func.sum(Payout.Amount).label('payout'))\
    .filter(Payout.Type != 10)\
    .filter(Payout.Type != 6) \
    .filter(created_timezone2 < month_start_2) \
    .group_by(Payout.UID)

creator_in = {element.creator_id: (element.pledge - element.fee1 - element.fee2) for element in result1}
print('got in')
creator_out = {element.creator_id: element.payout for element in result2}
print('got out')

running_total = 0
creator_ids = creator_in.keys()
for creator_id in creator_ids:
    if creator_out.get(creator_id):
        diff = creator_out[creator_id] - creator_in[creator_id]
        if diff >= 100:
            print("{} {}".format(creator_id, diff / 100))
            Payout.insert({
                'Amount': -1*diff,
                'UID': creator_id,
                'Type': 4,
                'TransID': 'Negative Balance HACK'
            })
            db_session.commit()
        if diff > 10:
            running_total += diff

print(running_total)

