from patreon.services.payment_apis import StripeAPI
from patreon.util.unsorted import jsencode


last = None

def get_more_stripe_transactions():
    global last
    results = StripeAPI().get_recent_transactions(last).get('data')
    if not results:
        return None
    last = results[-1]
    return results

all_charges = []

while True:
    charges = get_more_stripe_transactions()
    for charge in charges:
        if charge['created'] < 1435708800:
            f = open('out.json', 'w')
            f.write(jsencode(all_charges))
            exit()

    all_charges += charges
