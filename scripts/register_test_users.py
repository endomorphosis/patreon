import string
from patreon.model.manager import user_mgr
import random

def generate_random_string(length=12):
    return ''.join(random.choice(string.ascii_uppercase) for i in range(length))

count = 0
for i in range(0, count):
    email = generate_random_string() + '@' + generate_random_string() + '.com'
    password = generate_random_string()

    user_mgr.register_user(email, password, None, 'Cat', 'Catterson')
    print(email + ' registered with password ' + password)
