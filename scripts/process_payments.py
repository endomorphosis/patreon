import time
from patreon.model.payment_queue import BillObject


def process_single_payment():
    with BillObject() as bill_id:
        if bill_id is None:
            time.sleep(10)
            return

        payment_mgr.pay_bill(bill_id)


while True:
    process_single_payment()
