from itertools import count

from patreon.model.manager import activity_mgr, campaign_mgr

from patreon.services.algolia import Algolia


upload_count_at_once = 2000
upload_post_count = 400000

# TODO: move these constants into configuration files
client = Algolia("27QC0IB0ER", 'eeb13fc8f23b4d3c3a7571d7f3f77475')

for n in count():
    campaigns = campaign_mgr.find_all_paginated(upload_count_at_once, upload_count_at_once * n)
    ids = [campaign.campaign_id for campaign in campaigns]
    if not ids:
        break
    print("updating " + str(len(ids)) + " campaigns, starting with campaign_id=" + str(ids[0]))
    client.update_campaigns_by_ids(ids)

for n in count():
    if (n + 1) * upload_count_at_once > upload_post_count:
        break
    posts = activity_mgr.find_searchable_with_limit(upload_count_at_once, upload_count_at_once * n)
    ids = [post.activity_id for post in posts]
    if not ids:
        break
    print("updating " + str(len(ids)) + " posts, starting with activity_id=" + str(ids[0]))
    client.update_posts_by_ids(ids)
