import os
import boto

def main():
	# This needs to be defined in the environment same as boto tools
	aws_access_key_id = os.environ.get('AWS_ACCESS_KEY_ID')
	aws_secret_access_key = os.environ.get('AWS_SECRET_ACCESS_KEY')

	if not aws_access_key_id or not aws_secret_access_key:
		print("In order to use this script, set your environment variables to")
		print("match the values defined in 1Password under AWS Access Keys")
		print("")
		print("EXPORT AWS_ACCESS_KEY_ID=<Access Key>")
		print("EXPORT AWS_SECRET_ACCESS_KEY=<Secret Key>")
		return

	# Get the Region object for N. California
	regions = boto.regioninfo.get_regions('ec2')
	us_west_1 = [r for r in regions if r.name == 'us-west-1'][0]

	# Connect to EC2 and get list of instances
	connection = boto.connect_ec2(aws_access_key_id, aws_secret_access_key, region=us_west_1)
	reservations = connection.get_all_instances()
	instances = [r.instances[0] for r in reservations]

	# Extract the IP data from all the instances
	ip_data = [(instance.tags.get('Name'), instance.ip_address) for instance in instances]
	ip_data = [(name.split()[-1], ip) for (name, ip) in ip_data if (name and ip)]

	# Print out all the data in /etc/hosts friendly format
	just = len('255.255.255.255')
	for name, ip in sorted(ip_data):
		print(ip.ljust(just), name)

if __name__ == '__main__':
	main()
