#!/usr/bin/env python

from flask import Flask, request
import time
import urllib
import json
import os
import sys
import requests

app = Flask(__name__)


# ========================
# Deploy code
# ========================

def print_slack(message, bot_name='Deploy Bot'):
    channel = '#engineering'
    icon_emoji = ':hammer:'
    url = 'https://hooks.slack.com/services/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'
    data = {
        'text': message,
        'username': bot_name,
        'channel': channel,
        'icon_emoji': icon_emoji
    }
    urllib.urlopen(url, json.dumps(data))


def run(command):
    # TODO: Make more sophisticated output capture.
    log("Running command: {0}".format(command))
    retcode = os.system(command)
    if retcode != 0:
        print_slack("ERROR DURING DEPLOY: Failure to run command: {0}".format(command))
    return retcode


def deploy_php_code(revision):
    retcodes = []
    for host in ('165.225.150.164', '165.225.151.124', '165.225.145.157', '165.225.149.8'):
        r = run("ssh {host} 'cd /home/patreon/web/public; /opt/local/bin/git fetch && /opt/local/bin/git checkout -f {revision}'".format(
            host=host,
            revision=revision
            ))
        retcodes.append(r)
    for host in ('web1', 'web2', 'web3'):
        r = run("curl -s http://{subdomain}.patreon.com/scripts/combineScripts 2>&1 > /dev/null".format(subdomain=host))
        retcodes.append(r)
        
    for host in ('165.225.150.164', '165.225.151.124', '165.225.145.157', '165.225.149.8'):
        r = run("ssh {host} '/opt/local/bin/php /home/patreon/web/deploy/deployScripts/cacheBust.php'".format(host=host))
        retcodes.append(r)

    if all(r == 0 for r in retcodes):
        print_slack("Deploy to revision {0} success!".format(revision))
    else:
        print_slack("Deploy to revision {0} failure, see logs for details.".format(revision))

    # Launch PHP deploy job via Ansible Tower
    requests.post("http://54.67.119.245/api/v1/job_templates/144/launch/", data={}, auth=('deploy_php', 'Ash1hohjKex2hanoAhghe0ei'), verify=False, headers={'content-type': 'application/json'})        


def deploy_py_code(revision):
    # TODO(port): This is a quick and dirty single-server version of continuous deployment.
    # This needs to be integrated with CircleCI and only pushed to on tests passing.
    pydir = '/home/ansible/patreon_py'
    run('GIT_DIR={pydir}/.git git fetch'.format(pydir=pydir))
    run('GIT_DIR={pydir}/.git git checkout -f {revision}'.format(pydir=pydir, revision=revision))
    run('forever restartall')
    #run('PYTHONPATH={pydir} forever start -c {pydir}/venv/bin/python {pydir}/runserver.py'.format(pydir=pydir))
    #run('PYTHONPATH={pydir} forever start -c {pydir}/venv/bin/python {pydir}/scripts/darkmode_replay.py'.format(
    #    pydir=pydir))


# ========================
# Utility code
# ========================


def log(message):
    print(time.asctime(), message)


def parse_message_from_request():
    message = request.get_json()
    commit = message.get('after')
    ref = message.get('ref')
    return (commit, ref)


@app.route('/handle_patreon_php', methods=['POST'])
def handle_patreon_php():
    commit, ref = parse_message_from_request()
    if commit and ref:
        log("Received update to PHP codebase {0} to commit {1}.".format(ref, commit))
        if ref == 'refs/heads/release':
            print_slack("Starting deploy process for PHP codebase revision {0}".format(commit))
            deploy_php_code(commit)
        else:
            log("Recevied")
    else:
        log("Did not receive a valid commit message from Github hook.")
    return "OK"


@app.route('/handle_patreon_py', methods=['POST'])
def handle_patreon_py():
    commit, ref = parse_message_from_request()
    if commit and ref:
        log("Received update to Python codebase {0} to commit {1}.".format(ref, commit))
        if ref == 'refs/heads/master':
            log("Running Python deploy process.")
            deploy_py_code(commit)
    else:
        log("Did not receive a valid commit message from Github hook.")
    return "OK"


def main():
    if len(sys.argv) > 1:
        port = int(sys.argv[1])
    else:
        port = 8080
    app.run(host='0.0.0.0', port=port)


if __name__ == '__main__':
    main()
