import patreon
from patreon.model.manager import address_mgr
from patreon.model.table import Pledge

LIMIT = 10000


def get_all_pledges_with_addresses():
    return Pledge.query \
        .filter(Pledge.Street.isnot(None)) \
        .filter(Pledge.address_id.is_(None)) \
        .limit(LIMIT)\
        .all()

pledges = get_all_pledges_with_addresses()
for pledge in pledges:
    address = address_mgr.create_address(
        user_id=pledge.patron_id,
        addressee=pledge.user.full_name,
        address=pledge.address,
        name=pledge.Street
    )
    pledge.update({
        'address_id': address.address_id
    })

print("Inserted {} addresses.".format(len(pledges)))
