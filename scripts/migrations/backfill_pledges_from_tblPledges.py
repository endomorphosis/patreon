import patreon
from patreon.model.table import Pledge
from patreon.model.manager import new_pledge_mgr, campaign_mgr
from patreon.services.db import db_session

last_patron_id = 0
batch = 500
keep_going = True
while keep_going:
    print(last_patron_id)
    with db_session.begin():
        pledges = Pledge.query.filter(Pledge.UID >= last_patron_id).limit(batch).all()
        if not pledges:
            keep_going = False
        else:
            last_patron_id = pledges[-1].patron_id
        insert_count = 0
        for pledge in pledges:
            campaign_id = campaign_mgr.try_get_campaign_id_by_user_id_hack(pledge.creator_id)
            new_pledge = None
            if campaign_id:
                new_pledge = new_pledge_mgr.get_pledge_by_patron_and_campaign(pledge.patron_id, campaign_id)
            else:
                with open('missing_campaigns.txt','a') as f: f.write(str(pledge.creator_id) + "\n")
            if new_pledge:
                if new_pledge.amount_cents != pledge.amount \
                or new_pledge.pledge_cap_amount_cents != pledge.pledge_cap \
                or new_pledge.reward_tier_id != pledge.reward_id:
                    new_pledge = None
            if campaign_id and not new_pledge:
                insert_count += 1
                new_pledge_mgr.make_pledge(
                    user_id = pledge.patron_id,
                    campaign_id = campaign_id,
                    amount_cents = pledge.amount,
                    pledge_cap_amount_cents = pledge.pledge_cap,
                    reward_tier_id = pledge.reward_id,
                    created_at = pledge.created_at
                )
    print(str(insert_count) + " inserted")



