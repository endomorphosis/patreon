-- Author: karen
-- Production run date: 8/12/15

-- Fix comment_count column in `activites` to accurately reflect the number
-- of non-spam comments in `tblComments`.

UPDATE activities
LEFT JOIN (SELECT HID AS activity_id,
        COUNT(1) AS actual_count
        FROM tblComments
        WHERE is_spam = 0
        GROUP BY HID) AS temp
ON (activities.activity_id = temp.activity_id)
SET activities.comment_count = temp.actual_count
WHERE activities.comment_count != temp.actual_count;
