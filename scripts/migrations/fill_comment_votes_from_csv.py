import csv
import sys

from parse import parse
from patreon.model.table import CommentVote, Comment
from patreon.services.db import db_session
from patreon.util.unsorted import jsdecode
from sqlalchemy import func

#   This requires data.csv to be in the same directory because I'm lazy <3

print('Starting inserts')
csv.field_size_limit(sys.maxsize)
with open('data.csv', 'r') as file:
    rows = csv.DictReader(file)
    iterations = 0
    for row in rows:
        if not row['id'].startswith('user_'):
            continue
        user_id, activity_id = parse('user_{}_creation_{}', row['id'])
        comment_data = jsdecode(row['value'])
        comment_data = comment_data['comment_voted']
        for comment_id, vote in comment_data.items():
            values_dict = {
                'user_id': user_id,
                'activity_id': activity_id,
                'comment_id': comment_id,
                'vote': vote
            }
            existing = CommentVote.get(comment_id=comment_id, user_id=user_id)
            if not existing:
                CommentVote.insert(values_dict)
            iterations += 1
        if iterations % 10000 == 0:
            print(str(iterations) + ' inserted')

print('Done Inserting')
vote_sums = db_session.query(CommentVote.comment_id, func.sum(CommentVote.vote).label('vote_total')) \
    .group_by(CommentVote.comment_id).all()
for element in vote_sums:
    comment = Comment.get(element.comment_id)
    iterations = 0
    if comment:
        comment.update({
            'vote_sum': element.vote_total
        })
        iterations += 1
    if iterations % 10000 == 0:
            print(str(iterations) + ' inserted')