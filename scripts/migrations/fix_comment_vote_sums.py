from patreon.model.table import CommentVote, Comment
from patreon.services.db import db_session
from sqlalchemy import func

"""
You can just do
UPDATE tblComments
SET vote_sum = 0

then

UPDATE tblComments
INNER JOIN (SELECT comment_id,
                          IFNULL(SUM(vote), 0) AS sum
                   FROM   comment_votes
                   GROUP  BY comment_id) AS temp
               ON tblComments.cid = temp.comment_id
SET    tblComments.vote_sum = temp.sum

"""

comment_ids = db_session.query(Comment.CID).all()
comments = len(comment_ids)
iterations = 0
print('Starting for loop')
for comment_id in comment_ids:
    comment_id = comment_id.CID
    comment = Comment.get(comment_id)
    votes = db_session.query(func.sum(CommentVote.vote).label('sum')).filter(CommentVote.comment_id == comment_id).first()
    vote_sum = 0
    if votes:
        vote_sum = votes.sum or 0

    comment.update({
        'vote_sum': vote_sum
    })
    db_session.close()
    iterations += 1
    if iterations % 100 == 0:
        print(str(iterations) + ' complate, ' + str(comments) + ' total')
