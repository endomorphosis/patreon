import urllib.error
import urllib.request
import sys

from patreon.constants import image_sizes
from patreon.model.manager import make_post_mgr
from patreon.model.table import Activity

if len(sys.argv) > 1:
    last_id = sys.argv[1]
else:
    last_id = 0

chunk_size = 500
chunk_num = 1
offset = 0

successes = 0
failure_ids = {}

temp_filename = '_temporary.jpeg'

try:
    while True:
        print('working on chunk {chunk_num}, starting with id {last_id}'.format(
            chunk_num=chunk_num,
            last_id=last_id
        ))

        chunk = Activity.query \
            .filter(Activity.activity_id > last_id) \
            .filter(Activity.thumbnail_key != None) \
            .order_by(Activity.activity_id.asc()) \
            .limit(chunk_size) \
            .all()

        if not len(chunk):
            break

        last_id = chunk[-1].activity_id

        for post in chunk:
            thumbnail_url = post.thumbnail_url_size(image_sizes.POST_THUMB_SQUARE_LARGE)
            response = urllib.request.urlopen(thumbnail_url)

            with open(temp_filename, 'wb') as f:
                f.write(response.read())

            make_post_mgr._store_blurred_thumbnail(
                temp_filename,
                post.thumbnail_key
            )
            successes += 1
        chunk_num += 1
except (Exception, KeyboardInterrupt):
    print('failed. last chunk start id = {0}'.format(last_id))
    print('Wrote {0} thumbnails'.format(successes))
    raise

print('Done! Wrote {0} thumbanils'.format(successes))
