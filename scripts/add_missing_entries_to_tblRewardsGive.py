__author__ = 'iathougies'

import patreon
from datetime import datetime
from patreon.model.table import RewardsGive, Payout
from patreon.services.db import db_session
from patreon.util.datetime import timezone_aware_sql_date_func
import pytz
from sqlalchemy import func

import csv


created_timezone = timezone_aware_sql_date_func(RewardsGive.Created, pytz.timezone('US/Pacific'))
created_timezone2 = timezone_aware_sql_date_func(Payout.Created, pytz.timezone('US/Pacific'))
now = datetime.now()

month_start_1 = datetime(year=now.year, month=(now.month-1 or 12), day=1, hour=0, minute=0, second=0)
month_start_2 = datetime(year=now.year, month=(now.month-1 or 12), day=31, hour=23, minute=59, second=59)

filename = "scripts/sample_entries.tsv"
results_filename = "scripts/results.tsv"

def read_in_entries_and_organize_by_creator(filename):
    # Open file and get a reader
    file_handle = open(filename, "r")
    file_reader = csv.reader(file_handle, delimiter="\t")

    # Dictionary of creators to their missing entries
    creator_to_missing_entries = {}

    line = find_next_entry(file_reader)

    while line != -1:
        cid = int(line[6])

        if cid in creator_to_missing_entries.keys():
            # We've seen this cid before. Add  the missing lines to list.
            creator_to_missing_entries[cid].append(line)
        else:
            # First time seeing this creator. Create new entry.
            creator_to_missing_entries[cid] = [line]
        line = find_next_entry(file_reader)


    return creator_to_missing_entries

def calculate_amount_pledged_in_missing_entry_list(missing_entries):
    pledge = 0
    fees = 0
    patreon_fees = 0

    for entry in missing_entries:
        # If the status is 1
        if entry[7] == "1":
            # Keep a running sum of the pledge, fees, and patreon fees
            pledge += int(entry[9])
            fees += int(entry[17])
            patreon_fees += int(entry[18])

    return pledge, fees, patreon_fees

def calculate_correction_to_manual_fix(cid, retrieved_pledges, retrieved_fees, retrieved_patreon_fees):

    # Find the amount paid out
    payout_results = db_session.query(Payout.PayID, Payout.Amount)\
                                    .filter(Payout.Type == 4)\
                                    .filter(Payout.TransID == "Negative Balance HACK")\
                                    .filter(Payout.UID == cid) \
                                    .all()

    if payout_results == []:
        # There was no manual correction for this creator, no correction is needed.
        correction = 0
        payout_id = None
    else:
        payout_id = payout_results[0].PayID

        # Get the payout - remember that it is a negative number since it is credit
        payout = payout_results[0].Amount

        difference = retrieved_pledges + payout
        correction = 0
        if difference >= 0:
            # We found more pledges than we originally paid the creator.
            # Reverse our payout by adding an entry.
            # Add an entry with the -payout.

            correction = -1*payout
        else:
            # The retrieved pledges sum to less than we paid out earlier.
            # Reverse only the portion of the payout that is matched by the retrieved pledges.
            # Add an entry with the -retrieved_pledges.
            correction = retrieved_pledges

    return payout_id, correction

# Test create
def reinsert_retrieved_rows_for_creator(cid, entries, results_writer):
    # Calculate the total amount pledged in the missing entries
    pledge, fees, patreon_fees = calculate_amount_pledged_in_missing_entry_list(entries)

    # Calculate the amount given for the manual fix and what the correction is
    payout_id, correction = calculate_correction_to_manual_fix(cid, pledge, fees, patreon_fees)

    # Put missing rows back into tblRewardsGive
    for entry in entries:
        entry_dict = createDictionaryFromEntry(entry)
        print(str(entry_dict))
        id = RewardsGive.insert(entry_dict)
        db_session.commit()
        print("Inserted into tblRewardsGive.")
    # Insert payout row for correction, if neccessary
    if correction > 0:
        Payout.insert({
                    'Amount': correction,
                    'UID': cid,
                    'Type': 4,
                    'TransID': 'Positive Balance HACK'
                })
        db_session.commit()
        print("Inserted into tblPayouts.")


def createDictionaryFromEntry(entry):
    dict = {"ID": makeIntOrNull(entry[0]),
            "UID": makeIntOrNull(entry[1]),
            "HID": makeIntOrNull(entry[2]),
            "RID": makeIntOrNull(entry[3]),
            "RDescription": entry[5],
            "CID": makeIntOrNull(entry[6]),
            "Status": makeIntOrNull(entry[7]),
            "Created": entry[8],
            "Pledge": makeIntOrNull(entry[9]),
            "CardID": entry[10],
            "Street": entry[11],
            "Street2": entry[12],
            "City": entry[13],
            "State": entry[14],
            "Zip": entry[15],
            "Country": entry[16],
            "Fee": makeIntOrNull(entry[17]),
            "PatreonFee": makeIntOrNull(entry[18]),
            "PayID": makeIntOrNull(entry[19]),
            "Complete": makeIntOrNull(entry[20])
            }

    return dict

def makeIntOrNull(number):
    if number == "NULL":
        return "NULL"
    else:
        return int(number)

def testing_functions():
    results_handle = open(results_filename, "w")
    results_writer = csv.writer(results_handle, delimiter="\t")

    creator_to_entries = read_in_entries_and_organize_by_creator(filename)

    for creator in creator_to_entries.keys():
        print("Working on creator: " + str(creator))
        reinsert_retrieved_rows_for_creator(creator, creator_to_entries[creator], results_writer)

def find_next_entry(cvs_reader, print_stuff=False):
    try:
        first_line = cvs_reader.__next__()
    except StopIteration:
        return -1

    if first_line == "":
        return ""
    print_stuff = True
    if print_stuff:
        print(first_line)
        print(len(first_line))

    while len(first_line) != 22:
        # If the first part is length 6, the issue is with the RDescription a \n in RDescription's html
        if len(first_line) == 6 :
            while len(first_line) < 22:

                # Get the next line
                line = cvs_reader.__next__()
                if print_stuff:
                    print("Looping through to more lines!")
                    print("Next line: " + str(line))
                if len(line) > 0:
                    first_line[5] = first_line[5] + "\n" + line[0]

                    if len(line) > 1:
                        first_line = first_line + line[1:]
                if print_stuff:
                    print(str(first_line))

        # If index 6, which is supposed to be the CID, isn't a digit, RDescription has tabs
        while (len(first_line) > 6) and (not first_line[6].isdigit()):
            if (print_stuff):
                print("Looping through consolidating RDescription")
                print(len(first_line))
                print(first_line)

            if len(first_line) > 7:
                new_RD = first_line[5] + "\t" + first_line[6]
                first_line = first_line[0:5] + [new_RD] + first_line[7:]
            elif len(first_line) == 7:
                first_line = first_line[0:5] + [first_line[5] + "\t" + first_line[6]]

        # If Status, Pledge, and the Created date are where we expect them,
        # But the Zip is not at Index 15 where we expect it, the address has probably
        # got weird characters that messed up the breaks.
        # (Happens with non-English characters)
        while (len(first_line) > 22) and \
                (first_line[9].isdigit() and \
                first_line[7].isdigit() and \
                first_line[8].count(":") == 2) and \
                not first_line[15].isdigit():
            print(first_line)
            first_line = first_line[0:12] + [first_line[12]+first_line[13]] + first_line[14:]

    return first_line

testing_functions()
