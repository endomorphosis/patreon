import requests
import json
import os
import sys
import datetime

API_KEY = os.environ.get('API_KEY') or '5NkzENaO.xuAs1krLv78wjjkxuBqDPKK'
API_BASE = 'https://app.asana.com/api/1.0'
BUGS_PROJECT = '15342483836751'

TIME_SLOTS = [
    '2012-01-01T00:00:00.000Z'
]
bugs_week = datetime.datetime(year=2015, month=5, day=19)
while bugs_week < datetime.datetime.now():
    TIME_SLOTS.append(bugs_week.isoformat())
    bugs_week += datetime.timedelta(days=7)


def parse_timestamp(timestamp):
    if timestamp is None:
        return None
    else:
        return datetime.datetime.strptime(timestamp.split('.')[0], '%Y-%m-%dT%H:%M:%S')

def api_get(path):
    req = requests.get(API_BASE + path, auth=(API_KEY, None))
    return json.loads(req.text)['data']

def get_bug_tasks():
    return api_get('/projects/{0}/tasks'.format(BUGS_PROJECT))

def get_task_stories(task_id):
    return api_get('/tasks/{0}/stories'.format(task_id))

def story_is_move_to_p0(story):
    return (
        story['type'] == 'system' and
        story['text'].startswith('moved from') and
        story['text'].endswith('P0 (Fix now) (Bugs)')
        )

def story_is_move_to_p1(story):
    return (
        story['type'] == 'system' and
        story['text'].startswith('moved from') and
        story['text'].endswith('P1 (Fix within 1 week) (Bugs)')
        )

def story_is_move_to_p2(story):
    return (
        story['type'] == 'system' and
        story['text'].startswith('moved from') and
        story['text'].endswith('P2 (Fix when we can) (Bugs)')
        )

def story_is_assignment(story):
    return (
        story['type'] == 'system' and
        story['text'].startswith('assigned to')
        )

def story_is_completion(story):
    return (
        story['type'] == 'system' and
        story['text'].startswith('completed this task')
        )


def get_metrics(task_id):
    task_stories = get_task_stories(task_id)

    bug_type = 'P2'
    creation_time = task_stories[0]['created_at']
    triage_time = None
    last_assignment_time = None
    completed_time = None

    for story in task_stories:
        # Get whether this was the triage point.
        if story_is_move_to_p0(story):
            bug_type = 'P0'
            triage_time = story['created_at']
        if story_is_move_to_p1(story):
            bug_type = 'P1'
            triage_time = story['created_at']
        if story_is_move_to_p2(story):
            bug_type = 'P2'
            triage_time = story['created_at']

        # Get whether this was a reassignment, save if this is the last
        # reassignment until the completion.
        if story_is_assignment(story):
            last_assignment_time = story['created_at']

        # If this is a completion, then take the last reassignment
        # and save it as a completed_time
        if story_is_completion(story):
            completed_time = last_assignment_time

    if not triage_time:
        # Not a bug then?
        return None

    creation_datetime = parse_timestamp(creation_time)
    triage_datetime = parse_timestamp(triage_time)
    completed_datetime = parse_timestamp(completed_time)

    time_slot = max([slot for slot in TIME_SLOTS if slot < creation_time])

    if triage_datetime:
        time_to_triage = triage_datetime - creation_datetime
    else:
        time_to_triage = None

    is_completed = completed_time is not None
    if is_completed:
        fix_time = completed_datetime - triage_datetime
        fixed_within_7_days = fix_time < datetime.timedelta(7)
    else:
        fix_time = None
        fixed_within_7_days = False

    downtime = None

    return {
        'time_slot': time_slot,
        'bug_type': bug_type,
        'is_completed': is_completed,
        'fix_time': fix_time,
        'triage_time': time_to_triage,
        'fixed_within_7_days': fixed_within_7_days,
    }

def get_all_bug_metrics():
    all_tasks = get_bug_tasks()
    all_metrics = []
    for task in all_tasks:
        print(task['name'], file=sys.stderr)
        all_metrics.append(get_metrics(task['id']))
    return all_metrics

def summarize_metrics(metrics):
    num_p0s = len([m for m in metrics if m['bug_type'] == 'P0'])
    num_p1s = len([m for m in metrics if m['bug_type'] == 'P1'])
    num_p2s = len([m for m in metrics if m['bug_type'] == 'P2'])
    seconds_to_triage = [m['triage_time'].seconds for m in metrics if m['triage_time']]
    average_seconds_to_triage = sum(seconds_to_triage) / len(seconds_to_triage)
    maximum_seconds_to_triage = max(seconds_to_triage)
    num_p1s_fixed = len([m for m in metrics if m['bug_type'] == 'P1' and m['fixed_within_7_days']])
    downtime = sum(m['fix_time'].seconds for m in metrics if m['fix_time'] and m['bug_type'] == 'P0')

    print("Number of P0s:", num_p0s)
    print("Number of P1s:", num_p1s)
    print("Number of P2s:", num_p2s)
    print("Average time to triage:", average_seconds_to_triage / 86400.0)
    print("Maximum time to triage:", maximum_seconds_to_triage / 86400.0)
    print("% of P1s fixed in 7 days:", 100 * (float(num_p1s_fixed) / num_p1s))
    print("Downtime from P0s:", downtime)

def group_and_summarize_metrics(metrics):
    groups = {}
    metrics = [m for m in metrics if m]
    for m in metrics:
        groups.setdefault(m['time_slot'], []).append(m)
    for group, mlist in sorted(groups.items()):
        print('')
        print(group)
        print('=======')
        summarize_metrics(mlist)

if __name__ == '__main__':
    metrics = get_all_bug_metrics()
    group_and_summarize_metrics(metrics)