"""
Tool for hitting a Python backend with replayed darkmode data
pulled from the PHP layer.
"""

import patreon

import sys
import time
import uuid
import requests
import traceback



# This needs to be local because session data is pulled
# from the local filesystem.
base_url = "http://localhost:5000"


def get_request_params_from_queue():
    """Pull next request replay from the darkmode queue."""
    backoff = 0.1
    while True:
        message = patreon.services.aws.sqs.get('prodweb-python-darkmode')
        if message:
            return message
        time.sleep(backoff)
        backoff = min(backoff * 2, 2.0)


def prepare_request(message):
    cookies = message['cookie'] or {}
    session_id = cookies.get('session_id')


def send_request(message, retries=10):
    """Hit the backend with the request, playing through redirects as well."""
    for _ in range(retries):
        url = base_url + message['server']['REQUEST_URI']
        params = message['get']
        cookies = message['cookie']
        timestart = time.time()
        response = requests.get(url, cookies=cookies)
        try:
            response.encoding = 'utf-8'
            timeend = time.time()
            response.response_time = timeend - timestart
            return response
        except requests.adapters.ConnectionError:
            # Assume that the webserver is restarting or something.
            time.sleep(0.5)


def record_response(response):
    """Record the HTTP response code, the HTML, and response time.
    Upload the HTML to S3 and put the URL, response code, request params,
    render time taken, timestamp into DB."""
    # Upload response HTML to S3
    timestamp = time.time()
    response_key = str(uuid.uuid4())
    bucket = patreon.services.aws.S3Bucket("patreon-darkmode-logs")
    url = bucket.set(response_key, response.text)
    prefix = patreon.config.aws['prefix']
    response_time = int(response.response_time * 1000)

    patreon.services.db.query_log("""
        INSERT INTO darkmode_response (prefix, http_code, response_time, url, response_key, timestamp)
        VALUES (%s, %s, %s, %s, %s, %s)
        """, [prefix, response.status_code, response_time, response.url, response_key, timestamp])

    log_line = ' '.join([
        prefix,
        response.url,
        str(response.status_code),
        str(response_time),
        response_key,
        str(timestamp)
    ])
    print(log_line)


def main():
    try:
        while True:
            try:
                request_params = get_request_params_from_queue()
                prepare_request(request_params)
                response = send_request(request_params)
                record_response(response)
            except Exception as e:
                traceback.print_exc(file=sys.stderr)
    except KeyboardInterrupt:
        print("Quitting.")


if __name__ == '__main__':
    main()