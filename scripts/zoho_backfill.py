import json
import patreon
from patreon.model.manager import campaign_mgr

ids = campaign_mgr.find_all()
for id in ids:
    data = {
        'campaign_id': id[0]
    }

    patreon.services.aws.sqs.put('prodweb-zoho_update', json.dumps(data))
