import os
import sys

input_ = sys.argv[1]
lines = input_.split('\n')
keys = []
nodes = int(os.getenv('CIRCLE_NODE_TOTAL') or 1)
node_index = int(os.getenv('CIRCLE_NODE_INDEX') or 1)

for line in lines:
    parts = line.strip().replace('#', '').split(' ')
    if len(parts) != 4:
        continue
    if parts[1] == 'test.app.web.pages.test_walk.test_walk':
        continue
    id_ = int(parts[0])
    if (id_ % (nodes - 1)) + 1 == node_index:
        keys.append(str(id_))

print(' '.join(keys))
