/*
 * Reference for creating the log database. This file itself
 * does not do anything on deploy and needs to be manually
 * run against the log database to work.
 */

/** Response Log **/

create table response_log (
  request_ip varchar(80),
  user_id bigint,
  request_method varchar(80),
  url varchar(255),
  request_data text,
  request_headers json,
  response_code integer,
  response_key varchar(255),
  host varchar(255),
  session_id varchar(255),
  created_at timestamp
);

create index on response_log (host, created_at);
create index on response_log (request_ip, created_at);
create index on response_log (session_id, created_at);
create index on response_log (url, created_at);
create index on response_log (user_id, created_at);


/** Log Lines **/

create table loglines (
  action varchar(120),
  data json,
  created_at timestamp
);

create index on loglines (action, created_at);
create index on loglines (created_at, action);


/** Cron Log **/

create table cron_log (
  owner varchar(80),
  function_module varchar(255),
  function_name varchar(255),
  started_at timestamp,
  finished_at timestamp,
  status integer
);

create index on cron_log (started_at);
create index on cron_log (function_module, function_name, started_at);
