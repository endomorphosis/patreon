
/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


DROP TABLE IF EXISTS `a_b_test_assignments`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `a_b_test_assignments` (
  `a_b_test_assignment_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned NOT NULL,
  `group_id` varchar(255) NOT NULL,
  PRIMARY KEY (`a_b_test_assignment_id`),
  UNIQUE KEY `user_id` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;


DROP TABLE IF EXISTS `accounts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `accounts` (
  `account_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `type` varchar(64) NOT NULL,
  `user_id` bigint(20) unsigned NOT NULL DEFAULT '0',
  `payment_instrument_id` varchar(255) NOT NULL DEFAULT '',
  `created_at` datetime NOT NULL,
  `deleted_at` datetime DEFAULT NULL,
  `dirtied_at` datetime DEFAULT NULL,
  `system_classification` varchar(255) NOT NULL DEFAULT '',
  `campaign_id` bigint(20) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`account_id`),
  UNIQUE KEY `account_unique` (`type`,`user_id`,`payment_instrument_id`,`campaign_id`,`system_classification`),
  KEY `user_id` (`user_id`),
  KEY `payment_instrument_id` (`payment_instrument_id`),
  KEY `type` (`type`),
  KEY `user_id_type` (`user_id`,`type`),
  KEY `type-user_id` (`type`,`user_id`),
  KEY `type-payment_instrument_id` (`type`,`payment_instrument_id`),
  KEY `type-campaign_id` (`type`,`campaign_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;


DROP TABLE IF EXISTS `activities`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `activities` (
  `activity_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned NOT NULL,
  `campaign_id` int(10) unsigned NOT NULL,
  `activity_type` tinyint(3) unsigned NOT NULL,
  `post_type` varchar(32) NOT NULL,
  `activity_title` varchar(512) DEFAULT NULL,
  `activity_content` text,
  `category` smallint(5) unsigned DEFAULT NULL,
  `photo_key` varchar(128) DEFAULT NULL,
  `thumbnail_key` varchar(128) DEFAULT NULL,
  `photo_extension` varchar(32) DEFAULT NULL,
  `comment_count` smallint(6) NOT NULL DEFAULT '0',
  `min_cents_pledged_to_view` int(10) unsigned NOT NULL DEFAULT '0',
  `like_count` smallint(6) unsigned NOT NULL DEFAULT '0',
  `image_url` varchar(255) DEFAULT NULL,
  `image_large_url` varchar(255) DEFAULT NULL,
  `image_thumb_url` varchar(255) DEFAULT NULL,
  `image_height` smallint(6) unsigned NOT NULL DEFAULT '0',
  `image_width` smallint(6) unsigned NOT NULL DEFAULT '0',
  `link_url` varchar(255) DEFAULT NULL,
  `link_domain` varchar(128) DEFAULT NULL,
  `link_subject` varchar(255) DEFAULT NULL,
  `link_description` text,
  `link_embed` varchar(1024) DEFAULT NULL,
  `file_name` varchar(128) DEFAULT NULL,
  `file_key` varchar(128) DEFAULT NULL,
  `is_paid` tinyint(4) NOT NULL DEFAULT '0',
  `is_creation` tinyint(4) NOT NULL DEFAULT '0',
  `cents_pledged_at_creation` int(11) NOT NULL DEFAULT '0',
  `created_at` datetime NOT NULL,
  `published_at` datetime DEFAULT NULL,
  `edited_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  PRIMARY KEY (`activity_id`),
  KEY `created_at` (`created_at`),
  KEY `campaign_id` (`campaign_id`),
  KEY `activity_type` (`activity_type`),
  KEY `min_cents_pledged_to_view` (`min_cents_pledged_to_view`),
  KEY `like_count` (`like_count`),
  KEY `user_id` (`user_id`),
  KEY `comment_count` (`comment_count`),
  KEY `paid_popularity` (`min_cents_pledged_to_view`,`like_count`,`comment_count`),
  KEY `paid_creation_popularity` (`is_creation`,`min_cents_pledged_to_view`,`like_count`,`comment_count`),
  KEY `campaign_id_created_at` (`campaign_id`,`created_at`),
  KEY `campaign_id_published_at` (`campaign_id`,`published_at`),
  KEY `user_id_published_at` (`user_id`,`published_at`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;


DROP TABLE IF EXISTS `bill_payments`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `bill_payments` (
  `bill_payment_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `bill_id` bigint(20) unsigned NOT NULL,
  `transaction_id` bigint(20) unsigned NOT NULL,
  `pledge_amount_cents` int(11) NOT NULL,
  `fee_amount_cents` int(11) NOT NULL DEFAULT '0',
  `vat_amount_cents` int(11) NOT NULL DEFAULT '0',
  `type` varchar(255) NOT NULL,
  `external_payment_id` bigint(20) unsigned NOT NULL,
  `external_payment_amount_cents` int(11) NOT NULL DEFAULT '0',
  `credit_amount_cents` int(11) NOT NULL DEFAULT '0',
  `created_at` datetime NOT NULL,
  `attempted_at` datetime DEFAULT NULL,
  `failed_at` datetime DEFAULT NULL,
  `succeeded_at` datetime DEFAULT NULL,
  PRIMARY KEY (`bill_payment_id`),
  KEY `bill_id` (`bill_id`),
  KEY `transaction_id` (`transaction_id`),
  KEY `external_payment_id` (`external_payment_id`),
  KEY `created_at` (`created_at`),
  KEY `type` (`type`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;


DROP TABLE IF EXISTS `bills`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `bills` (
  `bill_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) unsigned NOT NULL,
  `campaign_id` bigint(20) unsigned NOT NULL,
  `pledge_id` bigint(20) unsigned NOT NULL,
  `vat_charge_id` bigint(20) unsigned DEFAULT NULL,
  `amount_cents` int(11) NOT NULL,
  `type` varchar(255) NOT NULL,
  `post_id` bigint(20) unsigned DEFAULT NULL,
  `billing_cycle` datetime DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `attempted_at` datetime DEFAULT NULL,
  `failed_at` datetime DEFAULT NULL,
  `succeeded_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  PRIMARY KEY (`bill_id`),
  KEY `user_id` (`user_id`),
  KEY `pledge_id` (`pledge_id`),
  KEY `campaign_id` (`campaign_id`),
  KEY `post_id` (`post_id`),
  KEY `created_at` (`created_at`),
  KEY `type` (`type`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;


DROP TABLE IF EXISTS `campaigns`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `campaigns` (
  `campaign_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `summary` text,
  `creation_name` varchar(64) DEFAULT NULL,
  `pay_per_name` varchar(64) DEFAULT NULL,
  `one_liner` varchar(256) DEFAULT NULL,
  `main_video_embed` varchar(512) DEFAULT NULL,
  `main_video_url` varchar(128) DEFAULT NULL,
  `image_small_url` varchar(128) DEFAULT NULL,
  `image_url` varchar(128) DEFAULT NULL,
  `thanks_video_url` varchar(128) DEFAULT NULL,
  `thanks_embed` varchar(512) DEFAULT NULL,
  `thanks_msg` text,
  `is_monthly` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `is_nsfw` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `created_at` datetime NOT NULL,
  `published_at` datetime DEFAULT NULL,
  PRIMARY KEY (`campaign_id`),
  KEY `published_at` (`published_at`),
  KEY `is_nsfw` (`is_nsfw`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;


DROP TABLE IF EXISTS `campaigns_users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `campaigns_users` (
  `user_id` int(10) unsigned NOT NULL,
  `campaign_id` int(10) unsigned NOT NULL,
  `is_admin` tinyint(3) unsigned NOT NULL DEFAULT '1',
  `joined_at` datetime NOT NULL,
  `should_email_on_patron_post` tinyint(3) unsigned NOT NULL DEFAULT '1',
  `should_email_on_new_patron` tinyint(3) unsigned NOT NULL DEFAULT '1',
  `should_push_on_patron_post` tinyint(3) unsigned DEFAULT '1',
  `should_push_on_new_patron` tinyint(3) unsigned DEFAULT '1',
  PRIMARY KEY (`user_id`,`campaign_id`),
  KEY `campaign_id` (`campaign_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;


DROP TABLE IF EXISTS `comment_flags`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `comment_flags` (
  `comment_id` int(10) unsigned NOT NULL,
  `user_id` int(10) unsigned NOT NULL,
  `flag_type` int(10) unsigned NOT NULL,
  `created_at` datetime NOT NULL,
  PRIMARY KEY (`comment_id`,`user_id`),
  KEY `user_id` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;


DROP TABLE IF EXISTS `comment_votes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `comment_votes` (
  `activity_id` bigint(20) unsigned NOT NULL,
  `comment_id` bigint(20) unsigned NOT NULL,
  `user_id` bigint(20) unsigned NOT NULL,
  `vote` tinyint(4) DEFAULT '0',
  PRIMARY KEY (`user_id`,`comment_id`),
  KEY `activity_id` (`activity_id`,`vote`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;


DROP TABLE IF EXISTS `creator_settings`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `creator_settings` (
  `user_id` int(10) unsigned NOT NULL,
  `payout_method` varchar(32) NOT NULL,
  `is_auto_pay` tinyint(3) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;


DROP TABLE IF EXISTS `credit_adjustments`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `credit_adjustments` (
  `credit_adjustment_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `txn_id` int(10) unsigned DEFAULT NULL,
  `amount_cents` int(11) NOT NULL,
  `user_id` int(10) unsigned NOT NULL,
  `adjuster_user_id` int(10) unsigned NOT NULL,
  `created_at` datetime NOT NULL,
  PRIMARY KEY (`credit_adjustment_id`),
  KEY `txn_id` (`txn_id`),
  KEY `user_id` (`user_id`),
  KEY `adjuster_id` (`adjuster_user_id`),
  KEY `created_at` (`created_at`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;


DROP TABLE IF EXISTS `credit_purchase`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `credit_purchase` (
  `credit_purchase_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `txn_id` int(10) unsigned DEFAULT NULL,
  `amount_cents` int(10) unsigned NOT NULL,
  `purchasing_user_id` int(10) unsigned NOT NULL,
  `campaign_id` int(10) unsigned NOT NULL,
  `created_at` datetime NOT NULL,
  `payment_instrument_id` varchar(128) DEFAULT NULL,
  PRIMARY KEY (`credit_purchase_id`),
  KEY `txn_id` (`txn_id`),
  KEY `user_id` (`purchasing_user_id`),
  KEY `campaign_id` (`campaign_id`),
  KEY `created_at` (`created_at`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;


DROP TABLE IF EXISTS `dmca_takedowns`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `dmca_takedowns` (
  `user_id` int(10) unsigned NOT NULL,
  `claimant_name` varchar(256) DEFAULT NULL,
  `claimant_url` varchar(256) DEFAULT NULL,
  `notice_url` varchar(256) DEFAULT NULL,
  PRIMARY KEY (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;


DROP TABLE IF EXISTS `emails`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `emails` (
  `email` varchar(128) NOT NULL,
  `user_id` int(10) unsigned NOT NULL,
  `created_at` datetime NOT NULL,
  `active_at` datetime DEFAULT NULL,
  `verified_at` datetime DEFAULT NULL,
  `activation_hash` varchar(128) NOT NULL,
  PRIMARY KEY (`email`,`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;


DROP TABLE IF EXISTS `external_payments`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `external_payments` (
  `external_payment_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `txn_id` bigint(20) DEFAULT NULL,
  `payment_instrument_id` bigint(20) unsigned NOT NULL,
  `type` varchar(64) DEFAULT NULL,
  `external_id` varchar(255) DEFAULT NULL,
  `amount_cents` int(10) unsigned NOT NULL,
  `fee_cents` int(10) unsigned NOT NULL DEFAULT '0',
  `created_at` datetime NOT NULL,
  `succeeded_at` datetime DEFAULT NULL,
  `failed_at` datetime DEFAULT NULL,
  `pending_at` datetime DEFAULT NULL,
  PRIMARY KEY (`external_payment_id`),
  KEY `txn_id` (`txn_id`),
  KEY `external_id` (`external_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;


DROP TABLE IF EXISTS `follows`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `follows` (
  `follower_id` bigint(20) unsigned NOT NULL DEFAULT '0',
  `followed_id` bigint(20) unsigned NOT NULL DEFAULT '0',
  `created_at` datetime NOT NULL,
  PRIMARY KEY (`follower_id`,`followed_id`),
  KEY `followed_id` (`followed_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;


DROP TABLE IF EXISTS `goals`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `goals` (
  `goal_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `campaign_id` int(10) unsigned NOT NULL,
  `amount_cents` int(10) unsigned NOT NULL,
  `goal_title` varchar(256) NOT NULL,
  `goal_text` text,
  `created_at` datetime NOT NULL,
  `reached_at` datetime DEFAULT NULL,
  PRIMARY KEY (`goal_id`),
  KEY `campaign_id` (`campaign_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;


DROP TABLE IF EXISTS `homepage_featured_campaigns`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `homepage_featured_campaigns` (
  `feature_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `campaign_id` int(10) unsigned NOT NULL,
  `approval_status` tinyint(4) NOT NULL,
  `launched_at` datetime NOT NULL,
  PRIMARY KEY (`feature_id`),
  UNIQUE KEY `campaign_id` (`campaign_id`),
  KEY `approval_status_launched_at` (`approval_status`,`launched_at`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;


DROP TABLE IF EXISTS `ledger_entries`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ledger_entries` (
  `ledger_entry_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `txn_id` bigint(20) unsigned NOT NULL,
  `account_id` int(11) NOT NULL,
  `amount_cents` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `squared_at` datetime DEFAULT NULL,
  `squared_by_txn_id` bigint(20) unsigned DEFAULT NULL,
  PRIMARY KEY (`ledger_entry_id`),
  KEY `txn_id` (`txn_id`),
  KEY `squared_at_and_account_id` (`squared_at`,`account_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;


DROP TABLE IF EXISTS `mail_queue`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mail_queue` (
  `id` bigint(20) NOT NULL DEFAULT '0',
  `create_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `time_to_send` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `sent_time` datetime DEFAULT NULL,
  `id_user` bigint(20) NOT NULL DEFAULT '0',
  `ip` varchar(20) NOT NULL DEFAULT 'unknown',
  `sender` varchar(50) NOT NULL DEFAULT '',
  `recipient` text NOT NULL,
  `headers` text NOT NULL,
  `body` longtext NOT NULL,
  `try_sent` tinyint(4) NOT NULL DEFAULT '0',
  `delete_after_send` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `id` (`id`),
  KEY `time_to_send` (`time_to_send`),
  KEY `id_user` (`id_user`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;


DROP TABLE IF EXISTS `mail_queue_seq`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mail_queue_seq` (
  `sequence` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`sequence`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;


DROP TABLE IF EXISTS `messages`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `messages` (
  `message_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `sender_id` bigint(20) unsigned NOT NULL,
  `recipient_id` bigint(20) unsigned NOT NULL,
  `subject` varchar(255) DEFAULT NULL,
  `content` text NOT NULL,
  `sent_at` datetime NOT NULL,
  `opened_at` datetime DEFAULT NULL,
  PRIMARY KEY (`message_id`),
  UNIQUE KEY `unique_index` (`sender_id`,`recipient_id`,`sent_at`),
  KEY `sender_id` (`sender_id`),
  KEY `recipient_id` (`recipient_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;


DROP TABLE IF EXISTS `monthlySnapshot`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `monthlySnapshot` (
  `UID` int(10) unsigned NOT NULL,
  `Type` tinyint(3) unsigned NOT NULL,
  `Count` int(10) unsigned NOT NULL,
  `Created` date NOT NULL,
  PRIMARY KEY (`UID`,`Created`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;


DROP TABLE IF EXISTS `oauth_access_tokens`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `oauth_access_tokens` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `access_token` varchar(255) DEFAULT NULL,
  `client_id` varchar(255) DEFAULT NULL,
  `user_id` varchar(255) DEFAULT NULL,
  `expires` datetime DEFAULT NULL,
  `scope` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `oauth_access_tokens__access_token` (`access_token`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;


DROP TABLE IF EXISTS `oauth_authorization_codes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `oauth_authorization_codes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `authorization_code` varchar(255) DEFAULT NULL,
  `client_id` varchar(255) DEFAULT NULL,
  `user_id` int(10) unsigned DEFAULT NULL,
  `redirect_uri` varchar(255) DEFAULT NULL,
  `expires` datetime DEFAULT NULL,
  `scope` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `oauth_authorization_codes_authorization_code` (`authorization_code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;


DROP TABLE IF EXISTS `oauth_client`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `oauth_client` (
  `client_id` varchar(127) NOT NULL,
  `name` varchar(127) NOT NULL,
  `description` varchar(511) NOT NULL,
  `user_id` int(10) unsigned NOT NULL,
  `is_confidential` tinyint(4) DEFAULT '0',
  `client_secret` varchar(127) NOT NULL,
  `_redirect_uris` text,
  `_default_scopes` text,
  PRIMARY KEY (`client_id`),
  UNIQUE KEY `client_secret` (`client_secret`),
  KEY `user_id` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;


DROP TABLE IF EXISTS `oauth_clients`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `oauth_clients` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `client_id` varchar(255) NOT NULL,
  `client_secret` varchar(255) DEFAULT NULL,
  `redirect_uri` varchar(255) DEFAULT NULL,
  `grant_types` varchar(255) DEFAULT NULL,
  `scope` varchar(255) DEFAULT NULL,
  `user_id` varchar(255) DEFAULT NULL,
  `public_key` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `oauth_clients__client_id` (`client_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;


DROP TABLE IF EXISTS `oauth_grant`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `oauth_grant` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned NOT NULL,
  `client_id` varchar(127) NOT NULL,
  `code` varchar(255) NOT NULL,
  `redirect_uri` varchar(255) DEFAULT NULL,
  `expires` datetime DEFAULT NULL,
  `_scopes` text,
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`),
  KEY `client_id` (`client_id`),
  KEY `code` (`code`),
  KEY `expires` (`expires`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;


DROP TABLE IF EXISTS `oauth_token`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `oauth_token` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned NOT NULL,
  `client_id` varchar(127) NOT NULL,
  `token_type` varchar(127) NOT NULL,
  `access_token` varchar(255) NOT NULL,
  `refresh_token` varchar(255) NOT NULL,
  `expires` datetime DEFAULT NULL,
  `_scopes` text,
  PRIMARY KEY (`id`),
  UNIQUE KEY `access_token` (`access_token`),
  UNIQUE KEY `refresh_token` (`refresh_token`),
  KEY `user_id` (`user_id`),
  KEY `client_id` (`client_id`),
  KEY `expires` (`expires`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;


DROP TABLE IF EXISTS `payment_events`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `payment_events` (
  `payment_event_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `type` varchar(255) NOT NULL,
  `subtype` varchar(255) NOT NULL,
  `json_data` text NOT NULL,
  `created_at` datetime NOT NULL,
  PRIMARY KEY (`payment_event_id`),
  KEY `type` (`type`),
  KEY `subtype` (`subtype`),
  KEY `type_and_subtype` (`type`,`subtype`),
  KEY `created_at` (`created_at`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;


DROP TABLE IF EXISTS `payouts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `payouts` (
  `payout_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `txn_id` int(10) unsigned DEFAULT NULL,
  `requested_amount_cents` int(10) unsigned NOT NULL,
  `actual_amount_cents` int(10) unsigned NOT NULL,
  `requesting_user_id` int(10) unsigned NOT NULL,
  `campaign_id` int(10) unsigned NOT NULL,
  `created_at` datetime NOT NULL,
  PRIMARY KEY (`payout_id`),
  KEY `txn_id` (`txn_id`),
  KEY `user_id` (`requesting_user_id`),
  KEY `campaign_id` (`campaign_id`),
  KEY `created_at` (`created_at`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;


DROP TABLE IF EXISTS `paypal_tokens`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `paypal_tokens` (
  `user_id` int(10) unsigned NOT NULL,
  `paypal_token` varchar(255) NOT NULL,
  `post_info_json` text NOT NULL,
  `created_at` datetime DEFAULT NULL,
  PRIMARY KEY (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;


DROP TABLE IF EXISTS `pledge_vat_locations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pledge_vat_locations` (
  `pledge_vat_location_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `ip_address` varchar(45) DEFAULT NULL,
  `geolocation_country` char(2) DEFAULT NULL,
  `selected_country` char(2) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  PRIMARY KEY (`pledge_vat_location_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;


DROP TABLE IF EXISTS `pledges`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pledges` (
  `pledge_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned NOT NULL,
  `campaign_id` int(10) unsigned NOT NULL,
  `amount_cents` int(10) unsigned NOT NULL,
  `pledge_cap_amount_cents` int(10) unsigned DEFAULT NULL,
  `payment_instrument_id` bigint(20) unsigned DEFAULT NULL,
  `reward_tier_id` int(10) unsigned DEFAULT NULL,
  `mailing_address_id` bigint(20) unsigned DEFAULT NULL,
  `pledge_vat_location_id` bigint(20) unsigned DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `deleted_at` datetime DEFAULT NULL,
  `patron_pays_fees` tinyint(4) DEFAULT '0',
  `transaction_fee_schedule_id` bigint(20) unsigned DEFAULT NULL,
  PRIMARY KEY (`pledge_id`),
  KEY `tblPledges_user_id_created_at` (`user_id`,`created_at`),
  KEY `tblPledges_user_id_deleted_at` (`user_id`,`deleted_at`),
  KEY `tblPledges_campaign_id_created_at` (`campaign_id`,`created_at`),
  KEY `tblPledges_campaign_id_deleted_at` (`campaign_id`,`deleted_at`),
  KEY `tblPledges_user_id_campaign_id_created_at` (`user_id`,`campaign_id`,`created_at`),
  KEY `tblPledges_user_id_campaign_id_deleted_at` (`user_id`,`campaign_id`,`deleted_at`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;


DROP TABLE IF EXISTS `popular_creations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `popular_creations` (
  `category` int(11) NOT NULL DEFAULT '0',
  `rank` int(11) NOT NULL,
  `creation_id` int(11) NOT NULL,
  `score` double DEFAULT NULL,
  PRIMARY KEY (`category`,`rank`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;


DROP TABLE IF EXISTS `presence`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `presence` (
  `presence_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned NOT NULL,
  `room` varchar(255) NOT NULL,
  `last_seen` datetime NOT NULL,
  `expiration_time` datetime NOT NULL,
  PRIMARY KEY (`presence_id`),
  KEY `user_id` (`user_id`,`last_seen`,`room`),
  KEY `room` (`room`,`last_seen`,`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;


DROP TABLE IF EXISTS `pstore`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pstore` (
  `update_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `key_name` varchar(120) DEFAULT NULL,
  `value_json` text,
  PRIMARY KEY (`update_id`),
  UNIQUE KEY `key_name` (`key_name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;


DROP TABLE IF EXISTS `schema_version`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `schema_version` (
  `version_rank` int(11) NOT NULL,
  `installed_rank` int(11) NOT NULL,
  `version` varchar(50) NOT NULL,
  `description` varchar(200) NOT NULL,
  `type` varchar(20) NOT NULL,
  `script` varchar(1000) NOT NULL,
  `checksum` int(11) DEFAULT NULL,
  `installed_by` varchar(100) NOT NULL,
  `installed_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `execution_time` int(11) NOT NULL,
  `success` tinyint(1) NOT NULL,
  PRIMARY KEY (`version`),
  KEY `schema_version_vr_idx` (`version_rank`),
  KEY `schema_version_ir_idx` (`installed_rank`),
  KEY `schema_version_s_idx` (`success`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;


DROP TABLE IF EXISTS `sessions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sessions` (
  `session_token` varchar(128) NOT NULL,
  `user_id` int(10) unsigned DEFAULT NULL,
  `csrf_token` varchar(64) DEFAULT NULL,
  `csrf_token_expires_at` datetime DEFAULT NULL,
  `is_admin` tinyint(1) DEFAULT '0',
  `extra_data_json` text,
  `created_at` datetime DEFAULT NULL,
  `expires_at` datetime DEFAULT NULL,
  PRIMARY KEY (`session_token`),
  KEY `user_id` (`user_id`),
  KEY `expires_at` (`expires_at`),
  KEY `is_admin_index` (`is_admin`),
  KEY `session_token` (`session_token`,`expires_at`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;


DROP TABLE IF EXISTS `sessions_new`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sessions_new` (
  `session_token` varchar(128) NOT NULL,
  `user_id` int(10) unsigned DEFAULT NULL,
  `csrf_token` varchar(64) DEFAULT NULL,
  `csrf_token_expires_at` datetime DEFAULT NULL,
  `is_admin` tinyint(1) DEFAULT '0',
  `extra_data_json` text,
  `created_at` datetime DEFAULT NULL,
  `expires_at` datetime DEFAULT NULL,
  PRIMARY KEY (`session_token`),
  KEY `user_id` (`user_id`),
  KEY `expires_at` (`expires_at`),
  KEY `is_admin_index` (`is_admin`),
  KEY `sessions_id_expires` (`session_token`,`expires_at`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;


DROP TABLE IF EXISTS `shipping_addresses`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `shipping_addresses` (
  `address_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) unsigned NOT NULL,
  `name` varchar(128) DEFAULT NULL,
  `addressee` varchar(128) DEFAULT NULL,
  `line_1` varchar(128) DEFAULT NULL,
  `line_2` varchar(128) DEFAULT NULL,
  `postal_code` varchar(32) DEFAULT NULL,
  `city` varchar(64) DEFAULT NULL,
  `state` varchar(32) DEFAULT NULL,
  `country` varchar(32) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `deleted_at` datetime DEFAULT NULL,
  PRIMARY KEY (`address_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;


DROP TABLE IF EXISTS `stats_averages`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `stats_averages` (
  `ID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `Date` date NOT NULL,
  `Value` decimal(18,4) NOT NULL,
  `Key` varchar(64) NOT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `Date` (`Date`,`Key`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;


DROP TABLE IF EXISTS `subbable_pledge`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `subbable_pledge` (
  `email` varchar(255) NOT NULL DEFAULT '',
  `name` varchar(255) DEFAULT NULL,
  `creator_shortname` varchar(255) NOT NULL DEFAULT '',
  `monthly_amount_cents` bigint(20) unsigned DEFAULT '0',
  PRIMARY KEY (`email`,`creator_shortname`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;


DROP TABLE IF EXISTS `subbable_state`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `subbable_state` (
  `user_id` int(10) unsigned NOT NULL,
  `subbable_email` varchar(255) DEFAULT NULL,
  `subbable_state` int(10) unsigned NOT NULL DEFAULT '0',
  `data_json` text,
  `created_at` datetime DEFAULT NULL,
  PRIMARY KEY (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;


DROP TABLE IF EXISTS `taxForms`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `taxForms` (
  `UID` int(11) NOT NULL,
  `FullName` varchar(256) DEFAULT NULL,
  `BirthDate` text,
  `BusinessName` varchar(256) DEFAULT NULL,
  `Address` text,
  `AddressLocation` text,
  `State` text,
  `Zip` text,
  `USTaxID` text,
  `TaxID` text,
  `TaxClass` varchar(32) DEFAULT NULL,
  `ElectronicForms` tinyint(4) DEFAULT NULL,
  `Signature` varchar(256) DEFAULT NULL,
  `CountryCitizen` varchar(256) DEFAULT NULL,
  `Country` varchar(256) DEFAULT NULL,
  `Reference` varchar(64) DEFAULT NULL,
  `TreatyArticle` varchar(32) DEFAULT NULL,
  `TreatyPercent` varchar(32) DEFAULT NULL,
  `TreatyIncome` varchar(32) DEFAULT NULL,
  `TreatyReason` varchar(32) DEFAULT NULL,
  `ActingCapacity` varchar(256) DEFAULT NULL,
  `Type` tinyint(4) NOT NULL DEFAULT '0',
  `Created` datetime DEFAULT NULL,
  PRIMARY KEY (`UID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;


DROP TABLE IF EXISTS `tblAPIKeys`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tblAPIKeys` (
  `UID` int(10) unsigned NOT NULL,
  `APIKey` varchar(64) NOT NULL,
  `Created` datetime NOT NULL,
  PRIMARY KEY (`UID`,`APIKey`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;


DROP TABLE IF EXISTS `tblAPITokens`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tblAPITokens` (
  `UID` int(10) unsigned NOT NULL,
  `Token` varchar(32) NOT NULL,
  `Created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`UID`,`Token`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;


DROP TABLE IF EXISTS `tblActions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tblActions` (
  `UID` int(10) unsigned NOT NULL,
  `HID` int(10) unsigned NOT NULL,
  `Type` smallint(5) unsigned NOT NULL,
  `Created` datetime NOT NULL,
  PRIMARY KEY (`UID`,`HID`,`Type`),
  KEY `tblActions_Created` (`Created`),
  KEY `tblActions_UID_Type` (`UID`,`Type`),
  KEY `tblActions_HID` (`HID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;


DROP TABLE IF EXISTS `tblAlerts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tblAlerts` (
  `AlertID` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `AlertType` int(11) DEFAULT NULL,
  `Begins` datetime DEFAULT NULL,
  `Expires` datetime DEFAULT NULL,
  `CustomText` text,
  PRIMARY KEY (`AlertID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;


DROP TABLE IF EXISTS `tblAlertsDismissed`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tblAlertsDismissed` (
  `AlertID` int(11) DEFAULT NULL,
  `UID` int(11) DEFAULT NULL,
  KEY `AlertID` (`AlertID`,`UID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;


DROP TABLE IF EXISTS `tblAlreadyPaid`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tblAlreadyPaid` (
  `ID` int(11) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;


DROP TABLE IF EXISTS `tblBackupPledges`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tblBackupPledges` (
  `CID` int(10) unsigned DEFAULT NULL,
  `UID` int(10) unsigned DEFAULT NULL,
  `Pledge` int(10) unsigned DEFAULT NULL,
  `MaxAmount` int(10) unsigned DEFAULT NULL,
  `RID` int(10) unsigned DEFAULT NULL,
  `Invalid` datetime DEFAULT NULL,
  `Created` datetime DEFAULT NULL,
  `CardID` varchar(32) DEFAULT NULL,
  `LastUpdated` datetime DEFAULT NULL,
  `Street` varchar(128) DEFAULT NULL,
  `Street2` varchar(128) DEFAULT NULL,
  `City` varchar(64) DEFAULT NULL,
  `State` varchar(32) DEFAULT NULL,
  `Zip` varchar(32) DEFAULT NULL,
  `Country` varchar(32) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;


DROP TABLE IF EXISTS `tblBackupUsers`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tblBackupUsers` (
  `UID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `Email` varchar(128) DEFAULT NULL,
  `FName` varchar(64) DEFAULT NULL,
  `LName` varchar(64) DEFAULT NULL,
  `Password` varchar(64) DEFAULT NULL,
  `Created` datetime DEFAULT NULL,
  `FBID` bigint(16) unsigned DEFAULT NULL,
  `ImageUrl` varchar(128) DEFAULT NULL,
  `ThumbUrl` varchar(128) DEFAULT NULL,
  `Gender` tinyint(4) DEFAULT '0',
  `Status` tinyint(4) DEFAULT '0',
  `Vanity` varchar(64) DEFAULT NULL,
  `About` text,
  `IsCreator` tinyint(4) DEFAULT '0',
  `CreatorImageUrl` varchar(128) DEFAULT NULL,
  `CreatorOneLine` varchar(255) DEFAULT NULL,
  `CreatorCreate` varchar(64) DEFAULT NULL,
  `CreatorCreateSingle` varchar(64) DEFAULT NULL,
  `CreatorImageSmallUrl` varchar(128) DEFAULT NULL,
  `CreatorVideoUrl` varchar(128) DEFAULT NULL,
  `CreatorEmbed` varchar(255) DEFAULT NULL,
  `Youtube` varchar(64) DEFAULT NULL,
  `Twitter` varchar(64) DEFAULT NULL,
  `Facebook` varchar(64) DEFAULT NULL,
  `CreatorAbout` text,
  PRIMARY KEY (`UID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;


DROP TABLE IF EXISTS `tblCardInfo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tblCardInfo` (
  `RID` varchar(32) NOT NULL,
  `UID` int(10) unsigned NOT NULL,
  `Name` varchar(255) DEFAULT NULL,
  `Street` varchar(128) DEFAULT NULL,
  `Street2` varchar(128) DEFAULT NULL,
  `City` varchar(64) DEFAULT NULL,
  `State` varchar(32) DEFAULT NULL,
  `Zip` varchar(32) DEFAULT NULL,
  `ExpirationDate` date DEFAULT NULL,
  `Number` smallint(5) unsigned DEFAULT NULL,
  `Type` tinyint(3) unsigned DEFAULT NULL,
  `Created` datetime DEFAULT NULL,
  `Country` varchar(32) DEFAULT NULL,
  `Verified` tinyint(4) NOT NULL DEFAULT '0',
  PRIMARY KEY (`RID`,`UID`),
  KEY `tblCardInfo_UID` (`UID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;


DROP TABLE IF EXISTS `tblCategories`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tblCategories` (
  `UID` int(10) unsigned NOT NULL,
  `Category` smallint(5) unsigned NOT NULL,
  PRIMARY KEY (`UID`,`Category`),
  KEY `tblCategories_Category_UID` (`Category`,`UID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;


DROP TABLE IF EXISTS `tblChangeEmail`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tblChangeEmail` (
  `Verification` varchar(32) NOT NULL,
  `Email` varchar(128) NOT NULL,
  `UID` int(10) unsigned NOT NULL,
  `Created` datetime NOT NULL,
  PRIMARY KEY (`Verification`),
  KEY `tblChangeEmail_Created` (`Created`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;


DROP TABLE IF EXISTS `tblChargeDetails`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tblChargeDetails` (
  `ChargeDetailsID` int(11) NOT NULL AUTO_INCREMENT,
  `ChargeID` varchar(128) DEFAULT NULL,
  `Amount` int(11) DEFAULT NULL,
  `Fee` int(11) DEFAULT NULL,
  PRIMARY KEY (`ChargeDetailsID`),
  KEY `ChargeID` (`ChargeID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;


DROP TABLE IF EXISTS `tblCharges`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tblCharges` (
  `CID` varchar(32) NOT NULL,
  `UID` int(10) unsigned NOT NULL,
  `SID` int(10) unsigned NOT NULL,
  `Amount` int(11) NOT NULL,
  `Fee` int(11) NOT NULL,
  `Created` datetime NOT NULL,
  PRIMARY KEY (`CID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;


DROP TABLE IF EXISTS `tblChargesSetup`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tblChargesSetup` (
  `UID` int(10) unsigned NOT NULL,
  `CardID` varchar(32) NOT NULL DEFAULT '',
  `Amount` int(10) unsigned NOT NULL,
  `Type` tinyint(3) unsigned DEFAULT NULL,
  PRIMARY KEY (`UID`,`CardID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;


DROP TABLE IF EXISTS `tblChurnTest`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tblChurnTest` (
  `UID` int(11) NOT NULL,
  `CID` int(11) NOT NULL,
  `LastCreated` datetime DEFAULT NULL,
  `LastDeleted` datetime DEFAULT NULL,
  PRIMARY KEY (`UID`,`CID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;


DROP TABLE IF EXISTS `tblComments`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tblComments` (
  `CID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `UID` int(10) unsigned NOT NULL,
  `HID` int(10) unsigned NOT NULL,
  `Created` datetime(3) DEFAULT NULL,
  `Comment` text NOT NULL,
  `ThreadID` int(11) unsigned DEFAULT NULL,
  `DeletedAt` datetime(3) DEFAULT NULL,
  `EditedAt` datetime(3) DEFAULT NULL,
  `is_spam` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `vote_sum` int(11) DEFAULT '0',
  PRIMARY KEY (`CID`),
  KEY `tblComments_HID_Created` (`HID`,`Created`),
  KEY `tblComments_UID` (`UID`),
  KEY `tblComments_threadID` (`ThreadID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;


DROP TABLE IF EXISTS `tblCredits`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tblCredits` (
  `CID` int(11) NOT NULL AUTO_INCREMENT,
  `UID` int(10) unsigned NOT NULL,
  `SID` int(10) unsigned DEFAULT NULL,
  `Amount` int(11) NOT NULL,
  `Fee` int(11) NOT NULL DEFAULT '0',
  `Created` datetime NOT NULL,
  `PatreonFee` int(11) NOT NULL,
  `Type` tinyint(4) NOT NULL DEFAULT '1',
  PRIMARY KEY (`CID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;


DROP TABLE IF EXISTS `tblData`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tblData` (
  `ID` int(10) unsigned NOT NULL,
  `type` int(10) unsigned NOT NULL,
  `data` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`ID`,`type`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;


DROP TABLE IF EXISTS `tblFeatured`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tblFeatured` (
  `HID` int(10) unsigned NOT NULL,
  `Category` smallint(5) unsigned NOT NULL DEFAULT '0',
  `Weight` int(10) unsigned NOT NULL,
  `Created` datetime DEFAULT NULL,
  PRIMARY KEY (`HID`,`Category`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;


DROP TABLE IF EXISTS `tblForgotPassword`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tblForgotPassword` (
  `SecretID` varchar(32) NOT NULL,
  `UID` int(10) unsigned NOT NULL,
  `Created` datetime NOT NULL,
  PRIMARY KEY (`SecretID`),
  KEY `tblForgotPassword_UID` (`UID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;


DROP TABLE IF EXISTS `tblLinkPledgeShare`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tblLinkPledgeShare` (
  `HID` int(10) unsigned NOT NULL,
  `UID` int(10) unsigned NOT NULL,
  `RID` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`HID`,`UID`),
  KEY `tblLinkPledgeShare_RID` (`RID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;


DROP TABLE IF EXISTS `tblLoginTokens`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tblLoginTokens` (
  `UID` int(10) unsigned NOT NULL,
  `Series` varchar(32) NOT NULL,
  `Token` varchar(32) NOT NULL,
  `Updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`UID`,`Series`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;


DROP TABLE IF EXISTS `tblNews`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tblNews` (
  `NID` int(10) unsigned NOT NULL,
  `Type` smallint(5) unsigned NOT NULL,
  `UID` int(10) unsigned NOT NULL,
  `OtherUID` int(10) unsigned DEFAULT NULL,
  `HID` int(10) unsigned DEFAULT NULL,
  `SID` int(10) unsigned DEFAULT NULL,
  `Created` datetime NOT NULL,
  `Viewed` tinyint(4) NOT NULL DEFAULT '0',
  PRIMARY KEY (`NID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;


DROP TABLE IF EXISTS `tblPayRequested`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tblPayRequested` (
  `UID` int(10) unsigned NOT NULL,
  `Created` datetime DEFAULT NULL,
  PRIMARY KEY (`UID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;


DROP TABLE IF EXISTS `tblPayouts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tblPayouts` (
  `PayID` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `UID` int(10) unsigned NOT NULL,
  `Amount` int(11) NOT NULL,
  `Created` datetime NOT NULL,
  `Type` tinyint(4) NOT NULL DEFAULT '0',
  `TransID` varchar(32) DEFAULT NULL,
  `Fee` int(11) NOT NULL DEFAULT '0',
  `CreditUsed` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`PayID`),
  KEY `tblPayouts_UID` (`UID`),
  KEY `tblPayouts_Type` (`Type`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;


DROP TABLE IF EXISTS `tblPledges`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tblPledges` (
  `CID` int(10) unsigned NOT NULL,
  `UID` int(10) unsigned NOT NULL,
  `Pledge` int(10) unsigned NOT NULL,
  `MaxAmount` int(10) unsigned DEFAULT NULL,
  `RID` int(10) unsigned DEFAULT NULL,
  `address_id` int(11) DEFAULT NULL,
  `Invalid` datetime DEFAULT NULL,
  `Created` datetime NOT NULL,
  `CardID` varchar(32) NOT NULL DEFAULT '',
  `LastUpdated` datetime DEFAULT NULL,
  `Street` varchar(128) DEFAULT NULL,
  `Street2` varchar(128) DEFAULT NULL,
  `City` varchar(64) DEFAULT NULL,
  `State` varchar(32) DEFAULT NULL,
  `Zip` varchar(32) DEFAULT NULL,
  `Country` varchar(32) DEFAULT NULL,
  `EmailPost` tinyint(4) unsigned NOT NULL DEFAULT '1',
  `EmailPaid` tinyint(4) unsigned NOT NULL DEFAULT '1',
  `PatronPaysFees` tinyint(4) DEFAULT '0',
  PRIMARY KEY (`CID`,`UID`),
  KEY `tblPledges_RID` (`RID`),
  KEY `tblPledges_Invalid` (`Invalid`),
  KEY `tblPledges_Pledge` (`Pledge`),
  KEY `tblPledges_Created` (`Created`),
  KEY `tblPledges_UID` (`UID`),
  KEY `tblPledges_CID_and_Pledge` (`CID`,`Pledge`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;


DROP TABLE IF EXISTS `tblPledgesDelete`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tblPledgesDelete` (
  `CID` int(10) unsigned NOT NULL,
  `UID` int(10) unsigned NOT NULL,
  `Amount` int(10) unsigned NOT NULL,
  `Created` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;


DROP TABLE IF EXISTS `tblPledgesHistory`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tblPledgesHistory` (
  `CID` int(10) unsigned NOT NULL,
  `UID` int(10) unsigned NOT NULL,
  `Pledge` int(10) unsigned NOT NULL,
  `MaxAmount` int(10) unsigned DEFAULT NULL,
  `Created` datetime NOT NULL,
  PRIMARY KEY (`CID`,`UID`,`Created`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;


DROP TABLE IF EXISTS `tblReportDone`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tblReportDone` (
  `UID` int(10) unsigned NOT NULL,
  PRIMARY KEY (`UID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;


DROP TABLE IF EXISTS `tblRewards`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tblRewards` (
  `RID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `UID` int(10) unsigned NOT NULL,
  `Amount` int(10) unsigned NOT NULL DEFAULT '0',
  `UserLimit` int(10) unsigned DEFAULT NULL,
  `Description` text,
  `Created` datetime NOT NULL,
  `Shipping` tinyint(4) NOT NULL DEFAULT '0',
  PRIMARY KEY (`RID`),
  KEY `tblRewards_UID` (`UID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;


DROP TABLE IF EXISTS `tblRewardsGive`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tblRewardsGive` (
  `ID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `UID` int(10) unsigned NOT NULL,
  `HID` int(10) unsigned NOT NULL,
  `RID` int(10) unsigned NOT NULL,
  `address_id` int(11) DEFAULT NULL,
  `RDescription` text,
  `CID` int(10) unsigned NOT NULL,
  `Status` tinyint(3) unsigned NOT NULL,
  `Created` datetime DEFAULT NULL,
  `Pledge` int(11) unsigned DEFAULT NULL,
  `CardID` varchar(32) DEFAULT NULL,
  `Street` varchar(128) DEFAULT NULL,
  `Street2` varchar(128) DEFAULT NULL,
  `City` varchar(64) DEFAULT NULL,
  `State` varchar(32) DEFAULT NULL,
  `Zip` varchar(32) DEFAULT NULL,
  `Country` varchar(32) DEFAULT NULL,
  `Fee` int(11) NOT NULL DEFAULT '0',
  `PatreonFee` int(11) NOT NULL DEFAULT '0',
  `PayID` int(11) DEFAULT NULL,
  `Complete` tinyint(4) NOT NULL DEFAULT '0',
  `transaction_fee_schedule_id` bigint(20) unsigned DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `tblRewardsGive_UID` (`UID`),
  KEY `tblRewardsGive_CID` (`CID`),
  KEY `tblRewardsGive_Status` (`Status`),
  KEY `tblRewardsGive_HID` (`HID`),
  KEY `tblRewardsGive_CID_Status` (`CID`,`Status`),
  KEY `tblRewardsGive_CID_Status_UID` (`CID`,`Status`,`UID`),
  KEY `Created` (`Created`),
  KEY `Status_Created` (`Status`,`Created`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;


DROP TABLE IF EXISTS `tblRewardsPaid`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tblRewardsPaid` (
  `ID` int(10) unsigned NOT NULL,
  `ChargeID` varchar(128) NOT NULL,
  `Type` smallint(5) unsigned DEFAULT NULL,
  `ChargeDetailsID` int(11) DEFAULT NULL,
  `Created` datetime NOT NULL,
  `Refunded` datetime DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `ChargeID` (`ChargeID`),
  KEY `ChargeDetailsID` (`ChargeDetailsID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;


DROP TABLE IF EXISTS `tblRewardsWait`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tblRewardsWait` (
  `RID` int(10) unsigned NOT NULL,
  `UID` int(10) unsigned NOT NULL,
  PRIMARY KEY (`RID`,`UID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;


DROP TABLE IF EXISTS `tblSearchText`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tblSearchText` (
  `UID` int(10) unsigned NOT NULL,
  `SearchText` varchar(256) NOT NULL,
  `Score` mediumint(8) unsigned NOT NULL,
  PRIMARY KEY (`UID`),
  KEY `tblSearchText_Score` (`Score`),
  FULLTEXT KEY `tblSearchText_SearchText` (`SearchText`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;


DROP TABLE IF EXISTS `tblSharesAttach`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tblSharesAttach` (
  `ID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `HID` int(10) unsigned NOT NULL,
  `Url` varchar(255) NOT NULL,
  `Name` varchar(128) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `tblSharesAttach_HID` (`HID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;


DROP TABLE IF EXISTS `tblStatsRefer`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tblStatsRefer` (
  `UID` int(10) unsigned NOT NULL,
  `CID` int(10) unsigned NOT NULL,
  `HID` int(10) unsigned NOT NULL,
  `Count` int(10) unsigned NOT NULL,
  PRIMARY KEY (`UID`,`CID`,`HID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;


DROP TABLE IF EXISTS `tblUnverifiedUsers`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tblUnverifiedUsers` (
  `Verification` varchar(32) NOT NULL DEFAULT '',
  `Email` varchar(128) NOT NULL,
  `FName` varchar(64) DEFAULT NULL,
  `LName` varchar(64) DEFAULT NULL,
  `Password` varchar(64) DEFAULT NULL,
  `Created` datetime NOT NULL,
  `FBID` int(10) unsigned DEFAULT NULL,
  `ImageUrl` varchar(128) DEFAULT NULL,
  `ThumbUrl` varchar(128) DEFAULT NULL,
  `Gender` tinyint(4) NOT NULL DEFAULT '0',
  `Vanity` varchar(64) DEFAULT NULL,
  PRIMARY KEY (`Verification`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;


DROP TABLE IF EXISTS `tblUsers`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tblUsers` (
  `UID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `Email` varchar(128) DEFAULT NULL,
  `FName` varchar(64) DEFAULT NULL,
  `LName` varchar(64) DEFAULT NULL,
  `Password` varchar(64) DEFAULT NULL,
  `Created` datetime NOT NULL,
  `FBID` bigint(16) unsigned DEFAULT NULL,
  `ImageUrl` varchar(128) DEFAULT NULL,
  `ThumbUrl` varchar(128) DEFAULT NULL,
  `photo_key` varchar(128) DEFAULT NULL,
  `thumbnail_key` varchar(128) DEFAULT NULL,
  `Gender` tinyint(4) NOT NULL DEFAULT '0',
  `Status` tinyint(4) NOT NULL DEFAULT '0',
  `Vanity` varchar(64) DEFAULT NULL,
  `About` text,
  `Youtube` varchar(64) DEFAULT NULL,
  `Twitter` varchar(64) DEFAULT NULL,
  `Facebook` varchar(64) DEFAULT NULL,
  `is_suspended` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `is_deleted` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `is_nuked` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `is_admin` tinyint(4) DEFAULT '0',
  `two_factor_enabled` tinyint(4) DEFAULT '0',
  PRIMARY KEY (`UID`),
  UNIQUE KEY `tblUsers_Email` (`Email`),
  UNIQUE KEY `tblUsers_FBID` (`FBID`),
  KEY `tblUsers_Vanity` (`Vanity`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;


DROP TABLE IF EXISTS `tblUsersExtra`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tblUsersExtra` (
  `UID` int(10) unsigned NOT NULL,
  `LastNewsUpdate` datetime NOT NULL DEFAULT '2013-03-05 00:00:00',
  `NewMsgs` mediumint(9) unsigned NOT NULL DEFAULT '0',
  `PaypalEmail` varchar(128) DEFAULT NULL,
  `PayStreet` varchar(128) DEFAULT NULL,
  `PayStreet2` varchar(128) DEFAULT NULL,
  `PayCity` varchar(64) DEFAULT NULL,
  `PayState` varchar(32) DEFAULT NULL,
  `PayZip` varchar(32) DEFAULT NULL,
  `PayCountry` varchar(32) DEFAULT NULL,
  `FlagYoutube` tinyint(4) NOT NULL DEFAULT '0',
  `StripeRecipientID` varchar(128) DEFAULT NULL,
  `IsUS` tinyint(4) NOT NULL DEFAULT '0',
  `StripeLastFour` smallint(5) unsigned DEFAULT NULL,
  `StripeName` varchar(128) DEFAULT NULL,
  `IsCorp` tinyint(4) NOT NULL DEFAULT '0',
  `AutoPay` tinyint(4) NOT NULL DEFAULT '0',
  `PayMethod` tinyint(4) NOT NULL DEFAULT '0',
  `PayoneerSetup` tinyint(4) NOT NULL DEFAULT '0',
  PRIMARY KEY (`UID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;


DROP TABLE IF EXISTS `tblUsersSettings`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tblUsersSettings` (
  `UID` int(10) unsigned NOT NULL,
  `FBImport` tinyint(4) NOT NULL DEFAULT '1',
  `Privacy` tinyint(4) NOT NULL DEFAULT '0',
  PRIMARY KEY (`UID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;


DROP TABLE IF EXISTS `tblYoutubeImport`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tblYoutubeImport` (
  `ID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `UID` int(10) unsigned NOT NULL,
  `Title` varchar(512) DEFAULT NULL,
  `Content` text,
  `Url` varchar(255) DEFAULT NULL,
  `ImageUrl` varchar(255) DEFAULT NULL,
  `Created` datetime DEFAULT NULL,
  `VideoID` varchar(16) NOT NULL DEFAULT '',
  PRIMARY KEY (`ID`),
  UNIQUE KEY `tblYoutubeImport_UIDVideoID` (`UID`,`VideoID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;


DROP TABLE IF EXISTS `transaction_fee_schedules`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `transaction_fee_schedules` (
  `transaction_fee_schedule_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `flat_amount_cents` int(11) DEFAULT NULL,
  `percentage_basis_points` int(11) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  PRIMARY KEY (`transaction_fee_schedule_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;


DROP TABLE IF EXISTS `two_factor_codes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `two_factor_codes` (
  `two_factor_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned NOT NULL,
  `type` int(10) unsigned NOT NULL DEFAULT '1',
  `secret` text NOT NULL,
  PRIMARY KEY (`two_factor_id`),
  KEY `user_id` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;


DROP TABLE IF EXISTS `txn_history`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `txn_history` (
  `txn_history_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `txn_id` int(10) unsigned DEFAULT NULL,
  `txn_succeeded_at` datetime DEFAULT NULL,
  `txn_failed_at` datetime DEFAULT NULL,
  `txn_began_processing_at` datetime DEFAULT NULL,
  `txn_created_at` datetime DEFAULT NULL,
  `txn_pending_at` datetime DEFAULT NULL,
  PRIMARY KEY (`txn_history_id`),
  KEY `txn_id` (`txn_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;


DROP TABLE IF EXISTS `txns`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `txns` (
  `txn_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `created_at` datetime NOT NULL,
  `succeeded_at` datetime DEFAULT NULL,
  `failed_at` datetime DEFAULT NULL,
  `type` varchar(64) DEFAULT NULL,
  `began_processing_at` datetime DEFAULT NULL,
  `pending_at` datetime DEFAULT NULL,
  `internal_note` text,
  PRIMARY KEY (`txn_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;


DROP TABLE IF EXISTS `unread_status`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `unread_status` (
  `unread_key` varchar(255) NOT NULL,
  `user_id` int(10) unsigned NOT NULL,
  `read_timestamp` datetime NOT NULL,
  PRIMARY KEY (`user_id`,`unread_key`),
  KEY `user_id` (`user_id`),
  KEY `unread_key` (`unread_key`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;


DROP TABLE IF EXISTS `user_campaign_settings`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_campaign_settings` (
  `user_id` int(10) unsigned NOT NULL,
  `campaign_id` int(10) unsigned NOT NULL,
  `email_new_comment` tinyint(3) unsigned NOT NULL DEFAULT '1',
  `email_post` tinyint(3) unsigned NOT NULL DEFAULT '1',
  `email_paid_post` tinyint(3) unsigned NOT NULL DEFAULT '1',
  `push_new_comment` tinyint(3) unsigned DEFAULT '1',
  `push_post` tinyint(3) unsigned DEFAULT '1',
  `push_paid_post` tinyint(3) unsigned DEFAULT '1',
  `push_creator_live` tinyint(3) unsigned DEFAULT '1',
  PRIMARY KEY (`user_id`,`campaign_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;


DROP TABLE IF EXISTS `user_locations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_locations` (
  `user_id` int(10) unsigned NOT NULL,
  `lat_long` point NOT NULL,
  `display_name` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`user_id`,`lat_long`(25)),
  SPATIAL KEY `lat_long` (`lat_long`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;


DROP TABLE IF EXISTS `user_settings`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_settings` (
  `user_id` int(10) unsigned NOT NULL,
  `email_new_comment` tinyint(3) unsigned DEFAULT '1',
  `email_goal` tinyint(3) unsigned DEFAULT '1',
  `email_patreon` tinyint(3) unsigned DEFAULT '1',
  `disable_email_post` tinyint(3) unsigned DEFAULT '0',
  `disable_email_paid_post` tinyint(3) unsigned DEFAULT '0',
  `push_new_comment` tinyint(3) unsigned DEFAULT '1',
  `push_goal` tinyint(3) unsigned DEFAULT '1',
  `push_patreon` tinyint(3) unsigned DEFAULT '1',
  PRIMARY KEY (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;


DROP TABLE IF EXISTS `users_push_info`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users_push_info` (
  `users_push_info_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned NOT NULL,
  `bundle_id` varchar(255) DEFAULT NULL,
  `token` varchar(255) DEFAULT NULL,
  `sns_arn` varchar(255) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `edited_at` datetime NOT NULL,
  PRIMARY KEY (`users_push_info_id`),
  KEY `user_id` (`user_id`),
  KEY `bundle_id` (`bundle_id`),
  KEY `users_push_info_user_id_bundle_id` (`user_id`,`bundle_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;


DROP TABLE IF EXISTS `vat_charges`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `vat_charges` (
  `vat_charge_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `vat_amount_cents` int(10) unsigned NOT NULL,
  `rate_basis_points` int(10) unsigned NOT NULL,
  `rate_type` varchar(32) NOT NULL DEFAULT 'standard',
  `vat_rules_version_id` int(10) unsigned NOT NULL,
  `vat_product_category_id` int(10) unsigned DEFAULT NULL,
  `vat_remittance_batch_id` int(10) unsigned DEFAULT NULL,
  `rewards_give_id` bigint(20) unsigned DEFAULT NULL,
  `txn_id` bigint(20) unsigned DEFAULT NULL,
  `created_at` datetime NOT NULL,
  PRIMARY KEY (`vat_charge_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;


DROP TABLE IF EXISTS `vat_rates_versions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `vat_rates_versions` (
  `vat_rates_version_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `version` varchar(64) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  PRIMARY KEY (`vat_rates_version_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;


DROP TABLE IF EXISTS `vat_remittance_batches`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `vat_remittance_batches` (
  `vat_remittance_batch_id` bigint(20) unsigned DEFAULT NULL,
  `description` text,
  `created_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;


DROP TABLE IF EXISTS `zoho_campaigns`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `zoho_campaigns` (
  `campaign_id` int(20) NOT NULL DEFAULT '0',
  `zoho_id` varchar(255) NOT NULL DEFAULT '',
  `is_converted` smallint(6) DEFAULT '0',
  PRIMARY KEY (`campaign_id`,`zoho_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

