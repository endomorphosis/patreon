--
-- PostgreSQL database dump
--

SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;

--
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner:
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner:
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


SET search_path = public, pg_catalog;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: cron_log; Type: TABLE; Schema: public; Owner: patreon; Tablespace:
--

DROP TABLE cron_log;
CREATE TABLE cron_log (
    owner character varying(80),
    function_module character varying(255),
    function_name character varying(255),
    started_at timestamp without time zone,
    finished_at timestamp without time zone,
    status integer
);


ALTER TABLE cron_log OWNER TO patreon;

--
-- Name: darkmode_response; Type: TABLE; Schema: public; Owner: patreon; Tablespace:
--

DROP TABLE darkmode_response;
CREATE TABLE darkmode_response (
    response_id bigint NOT NULL,
    prefix character varying(80) NOT NULL,
    http_code integer NOT NULL,
    response_time bigint NOT NULL,
    url character varying(255) NOT NULL,
    response_key character varying(255) NOT NULL,
    "timestamp" bigint NOT NULL
);


ALTER TABLE darkmode_response OWNER TO patreon;

--
-- Name: darkmode_response_response_id_seq; Type: SEQUENCE; Schema: public; Owner: patreon
--

CREATE SEQUENCE darkmode_response_response_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE darkmode_response_response_id_seq OWNER TO patreon;

--
-- Name: darkmode_response_response_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: patreon
--

ALTER SEQUENCE darkmode_response_response_id_seq OWNED BY darkmode_response.response_id;


--
-- Name: loglines; Type: TABLE; Schema: public; Owner: patreon; Tablespace:
--

DROP TABLE loglines;
CREATE TABLE loglines (
    action character varying(120),
    data json,
    created_at time without time zone
);


ALTER TABLE loglines OWNER TO patreon;

--
-- Name: payment_events; Type: TABLE; Schema: public; Owner: patreon; Tablespace:
--
DROP TABLE payment_events;
CREATE TABLE payment_events (
    payment_event_id integer NOT NULL,
    type character varying(255),
    subtype character varying(255),
    json_data json,
    created_at timestamp without time zone
);


ALTER TABLE payment_events OWNER TO patreon;

--
-- Name: payment_events_payment_event_id_seq; Type: SEQUENCE; Schema: public; Owner: patreon
--

CREATE SEQUENCE payment_events_payment_event_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE payment_events_payment_event_id_seq OWNER TO patreon;

--
-- Name: payment_events_payment_event_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: patreon
--

ALTER SEQUENCE payment_events_payment_event_id_seq OWNED BY payment_events.payment_event_id;


--
-- Name: response_log; Type: TABLE; Schema: public; Owner: patreon; Tablespace:
--

DROP TABLE response_log;
CREATE TABLE response_log (
    request_ip character varying(80),
    user_id bigint,
    request_method character varying(80),
    url character varying(255),
    request_data text,
    request_headers json,
    response_code integer,
    response_key character varying(255),
    host character varying(255),
    session_id character varying(255),
    created_at timestamp without time zone
);


ALTER TABLE response_log OWNER TO patreon;

--
-- Name: response_id; Type: DEFAULT; Schema: public; Owner: patreon
--

ALTER TABLE ONLY darkmode_response ALTER COLUMN response_id SET DEFAULT nextval('darkmode_response_response_id_seq'::regclass);


--
-- Name: payment_event_id; Type: DEFAULT; Schema: public; Owner: patreon
--

ALTER TABLE ONLY payment_events ALTER COLUMN payment_event_id SET DEFAULT nextval('payment_events_payment_event_id_seq'::regclass);


--
-- Name: darkmode_response_pkey; Type: CONSTRAINT; Schema: public; Owner: patreon; Tablespace:
--

ALTER TABLE ONLY darkmode_response
    ADD CONSTRAINT darkmode_response_pkey PRIMARY KEY (response_id);


--
-- Name: primary key; Type: CONSTRAINT; Schema: public; Owner: patreon; Tablespace:
--

ALTER TABLE ONLY payment_events
    ADD CONSTRAINT "primary key" PRIMARY KEY (payment_event_id);


--
-- Name: cron_log_function_module_function_name_started_at_idx; Type: INDEX; Schema: public; Owner: patreon; Tablespace:
--

CREATE INDEX cron_log_function_module_function_name_started_at_idx ON cron_log USING btree (function_module, function_name, started_at);


--
-- Name: cron_log_started_at_idx; Type: INDEX; Schema: public; Owner: patreon; Tablespace:
--

CREATE INDEX cron_log_started_at_idx ON cron_log USING btree (started_at);


--
-- Name: darkmode_response_prefix_http_code_idx; Type: INDEX; Schema: public; Owner: patreon; Tablespace:
--

CREATE INDEX darkmode_response_prefix_http_code_idx ON darkmode_response USING btree (prefix, http_code);


--
-- Name: darkmode_response_prefix_url_idx; Type: INDEX; Schema: public; Owner: patreon; Tablespace:
--

CREATE INDEX darkmode_response_prefix_url_idx ON darkmode_response USING btree (prefix, url);


--
-- Name: date_index; Type: INDEX; Schema: public; Owner: patreon; Tablespace:
--

CREATE INDEX date_index ON payment_events USING btree (created_at DESC);


--
-- Name: loglines_action_created_at_idx; Type: INDEX; Schema: public; Owner: patreon; Tablespace:
--

CREATE INDEX loglines_action_created_at_idx ON loglines USING btree (action, created_at);


--
-- Name: loglines_created_at_action_idx; Type: INDEX; Schema: public; Owner: patreon; Tablespace:
--

CREATE INDEX loglines_created_at_action_idx ON loglines USING btree (created_at, action);


--
-- Name: response_log_host_created_at_idx; Type: INDEX; Schema: public; Owner: patreon; Tablespace:
--

CREATE INDEX response_log_host_created_at_idx ON response_log USING btree (host, created_at);


--
-- Name: response_log_request_ip_created_at_idx; Type: INDEX; Schema: public; Owner: patreon; Tablespace:
--

CREATE INDEX response_log_request_ip_created_at_idx ON response_log USING btree (request_ip, created_at);


--
-- Name: response_log_session_id_created_at_idx; Type: INDEX; Schema: public; Owner: patreon; Tablespace:
--

CREATE INDEX response_log_session_id_created_at_idx ON response_log USING btree (session_id, created_at);


--
-- Name: response_log_url_created_at_idx; Type: INDEX; Schema: public; Owner: patreon; Tablespace:
--

CREATE INDEX response_log_url_created_at_idx ON response_log USING btree (url, created_at);


--
-- Name: response_log_user_id_created_at_idx; Type: INDEX; Schema: public; Owner: patreon; Tablespace:
--

CREATE INDEX response_log_user_id_created_at_idx ON response_log USING btree (user_id, created_at);


--
-- Name: subtype_index; Type: INDEX; Schema: public; Owner: patreon; Tablespace:
--

CREATE INDEX subtype_index ON payment_events USING btree (subtype);


--
-- Name: type_index; Type: INDEX; Schema: public; Owner: patreon; Tablespace:
--

CREATE INDEX type_index ON payment_events USING btree (type);


--
-- Name: type_subtype_index; Type: INDEX; Schema: public; Owner: patreon; Tablespace:
--

CREATE INDEX type_subtype_index ON payment_events USING btree (type, subtype);


--
-- Name: public; Type: ACL; Schema: -; Owner: root
--

REVOKE ALL ON SCHEMA public FROM PUBLIC;
REVOKE ALL ON SCHEMA public FROM root;
GRANT ALL ON SCHEMA public TO root;
GRANT ALL ON SCHEMA public TO PUBLIC;


--
-- PostgreSQL database dump complete
--
