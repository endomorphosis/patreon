CREATE USER 'patreon'@'localhost' IDENTIFIED BY 'insecurepass';
CREATE USER 'patreon'@'*' IDENTIFIED BY 'insecurepass';
GRANT ALL PRIVILEGES ON *.* TO 'patreon'@'localhost';
GRANT ALL PRIVILEGES ON *.* TO 'patreon'@'*';
CREATE DATABASE patreon_test;