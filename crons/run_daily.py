# These crons run once a day, at 12:01AM.
# Import your function and add it to the run function.

# TODO: Make each function run in a separate process and stuff.

import patreon
from patreon.model.manager import cron_mgr
from patreon.stats.get_average_stats import get_average_stats, store_dashboard_data
from patreon.stats.mobile_beta_data import generate_and_upload_all_creator_data


def run_daily():
    cron_mgr.run_cron(get_average_stats, owner='seanw')
    cron_mgr.run_cron(generate_and_upload_all_creator_data, owner='albert')
    cron_mgr.run_cron(store_dashboard_data, owner='seanw')

if __name__ == '__main__':
    # TODO: Move this somewhere higher up the stream, but before app initialize.
    patreon.services.logging.rollbar.initialize()
    run_daily()
