# These crons run once every 5 minutes, on any minute that is divisible by 5.
# Import your function and add it to the run function.
from patreon.app.web.view.InternalPaymentCronStatus.internal_payment_cron_status import cron_status_data


def run_five_minutely():
    cron_status_data()


if __name__ == '__main__':
    run_five_minutely()
