# Patreon Python Port

Installation on MacOSX
----------------------

TODO: This is so complicated. Make this a one script install.

Install Python, MySQL, and Nginx

    brew install python3 mysql nginx
    brew install postgres
    brew install libffi
    brew install libxml2
    brew install libxslt
    brew link libffi —force
    brew link libxml2 --force
    brew link libxslt —force

Set up the virtual environment

    pip3 install virtualenv
    virtualenv venv
    source venv/bin/activate

Install all the packages for Python3 into the virtualenv

    pip3 install -r requirements.txt
    pip3 install werkzeug==0.10

Create a config.py file for yourself. This file is the equivalent of
environments.json in PHP. There is an example config.py that you can fill
out with your PHP credentials from config/environments.json, or you can ask
anyone for their version of config.py.

    cp patreon/config.py.example patreon/config.py
    vi patreon/config.py

One thing to add to your config.py is the location of your PHP repo so that
you can pull static resources from that location. For example, adding the line

    php_repo = '/Users/albert/Development/patreon_php'

Would tell the development server to pull all the static resources from my
Development folder's php repository.


To run tests:
------------

Run the MySQL server

    mysql.server start
    pg_ctl -D /usr/local/var/postgres -l /usr/local/var/postgres/server.log start

Create the test database

    cat database/create_test_user.sql | mysql -uroot
    cat database/schema.sql | mysql -uroot patreon_test
    psql postgres < database/postgres_test_user.sql
    psql postgres < database/postgres_schema.sql

Run the tests

    ./test.sh

or
    ./test_single.sh <test>

To run the webserver on MacOSX:
-------------------------------

Run the Python webserver

    python3 runserver.py

You should see the webserver


Installation on your devserver:
---------------------------------------

This installation will be more straightforward. Starting from your Github clone:

Set up the virtual environment:

    sudo apt-get install python3-pip libpq-dev
    pip3 install virtualenv
    virtualenv venv/
    source venv/bin/activate

Install all the packages:

    pip3 install -r requirements.txt
    
If the line above fails, you may need to do one or both of the following before that line will finish successfully:

    apt-get install libxml2-dev libxslt1-dev python-dev
    apt-get install libffi-dev

Copy a config.py from somewhere. A simple way is to just copy someone else's:

    scp albert:/home/ansible/patreon_py/patreon/config.py patreon/config.py

If you do this, make sure to edit it to remove all the references to 'Albert'
from your config.py file.

    vi patreon/config.py

Then, run the Python webserver

    python3 runserver.py

You should be able to see your Python installation running at:

    https://<your name>.anaconda.patreon.com  
