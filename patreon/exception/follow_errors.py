from patreon.constants.http_codes import RESOURCE_CONFLICT_409, NOT_FOUND_404
from patreon.exception import ParameterMissing, ActionForbidden, APIException


class FollowedRelationshipMissing(ParameterMissing):
    parameter_name = 'followed'


class FollowCreationForbidden(ActionForbidden):
    action = 'create'

    def __init__(self):
        super().__init__(resource_name='follow')


class FollowAlreadyExists(ActionForbidden):
    status_code = RESOURCE_CONFLICT_409
    action = 'create'

    def __init__(self):
        super().__init__(resource_name='follow')
        self.error_description = 'That follow already exists'


class FollowCreationFailed(APIException):
    error_description = 'Unable to create follow for unknown reason'


class FollowDeletionForbidden(ActionForbidden):
    action = 'delete'

    def __init__(self):
        super().__init__(resource_name='follow')


class FollowDoesNotYetExist(ActionForbidden):
    status_code = NOT_FOUND_404
    action = 'delete'

    def __init__(self):
        super().__init__(resource_name='follow')
        self.error_description = 'That follow does not yet exist'
