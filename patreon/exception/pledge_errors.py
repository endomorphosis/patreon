from patreon.constants.error_level import ErrorLevel
from patreon.constants.http_codes import PAYMENT_REQUIRED_402, NOT_AUTHORIZED_403, INVALID_REQUEST_400
from patreon.exception import APIException


class PledgeError(APIException):
    pass


class RewardNotAvailable(PledgeError):
    status_code = INVALID_REQUEST_400
    error_type = "reward_not_available"
    error_title = "The selected reward is no longer available."
    error_level = ErrorLevel.INFO


class InsufficientPledgeForReward(PledgeError):
    status_code = INVALID_REQUEST_400
    error_type = "insufficient_pledge_for_reward"
    error_title = "The pledge amount was not high enough for the selected reward."
    error_level = ErrorLevel.INFO


class InvalidPledgeCap(PledgeError):
    status_code = INVALID_REQUEST_400
    error_type = "invalid_pledge_cap"
    error_title = "The monthly max must be a multiple of your pledge."
    error_level = ErrorLevel.INFO


class InvalidCard(PledgeError):
    status_code = INVALID_REQUEST_400
    error_type = "invalid_card"
    error_title = "The selected payment method is not available."
    error_level = ErrorLevel.INFO

class VatCountryMismatch(PledgeError):
    status_code = INVALID_REQUEST_400
    error_type = "vat_country_mismatch"
    error_title = "Your detected country has changed"
    error_level = ErrorLevel.INFO


class PledgeNotExist(PledgeError):
    status_code = INVALID_REQUEST_400
    error_type = "pledge_not_exist"
    error_title = "You are not pledging to this campaign"
    error_leel = ErrorLevel.INFO


class NSFWPaypalPledge(PledgeError):
    status_code = INVALID_REQUEST_400
    error_type = "paypal_nsfw"
    error_title = "You may not pledge to a NSFW campaign with paypal"


class ShippingAddressRequired(PledgeError):
    status_code = INVALID_REQUEST_400
    error_type = "shipping_address_required"
    error_title = "The selected reward requires you to supply a shipping address."


class BigMoneyRequired(PledgeError):
    status_code = PAYMENT_REQUIRED_402
    error_type = "big_money_escrow_required"
    error_title = "For larger pledges we process the funds immediately."
    error_leel = ErrorLevel.INFO


class BigMoneyFailed(PledgeError):
    status_code = INVALID_REQUEST_400
    error_type = "big_money_escrow_charge_failed"
    error_title = "Sorry, there was an issue with charging your payment method. Please check your info and try again!"
    error_leel = ErrorLevel.INFO
