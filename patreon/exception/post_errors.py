from patreon.constants.http_codes import NOT_FOUND_404
from patreon.exception import APIException, ResourceMissing, ViewForbidden, \
    EditForbidden, ActionForbidden, ParameterInvalid


class EmbedMissing(APIException):
    status_code = NOT_FOUND_404
    error_title = "Invalid embed URL."


class BadEmbed(APIException):
    status_code = NOT_FOUND_404
    error_title = "Invalid embed URL."

    def __init__(self, url):
        error_description = "Invalid embed URL: {}.".format(url)
        super().__init__(error_description=error_description)


class InvalidImageFormat(APIException):
    error_title = "Could not process file with non-image format."

    def __init__(self, invalid_format):
        error_description = "Could not process file '{}' with non-image format."\
            .format(invalid_format)
        super().__init__(error_description=error_description)


class PostNotFound(ResourceMissing):
    def __init__(self, post_id):
        super().__init__('Post', post_id)
        self.resource_id = post_id


class PostViewForbidden(ViewForbidden):
    def __init__(self, post_id):
        super().__init__('post', post_id)


class PostEditForbidden(EditForbidden):
    def __init__(self, post_id):
        super().__init__('post', post_id)


class CannotPost(ActionForbidden):
    action = 'post to'

    def __init__(self, campaign_id):
        super().__init__('campaign', campaign_id)


class CannotPostAgain(ActionForbidden):
    action = 're-publish'

    def __init__(self, post_id):
        super().__init__('post', post_id)


class BadPostType(ParameterInvalid):
    def __init__(self, parameter_value):
        super().__init__('post_type', parameter_value)
