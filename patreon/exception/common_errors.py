from patreon.constants.http_codes import AUTHENTICATION_REQUIRED_401, INVALID_REQUEST_400
from patreon.exception import ParameterMissing, APIException


class FileMissing(APIException):
    status_code = INVALID_REQUEST_400
    error_title = "File missing."


class DataMissing(ParameterMissing):
    parameter_name = 'data'


class APIKeyMissing(APIException):
    status_code = AUTHENTICATION_REQUIRED_401
    error_title = "An API key required."


class LoginRequired(APIException):
    status_code = AUTHENTICATION_REQUIRED_401
    error_title = "This route is restricted to logged in users."
