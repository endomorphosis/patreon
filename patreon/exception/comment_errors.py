from patreon.exception import ResourceMissing, ParameterInvalid, \
    DeleteForbidden, EditForbidden, ViewForbidden


class CommentNotFound(ResourceMissing):
    def __init__(self, comment_id):
        super().__init__('Comment', comment_id)
        self.resource_id = comment_id


class InvalidParent(ParameterInvalid):
    def __init__(self, comment_id):
        super().__init__('thread_id', comment_id)


class InvalidComment(ParameterInvalid):
    def __init__(self, comment_text):
        super().__init__('comment_text', comment_text)


class CommentViewForbidden(ViewForbidden):
    def __init__(self, comment_id):
        super().__init__('comment', comment_id)


class CommentEditForbidden(EditForbidden):
    def __init__(self, comment_id):
        super().__init__('comment', comment_id)


class CommentDeleteForbidden(DeleteForbidden):
    def __init__(self, comment_id):
        super().__init__('comment', comment_id)
