from patreon.exception import ParameterMissing, ResourceMissing


class CampaignIDMissing(ParameterMissing):
    parameter_name = 'campaign_id'


class HasNoCampaign(ResourceMissing):
    def __init__(self):
        self.error_title = "Current user does not have a campaign."


class CampaignNotFound(ResourceMissing):
    def __init__(self, campaign_id):
        super().__init__('Campaign', campaign_id)

