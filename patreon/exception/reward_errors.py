from patreon.exception import ResourceMissing, EditForbidden


class RewardNotFound(ResourceMissing):
    def __init__(self, reward_id):
        super().__init__('Reward', reward_id)


class RewardEditForbidden(EditForbidden):
    def __init__(self, reward_id):
        super().__init__('Reward', reward_id)
