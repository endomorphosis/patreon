"""
All defined routes for API endpoints.
TODO(postport): Make this more modular and integrate with SQLAlchemy
This is just to support the single endpoint for Ryan Leslie / testing
for mobile / Angular.
"""
import json
import time
import socket

import functools
import newrelic.agent
from flask import Flask, request, session, Response, has_request_context
from werkzeug.exceptions import RequestEntityTooLarge
import patreon
from patreon.app.api.resources import ApiResource
from patreon.constants.error_level import ErrorLevel
from patreon.exception import APIException
from patreon.exception.attachment_errors import FileTooLarge
from patreon.exception.common_errors import DataMissing, LoginRequired, FileMissing
from patreon.model.manager import csrf, feature_flag
from patreon.model.request.csrf import handle_csrf_check
from patreon.services.logging import rollbar
from patreon.services.logging.unsorted import log_json, log_response

api_app = Flask(__name__)
# Use custom PHP-style sessions.
api_app.session_interface = patreon.model.request.session.PatreonSessionInterface()
api_app.request_class = patreon.model.request.PatreonRequest
# Initialize Rollbar error reporting if enabled.
patreon.services.logging.rollbar.initialize(api_app)

# Initialize New Relic after Rollbar
if socket.gethostname() in patreon.config.newrelic_servers:
    api_app = newrelic.agent.WSGIApplicationWrapper(api_app)

# Increase the max file upload size globally
# todo(postpost): set maximum per request type.
api_app.config['MAX_CONTENT_LENGTH'] = patreon.config.max_attachment_bytes
patreon.config.allow_authorization_header = True


@api_app.before_request
def set_csrf_token():
    if session.user_id != 0 and session.csrf_token is None:
        session.csrf_token = csrf.get_user_secret(session.user_id)


@api_app.before_request
def csrf_protect():
    csrf_uri = request.headers.get('X-CSRF-URI')
    csrf_signature = request.headers.get('X-CSRF-Signature')
    csrf_time = request.headers.get('X-CSRF-Time')

    if request.method != "GET" and session.using_cookie_authentication:
        if csrf.is_api_csrf_protection_disabled(request.path):
            csrf.log_csrf_event('csrf_protection_disabled', csrf_time, csrf_uri, csrf_signature)
            return

        try:
            handle_csrf_check(csrf_uri, csrf_signature, csrf_time)
        except:
            # Don't process CSRF failures yet for the api - just log
            pass

    elif not session.using_cookie_authentication:
        csrf.log_csrf_event('csrf_skipped', csrf_time, csrf_uri, csrf_signature)


@api_app.before_request
def assign_ab_test_id():
    cookie_group_id = request.ab_test_group_id
    user_group_id = feature_flag.get_or_set_group_id_for_user(session.user_id, cookie_group_id)
    if cookie_group_id != user_group_id:
        feature_flag.set_group_id_cookie(user_group_id)
        temp_cookies = dict(request.cookies)
        temp_cookies['group_id'] = user_group_id
        request.cookies = temp_cookies


@api_app.teardown_request
def kill_sqlalchemy_session(exception=None):
    from patreon.services.db import db_session, log_db_session

    db_session.commit()
    db_session.close()

    if log_db_session:
        log_db_session.commit()
        log_db_session.close()


@api_app.after_request
def log_response_object(response):
    log_response(session, request, response)
    return response


def make_error_response(api_exception, time_taken=0):
    error_id = str(id(api_exception))
    error_type = api_exception.error_type
    error_code = api_exception.error_code
    error_title = api_exception.error_title
    error_description = api_exception.error_description or error_title
    status_code = str(api_exception.status_code)

    response = Response(
        json.dumps(
            {'errors': [{
                'id': error_id,  # unique key for this occurrence
                'code_name': error_type,  # patreon-specific error name
                'code': error_code,  # patreon-specific error code
                'title': error_title,  # human readable description
                'detail': error_description,  # explanation for this occurrence
                'status': status_code,  # http status code
                'time_taken': time_taken  # request duration in milliseconds
            }]},
            sort_keys=True,
            indent=4,
            separators=(',', ': ')
        ),
        status=status_code,
        mimetype="application/vnd.api+json"
    )

    return add_response_headers(response)


def get_data():
    if request.json and request.json.get('data'):
        data = request.json.get('data')
    elif request.form and request.form.get('data'):
        data_str = request.form.get('data')
        data = json.loads(data_str)
    else:
        raise DataMissing()
    # TODO: have get_data clients expect attributes to be nested
    if get_json_api_version() == ApiResource.JSON_API_STABLE:
        data.update(data.get('attributes', {}))
    return data


def get_sort():
    return request.args.get('sort')


def get_includes():
    """Returns a list of the requested resources to include in the response for this request."""
    includes_string = request.args.get('include')
    if includes_string:
        return includes_string.split(',')
    else:
        return None


def get_included():
    """Returns a list of the included resources sent from the client"""
    if not request.json:
        raise DataMissing()
    return request.json.get('included', [])


def get_included_with_filter(type_, id_=None):
    included = get_included()
    return [
        resource
        for resource in included
        if resource.get('type') == type_
        and (id_ is None or id_ == resource.get('id'))
        ]


def get_filters():
    filters = {}
    for key in request.args:
        if key.startswith('filter['):
            filter_key = key[(len('filter[')):-1]
            filters[filter_key] = request.args.get(key)
    return filters


def get_json_api_version(default_version=ApiResource.JSON_API_DEFAULT_VERSION):
    if request:
        return request.args.get('json-api-version', default_version)
    else:
        return default_version


def get_bool_from_json_value(value):
    return bool(str(value).lower() != 'false')


def get_files():
    try:
        if not request.files:
            return []
        return request.files

    except RequestEntityTooLarge:
        raise FileTooLarge()


def get_file():
    try:
        if not request.files or not request.files.get('file'):
            raise FileMissing()

        return request.files.get('file')

    except RequestEntityTooLarge:
        raise FileTooLarge()


def fail(api_exception):
    raise APIException(*api_exception)


def abort(status_code=403, type_='generic-error', title='A runtime check failed.'):
    raise APIException(status_code, type_, title)


@api_app.after_request
def per_request_callbacks(response):
    return patreon.model.request.call_deferred_functions(response)


@api_app.before_request
def update_pstore():
    patreon.pstore.update()


@api_app.after_request
def add_response_headers(response):
    if has_request_context() \
            and request.headers.get('Origin') \
            and (
                        not patreon.config.stage == 'production'
                    or request.headers.get('Origin').endswith('patreon.com')
            ):
        origin = request.headers.get('Origin')
    elif patreon.config.stage == 'production':
        origin = 'patreon.com'
    else:
        origin = '*'

    response.headers['Access-Control-Allow-Origin'] = origin
    response.headers['Access-Control-Allow-Credentials'] = 'true'
    response.headers['Access-Control-Allow-Headers'] = \
        'Content-Type, X-File-Name, X-Requested-With,' \
        ' X-CSRF-URI, X-CSRF-Signature, X-CSRF-Time'
    response.headers['Cache-control'] = 'private'

    # Cross-site HTTP requests.
    if has_request_context() and not response.headers.get('Access-Control-Allow-Methods'):
        response.headers['Access-Control-Allow-Methods'] = response.headers.get('Allow')

    return response


def return_as_json(route):
    """Indicates that this route should return mimetype JSON objects."""

    @functools.wraps(route)
    def json_route(*args, **kwargs):
        start_time = time.time()
        try:
            response_object = route(*args, **kwargs)
            timetaken_ms = (time.time() - start_time) * 1000
            if type(response_object) == tuple:
                response_object, options = response_object
            else:
                options = {}

            status_code = 200
            location_header = None
            if options.get("created"):
                status_code = 201
                location_header = options.get("created")
            if options.get("deleted"):
                status_code = 204
                location_header = options.get("deleted")

            # Cast object returns into JSON objects.
            response_json = json.dumps(
                response_object,
                sort_keys=True,
                indent=4,
                separators=(',', ': ')
            )
            try:
                response_type = response_object['data']['type']
            except (KeyError, TypeError):
                response_type = None
            response = Response(response_json, status=status_code, mimetype="application/vnd.api+json")
            if location_header:
                response.headers['Location'] = location_header

            return response

        except APIException as ae:
            time_taken_ms = (time.time() - start_time) * 1000
            log_json({
                'verb': 'apiabort',
                'timetaken': int(time_taken_ms),
                'error': {
                    'code': ae.status_code,
                    'type': ae.error_type,
                    'title': ae.error_title,
                }
            })
            if ae.error_level in [ErrorLevel.ERROR, ErrorLevel.FATAL]:
                extra_data = {
                    'error_description': ae.error_description,
                    'error_title': ae.error_title
                }
                rollbar.log(message=ae.error_type, extra_data=extra_data, request=request)
            return make_error_response(ae, int(time_taken_ms))

    return json_route


def require_login(route):
    @functools.wraps(route)
    def logged_in_route(*args, **kwargs):
        if session.user_id == 0:
            return make_error_response(LoginRequired())

        kwargs['current_user_id'] = session.user_id
        return route(*args, **kwargs)

    return logged_in_route


# Import will also initialize routes.
from . import routes
