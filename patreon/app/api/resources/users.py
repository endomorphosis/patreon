from flask import session
from patreon.app.api.resources import ApiResourceCollection, ApiResourceNull
from patreon.app.api.resources.blueprints.blueprint_api_resource import BlueprintAPIResource
from patreon.app.api.resources.pledges import PledgeResource
from patreon.app.api.resources.cards import CardResource
from patreon.app.api.resources.follows import FollowResource
from patreon.app.api.resources.campaigns import CampaignResource
from patreon.app.api.resources.presence import PresenceResource
from patreon.app.api.view import user_view
from patreon.model.manager import campaign_mgr, card_mgr, follow_mgr, presence_mgr, pledge_mgr


class UserResource(BlueprintAPIResource):
    @staticmethod
    def resource_type():
        return "user"

    @staticmethod
    def default_includes():
        return ['campaign']

    @classmethod
    def blueprint(cls):
        return {
            # "type": "user",
            "id": "user_id",
            # TODO: port user_view to blueprint
            "relationships": {
                # "pledges": {
                #     "type": "array:pledge",
                #     "id": "Pledge.find_by_patron_id(self.model.user_id)",
                # },
                # "cards": {
                #     "type": "array:card",
                #     "id": "Card.find_by_user_id(self.model.user_id)",
                # },
                # "follows": {
                #     "type": "array:follow",
                #     "id": "Follow.find_by_follower_id(self.model.user_id)",
                # },
                # "campaign": {
                #     "type": "campaign",
                #     "id": "campaign_mgr.try_get_campaign_id_by_user_id_hack(self.model.user_id)",
                # },
            }
        }

    def pledges(self):
        includes = PledgeResource.default_includes()
        if 'pledges.unread_count' in self.includes:
            includes.append('unread_count')
        return ApiResourceCollection([
            PledgeResource(pledge, includes=includes)
            for pledge in pledge_mgr.find_by_patron_id(self.model.user_id)
        ])

    def cards(self):
        return ApiResourceCollection([
            CardResource(card)
            for card in card_mgr.find_by_user_id(self.model.user_id)
        ])

    def follows(self):
        includes = FollowResource.default_includes()
        if 'follows.unread_count' in self.includes:
            includes.append('unread_count')
        return ApiResourceCollection([
            FollowResource(follow, includes=includes)
            for follow in follow_mgr.find_by_follower_id(self.model.user_id)
        ])

    def campaign(self):
        if self.model and self.model.user_id:
            campaign = campaign_mgr.try_get_campaign_by_user_id_hack(self.model.user_id)
            if campaign:
                return CampaignResource(campaign)
        return ApiResourceNull()

    def presence(self):
        return ApiResourceCollection([
            PresenceResource(presence)
            for presence in presence_mgr.find_by_user_id(self.model.user_id)
        ])

    def prime_for_includes(self):
        super().prime_for_includes()
        if self.should_include_linked_resource('pledges'):
            pledge_mgr.find_by_patron_id.prime(self.model.user_id)
        if self.should_include_linked_resource('cards'):
            card_mgr.find_by_user_id.prime(self.model.user_id)
        if self.should_include_linked_resource('follows'):
            follow_mgr.find_by_follower_id.prime(self.model.user_id)
        if self.should_include_linked_resource('campaign'):
            campaign_mgr.get_campaigns_by_user_id.prime(self.model.user_id)
        if self.should_include_linked_resource('presence'):
            presence_mgr.find_by_user_id.prime(self.model.user_id)

    def linked_resources_blacklist(self):
        current_user_id = session.user_id if session else None
        if self.model.user_id == current_user_id:
            return []
        elif self.model.is_private:
            return ['pledges', 'cards', 'follows']
        else:
            return ['cards']

    def should_include_linked_resource(self, key):
        return key in self.includes and key not in self.linked_resources_blacklist()

    def linked_resources(self):
        links = {}
        if self.should_include_linked_resource('pledges'):
            links['pledges'] = self.pledges()
        if self.should_include_linked_resource('cards'):
            links['cards'] = self.cards()
        if self.should_include_linked_resource('follows'):
            links['follows'] = self.follows()
        if self.should_include_linked_resource('campaign'):
            links['campaign'] = self.campaign()
        if self.should_include_linked_resource('presence'):
            links['presence'] = self.presence()
        return links

    def mask_for_json(self):
        mask = user_view.public_profile_whitelist
        current_user_id = session.user_id if session else None
        if self.model.user_id == current_user_id:
            mask = user_view.self_whitelist
        elif self.model.is_private:
            mask = user_view.private_profile_whitelist
        # TODO: patronage whitelist if user is a patron of current user
        return mask

    def as_json(self):
        return user_view.mask(self.model, self.mask_for_json())


class UsersCollection(ApiResourceCollection):
    def __init__(self, users, includes=[]):
        self._members = [UserResource(user, includes) for user in users]
