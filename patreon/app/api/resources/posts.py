from flask import session
from patreon.app.api.resources import ApiResourceCollection, ApiResourceNull
from patreon.app.api.resources.blueprints.blueprint_api_resource import BlueprintAPIResource
from patreon.app.api.resources.actions import PostLikesCollection
from patreon.app.api.view import post_view
from patreon.model.manager import attachment_mgr, action_mgr, campaign_mgr, presence_mgr, post_mgr, push_mgr
from patreon.model.table import Actions, Presence
from patreon.model.type import ActionType


class PostResource(BlueprintAPIResource):
    def __init__(self, post, mask=None, includes=None):
        if includes is None:
            includes = self.default_includes()
        if mask is None:
            current_user_id = None
            if session and session.user_id:
                current_user_id = session.user_id
            if post_mgr.can_view(current_user_id, post):
                mask = post_view.public_whitelist
            else:
                mask = post_view.private_whitelist
        includes = list(set(mask[1]).intersection(set(includes)))
        super().__init__(post, includes)
        self.json_mask = mask[0]

    @staticmethod
    def resource_type():
        return "post"

    @staticmethod
    def default_includes():
        return ['attachments', 'user', 'campaign', 'likes']

    @classmethod
    def blueprint(cls):
        return {
            # "type": "post",
            "id": "activity_id",
            # TODO: port post_view to blueprint
            "relationships": {
                "user": {
                    "type": "user",
                    "id": "user_id",
                },
                "campaign": {
                    "type": "campaign",
                    "id": "campaign_id",
                },
                # "attachments": {
                # "type": "array:attachment",
                #     "accessor": "attachment_mgr.get_attachments_by_activity_id(activity_id)",
                # },
                # "likes": {
                # "type": "array:like",
                #     "accessor": "action_mgr.find_likes_by_activity_id_paginated_by_cursor...",
                # },
            }
        }

    def current_user_has_liked(self):
        if not session or not session.user_id:
            return None
        return action_mgr.get_like(session.user_id, self.model.activity_id) is not None

    def count_of_pushable_users(self):
        if not session or not session.user_id \
                or session.user_id != self.model.user_id:
            return None
        return push_mgr.get_count_of_users_pushed_for_presence_on_post(self.model)

    def prime_for_includes(self):
        super().prime_for_includes()
        campaign_mgr.get_user_ids_by_campaign_id.prime(self.model.campaign_id)
        if 'attachments' in self.includes:
            attachment_mgr.get_attachments_by_activity_id.prime(self.model.activity_id)
        if session and session.user_id:
            Actions.get.prime(session.user_id, self.model.activity_id, ActionType.LIKE.value)
        if 'presence' in self.includes:
            presence_mgr.find_by_room.prime(Presence.room_name_for_activity_id(self.model.activity_id))

    def attachments(self):
        from patreon.app.api.resources.attachments import AttachmentsCollection

        return AttachmentsCollection(attachment_mgr.get_attachments_by_activity_id(self.model.activity_id))

    def presence(self):
        from patreon.app.api.resources.presence import PresenceResource

        presence = presence_mgr.find_by_activity_id(self.model.activity_id)
        if presence:
            return PresenceResource(presence)
        return ApiResourceNull()

    def likes(self):
        return PostLikesCollection(self.model)

    def linked_resources(self):
        links = super().linked_resources()
        if 'attachments' in self.includes:
            links['attachments'] = self.attachments()
        if 'presence' in self.includes:
            links['presence'] = self.presence()
        if 'likes' in self.includes:
            links['likes'] = self.likes()
        return links

    def as_json(self):
        json = post_view.mask(self.model, self.json_mask)
        liked = self.current_user_has_liked()
        if liked is not None:
            json['current_user_has_liked'] = bool(liked)
        pushable_count = self.count_of_pushable_users()
        if pushable_count is not None:
            json['num_pushable_users'] = pushable_count
        return json


class PostsCollection(ApiResourceCollection):
    def __init__(self, posts, masks, extra_includes=None):
        includes = PostResource.default_includes()
        if extra_includes:
            includes += extra_includes
        resources = []
        for i in range(0, len(posts)):
            resources.append(PostResource(posts[i], mask=masks[i], includes=includes))
        super().__init__(resources)
