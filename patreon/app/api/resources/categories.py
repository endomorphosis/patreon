from patreon.app.api.resources.blueprints.blueprint_api_resource import BlueprintAPIResource
from patreon.app.api.resources import ApiResourceCollection
from patreon.app.api.view import post_view
from patreon.model.manager import curation_mgr, category_mgr
from patreon.model.type import Category


class CategoryResource(BlueprintAPIResource):
    @staticmethod
    def resource_type():
        return 'category'

    def featured(self):
        from patreon.app.api.resources.posts import PostsCollection
        FRONT_PAGE = Category.FRONT_PAGE.category_id
        if self.model.category_id == FRONT_PAGE:
            featured = curation_mgr.find_posts_by_single_category(FRONT_PAGE)
        else:
            featured = curation_mgr.find_posts_by_category(self.model.category_id)
        # all featured posts are not patron-only
        return PostsCollection(featured, masks=[post_view.public_whitelist for _ in featured])

    def prime_for_includes(self):
        super().prime_for_includes()
        if 'featured' in self.includes and self.model.category_id != 0:
            curation_mgr.find_posts_by_category.prime(self.model.category_id)

    def linked_resources(self):
        links = super().linked_resources()
        if 'featured' in self.includes:
            links["featured"] = self.featured()
        return links

    @classmethod
    def blueprint(cls):
        return {
            # 'type': 'category',
            'id': 'category_id',
            'name': 'name',
            # 'relationships': {
            #     'featured': {
            #         'type': 'array:post',
            #         # ??
            #     },
            # }
        }


class UserCategoriesCollection(ApiResourceCollection):
    def __init__(self, includes=None):
        categories = category_mgr.user_categories()
        super().__init__([
            CategoryResource(category, includes=includes)
            for category in categories
        ])


class AllCategoriesCollection(ApiResourceCollection):
    def __init__(self, includes=None):
        categories = category_mgr.all_categories()
        super().__init__([
            CategoryResource(category, includes=includes)
            for category in categories
        ])
