from flask import session
from patreon.app.api.resources import ApiResourceNull
from patreon.app.api.resources.pledge_vat_locations import PledgeVatLocationResource
from patreon.app.api.resources.blueprints.blueprint_api_resource import BlueprintAPIResource
from patreon.model.manager import reward_mgr, campaign_mgr, card_mgr, unread_status_mgr, new_pledge_mgr
from patreon.model.table import User, Reward


class PledgeResource(BlueprintAPIResource):
    @staticmethod
    def resource_type():
        return "pledge"

    @staticmethod
    def route_prefix():
        return "pledges"

    @staticmethod
    def default_includes():
        return ['patron', 'creator', 'reward', 'pledge_vat_location']

    @classmethod
    def blueprint(cls):
        return {
            # "type": "pledge",
            "id": "pledge_id",
            'amount_cents': 'amount',
            'created_at': 'date:created_at',
            'pledge_cap_cents': 'pledge_cap',
            'patron_pays_fees': 'PatronPaysFees',
            "relationships": {
                "patron": {
                    "type": "user",
                    "id": "patron_id",
                },
                "reward": {
                    "type": "reward",
                    "id": "reward_id",
                },
                # "creator": {
                #     "type": "user",
                #     "id": "creator_id",
                # },
            }
        }

    def creator(self):
        from patreon.app.api.resources.users import UserResource

        return UserResource(User.get(self.model.creator_id), includes=['campaign'])

    def reward(self):
        from patreon.app.api.resources.rewards import RewardResource

        return RewardResource(reward_mgr.get(self.model.reward_id))

    def card(self):
        from patreon.app.api.resources.cards import CardResource

        if self.model.card_id:
            return CardResource(card_mgr.get(self.model.card_id))
        else:
            return ApiResourceNull()

    def address(self):
        if self.model.has_address:
            from patreon.app.api.resources.addresses import AddressResource

            return AddressResource(self.model.address, "pledge-address-" + self.model.pledge_id)
        else:
            return ApiResourceNull()

    def pledge_vat_location(self):
        campaign_id = campaign_mgr.try_get_campaign_id_by_user_id_hack(user_id=self.model.creator_id)
        new_pledge = new_pledge_mgr.get_pledge_by_patron_and_campaign(self.model.patron_id, campaign_id)
        if new_pledge and new_pledge.pledge_vat_location:
            return PledgeVatLocationResource(new_pledge.pledge_vat_location)
        else:
            return ApiResourceNull()

    def prime_for_includes(self):
        super().prime_for_includes()
        if self.model.creator_id:
            User.get.prime(self.model.creator_id)
        if 'patron' in self.includes:
            User.get.prime(self.model.patron_id)
        if 'reward' in self.includes:
            Reward.get.prime(self.model.reward_id)
        if 'unread_count' in self.includes:
            campaign_mgr.get_campaign_ids_by_user_id.prime(self.model.followed_id)

    def linked_resources(self):
        links = super().linked_resources()
        if self.model.creator_id:
            links['creator'] = self.creator()
        if session and session.user_id:
            # TODO: something smarter here so impersonated users don't see admin data
            if session.user_id == self.model.patron_id or session.user_id == self.model.creator_id or session.is_admin:
                links['address'] = self.address()
            if session.user_id == self.model.patron_id or session.is_admin:
                links['card'] = self.card()
                links['pledge_vat_location'] = self.pledge_vat_location()
        return links

    def unread_count(self):
        campaign_id = campaign_mgr.try_get_campaign_id_by_user_id_hack(self.model.creator_id)
        if campaign_id:
            return unread_status_mgr.get_unread_count_for_campaign_creations(campaign_id, self.model.patron_id)
        return 0

    def as_json(self):
        json = super().as_json()
        if 'unread_count' in self.includes:
            json['unread_count'] = self.unread_count()
        return json
