from patreon.app.api.resources.blueprints.blueprint_api_resource import BlueprintAPIResource


class PledgeVatLocationResource(BlueprintAPIResource):
    @staticmethod
    def resource_type():
        return "pledge_vat_location"

    @classmethod
    def blueprint(cls):
        return {
            # "type": "pledge_vat_location",
            "id": "pledge_vat_location_id",
            'geolocation_country': 'geolocation_country',
            'selected_country': 'selected_country',
            # I'm intentionally omitting the ip_address, out of security paranoia. ~Jesse
            'created_at': 'date:created_at',
        }
