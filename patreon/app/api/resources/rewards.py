from patreon.app.api.resources import ApiResourceCollection
from patreon.app.api.resources.blueprints.blueprint_api_resource import BlueprintAPIResource
from patreon.model.table import User


class RewardResource(BlueprintAPIResource):
    @staticmethod
    def resource_type():
        return 'reward'

    @staticmethod
    def default_includes():
        return ['creator']

    def prime_for_includes(self):
        super().prime_for_includes()
        self.model._count_parameterized.prime(self.model.RID)

    @classmethod
    def blueprint(cls):
        return {
            # 'type': 'reward',
            'id': 'reward_id',
            'amount': 'amount', # deprecated
            'amount_cents': 'amount',
            'user_limit': 'user_limit',
            'remaining': 'remaining()',
            'description': 'description',
            'requires_shipping': 'bool:requires_shipping',
            'created_at': 'date:created_at',
            'url': 'url',
            'relationships': {
                'creator': {
                    'type': 'user',
                    'id': 'creator_id',
                },
            }
        }


class RewardEveryoneTierResource(RewardResource):
    def __init__(self, user_id):
        self.user_id = user_id

    @staticmethod
    def resource_type():
        return 'reward'

    def resource_id(self):
        return str(-1)

    def creator(self):
        from patreon.app.api.resources.users import UserResource
        return UserResource(User.get(self.user_id))

    def linked_resources(self):
        return {'creator': self.creator()}

    def as_json(self):
        reward = {
            'type': 'reward',
            'id': None,
            'amount': None, # deprecated
            'amount_cents': None,
            'user_limit': None,
            'remaining': 0,
            'description': None,
            'requires_shipping': False,
            'created_at': None,
            'url': None,
            'relationships': {
                'creator': {
                    'type': 'user',
                    'id': None,
                },
            }
        }
        reward.update({
            'id': '-1',
            'amount': 0, # deprecated
            'amount_cents': 0,
            'description': 'Everyone',
        })
        return reward


class RewardPatronTierResource(RewardResource):
    def __init__(self, user_id):
        self.user_id = user_id

    @staticmethod
    def resource_type():
        return 'reward'

    def resource_id(self):
        return str(0)

    def creator(self):
        from patreon.app.api.resources.users import UserResource
        return UserResource(User.get(self.user_id))

    def linked_resources(self):
        return { 'creator': self.creator() }

    def as_json(self):
        reward = {
            'type': 'reward',
            'id': None,
            'amount': None,  # deprecated
            'amount_cents': None,
            'user_limit': None,
            'remaining': 0,
            'description': None,
            'requires_shipping': False,
            'created_at': None,
            'url': None,
            'relationships': {
                'creator': {
                    'type': 'user',
                    'id': None,
                },
            }
        }
        reward.update({
            'id': '0',
            'amount': 1, # deprecated
            'amount_cents': 1,
            'description': 'Patrons Only',
        })
        return reward


class RewardsCollection(ApiResourceCollection):
    def __init__(self, rewards, user_id):
        resources = [RewardResource(reward) for reward in rewards]
        if not rewards or rewards[0].amount > 1:
            resources.insert(0, RewardPatronTierResource(user_id))
        resources.insert(0, RewardEveryoneTierResource(user_id))
        super().__init__(resources)
