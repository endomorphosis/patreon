from patreon.app.api.resources.simple_resource import SimpleResource


class Countries(SimpleResource):
    @staticmethod
    def resource_type():
        return "countries"
