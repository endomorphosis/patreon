from patreon.app.api.resources.simple_resource import SimpleResource


class PaymentEventStats(SimpleResource):
    @staticmethod
    def resource_type():
        return "payment_event_stats"
