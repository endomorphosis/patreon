from patreon.app.api.resources.blueprints.blueprint_api_resource import BlueprintAPIResource


class GoalResource(BlueprintAPIResource):
    @staticmethod
    def resource_type():
        return 'goal'

    @classmethod
    def blueprint(cls):
        return {
            # 'type': 'goal',
            'id': 'goal_id',
            'amount': 'amount_cents',  # deprecated
            'amount_cents': 'amount_cents',
            'title': 'goal_title',
            'description': 'goal_text',
            'created_at': 'date:created_at',
            'reached_at': 'date:reached_at'
        }
