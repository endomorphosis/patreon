from patreon.app.api.resources.blueprints.blueprint_api_resource import BlueprintAPIResource


class PaymentEventResource(BlueprintAPIResource):
    @staticmethod
    def resource_type():
        return "payment_event"

    @classmethod
    def blueprint(cls):
        return {
            # "type": "payment_event",
            "id": "payment_event_id",
            "payment_event_type": "type",
            "payment_event_subtype": "subtype",
            "data": "json_data",
            "created_at": "date:created_at",
            "relationships": {
            }
        }

