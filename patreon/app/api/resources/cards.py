from patreon.app.api.resources.blueprints.blueprint_api_resource import BlueprintAPIResource
from patreon.constants.cards import card_types


class CardResource(BlueprintAPIResource):
    @staticmethod
    def resource_type():
        return "card"

    @staticmethod
    def default_includes():
        return ['user']

    @classmethod
    def blueprint(cls):
        return {
            # "type": "card",
            'id': 'card_id',
            'number': 'number',
            'expiration_date': 'date:expiration_date',
            'is_verified': 'bool:is_verified',
            # 'card_type': card_types.get(int(self.card.type), 'Card')
            'created_at': 'date:created_at',
            'relationships': {
                'user': {
                    'type': 'user',
                    'id': 'user_id'
                }
            }
        }

    def as_json(self):
        json = super().as_json()
        json['card_type'] = card_types.get(int(self.model.type), 'Card')
        return json
