"""
patreon/app/api/resources/notifications.py

These objects represent transient renderings of things that happen on Patreon,
for example, the cards on the (proposed) /updates page
"""
from datetime import timedelta
import dateutil.parser

import patreon
from patreon import util
from patreon.app.api.resources import ApiResourceCollection, ApiResourceNull
from patreon.app.api.resources.blueprints.blueprint_api_resource import BlueprintAPIResource
from patreon.app.api.resources.campaigns import CampaignResource
from patreon.app.api.resources.comments import CommentResource
from patreon.app.api.resources.users import UserResource
from patreon.app.api.resources.posts import PostResource
from patreon.model.notifications import DailyCommentsNotification, DailyLikesNotification, \
    MonthlyPledgesNotification, PaymentsProcessingNotification
from patreon.model.manager import notifications_mgr, unread_status_mgr
from patreon.model.type import UnreadStatusType
from patreon.model.table import User, Campaign


class NotificationCardsCollection(ApiResourceCollection):
    homogeneous_type = "notification"

    def __init__(self, current_user, tz, per_page=10, cursor=None, items_per_card=5):
        self.current_user = current_user
        self.cursor = cursor
        self.per_page = per_page
        self.timezone = tz
        self.items_per_card = items_per_card
        self._members = None

    def cursor_date(self):
        if self.cursor:
            return dateutil.parser.parse(self.cursor).date()
        return None

    def resource_for_model(self, model):
        if issubclass(type(model), DailyCommentsNotification):
            return DailyCommentsNotificationResource(model)
        elif issubclass(type(model), DailyLikesNotification):
            return DailyLikesNotificationResource(model)
        elif issubclass(type(model), MonthlyPledgesNotification):
            return MonthlyPledgesNotificationResource(model)
        elif issubclass(type(model), PaymentsProcessingNotification):
            return PaymentsProcessingNotificationResource(model)
        else:
            raise Exception(str(type(model)) + " is not mapped to a resource")

    def members(self):
        if self._members is not None:
            return self._members

        sorted_models = notifications_mgr.find_notifications_for_user_paginated(
            self.current_user, self.per_page, self.cursor_date(), self.timezone,
            self.items_per_card
        )
        self._members = [self.resource_for_model(model) for model in sorted_models]
        return self._members

    def next_page_cursor(self):
        members = self.members()
        if members:
            last_card = members[-1]
            last_date = last_card.date()
            next_date = last_date - timedelta(days=1)
            return next_date.isoformat()
        return None

    def next_page_url(self):
        cursor = self.next_page_cursor()
        if cursor:
            # TODO something smarter to build URLs
            url = "https://{}/notifications".format(patreon.config.api_server)
            url += "?page[cursor]=" + cursor
            url += "&page[count]=" + str(self.per_page)
            return url

        return None

    def first_page_url(self):
        cursor = self.next_page_cursor()
        if cursor:
            # TODO something smarter to build URLs
            url = "https://{}/notifications".format(patreon.config.api_server)
            url += "?page[count]=" + str(self.per_page)
            return url

        return None


class NotificationResource(BlueprintAPIResource):
    @staticmethod
    def resource_type():
        return "notification"

    def date(self):
        # I was hoping that this would be useful to override,
        # but we have code that relies on model.date before it gets wrapped
        # in a NotificationResource, so I wasn't able to decouple this yet ~JW
        return self.model.date


# Comments

class DailyCommentsNotificationResource(NotificationResource):
    def comment_resource_collection(self):
        comment_resources = [
            NotificationCommentResource(comment, self.model.current_user)
            for comment in self.model.comments()
            ]
        links = {}
        if self.model.next_page():
            links["next"] = self.model.next_page()
        return ApiResourceCollection(comment_resources, links=links)

    def linked_resources(self):
        links = super().linked_resources()
        links["comments"] = self.comment_resource_collection()
        return links

    @classmethod
    def blueprint(cls):
        return {
            'id': 'resource_id()',
            'notification_type': 'notification_type()',
            'date': 'date:date_for_json()',
            'unread_count': 'unread_count()',
            'total_count': 'total_count()',
            'from_who_description': 'from_who_description()',
        }


class NotificationCommentResource(CommentResource):
    def __init__(self, comment, current_user=None):
        super().__init__(comment)
        self.current_user = current_user

    def replies_from_current_user_resources(self):
        replies = self.model.replies_from_user(self.current_user)
        reply_resources = [CommentResource(comment) for comment in replies]
        return ApiResourceCollection(reply_resources)

    def linked_resources(self):
        links = super().linked_resources()
        if self.current_user:
            links["replies"] = self.replies_from_current_user_resources()
        return links

    def is_unread(self):
        unread_time = unread_status_mgr.latest_read_time(
            self.current_user.user_id, UnreadStatusType.notifications
        )
        is_unread = True
        if unread_time:
            is_unread = self.model.created_at > unread_time
        return is_unread

    def as_json(self):
        json = super().as_json()
        json['is_unread'] = bool(self.is_unread())
        return json


class NotificationUserResource(UserResource):
    pass


class NotificationPostResource(PostResource):
    pass


# Likes

class DailyLikesNotificationResource(NotificationResource):
    def like_resource_collection(self):
        likes_resources = [
            LikesNotificationResource(likes_notification)
            for likes_notification in self.model.likes_notifications()
            ]

        links = {}
        if self.model.next_page():
            links["next"] = self.model.next_page()

        return ApiResourceCollection(likes_resources, links)

    def linked_resources(self):
        return {"likes_notifications": self.like_resource_collection()}

    @classmethod
    def blueprint(cls):
        return {
            'id': 'resource_id()',
            'notification_type': 'notification_type()',
            'date': 'date:date_for_json()',
            'unread_count': 'unread_count()',
            'total_count': 'total_count()',
            'total_likes': 'total_likes()'
        }


class LikesNotificationResource(NotificationResource):
    @staticmethod
    def resource_type():
        return "likes-notification"

    def linked_resources(self):
        # TODO: mask
        links = {"post": PostResource(self.model.post)}
        if self.model.latest_like():
            from patreon.app.api.resources.actions import LikeResource

            links["most_recent_like"] = LikeResource(self.model.latest_like())
        else:
            links["most_recent_like"] = ApiResourceNull()
        return links

    @classmethod
    def blueprint(cls):
        return {
            'id': 'resource_id()',
            'unread_count': 'unread_count()',
            'total_likes': 'total_likes()'
        }

    def as_json(self):
        json = super().as_json()
        if self.model.latest_like():
            json['latest_timestamp'] = util.datetime.as_utc(self.model.latest_like().created_at).isoformat()
        return json


# Pledges

class MonthlyPledgesNotificationResource(NotificationResource):
    def pledges_resource_collection(self):
        pledges_resources = [
            PledgeNotificationResource(pledge_notification)
            for pledge_notification in self.model.pledge_notifications()
            ]

        links = {}
        next_page = self.model.next_page()
        if next_page:
            links["next"] = next_page

        return ApiResourceCollection(pledges_resources, links)

    def linked_resources(self):
        return {"pledge_notifications": self.pledges_resource_collection()}

    @classmethod
    def blueprint(cls):
        return {
            'id': 'resource_id()',
            'notification_type': 'notification_type()',
            'date': 'date:date_for_json()',
            'unread_count': 'unread_count()',
            'total_count': 'total_count()',
            'net_pledged': 'net_pledged()',
            'net_num_patrons': 'net_num_patrons()'
        }


class PledgeNotificationResource(NotificationResource):
    @staticmethod
    def resource_type():
        return "pledge-notification"

    def prime_for_includes(self):
        super().prime_for_includes()
        Campaign.get.prime(self.model.first_pledge().campaign_id)
        User.get.prime(self.model.first_pledge().user_id)

    def linked_resources(self):
        return {"campaign": CampaignResource(Campaign.get(self.model.first_pledge().campaign_id)),
                "patron": UserResource(User.get(self.model.first_pledge().user_id))}

    @classmethod
    def blueprint(cls):
        return {
            'id': 'resource_id()',
            'timestamp': 'date:timestamp()',
            'start_amount_cents': 'start_amount()',
            'start_pledge_cap_cents': 'start_cap()',
            'end_amount_cents': 'end_amount()',
            'end_pledge_cap_cents': 'end_cap()',
            'is_creation': 'bool:is_creation()',
            'is_deletion': 'bool:is_deletion()',
            'is_unread': 'bool:is_unread()'
        }


# Payments processing

class PaymentsProcessingNotificationResource(NotificationResource):
    @classmethod
    def blueprint(cls):
        return {
            'id': 'resource_id()',
            'timestamp': 'date:timestamp()',
            'date': 'date:date_for_json()',  # part of the model.date family of kludges
            'charging_for_month': 'date:charging_for_month()',
            'notification_type': 'notification_type()',
            'charge_state': 'charge_state()',
            'transfer_state': 'transfer_state()',
            'amount_cents': 'amount_cents',
            'transfer_account_description': 'transfer_account_description()'
        }
