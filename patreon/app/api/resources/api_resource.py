class ApiResource(object):
    JSON_API_RC2 = "1.0RC2"
    JSON_API_RC3 = "1.0RC3"
    JSON_API_STABLE = "1.0"
    JSON_API_DEFAULT_VERSION = JSON_API_RC3

    # TODO: implement "include" parameter
    # TODO: implement "fields[TYPE]" parameter
    # TODO: implement "sort" parameter
    # TODO: JSON-API-to-Resource for writes
    # TODO: error objects

    def resource_id(self):
        """Override this in a subclass to return a unique id string"""
        return None

    def resource_id_str(self):
        id_ = self.resource_id()
        return str(id_) if id_ is not None else None

    @staticmethod
    def resource_type():
        """Override this in a subclass to return a type name string"""
        raise NotImplementedError()

    def as_json(self):
        """Override this in a subclass to return json"""
        raise NotImplementedError()

    def linked_resources(self):
        """Override this in a subclass to link to other resources"""
        return {}

    def relationship_url(self):
        return None

    def resource_url(self):
        return None

    def resource_key(self):
        if self.resource_id_str():
            return (self.resource_type(), self.resource_id_str())
        else:  # ApiResourceCollection or ApiResourceNull
            return None

    def resource_keys(self):
        if self.resource_key():
            return [self.resource_key()]
        else:
            return []

    def list_of_linked_resources(self):
        return [resource for link_name, resource in self.linked_resources().items()]

    def deeply_linked_resources(self, skip_self=True):
        if skip_self:  # prevent the toplevel object/collection from appearing in "linked"
            resources_seen = set(self.resource_keys())
            all_linked_resources = []
        else:
            resources_seen = set()
            all_linked_resources = [self] if self.resource_key() else []

        resources = self.list_of_linked_resources()
        while resources:
            for resource in resources:
                if not resource.resource_key():
                    continue
                if resource.resource_key() in resources_seen:
                    continue
                all_linked_resources.append(resource)
                resources_seen.add(resource.resource_key())

            resources = [
                linked_resource
                for resource in resources
                for linked_resource in resource.list_of_linked_resources()
                if (
                    not linked_resource.resource_key()  # arrays and nulls
                    or linked_resource.resource_key() not in resources_seen
                )
                ]

        return all_linked_resources

    def deeply_linked_resources_as_json(self, version, skip_self=True):
        linked_json = [resource.as_json_api_data(version) for resource in self.deeply_linked_resources(skip_self)]
        linked_json = [json for json in linked_json if json]
        return linked_json

    def resource_links_json(self, version):
        links_json = {
            link_name: resource.as_link_json(version)
            for link_name, resource in self.linked_resources().items()
            }
        if self.resource_url():
            links_json["self"] = self.resource_url()
        return links_json

    def as_linkage_json(self):
        return {"type": self.resource_type(), "id": self.resource_id_str()}

    def relationship_urls_json(self, version):
        relationship_urls_json = {}
        if self.relationship_url():
            relationship_urls_json["self"] = self.relationship_url()
        if self.resource_url():
            if version == ApiResource.JSON_API_RC2:
                relationship_urls_json["resource"] = self.resource_url()
            elif version == ApiResource.JSON_API_RC3 or version == ApiResource.JSON_API_STABLE:
                relationship_urls_json["related"] = self.resource_url()
            else:
                raise ValueError("Unknown JSON API version")
        return relationship_urls_json

    def as_link_json(self, version):
        link_json = {}

        if version == ApiResource.JSON_API_RC2:
            link_json.update(self.as_linkage_json())
        elif version == ApiResource.JSON_API_RC3:
            link_json["linkage"] = self.as_linkage_json()
        elif version == ApiResource.JSON_API_STABLE:
            link_json["data"] = self.as_linkage_json()
        else:
            raise ValueError("Unknown JSON API version")

        link_json.update(self.relationship_urls_json(version))

        return link_json

    def as_json_api_data(self, version):
        if version == ApiResource.JSON_API_STABLE:
            attributes = self.as_json()
            if 'id' in attributes:
                del attributes['id']
            json = {'attributes': attributes}
        else:
            json = self.as_json()
        json["id"] = self.resource_id_str()
        json["type"] = self.resource_type()
        links = self.resource_links_json(version)
        if links:
            if version == ApiResource.JSON_API_STABLE:
                json["relationships"] = links
            else:
                json["links"] = links
        # TODO: optional keys: "meta"
        return json

    def next_page_url(self):
        return None

    def prev_page_url(self):
        return None

    def last_page_url(self):
        return None

    def first_page_url(self):
        return None

    def meta(self):
        return None

    def document_links_urls(self):
        links = {
            "next": self.next_page_url(),
            "prev": self.prev_page_url(),
            "last": self.last_page_url(),
            "first": self.first_page_url()
        }
        links = {key: value for key, value in links.items() if value}
        return links

    def included_resources_key(self, version):
        if version == ApiResource.JSON_API_RC2:
            return "linked"
        elif version == ApiResource.JSON_API_RC3 or version == ApiResource.JSON_API_STABLE:
            return "included"
        else:
            raise ValueError("Unknown JSON API version")

    def as_json_api_document(self, version=None):
        # TODO: remove this hack once versions are stabilized
        if version is None:
            from patreon.app.api import get_json_api_version
            version = get_json_api_version()
        return self.document_with_data(self.as_json_api_data(version), version)

    def as_json_api_relationship_document(self, version=None):
        # TODO: remove this hack once versions are stabilized
        if version is None:
            from patreon.app.api import get_json_api_version
            version = get_json_api_version()
        return self.document_with_data(self.as_linkage_json(), version, False)

    def document_with_data(self, data, version, skip_self=True):
        response = {"data": data}
        included_resources = self.deeply_linked_resources_as_json(version, skip_self)
        if included_resources:
            response[self.included_resources_key(version)] = included_resources
        links = self.document_links_urls()
        if links:
            response["links"] = links
        meta = self.meta()
        if meta is not None:
            response["meta"] = meta
        return response
