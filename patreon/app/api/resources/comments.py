from flask import session
import patreon
from patreon.app.api.resources import ApiResourceCollection, ApiResource
from patreon.app.api.resources.blueprints.blueprint_api_resource import BlueprintAPIResource
from patreon.model.manager import comment_mgr
from patreon.model.table import Comment, Pledge


class CommentResource(BlueprintAPIResource):
    def __init__(self, comment, include_children=False, includes=None):
        self.include_children = include_children
        super().__init__(comment, includes=includes)

    @staticmethod
    def resource_type():
        return "comment"

    @staticmethod
    def route_prefix():
        return "comment"

    @staticmethod
    def default_includes():
        return ['parent', 'commenter', 'post']

    def prime_for_includes(self):
        super().prime_for_includes()
        if self.include_children:
            comment_mgr.find_by_parent_comment_id.prime(self.model.comment_id)
        # Prime the is_by_patron get
        for creator in self.model.post.campaign.users:
            Pledge.get.prime(self.model.commenter_id, creator.user_id)

    @classmethod
    def blueprint(cls):
        return {
            # "type": "comment",
            "id": "comment_id",
            "body": "comment_text",
            "created": "date:created_at",
            "is_by_creator": "is_by_creator()",
            "is_by_patron": "is_by_patron()",
            "vote_sum": "vote_sum",
            "relationships": {
                "parent": {
                    "type": "comment",
                    "id": "thread_id",
                },
                "commenter": {
                    "type": "user",
                    "id": "commenter_id",
                },
                "post": {
                    "type": "post",
                    "id": "post_id",
                },
                "replies": {
                    "type": "array:comment",
                    "accessor": "children",
                }
            }
        }

    def children(self):
        return ApiResourceCollection([
            CommentResource(comment, include_children=True)
            for comment in comment_mgr.get_flattened_comments_for_post(self.model.post_id).get(self.model.comment_id, [])
        ])

    def linked_resources(self):
        links = super().linked_resources()
        if self.include_children:
            links["replies"] = self.children()
            # remove this once all clients are done migrating to 'replies' as the key
            links["children"] = links["replies"]
        return links

    def current_user_vote(self):
        if not session or not session.user_id:
            return 0
        return comment_mgr.get_vote_for_comment_by_user_id(
            comment_id=self.model.comment_id, user_id=session.user_id
        )

    def as_json(self):
        json = super().as_json()
        json['current_user_vote'] = self.current_user_vote()
        return json

    @classmethod
    def insertable_from_json_api_document(cls, json, version=None, current_user=None):
        # TODO: remove this hack once versions are stabilized
        model = super().insertable_from_json_api_document(
            json,
            version or patreon.app.api.get_json_api_version()
        )
        return {
            'UID': current_user.user_id,
            'HID': model.get('post_id'),
            'Comment': model.get('comment_text'),
            'ThreadID': model.get('thread_id')
        }

    @classmethod
    def new_from_json_api_document(cls, json, version=None, current_user=None):
        # TODO: remove this hack once versions are stabilized
        # TODO: how else to pass the extra arg into super such that it gets passed to insertable_from_json_api_document?
        insertable = cls.insertable_from_json_api_document(
            json,
            version=version or patreon.app.api.get_json_api_version(),
            current_user=current_user
        )

        model = comment_mgr.save_comment(
            insertable['UID'], insertable['HID'], insertable['Comment'], insertable['ThreadID']
        )
        # id_ = cls.model_class().insert(insertable)[0]
        # model = cls.model_class().get(**{cls.blueprint().get('id'): id_})
        resource = cls(model)
        return resource

    @classmethod
    def update_from_json_api_document(cls, json, version=None, current_user=None):
        # TODO: remove this hack once versions are stabilized
        if version is None:
            version = patreon.app.api.get_json_api_version()

        # TODO: assert that the 'type' is included
        comment_id = int(json['data']['id'])
        # TODO: assert that no extra data is included.
        attributes = json['data']['attributes'] if version == ApiResource.JSON_API_STABLE else json['data']
        body = attributes['body']
        comment = Comment.get(comment_id=comment_id)
        if not comment:
            return None

        if comment.commenter_id != current_user.user_id:
            raise Exception("Access Denied")

        comment.update({"Comment": body})

        return cls(comment)


class CommentRepliesCollection(ApiResourceCollection):
    def __init__(self, comment):
        self.comment = comment
        self._members = None

    def members(self):
        if self._members is not None:
            return self._members

        self._members = CommentResource(self.comment, include_children=True).children().members()
        return self._members


class PostCommentsCollection(ApiResourceCollection):
    def __init__(self, post, sort=None):
        self.post = post
        self.sort = sort if sort else 'created'
        self._members = None

    def meta(self):
        return { "count": comment_mgr.count_by_activity_id(self.post.activity_id) }

    def members(self):
        if self._members is not None:
            return self._members

        thread_heads = comment_mgr.find_thread_heads_by_activity_id(self.post.activity_id, sort=self.sort)
        self._members = [
            CommentResource(comment, include_children=True)
            for comment in thread_heads
            ]
        return self._members


class PostCommentsCollectionPaged(ApiResourceCollection):
    def __init__(self, post, per_page=10, cursor=None, offset=None, sort=None):
        self.post = post
        self.cursor = cursor
        self.per_page = per_page
        self.offset = offset
        self.sort = sort if sort else 'created'
        self._members = None

    def meta(self):
        return { "count": comment_mgr.count_by_activity_id(self.post.activity_id) }

    def members(self):
        if self._members is not None:
            return self._members

        thread_heads = comment_mgr.find_thread_heads_by_activity_id_paged(
            self.post.activity_id,
            cursor=self.cursor,
            offset=self.offset,
            per_page=self.per_page,
            sort=self.sort
        )
        self._members = [
            CommentResource(comment, include_children=True)
            for comment in thread_heads
            ]
        return self._members

    def _paging_sort_param(self):
        return '&sort={}'.format(self.sort) if self.sort else ''

    def next_page_cursor(self):
        members = self.members()
        if members:
            last_comment = members[-1]
            return str(last_comment.model.CID)
        return None

    def next_page_url(self):
        if self.per_page == 'infinity':
            return None

        cursor = self.next_page_cursor()
        if cursor or self.offset:
            # TODO something smarter to build URLs
            url = "https://{0}/posts/{1}/comments".format(
                patreon.config.api_server,
                self.post.activity_id
            )
            if self.offset is not None:
                url += "?page[offset]=" + str(int(self.offset) + int(self.per_page))
            else:
                url += "?page[cursor]=" + cursor
            url += "&page[count]=" + str(self.per_page)
            url += self._paging_sort_param()
            return url

        return None

    def first_page_url(self):
        url = "https://{0}/posts/{1}/comments".format(
            patreon.config.api_server,
            self.post.activity_id
        )
        url += "?page[count]=" + str(self.per_page)
        url += self._paging_sort_param()
        return url


class CommentVoteResource(BlueprintAPIResource):
    @staticmethod
    def resource_type():
        return "comment-vote"

    @staticmethod
    def default_includes():
        return ['user', 'comment']

    @classmethod
    def blueprint(cls):
        return {
            # "type": "comment-vote",
            "id": "comment_vote_id",
            "vote": "vote",
            "relationships": {
                "comment": {
                    "type": "comment",
                    "id": "comment_id",
                },
                "user": {
                    "type": "user",
                    "id": "user_id",
                },
                "post": {
                    "type": "post",
                    "id": "activity_id"
                }
            }
        }

    @classmethod
    def insertable_from_json_api_document(cls, json, version=None):
        model = super().insertable_from_json_api_document(json, version)
        comment = comment_mgr.get_comment_or_throw(model['comment_id'])
        model['activity_id'] = comment.HID
        return model
