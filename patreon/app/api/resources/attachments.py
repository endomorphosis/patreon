from patreon.app.api.resources.blueprints.blueprint_api_resource import BlueprintAPIResource
from patreon.app.api.resources import ApiResourceCollection


class AttachmentResource(BlueprintAPIResource):
    @staticmethod
    def resource_type():
        return "attachment"

    @staticmethod
    def default_includes():
        return ['post']

    @classmethod
    def blueprint(cls):
        return {
            # "type": "attachment",
            'id': 'attachment_id',
            'url': 'url',
            'name': 'name',
            'relationships': {
                'post': {
                    'type': 'post',
                    'id': 'activity_id'
                }
            }
        }


class AttachmentsCollection(ApiResourceCollection):
    def __init__(self, attachments):
        super().__init__([AttachmentResource(attachment) for attachment in attachments])
