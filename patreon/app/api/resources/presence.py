from patreon.app.api.resources.blueprints.blueprint_api_resource import BlueprintAPIResource


class PresenceResource(BlueprintAPIResource):
    @staticmethod
    def resource_type():
        return "presence"

    @staticmethod
    def default_includes():
        return ['user']

    @classmethod
    def blueprint(cls):
        return {
            'id': 'presence_id',
            'room': 'room',
            'last_seen': 'date:last_seen',
            'expiration_time': 'date:expiration_time',
            'relationships': {
                'user': {
                    'type': 'user',
                    'id': 'user_id'
                }
            }
        }
