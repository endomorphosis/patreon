import dateutil.parser
import patreon
from patreon.app.api.resources import ApiResource, ApiResourceCollection, ApiResourceNull
from patreon.app.api.resources.blueprints.blueprint_class_map import blueprint_class_map
from patreon import util
import json


class BlueprintAPIResource(ApiResource):
    def __init__(self, model, includes=None):
        self.model = model
        if includes is None:
            includes = type(self).default_includes()
        self.includes = includes
        self.prime_for_includes()

    @classmethod
    def model_class(cls):
        return blueprint_class_map().get(cls.resource_type()).get('model_class')

    @classmethod
    def resource_class(cls):
        return blueprint_class_map().get(cls.resource_type()).get('resource_class')

    @classmethod
    def blueprint(cls):
        """Override this in a subclass to define model <=> API mappings"""
        raise NotImplementedError()

    @staticmethod
    def default_includes():
        return []

    def blueprint_lookup(self, json_key):
        return type(self).blueprint().get(json_key)

    def resource_id(self):
        return self.dereference_blueprint_value('id')

    @staticmethod
    def route_prefix():
        return None

    def resource_url(self):
        if self.route_prefix():
            return "https://{}/{}/{}".format(patreon.config.api_server, self.route_prefix(), self.resource_id_str())
        return super().resource_url()

    ## Serialization

    # Included resources

    def dereference_blueprint_relationship(self, key):
        relationships_blueprint = self.blueprint_lookup('relationships')
        relationship_data_blueprint = relationships_blueprint.get(key)
        relationship_model_type = relationship_data_blueprint.get('type')
        if relationship_model_type.startswith('array:'):
            relationship_model_type = relationship_model_type.split(':')[-1]
            resource_class = blueprint_class_map().get(relationship_model_type).get('resource_class')
            if 'id' in relationship_data_blueprint:
                # TODO: 'id' on to-many relations?
                models = []
            else:
                models = getattr(self.model, relationship_data_blueprint.get('accessor'))
            # TODO: custom collection class, custom arguments to resource_class
            return ApiResourceCollection([ resource_class(model) for model in models ])
        else:
            if 'id' in relationship_data_blueprint:
                relationship_model_class = blueprint_class_map().get(relationship_model_type).get('model_class')
                model = relationship_model_class.get(getattr(self.model, relationship_data_blueprint.get('id')))
            else:
                model = getattr(self.model, relationship_data_blueprint.get('accessor'))
            relationship_resource_class = blueprint_class_map().get(relationship_model_type).get('resource_class')
            if model:
                return relationship_resource_class(model)
            else:
                return ApiResourceNull()

    def linked_resources(self):
        links = {}
        relationships_blueprint = self.blueprint_lookup('relationships')
        if relationships_blueprint:
            for key in relationships_blueprint:
                if key in self.includes:
                    links[key] = self.dereference_blueprint_relationship(key)
        return links

    # SQL Priming for Serialization

    def prime_blueprint_relationship(self, key):
        relationships_blueprint = self.blueprint_lookup('relationships')
        relationship_data_blueprint = relationships_blueprint.get(key)
        relationship_type = relationship_data_blueprint.get('type')
        if relationship_type.startswith('array:'):
            # TODO: prime arrays?
            pass
        else:
            if 'id' in relationship_data_blueprint:
                relationship_model_class = blueprint_class_map().get(relationship_type).get('model_class')
                if hasattr(relationship_model_class.get, 'prime') and getattr(relationship_model_class.get, 'multiget') is not None:
                    relationship_model_class.get.prime(getattr(self.model, relationship_data_blueprint.get('id')))
            else:
                # TODO: prime accessors?
                pass

    def prime_for_includes(self):
        relationships_blueprint = self.blueprint_lookup('relationships')
        if relationships_blueprint:
            for key in relationships_blueprint:
                if key in self.includes:
                    self.prime_blueprint_relationship(key)

    # Top-Level Values

    def dereference_blueprint_value(self, key):
        attr_name = self.blueprint_lookup(key)
        if attr_name.endswith('()'):
            value = getattr(self.model, attr_name[:-2].split(':')[-1])()
        else:
            value = getattr(self.model, attr_name.split(':')[-1])

        if attr_name.startswith('date:') and value is not None:
            value = util.datetime.as_utc(value).isoformat()
        elif attr_name.startswith('bool:') and value is not None:
            value = bool(value)
        elif attr_name.startswith('json:') and value is not None:
            value = json.loads(value)

        return value

    def as_json(self):
        json = {}
        blueprint = type(self).blueprint()
        for key in blueprint:
            if key == 'relationships':
                continue
            if key == 'type':
                continue

            json[key] = self.dereference_blueprint_value(key)
        return json

    ## De-serialization

    @classmethod
    def insertable_from_json_api_document(cls, json, version=None):
        # TODO: remove this hack once versions are stabilized
        if version is None:
            from patreon.app.api import get_json_api_version
            version = get_json_api_version()

        blueprint = cls.blueprint()
        model = {} # TODO: make real SQLAlchemy model that has no db row then call db_session.flush to really insert it

        # TODO: assert that no extra data is included.
        # TODO: assert that the 'type' is included
        for key in blueprint:
            if key == 'relationships':
                continue
            if key == 'type':
                continue

            attr_name = blueprint.get(key)
            if attr_name.endswith('()'):
                continue
            else:
                if version == ApiResource.JSON_API_STABLE:
                    if key == 'id':
                        value = json.get('data').get(key)
                    else:
                        value = json.get('data').get('attributes').get(key)
                else:
                    value = json.get('data').get(key)

            # TODO: how to handle Null better?
            if value is None:
                continue

            if attr_name.startswith('date:'):
                value = dateutil.parser.parse(value)
                value = util.datetime.make_naive(value)
            elif attr_name.startswith('bool:'):
                value = bool(str(value).lower() != 'false')
            attr_name = attr_name.split(':')[-1]

            model[attr_name] = value

        # TODO: confirm 'type' of relationships
        relationship_blueprint = blueprint.get('relationships')
        if relationship_blueprint:
            for key in relationship_blueprint:
                if version == ApiResource.JSON_API_STABLE:
                    json_relationship_data = json.get('data', {}).get('relationships', {}).get(key, {}).get('data')
                else:
                    json_relationship_data = json.get('data', {}).get('links', {}).get(key, {}).get('linkage')
                if json_relationship_data is None:
                    continue

                relationship_data_blueprint = relationship_blueprint.get(key)
                relationship_model_type = relationship_data_blueprint.get('type')
                if relationship_model_type.startswith('array:'):
                    # TODO: arrays
                    pass
                else:
                    if 'id' in relationship_data_blueprint:
                        model[relationship_data_blueprint.get('id')] = json_relationship_data.get('id')
                    else:
                        # TODO: ???
                        pass

        return model

    @classmethod
    def new_from_json_api_document(cls, json, version=None):
        # TODO: remove this hack once versions are stabilized
        if version is None:
            from patreon.app.api import get_json_api_version
            version = get_json_api_version()
        insertable = cls.insertable_from_json_api_document(json, version=version)
        id_ = cls.model_class().insert(insertable)[0]
        model = cls.model_class().get(**{cls.blueprint().get('id'): id_})
        return cls(model)
