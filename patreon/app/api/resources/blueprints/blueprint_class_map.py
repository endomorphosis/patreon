from patreon.model.table import Comment, Activity, User, Attachments,\
    Campaign, Card, Follow, Pledge, Reward, Actions


def blueprint_class_map():
    from patreon.app.api.resources.comments import CommentResource
    from patreon.app.api.resources.users import UserResource
    from patreon.app.api.resources.posts import PostResource
    from patreon.app.api.resources.attachments import AttachmentResource
    from patreon.app.api.resources.campaigns import CampaignResource
    from patreon.app.api.resources.cards import CardResource
    from patreon.app.api.resources.follows import FollowResource
    from patreon.app.api.resources.pledges import PledgeResource
    from patreon.app.api.resources.rewards import RewardResource
    from patreon.app.api.resources.actions import ActionResource, LikeResource
    return {
        'comment': {
            'model_class': Comment,
            'resource_class': CommentResource
        },
        'post': {
            'model_class': Activity,
            'resource_class': PostResource
        },
        'user': {
            'model_class': User,
            'resource_class': UserResource
        },
        'attachment': {
            'model_class': Attachments,
            'resource_class': AttachmentResource
        },
        'campaign': {
            'model_class': Campaign,
            'resource_class': CampaignResource
        },
        'card': {
            'model_class': Card,
            'resource_class': CardResource
        },
        'follow': {
            'model_class': Follow,
            'resource_class': FollowResource
        },
        'pledge': {
            'model_class': Pledge,
            'resource_class': PledgeResource
        },
        'reward': {
            'model_class': Reward,
            'resource_class': RewardResource
        },
        'action': {
            'model_class': Actions,
            'resource_class': ActionResource
        },
        'like': {
            'model_class': Actions,
            'resource_class': LikeResource
        }
    }
