from patreon.app.api.resources.blueprints.blueprint_api_resource import BlueprintAPIResource


class PushInfoResource(BlueprintAPIResource):
    @staticmethod
    def resource_type():
        return "push-info"

    @staticmethod
    def route_prefix():
        return "push-info"

    @staticmethod
    def default_includes():
        return ['user']

    @classmethod
    def blueprint(cls):
        return {
            # "type": "push-info",
            'id': 'users_push_info_id',
            'bundle_id': 'bundle_id',
            'created_at': 'date:created_at',
            'edited_at': 'date:edited_at',
            'relationships': {
                'user': {
                    'type': 'user',
                    'id': 'user_id'
                }
            }
        }
