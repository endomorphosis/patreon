from patreon.app.api.resources.blueprints.blueprint_api_resource import BlueprintAPIResource
from patreon.model.manager import unread_status_mgr


class UnreadStatusResource(BlueprintAPIResource):
    @staticmethod
    def resource_type():
        return "unread_status"

    @staticmethod
    def default_includes():
        return []

    @classmethod
    def blueprint(cls):
        return {
            # "type": "unread_status",
            'id': 'unread_status_id',
            'unread_key': 'unread_key',
            # 'unread_count': 'unread_status_mgr.unread_count(self.model.user_id, self.model.unread_key)',
            'read_timestamp': 'date:read_timestamp',
            'relationships': {
                'user': {
                    'type': 'user',
                    'id': 'user_id'
                }
            }
        }

    def as_json(self):
        json = super().as_json()
        json['unread_count'] = unread_status_mgr.unread_count(self.model.user_id, self.model.unread_key)
        return json
