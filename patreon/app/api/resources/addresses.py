from patreon.app.api import ApiResource


class AddressResource(ApiResource):
    def __init__(self, address, address_id):
        self.address_id = address_id  # this is fake until we get a real address table in sql
        self.address = address

    @staticmethod
    def resource_type():
        return "address"

    def resource_id(self):
        return self.address_id

    def as_json(self):
        return {
            "line_1": self.address.address_line_1,
            "line_2": self.address.address_line_2,
            "postal_code": self.address.address_zip,
            "city": self.address.city,
            "state": self.address.state,
            "country": self.address.country,
        }

