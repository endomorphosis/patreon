from patreon.app.api.resources.blueprints.blueprint_api_resource import BlueprintAPIResource
from patreon.model.manager import campaign_mgr, unread_status_mgr
from patreon.model.table import User


class FollowResource(BlueprintAPIResource):
    @staticmethod
    def resource_type():
        return "follow"

    @staticmethod
    def route_prefix():
        return "follows"

    @staticmethod
    def default_includes():
        return ['follower','followed']

    @classmethod
    def blueprint(cls):
        return {
            # "type": "follow",
            "id": "follow_id",
            "created_at": "date:created_at",
            "relationships": {
                "follower": {
                    "type": "user",
                    "id": "follower_id",
                },
                "followed": {
                    "type": "user",
                    "id": "followed_id",
                }
            }
        }

    def prime_for_includes(self):
        super().prime_for_includes()
        if 'follower' in self.includes:
            User.get.prime(self.model.follower_id)
        if 'followed' in self.includes:
            User.get.prime(self.model.followed_id)
        if 'unread_count' in self.includes:
            campaign_mgr.get_campaign_ids_by_user_id.prime(self.model.followed_id)

    def unread_count(self):
        campaign_id = campaign_mgr.try_get_campaign_id_by_user_id_hack(self.model.followed_id)
        if campaign_id:
            return unread_status_mgr.get_unread_count_for_campaign_creations(campaign_id, self.model.follower_id)
        return 0

    def as_json(self):
        json = super().as_json()
        if 'unread_count' in self.includes:
            json['unread_count'] = self.unread_count()
        return json
