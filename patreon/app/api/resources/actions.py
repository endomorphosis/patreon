import patreon
from patreon.app.api.resources.blueprints.blueprint_api_resource import BlueprintAPIResource
from patreon.app.api.resources.api_resource_collection import ApiResourceCollection
from patreon.model.manager import action_mgr


class ActionResource(BlueprintAPIResource):
    @staticmethod
    def resource_type():
        return "action"

    @staticmethod
    def default_includes():
        return ['user', 'post']

    @classmethod
    def blueprint(cls):
        return {
            # "type": "action",
            "id": "action_id",
            "created_at": "date:created_at",
            "relationships": {
                "user": {
                    "type": "user",
                    "id": "user_id",
                },
                "post": {
                    "type": "post",
                    "id": "activity_id",
                }
            }
        }


class LikeResource(ActionResource):
    @staticmethod
    def resource_type():
        return "like"

    @staticmethod
    def route_prefix():
        return "likes"


class PostLikesCollection(ApiResourceCollection):
    def __init__(self, post, cursor_date=None, page_size=2):
        super().__init__()
        self.post = post
        self.cursor_date = cursor_date
        self.page_size = page_size
        self.has_more_pages = True

    def members(self):
        if self._members:
            return self._members
        likes_with_one_extra = action_mgr.find_likes_by_activity_id_paginated_by_cursor(
            self.post.activity_id, cursor_date=self.cursor_date, per_page=self.page_size + 1
        )
        self.has_more_pages = len(likes_with_one_extra) > self.page_size
        self._members = [LikeResource(like) for like in likes_with_one_extra[:self.page_size]]
        return self._members

    def next_page_cursor(self):
        if self.has_more_pages:
            members = self.members()
            return members[-1].model.created_at.isoformat()
        return None

    def next_page_url(self):
        cursor = self.next_page_cursor()
        if cursor is not None:
            return "https://{0}/posts/{1}/likes?page[cursor]={2}&page[count]={3}".format(
                patreon.config.api_server,
                self.post.activity_id,
                cursor,
                self.page_size
            )
        else:
            return None

    def first_page_url(self):
        return "https://{0}/posts/{1}/likes?page[count]={2}".format(
            patreon.config.api_server,
            self.post.activity_id,
            self.page_size
        )
