from patreon.app.api.resources.simple_resource import SimpleResource


class VatResource(SimpleResource):
    @staticmethod
    def resource_type():
        return "vat_rate"


class VatCountries(SimpleResource):
    @staticmethod
    def resource_type():
        return "vat_countries"


class VatCountriesRates(SimpleResource):
    @staticmethod
    def resource_type():
        return "vat_countries_rates"
