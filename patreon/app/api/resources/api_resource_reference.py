from patreon.app.api.resources import ApiResource


class ApiResourceReference(ApiResource):
    """This is an link to another object"""

    def __init__(self, resource_url=None, relationship_url=None):
        if not resource_url and not relationship_url:
            raise ValueError("at least one of resource_url or relationship_url must be set")
        self._resource_url = resource_url
        self._relationship_url = relationship_url

    def resource_url(self):
        return self._resource_url

    def relationship_url(self):
        return self._relationship_url

    def as_json_api_data(self):
        return None

    def as_link_json(self, version):
        if not self.relationship_url():
            return self.resource_url()
        json = {
            "self": self.relationship_url()
        }
        if self.resource_url():
            json["resource"] = self.resource_url()
        return json
