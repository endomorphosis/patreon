from patreon.app.api.resources import ApiResource


class ApiResourceNull(ApiResource):
    def as_linkage_json(self):
        return None

    def as_link_json(self, version):
        if version == ApiResource.JSON_API_RC2:
            return {"id": None}
        elif version == ApiResource.JSON_API_RC3:
            return {"linkage": self.as_linkage_json()}
        elif version == ApiResource.JSON_API_STABLE:
            return {"data": self.as_linkage_json()}
        else:
            raise ValueError("Unknown JSON API version")
