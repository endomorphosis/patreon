from patreon.model.manager import complete_pledge_mgr, reward_mgr, campaign_mgr, \
    goal_mgr, rewards_give_mgr, pledge_mgr, activity_mgr
from patreon.app.api.resources.blueprints.blueprint_api_resource import BlueprintAPIResource
from patreon.app.api.resources.rewards import RewardsCollection
from patreon.app.api.resources.goals import GoalResource
from patreon.app.api.resources.pledges import PledgeResource
from patreon.app.api.resources import ApiResourceCollection, ApiResourceNull


class CampaignResource(BlueprintAPIResource):
    def __init__(self, model, includes=None, current_user_id=None):
        super().__init__(model, includes=includes)
        self.current_user_id = current_user_id

    @staticmethod
    def resource_type():
        return "campaign"

    @staticmethod
    def default_includes():
        return ['rewards', 'creator', 'goals']

    @classmethod
    def blueprint(cls):
        return {
            # "type": "campaign",
            'id': 'campaign_id',
            'summary': 'summary',
            'creation_name': 'creation_name',
            'pay_per_name': 'pay_per_name',
            'one_liner': 'one_liner',
            'main_video_embed': 'main_video_embed',
            'main_video_url': 'main_video_url',
            'image_small_url': 'image_small_url',
            'image_url': 'image_url',
            'thanks_video_url': 'thanks_video_url',
            'thanks_embed': 'thanks_embed',
            'thanks_msg': 'thanks_msg',
            'is_monthly': 'bool:is_monthly',
            'is_nsfw': 'bool:is_nsfw',
            'created_at': 'date:created_at',
            'published_at': 'date:published_at',
            # 'pledge_sum': pledge_mgr.get_sum_pledges_to_creator(creator_id),
            # 'patron_count': pledge_mgr.get_count_pledges_to_creator(creator_id),
            'pledge_url': 'pledge_url',
            'relationships': {
                # 'creator': {
                #     'type': 'user',
                #     'id': 'creator_id'
                # },
                # 'rewards': {
                #     'type': 'array:reward',
                #     'accessor': 'Reward.find_by_creator_id(creator_id)'
                # }
            }
        }

    def prime_for_includes(self):
        super().prime_for_includes()
        if 'creator' in self.includes:
            campaign_mgr.get_users_by_campaign_id.prime(self.model.campaign_id)
        reward_mgr.find_by_creator_id.prime(self.model.creator_id)

        pledge_mgr.get_pledge_total_by_creator_id.prime(self.model.creator_id)
        pledge_mgr.get_patron_count_by_creator_id.prime(self.model.creator_id)

    def rewards(self):
        rewards = reward_mgr.find_by_creator_id(self.model.creator_id)
        return RewardsCollection(rewards, self.model.creator_id)

    def goals(self):
        goals = goal_mgr.find_by_campaign_id(self.model.campaign_id)
        goal_resources = [GoalResource(goal) for goal in goals]
        return ApiResourceCollection(goal_resources)

    def creator(self):
        from patreon.app.api.resources.users import UserResource

        user = campaign_mgr.try_get_user_by_campaign_id_hack(self.model.campaign_id)
        return UserResource(user)

    def current_user_pledge(self):
        pledge = None
        if self.current_user_id:
            pledge = complete_pledge_mgr.get_pledge_by_patron_and_campaign_or_creator(
                patron_id=self.current_user_id, creator_id=self.model.creator_id
            ).legacy
        if pledge:
            return PledgeResource(pledge)
        else:
            return ApiResourceNull()

    def linked_resources(self):
        links = super().linked_resources()
        if 'creator' in self.includes:
            links['creator'] = self.creator()
        if 'goals' in self.includes:
            links['goals'] = self.goals()
        if 'rewards' in self.includes:
            links['rewards'] = self.rewards()
        if 'current_user_pledge' in self.includes and self.current_user_id:
            links['current_user_pledge'] = self.current_user_pledge()
        return links

    def as_json(self):
        json = super().as_json()
        json['pledge_sum'] = int(pledge_mgr.get_pledge_total_by_creator_id(self.model.creator_id))
        json['patron_count'] = int(pledge_mgr.get_patron_count_by_creator_id(self.model.creator_id))
        json['creation_count'] = int(activity_mgr.get_creator_post_count_by_campaign_id(self.model.campaign_id))
        if self.current_user_id:
            json['outstanding_payment_amount_cents'] = int(
                rewards_give_mgr.pending_pledge_sum_for_creator_and_user(
                    creator_id=self.model.creator_id, user_id=self.current_user_id
                )
            )
        return json
