from patreon.app.api.resources.blueprints.blueprint_api_resource import BlueprintAPIResource
from patreon.app.api.resources import ApiResourceCollection


class SettingsResource(BlueprintAPIResource):
    @staticmethod
    def resource_type():
        return "settings"

    @staticmethod
    def default_includes():
        return ['user', 'follow-settings', 'campaign-settings']

    @classmethod
    def blueprint(cls):
        return {
            # "type": "settings",
            'id': 'settings_id()',
            'email_about_all_new_comments': 'bool:email_about_all_new_comments()',
            'email_about_milestone_goals': 'bool:email_about_milestone_goals()',
            'email_about_patreon_updates': 'bool:email_about_patreon_updates()',
            'push_about_all_new_comments': 'bool:push_about_all_new_comments()',
            'push_about_milestone_goals': 'bool:push_about_milestone_goals()',
            'push_about_patreon_updates': 'bool:push_about_patreon_updates()',
            'pledges_are_private': 'bool:pledges_are_private()',
            'relationships': {
                'user': {
                    'type': 'user',
                    'id': 'user_id'
                },
                # 'follow-settings': {
                #     'type': 'array:follow-settings',
                #     'accessor': 'follow-settings()'
                # }
                # 'campaign-settings': {
                #     'type': 'array:campaign-settings',
                #     'accessor': 'campaign-settings()'
                # }
            }
        }

    def follow_settings(self):
        return ApiResourceCollection([
            FollowSettingsResource(follow_settings)
            for follow_settings in self.model.follow_settings
        ])

    def campaign_settings(self):
        return ApiResourceCollection([
            CampaignSettingsResource(campaign_settings)
            for campaign_settings in self.model.campaign_settings
        ])

    def linked_resources(self):
        links = super().linked_resources()
        if 'follow-settings' in self.includes:
            links['follow-settings'] = self.follow_settings()
        if 'campaign-settings' in self.includes:
            links['campaign-settings'] = self.campaign_settings()
        return links


class FollowSettingsResource(BlueprintAPIResource):
    @staticmethod
    def resource_type():
        return "follow-settings"

    @staticmethod
    def default_includes():
        return ['user', 'campaign']

    @classmethod
    def blueprint(cls):
        return {
            # "type": "follow_settings",
            'id': 'settings_id',
            'email_about_new_comments': 'bool:email_new_comment',
            'email_about_new_posts': 'bool:email_post',
            'email_about_new_paid_posts': 'bool:email_paid_post',
            'push_about_new_comments': 'bool:push_new_comment',
            'push_about_new_posts': 'bool:push_post',
            'push_about_new_paid_posts': 'bool:push_paid_post',
            'push_about_going_live': 'bool:push_creator_live',
            'relationships': {
                'user': {
                    'type': 'user',
                    'id': 'user_id'
                },
                'campaign': {
                    'type': 'campaign',
                    'id': 'campaign_id'
                }
            }
        }


class CampaignSettingsResource(BlueprintAPIResource):
    @staticmethod
    def resource_type():
        return "campaign-settings"

    @staticmethod
    def default_includes():
        return ['campaign']

    @classmethod
    def blueprint(cls):
        return {
            # "type": "campaign_settings",
            'id': 'settings_id',
            'email_about_new_patron_posts': 'bool:should_email_on_patron_post',
            'email_about_new_patrons': 'bool:should_email_on_new_patron',
            'push_about_new_patron_posts': 'bool:should_push_on_patron_post',
            'push_about_new_patrons': 'bool:should_push_on_new_patron',
            'relationships': {
                'user': {
                    'type': 'user',
                    'id': 'user_id'
                },
                'campaign': {
                    'type': 'campaign',
                    'id': 'campaign_id'
                }
            }
        }
