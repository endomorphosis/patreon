from patreon.constants import image_sizes
from patreon.util import datetime

# TODO MARCOS move from role-based to field-based ACL

def to_json(user):
    return {
        'id': str(user.user_id),
        'type': 'user',
        'email': user.email,
        'first_name': user.first_name,
        'last_name': user.last_name,
        'full_name': user.full_name,
        'gender': user.gender,
        'status': user.status,
        'vanity': user.vanity,
        'about': user.about,
        'facebook_id': user.facebook_id,
        'image_url': user.image_url,
        'thumb_url': user.thumb_url,
        'thumbnails': {
            size.name: user.thumbnail_url_size(size)
            for size in image_sizes.USER_THUMB_SIZES
        },
        'youtube': user.youtube,
        'twitter': user.twitter,
        'facebook': user.facebook,
        'is_suspended': bool(user.is_suspended),
        'is_deleted': bool(user.is_deleted),
        'is_nuked': bool(user.is_nuked),
        'created': datetime.as_utc(user.created_at).isoformat(),
        'url': user.canonical_url()
    }


def mask(user, acl):
    fields = to_json(user)

    return {
        key: fields[key]
        for key in fields if key in acl['user']
    }

self_whitelist = {
    'user': [
        'id',
        'type',
        'email',
        'first_name',
        'last_name',
        'full_name',
        'gender',
        'status',
        'vanity',
        'about',
        'facebook_id',
        'image_url',
        'thumb_url',
        'thumbnails',
        'youtube',
        'twitter',
        'facebook',
        'is_suspended',
        'is_deleted',
        'is_nuked',
        'created',
        'url'
    ]
}

private_profile_whitelist = {
    'user': [
        'id',
        'type',
        'first_name',
        'last_name',
        'full_name',
        'gender',
        'status',
        'vanity',
        'about',
        'facebook_id',
        'image_url',
        'thumb_url',
        'thumbnails',
        'youtube',
        'twitter',
        'facebook',
        'is_suspended',
        'is_deleted',
        'is_nuked',
        'created',
        'url'
    ]
}

public_profile_whitelist = {
    'user': [
        'id',
        'type',
        'first_name',
        'last_name',
        'full_name',
        'gender',
        'status',
        'vanity',
        'about',
        'facebook_id',
        'image_url',
        'thumb_url',
        'thumbnails',
        'youtube',
        'twitter',
        'facebook',
        'is_suspended',
        'is_deleted',
        'is_nuked',
        'created',
        'url'
    ]
}

patronage_whitelist = {
    "user": [
        'id',
        'type',
        'first_name',
        'last_name',
        'full_name',
        'email',
        'vanity',
        'gender',
        'created',
        'image_url',
        'thumb_url',
        'thumbnails',
        'status',
        'about',
        'url'
    ]
}

bare_bones_whitelist = {
    "user": [
        'full_name',
        'thumb_url',
        'url'
    ]
}
