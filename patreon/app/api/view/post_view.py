from patreon.constants import image_sizes
from patreon.model.type import file_bucket_enum, photo_key_type, PostType
from patreon import util


def to_json(post):
    embed = image = thumbnail = post_file = None
    blurred_thumbnail = None

    if post.link_url or post.link_domain or post.link_subject \
            or post.link_description or post.link_embed:
        embed = {
            'url': post.link_url,
            'provider': post.link_domain,
            'provider_url': post.link_domain,
            'subject': post.link_subject,
            'description': post.link_description,
            'html': post.link_embed,
        }

    if post.image_url or post.image_large_url or post.image_thumb_url \
            or post.image_height or post.image_width:
        image = {
            'thumb_url': post.image_thumb_url,
            'url': post.image_url,
            'large_url': post.image_large_url,
            'height': post.image_height,
            'width': post.image_width
        }

    if post.thumbnail_key:
        thumbnail = {
            image_size.name:
                "https:" + photo_key_type.get_image_url(
                    file_bucket_enum.config_bucket_post, post.thumbnail_key, image_size
                )
            for image_size in image_sizes.POST_THUMB_SIZES
        }
        thumbnail['url'] = thumbnail[image_sizes.POST_THUMB_LARGE_2.name]
        blurred_thumbnail = "https:" + photo_key_type.get_file_url(
            file_bucket_enum.config_bucket_post,
            post.thumbnail_key + '_blurred',
            '.jpeg'
        )

    if post.file_url:
        post_file = {
            'url': post.file_url,
            'name': post.file_name
        }

    return {
        'id': str(post.activity_id),
        'type': 'activity',
        'post_type': PostType.from_id(post.activity_type).name.lower(),
        'title': post.title(),
        'content': post.activity_content,
        'category': post.category,
        'comment_count': post.comment_count,
        'min_cents_pledged_to_view': int(post.min_cents_pledged_to_view),
        'like_count': post.like_count,
        'post_file': post_file,
        'image': image,
        'thumbnail': thumbnail,
        'blurred_thumbnail': blurred_thumbnail,
        'embed': embed,
        'is_paid': bool(post.is_paid),
        'is_creation': bool(post.is_creation),
        'cents_pledged_at_creation': int(post.cents_pledged_at_creation),
        'created_at': util.datetime.as_utc(post.created_at).isoformat(),
        'published_at': util.datetime.as_utc(post.published_at).isoformat() if post.published_at else None,
        'edited_at': util.datetime.as_utc(post.edited_at).isoformat() if post.edited_at else None,
        'deleted_at': util.datetime.as_utc(post.deleted_at).isoformat() if post.deleted_at else None,
        'url': post.canonical_url(),
        'pledge_url': post.pledge_url()
    }


def mask(user, acl):
    fields = to_json(user)

    return {
        key: fields[key]
        for key in fields if key in acl['post']
    }

public_whitelist = {
    'post': [
        'id',
        'type',
        'post_type',
        'title',
        'content',
        'category',
        'comment_count',
        'min_cents_pledged_to_view',
        'like_count',
        'post_file',
        'image',
        'thumbnail',
        'blurred_thumbnail',
        'embed',
        'is_paid',
        'is_creation',
        'cents_pledged_at_creation',
        'created_at',
        'published_at',
        'edited_at',
        'deleted_at',
        'url',
        'pledge_url'
    ]
}, ['attachments', 'user', 'campaign', 'likes', 'presence']

private_whitelist = {
    'post': [
        'id',
        'type',
        'post_type',
        'title',
        'min_cents_pledged_to_view',
        'blurred_thumbnail',
        'is_paid',
        'is_creation',
        'published_at',
        'url',
        'pledge_url'
    ]
}, ['user', 'campaign']
