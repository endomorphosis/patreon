from patreon.app.api import return_as_json, api_app
from patreon.app.api.resources.countries import Countries
from patreon.constants.countries import countries


@api_app.route('/countries')
@return_as_json
def list_countries():
    VERSION = 1

    return Countries(VERSION, {"countries": countries}).as_json_api_document()
