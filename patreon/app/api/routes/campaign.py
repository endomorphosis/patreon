from flask import session, request

from patreon.app.api import api_app, return_as_json, require_login, \
    get_includes, get_data, abort
from patreon.app.api.resources import ApiResourceCollection
from patreon.app.api.resources.campaigns import CampaignResource
from patreon.app.api.resources.users import UsersCollection
from patreon.constants.api import MULTI_GET_MAX
from patreon.model.manager import campaign_mgr, pledge_mgr, user_mgr, \
    homepage_featured_campaign_mgr
from patreon.model.request.route_wrappers import api_require_admin
from patreon.exception.campaign_errors import HasNoCampaign
from patreon.services.caching import memcached
from patreon.util.format import uncomma_ints


@api_app.route('/campaigns/<string:campaign_ids>', methods=['GET'])
@return_as_json
def api_get_campaigns_multi(campaign_ids):
    campaign_ids = uncomma_ints(campaign_ids)[:MULTI_GET_MAX]
    campaigns = campaign_mgr.get_campaigns(campaign_ids)
    return ApiResourceCollection([
        CampaignResource(campaign)
        for campaign in campaigns
    ]).as_json_api_document()


@api_app.route('/campaigns/<int:campaign_id>', methods=['GET'])
@return_as_json
def api_get_campaign(campaign_id):
    campaign = campaign_mgr.get_campaign_or_throw(campaign_id)
    current_user_id = session.user_id

    return CampaignResource(
        campaign,
        includes=get_includes(),
        current_user_id=current_user_id
    ).as_json_api_document()


@api_app.route('/campaigns/by_creator/<int:creator_id>', methods=['GET'])
@return_as_json
def api_get_campaign_by_creator(creator_id):
    # This is future-proof-ish in that it returns an array of campaigns,
    # even though right now it can only ever return 1 or 0 results.

    campaign_id = campaign_mgr.try_get_campaign_id_by_user_id_hack(creator_id)

    if not campaign_id:
        return ApiResourceCollection([]).as_json_api_document()

    campaign = campaign_mgr.get_campaign_or_throw(campaign_id)
    current_user_id = session.user_id

    return ApiResourceCollection([
        CampaignResource(campaign, includes=get_includes(), current_user_id=current_user_id)
    ]).as_json_api_document()


@api_app.route('/campaign/current_campaign', methods=['GET'])
@require_login
@return_as_json
def api_get_current_campaign(current_user_id):
    campaign_id = campaign_mgr.try_get_campaign_id_by_user_id_hack(current_user_id)
    if not campaign_id:
        raise HasNoCampaign()

    campaign = campaign_mgr.get_campaign_or_throw(campaign_id)

    return CampaignResource(campaign).as_json_api_document()


@api_app.route('/campaigns/<int:campaign_id>/patrons', methods=['GET'])
@return_as_json
def api_get_campaign_patrons(campaign_id):
    campaign = campaign_mgr.get_campaign_or_throw(campaign_id)
    patron_ids = pledge_mgr.find_top_patrons_by_creator_id(campaign.creator_id)
    patrons = user_mgr.get_users(patron_ids)

    return UsersCollection(patrons).as_json_api_document()


def campaign_collection(campaigns):
    includes = get_includes()
    return ApiResourceCollection([
        CampaignResource(c, includes=includes, current_user_id=session.user_id)
        for c in campaigns
    ]).as_json_api_document()


@api_app.route('/campaigns/featured', methods=['GET'])
@return_as_json
@memcached()
def get_new_creators():
    return campaign_collection(
        homepage_featured_campaign_mgr.get_temporary_homepage_campaigns()
    )


# ADMIN API ROUTES
@api_app.route('/campaigns/qualified', methods=['GET'])
@return_as_json
@api_require_admin
def get_qualified_campaigns():
    return campaign_collection(
        homepage_featured_campaign_mgr.get_qualified_campaigns()
    )


@api_app.route('/campaigns/approved', methods=['GET'])
@return_as_json
@api_require_admin
def get_approved_creators():
    return campaign_collection(
        homepage_featured_campaign_mgr.get_approved_campaigns()
    )


@api_app.route('/campaigns/disapproved', methods=['GET'])
@return_as_json
@api_require_admin
def get_disapproved_creators():
    return campaign_collection(
        homepage_featured_campaign_mgr.get_disapproved_campaigns()
    )


@api_app.route('/campaigns/<int:campaign_id>/qualified_status', methods=['GET'])
@return_as_json
@api_require_admin
def get_qualified_status(campaign_id):
    campaign = campaign_mgr.get_campaign_or_throw(campaign_id)
    status = homepage_featured_campaign_mgr.get_campaign_status(
        campaign.campaign_id
    )
    if status is not None:
        status = 'approved' if status['approval_status'] == 1 else 'disapproved'
    return { 'data': { 'status': status } }


@api_app.route('/campaigns/<int:campaign_id>/qualified_status', methods=['PUT'])
@return_as_json
@api_require_admin
def set_qualified_status(campaign_id):
    campaign = campaign_mgr.get_campaign_or_throw(campaign_id)

    data = get_data()
    approval_status = data.get('status')
    if approval_status not in ('approved', 'disapproved'):
        abort(400, 'invalid-parameters', 'Invalid parameters in API call')

    current_status = homepage_featured_campaign_mgr.get_campaign_status(
        campaign.campaign_id
    )

    if approval_status == 'approved':
        homepage_featured_campaign_mgr.approve_campaign(campaign)
    else:
        homepage_featured_campaign_mgr.disapprove_campaign(campaign)

    if current_status is None:
        options = { 'created': request.base_url }
    else:
        options = {}

    return { 'data': { 'status': approval_status } }, options


@api_app.route('/campaigns/<int:campaign_id>/qualified_status', methods=['DELETE'])
@return_as_json
@api_require_admin
def unset_qualified_status(campaign_id):
    homepage_featured_campaign_mgr.reset_campaign_status(campaign_id)
    return None, { 'deleted': request.base_url }
