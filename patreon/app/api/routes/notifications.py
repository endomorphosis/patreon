from flask import request
import pytz
import datetime

from patreon.app.api import api_app, return_as_json, require_login
from patreon.app.api.resources.notifications import NotificationCardsCollection, DailyCommentsNotificationResource, \
    MonthlyPledgesNotificationResource, DailyLikesNotificationResource
from patreon.model.notifications import DailyCommentsNotification, MonthlyPledgesNotification, DailyLikesNotification
from patreon.model.manager import user_mgr


def timezone_from_request():
    tz = pytz.timezone('US/Pacific')
    timezone_string = request.args.get('timezone')
    if timezone_string:
        try:
            tz = pytz.timezone(timezone_string)
        except pytz.exceptions.UnknownTimeZoneError:
            colon_stripped = ''.join(timezone_string.split(':'))
            tz = datetime.datetime.strptime(colon_stripped, '%z').tzinfo
    return tz


def get_count_with_default(param_name, default):
    param = request.args.get(param_name)
    if not param:
        return default
    param = int(param)
    if param <= 0:
        return default
    return param


@api_app.route('/notifications', methods=['GET'])
@require_login
@return_as_json
def notifications(current_user_id):
    current_user = user_mgr.get_user_or_throw(current_user_id)
    tz = timezone_from_request()

    page_cursor = request.args.get('page[cursor]')
    per_page = get_count_with_default('page[count]', 10)
    items_per_card = get_count_with_default('page[items_count]', 5)

    cards = NotificationCardsCollection(current_user, tz, per_page, page_cursor, items_per_card)
    return cards.as_json_api_document()

def notification_deep_linking(current_user_id):
    current_user = user_mgr.get_user_or_throw(current_user_id)
    tz = timezone_from_request()
    page_cursor = request.args.get('page[cursor]')
    per_page = get_count_with_default('page[count]', 5)
    return current_user, tz, page_cursor, per_page


@api_app.route('/notifications/<string:notification_id>/links/comments', methods=['GET'])
@require_login
@return_as_json
def notification_comments(notification_id, current_user_id):
    current_user, tz, page_cursor, per_page = notification_deep_linking(current_user_id)
    card = DailyCommentsNotification.find_by_id(notification_id, current_user, tz, per_page, page_cursor)
    resource = DailyCommentsNotificationResource(card)
    return resource.comment_resource_collection().as_json_api_relationship_document()


@api_app.route('/notifications/<string:notification_id>/links/likes-notifications', methods=['GET'])
@require_login
@return_as_json
def notification_likes(notification_id, current_user_id):
    current_user, tz, page_cursor, per_page = notification_deep_linking(current_user_id)
    card = DailyLikesNotification.find_by_id(notification_id, current_user, tz, per_page, page_cursor)
    resource = DailyLikesNotificationResource(card)
    return resource.like_resource_collection().as_json_api_relationship_document()


@api_app.route('/notifications/<string:notification_id>/links/pledge-notifications', methods=['GET'])
@require_login
@return_as_json
def notification_pledges(notification_id, current_user_id):
    current_user, tz, page_cursor, per_page = notification_deep_linking(current_user_id)
    card = MonthlyPledgesNotification.find_by_id(notification_id, current_user, tz, per_page, page_cursor)
    resource = MonthlyPledgesNotificationResource(card)
    return resource.pledges_resource_collection().as_json_api_relationship_document()
