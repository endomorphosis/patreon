from patreon.app.api import api_app
from documentation.src.generate_endpoints_json import generate_endpoints_json
from documentation.src.generate_resources_json import generate_resources_json
from documentation.src.generate_html_doc import generate_html_doc


@api_app.route('/')
def api_docs():
    return generate_html_doc(
        endpoints_json=generate_endpoints_json(),
        resources_json=generate_resources_json()
    )
