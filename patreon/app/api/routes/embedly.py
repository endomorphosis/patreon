from patreon.app.api import api_app, return_as_json, get_data
from patreon.exception.post_errors import BadEmbed
from patreon.services import embedder


@api_app.route('/embed', methods=['POST'])
@return_as_json
def get_embedly_data():
    data = get_data()

    url = data.get('url')
    embed_json = embedder.get_embed(url)
    if not embed_json:
        raise BadEmbed(url)

    # TODO: json api version? what does get_embed return?
    return { 'data': embed_json }
