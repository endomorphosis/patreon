from flask import request
from patreon.app.api import api_app, return_as_json
from patreon.model.manager import activity_mgr, campaign_mgr
from patreon.util import jsondate


def _share_row_to_public_share(share):
    """Convert a 'share' row into publicly accessible information."""
    is_private = share['Privacy'] > 0
    if is_private:
        return {
            'type': 'share',
            'id': str(share['HID']),
            'creator': str(share['UID']),
            'created': int(share['Created'].timestamp()),
            'is_private': is_private,
        }
    else:
        return {
            'type': 'share',
            'id': str(share['HID']),
            'creator': str(share['UID']),
            'title': share['Title'],
            'content': share['Content'],
            'description': share['Description'],
            'embed': share['Embed'],
            'created': jsondate.coerce(share['Created']),
            'likes': int(share['LikeCount']),
            'comments': int(share['CommentCount']),
            'image_url': share['ImageUrl'],
            'image_large_url': share['ImageLargeUrl'],
            'image_thumb_url': share['ImageThumbUrl'],
            'image_height': int(share['ImageHeight'] or 0),
            'image_width': int(share['ImageWidth'] or 0),
            'url': share['Url'],
            'patron_count': int(share['PatronCount']),
            'is_private': is_private,
        }


@api_app.route('/shares')
@return_as_json
def api_shares():
    # TODO(postport): This is a one-off, and is deprecated in favor of `GET /user/<int:creator_user_id>/campaign/posts`
    # (that is, until that API is deprecated because users can have more than one campaign)
    creator_id = int(request.args.get('creator'))
    if not creator_id:
        raise NotImplemented("/shares API call requires a creator id")
    campaign_id = campaign_mgr.try_get_campaign_id_by_user_id_hack(creator_id)

    page = int(request.args.get('page', 1))

    # TODO(implement): hide_private should depend on logged in user.
    posts = activity_mgr.find_creator_posts_by_campaign_id_paginated(
        campaign_id, page
    )
    shares = [ _share_row_to_public_share(row) for row in posts ]
    return { 'data': shares }
