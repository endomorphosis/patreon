"""
All defined routes for API endpoints.

TODO(postport): Make this more modular and integrate with SQLAlchemy
This is just to support the single endpoint for Ryan Leslie / testing
for mobile / Angular.
"""

from . import api_docs
from . import xdomain
from . import attachment
from . import actions
from . import auth
from . import campaign
from . import card
from . import category
from . import comment
from . import follow
from . import notifications
from . import patron
from . import pledge
from . import post
from . import presence
from . import resend_verify_email
from . import share
from . import user
from . import stats
from . import reward
from . import embedly
from . import unread_status
from . import settings
from . import stream
from . import vat
from . import countries
from .admin import admin_payments
