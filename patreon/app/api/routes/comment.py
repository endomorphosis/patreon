from werkzeug.utils import secure_filename
from flask import request, session

from patreon.app.api import abort, api_app, return_as_json, require_login, \
    get_sort, get_files, get_data, get_json_api_version
from patreon.app.api.resources import ApiResourceCollection, ApiResource
from patreon.app.api.resources.comments import CommentRepliesCollection, \
    CommentResource, PostCommentsCollection, PostCommentsCollectionPaged, \
    CommentVoteResource
from patreon.exception.post_errors import PostViewForbidden
from patreon.model.manager import post_mgr, user_mgr, comment_mgr, feature_flag
from patreon.model.type import file_bucket_enum, photo_key_type, TemporaryUpload
from patreon.services import bonfire, file_helper, uploader
from patreon.util import random
from patreon.util.image_processing import converter, rotator


@api_app.route('/posts/<int:post_id>/comments/<int:comment_id>', methods=['GET'])
@return_as_json
def get_comment(post_id, comment_id):
    current_user_id = session.user_id
    post = post_mgr.get_post_or_throw(post_id)

    if not post_mgr.can_view(current_user_id, post):
        raise PostViewForbidden(post_id)

    comment = comment_mgr.get_comment_or_throw(comment_id)

    return CommentResource(comment).as_json_api_document()


# @api_app.route('/comment/<int:comment_id>', methods=['GET'])
# @return_as_json
# def comment(comment_id):
# # TODO: extract ACL from replies_to_comment, then just return the comment

def prepare_images(files, images):
    if not images:
        return None
    files_dict = {
        file.filename: file_helper.file_stream_to_tempfile(file)
        for file in files
    }

    return [
        {
            'tempfile_name': files_dict[filename],
            'filename': filename,
            'file_key': file_key
        }
        for file_key, filename in images.items()
    ]


def inline_images(html, images):
    for image in images:
        url = upload_comment_image(image['tempfile_name'], image['filename'])
        if url:
            img_tag = "<img src=\"{}\">".format(url)
            html = html.replace(image['file_key'], img_tag)

    return html


def upload_comment_image(temp_filename, source_filename):
    source_filename = secure_filename(source_filename)
    source_extension = file_helper.get_extension(source_filename)
    file_key = random.random_key()

    with temp_filename as temp_filename:
        if converter.is_image_file(temp_filename):
            destination_format = converter.convert_format(source_extension)
            rotator.correct_file_rotation(temp_filename, destination_format)
            destination_extension = '.' + destination_format

            uploader.upload_file(TemporaryUpload(
                temp_filename,
                file_key + destination_extension,
                file_bucket_enum.config_bucket_comment
            ))

            return 'https:' + photo_key_type.get_file_url(
                file_bucket_enum.config_bucket_comment,
                file_key,
                destination_extension
            )

    return None


@api_app.route('/images_comments', methods=['POST'])
@require_login
@return_as_json
def post_images_comment(current_user_id):
    current_user = user_mgr.get_user_or_throw(current_user_id)

    data = get_data()

    files = get_files().values()
    file_dicts = prepare_images(files, data['images'])

    data['body'] = inline_images(data['body'], file_dicts)
    if get_json_api_version() == ApiResource.JSON_API_STABLE:
        data['attributes']['body'] = data['body']

    comment_resource = CommentResource.new_from_json_api_document(
        {'data': data}, current_user=current_user
    )
    comment_json = comment_resource.as_json_api_document()

    return comment_json, {'created': comment_resource.resource_url()}


@api_app.route('/comment', methods=['POST'])
@api_app.route('/comments', methods=['POST'])
@require_login
@return_as_json
def post_comment(current_user_id):
    # TODO: require correct mime type
    # TODO: write a frisby test for this
    current_user = user_mgr.get_user_or_throw(current_user_id)

    comment_resource = CommentResource.new_from_json_api_document(
        request.get_json(force=True),
        current_user=current_user
    )
    comment_json = comment_resource.as_json_api_document()

    return comment_json, {'created': comment_resource.resource_url()}


@api_app.route('/comment/<int:comment_id>', methods=['PATCH'])
@api_app.route('/comments/<int:comment_id>', methods=['PATCH'])
@require_login
@return_as_json
def patch_comment(comment_id, current_user_id):
    # TODO: require correct mime type
    # TODO: write a frisby test for this
    current_user = user_mgr.get_user_or_throw(current_user_id)

    # TODO: comment_id should match id in data
    comment_resource = CommentResource.update_from_json_api_document(
        request.get_json(force=True),
        current_user=current_user
    )
    if not comment_resource:
        abort(404)

    return comment_resource.as_json_api_document()


@api_app.route('/comment/<int:comment_id>', methods=['DELETE'])
@api_app.route('/comments/<int:comment_id>', methods=['DELETE'])
@require_login
@return_as_json
def delete_comment(comment_id, current_user_id):
    comment_mgr.delete_comment_as_user(comment_id, current_user_id)
    return None, {'deleted': None}


@api_app.route('/comment/<int:comment_id>/replies', methods=['GET'])
@api_app.route('/comments/<int:comment_id>/replies', methods=['GET'])
@return_as_json
def replies_to_comment(comment_id):
    current_user_id = session.user_id
    comment = comment_mgr.get_comment_or_throw(comment_id)

    # Comments on deleted posts should 404
    post = post_mgr.get_post_or_throw(comment.post_id)
    if not post_mgr.can_view(current_user_id, post):
        raise PostViewForbidden(post.activity_id)

    return CommentRepliesCollection(comment).as_json_api_document()


@api_app.route('/comment/<int:comment_id>/votes', methods=['GET'])
@api_app.route('/comments/<int:comment_id>/votes', methods=['GET'])
@return_as_json
def votes_on_comment(comment_id):
    current_user_id = session.user_id
    comment = comment_mgr.get_comment_or_throw(comment_id)

    # Comments on deleted posts should 404
    post = post_mgr.get_post_or_throw(comment.post_id)
    if not post_mgr.can_view(current_user_id, post):
        raise PostViewForbidden(post.activity_id)

    comment_votes = comment_mgr.get_comment_votes_by_comment_id(comment_id)

    return ApiResourceCollection([
        CommentVoteResource(comment_vote)
        for comment_vote in comment_votes
    ]).as_json_api_document()


@api_app.route('/comment/<int:comment_id>/votes', methods=['POST'])
@api_app.route('/comments/<int:comment_id>/votes', methods=['POST'])
@require_login
@return_as_json
def create_or_edit_votes_on_comment(comment_id, current_user_id):
    comment = comment_mgr.get_comment_or_throw(comment_id)

    # Comments on deleted posts should 404
    post = post_mgr.get_post_or_throw(comment.post_id)
    if not post_mgr.can_view(current_user_id, post):
        raise PostViewForbidden(post.activity_id)

    insertable = CommentVoteResource.insertable_from_json_api_document(request.get_json(force=True))
    vote = comment_mgr.add_or_update_comment_vote(
        comment_id=comment_id,
        user_id=current_user_id,
        vote=insertable['vote']
    )

    # Ping the real-time chat server
    resource_with_post = CommentVoteResource(vote, includes=['user', 'comment', 'post'])
    post = post_mgr.get_post(resource_with_post.model.activity_id)
    if post.user_id == post.creator_id \
            and feature_flag.is_enabled('real_time_comments', group_id=request.ab_test_group_id, user_id=post.user_id):
        bonfire.post('/comment_vote', resource_with_post.as_json_api_document(version=ApiResource.JSON_API_STABLE))

    return CommentVoteResource(vote).as_json_api_document()


@api_app.route('/posts/<int:post_id>/comments', methods=['GET'])
@return_as_json
def comments_on_post(post_id):
    current_user_id = session.user_id

    post = post_mgr.get_post_or_throw(post_id)
    if not post_mgr.can_view(current_user_id, post):
        raise PostViewForbidden(post_id)

    page_cursor = request.args.get('page[cursor]')
    page_offset = request.args.get('page[offset]')
    sort = get_sort()
    if sort is not None and 'vote_sum' in sort and page_offset is None:
        page_offset = 0
    elif page_offset is not None:
        page_offset = int(page_offset)
    per_page = request.args.get('page[count]')

    if page_cursor or per_page:
        comments = PostCommentsCollectionPaged(
            post,
            per_page=per_page,
            cursor=page_cursor,
            offset=page_offset,
            sort=sort
        )
    else:
        # Return all comments if no page parameters are set.
        # (Backwards compatibility)
        comments = PostCommentsCollection(post, sort=get_sort())
    return comments.as_json_api_document()
