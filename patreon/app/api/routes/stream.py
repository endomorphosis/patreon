from flask import request

from patreon import globalvars
from patreon.app.api import api_app, return_as_json, require_login
from patreon.app.api.resources.posts import PostsCollection
from patreon.app.api.view import post_view
from patreon.model.manager import post_mgr, follow_mgr, campaign_mgr, presence_mgr, pledge_mgr


def stream_creators_for_user_id(user_id):
    creator_ids = [pledge.creator_id for pledge in pledge_mgr.find_by_patron_id(user_id)]
    creator_ids += [follow.followed_id for follow in follow_mgr.find_by_follower_id(user_id)]
    return [creator_id for creator_id in creator_ids if creator_id != user_id]


@api_app.route('/stream', methods=['GET'])
@require_login
@return_as_json
def api_get_followed_posts(current_user_id):
    creator_ids = stream_creators_for_user_id(current_user_id)
    campaign_ids = campaign_mgr.get_all_campaign_ids_for_user_ids(creator_ids)

    # TODO: remove once all clients are done sending up this filter
    is_creation_filter = request.args.get('filter[is_creation]') or None
    if is_creation_filter is not None:
        is_creation_filter = is_creation_filter in ['true', 'True', '1', True]

    is_by_creator_filter = request.args.get('filter[is_by_creator]') or None
    if is_by_creator_filter is not None:
        is_by_creator_filter = is_by_creator_filter in ['true', 'True', '1', True]

    page_cursor = request.args.get('page[cursor]') or None
    per_page = request.args.get('page[count]') or globalvars.get('SHARES_PER_PAGE')  # 'infinity' is a valid value

    posts = post_mgr.get_posts_by_campaign_ids_paginated(
        campaign_ids,
        cursor=page_cursor,
        per_page=per_page,
        is_creation=is_creation_filter,
        is_by_creator=is_by_creator_filter,
        visible_to_user_id=current_user_id,
        allow_community=False
    )

    return PostsCollection(
        posts,
        masks=[post_view.public_whitelist for _ in posts]
    ).as_json_api_document()


@api_app.route('/stream/live', methods=['GET'])
@require_login
@return_as_json
def api_get_live_posts(current_user_id):
    creator_ids = stream_creators_for_user_id(current_user_id)
    presences = presence_mgr.find_live_by_user_ids(creator_ids)
    post_ids = [presence.activity_id()
                for presence in presences
                if presence.activity_id()]
    live_posts = post_mgr.get_posts_visible_to_user(post_ids, current_user_id)

    return PostsCollection(
        live_posts,
        masks=[post_view.public_whitelist for _ in live_posts],
        extra_includes=['presence']
    ).as_json_api_document()
