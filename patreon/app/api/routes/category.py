from patreon.app.api import api_app, return_as_json, get_includes
from patreon.app.api.resources.categories import CategoryResource, UserCategoriesCollection
from patreon.model.manager import category_mgr


@api_app.route('/categories', methods=['GET'])
@return_as_json
def get_categories():
    return UserCategoriesCollection(includes=get_includes()).as_json_api_document()


@api_app.route('/categories/<int:category_id>', methods=['GET'])
@return_as_json
def featured_content(category_id):
    category = category_mgr.get_category(category_id)
    return CategoryResource(category, includes=get_includes()).as_json_api_document()
