import datetime

from flask import request
import dateutil.parser
from patreon.app.api import api_app, return_as_json, require_login
from patreon.app.api.resources import ApiResourceCollection
from patreon.app.api.resources.unread_status import UnreadStatusResource
from patreon.model.manager import user_mgr, unread_status_mgr
from patreon.model.type import UnreadStatusType


@api_app.route('/unread_status/<string:unread_key>', methods=['POST'])
@require_login
@return_as_json
def mark_unread_key_read(unread_key, current_user_id):
    current_user = user_mgr.get_user_or_throw(current_user_id)
    read_timestamp = datetime.datetime.now()
    if request.json and request.json.get('data'):
        data = request.json.get('data')
        user_read_timestamp = data.get('read_timestamp')
        if user_read_timestamp:
            read_timestamp = dateutil.parser.parse(user_read_timestamp)
    unread_status_mgr.mark_as_read(current_user.user_id, unread_key, read_timestamp)
    return {
        'data': { 'read_timestamp': read_timestamp.isoformat() }
    }


@api_app.route('/unread_status', methods=['GET'])
@require_login
@return_as_json
def get_unread_statuses(current_user_id):
    statuses = [
        unread_status_mgr.get_or_create(current_user_id, unread_status_type.name)
        for unread_status_type in UnreadStatusType
    ]
    resource = ApiResourceCollection([
        UnreadStatusResource(status)
        for status in statuses if status is not None
    ])
    return resource.as_json_api_document()


@api_app.route('/unread_status/<string:unread_key>', methods=['GET'])
@require_login
@return_as_json
def get_unread_status(current_user_id, unread_key):
    status = unread_status_mgr.get_or_create(current_user_id, unread_key)
    resource = UnreadStatusResource(status)
    return resource.as_json_api_document()
