from flask import request
from patreon.app.api import api_app, get_json_api_version, return_as_json, require_login
from patreon.app.api.resources.posted_document import PostedDocument
from patreon.app.api.resources.pledges import PledgeResource
from patreon.exception.payment_errors import PledgeCreationError, PaymentException
from patreon.exception.pledge_errors import BigMoneyRequired, PledgeNotExist
from patreon.model.manager import campaign_mgr, vat_mgr, complete_pledge_mgr, \
    payment_event_mgr, pledge_helper, payment_mgr, new_pledge_mgr, pledge_mgr, \
    user_mgr, goal_mgr
from patreon.model.manager.queue_mgr import enqueue_campaign_for_update
from patreon.services import analytics
from patreon.services.analytics_events import PledgeEvent
from patreon.services.mail import send_template_email
from patreon.services.mail.templates import Receipt, Patron, BigMoney
from patreon.util.unsorted import get_int


def get_ip():
    return request.request_ip()


@api_app.route('/pledges', methods=['POST'])
@require_login
@return_as_json
def create_pledge(current_user_id):
    # TODO: invent a way to use BluePrint for destructuring posted data

    posted_pledge = PostedDocument(request.json, version=get_json_api_version()).data()
    posted_pledge.assert_type("pledge")

    patron_id = current_user_id
    patron = user_mgr.get(patron_id)

    creator, campaign = pledge_helper.creator_and_campaign_from_campaign_or_creator_relationship(posted_pledge)

    reward_id = pledge_helper.reward_id_from_relationship(posted_pledge)

    card_id = pledge_helper.card_id_from_relationship(posted_pledge)
    card = pledge_helper.get_valid_card_for_user_or_raise_exception(card_id, patron_id)

    address = pledge_helper.address_from_relationship(posted_pledge)
    if reward_id:
        pledge_helper.raise_unless_address_is_sufficient_for_reward_tier_id(address, reward_id)

    patron_pays_fees = posted_pledge.attribute("patron_pays_fees")
    vat_country = posted_pledge.attribute("vat_country")
    pledge_amount = posted_pledge.attribute("amount_cents")
    pledge_cap = posted_pledge.attribute('pledge_cap_cents')

    pledge_helper.check_nsfw_does_not_use_paypal(campaign.campaign_id, card_id)

    # This check has to run before Big Money.
    # (TODO: refactor Big Money flow into pledge_mgr.create_or_update_pledge)
    vat_mgr.check_vat_country_matches_geoip(
        vat_country=vat_country,
        ip_address=get_ip()
    )

    pledge_helper.check_pledge_cap_amount_is_multiple_of_pledge(
        pledge_amount=pledge_amount,
        pledge_cap=pledge_cap
    )

    vat_amount = vat_mgr.calculate_vat_on_amount_for_country(
        pledge_amount, vat_country
    )

    existing_pledge = new_pledge_mgr.get_pledge_by_patron_and_campaign(
        patron_id=patron_id, campaign_id=campaign.campaign_id
    )
    existing_pledge_cents = existing_pledge.amount_cents if existing_pledge else None

    current_reward_tier = None
    if existing_pledge:
        current_reward_tier = existing_pledge.reward_tier_id
    pledge_helper.check_reward_tier(patron_id, pledge_amount, reward_id, current_reward_tier)

    charge_immediately = False
    is_big_money = False
    if pledge_helper.is_big_money(posted_pledge) and not card.is_verified:
        charge_immediately = True
        is_big_money = True
        escrow_amount = pledge_amount + vat_amount
        if not pledge_helper.big_money_charge_accepted(posted_pledge):
            raise BigMoneyRequired()
        payment_mgr.charge_big_money(
            user_id=patron_id,
            payment_instrument_token=card_id,
            amount_cents=escrow_amount
        )

    try:
        # Create or update pledge
        complete_pledge_mgr.create_or_update_pledge(
            patron_id=patron_id,
            creator_id=creator.user_id,
            amount=pledge_amount,
            pledge_cap=pledge_cap,
            reward_id=reward_id,
            card_id=card_id,
            address=address,
            vat_country=vat_country,
            ip_address=get_ip(),
            patron_pays_pledge_fees=patron_pays_fees
        )
    except PaymentException as exception:
        new_exception = PledgeCreationError(exception)
        payment_event_mgr.add_exception_event(new_exception)
        raise new_exception

    pledge = complete_pledge_mgr.get_pledge_by_patron_and_campaign_or_creator(
        patron_id=patron_id, creator_id=creator.user_id
    ).legacy

    send_template_email(
        Receipt(
            patron, creator, campaign, pledge,
            is_edit=(existing_pledge is not None),
            is_verified=charge_immediately
        )
    )

    if charge_immediately and is_big_money:
        send_template_email(BigMoney(patron))

    if campaign.campaigns_users[0].should_email_on_new_patron:
        if not existing_pledge:
            send_template_email(
                Patron(creator, patron, pledge_amount)
            )
        elif existing_pledge_cents != pledge_amount:
            send_template_email(
                Patron(creator, patron, pledge_amount, existing_pledge_cents)
            )

    goal_mgr.check_if_goal_reached(campaign.campaign_id)

    action = 'edit' if existing_pledge else 'create'
    payment_event_mgr.add_pledge_event(
        patron_id, campaign.campaign_id, pledge_amount,
        pledge_cap, reward_id,
        vat_country, vat_amount, action
    )

    new_patron_count = pledge_mgr.get_patron_count_by_creator_id(creator.user_id)
    new_pledge_total = get_int(pledge_mgr.get_pledge_total_by_creator_id(creator.user_id))

    analytics.log_patreon_event(
        PledgeEvent.EDIT_EVENT if existing_pledge else PledgeEvent.ADD_EVENT,
        event_properties={
            'creator_id': creator.user_id,
            'campaign_id': campaign.campaign_id,
            'amount changed to': pledge_amount,
            'amount changed from': existing_pledge_cents,
            'amount difference': pledge_amount - (existing_pledge_cents or 0),
            'patrons changed to': new_patron_count,
            'pledge total changed to': new_pledge_total,
            'did immediately charge': charge_immediately,
            'max cap': pledge_cap,
            'reward id': reward_id,
        }
    )
    enqueue_campaign_for_update(campaign.campaign_id)
    pledge_resource = PledgeResource(pledge)
    return pledge_resource.as_json_api_document(), {'created': pledge_resource.resource_url()}


@api_app.route('/pledges/<int:campaign_id>', methods=['DELETE'])
@require_login
@return_as_json
def delete_pledge(campaign_id, current_user_id):
    existing_pledge = new_pledge_mgr.get_pledge_by_patron_and_campaign(
        patron_id=current_user_id, campaign_id=campaign_id
    )
    if not existing_pledge:
        raise PledgeNotExist

    complete_pledge_mgr.delete_pledge(patron_id=current_user_id, campaign_id=campaign_id)

    vat_amount = None
    vat_country = None
    if existing_pledge.pledge_vat_location:
        vat_amount = vat_mgr.calculate_vat_on_amount_for_country(
            existing_pledge.amount_cents, existing_pledge.pledge_vat_location.geolocation_country
        )
        vat_country = existing_pledge.pledge_vat_location.geolocation_country

    payment_event_mgr.add_pledge_event(
        current_user_id, campaign_id, existing_pledge.amount_cents,
        existing_pledge.pledge_cap_amount_cents, existing_pledge.reward_tier_id,
        vat_country, vat_amount, 'delete'
    )

    creator_id = campaign_mgr.try_get_user_id_by_campaign_id_hack(campaign_id)

    new_patron_count = pledge_mgr.get_patron_count_by_creator_id(creator_id)

    analytics.log_patreon_event(
        PledgeEvent.DELETE_EVENT,
        event_properties={
            'creator_id': creator_id,
            'campaign_id': campaign_id,
            'amount changed to': 0,
            'amount changed from': existing_pledge.amount_cents,
            'amount difference': 0 - existing_pledge.amount_cents,
            'patrons changed to': new_patron_count
        }
    )

    return None, {'deleted': None}
