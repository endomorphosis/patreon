from flask import request

from patreon.app.api import api_app, return_as_json, get_json_api_version, require_login
from patreon.app.api.resources.follows import FollowResource
from patreon.app.api.resources.posted_document import PostedDocument
from patreon.exception.follow_errors import FollowedRelationshipMissing, FollowCreationForbidden, FollowAlreadyExists, \
    FollowCreationFailed, FollowDoesNotYetExist, FollowDeletionForbidden
from patreon.model.manager import follow_mgr
from patreon.services.logging.unsorted import log_state_change


@api_app.route('/follow', methods=['POST'])
@api_app.route('/follows', methods=['POST'])
@require_login
@return_as_json
def create_follow(current_user_id):
    posted_follow = PostedDocument(request.json, version=get_json_api_version()).data()
    posted_follow.assert_type("follow")

    follower_relationship = posted_follow.relationship_id('follower')
    if follower_relationship is None:
        follower_id = current_user_id
    else:
        follower_id = str(follower_relationship.assert_type("user").resource_id())
        if follower_id != str(current_user_id):
            raise FollowCreationForbidden()

    followed_relationship = posted_follow.relationship_id('followed')
    if followed_relationship is None:
        raise FollowedRelationshipMissing
    followed_id = str(followed_relationship.assert_type("user").resource_id())

    existing_follow = follow_mgr.get(follower_id=follower_id, followed_id=followed_id)
    if existing_follow is not None:
        raise FollowAlreadyExists

    result = follow_mgr.insert(follower_id, followed_id)
    log_state_change('follows', follower_id, 'create', '/follow')

    if result:
        return FollowResource(follow_mgr.get(follower_id, followed_id)).as_json_api_document()
    else:
        raise FollowCreationFailed


@api_app.route('/follow/<string:follow_id>', methods=['DELETE'])
@api_app.route('/follows/<string:follow_id>', methods=['DELETE'])
@require_login
@return_as_json
def delete_follow(follow_id, current_user_id):
    follower_id, followed_id = follow_mgr.follower_and_followed_ids_for_follow_id(follow_id)
    if str(follower_id) != str(current_user_id):
        raise FollowDeletionForbidden
    existing_follow = follow_mgr.get(follower_id=follower_id, followed_id=followed_id)
    if existing_follow is None:
        raise FollowDoesNotYetExist
    follow_resource = FollowResource(existing_follow)
    response = follow_resource.as_json_api_document()
    url = follow_resource.resource_url()
    follow_mgr.delete(follower_id=follower_id, followed_id=followed_id)
    return response, {'deleted': url}
