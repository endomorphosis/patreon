import dateutil.parser
from flask import request

from patreon.app.api import api_app, return_as_json, require_login, \
    get_data, abort, get_json_api_version, ApiResource
from patreon.app.api.resources.actions import LikeResource, PostLikesCollection
from patreon.model.manager import action_mgr, post_mgr


def parse_like(data):
    type_ = data.get('type')
    if type_ != 'like':
        abort(400, 'invalid-parameters', "Invalid parameters in API call.")

    json_api_version = get_json_api_version()
    if json_api_version == ApiResource.JSON_API_STABLE:
        post_link = data['relationships']['post']['data']
    else:
        post_link = data['links']['post']['linkage']
    post_type = post_link['type']
    if post_type != 'post':
        abort(400, 'invalid-parameters', "Invalid parameters in API call.")

    return post_link['id']


@api_app.route('/likes', methods=['POST'])
@require_login
@return_as_json
def post_like(current_user_id):
    try:
        data = get_data()
        post_id = parse_like(data)
        existing_like = action_mgr.get_like(user_id=current_user_id, activity_id=post_id)
        if existing_like is not None:
            abort(409, 'existing-resource', "This user already likes that post.")
        like = action_mgr.execute_like(user_id=current_user_id, activity_id=post_id, add_like=True)
        like_resource = LikeResource(like)
        return like_resource.as_json_api_document(), {'created': like_resource.resource_url()}

    except KeyError:
        abort(400, 'invalid-parameters', "Invalid parameters in API call.")


@api_app.route('/likes', methods=['DELETE'])
@require_login
@return_as_json
def delete_like(current_user_id):
    try:
        data = get_data()
        post_id = parse_like(data)

        existing_like = action_mgr.get_like(user_id=current_user_id, activity_id=post_id)
        if existing_like is None:
            abort(409, 'non-existent-resource', "This user does not yet like that post.")

        like_resource = LikeResource(existing_like)
        response = like_resource.as_json_api_document()
        url = like_resource.resource_url()
        action_mgr.execute_like(user_id=current_user_id, activity_id=post_id, add_like=False)
        return response, {'deleted': url}

    except KeyError:
        abort(400, 'invalid-parameters', "Invalid parameters in API call.")


@api_app.route('/posts/<int:post_id>/likes', methods=['GET'])
@require_login
@return_as_json
def likes_on_post(post_id, current_user_id):
    post = post_mgr.get_post_or_throw(post_id)
    if not post_mgr.can_view(current_user_id, post):
        abort(403)
    per_page = request.args.get('page[count]')
    page_cursor = request.args.get('page[cursor]')
    if page_cursor is not None:
        page_cursor = dateutil.parser.parse(page_cursor)
    if per_page is not None:
        per_page = int(per_page)
    return PostLikesCollection(post, cursor_date=page_cursor, page_size=per_page).as_json_api_document()
