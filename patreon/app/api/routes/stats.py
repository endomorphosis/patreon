from patreon.model.table import Comment
from patreon.app.api import api_app, return_as_json, get_json_api_version, ApiResource


@api_app.route('/stats/comment_spam', methods=['GET'])
@return_as_json
def comment_spam():
    json_ = {
        'data': {
            'type': 'statistic',
            'id': 'comment_spam'
        }
    }
    attributes = {'comment_spam_count': Comment.get_spam_count()}
    if get_json_api_version() == ApiResource.JSON_API_STABLE:
        json_['data']['attributes'] = attributes
    else:
        json_['data'].update(attributes)
    return json_
