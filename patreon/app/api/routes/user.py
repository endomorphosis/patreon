from flask import request, session

import patreon
from patreon.app.api import api_app, return_as_json, require_login, abort, \
    get_includes, get_data, get_json_api_version, ApiResource, get_filters
from patreon.app.api.resources.users import UserResource, UsersCollection
from patreon.exception.auth_errors import UserAlreadyExists, BadCaptcha
from patreon.exception.user_errors import PasswordTooShort, UserNotFound
from patreon.model.manager import auth_mgr, user_mgr, pledge_mgr, session_mgr
from patreon.model.request.session import login_user
from patreon.services import analytics, facebook
from patreon.services.analytics_events import SignupEvent
from patreon.services.logging.unsorted import log_state_change, log_json
from patreon.util.unsorted import split_name
from patreon.services import captcha


def reset_session_id():
    sid = session_mgr.get_new_session_id()

    @patreon.model.request.call_after_request
    def force_session_cookie(response):
        api_app.session_interface.set_session_cookie(response, sid)
        return response

    session.session_id = sid


@api_app.route('/user', methods=['POST'])
@return_as_json
def create_user():
    # TODO(postport): This function is a little monolithic. Maybe cut it down.
    # TODO(port): Make this rate limited

    # TODO: Fix this after the hack is done.
    if patreon.pstore.get('disable_empty_user_create') and request.headers.get('Referer', '').strip() == '':
        abort(403)

    try:
        json_api_version = get_json_api_version(default_version=ApiResource.JSON_API_RC2)
        attributes = request.get_json()['data']
        if json_api_version == ApiResource.JSON_API_STABLE:
            attributes = attributes['attributes']

        if 'fb_access_token' in attributes:
            email = attributes.get('email')
            password = None  # Not needed for FB signup
            access_token = attributes['fb_access_token']
            me = facebook.me(access_token)
            first_name = me['first_name']
            last_name = me['last_name']
            facebook_id = me['id']

            # Check if facebook user already exists
            # if it does, then log them in and return the
            # current user.
            maybe_user = user_mgr.get_unique(facebook_id=facebook_id)
            if maybe_user:
                login_user(maybe_user, api_app)
                return UserResource(user_mgr.get(session.user_id)).as_json_api_document(json_api_version)

            # If it does not and email is not set, then
            # return and ask for email address.
            if not email:
                return {
                    'errors': [{
                        'id': 'email-required-for-fb-signup',
                        'title': 'Email is required for Facebook signup',
                    }]
                }
                # If the code reaches this point, we should have a
                # full name, an email, and a password.
        else:
            full_name = attributes['name']
            password = attributes['password']
            email = attributes['email']
            first_name, _, last_name = full_name.partition(' ')
            facebook_id = None

            maybe_user_id = auth_mgr.get_user_by_email_and_password(email, password)
            if maybe_user_id:
                login_user(user_mgr.get(maybe_user_id), api_app)
                return UserResource(user_mgr.get(session.user_id)).as_json_api_document(json_api_version)

        if not ('fb_access_token' in attributes) and len(password) < 6:
            raise PasswordTooShort()

        # If we didn't automatically sign the user in via the
        # authentication methods above, then we know we have to have
        # enough information to log the user in.
        should_check_captcha = not (email and
                                    email.startswith('no-reply+test-') and
                                    email.endswith('@patreon.com')
                                    ) \
                               and captcha.is_enabled('signup')

        captcha_response_is_invalid = not captcha.captcha_response_is_valid(attributes.get('recaptcha_response_field'),
                                                                            request.request_ip())

        if should_check_captcha and captcha_response_is_invalid:
            raise BadCaptcha()

        token = user_mgr.register_user(
            email=email,
            password=password,
            vanity=None,
            first_name=first_name,
            last_name=last_name,
            facebook_id=facebook_id)

        new_user_id = user_mgr.get_unique(email=email).UID if token else None

        if new_user_id:
            log_json({
                'verb': 'signup',
                'signup': {
                    'email': email,
                    'facebook': facebook_id,
                    'fname': first_name,
                    'lname': last_name
                }
            })
            log_state_change('user', new_user_id, 'create', '/user')
            login_user(user_mgr.get(new_user_id), api_app)

            analytics.log_patreon_event(
                SignupEvent.ADD_EVENT,
                {
                    'new_user_id': new_user_id,
                    'email': email,
                    'ip': request.request_ip()
                }
            )
            return UserResource(user_mgr.get(session.user_id)).as_json_api_document(json_api_version)
        else:
            raise UserAlreadyExists()

    except KeyError as ke:
        abort(400, 'invalid-parameters', "Invalid parameters in API call.")


@api_app.route('/current_user', methods=['PATCH'])
@require_login
@return_as_json
def edit_profile(current_user_id):
    current_user = user_mgr.get(current_user_id)
    data = get_data()

    first_name, last_name = split_name(data.get('name'))
    about = data.get('about')
    email = data.get('email')
    vanity = data.get('vanity')
    facebook_url = data.get('facebook')
    youtube_url = data.get('youtube')
    twitter_handle = data.get('twitter')
    old_password = data.get('old_password')
    new_password = data.get('new_password')
    new_password_confirmation = data.get('new_password_confirmation')

    # TODO: lat/lon

    user_mgr.sanitize_and_update_user_params(
        current_user, fname=first_name, lname=last_name, email=email,
        about=about, vanity=vanity, facebook=facebook_url,
        twitter=twitter_handle, youtube=youtube_url, new_password=new_password,
        old_password=old_password,
        new_password_confirmation=new_password_confirmation
    )

    updated_user = user_mgr.get(current_user_id)

    return UserResource(updated_user).as_json_api_document()


@api_app.route('/user/<string:user_id>')
@return_as_json
def get_user(user_id):
    user = user_mgr.get_user_or_throw(user_id)
    json_api_version = get_json_api_version(default_version=ApiResource.JSON_API_RC2)
    return UserResource(user, includes=get_includes()).as_json_api_document(json_api_version)


@api_app.route('/users', methods=['GET'])
@return_as_json
def get_filtered_users():
    filters = get_filters()
    if 'vanity' in filters:
        user = user_mgr.get_user_unique(vanity=filters['vanity'])
        if user is None:
            raise UserNotFound(filters['vanity'])
        return UserResource(user, includes=get_includes()).as_json_api_document()
    else:
        # only supporting filtering by vanity for now
        abort(500)


# TODO(postport): Need to determine if this naming for route is acceptable
@api_app.route('/current_user')
@return_as_json
def get_current_user():
    session_id = session.user_id
    user = user_mgr.get(session_id)
    if not user:
        return {'data': {'id': 0, 'type': 'user'}}
        # abort(403)
    json_api_version = get_json_api_version(default_version=ApiResource.JSON_API_RC2)
    return UserResource(user, get_includes()).as_json_api_document(json_api_version)


# TODO: should be `GET /campaign/<campaign_id>/pledges`
# (returning a collection of type=pledge rather than type=user !)
# once users pledge to campaigns rather than other users
@api_app.route('/user/<int:user_id>/patrons', methods=['GET'])
@return_as_json
def get_pledges_for_user(user_id):
    pledges = pledge_mgr.find_by_creator_id(user_id)
    patrons = [user_mgr.get(pledge.patron_id) for pledge in pledges]
    return UsersCollection(patrons, includes=['pledges']).as_json_api_document()
