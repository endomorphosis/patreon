from flask import request

from patreon.app.api import abort, api_app, return_as_json, require_login
from patreon.app.api.resources.presence import PresenceResource
from patreon.app.api.resources.api_resource import ApiResource
from patreon.model.manager import presence_mgr, feature_flag
from patreon.services import bonfire


@api_app.route('/presence', methods=['POST'])
@require_login
@return_as_json
def update_presence(current_user_id):
    parsed_json = PresenceResource.insertable_from_json_api_document(request.json)
    presence_id = presence_mgr.update_presence(
        user_id=current_user_id,
        room=parsed_json.get('room'),
        last_seen=parsed_json.get('last_seen'),
        expiration_time=parsed_json.get('expiration_time')
    )
    if presence_id:
        json_response = PresenceResource(presence_mgr.get(presence_id))\
            .as_json_api_document(version=ApiResource.JSON_API_STABLE)

        if feature_flag.is_enabled(
                'real_time_comments',
                group_id=request.ab_test_group_id,
                user_id=current_user_id):
            bonfire.post('/creator_presence', json_response)

        return json_response
    else:
        abort(403)
