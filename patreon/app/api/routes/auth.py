from flask import session, request

from patreon.app.api import abort, api_app, return_as_json, get_data, \
    require_login, get_json_api_version
from patreon.app.api.resources import ApiResource
from patreon.app.api.resources.users import UserResource
from patreon.exception.auth_errors import InvalidUser, \
    ResetPasswordInvalidEmail, ResetPasswordUserHasNoPassword, \
    TwoFactorRequired, TwoFactorInvalid
from patreon.model.manager import csrf, auth_mgr, user_mgr, forgot_password_mgr
from patreon.model.request.session import login_user
from patreon.services import facebook
from patreon.services.logging.unsorted import log_state_change
from patreon.services.mail import send_template_email
from patreon.services.mail.templates import PasswordReset


@api_app.route('/login', methods=['POST'])
@return_as_json
def process_user_login():
    try:
        data = get_data()
        if 'email' in data and 'password' in data:
            email = data['email']
            password = data['password']
            user_id = auth_mgr.get_user_by_email_and_password(email, password)
            # handle error case
            if password == '':
                raise InvalidUser()

        elif 'fb_access_token' in data:
            token = data['fb_access_token']
            facebook_id = facebook.me(token)['id']
            maybe_user = user_mgr.get_unique(facebook_id=facebook_id)
            user_id = maybe_user.UID if maybe_user else None

        else:
            user_id = None

        if user_id:
            user = user_mgr.get(user_id)
            if user.two_factor_enabled and 'two_factor_code' not in data:
                raise TwoFactorRequired()
            elif user.two_factor_enabled \
                    and not auth_mgr.verify_two_factor_code(user.two_factor_secret, data['two_factor_code']):
                raise TwoFactorInvalid()

            login_user(user, api_app)

            log_state_change('session', session.session_id, 'create', '/user')

            json_api_version = get_json_api_version(default_version=ApiResource.JSON_API_RC2)
            user = UserResource(user_mgr.get(session.user_id)).as_json_api_document(json_api_version)

            return user
        else:
            raise InvalidUser()

    except KeyError:
        abort(400, 'invalid-parameters', "Invalid parameters in API call.")


@api_app.route('/CSRFTicket', methods=['GET'])
@require_login
@return_as_json
def get_csrf_ticket(current_user_id):
    return csrf.get_csrf_ticket(request.args.get('uri'), current_user_id, session.csrf_token)


@api_app.route('/auth/forgot-password', methods=['POST'])
@return_as_json
def rest_auth_forgot_pass_route():
    data = get_data()
    email = data.get('email')
    if not email:
        raise ResetPasswordInvalidEmail('email', email)
    user = user_mgr.get_unique(email=email)
    if not user or user.is_nuked:
        raise ResetPasswordInvalidEmail('email', email)
    if not user.Password and not user.FBID:
        raise ResetPasswordUserHasNoPassword('email', email)

    token = forgot_password_mgr.insert(user.user_id)
    log_state_change('password_reset_token', token, 'create', '/REST/auth/forgot_password_reset')
    send_template_email(PasswordReset(user, token))

    return {}
