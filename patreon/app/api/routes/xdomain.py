from patreon.app.api import api_app
from iesupport.ie_xdomain_support import render_proxy_page


@api_app.route('/ie')
def xdomain():
    return render_proxy_page()
