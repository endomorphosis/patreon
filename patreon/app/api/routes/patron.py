from flask import request
from patreon.app.api import api_app, return_as_json
from patreon.model.manager import pledge_mgr

"""
# api.patreon.com/patrons?creator=<creator_id>

return pleges that match:
    user_id of patrons
    reward_id
    vanity if the user is a creatorspledge
    max_amount per month
    email for user

    {
    patrons: [
        {
            id: user_id as string
            vanity:
            email:
            reward: reward that the user selected
            amount_cents:
            pledge_cap_cents:
        },

        ...
    ]
    }
"""


@api_app.route('/patrons')
@return_as_json
def api_patrons():
    creator_id = int(request.args.get('creator'))
    if not creator_id:
        raise NotImplemented("/patrons API call is not actually fleshed out")

    # TODO(port): generalize this format so we're repeating ourselves for each API
    # TODO(port): move this into using PledgeResource?
    patrons = [
        {
            'id': str(pledge['UID']),
            'vanity': pledge['Vanity'],
            'email': pledge['Email'],
            'amount_cents': pledge['Pledge'],
            'pledge_cap_cents': pledge['MaxAmount'],
            'links': {
                # Don't cast "None" to a string, return it as null.
                'reward': str(pledge['RID']) if pledge['RID'] is not None else None
            }
        }
        for pledge in pledge_mgr.find_all_by_creator_id(creator_id)
    ]
    return { 'patrons': patrons }
