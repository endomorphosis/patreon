import json
from flask import request, session

from patreon import globalvars
from patreon.app.api import api_app, return_as_json, require_login, get_file, \
    get_data, ApiResource, get_json_api_version
from patreon.app.api.resources.posts import PostResource, PostsCollection
from patreon.app.api.view import post_view
from patreon.exception.campaign_errors import HasNoCampaign
from patreon.exception.post_errors import CannotPost, PostEditForbidden
from patreon.model.manager import activity_mgr, draft_mgr, campaign_mgr, \
    make_post_mgr, post_mgr
from patreon.model.type import PostType
from patreon.services import file_helper
from patreon.util import jsondate


@api_app.route('/campaigns/<int:campaign_id>/post', methods=['POST'])
@require_login
@return_as_json
def api_patron_post(campaign_id, current_user_id):
    # TODO MARCOS 1 photo
    # TODO MARCOS 2 thumbnail
    # TODO MARCOS 3 attachments

    campaign = campaign_mgr.get_campaign_or_throw(campaign_id)

    # User must be the creator or a patron.
    if not campaign_mgr.can_patron_post(current_user_id, campaign):
        raise CannotPost(campaign.campaign_id)

    data = get_data()

    post = make_post_mgr.patron_post(
        user_id=current_user_id,
        campaign_id=campaign_id,
        post_type=PostType.get(data.get('post_type')),
        title=data.get('title'),
        content_html=data.get('content'),
        embed_raw=data.get('embed'),
        min_cents_pledged_to_view=data.get('min_cents_pledged_to_view')
    )

    return PostResource(post, mask=post_view.public_whitelist).as_json_api_document()


@api_app.route('/posts/new', methods=['GET'])
@require_login
@return_as_json
def api_new_draft(current_user_id):
    campaign_id = campaign_mgr.try_get_campaign_id_by_user_id_hack(
        current_user_id
    )
    if not campaign_id:
        raise HasNoCampaign()

    clear_draft = draft_mgr.get_last_empty_draft(campaign_id) \
                  or draft_mgr.get_new_draft(campaign_id, current_user_id)

    return PostResource(clear_draft, mask=post_view.public_whitelist).as_json_api_document()


@api_app.route('/posts/<int:post_id>', methods=['GET'])
@return_as_json
def api_get_post(post_id):
    current_user_id = session.user_id
    post = activity_mgr.get_activity_or_throw(post_id)

    if post_mgr.can_view(current_user_id, post):
        mask = post_view.public_whitelist
    else:
        mask = post_view.private_whitelist

    return PostResource(post, mask=mask).as_json_api_document()


@api_app.route('/posts/<int:post_id>', methods=['POST'])
@require_login
@return_as_json
def api_save_post(post_id, current_user_id):
    post = activity_mgr.get_activity_or_throw(post_id)
    campaign = campaign_mgr.get_campaign(post.campaign_id)

    if not campaign_mgr.can_edit(current_user_id, campaign):
        raise CannotPost(campaign.campaign_id)

    data = get_data()
    tags = data.get('tags')

    post = make_post_mgr.save_post(
        post_id=post_id,
        user_id=current_user_id,
        post_type=PostType.get(data.get('post_type')),
        title=data.get('title'),
        content_html=data.get('content'),
        thumbnail=data.get('thumbnail'),
        embed_raw=data.get('embed'),
        min_cents_pledged_to_view=data.get('min_cents_pledged_to_view'),
        category=data.get('category'),
        is_paid=data.get('is_paid') == 'true',
        will_publish=(tags.get('publish') or False) if tags else False
    )

    return PostResource(post, mask=post_view.public_whitelist).as_json_api_document()


@api_app.route('/posts/<int:post_id>/edit', methods=['GET'])
@require_login
@return_as_json
def api_edit_post(post_id, current_user_id):
    activity = activity_mgr.get_activity_or_throw(post_id)

    if not post_mgr.can_edit(current_user_id, activity):
        raise PostEditForbidden(post_id)

    return PostResource(activity, mask=post_view.public_whitelist).as_json_api_document()


@api_app.route('/posts/<int:post_id>', methods=['DELETE'])
@require_login
@return_as_json
def api_delete_post(post_id, current_user_id):
    activity = activity_mgr.get_activity_or_throw(post_id)

    if not post_mgr.can_delete(current_user_id, activity):
        raise PostEditForbidden(post_id)

    activity_mgr.delete_activity(post_id)

    # TODO: what's the JSONAPI standard for repsonses to deletions?
    json_ = {'data': {
        'id': post_id,
        'type': 'post'
    }}
    attributes = {'deleted_at': jsondate.iso_now()}
    if get_json_api_version() == ApiResource.JSON_API_STABLE:
        json_['data']['attributes'] = attributes
    else:
        json_['data'].update(attributes)
    return json_


@api_app.route('/posts/<int:post_id>/undelete', methods=['POST'])
@require_login
@return_as_json
def api_undelete_post(post_id, current_user_id):
    activity = activity_mgr.get_deleted_activity_or_throw(post_id)

    if not post_mgr.can_edit(current_user_id, activity):
        raise PostEditForbidden(post_id)

    activity_mgr.undelete_activity(post_id)

    activity = activity_mgr.get_activity(post_id)
    return PostResource(activity, mask=post_view.public_whitelist).as_json_api_document()


@api_app.route('/campaigns/<int:campaign_id>/posts', methods=['GET'])
@return_as_json
def api_get_posts(campaign_id):
    current_user_id = session.user_id
    is_creation_filter = request.args.get('filter[is_creation]') or None
    if is_creation_filter is not None:
        is_creation_filter = is_creation_filter in ['true', 'True', '1', True]
    is_by_creator_filter = request.args.get('filter[is_by_creator]') or None
    if is_by_creator_filter is not None:
        is_by_creator_filter = is_by_creator_filter in ['true', 'True', '1', True]
    page_cursor = request.args.get('page[cursor]') or None
    per_page = request.args.get('page[count]') \
               or globalvars.get('SHARES_PER_PAGE')  # 'infinity' is a valid value

    posts = post_mgr.get_posts_by_campaign_id_paginated(
        campaign_id,
        cursor=page_cursor,
        per_page=per_page,
        is_creation=is_creation_filter,
        is_by_creator=is_by_creator_filter,
        visible_to_user_id=current_user_id
    )
    if current_user_id:
        masks = [post_view.public_whitelist for _ in posts]
    else:
        masks = [post_view.public_whitelist if post.min_cents_pledged_to_view == 0 else post_view.private_whitelist
                 for post in posts]
    return PostsCollection(posts, masks=masks).as_json_api_document()


@api_app.route('/campaigns/<int:campaign_id>/drafts', methods=['GET'])
@require_login
@return_as_json
def api_get_drafts(campaign_id, current_user_id):
    campaign = campaign_mgr.get_campaign_or_throw(campaign_id)

    if not campaign_mgr.can_edit(current_user_id, campaign):
        raise CannotPost(campaign_id)

    non_blank_drafts = draft_mgr.get_non_blank_drafts_by_campaign_id(campaign_id)
    return PostsCollection(non_blank_drafts, masks=[post_view.public_whitelist for _ in non_blank_drafts]).as_json_api_document()


@api_app.route('/posts/<int:post_id>/post_file', methods=["POST"])
@require_login
@return_as_json
def add_post_file(post_id, current_user_id):
    post_file = get_file()
    post = activity_mgr.get_activity_or_throw(post_id)

    top_offset = None
    if request.form and request.form.get('data'):
        data = json.loads(request.form.get('data'))
        top_offset = data.get('top_offset')

    if not post_mgr.can_edit(current_user_id, post):
        raise PostEditForbidden(post_id)

    source_extension = file_helper.get_extension(post_file.filename)
    with file_helper.file_stream_to_tempfile(post_file) as temp_filename:
        make_post_mgr.set_post_file(
            post.activity_id,
            temp_filename,
            post_file.filename,
            source_extension,
            top_offset
        )

    return PostResource(post, mask=post_view.public_whitelist).as_json_api_document()


@api_app.route('/posts/<int:post_id>/thumbnail', methods=["POST"])
@require_login
@return_as_json
def add_post_thumbnail(post_id, current_user_id):
    thumbnail = get_file()
    post = activity_mgr.get_activity_or_throw(post_id)

    top_offset = None
    if request.form and request.form.get('data'):
        data = json.loads(request.form.get('data'))
        top_offset = data.get('top_offset')

    if not post_mgr.can_edit(current_user_id, post):
        raise PostEditForbidden(post_id)

    with file_helper.file_stream_to_tempfile(thumbnail) as temp_filename:
        make_post_mgr.set_thumbnail(post.activity_id, temp_filename, top_offset)

    post = activity_mgr.get_activity(post_id)
    return PostResource(post, mask=post_view.public_whitelist).as_json_api_document()
