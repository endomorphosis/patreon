import datetime

from flask import request
from patreon.app.api import api_app, return_as_json
from patreon.app.api.resources import ApiResourceCollection, ApiResource
from patreon.app.api.resources.payment_event_stats import PaymentEventStats
from patreon.app.api.resources.pledges import PledgeResource
from patreon.app.api.resources.payment_events import PaymentEventResource
from patreon.exception.payment_errors import TimeNotEpoch
from patreon.model.manager import pledge_mgr, payment_event_mgr
from patreon.model.request.route_wrappers import api_require_admin
from patreon.util.collections import group_by
from patreon.util.datetime import offset_from_time, seconds_since_epoch
from patreon.util.unsorted import build_url_from_parameters, get_int


@api_app.route('/admin/payments/pledges', methods=['GET'])
@return_as_json
@api_require_admin
def get_pledges():
    pledges = pledge_mgr.get_recent_pledges_from_all_users(count=100)
    resources = (PledgeResource(pledge) for pledge in pledges)
    return ApiResourceCollection(resources).as_json_api_document()


def _parse_input_epoch_to_datetime(epoch_string, default=None):
    try:
        if epoch_string:
            begin_epoch = int(epoch_string)
            return datetime.datetime.fromtimestamp(begin_epoch)
        else:
            return default
    except:
        raise TimeNotEpoch(epoch_string)


@api_app.route('/admin/payments/events', methods=['GET'])
@return_as_json
@api_require_admin
def get_events():
    filter_args = request.dictionary_from_get('filter')
    filter_by_type = filter_args.get('payment_event_type')
    filter_by_subtype = filter_args.get('payment_event_subtype')

    filter_by_start_time = _parse_input_epoch_to_datetime(filter_args.get('created_at', {}).get('begin'))
    filter_by_end_time = _parse_input_epoch_to_datetime(filter_args.get('created_at', {}).get('end'))

    paging_args = request.dictionary_from_get('page')
    limit = min(get_int(paging_args.get('size', 100)), 500)
    cursor = paging_args.get('cursor')

    json_api_version = request.args.get('json-api-version', ApiResource.JSON_API_DEFAULT_VERSION)

    events = payment_event_mgr.get_filtered_events(type=filter_by_type,
                                                   subtype=filter_by_subtype,
                                                   earliest_time=filter_by_start_time,
                                                   latest_time=filter_by_end_time,
                                                   limit=limit,
                                                   cursor=cursor)

    event_resources = (PaymentEventResource(event) for event in events)

    next_url = None
    if events:
        last_event_id = events[-1].payment_event_id
        arguments = {
            'filter': filter_args,
            'page': {
                'size': limit,
                'cursor': last_event_id
            },
            'json-api-version': json_api_version
        }
        next_url = build_url_from_parameters('/admin/payments/events', arguments)

    return ApiResourceCollection(event_resources, links={'next': next_url}).as_json_api_document()


@api_app.route('/admin/payments/events/stats', methods=['GET'])
@return_as_json
@api_require_admin
def get_event_stats():
    filter_args = request.dictionary_from_get('filter')

    filter_by_start_time = _parse_input_epoch_to_datetime(filter_args.get('begin'), offset_from_time(hours=-24))
    filter_by_end_time = _parse_input_epoch_to_datetime(filter_args.get('end'), datetime.datetime.now())

    events = payment_event_mgr.get_filtered_events(earliest_time=filter_by_start_time,
                                                   latest_time=filter_by_end_time,
                                                   limit=None)
    event_types = group_by(events, lambda event: event.type).items()

    event_occurrences = {
        'event_occurrences': [
            {
                'name': event_type,
                'count': len(events_),
                'subtypes': _get_subtypes_from_event_list(events_)
            } for event_type, events_ in event_types
            ],
        'count': len(events)
    }

    return PaymentEventStats(id="{}-{}".format(seconds_since_epoch(filter_by_start_time),
                                               seconds_since_epoch(filter_by_end_time)),
                             data=event_occurrences).as_json_api_document()


def _get_subtypes_from_event_list(events):
    event_subtypes = group_by(events, lambda event: event.subtype).items()

    return [
        {
            'name': subtype,
            'count': len(events_)
        } for subtype, events_ in event_subtypes
        ]
