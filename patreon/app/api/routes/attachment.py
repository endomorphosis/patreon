from patreon.app.api.resources import ApiResource
from patreon.app.api.resources.attachments import AttachmentResource, AttachmentsCollection
from patreon.app.api import api_app, return_as_json, require_login, get_file, get_json_api_version
from patreon.exception.attachment_errors import AttachmentViewForbidden
from patreon.exception.post_errors import PostViewForbidden, PostEditForbidden
from patreon.model.manager import attachment_mgr, post_mgr, activity_mgr
from patreon.services import file_helper
from patreon.util import jsondate


@api_app.route('/posts/<int:post_id>/attachments/<int:attachment_id>', methods=['GET'])
@require_login
@return_as_json
def get_post_attachment(post_id, attachment_id, current_user_id):
    post = activity_mgr.get_activity_or_throw(post_id)

    if not post_mgr.can_view(current_user_id, post):
        raise AttachmentViewForbidden(attachment_id)

    attachment = attachment_mgr.get_attachment_or_throw(post_id, attachment_id)

    return AttachmentResource(attachment).as_json_api_document()


@api_app.route('/posts/<int:post_id>/attachments', methods=['GET'])
@require_login
@return_as_json
def get_post_attachments(post_id, current_user_id):
    post = activity_mgr.get_activity_or_throw(post_id)

    if not post_mgr.can_view(current_user_id, post):
        raise PostViewForbidden(post_id)

    attachments = attachment_mgr.get_attachments_by_activity_id(post_id)
    return AttachmentsCollection(attachments).as_json_api_document()


@api_app.route('/posts/<int:post_id>/attachments', methods=["POST"])
@require_login
@return_as_json
def add_post_attachment(post_id, current_user_id):
    file = get_file()
    post = activity_mgr.get_activity_or_throw(post_id)

    if not post_mgr.can_edit(current_user_id, post):
        raise PostEditForbidden(post_id)

    with file_helper.file_stream_to_tempfile(file) as temp_filename:
        attachment = attachment_mgr.attach_creations_file(post_id, temp_filename, file.filename)

    return AttachmentResource(attachment).as_json_api_document()


@api_app.route('/posts/<int:post_id>/attachments/<int:attachment_id>', methods=["DELETE"])
@require_login
@return_as_json
def delete_post_attachment(post_id, attachment_id, current_user_id):
    post = activity_mgr.get_activity_or_throw(post_id)

    if not post_mgr.can_edit(current_user_id, post):
        raise PostEditForbidden(post_id)

    # TODO MARCOS HACK Getting attachment to check existence.
    attachment_mgr.get_attachment_or_throw(post_id, attachment_id)

    attachment_mgr.delete_attachment(post_id, attachment_id)

    json = {
        'data': {
            'id': attachment_id,
            'type': 'attachment'
        }
    }
    attributes = {'deleted_at': jsondate.iso_now()}
    if get_json_api_version() == ApiResource.JSON_API_STABLE:
        json['data']['attributes'] = attributes
    else:
        json['data'].update(attributes)
    return json
