from patreon import globalvars
from patreon.app.api import abort, api_app, require_login, return_as_json
from patreon.constants.http_codes import INVALID_REQUEST_400
from patreon.model.manager import user_mgr
from patreon.services.mail import send_template_email
from patreon.services.mail.templates import ConfirmEmail


@api_app.route('/resend_verify_email', methods=['POST'])
@require_login
@return_as_json
def resend_verify_email(current_user_id):
    token = user_mgr.resend_confirm(current_user_id)
    if token is None:
        abort(INVALID_REQUEST_400)

    activation_link = 'https://{main_server}/confirm?ver={token}'.format(
        main_server=globalvars.get('MAIN_SERVER'),
        token=token
    )

    user = user_mgr.get(current_user_id)
    send_template_email(ConfirmEmail(user, activation_link))
    return {}
