from patreon.app.api import api_app, return_as_json, require_login, get_data
from patreon.app.api.resources import ApiResourceCollection
from patreon.app.api.resources.cards import CardResource
from patreon.exception.payment_errors import MissingStripeToken
from patreon.model.table import Card
from patreon.model.manager import card_mgr


@api_app.route('/cards', methods=['GET'])
@require_login
@return_as_json
def get_cards(current_user_id):
    cards = card_mgr.find_by_user_id(current_user_id)
    cards.sort(key=lambda card: card.created_at, reverse=True)
    card_resources = [CardResource(card) for card in cards]
    card_resource_collection = ApiResourceCollection(card_resources)
    return card_resource_collection.as_json_api_document()


@api_app.route('/card', methods=['POST'])
@api_app.route('/cards', methods=['POST'])
@require_login
@return_as_json
def create_card(current_user_id):
    # TODO(port): This isn't actually JSON API spec,
    # move this so that it requires the user id in the POST.
    data = get_data()

    if not data:
        raise MissingStripeToken

    stripe_token = data.get('stripe_token')

    # This is because subabble expects the data in a specific format
    if not stripe_token and data.get('data'):
        stripe_token = data.get('data').get('stripe_token')

    if not stripe_token:
        raise MissingStripeToken

    card_id = card_mgr.save_card_from_payment_api(current_user_id, stripe_token)

    return CardResource(Card.get(card_id)).as_json_api_document()
