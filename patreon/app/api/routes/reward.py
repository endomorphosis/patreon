from patreon.app.api import api_app, return_as_json
from patreon.app.api.resources.rewards import RewardResource
from patreon.model.manager import reward_mgr


@api_app.route('/reward/<int:reward_id>', methods=['GET'])
@return_as_json
def get_reward(reward_id):
    reward = reward_mgr.get_reward_or_throw(reward_id)

    return RewardResource(reward).as_json_api_document()
