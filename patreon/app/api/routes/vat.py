from flask import request
from patreon.app.api import api_app, return_as_json
from patreon.app.api.resources.vat import VatResource, VatCountries
from patreon.constants.vat_rates import STANDARD_VAT_RATES_IN_BPS
from patreon.model.manager import vat_mgr
from patreon.util.unsorted import base64encode


def get_ip():
    """
    Ugly method to make testing easier
    """
    return request.request_ip()


@api_app.route('/vat_rate')
@return_as_json
def get_vat_rate():
    ip_address = get_ip()
    iso_code = vat_mgr.get_country_by_ip_address(ip_address)
    vat_data = {
        'iso_code': iso_code,
        'friendly_name': vat_mgr.get_friendly_name_by_country_code(iso_code),
        'vat_rate': vat_mgr.standard_vat_rate_in_bps_for_country_code(iso_code)
    }

    return VatResource(base64encode(ip_address), vat_data).as_json_api_document()


@api_app.route('/vat_countries')
@return_as_json
def get_vat_countries():
    vat_countries_id = vat_mgr.vat_rules_version()
    vat_countries = vat_mgr.vat_countries()

    return VatCountries(vat_countries_id, {"countries": vat_countries}).as_json_api_document()


@api_app.route('/vat_countries_rates')
@return_as_json
def get_vat_countries_rates():
    vat_countries_id = vat_mgr.vat_rules_version()

    return VatCountries(vat_countries_id, {"vat_countries_rates": STANDARD_VAT_RATES_IN_BPS}).as_json_api_document()
