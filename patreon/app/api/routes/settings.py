from flask import session
from patreon.app.api import api_app, return_as_json, require_login, \
    get_data, get_bool_from_json_value, abort
from patreon.app.api.resources.push_info import PushInfoResource
from patreon.app.api.resources.settings import SettingsResource, \
    FollowSettingsResource, CampaignSettingsResource
from patreon.exception.common_errors import LoginRequired
from patreon.exception.settings_errors import PushInfoNotFound
from patreon.model.manager import settings_mgr, user_campaign_settings_mgr, \
    push_mgr, user_mgr
from patreon.services.logging.unsorted import log_state_change


@api_app.route('/settings', methods=['GET'])
@require_login
@return_as_json
def get_all_settings(current_user_id):
    all_settings = settings_mgr.get_all_settings(current_user_id)
    return SettingsResource(all_settings).as_json_api_document()


@api_app.route('/settings', methods=['PATCH'])
@require_login
@return_as_json
def update_settings(current_user_id):
    data = get_data()

    log_state_change('user', current_user_id, 'update', 'PATCH /settings')
    settings_mgr.update_settings(
        user_id=current_user_id,
        actor_id=current_user_id,
        privacy=data.get('pledges_are_private'),
        email_new_comment=data.get('email_about_all_new_comments'),
        email_update=data.get('email_about_patreon_updates'),
        email_goal=data.get('email_about_milestone_goals'),
        push_new_comment=data.get('push_about_all_new_comments'),
        push_update=data.get('push_about_patreon_updates'),
        push_goal=data.get('push_about_milestone_goals')
    )

    # TODO: discuss whether or not this API should allow including modifications to these related resources
    # if so, make sure to cover these with tests
    #
    # campaign_settings = get_included_with_filter('campaign_settings')
    # if campaign_settings:
    #     for campaign_setting in campaign_settings:
    #         update_campaign_settings_with_data(current_user_id, campaign_setting)
    #
    # follow_settings = get_included_with_filter('follow_settings')
    # if follow_settings:
    #     for follow_setting in follow_settings:
    #         update_follow_settings_with_data(current_user_id, follow_setting)

    all_settings = settings_mgr.get_all_settings(current_user_id)
    return SettingsResource(all_settings).as_json_api_document()


@api_app.route('/follow-settings/<string:follow_settings_id>', methods=['GET'])
@require_login
@return_as_json
def get_follow_settings(current_user_id, follow_settings_id):
    follow_settings = user_campaign_settings_mgr.get_by_id(follow_settings_id)
    return FollowSettingsResource(follow_settings).as_json_api_document()


@api_app.route('/follow-settings/<string:follow_settings_id>', methods=['PATCH'])
@require_login
@return_as_json
def update_follow_settings(current_user_id, follow_settings_id):
    data = get_data()
    data['id'] = follow_settings_id
    follow_settings = update_follow_settings_with_data(current_user_id, data)
    return FollowSettingsResource(follow_settings).as_json_api_document()


def update_follow_settings_with_data(current_user_id, follow_settings_data):
    follow_setting = user_campaign_settings_mgr.get_by_id(follow_settings_data.get('id'))
    if not follow_setting or not follow_setting.campaign_id:
        abort(404, "Could not find follow-setting with id " + follow_settings_data.get('id'))

    campaign_id = follow_setting.campaign_id
    email_followed_campaign_ids = [campaign_id]
    email_followed_paid = email_followed_post = email_followed_comment = None
    if follow_settings_data.get('email_about_new_paid_posts') is not None:
        email_followed_paid = [get_bool_from_json_value(follow_settings_data.get('email_about_new_paid_posts'))]
    if follow_settings_data.get('email_about_new_posts') is not None:
        email_followed_post = [get_bool_from_json_value(follow_settings_data.get('email_about_new_posts'))]
    if follow_settings_data.get('email_about_new_comments') is not None:
        email_followed_comment = [get_bool_from_json_value(follow_settings_data.get('email_about_new_comments'))]

    push_followed_paid = push_followed_post = push_followed_comment = push_creator_live = None
    if follow_settings_data.get('push_about_new_paid_posts') is not None:
        push_followed_paid = [get_bool_from_json_value(follow_settings_data.get('push_about_new_paid_posts'))]
    if follow_settings_data.get('push_about_new_posts') is not None:
        push_followed_post = [get_bool_from_json_value(follow_settings_data.get('push_about_new_posts'))]
    if follow_settings_data.get('push_about_new_comments') is not None:
        push_followed_comment = [get_bool_from_json_value(follow_settings_data.get('push_about_new_comments'))]
    if follow_settings_data.get('push_about_going_live') is not None:
        push_creator_live = [get_bool_from_json_value(follow_settings_data.get('push_about_going_live'))]

    log_state_change('user', current_user_id, 'update', 'PATCH /follow-settings')
    settings_mgr.update_settings(
        user_id=current_user_id,
        actor_id=current_user_id,
        email_campaign_ids=email_followed_campaign_ids,
        email_creator_paid=email_followed_paid,
        email_creator_post=email_followed_post,
        email_creator_comment=email_followed_comment,
        push_creator_paid=push_followed_paid,
        push_creator_post=push_followed_post,
        push_creator_comment=push_followed_comment,
        push_creator_live=push_creator_live
    )

    return user_campaign_settings_mgr.get(current_user_id, campaign_id)


@api_app.route('/campaign-settings/<string:campaign_settings_id>', methods=['GET'])
@require_login
@return_as_json
def get_campaign_settings(current_user_id, campaign_settings_id):
    all_settings = settings_mgr.get_all_settings(current_user_id)
    campaign_settings = all_settings.campaign_settings[0]
    return CampaignSettingsResource(campaign_settings).as_json_api_document()


@api_app.route('/campaign-settings/<string:campaign_settings_id>', methods=['PATCH'])
@require_login
@return_as_json
def update_campaign_settings(current_user_id, campaign_settings_id):
    data = get_data()
    data['id'] = campaign_settings_id
    campaign_settings = update_campaign_settings_with_data(current_user_id, data)
    return CampaignSettingsResource(campaign_settings).as_json_api_document()


# TODO: touch-ups needed once users can have more than one campaign
# (parse ID to know which was being spoken of,
#    return that one instead of just the first in the array on the AllSettings object)
def update_campaign_settings_with_data(current_user_id, campaign_settings_data):
    email_patron_update = campaign_settings_data.get('email_about_new_patrons')
    email_post_update = campaign_settings_data.get('email_about_new_patron_posts')
    push_patron_update = campaign_settings_data.get('push_about_new_patrons')
    push_post_update = campaign_settings_data.get('push_about_new_patron_posts')

    log_state_change('user', current_user_id, 'update', 'PATCH /campaign-settings')
    settings_mgr.update_settings(
        user_id=current_user_id,
        actor_id=current_user_id,
        email_new_patron=email_patron_update,
        email_patron_post=email_post_update,
        push_new_patron=push_patron_update,
        push_patron_post=push_post_update
    )

    all_settings = settings_mgr.get_all_settings(current_user_id)
    return all_settings.campaign_settings[0]


@api_app.route('/push-info', methods=['POST'])
@require_login
@return_as_json
def update_push_settings(current_user_id):
    data = get_data()
    push_info = push_mgr.update_or_create(current_user_id, data['bundle_id'], data['token'])
    push_info_resource = PushInfoResource(push_info)
    return push_info_resource.as_json_api_document(), {'created': push_info_resource.resource_url()}


@api_app.route('/push-info', methods=['DELETE'])
@return_as_json
def delete_push_settings():
    data = get_data()
    bundle_id = data['bundle_id']
    token = data['token']
    current_user_id = data.get('relationships', {}).get('user', {}).get('data', {}).get('id')
    current_user_id = current_user_id or session.user_id
    # allow logged-out users to delete push info if they have all three pieces of info
    # this is called automatically by clients when the user is auto-logged out (on, say, a HTTP 401)
    if current_user_id == 0:
        raise LoginRequired()
    push_info = push_mgr.get_with_token(current_user_id, bundle_id, token)
    if not push_info:
        raise PushInfoNotFound(user_id=current_user_id, bundle_id=bundle_id, token=token)
    push_info_resource = PushInfoResource(push_info)
    response = push_info_resource.as_json_api_document()
    url = push_info_resource.resource_url()
    push_info.delete()
    return response, {'deleted': url}


@api_app.route('/settings/change-password', methods=['POST'])
@require_login
@return_as_json
def change_password(current_user_id):
    user = user_mgr.get_user_or_throw(current_user_id)
    data = get_data()
    old_password = data.get('old_password')
    new_password = data.get('new_password')
    new_password_confirmation = data.get('new_password_confirmation')
    error_msg = user_mgr.update_password(
        user,
        old_password=old_password,
        new_password=new_password,
        new_password_confirmation=new_password_confirmation,
        execute_update=True
    )
    if error_msg:
        abort(400, 'change password error', error_msg)
    else:
        return {}
