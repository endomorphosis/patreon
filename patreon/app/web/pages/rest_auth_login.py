from _md5 import md5

from flask import request, session
import patreon
from patreon.app.web import web_app
from patreon.model.manager.csrf import get_user_secret
from patreon.model.manager import user_mgr
from patreon.model.manager.session_mgr import update_session
from patreon.services.logging.unsorted import log_auth_fail, log_state_change
from patreon.util.unsorted import jsencode
from patreon.services import ratelimit
from patreon.model.manager.session_mgr import get_new_session_id


@web_app.route('/REST/auth/login', methods=['POST'])
def login_endpoint():
    if not request.form:
        return jsencode({'error': 'Bad Request.'}), 400

    email = request.form['email']
    password = request.form['password']
    result = False
    email_hash = md5((email + (request.request_ip() or '')).encode()).hexdigest()

    if ratelimit.overlimit(email_hash, 'login-attempt', 60, 3600):
        return jsencode({'error': 'You have entered too many wrong passwords. Please try again later.'}), 429
    user_id = patreon.model.manager.auth_mgr.get_user_by_email_and_password(email, password)

    if user_id is None:
        log_auth_fail(email)

    if user_id is not None:
        result = True
        user = user_mgr.get(user_id=user_id)
        session.login(user.UID, user.is_admin)
        new_session_id = get_new_session_id()
        session.session_id = new_session_id
        log_state_change('session', session.session_id, 'create', '/REST/auth/login')
        update_session(new_session_id, user.UID, get_user_secret(user.UID), None, 0, '{}',
                       patreon.globalvars.get('SESSION_LIFETIME'))

        @patreon.model.request.call_after_request
        def force_session_cookie(response):
            web_app.session_interface.set_session_cookie(response, new_session_id)
            return response
    else:
        ratelimit.throttle(email_hash, 'login-attempt', 60, 3600)

    if not result:
        return jsencode({'error': 'Your email / password was incorrect.'}), 403

    return jsencode(result)


@web_app.route('/REST/auth/login', methods=['GET'])
def rest_auth_login_get():
    return '', 405
