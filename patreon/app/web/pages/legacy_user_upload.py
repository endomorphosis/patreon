from patreon.app.web import web_app, get_qqfile
from patreon.constants import image_sizes
from patreon.model.manager import profile_mgr
from patreon.model.request.route_wrappers import set_content_type_json
from patreon.model.type import file_bucket_enum, photo_key_type
from patreon.services import file_helper
from patreon.util.unsorted import jsencode


@web_app.route('/legacyUserUpload', methods=['POST'])
@set_content_type_json
def legacy_user_upload_post():
    user_image = get_qqfile()

    with file_helper.file_stream_to_tempfile(user_image) as temp_filename:
        photo_key = profile_mgr.upload_profile_image(temp_filename)

    image_url = "https:" + photo_key_type.get_image_url(
        file_bucket_enum.config_bucket_user,
        photo_key,
        image_sizes.USER_LARGE
    )

    return jsencode({
        'success': True,
        'photo_key': photo_key,
        'image_url': image_url
    })


@web_app.route('/legacyUserUpload', methods=['GET'])
def legacy_user_upload_get():
    return '', 405
