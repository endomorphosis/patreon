from flask import request
from patreon.app.web import web_app
from patreon.model.manager import action_mgr, activity_mgr
from patreon.services.logging.unsorted import log_page_anomaly
from patreon.util.unsorted import jsencode, get_int
from patreon.model.request.route_wrappers import set_content_type_json, require_login


def handle_process_action(user_id, activity_id, add_like, action_type):
    if not activity_id or not activity_mgr.get(activity_id=activity_id) or action_type != 1:
        log_page_anomaly('post', 'bad_parameters')
        return jsencode({}), 400

    action_mgr.execute_like(user_id, activity_id, add_like)

    count = action_mgr.find_likes_by_activity_id(activity_id).count()
    return jsencode({'type': 1,
                     'user_id': user_id,
                     'creation_id': activity_id,
                     'status': bool(add_like),
                     'count': count}), 200


@web_app.route('/processAction', methods=['POST'])
@set_content_type_json
@require_login
def process_action_route(current_user_id):
    activity_id = get_int(request.args.get('hid'))
    get_on = get_int(request.args.get('on'))
    action_type = get_int(request.args.get('ty'))

    return handle_process_action(current_user_id, activity_id, get_on, action_type)


@web_app.route('/processAction', methods=['GET'])
def process_action_get():
    return '', 405
