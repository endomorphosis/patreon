from patreon.app.web import web_app
from patreon.app.web.view import render_page, PostCreator


# TODO: if appropriate viewport, etc. settings for all client-rendered pages, store in a CONST.
@web_app.route('/post')
@web_app.route('/posts/new')
def new_post():
    return render_page(
        inner=PostCreator,
        title='Patreon: Create Post',
        viewport='device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no',
        height=None,
        no_legacy_javascript=True,
        no_legacy_stylesheets=True,
        patreon_fonts_override=True
    )
