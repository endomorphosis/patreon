from flask import request, session
import patreon
from patreon.app.web import web_app
from patreon.app.web.view import render_page, BePatronDonePage
from patreon.model.manager import user_mgr, new_pledge_mgr, feature_flag
from patreon.routing import redirect
from patreon.services.logging.unsorted import log_json, log_action
from patreon.util.unsorted import get_int


@web_app.route('/bePatronDone', methods=['GET', 'POST'])
def be_patron_done():
    creator_id = get_int(request.args.get('u'))
    if not creator_id and request.method == 'POST':
        creator_id = session.user_id

    creator = user_mgr.get(user_id=creator_id)
    if not creator or not creator.campaigns:
        return redirect('/')

    single_page_enabled = (
        feature_flag.get_abtest_assignment(
            'be_patron_single_page',
            request.ab_test_group_id,
            session.user_id
        )
        or new_pledge_mgr.patron_has_any_vat_pledges(session.user_id)
    )

    log_action(
        'load_be_patron_done',
        single_page_enabled=single_page_enabled,
        session_id=session.session_id,
        user_id=session.user_id
    )

    if request.cookies.get('rid') is not None:
        log_json({
            'verb': 'refer',
            'type': 'pledge',
            'referrer': request.cookies.get('rid'),
            'referred_to': request.args.get('u')
        })

        @patreon.model.request.call_after_request
        def set_referrer_cookie(response):
            response.set_cookie('rid', "", expires=0)
            return response
    return render_page(
        inner=BePatronDonePage,
        title='Thank you for your pledge to {}'.format(creator.full_name),
        creator_id=creator_id
    )
