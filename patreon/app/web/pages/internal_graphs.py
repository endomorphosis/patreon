import datetime
from collections import OrderedDict, defaultdict
from flask import jsonify, request, Response

from patreon.app.web.view import InternalGraphs, InternalMobileBetaData, render_page
from patreon.model.request.route_wrappers import require_admin, register_internal_page
from patreon.model.manager import stats_mgr
from patreon.stats import mobile_beta_data, stats_dashboard_input

# A dictionary of {'stat group': [stats...]}.
TABLE_STATS = stats_dashboard_input.build_ordered_list_of_dashboard_data_keys_all_historical_metrics()
TABLE_STATS['all_stats'] = [
    items
    for _, _list in TABLE_STATS.items()
    for items in _list
]


@require_admin
def get_data(key):
    now = datetime.datetime.now()

    stats_range = stats_mgr.get_range(
        key,
        now - datetime.timedelta(days=7),
        now - datetime.timedelta(days=1)
    )

    return [
        {
            'date': stat.Date.strftime('%y-%m-%d'),
            'value': stat.Value
        }
        for stat in stats_range
    ]


@register_internal_page('/internal/dashboard/graphs', name="Graphs Index", category='Stats')
def graphs_page():
    return render_page(
        inner=InternalGraphs,
        title='Patreon: Key Lever Graphs',
    )


@register_internal_page('/internal/dashboard/graphs/average_pledge_amount', name="Avg. Pledge Amount Graph",
                        category='Stats')
def average_pledge_amount():
    return jsonify(data=get_data('average_pledge_amount'), labels={'x': 'Date', 'y': 'Cents'})


@register_internal_page('/internal/dashboard/graphs/pledges_per_patron', name="Pledges Per Patron Graph",
                        category='Stats')
def pledges_per_patron():
    return jsonify(data=get_data('pledges_per_patron'), labels={'x': 'Date', 'y': 'Pledges'})


@register_internal_page('/internal/dashboard/graphs/table_<stat_id>', category='Stats', display=False)
def table_stats(stat_id):
    if stat_id not in TABLE_STATS:
        return '', 404

    stats_group = TABLE_STATS.get(stat_id)

    rows = defaultdict(list)
    row_metadata = OrderedDict([
        (s['key'], {
            'class': s['class'],
            'label': s['label'],
            'row_class': s.get('row_class')
        })
        for s in stats_group
    ])
    table_stats = stats_mgr.get_multiple_keys(*row_metadata.keys())

    dates = []
    # Get all the dates, so if a stat is missing it, our data isn't messed up.
    for s in table_stats:
        date = s.Date.strftime('%b-%y')
        if date not in dates:
            dates.append(date)

    data = defaultdict(dict)

    for s in table_stats:
        data[s.Key][s.Date.strftime('%b-%y')] = s.Value

    for date in dates:
        for k in data:
            if date not in data[k]:
                rows[k].append('-')
            else:
                rows[k].append(data[k][date])

    ordered_rows = []
    for row_key in row_metadata:
        ordered_rows.append({
            'label': row_metadata[row_key]['label'],
            'data': rows[row_key],
            'class': row_metadata[row_key]['class'],
            'row_class': row_metadata[row_key]['row_class'],
        })

    return jsonify(headers=dates, rows=ordered_rows)


@register_internal_page('/internal/dashboard/graphs/kpi_summary', category='Stats', display=False)
def kpi_summary():
    data = []
    # Get the two preceding months
    for s in stats_mgr.get_multiple_keys_range_date_sub(
            'NetPaymentsProcessed_Growth',
            'NetPaymentsProcessed_Value',
            'CreatorsOverThreshold_Growth',
            'CreatorsOverThreshold_Count',
            'NPS_CreatorsOverThreshold',
            interval=3):
        data.append((s.Date.strftime('%Y-%m'), s.Key, s.Value))

    # Get this month's data
    for s in stats_mgr.get_multiple_keys(
            'NetPaymentsProcessed_GrowthForecast',
            'NetPaymentsProcessed_ValueForecast',
            'NetPaymentsProcessed_GrowthToDate',
            'NetPaymentsProcessed_ValueToDate',
            'CreatorsOverThreshold_GrowthForecast',
            'CreatorsOverThreshold_CountForecast',
            'CreatorsOverThreshold_GrowthToDate',
            'CreatorsOverThreshold_CountToDate',
            'NPS_CreatorsOverThreshold'):
        data.append((s.Date.strftime('%Y-%m'), s.Key, s.Value))

    # Make the data a dict
    data_dict = dict()
    for m in data:
        data_dict[m[0]] = {}
    for m in data:
        data_dict[m[0]][m[1]] = m[2]

    # Find the months in the data
    all_months = list(data_dict.keys())
    all_months.sort(reverse=True)

    # Create a series of strings to show on the screen
    text_to_display = [
        "PAYMENTS PROCESSED:",
        "This month forecast: ({:.2f}% growth; ${:,.0f} processed)".format(
            data_dict[all_months[0]]['NetPaymentsProcessed_GrowthForecast'],
            data_dict[all_months[0]]['NetPaymentsProcessed_ValueForecast']
        ),
        "This month to-date:  ({:.2f}%; ${:,.0f})".format(
            data_dict[all_months[0]]['NetPaymentsProcessed_GrowthToDate'],
            data_dict[all_months[0]]['NetPaymentsProcessed_ValueToDate']
        ),
        "Last month:          ({:.2f}%; ${:,.0f})".format(
            data_dict[all_months[1]]['NetPaymentsProcessed_Growth'],
            data_dict[all_months[1]]['NetPaymentsProcessed_Value']
        ),
        "Two months ago:      ({:.2f}%; ${:,.0f})".format(
            data_dict[all_months[2]]['NetPaymentsProcessed_Growth'],
            data_dict[all_months[2]]['NetPaymentsProcessed_Value']
        ),
        "HUNDRED-DOLLAR CREATORS:",
        "This month forecast: ({:.2f}% growth; {:,.0f} creators)".format(
            data_dict[all_months[0]]['CreatorsOverThreshold_GrowthForecast'],
            data_dict[all_months[0]]['CreatorsOverThreshold_CountForecast']
        ),
        "This month to-date:  ({:.2f}%; {:,.0f})".format(
            data_dict[all_months[0]]['CreatorsOverThreshold_GrowthToDate'],
            data_dict[all_months[0]]['CreatorsOverThreshold_CountToDate']
        ),
        "Last month:          ({:.2f}%; {:,.0f})".format(
            data_dict[all_months[1]]['CreatorsOverThreshold_Growth'],
            data_dict[all_months[1]]['CreatorsOverThreshold_Count']
        ),
        "Two months ago:      ({:.2f}%; {:,.0f})".format(
            data_dict[all_months[2]]['CreatorsOverThreshold_Growth'],
            data_dict[all_months[2]]['CreatorsOverThreshold_Count']
        ),
        "NPS AMONG HUNDRED-DOLLAR CREATORS:",
        "This month:          {:.1f}".format(
            data_dict[all_months[0]]['NPS_CreatorsOverThreshold']
        ),
        "Last month:          {:.1f}".format(
            data_dict[all_months[1]]['NPS_CreatorsOverThreshold']
        ),
        "Two months ago:      {:.1f}".format(
            data_dict[all_months[2]]['NPS_CreatorsOverThreshold']
        )
    ]

    # return jsonify(data=data)
    return jsonify(data=text_to_display)


@register_internal_page('/internal/mobile_beta_data', name="Mobile Data", category='Stats')
def get_mobile_beta_data():
    return render_page(
        inner=InternalMobileBetaData,
        title='Patreon: Mobile Beta Data'
    )


@register_internal_page('/internal/mobile_beta_data/data', category='Stats', display=False)
def get_mobile_beta_data_file():
    key = request.args.get('key')
    data = mobile_beta_data.get_uploaded_data(key) if key else None
    if not data:
        data = 'No data found.'
    return Response(data, content_type='text/plain')
