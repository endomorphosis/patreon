from flask import request, redirect
from patreon.app.web.view.BasePage.base_page import render_page
from patreon.app.web.view.GenericInternalPage.generic_internal_page import GenericInternalPage
from patreon.model.request.route_wrappers import register_internal_page
from patreon.services.caching import memcache

post_register = {}


def register_generic_post(function_name):
    def decorator(f):
        global post_register

        post_register[function_name] = f

    return decorator


@register_internal_page('/internal/generic/<string:function_name>', methods=['POST'], display=False)
def generic_post_internal_page(function_name):
    post_register[function_name]()
    return redirect(request.referrer)


@register_generic_post('memcache_clear')
def clear_memcache():
    memcache.flush_all()
    print('Yes, this got called')


@register_internal_page('/internal/clear_memcache', name='Memcache Control', category='Tools')
def clear_memcache_page():
    return render_page(
        inner=GenericInternalPage,
        title="Patreon: Memcache Control",
        viewport='device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no',
        height=None,

        internal_title='Memcache Control',
        function_name='memcache_clear',
        form_labels=['Clear Memcache']
    )
