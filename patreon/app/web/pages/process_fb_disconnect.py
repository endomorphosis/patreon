from flask import session
from patreon.app.web import web_app
from patreon.model.manager import user_mgr


@web_app.route('/processFBDisconnect', methods=['POST'])
def process_fb_disconnect():
    if not session.user_id:
        return '', 403

    user_mgr.remove_fb(session.user_id)
    return '', 200
