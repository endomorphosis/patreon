from flask import session
from patreon.app.web import web_app
from patreon.app.web.view import ImportYt, render_iframe
from patreon.routing import redirect
from patreon.services.logging.unsorted import log_page_anomaly


@web_app.route('/importYtPop')
@web_app.route('/importYtPop.php')
def import_yt_pop():
    if session.user_id == 0:
        log_page_anomaly('get', 'unauthorized')
        return redirect('/')

    return render_iframe(
        inner=ImportYt
    )
