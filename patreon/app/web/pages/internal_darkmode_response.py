from patreon.app.web.view import render_iframe, DarkmodeResponseContent
from patreon.model.request.route_wrappers import register_internal_page


@register_internal_page('/internal/darkmode/response', display=False)
def darkmode_view_response():
    return render_iframe(
        inner=DarkmodeResponseContent,
        title='Darkmode Response',
        no_share=True,
    )
