from patreon.app.web import web_app
from patreon.model.manager import campaign_mgr, queue_mgr
from patreon.model.request.route_wrappers import require_login
from patreon.services.analytics import log_patreon_event
from patreon.services.analytics_events import LaunchEvent
from patreon.services.logging.unsorted import log_json


@web_app.route('/processUserSetup', methods=['POST'])
@require_login
def process_launch(current_user_id):
    campaign_id = (campaign_mgr.try_get_campaign_id_by_user_id_hack(current_user_id) or
                   campaign_mgr.create_campaign(current_user_id))

    campaign_mgr.publish_campaign(campaign_id, current_user_id)
    queue_mgr.enqueue_campaign_for_update(campaign_id)

    log_json({'verb': 'launch_creator'})
    log_patreon_event(LaunchEvent.SUCCESS, {
        'campaign_id': campaign_id
    })

    return '', 200
