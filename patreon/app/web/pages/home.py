import patreon
from patreon.app.web import web_app
from patreon.app.web.view import render_page, HomeLegacy

PAGE_HOME = patreon.globalvars.get('PAGE_HOME')


@web_app.route('/home')
@web_app.route('/home.php')
def home():
    return render_page(
        inner=HomeLegacy,
        title='Patreon: Home',
        no_footer=True,
        os_page=PAGE_HOME,
    )
