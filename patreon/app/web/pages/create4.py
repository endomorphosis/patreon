from flask import request
import patreon
from patreon.app.web import web_app
from patreon.app.web.view import render_page, Create4
from patreon.model.manager import campaign_mgr, reward_mgr, user_mgr
from patreon.model.request.route_wrappers import require_login
from patreon.routing import redirect
from patreon.services.logging.unsorted import log_page_anomaly, log_state_change
from patreon.util import collections
from patreon.util.html_cleaner import clean


@web_app.route('/create4', methods=['GET'])
@require_login
def create4_get(current_user_id):
    current_user = user_mgr.get(current_user_id)
    campaign = campaign_mgr.try_get_campaign_by_user_id_hack(current_user.user_id)

    if not campaign:
        log_page_anomaly('get', 'no_campaign')
        return redirect('/create')

    return render_page(
        inner=Create4,
        title='Creator Setup',
        no_share=True,
        current_user=current_user,
        campaign=campaign
    )


def handle_create4_post(user_id, reward_ids, reward_amounts,
                        reward_descriptions, reward_shippings, reward_limits,
                        video_url, share_embed, thanks_message):
    user_mgr.get_user_or_throw(user_id)

    campaign = campaign_mgr.try_get_campaign_by_user_id_hack(user_id)
    if not campaign:
        log_page_anomaly('post', 'no_campaign')
        return '', 400

    if not (len(reward_ids)
                == len(reward_amounts)
                == len(reward_descriptions)
                == len(reward_shippings)
                == len(reward_limits)):
        log_page_anomaly('post', 'variable_mismatch')
        return '', 400

    rewards = collections.zip_lists_to_dicts(
        ['reward_id', 'amount', 'description', 'shipping', 'limit'],
        [reward_ids, reward_amounts, reward_descriptions, reward_shippings, reward_limits]
    )

    success = reward_mgr.set_rewards(user_id, rewards)
    if not success:
        redirect('/create4')

    campaign_mgr.update_campaign(
        campaign.campaign_id,
        thanks_message=thanks_message,
        thanks_video_url=video_url,
        thanks_embed=share_embed
    )

    log_state_change('campaign', campaign.campaign_id, 'update', '/create4')

    redirect('/user?alert=' + str(patreon.globalvars.get('ALERT_USER_SETUP')))


@web_app.route('/create4', methods=['POST'])
@require_login
def create4_post(current_user_id):
    reward_ids          = request.form.getlist('rid[]')
    reward_amounts      = request.form.getlist('rAmount[]')
    reward_descriptions = request.form.getlist('rDescription[]')
    reward_shippings    = request.form.getlist('rShipping[]')
    reward_limits       = request.form.getlist('rLimit[]')
    video_url           = clean(request.form.get('videoUrl'))
    share_embed         = clean(request.form.get('shareEmbed'))
    thanks_message      = clean(request.form.get('thanksMsg'))

    return handle_create4_post(
        current_user_id, reward_ids, reward_amounts, reward_descriptions,
        reward_shippings, reward_limits, video_url, share_embed, thanks_message
    )
