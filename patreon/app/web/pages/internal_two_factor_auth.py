from flask import request, send_file, session
from patreon.app.web.view import render_page, TwoFactorAuth
from patreon.model.manager import user_mgr, auth_mgr
from patreon.model.request.route_wrappers import register_internal_page
from patreon.routing import redirect


@register_internal_page('/internal/two_factor_setup', methods=['GET'], name="Two Factor Setup", category='Auth')
def two_factor_page():
    user_id = session.original_user_id
    user = user_mgr.get(user_id)

    if user.two_factor_secret:
        return redirect('/')

    return render_page(
        inner=TwoFactorAuth,
        title='Patreon: Two Factor Auth Setup',
        viewport='device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no',
        height=None
    )


@register_internal_page('/internal/two_factor_save', methods=['POST'], name="Two Factor Setup", category='Auth')
def two_factor_setup():
    secret = request.form.get('secret')
    test_pass = request.form.get('test_pass')

    if not secret:
        return redirect('/internal/two_factor_setup?err=1')

    if not auth_mgr.verify_two_factor_code(secret, test_pass):
        return redirect('/internal/two_factor_setup?err=1')

    auth_mgr.setup_two_factor_auth(user_mgr.get(session.original_user_id), secret)
    session.logout()
    return redirect('/login')


@register_internal_page('/internal/two_factor_qr.png', display=False)
def two_factor_qr():
    secret = request.args.get('secret')
    email = request.args.get('email')

    uri = auth_mgr.get_authenticator_uri(secret, email)

    qr_code = auth_mgr.get_qr_code_for_authenticator(uri)

    return send_file(qr_code, mimetype='image/png')
