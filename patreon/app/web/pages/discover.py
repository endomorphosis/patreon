from flask import url_for, request
from patreon.app.web import web_app
from patreon.routing import redirect


@web_app.route('/discover')
def discover():
    featured_url = url_for('.featured', **request.args)
    return redirect(featured_url)
