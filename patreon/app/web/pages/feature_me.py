from patreon.app.web import web_app
from patreon.routing import redirect


@web_app.route('/featureme')
def feature_me():
    return redirect(
        "https://docs.google.com/forms/d/1AZsYygFAXM7TDkKV4TryFHRxl3eLMuCHsXfCKIgnBdk/viewform?usp=send_form"
    )
