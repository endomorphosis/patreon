# Static Pages (They don't import flask)
from .about import about
from .careers import careers
from .cds_creators import cds_creators
from .contact import contact
from .content_removed import content_removed
from .feature_me import feature_me
from .guidelines import guidelines
from .ios import ios_beta
from .jobs import jobs
from .legal import legal
from .press import press
from .subbable import subbable
from .toolbox import toolbox, toolbox_page

# Get pages
from .be_patron import be_patron
from .be_patron_confirm import be_patron_confirm
from .be_patron_done import be_patron_done
from .create import create_get
from .create2 import create2_get
from .create3 import create3_get
from .create4 import create4_get
from .create_start import create_start
from .creation import creation
from .discover import discover
from .dmca_takedown import dmca_takedown
from .featured import featured
from .home import home
from .home_next import home_next
from .index import index
from .internal_darkmode_response_html import darkmode_view_response_html
from .internal_darkmode import darkmode_index
from .internal_darkmode_response import darkmode_view_response
from .internal_flubstep_debug import flubstep_debugger
from .internal_feature_flags import flags_get, flags_post
from .login import login_form
from .logout import logout
from .manage_rewards import manage_rewards_get
from .manage_rewards_list import manage_rewards_list_get
from .notifications import notifications
from .patron_next import patron_next
from .pending import pending_get
from .process_check_vanity import process_check_vanity
from .post import post_show
from .post_edit import edit_post
from .post_new import new_post
from .rest_auth_csrf_ticket import csrf_ticket
from .search import search
from .signup import signup
from .start_i import start_i
from .file import file_get
from .channel import channel
from .import_yt_pop import import_yt_pop
from .forget_pass import forgot_pass
from .forget_pass_reset import forgot_pass_reset
from .user import user
from .user_msg_all import user_message_all
from .user_msg import user_message
from .user_next import user_next
from .process_settings import process_settings_get
from .settings_page import settings_get
from .payment_declined import payment_declined
from .transactions_i import transactions_i_get

# React Pages
from .creator_page import creator_page_pure

# Post pages
from .create import create_post
from .create2 import create2_post
from .create3 import create3_post
from .create4 import create4_post
from .process_action import process_action_route
from .process_follow import process_follow_route
from .process_comment import process_comment_page
from .process_comment_edit import process_comment_edit_page
from .process_comment_delete import process_comment_delete_page
from .process_delete_location import process_delete_location
from .process_fb_disconnect import process_fb_disconnect
from .process_launch import process_launch
from .process_msg import process_message
from .process_resend_email import process_resend_email
from .process_revert import process_revert
from .process_reward_wait import process_reward_wait
from .process_youtube_import import import_youtube
from .rest_auth_forgot_pass_reset import rest_auth_forgot_pass_reset_route
from .rest_auth_forgot_pass import rest_auth_forgot_pass_route
from .process_settings import process_settings_post
from .legacy_campaign_upload import legacy_campaign_upload_post
from .legacy_user_upload import legacy_user_upload_post
from .paypal import paypal_start
from .paypal import paypal_return

# get placeholders for post pages
from .process_patron import process_patron
from .process_action import process_action_get
from .process_follow import process_follow_get
from .process_comment import process_comment_get
from .process_comment_delete import process_comment_delete_get
from .process_comment_edit import process_comment_edit_get
from .rest_auth_forgot_pass_reset import forgot_password_reset_get
from .rest_auth_forgot_pass import forgot_password_get
from .rest_auth_login import rest_auth_login_get
from .legacy_campaign_upload import legacy_campaign_upload_get
from .legacy_user_upload import legacy_user_upload_get
from .paypal import paypal_start_get


# Admin routes
from .internal_impersonate import impersonate_user
from .internal_impersonate import impersonate_user_multi_identity
from .internal_zoho_link import link_zoho_id_to_account
from .internal_graphs import graphs_page
from .internal_graphs import average_pledge_amount
from .internal_graphs import pledges_per_patron
from .internal_graphs import table_stats
from .internal_graphs import kpi_summary
from .internal_payments_dashboard import show_payments_dashboard
from .internal_payment_cron_status import show_cron_status
from .internal_index import internal_index
from .internal_two_factor_auth import two_factor_qr
from .internal_two_factor_auth import two_factor_page
from .internal_two_factor_auth import two_factor_setup
from .internal_captcha_controls import edit_captcha_controls
from .internal_captcha_controls import show_captcha_services
from .internal_dash import internal_dash
from .internal_creator_queue import internal_creator_queue
from .internal_generic import *

# default route
from .user import route_vanity
