from flask import request, session
from patreon.exception.auth_errors import UnauthorizedAdminFunction
from patreon.model.manager import user_mgr
from patreon.model.request.route_wrappers import require_admin, register_internal_page
from patreon.routing import redirect
from patreon.services.logging.unsorted import log_json


@require_admin
def handle_impersonate(identifier):
    if not identifier:
        user = user_mgr.get(session.original_user_id)
    else:
        user = user_mgr.get(identifier) or user_mgr.get_unique(email=identifier) or user_mgr.get_unique(vanity=identifier)

    if not user:
        return None
    log_json({'verb': 'admin_action',
              'type': 'impersonation',
              'data': {
                  'target': user.user_id
              }})
    session.impersonate(user.user_id)
    return user


@register_internal_page('/internal/impersonate', display=False)
def impersonate_user():
    user_id = request.args.get('user_id')

    try:
        user = handle_impersonate(user_id)
    except UnauthorizedAdminFunction:
        return '', 403

    if not user:
        return redirect('/')

    return redirect(user.canonical_url())


@register_internal_page('/internal/impersonate', methods=['POST'], display=False)
def impersonate_user_multi_identity():
    # This can be a user_id, a vanity or an email
    identifier = request.form.get('identifier')

    try:
        user = handle_impersonate(identifier)
    except UnauthorizedAdminFunction:
        return '', 403

    if not user:
        return redirect('/')

    return redirect(user.canonical_url())
