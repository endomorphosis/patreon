from patreon.app.web import web_app
from patreon.app.web.view import render_page, PostEditor


@web_app.route('/posts/<string:slug>-<int:post_id>/edit')
@web_app.route('/posts/<int:post_id>/edit')
def edit_post(post_id=None, slug=None):
    return render_page(
        inner=PostEditor,
        title='Patreon: Edit Post',
        viewport='device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no',
        height=None,
        no_legacy_javascript=True,
        no_legacy_stylesheets=True,
        patreon_fonts_override=True,
        post_id=post_id
    )
