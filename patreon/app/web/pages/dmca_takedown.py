from patreon.app.web import web_app
from patreon.app.web.view import render_page, DMCATakedownPage


@web_app.route('/dmca')
def dmca_takedown():
    return render_page(
        inner=DMCATakedownPage,
        title='Patreon: DMCA Takedown',
        viewport='device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no',
        height=None,
        no_legacy_javascript=True,
        no_legacy_stylesheets=True,
        patreon_fonts_override=True
    )
