from flask import request
from patreon.app.web import web_app
from patreon.services.analytics_events import CommentEvent
from patreon.util.unsorted import jsencode, get_int
from patreon.model.request.route_wrappers import set_content_type_json, require_login
from patreon.model.manager.comment_mgr import delete_comment_as_user
from patreon.services import analytics
from patreon.exception import APIException


def handle_delete_comment(user_id, comment_id):
    try:
        comment = delete_comment_as_user(comment_id, user_id)
    except APIException as e:
        return jsencode({'error': e.error_description}), e.status_code

    if not comment:
        return jsencode(None), 200

    return jsencode({
        'CID': comment.comment_id,
        'HID': comment.post_id,
        'Comment': comment.comment_text,
        'ThreadID': comment.thread_id
    }), 200


@web_app.route('/processCommentDelete', methods=['POST'])
@set_content_type_json
@require_login
def process_comment_delete_page(current_user_id):
    comment_id = get_int(request.form.get('cid'))

    analytics.log_patreon_event(
        CommentEvent.DELETE_EVENT,
        event_properties={
            'source': 'web_comments_v1',
            'comment_id': comment_id,
            'user_id': current_user_id
        })

    return handle_delete_comment(current_user_id, comment_id)


@web_app.route('/processCommentDelete', methods=['GET'])
def process_comment_delete_get():
    return '', 405
