from patreon.app.web import web_app
from patreon.app.web.view import render_page, NotificationsPage
from patreon.model.manager import user_mgr
from patreon.model.request.route_wrappers import require_login
from patreon.routing import redirect


@web_app.route('/updates', methods=['GET'])
@require_login
def notifications(current_user_id):
    current_user = user_mgr.get(user_id=current_user_id)

    if not current_user.is_active_creator:
        return redirect('/')

    return render_page(
        inner=NotificationsPage,
        title='Dashboard',
        no_footer=True,
        no_legacy_javascript=True,
        no_legacy_stylesheets=True,
        patreon_fonts_override=True
    )
