import patreon
from flask import session, request
from patreon.app.web import web_app
from patreon.app.web.view import render_page, IndexContent
from patreon.model.manager import feature_flag
from patreon.routing import redirect

MAIN_SERVER = patreon.globalvars.get('MAIN_SERVER')


@web_app.route('/')
@web_app.route('/index.php')
def index():
    in_home_v2_test = feature_flag.get_abtest_assignment(
        'home_page_v2',
        group_id=request.ab_test_group_id,
        user_id=session.user_id
    )

    if session.user_id and in_home_v2_test:
        return redirect('/home')

    view_kwargs = { 'viewport': 1200 }
    if in_home_v2_test:
        view_kwargs.update({
            'viewport': 'device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no',
            'height': None,
            'no_legacy_javascript': True,
            'no_legacy_stylesheets': True,
            'patreon_fonts_override': True
        })

    return render_page(
        inner=IndexContent,
        title='Patreon: Support the creators you love',
        desc='Patreon is empowering a new generation of creators to make a living from their passion and hard work.',
        url='https://' + MAIN_SERVER,
        image_url='https://' + MAIN_SERVER + '/images/patreon_home.jpg',
        in_home_v2_test=in_home_v2_test,
        **view_kwargs
    )
