from patreon.app.web import web_app
from patreon.app.web.view import render_page, SettingsPage
from patreon.model.request.route_wrappers import require_login


@web_app.route('/settings')
@web_app.route('/settings.php')
@require_login
def settings_get(current_user_id):
    return render_page(
        inner=SettingsPage,
        title="Settings",
        viewport='device-width, initial-scale=1',
        height=None,
        no_share=True
    )
