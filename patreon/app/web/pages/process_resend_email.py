from flask import session
from patreon import globalvars
from patreon.app.web import web_app
from patreon.model.manager import user_mgr
from patreon.services.mail import send_template_email
from patreon.services.mail.templates import ConfirmEmail


@web_app.route('/processResendEmail', methods=['POST'])
@web_app.route('/processResendEmail.php', methods=['POST'])
def process_resend_email():
    if not session.user_id:
        return '', 403

    user = user_mgr.get(session.user_id)

    token = user_mgr.resend_confirm(session.user_id)
    if token is None:
        return '0', 200

    activation_link = 'https://{main_server}/confirm?ver={token}'.format(
        main_server=globalvars.get('MAIN_SERVER'),
        token=token
    )

    send_template_email(ConfirmEmail(user, activation_link))
    return '1', 200
