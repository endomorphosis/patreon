from flask import request
from patreon.app.web import web_app
from patreon.model.manager import message_mgr
from patreon.model.request.route_wrappers import require_login
from patreon.util import html_cleaner


@web_app.route('/processMsg', methods=['POST'])
@web_app.route('/processMsgGroup', methods=['POST']) # deprecated
@require_login
def process_message(current_user_id):
    # 'uids[]' is the PHP convention for arrays in get/post data
    # since we're not in PHP-land anymore, we can prefer the plain form
    recipients_list = (request.form.getlist('uids') or
                       request.form.getlist('uids[]'))

    if not recipients_list:
        recipient = request.form.get('uid')
        if not recipient:
            return '', 400
        recipients_list = [recipient]

    # TODO: Remove this once we figure out a better spam solution.
    if 1010662 in [int(r) for r in recipients_list]:
        return '', 403

    message = request.form.get('msg')
    if message is None:
        return '', 400

    message_mgr.process_message(
        current_user_id,
        recipients_list,
        html_cleaner.angular_escape(message)
    )
    return '', 200
