from flask import request, session
from patreon.app.web import web_app
from patreon.app.web.view import ShareBox, DisplayUserFull
from patreon.model.manager import action_mgr, campaign_mgr, activity_mgr, \
    user_mgr, pledge_mgr, comment_mgr
from patreon.services import analytics
from patreon.services.analytics_events import UserNextEvent
from patreon.util.unsorted import get_int


@web_app.route('/userNext')
@web_app.route('/userNext.php')
def user_next():
    current_user_id = session.user_id
    page_user_id = request.args.get('u')
    page = abs(get_int(request.args.get('p')) or 1)
    template_type = request.args.get('ty')
    srt = request.args.get('srt')

    page_user = user_mgr.get(page_user_id)
    if page_user is None:
        return ''

    # track userNext calls for now to monitor aggressive patron content viewing
    analytics.log_patreon_event(
        UserNextEvent.FETCH_EVENT,
        {
            'page': page,
            'page_user': page_user_id,
            'template_type': template_type,
            'viewer_user': current_user_id,
            'viewer_ip': request.request_ip()
        }
    )

    if template_type == 'c' or template_type == 'a':
        likes = action_mgr.find_likes_by_user_id_creation_dict(current_user_id)
        current_user = user_mgr.get(current_user_id)
        page_campaign = campaign_mgr.get_campaigns_users(user_id=page_user_id)
        if page_campaign is None:
            return ''
        shares = []
        if template_type == 'c':
            shares = activity_mgr.find_creator_posts_by_campaign_id_paginated(
                page_campaign.campaign_id, page
            ).all()
        elif template_type == 'a':
            shares = activity_mgr.find_posts_by_campaign_id_paginated(
                page_campaign.campaign_id, page
            ).all()

        post_ids = [post.activity_id for post in shares]
        commenters = comment_mgr.get_commenters_dict_for_posts(post_ids)

        return ''.join([
            ShareBox(post, current_user, commenters, likes).render()
            for post in shares
        ])

    elif template_type == 'p':
        pledges = pledge_mgr.find_by_creator_id_paginated(page_user_id, page)

        return ''.join([
            DisplayUserFull(pledge.patron_id, page_user).render()
            for pledge in pledges
        ])

    return ''
