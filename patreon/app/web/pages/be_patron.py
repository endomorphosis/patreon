from flask import session, request
from patreon.app.web import web_app
from patreon.app.web.view import render_page, BePatronPage, BePatronSinglePage
from patreon.services.logging.unsorted import log_action
from patreon.model.manager import feature_flag, new_pledge_mgr


@web_app.route('/bePatron')
def be_patron():
    single_page_enabled = (
        feature_flag.get_abtest_assignment(
            'be_patron_single_page',
            group_id=request.ab_test_group_id,
            user_id=session.user_id
        )
        or new_pledge_mgr.patron_has_any_vat_pledges(session.user_id)
    )

    log_action(
        'load_be_patron',
        single_page_enabled=single_page_enabled,
        session_id=session.session_id,
        user_id=session.user_id
    )

    if single_page_enabled:
        return render_page(
            inner=BePatronSinglePage,
            title='Patron Setup',
            no_share=True,
            viewport='device-width,initial-scale=1',
            no_legacy_javascript=True,
            no_legacy_stylesheets=True,
            internal_page=1
        )
    else:
        return render_page(
            inner=BePatronPage,
            title='Patron Setup',
            no_share=True
        )
