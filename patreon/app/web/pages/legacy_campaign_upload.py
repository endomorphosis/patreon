from flask import request
from patreon.app.web import web_app, get_qqfile
from patreon.model.manager import campaign_mgr
from patreon.model.request.route_wrappers import set_content_type_json
from patreon.services import file_helper
from patreon.util.unsorted import jsencode


@web_app.route('/legacyCampaignUpload', methods=['POST'])
@set_content_type_json
def legacy_campaign_upload_post():
    campaign_image_file = get_qqfile()
    filename = request.args.get('qqfile')

    with file_helper.file_stream_to_tempfile(campaign_image_file) as temp_filename:
        image_url, image_small_url = campaign_mgr.upload_campaign_image(temp_filename, filename)

    return jsencode({
        "success": True,
        "image_url": image_url,
        "image_small_url": image_small_url
    })


@web_app.route('/legacyCampaignUpload', methods=['GET'])
def legacy_campaign_upload_get():
    return '', 405
