from flask import request
from patreon.app.web import web_app
from patreon.routing import redirect
from patreon.util.unsorted import get_int


@web_app.route('/creation')
def creation():
    activity_id = get_int(request.args.get('hid'))
    return redirect('/posts/{}'.format(activity_id))
