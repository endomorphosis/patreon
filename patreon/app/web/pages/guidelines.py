from patreon.app.web import web_app
from patreon.app.web.view import render_page, GuidelinesPage


@web_app.route('/guidelines')
@web_app.route('/guidelines.php')
def guidelines():
    return render_page(
        inner=GuidelinesPage,
        title='Patreon: Community Guidelines',
        viewport='device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no',
        height=None,
        no_legacy_javascript=True,
        no_legacy_stylesheets=True,
        patreon_fonts_override=True,
        no_share=True
    )
