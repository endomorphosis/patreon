from flask import session, request
import patreon
from patreon.app.web import web_app
from patreon.app.web.view import render_page, Signup
from patreon.routing import redirect


@web_app.route('/signup')
@web_app.route('/signup.php')
def signup():
    if session.user_id:
        redirect_url = patreon.routing.sanitize_redirect_url(request.args.get('ru'))
        return redirect(redirect_url)

    return render_page(
        inner=Signup,
        title="Patreon: Sign Up",
        viewport='device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no',
        patreon_fonts_override=True,
        height=None,
        no_legacy_stylesheets=True
    )
