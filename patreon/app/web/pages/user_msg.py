from patreon.app.web import web_app
from patreon.app.web.view import render_iframe, UserMessageContent
from patreon.model.request.route_wrappers import require_login


@web_app.route('/userMsg')
@require_login
def user_message(current_user_id):
    return render_iframe(
        inner=UserMessageContent,
        i_bg_color='#EAEAEA',
        title='Message History'
    )
