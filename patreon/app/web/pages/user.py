from flask import request, session
import patreon
from patreon.app.web import web_app, register_admin_data
from patreon.app.web.view import render_page, UserCampaign, UserPatron
from patreon.model.manager import user_mgr, complete_pledge_mgr, dmca_mgr
from patreon.model.request.route_wrappers import unauthed_memcache
from patreon.routing import redirect
from patreon.services.logging.unsorted import log_json
from patreon.util.unsorted import strip_vanity_from_query_string, \
    set_query_string_part, get_int
from patreon.constants.vanity_redirects import vanity_redirect_map


@web_app.route('/user')
@web_app.route('/user.php')
def user():
    page_user_id = None
    if request.args.get('v') is not None:
        vanity_url = request.args.get('v')
        if vanity_url in vanity_redirect_map:
            return redirect('/' + vanity_redirect_map[vanity_url])

        page_user = user_mgr.get_unique(vanity=vanity_url)
        if page_user:
            page_user_id = page_user.user_id
        else:
            page_user_id = None

    elif request.args.get('u') is not None:
        page_user_id = request.args.get('u')
    elif session.user_id != 0:
        page_user_id = session.user_id
        page_user = user_mgr.get(user_id=page_user_id)
        if page_user.vanity:
            return redirect('/' + page_user.vanity + '?' + request.query_string.decode("utf-8"))

    if page_user_id is None:
        return redirect('/')
    return user_page(page_user_id, get_tab_type(), request.args.get('pat') is not None)


@web_app.route('/<string:vanity_url>')
def route_vanity(vanity_url):
    vanity_url = vanity_url.replace('%EF%BF%BD', '')

    if vanity_url in vanity_redirect_map:
        return redirect('/' + vanity_redirect_map[vanity_url])

    page_user = user_mgr.get_unique(vanity=vanity_url)

    if page_user is None:
        return redirect('/')
    return user_page(page_user.user_id, get_tab_type(), request.args.get('pat') is not None)


@unauthed_memcache
def user_page(page_user_id, tab_type, show_patron_page):
    page_user_id = get_int(page_user_id)
    page_user = user_mgr.get(page_user_id)
    if page_user is None:
        return redirect('/')

    register_admin_data('user_id', page_user_id)
    if page_user.is_suspended == 1:
        return redirect('/contentRemoved')

    # TODO: Move this into its own dashboard like feature flags
    if dmca_mgr.is_user_suspended(page_user.user_id):
        return redirect('/dmca?u={}'.format(page_user.user_id))

    is_creator = page_user.is_creator
    campaign = None
    if is_creator:
        campaign = page_user.campaigns[0]

    if (not tab_type or tab_type not in ['h', 'a', 'c', 'p']) and (is_creator and not show_patron_page):
        query_string = strip_vanity_from_query_string(request.query_string.decode('utf-8'))

        if complete_pledge_mgr.get_pledge_by_patron_and_campaign_or_creator(
                patron_id=session.user_id, creator_id=page_user.user_id
        ).legacy:
            return redirect(page_user.canonical_url(set_query_string_part(query_string, 'ty', 'c')))
        else:
            return redirect(page_user.canonical_url(set_query_string_part(query_string, 'ty', 'h')))

    if request.args.get('rf') is not None and request.args.get('rf') != session.user_id:
        log_json({
            'verb': 'refer',
            'type': 'view',
            'referrer': request.args.get('rf'),
            'referred_to': page_user.user_id
        })

        @patreon.model.request.call_after_request
        def set_referrer_cookie(response):
            response.set_cookie('rid' + str(page_user.user_id), request.args.get('rf'))
            response.set_cookie('rid', request.args.get('rf'))
            return response

    if page_user.is_private:
        session.meta['Privacy'] = True

    hide_footer = tab_type and tab_type != 'h'

    meta_url = page_user.canonical_url() or request.url
    meta_image_url = page_user.image_url

    is_owner = session.user_id == page_user.user_id

    if campaign is not None and campaign.image_url is not None:
        if campaign.image_url.strip() is not '':
            meta_image_url = campaign.image_url.strip()

    session.meta['is_vanity'] = page_user.vanity is not None
    session.meta['page_user_id'] = page_user.user_id
    if show_patron_page or not is_creator or (is_creator and not campaign.is_launched and not is_owner):
        return render_page(
            inner=UserPatron,
            title=(page_user.full_name or ''),
            page_user_id=page_user.user_id,
            os_page='user',
            image_url=page_user.image_url,
            url=meta_url,
            no_footer=hide_footer,
            tab_type=tab_type
        )

    register_admin_data('campaign', campaign)
    return render_page(
        inner=UserCampaign,
        title='Support ' + (page_user.full_name or '') + ' creating ' + (campaign.creation_name or ''),
        page_user_id=page_user.user_id,
        os_page='user',
        image_url=meta_image_url,
        url=meta_url,
        no_footer=hide_footer
    )


def get_tab_type():
    raw_tab_type = request.args.get('ty')
    if raw_tab_type not in ['h', 'a', 'c', 'p', 'l']:
        return None
    return raw_tab_type
