from patreon.app.web import web_app
from patreon.app.web.view import render_react_page, InternalDash, GenericDashboardPage
from patreon.model.request.route_wrappers import require_admin, register_internal_page

# Don't do this normally, but just for ease of use.
from patreon.model.internal.placard import *


@register_internal_page('/internal/dashboard', name="Patreon Dashboard", category='Stats')
def internal_dash():
    return render_react_page(
        inner=InternalDash,
        title='Patreon Internal Dashboard',
        no_share=True,
    )


def render_generic_dash(title, card_rows):
    return render_react_page(
        inner=GenericDashboardPage,
        title=title,
        card_rows=card_rows,
        page_title=title,
        no_share=True,
    )


@register_internal_page('/internal/dashboard/creator_relations', name="CR Dashboard", category='Stats')
def cr_dash():
    return render_generic_dash('CR Dashboard', [
        [CRPledgeAddedCards, CRPledgeDeletedCards, CRAllPledges]
    ])


@register_internal_page('/internal/dashboard/payment_crons', name="Payment Crons", category='Stats')
def payment_cron_dash():
    return render_generic_dash('Payment Crons', [
        [PaymentInProcessCard, PaymentTotalCard],
        [PaymentPendingCard, PaymentDeclinedCard, PaymentSuccessfulCard]
    ])