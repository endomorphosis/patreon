from patreon.app.web import web_app
from patreon.app.web.view import LegalPage, render_page


@web_app.route('/legal')
@web_app.route('/guidelines')
def legal():
    return render_page(
        inner=LegalPage,
        title='Patreon: Community Guidelines',
        viewport='device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no',
        height=None,
        no_legacy_javascript=True,
        no_legacy_stylesheets=True,
        patreon_fonts_override=True
    )
