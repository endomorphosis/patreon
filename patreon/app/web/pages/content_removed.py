from patreon.app.web import web_app
from patreon.app.web.view import render_page, ContentRemovedPage


@web_app.route('/contentRemoved')
@web_app.route('/contentRemoved.php')
def content_removed():
    return render_page(
        inner=ContentRemovedPage,
        title='Patreon: Content Removed',
        viewport='device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no',
        height=None,
        no_legacy_javascript=True,
        no_legacy_stylesheets=True,
        patreon_fonts_override=True
    )
