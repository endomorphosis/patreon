from flask import url_for, request
from patreon.app.web import web_app
from patreon.routing import redirect


@web_app.route('/jobs')
def jobs():
    return redirect(url_for('.careers', **request.args))
