from patreon.app.web import web_app
from patreon.app.web.view import render_page, CareersPage


@web_app.route('/careers')
def careers():
    return render_page(
        inner=CareersPage,
        title='Patreon: Careers',
    )