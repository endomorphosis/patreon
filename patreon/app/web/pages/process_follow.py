from flask import request
from patreon.app.web import web_app
from patreon.model.manager import follow_mgr, user_mgr
from patreon.services.logging.unsorted import log_json, log_state_change
from patreon.util.unsorted import get_int, jsencode
from patreon.model.request.route_wrappers import set_content_type_json, require_login


def handle_process_follow(user_id, followed_id, get_follow):
    if not followed_id or not user_mgr.get(user_id=followed_id):
        return jsencode({'following': False,
                         'follower_id': user_id,
                         'followed_id': user_id}), 400

    existing = follow_mgr.get(followed_id=followed_id, follower_id=user_id)
    if existing and not get_follow:
        log_state_change('follow', followed_id, 'delete', '/processFollow')
        existing.delete()
    elif not existing and get_follow:
        log_state_change('follow', followed_id, 'create', '/processFollow')
        follow_mgr.insert(follower_id=user_id, followed_id=followed_id)

    return_data = {
        'follower_id': user_id,
        'followed_id': followed_id,
        'is_following': bool(get_follow)
    }
    log_data = return_data.copy()
    log_data.update({'verb': 'follow'})
    log_json(log_data)

    return jsencode(return_data), 200


@web_app.route('/processFollow', methods=['POST'])
@set_content_type_json
@require_login
def process_follow_route(current_user_id):
    followed_id = get_int(request.args.get('uid'))
    get_follow = get_int(request.args.get('follow'))
    return handle_process_follow(current_user_id, followed_id, get_follow)


@web_app.route('/processFollow', methods=['GET'])
def process_follow_get():
    return '', 405
