from flask import session, request
from patreon.app.web import web_app
from patreon.exception.attachment_errors import AttachmentViewForbidden
from patreon.model.manager import post_mgr, activity_mgr, attachment_mgr
from patreon.util import download, unsorted
from patreon.services import logging


@web_app.route('/posts/<int:post_id>/attachments/<int:attachment_id>')
def attachment_get(post_id, attachment_id):
    current_user_id = session.user_id
    name, url = handle_file_get(current_user_id, post_id, attachment_id)

    return download.downloadable_response(name, url)


@web_app.route('/file')
@web_app.route('/file.php')
def file_get():
    current_user_id = session.user_id
    post_id = unsorted.get_int(request.args.get('h'))
    attachment_id = unsorted.get_int(request.args.get('i'))

    name, url = handle_file_get(current_user_id, post_id, attachment_id)

    return download.downloadable_response(name, url)


def handle_file_get(current_user_id, post_id, attachment_id):
    try:
        post = activity_mgr.get_activity_or_throw(post_id)
        if not post_mgr.can_view(current_user_id, post):
            raise AttachmentViewForbidden(attachment_id)

        attachment = attachment_mgr.get_attachment_or_throw(
            post_id, attachment_id
        )

        logging.unsorted.log_json({
            'verb': 'attachment_download',
            'activity_id': post_id,
            'creator_id': post.creator_id,
            'campaign_id': post.campaign_id,
            'file_url': attachment.url,
            'file_name': attachment.name
        })

        file_url = attachment.url
        if file_url.startswith('//'):
            file_url = 'http:' + file_url

        return attachment.name, file_url

    except:
        return None, None
