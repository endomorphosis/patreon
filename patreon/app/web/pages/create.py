from flask import request
from patreon.app.web import web_app
from patreon.app.web.view import render_page, CreatePage
from patreon.model.request.route_wrappers import require_login
from patreon.model.manager import campaign_mgr, user_mgr, category_mgr
from patreon.routing import redirect
from patreon.services.analytics_events import CreatorSettingsEvent
from patreon.util.unsorted import split_name
from patreon.services import analytics


@web_app.route('/create', methods=['POST'])
@require_login
def create_post(current_user_id):
    full_name           = request.form.get('fullName')
    one_liner           = request.form.get('oneline')
    pay_per_name        = request.form.get('createSingle')
    creation_name       = request.form.get('create')
    is_monthly          = request.form.get('monthCheck') is not None
    is_nsfw             = request.form.get('nsfwCheck') is not None
    image_url           = request.form.get('shareImgUrlPhoto')
    image_small_url     = request.form.get('shareImgSmallUrlPhoto')
    category_ids_raw    = request.form.getlist('categories[]')

    first_name, last_name = split_name(full_name)

    campaign_mgr.create_or_update_campaign(
        current_user_id, first_name, last_name, one_liner, pay_per_name,
        creation_name, is_monthly, is_nsfw, image_url, image_small_url,
        category_ids_raw
    )

    for category_id in category_ids_raw:
        category = category_mgr.get_category(category_id)
        analytics.log_patreon_event(
            CreatorSettingsEvent.CATEGORY_SAVE_EVENT,
            {'category_name': category.name, 'category_id': category.category_id},
            {'creator_category': category.name}
        )

    redirect('/create2')


@web_app.route('/create', methods=['GET'])
@require_login
def create_get(current_user_id):
    current_user = user_mgr.get(current_user_id)
    campaign = campaign_mgr.try_get_campaign_by_user_id_hack(current_user_id)

    return render_page(
        inner=CreatePage,
        title='Creator Setup',
        no_share=True,
        current_user=current_user,
        campaign=campaign
    )
