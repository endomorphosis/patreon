from patreon.app.web import web_app
from patreon.model.manager import campaign_mgr, queue_mgr, follow_mgr
from patreon.model.request.route_wrappers import require_login
from patreon.services.logging.unsorted import log_json


@web_app.route('/processRevert', methods=['POST'])
@require_login
def process_revert(current_user_id):
    campaign_id = campaign_mgr.try_get_campaign_id_by_user_id_hack(current_user_id)
    if campaign_id is None:
        return '', 200

    campaign_mgr.unpublish_campaign(campaign_id)
    follow_mgr.delete_by_followed_id(current_user_id)
    queue_mgr.enqueue_campaign_for_update(campaign_id)
    log_json({'verb': 'revert_creator'})
    return '', 200
