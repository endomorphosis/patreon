from patreon.app.web import web_app
from patreon.app.web.view import PressPage, render_page


@web_app.route('/press')
def press():
    return render_page(
        inner=PressPage,
        title='Patreon: Press'
    )
