from flask import request
from patreon.app.web import web_app
from patreon.model.manager import forgot_password_mgr, user_mgr
from patreon.services.logging.unsorted import log_state_change, log_page_anomaly
from patreon.model.manager.auth_mgr import change_password
from patreon.util.unsorted import jsencode, get_int
from patreon.model.request.route_wrappers import set_content_type_json


def handle_forgot_pass(user_id, token, password):
    if not user_id or not token or not password:
        log_page_anomaly('post', 'missing_params')
        return jsencode({'error': 'The link you used to make this request is invalid! '
                                  'Make sure it matches the email you were sent!'}), 400

    if not user_mgr.get(user_id=user_id) or not forgot_password_mgr.get(user_id=user_id, token=token):
        log_page_anomaly('post', 'bad_token')
        return jsencode({'error': 'The link you used to make this request is invalid!'}), 400

    change_password(user_id, password)
    log_state_change('user', user_id, 'update', '/REST/auth/forgot_password_reset')
    forgot_password_mgr.delete_all_user_tokens(user_id)
    log_state_change('password_reset_token', user_id, 'delete_all', '/REST/auth/forgot_password_reset')

    return jsencode({'success': True}), 200


@web_app.route('/REST/auth/forgot_password_reset', methods=['POST'])
@set_content_type_json
def rest_auth_forgot_pass_reset_route():
    user_id = get_int(request.form.get('user_id'))
    token = request.form.get('security_token')
    password = request.form.get('password')

    return handle_forgot_pass(user_id, token, password)


@web_app.route('/REST/auth/forgot_password_reset', methods=['GET'])
def forgot_password_reset_get():
    return '', 405