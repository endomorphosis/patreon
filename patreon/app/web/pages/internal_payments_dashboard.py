from patreon.app.web.view import render_page, PaymentsDashboard
from patreon.model.request.route_wrappers import register_internal_page


@register_internal_page('/internal/payments_dashboard', name="Payments Dashboard", category='Dashboards')
def show_payments_dashboard():
    return render_page(
        inner=PaymentsDashboard,
        title='Patreon: Payment Event Dashboard',
        viewport='device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no',
        height=None,
        no_legacy_javascript=True,
        no_legacy_stylesheets=True
    )
