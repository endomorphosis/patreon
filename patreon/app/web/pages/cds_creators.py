from patreon.app.web import web_app
from patreon.app.web.view import render_page, CDSCreatorsPage


@web_app.route('/CDSCreators')
def cds_creators():
    return render_page(
        inner=CDSCreatorsPage,
        title='',
        desc="Patreon is empowering a new generation of content creators to make"
             " a living out of their passion and hard work.",
        url="https://patreon.com/",
        image_url="https://patreon.com/images/patreon_home.jpg",
        os_page='home'
    )
