from patreon.app.web import web_app
from patreon.routing import redirect


@web_app.route('/contact')
@web_app.route('/Contact')
def contact():
    return redirect('http://support.patreon.com')


@web_app.route('/faq')
@web_app.route('/FAQ')
def faq():
    return redirect('http://support.patreon.com')
