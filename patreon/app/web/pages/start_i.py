from flask import session
from patreon.app.web import web_app
from patreon.app.web.view import StartI, render_iframe
from patreon.routing import redirect


@web_app.route('/start_i')
def start_i():
    if session.user_id == 0:
        return redirect('/')

    return render_iframe(
        inner=StartI
    )
