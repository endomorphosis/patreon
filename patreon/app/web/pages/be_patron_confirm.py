from flask import session, request
from patreon.app.web import web_app
from patreon.app.web.view import render_page, BePatronConfirmPage, BePatronSinglePage
from patreon.model.manager import user_mgr, feature_flag, new_pledge_mgr
from patreon.services.logging.unsorted import log_action
from patreon.routing import redirect, redirect_to_signup


@web_app.route('/bePatronConfirm')
def be_patron_confirm():
    creator_id = request.args.get('u')

    if not session.user_id:
        return redirect_to_signup(c=creator_id)

    if not creator_id:
        return redirect('/')

    creator = user_mgr.get(creator_id)
    if not creator.campaigns:
        return redirect('/')

    if not creator.campaigns[0].is_launched:
        return redirect('/')

    single_page_enabled = (
        feature_flag.get_abtest_assignment(
            'be_patron_single_page',
            request.ab_test_group_id,
            session.user_id
        )
        or new_pledge_mgr.patron_has_any_vat_pledges(session.user_id)
    )

    log_action(
        'load_be_patron_confirm',
        single_page_enabled=single_page_enabled,
        session_id=session.session_id,
        user_id=session.user_id
    )

    if single_page_enabled:
        return render_page(
            inner=BePatronSinglePage,
            title='Patron Setup',
            no_share=True,
            viewport='device-width,initial-scale=1',
            no_legacy_javascript=True,
            no_legacy_stylesheets=True,
            internal_page=2
        )
    else:
        return render_page(
            inner=BePatronConfirmPage,
            title='Patron Confirmation'
        )
