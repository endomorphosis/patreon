from patreon.app.web.view import render_page, InternalIndex
from patreon.model.request.route_wrappers import register_internal_page


@register_internal_page('/internal/index', category='index', display=False)
def internal_index():
    return render_page(
        inner=InternalIndex,
        title='Patreon: Payment Event Dashboard',
        viewport='device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no',
        height=None
    )
