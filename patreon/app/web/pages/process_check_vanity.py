from flask import request
from patreon.app.web import web_app
from patreon.model.manager import user_mgr
from patreon.services.logging.unsorted import log_json
from patreon.util.unsorted import jsencode
from patreon.model.request.route_wrappers import set_content_type_json


@web_app.route('/processCheckVanity')
@set_content_type_json
def process_check_vanity():
    vanity = request.args.get('vanity')

    return handle_process_check_vanity(vanity)


def handle_process_check_vanity(vanity):
    if not vanity or user_mgr.get_unique(vanity=vanity):
        if vanity:
            log_json({
                'verb': 'vanity_collision',
                'vanity': vanity
            })
        return jsencode('0')

    return jsencode(vanity)
