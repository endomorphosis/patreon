from flask import request
from patreon.app.web import web_app
from patreon.model.request.route_wrappers import set_content_type_json
from patreon.model.manager import forgot_password_mgr, user_mgr
from patreon.services.mail import send_template_email
from patreon.services.mail.templates import PasswordReset
from patreon.services.logging.unsorted import log_state_change, log_page_anomaly
from patreon.util.unsorted import jsencode


def handle_rest_auth_forgot_pass(email):
    user = user_mgr.get_unique(email=email)
    if not user:
        log_page_anomaly('post', 'bad_email')
        return jsencode({'error': 'Invalid email address!'}), 400
    if not user.Password and not user.FBID:
        log_page_anomaly('post', 'no_password')
        return jsencode({'error': 'Can\'t reset password!'}), 400
    if user.is_nuked:
        log_page_anomaly('post', 'nuked_user')
        return jsencode({'error': 'Invalid email address!'}), 400

    token = forgot_password_mgr.insert(user.user_id)
    log_state_change('password_reset_token', token, 'create', '/REST/auth/forgot_password_reset')
    send_template_email(PasswordReset(user, token))

    return jsencode(True)


@web_app.route('/REST/auth/forgot_password', methods=['POST'])
@set_content_type_json
def rest_auth_forgot_pass_route():
    email = request.form.get('email')
    return handle_rest_auth_forgot_pass(email)


@web_app.route('/REST/auth/forgot_password', methods=['GET'])
def forgot_password_get():
    return '', 405
