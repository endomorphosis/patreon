from flask import session
import patreon
from patreon.app.web import web_app
from patreon.model.manager.session_mgr import get_new_session_id
from patreon.routing import redirect
from patreon.services.logging.unsorted import log_state_change


@web_app.route('/logout')
def logout():
    web_app.session_interface.delete_session(session)
    log_state_change('session', session.session_id, 'delete', '/logout')
    session.session_id = get_new_session_id()
    session.logout()

    @patreon.model.request.call_after_request
    def force_session_cookie(response):
        web_app.session_interface.set_session_cookie(response, session.session_id)
        return response

    return redirect('/')
