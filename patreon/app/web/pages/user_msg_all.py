from patreon.app.web import web_app
from patreon.app.web.view import render_iframe, UserMessageAllContent
from patreon.model.request.route_wrappers import require_login


@web_app.route('/userMsgAll')
@require_login
def user_message_all(current_user_id):
    return render_iframe(
        inner=UserMessageAllContent,
        i_bg_color='#EAEAEA',
        title='Message History'
    )
