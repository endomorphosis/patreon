from flask import session, request

from patreon.app.web import web_app
from patreon.app.web.view import render_page, PaymentDeclined
from patreon.model.manager import user_mgr, rewards_give_mgr
from patreon.routing import redirect


@web_app.route('/payment-declined')
def payment_declined():
    if not session.user_id:
        return redirect('/')

    creator_id = request.args.get('creator_id')
    if creator_id is None:
        return redirect('/')

    creator = user_mgr.get(user_id=creator_id)
    if creator is None:
        return redirect('/')

    reward_relationship = rewards_give_mgr.get_most_recent_invoice_for_patron_and_creator(session.user_id, creator_id)

    if reward_relationship is None or not reward_relationship.declined:
        return redirect(request.headers.get('Referer', '/user?u=' +  str(creator.user_id)))

    rejected_card_id = reward_relationship.CardID

    return render_page(
        inner=PaymentDeclined,
        title="Patreon - Payment Declined",
        no_share=True,
        viewport='device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no',
        height=None,
        no_legacy_javascript=True,
        no_legacy_stylesheets=True,
        patreon_fonts_override=True,
        creator=creator,
        rejected_card_id=rejected_card_id
    )
