import dateutil.parser
from flask import request
from patreon.app.web import web_app
from patreon.model.manager import activity_mgr, youtube_import_mgr
from patreon.model.request.route_wrappers import require_login
from patreon.model.table import UserExtra
import patreon.services.youtube as youtube
from patreon.util.unsorted import get_int

YOUTUBE_VIDEO_PREFIX = 'https://www.youtube.com/watch?v='


@web_app.route('/processYoutubeImport', methods=['POST'])
@require_login
def import_youtube(current_user_id):
    no_import = get_int(request.args.get('no')) == 1
    if no_import:
        UserExtra.insert_on_duplicate_key_update({
            'UID': current_user_id,
            'FlagYoutube': 2
        })
        return '', 200

    youtube_url = request.form.get('YTUrl')
    if youtube_url is None:
        return '', 400

    youtube_username = get_channel_id(youtube_url)
    if not youtube_username:
        # capture both None and ""
        return '', 400

    queue_videos_for_import(current_user_id, youtube_username)

    UserExtra.insert_on_duplicate_key_update({
        'UID': current_user_id,
        'FlagYoutube': 1
    })
    return '', 200


def queue_videos_for_import(user_id, youtube_username):
    num_queued_videos = 0
    playlist_id = get_playlist_for_username(youtube_username) or get_playlist_for_channel_name(youtube_username)
    if playlist_id is None:
        return 0

    video_ids = get_video_ids_for_playlist(playlist_id)
    video_details = get_info_for_videos(video_ids)
    pending_imports = make_pending_imports(user_id, video_details)

    if len(pending_imports) == 0:
        return num_queued_videos

    skip_urls = find_already_imported_videos(user_id, pending_imports)
    prefix_size = len(YOUTUBE_VIDEO_PREFIX)
    skip_ids = set([url[prefix_size:] for url in skip_urls])
    for video in pending_imports:
        if video['VideoID'] not in skip_ids:
            youtube_import_mgr.add(video)
            num_queued_videos += 1

    return num_queued_videos


# YOUTUBE API CALLS
def get_playlist_for_username(username):
    channel_data = youtube.get_user_channel_by_username(username)
    if not channel_data['items']:
        return None
    return channel_data['items'][0]['contentDetails']['relatedPlaylists']['uploads']


def get_playlist_for_channel_name(channel_name):
    channel_data = youtube.get_user_channel_by_channel_name(channel_name)
    if not channel_data or not channel_data['items']:
        return None
    return channel_data['items'][0]['contentDetails']['relatedPlaylists']['uploads']


# YOUTUBE DATA CONSUMERS
def get_video_ids_for_playlist(playlist_id):
    response_data = youtube.get_playlist_items(playlist_id, None)
    feed_items = response_data['items']
    next_token = response_data.get('nextPageToken')

    # grab up to 9 more pages for total 10 pages
    for i in range(9):
        if next_token is None:
            break

        response_data = youtube.get_playlist_items(playlist_id, next_token)
        if len(response_data['items']) == 0:
            break

        feed_items.extend(response_data['items'])
        next_token = response_data.get('nextPageToken')

    return [item['contentDetails']['videoId'] for item in feed_items]


def get_info_for_videos(video_ids):
    chunk_size = 25
    start_index = 0
    end_index = chunk_size
    all_info = []

    while start_index < len(video_ids):
        chunk = video_ids[start_index:end_index]
        videos_info = youtube.get_details_for_videos(chunk)
        all_info.extend(videos_info['items'])
        start_index = end_index
        end_index += chunk_size

    return all_info


def select_largest_thumbnail(thumbnails):
    # the "default" key is the only key we can expect to exist
    largest_thumbnail = thumbnails['default']
    for thumbnail in thumbnails.values():
        if largest_thumbnail['width'] < thumbnail['width']:
            largest_thumbnail = thumbnail
    return largest_thumbnail


def make_pending_imports(user_id, videos):
    pending_imports = []
    for video in videos:
        if not video['id']:
            continue

        snippet = video['snippet']
        pending_imports.append({
            'UID': user_id,
            'VideoID': video['id'],
            'Title': snippet['title'],
            'Content': snippet['description'],
            'Url': 'https://youtube.com/watch?v=' + video['id'],
            'ImageUrl': select_largest_thumbnail(snippet['thumbnails'])['url'],
            'Created': dateutil.parser.parse(snippet['publishedAt'])
        })
    return pending_imports


def get_channel_id(youtube_url):
    lower_url = youtube_url.lower()
    search_substrings = [
        'youtube.com/user/',
        'youtube.com/channel/',
        'youtube.com/c/',
        'youtube.com/',
    ]

    scan_index = None
    for search_string in search_substrings:
        try:
            scan_index = len(search_string) + lower_url.index(search_string)
        except ValueError:
            continue
        break
    else:
        return None

    end_chars = set(['?', '#', '/'])
    for i in range(scan_index, len(lower_url)):
        if lower_url[i] in end_chars:
            return youtube_url[scan_index:i]
    return youtube_url[scan_index:]


def find_already_imported_videos(creator_id, videos):
    video_urls = [
        YOUTUBE_VIDEO_PREFIX + video['VideoID']
        for video in videos
        ]

    duplicates = activity_mgr.find_posts_by_user_id_and_link_urls(
        creator_id,
        video_urls
    )
    return [d.link_url for d in duplicates]
