from flask import request
from patreon.app.web import web_app
from patreon.model.manager import profile_mgr, user_mgr, settings_mgr
from patreon.model.request.route_wrappers import require_login
from patreon.services.logging.unsorted import log_state_change
from patreon.util.unsorted import get_int, split_name
from patreon.routing import redirect


@web_app.route('/processSettings', methods=['POST'])
@require_login
def process_settings_post(current_user_id):
    get_sect = get_int(request.args.get('sect'))
    current_user = user_mgr.get(current_user_id)

    if get_sect == 1:
        legacy_image_url = request.form.get('profileImgUrl')
        if legacy_image_url and not legacy_image_url.startswith('http'):
            legacy_image_url = 'https:' + legacy_image_url

        photo_key = request.form.get('photo_key')
        post_update_image = request.form.get('uploadThumb')
        if (photo_key or legacy_image_url) and post_update_image:
            profile_mgr.update_profile_image(
                user_id=current_user.user_id,
                photo_key=photo_key,
                legacy_image_url=legacy_image_url,
                width=get_int(request.form.get('w')),
                height=get_int(request.form.get('h')),
                x=get_int(request.form.get('x1')),
                y=get_int(request.form.get('y1'))
            )

        vanity = request.form.get('uname')
        fname, lname = split_name(request.form.get('name'))
        about = request.form.get('settingsAbout')

        facebook = request.form.get('settingsFacebook')
        twitter = request.form.get('settingsTwitter')
        youtube = request.form.get('settingsYoutube')

        new_password = request.form.get('passwordNR')
        old_password = request.form.get('oldPassword')
        new_password_confirmation = request.form.get('confirmPasswordNR')

        email = request.form.get('email')
        if email:
            email = email.strip()

        latitudes = request.form.getlist('postlat[]')
        longitudes = request.form.getlist('postlng[]')
        location_names = request.form.getlist('postlocation[]')

        extra_redirect_args = user_mgr.sanitize_and_update_user_params(
            current_user, vanity=vanity, fname=fname, lname=lname, about=about,
            facebook=facebook, twitter=twitter, youtube=youtube,
            new_password=new_password, old_password=old_password,
            new_password_confirmation=new_password_confirmation, email=email,
            longitudes=longitudes, latitudes=latitudes,
            location_names=location_names
        )

        return redirect('/settings?saved=1&' + extra_redirect_args)

    elif get_sect == 3:
        post_patron_update = request.form.get('patronUpdate')
        post_post_update = request.form.get('postUpdate')
        post_comment_update = request.form.get('commentUpdate')
        post_email_update = request.form.get('emailUpdate')
        post_privacy_update = request.form.get('privacyUpdate')
        post_miles_update = request.form.get('milesUpdate')
        post_email_cids = request.form.getlist('emailCids[]')
        post_email_paid = request.form.getlist('emailPaid[]')
        post_email_post = request.form.getlist('emailPost[]')

        log_state_change('user', current_user.user_id, 'update', '/processSettings')
        settings_mgr.update_settings(
            user_id=current_user.user_id, actor_id=current_user.user_id,
            privacy=post_privacy_update, email_new_patron=post_patron_update,
            email_new_comment=post_comment_update,
            email_patron_post=post_post_update, email_update=post_email_update,
            email_goal=post_miles_update, email_creator_ids=post_email_cids,
            email_creator_paid=post_email_paid,
            email_creator_post=post_email_post
        )

        return redirect('/settings?saved=1')
    else:
        return redirect('/settings')


@web_app.route('/processSettings', methods=['GET'])
def process_settings_get():
    return '', 405
