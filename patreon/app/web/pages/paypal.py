from urllib import parse

from flask import request
import patreon
from patreon.app.web import web_app
from patreon.model.manager import card_mgr
from patreon.model.request.route_wrappers import require_login
from patreon.routing import Redirect
from patreon.util.unsorted import get_float, url_is_patreon, append_get_var


@web_app.route('/paypalStart', methods=['POST'])
@require_login
def paypal_start(current_user_id):
    creator_id = request.form.get('cid')
    amount = get_float(request.form.get('amt'))
    express_checkout_token = card_mgr.get_express_checkout_token(creator_id, amount)
    form_data = request.form.copy()
    form_data['referrer_url'] = request.referrer

    card_mgr.save_paypal_state(current_user_id, request.form, express_checkout_token)

    url = patreon.config.paypal['url']
    if not url.endswith('/'):
        url += '/'

    raise Redirect('{}webscr&cmd=_express-checkout&token={}'.format(url, express_checkout_token))


@web_app.route('/paypalStart', methods=['GET'])
@require_login
def paypal_start_get(current_user_id):
    return '', 405


@web_app.route('/paypalReturn')
@require_login
def paypal_return(current_user_id):
    express_checkout_token = request.args.get('token')
    success = request.args.get('suc')

    paypal_info = card_mgr.retrieve_paypal_state(current_user_id)

    redirect_url = '/'
    if paypal_info:
        redirect_url = paypal_info.get('referrer_url') or redirect_url

    redirect_url = append_get_var(redirect_url, 'paypal_failed=1')

    if not success:
        raise Redirect(redirect_url)

    if not express_checkout_token:
        # This is one of those "Should never happen" errors.
        # A user doing this is in a bad state or manipulating the url
        raise Redirect(redirect_url)

    if not paypal_info:
        # This is one of those "Should never happen" errors.
        # A user doing this is in a bad state or manipulating the url
        raise Redirect(redirect_url)

    form_data = paypal_info.post_info_json

    paypal_info.delete()
    card_id = card_mgr.save_card_from_payment_api(current_user_id, express_checkout_token)

    if form_data.get('addnew') == 1:
        url = '/paymentSettings?paypal=1'
    else:
        get_params = {
            'street1': form_data.get('sStreet'),
            'street2': form_data.get('sStreet2'),
            'city': form_data.get('sCity'),
            'state': form_data.get('sState'),
            'zip': form_data.get('sZip'),
            'country': form_data.get('sCountry'),
            'rewardId': form_data.get('rid'),
            'amount': form_data.get('amt'),
            'u': form_data.get('cid'),
            'pledgeCap': form_data.get('pledge_cap'),
            'cardId': card_id
        }

        url = form_data.get('new_pledge_host')

        if not url or not url_is_patreon(url):
            # Kinda hardcoded grossness, but better than an open redirect
            url = 'https://www.patreon.com/bePatron'

        url += '?' + parse.urlencode(get_params)

    raise Redirect(url)
