from flask import session

from patreon.app.web import web_app
from patreon.app.web.view import render_page, ForgotPassReset
from patreon.routing import redirect
from patreon.services.logging.unsorted import log_page_anomaly


@web_app.route('/forgetPassReset')
def forgot_pass_reset():
    if session.user_id != 0:
        log_page_anomaly('get', 'authorized')
        return redirect('/')
    return render_page(
        inner=ForgotPassReset,
        title="Patreon: Forget Password",
        viewport='device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no',
        height=None,
        no_legacy_javascript=True,
        no_legacy_stylesheets=True,
        patreon_fonts_override=True
    )
