from flask import request
import patreon
from patreon.exception.auth_errors import UnauthorizedAdminFunction
from patreon.model.manager import zoho_campaigns_mgr, campaign_mgr, pledge_mgr
from patreon.model.request.route_wrappers import require_admin, register_internal_page
from patreon.routing import redirect
from patreon.util.unsorted import jsencode


@require_admin
def handle_zoho(campaign_id, zoho_id):
    old_zoho = zoho_campaigns_mgr.get(campaign_id=campaign_id, zoho_id=None)

    if not old_zoho:
        creator = campaign_mgr.get(campaign_id=campaign_id).users[0]
        zoho_campaigns_mgr.insert(
            campaign_id,
            zoho_id,
            pledge_mgr.get_patron_count_by_creator_id(creator_id=creator.user_id) >= 2
        )
    else:
        old_zoho.update({
            'zoho_id': zoho_id
        })

    data = {
        'campaign_id': campaign_id
    }

    patreon.services.aws.sqs.put('prodweb-zoho_update', jsencode(data))


@register_internal_page('/internal/zoho_link', methods=['POST'], display=False)
def link_zoho_id_to_account():
    zoho_id = request.form.get('zoho_id')
    campaign_id = request.form.get('campaign_id')
    try:
        handle_zoho(campaign_id, zoho_id)
    except UnauthorizedAdminFunction:
        return '', 403

    return redirect(request.referrer)
