from patreon.app.web import web_app
from patreon.app.web.view import ChannelPage
from patreon.model.request.route_wrappers import add_nocache_headers


@web_app.route('/channel')
@web_app.route('/channel.php')
@add_nocache_headers
def channel():
    return ChannelPage().render()
