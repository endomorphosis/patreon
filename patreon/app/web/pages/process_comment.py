from flask import request
from patreon.app.web import web_app
from patreon.exception import APIException
from patreon.exception.comment_errors import InvalidComment
from patreon.exception.post_errors import PostNotFound
from patreon.model.manager import comment_mgr
from patreon.model.request.route_wrappers import set_content_type_json, require_login
from patreon.services import analytics
from patreon.services.analytics_events import CommentEvent
from patreon.services.logging.unsorted import log_anomaly
from patreon.util.unsorted import jsencode, get_int


@web_app.route('/processComment', methods=['GET'])
def process_comment_get():
    return '', 405


@web_app.route('/processComment', methods=['POST'])
@set_content_type_json
@require_login
def process_comment_page(current_user_id):
    post_id = get_int(request.form.get('hid'))
    comment_text = request.form.get('inputComment')
    thread_id = get_int(request.form.get('threadid'))

    analytics.log_patreon_event(
        CommentEvent.ADD_EVENT,
        event_properties={
            'source': 'web_comments_v1',
            'post_id': post_id,
            'thread_id': thread_id,
            'user_id': current_user_id
        })

    return handle_process_comment(current_user_id, post_id, comment_text, thread_id)


def handle_process_comment(user_id, post_id, comment_text, thread_id):
    try:
        comment = comment_mgr.save_comment(
            user_id, post_id, comment_text, thread_id
        )
        if not comment:
            raise APIException()

        return jsencode({
            'CID': comment.comment_id,
            'HID': comment.post_id,
            'Comment': comment.comment_text,
            'ThreadID': comment.thread_id
        }), 200

    except InvalidComment:
        return jsencode({
            'error': 'inputComment is required'
        }), 400

    except PostNotFound:
        return jsencode({
            'error': 'hid is required'
        }), 400

    except APIException:
        log_anomaly('comment', None, None, 'create', 'random_failure')
        return jsencode({
            'error': 'Unable to comment'
        }), 500
