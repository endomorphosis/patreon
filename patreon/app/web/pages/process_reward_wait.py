from flask import session, request
from patreon.app.web import web_app
from patreon.model.manager import rewards_wait_mgr
from patreon.util.unsorted import get_int


@web_app.route('/processRewardWait', methods=['POST'])
def process_reward_wait():
    user_id = session.user_id
    reward_id = get_int(request.args.get('rid'))
    is_adding = request.args.get('add') == '1'

    if user_id == 0 or reward_id is None:
        return '', 400

    if is_adding:
        rewards_wait_mgr.add(user_id, reward_id)
    else:
        rewards_wait_mgr.delete(user_id, reward_id)

    return '', 200
