from patreon.app.web import web_app
from patreon.app.web.view import Toolbox, render_page


@web_app.route('/toolbox')
def toolbox():
    return render_page(
        inner=Toolbox,
        title='Patreon: Toolbox',
        page=None
    )


@web_app.route('/toolbox/<string:page>')
def toolbox_page(page=None):
    return render_page(
        inner=Toolbox,
        title='Patreon: Toolbox',
        page=page
    )

