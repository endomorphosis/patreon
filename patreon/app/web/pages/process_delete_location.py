from flask import request
from patreon.app.web import web_app
from patreon.model.manager import user_location_mgr
from patreon.model.request.route_wrappers import require_login
from patreon.util.unsorted import get_float


@web_app.route('/processDeleteLocation', methods=['POST'])
@require_login
def process_delete_location(current_user_id):
    latitude = get_float(request.args.get('lat'))
    longitude = get_float(request.args.get('lon'))

    if latitude is not None and longitude is not None:
        user_location_mgr.delete_by_location(current_user_id, latitude, longitude)

    # the php codebase always returns 200 at this endpoint
    return '', 200
