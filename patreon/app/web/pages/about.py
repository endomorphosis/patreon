from patreon.app.web import web_app
from patreon.app.web.view import AboutPage, render_page


@web_app.route('/about')
@web_app.route('/team')
def about():
    return render_page(
        inner=AboutPage,
        title='Patreon: About',
        viewport='device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no',
        height=None,
        no_legacy_javascript=True,
        no_legacy_stylesheets=True,
        patreon_fonts_override=True,
        no_share=True,
    )
