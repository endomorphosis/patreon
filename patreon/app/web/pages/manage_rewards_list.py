from patreon.app.web import web_app
from patreon.app.web.view import render_page, ManageRewardsList
from patreon.model.manager import user_mgr
from patreon.model.request.route_wrappers import require_login
from patreon.routing import redirect
from patreon.services.logging.unsorted import log_page_anomaly


@web_app.route('/manageRewardsList', methods=['GET'])
@require_login
def manage_rewards_list_get(current_user_id):
    current_user = user_mgr.get(user_id=current_user_id)
    if current_user.campaigns is None or len(current_user.campaigns) == 0:
        log_page_anomaly('get', 'no_campaign')
        return redirect('/create')

    campaign = current_user.campaigns[0]

    return render_page(
        inner=ManageRewardsList,
        title=current_user.full_name,
        image_url=current_user.image_url,
        url=current_user.canonical_url(),
        static_reduced=True,
        no_share=True,
        current_user=current_user,
        campaign=campaign
    )
