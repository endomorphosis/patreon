from patreon.app.web import web_app
from patreon.app.web.view import render_react_page, CreatorPage


@web_app.route('/creator-page')
def creator_page_pure():
    page_viewport = 'device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no'

    return render_react_page(
        inner=CreatorPage
    )
