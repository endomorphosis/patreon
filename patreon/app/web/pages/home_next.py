from flask import request, session

from patreon.app.web import web_app, view
from patreon.model.manager import action_mgr, campaign_mgr, follow_mgr, \
    pledge_mgr, post_mgr, user_mgr, curation_mgr, comment_mgr
from patreon.services.caching import memcached
from patreon.util.unsorted import get_int

home_posts_per_page_hack = 10


@web_app.route('/homeNext')
@web_app.route('/homeNext.php')
def home_next():
    # 1. Current User for ShareBox.
    current_user_id = session.user_id
    page = abs(get_int(request.args.get('p')) or 1)
    is_by_creator_filter = request.args.get('ty') != 'a'
    return _display_home_next_page_for_user(page, is_by_creator_filter, current_user_id)


@memcached(time_to_cache=180)
def _display_home_next_page_for_user(page, is_by_creator_filter, current_user_id):
    current_user = user_mgr.get_user(current_user_id)
    if not current_user:
        return ''

    # 2. Post Params.
    offset = (page - 1) * home_posts_per_page_hack

    # 3. Creators.
    pledges_to_creators = pledge_mgr.find_by_patron_id(current_user_id)
    creator_follows = follow_mgr.find_by_follower_id(current_user_id)
    creator_ids = [pledge.creator_id for pledge in pledges_to_creators] \
                  + [follow.followed_id for follow in creator_follows]
    if not creator_ids and page != 1:
        return ''

    # 4. Posts.
    campaign_ids = campaign_mgr.get_all_campaign_ids_for_user_ids(
        creator_ids
    )
    posts = post_mgr.get_posts_by_campaign_ids_paginated(
        campaign_ids,
        cursor=None,
        per_page=home_posts_per_page_hack,
        is_by_creator=is_by_creator_filter,
        visible_to_user_id=current_user_id,
        allow_community=False,
        offset_=offset
    )
    if not posts and page == 1:
        posts = curation_mgr.find_all_posts(20)
    elif not posts:
        return ''

    # 5. Commenters.
    post_ids = [post.activity_id for post in posts]
    commenters = comment_mgr.get_commenters_dict_for_posts(post_ids)

    # 6. Likes.
    likes = action_mgr.find_likes_by_user_id_creation_dict(current_user_id)

    return ''.join([
        view.ShareBox(
            post, current_user, commenters, likes, force_columns=1,
            no_creator_name=False, show_comments=True
        ).render()
        for post in posts
    ])
