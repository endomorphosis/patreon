from flask import request
from patreon.app.web import web_app
from patreon.model.manager import user_mgr
from patreon.model.table import Comment
from patreon.model.request.route_wrappers import set_content_type_json, require_login
from patreon.services import analytics
from patreon.services.analytics_events import CommentEvent
from patreon.services.spam import is_spam
from patreon.services.logging.unsorted import log_anomaly, log_state_change
from patreon.util import datetime
from patreon.util.unsorted import jsencode, get_int


def handle_process_comment_edit(user_id, comment_id, comment_text):
    user = user_mgr.get(user_id=user_id)
    comment = Comment.get(comment_id=comment_id)
    if not user or not comment or not comment_text:
        return jsencode({'error': 'Invalid parameters'}), 400

    if user.UID != comment.UID:
        log_anomaly('comment', comment.CID, comment.UID, 'edit', 'unauthorized')
        return jsencode({'error': 'Not authorized'}), 403

    update_dict = {
        'Comment': comment_text,
        'EditedAt': datetime.datetime.now(),
        'is_spam': is_spam(comment_text, user.email, user.full_name)
    }

    comment.update(update_dict)
    log_state_change('comment', comment.CID, 'update', '/processCommentEdit')

    return jsencode({
        'CID': comment.CID,
        'HID': comment.HID,
        'Comment': comment_text,
        'ThreadID': comment.ThreadID
    }), 200


@web_app.route('/processCommentEdit', methods=['POST'])
@set_content_type_json
@require_login
def process_comment_edit_page(current_user_id):
    comment_id = get_int(request.form.get('cid'))
    comment_text = request.form.get('new_message')

    analytics.log_patreon_event(
        CommentEvent.EDIT_EVENT,
        event_properties={
            'source': 'web_comments_v1',
            'comment_id': comment_id,
            'user_id': current_user_id
        })

    return handle_process_comment_edit(current_user_id, comment_id, comment_text)


@web_app.route('/processCommentEdit', methods=['GET'])
def process_comment_edit_get():
    return '', 405
