import patreon
from patreon.app.web import web_app
from patreon.model.request.route_wrappers import register_internal_page
from patreon.app.web.view import render_page, CreatorQueue

@register_internal_page('/internal/creator_queue', name='Creator Queue', category='Tools')
def internal_creator_queue():
    return render_page(
        inner=CreatorQueue,
        title='Patreon: Creator Queue, Approve new creators for Patreon Feeds',
        viewport='device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no',
        height=None,
        no_legacy_javascript=True,
        no_legacy_stylesheets=True,
        patreon_fonts_override=True
    )
