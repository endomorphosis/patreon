from patreon.app.web import web_app
from patreon.app.web.view import Subbable, render_page


@web_app.route('/subbable')
def subbable():
    return render_page(
        inner=Subbable,
        title='Welcome Subbable!',
        viewport='device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no',
        height=None,
        no_legacy_javascript=True,
        no_legacy_stylesheets=True,
        patreon_fonts_override=True,
        no_share=True,
    )
