from patreon.app.web import web_app
from patreon.app.web.view import render_iframe, Transactions
from patreon.model.manager import user_mgr
from patreon.model.request.route_wrappers import require_login


"""
Porting from transaction_i.php
"""


@web_app.route('/transactions_i', methods=['GET'])
@require_login
def transactions_i_get(current_user_id):
    current_user = user_mgr.get(user_id=current_user_id)

    return render_iframe(
        inner=Transactions,
        title=current_user.full_name,
        image_url=current_user.image_url,
        url=current_user.canonical_url,
    )
