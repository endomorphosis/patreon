import patreon
from patreon.app.web import web_app
from patreon.app.web.view import render_page, FeaturedContent

PAGE_INDEX = patreon.globalvars.get('PAGE_INDEX')


@web_app.route('/featured')
def featured():
    return render_page(
        inner=FeaturedContent,
        title='Patreon',
        os_page=PAGE_INDEX,
    )
