from flask import request
from patreon.app.web.view import render_page, CaptchaControls
from patreon.model.request.route_wrappers import register_internal_page
from patreon.routing import redirect
from patreon.services import captcha
from patreon.util.unsorted import get_int


@register_internal_page('/internal/captcha_controls', name='Captcha Controls', category='Tools')
def show_captcha_services():
    return render_page(
        inner=CaptchaControls,
        title='Patreon: Captcha control'
    )


@register_internal_page('/internal/captcha_controls', methods=['POST'], name='Captcha Controls', category='Tools')
def edit_captcha_controls():
    service = request.form.get('service')
    if service not in captcha.global_services and service not in captcha.per_id_services:
        return redirect('/internal/captcha_controls?err=1')
    id = request.form.get('id')
    enable = request.form.get('enable')
    if get_int(enable) == 1:
        captcha.enable_captcha_for_service(service, id)
    else:
        captcha.disable_captcha_for_service(service, id)
    return redirect('/internal/captcha_controls')
