from flask import request
from patreon.app.web import web_app
from patreon.app.web.view import render_page, Create2Page
from patreon.model.request.route_wrappers import require_login
from patreon.model.manager import campaign_mgr, user_mgr
from patreon.routing import redirect
from patreon.services.logging.unsorted import log_state_change, log_page_anomaly


@web_app.route('/create2', methods=['GET'])
@require_login
def create2_get(current_user_id):
    current_user = user_mgr.get(user_id=current_user_id)

    campaign = campaign_mgr.try_get_campaign_by_user_id_hack(current_user.user_id)
    if not campaign:
        log_page_anomaly('get', 'no_campaign')
        return redirect('/create')

    return render_page(
        inner=Create2Page,
        title='Creator Setup',
        no_share=True,
        current_user=current_user,
        campaign=campaign
    )


def handle_create2_post(user_id, about, video_url, video_embed):
    campaign_id = campaign_mgr.try_get_campaign_id_by_user_id_hack(user_id)
    if not campaign_id:
        log_page_anomaly('post', 'no_campaign')
        return '', 400

    campaign_mgr.update_campaign(
        campaign_id,
        summary=about,
        video_url=video_url,
        video_embed=video_embed
    )

    log_state_change('campaign', campaign_id, 'update', '/create2')

    redirect('/create3')


@web_app.route('/create2', methods=['POST'])
@require_login
def create2_post(current_user_id):
    about       = request.form.get('about')
    video_url   = request.form.get('videoUrl')
    video_embed = request.form.get('shareEmbed')

    return handle_create2_post(current_user_id, about, video_url, video_embed)
