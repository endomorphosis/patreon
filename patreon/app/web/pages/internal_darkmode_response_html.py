from flask import request
import patreon
from patreon.model.request.route_wrappers import register_internal_page


@register_internal_page('/internal/darkmode/response_html', display=False)
def darkmode_view_response_html():
    response_id = int(request.args.get('response_id'))
    return patreon.model.internal.darkmode.get_response_html(response_id)
