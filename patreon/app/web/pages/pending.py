from flask import request
from patreon.app.web import web_app
from patreon.app.web.view import render_page, Pending
from patreon.model.manager import user_mgr, rewards_give_mgr, bill_mgr, campaign_mgr
from patreon.model.request.route_wrappers import require_login
from patreon.routing import redirect
from patreon.services.logging.unsorted import log_state_change
from patreon.util.unsorted import get_int


@web_app.route('/pending', methods=['GET'])
@require_login
def pending_get(current_user_id):
    current_user = user_mgr.get(user_id=current_user_id)

    return render_page(
        inner=Pending,
        title=current_user.full_name,
        image_url=current_user.image_url,
        url=current_user.canonical_url,
        current_user=current_user
    )
