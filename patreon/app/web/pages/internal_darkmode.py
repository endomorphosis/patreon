from patreon.app.web.view import render_page, DarkmodeIndexContent
from patreon.model.request.route_wrappers import register_internal_page


@register_internal_page('/internal/darkmode', name='Darkmode')
def darkmode_index():
    return render_page(
        inner=DarkmodeIndexContent,
        title='Darkmode Pages',
        no_share=True
    )
