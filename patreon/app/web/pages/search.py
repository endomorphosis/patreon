from flask import request
import patreon
from patreon.app.web import web_app
from patreon.app.web.view import SearchPage, render_page
from patreon.services.logging.unsorted import log_json


@web_app.route('/search')
@web_app.route('/search.php')
def search():
    log_json({
        'verb': 'full_page_search',
        'query': request.args.get('q'),
        'sort': request.args.get('order'),
        'filter': request.args.get('t')
    })

    return render_page(
        inner=SearchPage,
        title='Patreon: Search',
        url='https://' + patreon.config.main_server,
        image_url='https://' + patreon.config.main_server + '/images/patreon_home.jpg'
    )
