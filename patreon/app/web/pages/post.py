from flask import session
from bs4 import BeautifulSoup

import patreon
from patreon.app.web import web_app
from patreon.app.web.view import render_page, PostPage
from patreon.model.request.route_wrappers import unauthed_memcache
from patreon.model.manager import activity_mgr, user_mgr, rewards_give_mgr, pledge_mgr
from patreon.routing import redirect
from patreon.services.logging.unsorted import log_page_anomaly

MAIN_SERVER = patreon.globalvars.get('MAIN_SERVER')
PAGE_SHARE = patreon.globalvars.get('PAGE_SHARE')


@web_app.route('/posts/<string:slug>-<int:post_id>')
@web_app.route('/posts/<int:post_id>')
@web_app.route('/creations/<int:post_id>')  # Deprecated
@unauthed_memcache
def post_show(post_id=None, slug=None):
    activity = activity_mgr.get(activity_id=post_id)

    if not activity:
        log_page_anomaly('get', 'nonexistant_activity')
        return redirect('/')

    page_uid = activity.campaign_user_ids[0]
    page_user = user_mgr.get(user_id=page_uid)

    if not page_user:
        log_page_anomaly('get', 'nonexistant_user')
        return redirect('/')

    reward_relationship = rewards_give_mgr.get_most_recent_invoice_for_patron_and_creator(session.user_id, page_uid)

    if reward_relationship is not None and reward_relationship.declined is True:
        return redirect('/payment-declined?creator_id={page_uid}&ru=/posts/{post_id}'.format(
            page_uid=page_uid,
            post_id=post_id
        ))

    pledge_relationship = pledge_mgr.get_pledge(session.user_id, page_uid)

    if pledge_relationship is not None and pledge_relationship.Invalid is not None:
        return redirect('/payment-declined?creator_id={page_uid}&ru=/posts/{post_id}'.format(
            page_uid=page_uid,
            post_id=post_id
        ))

    page_viewport = 'device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no'
    description = ''

    if activity.min_cents_pledged_to_view:
        description = 'Patrons only!'
        image_url = 'https://s3.amazonaws.com/patreon_public_assets/patron_only.jpg'
    else:
        if activity.activity_content is not None:
            description_as_html = BeautifulSoup(activity.activity_content)
            description = description_as_html.get_text()

        image_url = activity.image_large_url \
            or '//' + MAIN_SERVER + '/images/patron_home.jpg'

    return render_page(
        inner=PostPage,
        title=activity.activity_title or page_user.full_name + "'s Creation",
        desc=description,
        url=activity.patreon_url,
        key='',
        image_url=image_url,
        viewport=page_viewport,
        os_page=PAGE_SHARE,
        page_user=page_user,
        activity=activity
    )
