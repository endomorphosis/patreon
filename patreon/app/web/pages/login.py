from flask import session, request
import patreon
from patreon.app.web import web_app
from patreon.app.web.view import render_page, LoginForm
from patreon.routing import redirect


@web_app.route('/login')
@web_app.route('/login.php')
def login_form():
    if session.user_id:
        redirect_url = patreon.routing.sanitize_redirect_url(request.args.get('ru'))
        return redirect(redirect_url)

    return render_page(
        inner=LoginForm,
        title="Patreon - Login",
        no_share=True,
        viewport='device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no',
        height=None,
        no_legacy_stylesheets=True,
        patreon_fonts_override=True
    )
