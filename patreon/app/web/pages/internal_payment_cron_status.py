from patreon.app.web.view import render_page, InternalPaymentCronStatus
from patreon.model.request.route_wrappers import register_internal_page


@register_internal_page('/internal/payment_cron_status', name="Payment Cron Status Page", category="Dashboards")
def show_cron_status():
    return render_page(
        inner=InternalPaymentCronStatus,
        title='Patreon: Payment Event Dashboard',
        viewport='device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no',
        height=None,
        no_legacy_javascript=True,
        no_legacy_stylesheets=True
    )
