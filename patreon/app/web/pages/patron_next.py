from flask import request, session
from patreon.app.web import web_app
from patreon.app.web.view import ShareBox
from patreon.model.manager import post_mgr, action_mgr, activity_mgr, user_mgr,\
    rewards_give_mgr, comment_mgr
from patreon.util.unsorted import get_int


def _get_share_box_html(current_user_id, post_ids=None, posts=None):
    posts = posts or post_mgr.get_posts(post_ids)
    post_ids = post_ids or [post.activity_id for post in posts]

    return ''.join([
        ShareBox(
            post,
            current_user=user_mgr.get(current_user_id),
            commenters=comment_mgr.get_commenters_dict_for_posts(post_ids),
            user_likes=action_mgr.find_likes_by_user_id_creation_dict(current_user_id)
        ).render()
        for post in posts
    ])


@web_app.route('/patronNext')
@web_app.route('/patronNext.php')
def patron_next():
    current_user_id = session.user_id
    page = abs(get_int(request.args.get('p')) or 1)
    template_type = request.args.get('ty')
    user_id = request.args.get('u')
    if not user_mgr.get_user(user_id):
        return ''

    if template_type == 'a':
        posts = activity_mgr.find_posts_by_user_id_paginated(user_id, page).all()
        return _get_share_box_html(current_user_id, posts=posts)

    elif template_type == 'c':
        posts = rewards_give_mgr.find_supported_posts_by_user_id_paginated(user_id, page)
        return _get_share_box_html(current_user_id, posts=posts)

    elif template_type == 'l':
        post_ids = action_mgr.find_likes_by_user_id_paginated(user_id, page)
        return _get_share_box_html(current_user_id, post_ids=post_ids)

    return ''
