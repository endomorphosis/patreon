import patreon
from patreon.app.web import request
from patreon.app.web.view import FeatureFlags, render_page
from patreon.exception.auth_errors import UnauthorizedAdminFunction
from patreon.model.manager import feature_flag, user_mgr
from patreon.model.request.route_wrappers import require_admin, \
    set_content_type_json, register_internal_page
from patreon.util.unsorted import get_int, jsencode


def flag_info(feature_name):
    return {
        'name': feature_name,
        'cutoff': int(feature_flag.get_percentage_enabled(feature_name) * 100),
        'disabled': feature_flag.is_globally_disabled(feature_name)
    }


def flag_assignment_info(feature_name, user_id):
    return {
        "name": feature_name,
        "enabled": feature_flag.is_enabled(feature_name, group_id=request.ab_test_group_id, user_id=user_id),
        "forced": feature_flag.is_user_enabled(feature_name, user_id)
    }


@require_admin
def visible_flag_info():
    visible_flags = feature_flag.publicly_visible_feature_flags()
    return [flag_info(f) for f in visible_flags]


@require_admin
def user_flag_info(user_id):
    visible_flags = feature_flag.publicly_visible_feature_flags()
    return [flag_assignment_info(f, user_id) for f in visible_flags]


@require_admin
def update_feature_flag(feature_name, cutoff, is_disabled):
    patreon.pstore.update()

    message_format = (
        'Changing feature flag "{feature_name}" to ({cutoff}%, {enable_state}). '
        'Was ({old_cutoff}%, {old_enable_state})'
    )
    message = message_format.format(
        feature_name=feature_name,
        cutoff=cutoff,
        enable_state='off' if is_disabled else 'on',
        old_cutoff=int(100 * feature_flag.get_percentage_enabled(feature_name)),
        old_enable_state='off' if feature_flag.is_globally_disabled(feature_name) else 'on'
    )

    feature_flag.set_global_disabled_state(feature_name, is_disabled)
    feature_flag.set_percentage_enabled(feature_name, cutoff / 100.0)

    if not patreon.is_test_environment():
        channel = 'bots'
        api_key = patreon.config.slack_api_key
        url_format = "https://patreon.slack.com/services/hooks/slackbot?token={0}&channel=%23{1}"
        http = urllib3.PoolManager()
        http.request('POST', url_format.format(api_key, channel), body=message)


@require_admin
def update_user_feature_flag(feature_name, force_on, user_id):
    if force_on:
        feature_flag.set_user_enabled(feature_name, user_id)
    else:
        feature_flag.remove_user_enabled(feature_name, user_id)


@register_internal_page('/internal/feature_flags', name="Feature Flags")
def flags_get():
    return render_page(
        inner=FeatureFlags,
        title='Patreon: Feature Flags',
        flag_info=visible_flag_info()
    )


@register_internal_page('/internal/feature_flags', methods=['POST'], display=False)
def flags_post():
    if not request.form:
        return '', 400

    necessary_arguments = ['feature_name', 'cutoff', 'is_disabled']
    for arg_name in necessary_arguments:
        if arg_name not in request.form:
            return '', 400

    try:
        cutoff = int(request.form['cutoff'])
    except ValueError:
        return '', 400

    if cutoff < 0 or cutoff > 100:
        return '', 400

    feature_name = request.form['feature_name']
    is_disabled = request.form['is_disabled'] == 'true'
    update_feature_flag(feature_name, cutoff, is_disabled)
    return '', 204


@register_internal_page('/internal/user_feature_flags', display=False)
@set_content_type_json
def user_flags_get():
    identifier = request.args.get('identifier')
    user = (
        user_mgr.get(identifier) or
        user_mgr.get_unique(email=identifier) or
        user_mgr.get_unique(vanity=identifier)
    )

    if not user:
        return jsencode({}), 400

    try:
        return jsencode({
            "user_id": user.user_id,
            "flag_info": user_flag_info(user.user_id)
        })
    except UnauthorizedAdminFunction:
        return jsencode({}), 403


@register_internal_page('/internal/user_feature_flags', methods=['POST'], display=False)
def user_flags_post():
    if not request.form:
        return '', 400

    feature_name = request.form.get('feature_name')
    user_id = get_int(request.form.get('user_id'))
    force_on = request.form.get('force_test_group')
    if feature_name is None or force_on not in ('true', 'false') or user_id == 0:
        return '', 400

    update_user_feature_flag(feature_name, force_on == 'true', user_id)
    return '', 204
