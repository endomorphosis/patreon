from patreon.app.web import web_app
from patreon.app.web.view import CreateStart


@web_app.route('/create_start')
def create_start():
    return CreateStart().render()
