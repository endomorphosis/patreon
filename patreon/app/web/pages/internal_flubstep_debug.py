from patreon.app.web import web_app
from patreon.app.web.view import render_iframe, FlubstepDebug


@web_app.route('/internal/flubstep_debug')
def flubstep_debugger():
    return render_iframe(
        inner=FlubstepDebug,
        title='Flubstep Debugger',
        no_share=True,
    )
