from flask import request, session
from patreon.app.web import web_app
from patreon.model.request.route_wrappers import set_content_type_json
from patreon.model.manager.csrf import get_csrf_ticket
from patreon.util.unsorted import jsencode


@web_app.route('/REST/auth/CSRFTicket')
@set_content_type_json
def csrf_ticket():
    ticket = get_csrf_ticket(request.args.get('uri'), session.user_id, session.csrf_token)
    return jsencode(ticket)
