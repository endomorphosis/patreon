import patreon
from patreon.app.web import web_app
from patreon.routing import redirect


@web_app.route('/ios-beta')
def ios_beta():
    return redirect(
        patreon.pstore.get('ios_beta_link') or 'https://betas.to/E9Uymjo7'
    )


@web_app.route('/android-beta')
def android_beta():
    return redirect(
        patreon.pstore.get('android_beta_link') or 'https://betas.to/fgByg1cf'
    )