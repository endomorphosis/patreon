from flask import request
from patreon.app.web import web_app
from patreon.app.web.view import render_page, Create3
from patreon.model.manager import campaign_mgr, goal_mgr, user_mgr, queue_mgr
from patreon.model.request.route_wrappers import require_login
from patreon.routing import redirect
from patreon.services.logging.unsorted import log_page_anomaly
from patreon.util import collections


@web_app.route('/create3', methods=['GET'])
@require_login
def create3_get(current_user_id):
    current_user = user_mgr.get(user_id=current_user_id)

    if len(current_user.campaigns_users) == 0:
        return redirect('/create')

    return render_page(
        inner=Create3,
        title='Creator Setup',
        no_share=True,
        current_user=current_user
    )


def handle_create3_post(user_id, goal_ids, goal_titles, goal_descriptions, goal_amounts):
    user_mgr.get_user_or_throw(user_id)

    campaign = campaign_mgr.try_get_campaign_by_user_id_hack(user_id)
    if not campaign:
        log_page_anomaly('post', 'no_campaign')
        return '', 400

    if not (len(goal_ids)
                == len(goal_titles)
                == len(goal_descriptions)
                == len(goal_amounts)):
        log_page_anomaly('post', 'variable_mismatch')
        return redirect('/create3')

    goals = collections.zip_lists_to_dicts(
        ['goal_id', 'goal_title', 'goal_desc', 'goal_amount'],
        [goal_ids, goal_titles, goal_descriptions, goal_amounts]
    )

    success = goal_mgr.set_goals(campaign.campaign_id, user_id, goals)
    if not success:
        redirect('/create3')

    queue_mgr.enqueue_campaign_for_update(campaign.campaign_id)

    redirect('/create4')


@web_app.route('/create3', methods=['POST'])
@require_login
def create3_post(current_user_id):
    new_goal_ids        = request.form.getlist('gid[]')
    goal_titles         = request.form.getlist('rTitle[]')
    goal_descriptions   = request.form.getlist('rDescription[]')
    goal_amounts        = request.form.getlist('rAmount[]')

    return handle_create3_post(
        current_user_id, new_goal_ids, goal_titles, goal_descriptions, goal_amounts
    )

