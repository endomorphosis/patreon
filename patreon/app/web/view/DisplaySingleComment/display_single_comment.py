from patreon.app.web.view import base
from patreon import globalvars


class DisplayComment(base.View):
    def init(self, current_user_id, comment, creator, commenter, total_comments,
             level=0, comments_rendered=[]):
        self.current_user_id = current_user_id
        self.comment = comment
        self.level = level
        self.creator = creator
        self.commenter = commenter
        self.comments_rendered = comments_rendered
        self.total_comments = total_comments

    def data(self):
        thread = ' thread' if self.level > 0 else ''
        comment_hide_threshold = self.total_comments \
                                 - globalvars.get('COMMENTS_BEFORE_HIDE')

        return {
            'level': self.level,
            'comment': self.comment,
            'commenter': self.commenter,
            'thread': thread,
            'indent_px': self.level * 10,
            'current_user_id': self.current_user_id,
            # This is a kludge of the highest order.
            'comments_rendered': self.comments_rendered,
            'comment_hide_threshold': comment_hide_threshold,
            'creator': self.creator
        }

    def template(self):
        return 'DisplaySingleComment/display_single_comment.html'
