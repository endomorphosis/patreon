from patreon.app.web.view import base


class LegacyVideoEmbedder(base.View):
    def __init__(self, campaign, video=None):
        self.campaign = campaign
        self.video = video

    def data(self):
        if self.video == "thanks":
            video_url = self.campaign.thanks_video_url
            video_embed = self.campaign.thanks_embed
        else:
            video_url = self.campaign.main_video_url
            video_embed = self.campaign.main_video_embed

        return {
            'embed_api': 'https://api.patreon.com/embed',
            'video_url': video_url,
            'video_embed': video_embed
        }

    def template(self):
        return "LegacyVideoEmbedder/legacy_video_embedder.html"
