import os
from flask import request
from patreon.app.web.view import base
from patreon.services import file_helper

partials_template = os.path.dirname(__file__) + "/views/{}.html"

sections = [
    {
        'key': 'build',
        'name': "Build",
        'title': "Build"
    },
    {
        'key': 'launch',
        'name': "Launch",
        'title': "Launch Day Tips"
    },
    {
        'key': 'growth',
        'name': "Growth",
        'title': "Maintain Your Growth"
    },
    {
        'key': 'stats',
        'name': "Stats",
        'title': "By the Numbers"
    },
    {
        'key': 'media',
        'name': "Media",
        'title': "Patreon Media"
    }
]


class Toolbox(base.View):
    def init(self, page):
        self.page = 'build'
        page = page or request.args.get('ftyp')
        for section in sections:
            if section['key'] == page:
                self.page = page

    def data(self):
        title = ''
        for section in sections:
            if section['key'] == self.page:
                title = section['title']
            section['url'] = "/toolbox/" + section['key']

        content = file_helper.get_file_contents(partials_template.format(self.page))

        return {
            'sections': sections,
            'active_section': self.page,
            'content': content,
            'title': title
        }

    def template(self):
        return "Toolbox/toolbox.html"
