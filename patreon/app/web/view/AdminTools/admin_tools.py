import flask
from patreon.app.web.view import base


class AdminTools(base.View):
    def data(self):
        admin_data = flask.g.get('admin_data', {})
        return {
            'admin_data': admin_data,
            'user_id': admin_data.get('user_id'),
            'campaign': admin_data.get('campaign')
        }

    def template(self):
        return "AdminTools/admin_tools.html"
