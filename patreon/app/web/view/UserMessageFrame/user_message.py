from flask import session, request

from patreon.app.web.view import base
from patreon.model.manager import message_mgr, user_mgr


class UserMessageContent(base.View):
    def data(self):
        page_user_id = int(request.args.get('u') or 0)
        if not page_user_id:
            # TODO(postport): Render empty page?
            pass
        page_user = user_mgr.get(user_id=page_user_id)
        current_user = user_mgr.get(user_id=session.user_id)
        all_messages = message_mgr.get_conversation(page_user_id, session.user_id)

        # Used by Message() to render user info.
        senders_by_id = {
            page_user_id: page_user,
            session.user_id: current_user
        }
        return locals()

    def template(self):
        return "UserMessageFrame/user_message.html"
