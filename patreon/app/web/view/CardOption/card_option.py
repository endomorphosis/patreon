from patreon.app.web.view import base
from flask import request, session

class CardOption(base.View):

    def init(self, card, user_pledge):
        self.card = card
        self.user_pledge = user_pledge

    def data(self):
        date = self.card.get('ExpirationDate')
        if date:
            expiration_date = date.strftime('%m/%y')
        else:
            expiration_date = None
        return {
            'card': self.card,
            'user_pledge': self.user_pledge,
            'expiration_date': expiration_date
            }

    def template(self):
        return "CardOption/card_option.html"
