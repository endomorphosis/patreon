from patreon.app.web.view import base
from patreon.util.format import cents_as_dollar_string
from patreon.util.unsorted import get_int


class Reward(base.View):
    def init(self, reward, existing_pledge, checked_reward=None):
        self.reward = reward
        self.rid = self.reward.reward_id
        self.reward_count = reward.count or 0
        self.existing_pledge = existing_pledge
        self.checked_reward = checked_reward

    def data(self):
        reward_id = self.reward.reward_id
        limit = self.reward.user_limit

        is_available, reserved, sold_out = True, False, False
        if limit and limit == self.reward_count:
            is_available = False
            if self.existing_pledge \
               and self.existing_pledge.reward_id == reward_id:
                reserved = True
            else:
                sold_out = True

        is_checked = reward_id == get_int(self.checked_reward) and not sold_out

        return {
            'reward': self.reward,
            'dollar_string': cents_as_dollar_string(self.reward.amount),
            'limit': limit,
            'remaining': limit - self.reward_count,
            'is_available': 'true' if is_available else 'false',
            'is_reserved': reserved,
            'is_sold_out': sold_out,
            'is_disabled': 'disabled' if sold_out else '',
            'is_checked': 'checked' if is_checked else ''
        }

    def template(self):
        return "BePatronReward/be_patron_reward.html"
