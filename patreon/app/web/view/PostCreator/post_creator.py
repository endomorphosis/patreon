from patreon.app.web.view import base


class PostCreator(base.View):
    def template(self):
        return 'PostCreator/post_creator.html'
