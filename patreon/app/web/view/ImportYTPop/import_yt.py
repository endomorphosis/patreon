from flask import session
from patreon.app.web.view import base
from patreon.model.manager import user_mgr


class ImportYt(base.View):
    def data(self):
        return {
            'current_user': user_mgr.get(user_id=session.user_id)
        }

    def template(self):
        return "ImportYTPop/import_yt.html"
