from patreon.app.web.view import base


class GuidelinesPage(base.View):
    def template(self):
        return "GuidelinesPage/guidelines.html"
