from patreon.app.web.view import base
from patreon.constants.money import LARGE_PLEDGE_CENTS
from patreon.model.manager import pledge_mgr, rewards_give_mgr


class Ribbon(base.View):
    def init(self, user, creator, small=True):
        self.user = user
        self.creator = creator
        self.small = small
        return

    def data(self):
        creator_id = self.creator.user_id
        patron_id = self.user.user_id

        pledge_days = pledge_mgr.find_days_supported_by_creator_id(creator_id).get(patron_id) or 0
        pledge_sum = rewards_give_mgr.pledge_sums_for_users_by_creator_id(creator_id).get(patron_id) or 0

        return {
            'user': self.user,
            'creator': self.creator,
            'large_pledge': pledge_sum > LARGE_PLEDGE_CENTS,
            'patron_days': pledge_days,
            'pledge_sum': pledge_sum,
            'small': self.small
        }

    def template(self):
        return "Ribbon/ribbon.html"
