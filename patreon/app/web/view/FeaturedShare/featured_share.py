from patreon.app.web.view import base
from patreon.model.manager import goal_mgr, user_mgr, pledge_mgr
from patreon.model.type import PostType
from patreon.util.format import format_date, cents_as_dollar_string, truncate
from patreon.util.unsorted import get_int


class FeaturedShare(base.View):
    def init(self, share):
        self.share = share

    def cache_key(self):
        # IMPORTANT: If you ever change the way FeaturedShare is rendered, you
        # need to update this key! Change the 1 to 2.
        return 'FeaturedShare:1:{}'.format(self.share.activity_id)

    def data(self):
        post = self.share
        creator_id = post.campaign_user_ids[0]
        creator = user_mgr.get(user_id=creator_id)

        post_title = post.activity_title \
                     or post.link_subject \
                     or "Post by " + creator.full_name

        # TODO(postport): remove inline html
        # TODO(postport): how secure is share['Content']? especially being truncated?
        if PostType.get(post.activity_type) is PostType.TEXT_ONLY:
            left_image_html = '<div class="cbImage cbNote">{}</div>'.format(
                truncate(post.activity_content, 120, '...')
            )
        else:
            image_large_url = post.image_large_url or ''
            left_image_html = '<div class="cbImage" style="background:#000 url(\'' \
                              + image_large_url \
                              + '\') no-repeat center center; background-size:cover;"></div>'

        patrons_count = pledge_mgr.get_patron_count_by_creator_id(creator_id)
        patrons_plural = 'patron' if patrons_count == 1 else 'patrons'
        pledge_total = get_int(pledge_mgr.get_pledge_total_by_creator_id(creator_id)) or 0

        if pledge_total / 100:
            if post.campaign.pay_per_name == 'month':
                per_text = 'for each month'
            else:
                per_text = 'for this creation'
        else:
            per_text = ''

        campaign_id = post.campaign.campaign_id
        unmet_goals = goal_mgr.find_unmet_goals_by_campaign_id(
            campaign_id, pledge_total
        )

        if unmet_goals:
            next_goal_amount = unmet_goals[0].amount_cents
            goal_percentage = pledge_total / next_goal_amount
        else:
            goal_percentage = 1

        goal_percentage = int(goal_percentage * 100)
        goal_width = int((goal_percentage / 100) * 180)
        # TODO(postport): remove magic numbers
        goal_width_remain = 198 - goal_width

        amount_pledged_string = cents_as_dollar_string(
            pledge_total
        )

        return {
            'can_show': True,  # TODO(implement)
            'post': post,
            'post_title': post_title,
            'creator': creator,
            'left_image_html': left_image_html,
            'published_at': format_date(post.published_at),
            'patrons_count': patrons_count,
            'patrons_plural': patrons_plural,
            'amount_pledged_string': amount_pledged_string,
            'per_text': per_text,
            'goal_percentage': goal_percentage,
            'goal_width': goal_width,
            'goal_width_remain': goal_width_remain,
            'pledge_total': pledge_total
        }

    def template(self):
        return "FeaturedShare/featured_share.html"
