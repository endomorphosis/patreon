from flask import request
from patreon.app.web.view import base
from patreon.model.manager import complete_pledge_mgr, rewards_give_mgr
from patreon.model.table import Activity


class PendingReward(base.View):
    def init(self, current_user, creator):
        self.current_user = current_user
        self.creator = creator

    def data(self):
        creator_id = self.creator.user_id
        patron_id = self.current_user.user_id

        if request.args.get('ty'):
            bills = rewards_give_mgr.find_processed_by_creator_id_user_id(
                creator_id, patron_id
            )
        else:
            bills = rewards_give_mgr.find_failed_and_pending_by_creator_id_user_id(
                creator_id, patron_id
            )

        # Filter bills for delete_posts
        Activity.get.prime([bill.activity_id for bill in bills])
        bills = [bill for bill in bills if bill.post]

        is_supporting = complete_pledge_mgr.get_pledge_by_patron_and_campaign_or_creator(
            patron_id=patron_id, creator_id=creator_id
        ).legacy is not None

        edit_pledge_url = "/bePatronConfirm?edit=1&u=" + str(creator_id)

        return {
            'creator': self.creator,
            'bills': bills,
            'is_supporting': is_supporting,
            'edit_pledge_url': edit_pledge_url
        }

    def template(self):
        return "PendingReward/pending_reward.html"
