from patreon.app.web.view import base


class LegalPage(base.View):
    def template(self):
        return "LegalPage/legal.html"
