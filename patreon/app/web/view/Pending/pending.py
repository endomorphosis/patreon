from flask import request
from patreon.app.web.view import base
from patreon.model.manager import user_mgr, rewards_give_mgr


class Pending(base.View):
    def init(self, current_user):
        self.current_user = current_user

    def data(self):
        if not request.args.get('ty'):
            reward_creator_ids = rewards_give_mgr.find_pending_and_failed_creators_by_user_id(self.current_user.UID)
        else:
            reward_creator_ids = rewards_give_mgr.find_processed_creators_by_user_id(self.current_user.UID)

        reward_creators = user_mgr.get_users(reward_creator_ids)

        return {
            'current_user': self.current_user,
            'reward_creators': reward_creators,
            'tab_type': request.args.get('ty')
        }

    def template(self):
        return "Pending/pending.html"
