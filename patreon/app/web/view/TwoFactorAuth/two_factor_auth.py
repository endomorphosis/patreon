from flask import session, request
from patreon.model.manager import user_mgr
import pyotp
from patreon.app.web.view import base


class TwoFactorAuth(base.View):
    def data(self):
        return {
            'secret': pyotp.random_base32(),
            'email': user_mgr.get(session.user_id).email,
            'err': request.args.get('err')
        }

    def template(self):
        return "TwoFactorAuth/two_factor_auth.html"
