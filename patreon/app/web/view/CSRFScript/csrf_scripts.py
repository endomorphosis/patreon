from patreon.app.web.view import base

class CSRFScripts(base.View):
    def template(self):
        return "CSRFScript/csrf_script.html"
