from patreon.app.web.view import base
from patreon.model.manager import category_mgr


class CreateCategories(base.View):
    def init(self, category_ids):
        self.category_ids = category_ids

    def data(self):
        return {
            'categories': [
                {
                    'title': category.name,
                    'id': 'cat' + str(category.category_id),
                    'value': category.category_id,
                    'offset': category.offset,
                    'is_selected': category.category_id in self.category_ids
                }
                for category in category_mgr.user_categories()
            ]
        }

    def template(self):
        return "CreateCategories/create_categories.html"
