from patreon.app.web.view import base
from patreon.model.manager import user_mgr


class DisplayUserSquare(base.View):
    def init(self, user_id, url=None, extra_class=None, target_blank=False):
        self.extra_class = extra_class
        self.user = user_mgr.get(user_id=user_id)
        self.target = '_blank' if target_blank else '_top'

    def data(self):
        return {
            'extra_class': self.extra_class or '',
            'target': self.target,
            'user': self.user
        }

    def template(self):
        return "DisplayUserSquare/display_user_square.html"
