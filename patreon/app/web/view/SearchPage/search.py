from patreon.app.web.view import base
from flask import request


class SearchPage(base.View):
    def data(self):
        SEARCH_API_ID = '27QC0IB0ER'
        return {
            'template_type': request.args.get('ty'),
            'SEARCH_API_ID': SEARCH_API_ID,
            'SEARCH_SERVER': 'https://' + SEARCH_API_ID + '.algolia.io/1/indexes/*/queries',
            'SEARCH_API_KEY': '83db986a08481e94088c75fd57d90118'  # Search-only (read-only) key.
        }

    def template(self):
        return "SearchPage/search.html"
