
import patreon

from flask import session

from patreon.app.web.view import base

class Meta(base.View):

    def data(self):
        return { 'meta': session.meta }

    def template(self):
        return "Meta/meta.html"