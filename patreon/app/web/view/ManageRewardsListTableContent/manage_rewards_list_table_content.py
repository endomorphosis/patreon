from flask import session, request
from patreon.app.web.view import base
from patreon.model.manager import reward_mgr, user_mgr, rewards_give_mgr
from patreon.model.table import RewardsGive, User, Pledge
from patreon.services.db import db_session


class ManageRewardsListTableContent(base.View):
    def data(self):
        current_user = user_mgr.get(user_id=session.user_id)
        sort = sort_query(request.args.get('srt'), request.args.get('up'))
        get_activity = request.args.get('hid')
        get_completed = request.args.get('c')
        if sort is None:
            rewards = reward_mgr.find_by_creator_id(current_user.UID)
            patrons = {}
            for reward in rewards:
                patrons[reward.RID] = patron_getter(current_user.UID, get_activity, get_completed, sort, reward.RID)
        else:
            rewards = None
            patrons = patron_getter(current_user.UID, get_activity, get_completed, sort)
        return {
            'rewards': rewards,
            'patrons': patrons,
            'lifetimes': rewards_give_mgr.find_pledge_sums_for_creator_by_user_id(current_user.UID)
        }

    def template(self):
        return "ManageRewardsListTableContent/manage_rewards_list_table_content.html"


def patron_getter(creator_id, get_activity, get_completed, sort, reward_id=None):
    if get_activity:
        if get_completed:
            complete = 1
        else:
            complete = 0
        # This should live somewhere else but it caused circular import hell so I left it pending a model rework
        query = db_session.query(RewardsGive.Pledge,
                                 User,
                                 Pledge.Created,
                                 Pledge.MaxAmount,
                                 RewardsGive.RID,
                                 RewardsGive.Status,
                                 RewardsGive.Street,
                                 RewardsGive.Street2,
                                 RewardsGive.City,
                                 RewardsGive.State,
                                 RewardsGive.Zip,
                                 RewardsGive.Country) \
            .join(User, User.UID == RewardsGive.UID) \
            .outerjoin(Pledge, (RewardsGive.CID == Pledge.CID) & (RewardsGive.UID == Pledge.UID)) \
            .filter(RewardsGive.CID == creator_id) \
            .filter(RewardsGive.Complete == complete) \
            .group_by(RewardsGive.UID) \
            .group_by(RewardsGive.CID)

    else:
        # This should live somewhere else but it caused circular import hell so I left it pending a model rework
        query = db_session.query(Pledge.Created,
                                 User,
                                 Pledge.Pledge,
                                 RewardsGive.RID,
                                 Pledge.MaxAmount,
                                 Pledge.Invalid.label('Status'),
                                 Pledge.Street,
                                 Pledge.Street2,
                                 Pledge.City,
                                 Pledge.State,
                                 Pledge.Zip,
                                 Pledge.Country) \
            .join(User, User.UID == Pledge.UID) \
            .outerjoin(RewardsGive, (RewardsGive.CID == Pledge.CID) & (RewardsGive.UID == Pledge.UID)) \
            .filter(Pledge.CID == creator_id) \
            .group_by(Pledge.UID) \
            .group_by(Pledge.CID)

    if sort is not None:
        query = query.order_by(sort)
    else:
        query = query.filter(RewardsGive.RID == reward_id)

    if get_activity:
        query = query.order_by(RewardsGive.Pledge.desc()).order_by(RewardsGive.Created.desc())
    else:
        query = query.order_by(Pledge.RID.desc()).order_by(Pledge.Pledge.desc()).order_by(Pledge.Created.desc())

    return query.all()


def sort_query(get_sort, get_up):
    if not get_sort:
        return None

    if get_sort == '1':
        sort = User.FName
    elif get_sort == '2':
        sort = User.Email
    elif get_sort == '3':
        sort = Pledge.Pledge
    elif get_sort == '4':
        sort = RewardsGive.Status
    elif get_sort == '5':
        sort = Pledge.Created
    else:
        return None

    if get_up:
        sort = sort.asc()
    else:
        sort = sort.desc()

    return sort

