from flask import request
from patreon.app.web.view import base
from patreon.services import captcha


class CaptchaControls(base.View):
    def data(self):
        return {
            'global_services': captcha.global_services,
            'per_id_services': captcha.per_id_services,
            'ids_by_service': captcha.get_service_ids(),
            'captcha': captcha,
            'err': request.args.get('err')
        }

    def template(self):
        return "CaptchaControls/captcha_controls.html"
