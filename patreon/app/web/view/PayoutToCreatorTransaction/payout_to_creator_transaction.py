from patreon.app.web.view import base


class PayoutToCreator(base.View):
    def init(self, transaction):
        self.transaction = transaction

    def data(self):

        return {
            'transaction': self.transaction,
            'amount': abs(self.transaction["Amount"])
        }

    def template(self):
        return "PayoutToCreatorTransaction/payout_to_creator_transaction.html"
