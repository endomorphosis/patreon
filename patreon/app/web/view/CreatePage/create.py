from flask import request, session
from patreon.app.web.view import base
from patreon.model.type import PostType
from patreon.model.manager import feature_flag
from patreon.util.unsorted import jsencode


class CreatePage(base.View):
    def init(self, current_user, campaign):
        self.current_user = current_user
        self.campaign = campaign

    def data(self):
        campaign_id = None
        if self.campaign is not None:
            campaign_id = self.campaign.campaign_id

        show_campaign_start = request.args.get('rgsn') == '1'

        if show_campaign_start:
            show_campaign_start = not feature_flag.get_abtest_assignment(
                'no_creator_popup',
                group_id=request.ab_test_group_id,
                user_id=session.user_id
            )

        return {
            'current_user': self.current_user,
            'campaign': self.campaign,
            'js_campaign_id': jsencode(campaign_id),
            'js_campaign_launched': jsencode(self.campaign is not None and self.campaign.is_launched),
            'rgsn': show_campaign_start,
            'share_type_photo': PostType.IMAGE.value
        }

    def template(self):
        template_version = 'old'
        if feature_flag.is_enabled('creator_page_v2', group_id=request.ab_test_group_id, user_id=session.user_id):
            template_version = 'creator_page_v2'
        return 'CreatePage/create.{0}.html'.format(template_version)
