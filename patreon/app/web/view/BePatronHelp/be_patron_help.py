from patreon.app.web.view import base

class BePatronHelp(base.View):
    def init(self, campaign):
        self.campaign = campaign

    def data(self):
        return {
            'campaign_name': self.campaign.creator_name(),
            'pay_per_name': self.campaign.pay_per_name
        }

    def template(self):
        return "BePatronHelp/be_patron_help.html"
