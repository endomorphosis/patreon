from flask import session, request

from patreon.app.web.view import base
from patreon.model.manager import user_mgr, user_location_mgr, pledge_mgr, payment_mgr, feature_flag
from patreon.util.unsorted import get_int


class SettingsPage(base.View):
    react_dependencies = ['alertVerifyEmail']

    def data(self):
        current_user = user_mgr.get(session.user_id)
        display_errors = request.args.get('err') or request.args.get('passError')
        campaign = None
        if current_user.campaigns:
            campaign = current_user.campaigns[0]

        return {
            'current_user': current_user,
            'campaign': campaign,
            'total_user_credit': payment_mgr.get_credit_total_legacy(session.user_id),
            'locations': user_location_mgr.find_by_user_id_legacy(current_user.user_id),
            'supporting': [element.CID for element in pledge_mgr.find_by_patron_id(current_user.user_id)],
            'get_err': get_int(request.args.get('err')) or request.args.get('err'),
            'get_email': request.args.get('email') or '',
            'get_pass_err': get_int(request.args.get('passError')),
            'get_saved': request.args.get('saved'),
            'get_rgsn': request.args.get('rgsn'),
            'is_unverified': current_user.Status == 0,
            'alert_verify_email': feature_flag.is_enabled(
                'alert_verify_email',
                group_id=request.ab_test_group_id,
                user_id=session.user_id),
            'display_errors': display_errors,
            'display_success': not display_errors or request.args.get('emc')
        }

    def template(self):
        return "SettingsPage/settings_page.html"
