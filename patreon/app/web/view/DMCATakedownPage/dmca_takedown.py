from flask import request
from patreon.app.web.view import base
from patreon.util.unsorted import get_int
from patreon.model.manager import dmca_mgr


class DMCATakedownPage(base.View):
    def data(self):
        user_id = get_int(request.args.get('u'))

        return {
            'notice_url': dmca_mgr.get_takedown_url(user_id) if user_id else None
        }

    def template(self):
        return "DMCATakedownPage/dmca_takedown.html"
