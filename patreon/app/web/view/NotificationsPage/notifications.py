from patreon.app.web.view import base


class NotificationsPage(base.View):
    def template(self):
        return "NotificationsPage/notifications.html"
