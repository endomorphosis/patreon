from patreon.app.web.view import base

class GenericInternalPage(base.View):
    def init(self, function_name, internal_title='', internal_description='', form_labels=[''], form_fields=[{}], form_hidden_fields=[{}]):
        self.function_name = function_name
        self.form_fields = form_fields
        self.hidden_form_fields = form_hidden_fields
        self.title = internal_title
        self.description = internal_description
        self.form_labels = form_labels

    def data(self):
        return {
            'title': self.title,
            'description': self.description,
            'function_name': self.function_name,
            'fields': zip(self.form_labels, self.form_fields, self.hidden_form_fields)
        }

    def template(self):
        return "GenericInternalPage/generic_internal_page.html"
