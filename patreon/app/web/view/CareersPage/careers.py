from patreon.app.web.view import base
from patreon.services.logging import rollbar
import requests

department_api = "https://api.greenhouse.io/v1/boards/patreon/embed/departments"

class CareersPage(base.View):

    def data(self):
        jobs = []
        try:
            json = requests.get(department_api).json()

            for department in json['departments']:
                for job in department['jobs']:
                    jobs.append(job)

            return { "jobs": jobs }
        except:
            rollbar.log_current_exception('Exception in careers page')
            return {'jobs': []}

    def template(self):
        return "CareersPage/careers.html"
