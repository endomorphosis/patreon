from flask import request, session

from patreon.app.web.view import base
from patreon.constants.money import MAX_DB_CENTS
from patreon.model.manager import reward_mgr, user_mgr, complete_pledge_mgr
from patreon.routing import redirect
from patreon.util.format import format_cents_no_comma
from patreon.util.unsorted import get_float


class BePatronPage(base.View):
    def init(self):
        self.creator_id = request.args.get('u')
        self.pledge_dollars = get_float(request.args.get('patAmt'))
        self.edit = request.args.get('edit')
        self.error = None  # TODO(implement)

    def data(self):
        # 1. Redirect if parameters are bad.
        creator = user_mgr.get(self.creator_id)
        if not creator or len(creator.campaigns) == 0:
            redirect('/')

        campaign = creator.campaigns[0]
        if not campaign.is_launched:
            redirect('/')

        # 2. Get pledge as cents.
        if self.pledge_dollars is None:
            pledge_cents = 100
        else:
            # Can't convert infinity to integer.
            pledge_cents = int(min(self.pledge_dollars * 100, MAX_DB_CENTS))

        # 3. Get pledges and rewards.
        all_rewards = reward_mgr.find_by_creator_id(self.creator_id)
        existing_pledge = complete_pledge_mgr.get_pledge_by_patron_and_campaign_or_creator(
            patron_id=session.user_id, creator_id=self.creator_id
        ).legacy

        # 4. Set reward_id.
        reward_id = None
        if existing_pledge:
            reward_id = existing_pledge.reward_id or 0

        if not reward_id and pledge_cents:
            for reward in all_rewards:
                if reward.amount <= pledge_cents \
                        and (not reward.user_limit
                             or reward.count < reward.user_limit):
                    reward_id = reward.reward_id

        if not reward_id and request.args.get('rid'):
            reward_id = request.args.get('rid')

        # 5. Errors TODO(implement)
        if self.error:
            error_html = "Sorry, this reward has <strong>sold out!</strong>" \
                         " Please select another option."
            display_errors = ''
        else:
            error_html = ''
            display_errors = 'display:none;'

        return {
            'creator': creator,
            'campaign': campaign,
            'pledge_amount_no_comma': format_cents_no_comma(pledge_cents),
            'edit': self.edit,
            'all_rewards': all_rewards,
            'existing_pledge': existing_pledge,
            'reward_id': reward_id,
            'error_html': error_html,
            'display_errors': display_errors
        }

    def template(self):
        return "BePatronPage/be_patron.html"
