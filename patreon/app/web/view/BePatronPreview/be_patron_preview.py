from html import unescape
from patreon.app.web.view import base

class BePatronPreview(base.View):
    def init(self, campaign=None, title=None):
        self.campaign = campaign
        self.title = title

    def data(self):
        return {
            'title': self.title or "Preview",
            'campaign_name': (self.campaign and self.campaign.creator_name()) or "Jack Conte",
            'creation_name': (self.campaign and self.campaign.creation_name) or "Music Videos",
            'one_liner': unescape((self.campaign and self.campaign.one_liner) or "Rockin' to my own daily beat"),
            'image_url': (self.campaign and self.campaign.image_url) or "https://i.imgur.com/mL6VXNE.png",
            'pay_per_name': (self.campaign and self.campaign.pay_per_name) or "thing"
        }

    def template(self):
        return "BePatronPreview/be_patron_preview.html"
