from patreon.app.web.view import base
from patreon.model.manager import pledge_mgr,follow_mgr
from patreon.model.table import User


class SupportedUsers(base.View):
    def init(self, page_user, user_patron_following):
        self.page_user = page_user
        self.user_patron_following = user_patron_following

    def cache_key(self):
        return 'SupportedUsers:0:{}'.format(self.page_user.user_id)

    def data(self):
        pledging = [
            pledge.creator_id
            for pledge in pledge_mgr.find_by_patron_id(self.page_user.user_id)
        ]
        following = [
            follower.followed_id
            for follower in follow_mgr.find_by_follower_id(self.page_user.user_id)
        ]

        if self.user_patron_following:
            page_user_supporting = following + list(set(pledging) - set(following))
        else:
            page_user_supporting = pledging

        for user_id in page_user_supporting:
            User.get.prime(user_id)
        return {
            'page_user_supporting': page_user_supporting,
            'user_patron_following': self.user_patron_following,
            'page_user': self.page_user
        }

    def template(self):
        return "SupportedUsers/supported_users.html"
