from urllib.parse import quote_plus
from flask import request
from patreon.app.web.view import base


class LoginForm(base.View):
    def data(self):
        return {
            'email': request.args.get('ev') or '',
            'redirect_url': quote_plus(request.args.get('ru', ''))
        }

    def template(self):
        return 'LoginForm/login_form.html'
