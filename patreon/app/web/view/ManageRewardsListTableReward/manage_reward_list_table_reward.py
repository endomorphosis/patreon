from patreon.app.web.view import base


class ManageRewardsListTableReward(base.View):
    def init(self, reward):
        self.reward = reward

    def data(self):
        return {
            'reward': self.reward
        }

    def template(self):
        return "ManageRewardsListTableReward/manage_reward_list_table_reward.html"
