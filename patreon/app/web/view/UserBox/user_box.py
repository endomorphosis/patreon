from patreon.app.web.view import base


class UserBox(base.View):
    def init(self, user, include_name):
        self.user = user
        self.include_name = include_name

    def data(self):
        return {
            'user': self.user,
            'include_name': self.include_name
        }

    def template(self):
        return "UserBox/user_box.html"
