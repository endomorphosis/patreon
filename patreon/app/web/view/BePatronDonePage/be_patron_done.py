from flask import request, session
from patreon.app.web.view import base
from patreon.model.manager import user_mgr, complete_pledge_mgr
from patreon.util import social


class BePatronDonePage(base.View):
    def init(self, creator_id):
        self.creator_id = creator_id

    def data(self):
        creator = user_mgr.get(self.creator_id)
        user_url = creator.canonical_url('rf=' + str(session.user_id))

        text = session.meta.get('title')
        # Overwrite the creator_extra stuff depending on if the creator wants to
        # preview their thanks page.
        embed_override = request.form.get('shareEmbed')
        thanks_override = request.form.get('thanksMsg')

        is_supporting = complete_pledge_mgr.get_pledge_by_patron_and_campaign_or_creator(
            creator_id=self.creator_id,
            patron_id=session.user_id
        ).legacy
        suggested_creators = user_mgr.find_suggest_creators_by_creator_id(
            self.creator_id
        )

        return {
            'creator': creator,
            'campaign': creator.campaigns[0],
            'user_url': user_url,
            'share_twitter_link_html': social.share_twitter_link(creator),
            'share_facebook_link_html': social.share_facebook_link(creator, text),
            'is_supporting': is_supporting,
            'suggested_creators': suggested_creators,
            'embed_override': embed_override or None,
            'thanks_override': thanks_override or None,
        }

    def template(self):
        return "BePatronDonePage/be_patron_done.html"
