import patreon

from patreon.app.web.view import base


class DarkmodeIndexContent(base.View):
    def data(self):
        responses = patreon.model.internal.darkmode.get_recent_responses() or []
        return locals()

    def template(self):
        return "DarkmodeIndex/darkmode_index.html"