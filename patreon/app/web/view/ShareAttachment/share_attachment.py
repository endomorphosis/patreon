from patreon.app.web.view import base


class ShareAttachment(base.View):
    def init(self, attachment):
        self.attachment = attachment

    def data(self):
        name = self.attachment.name or ''
        name_short = name if len(name) < 8 else "{}...".format(name[:5])

        return {
            'name': name,
            'name_short': name_short,
            'css_class': css_class(self.attachment),
            'link': self.attachment.download_url
        }

    def template(self):
        return "ShareAttachment/share_attachment.html"


def css_class(attachment):
    return {
        '.txt':     'doc',
        '.rtf':     'doc',
        '.doc':     'doc',
        '.docx':    'doc',

        '.jpg':     'jpg',
        '.jpeg':    'jpg',
        '.png':     'jpg',
        '.gif':     'jpg',
        '.psd':     'jpg',
        '.ai':      'jpg',
        '.tiff':    'jpg',

        '.m4a':     'mp3',
        '.wma':     'mp3',
        '.aiff':    'mp3',
        '.wav':     'mp3',
        '.mp3':     'mp3',

        '.mov':     'mov',
        '.apk':     'apk',
        '.pdf':     'pdf',
        '.ics':     'ics',
        '.zip':     'zip',
        '.rar':     'rar',
        '.xls':     'xls',
        '.ppt':     'ppt',
    }.get(attachment.extension, 'html')
