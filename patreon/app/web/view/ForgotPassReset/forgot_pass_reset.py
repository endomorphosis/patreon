from flask import request
import patreon
from patreon.app.web.view import base


class ForgotPassReset(base.View):
    def data(self):
        return {
            'get_sec': request.args.get('sec') or '',
            'get_u': request.args.get('u') or ''
        }

    def template(self):
        return "ForgotPassReset/forgot_pass_reset.html"