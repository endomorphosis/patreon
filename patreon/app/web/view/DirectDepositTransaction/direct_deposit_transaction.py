from patreon.app.web.view import base


class DirectDeposit(base.View):
    def init(self, transaction):
        self.transaction = transaction

    def data(self):

        return {
            "transaction": self.transaction,
            'amount': abs(self.transaction["Amount"])
        }

    def template(self):
        return "DirectDepositTransaction/direct_deposit_transaction.html"
