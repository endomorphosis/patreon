from patreon.app.web.view import base


class CampaignMilestone(base.View):
    def init(self, goal, pay_per_name, is_met=True):
        self.goal = goal
        self.pay_per_name = pay_per_name
        self.is_met = is_met

    def data(self):
        return {
            'goal': self.goal,
            'is_met': self.is_met,
            'pay_per_name': self.pay_per_name,
            'met': "met" if self.is_met else "unmet",
        }

    def template(self):
        return "CampaignMilestone/campaign_milestone.html"
