from flask import request, session
from patreon.app.web.view import base
from patreon.model.manager import reward_mgr, feature_flag
from patreon.util.unsorted import jsencode


class Create4(base.View):
    def init(self, current_user, campaign):
        self.current_user = current_user
        self.campaign = campaign

    def data(self):
        rewards = reward_mgr.find_by_creator_id(self.current_user.user_id)
        campaign_id = None
        if self.campaign is not None:
            campaign_id = self.campaign.campaign_id

        return {
            'current_user': self.current_user,
            'campaign': self.campaign,
            'js_campaign_id': jsencode(campaign_id),
            'js_campaign_launched': jsencode(self.campaign is not None and self.campaign.is_launched),
            'rewards': rewards,
            'thanks_msg': self.campaign.thanks_msg,
            'thanks_video_url': self.campaign.thanks_video_url,
            'thanks_embed': self.campaign.thanks_embed
        }

    def template(self):
        template_version = 'old'
        if feature_flag.is_enabled('creator_page_v2', group_id=request.ab_test_group_id, user_id=session.user_id):
            template_version = 'creator_page_v2'
        return 'Create4Page/create4.{0}.html'.format(template_version)
