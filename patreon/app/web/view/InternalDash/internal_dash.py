from patreon.app.web.view import base

class InternalDash(base.View):

    def template(self):
        return 'InternalDash/internal_dash.html'
