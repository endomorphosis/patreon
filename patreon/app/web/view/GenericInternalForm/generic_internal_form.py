from patreon.app.web.view import base

class GenericInternalForm(base.View):
    def init(self, function_name, form_label, form_fields, form_hidden_fields):
        self.function_name = function_name
        self.form_fields = form_fields
        self.hidden_form_fields = form_hidden_fields
        self.form_label = form_label

    def data(self):
        return {
            'form_label': self.form_label,
            'form_fields': self.form_fields,
            'hidden_form_fields': self.hidden_form_fields,
            'function_name': self.function_name
        }

    def template(self):
        return "GenericInternalForm/generic_internal_form.html"
