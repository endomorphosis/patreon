from patreon.app.web.view import base
from patreon.model.manager import user_mgr


class CDSCreatorsPage(base.View):
    def data(self):
        creator_ids = [
            88058, 87768, 139468,
            155175, 104049, 43915,
            55092, 54678, 231823,
            156191
        ]

        # Hack for when we're in an environment with an empty db.
        creators = user_mgr.get_users(creator_ids)
        creator_ids = [ creator.user_id for creator in creators ]

        return { 'creator_ids': creator_ids }

    def template(self):
        return "CDSCreatorsPage/cds_creators.html"
