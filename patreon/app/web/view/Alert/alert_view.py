from flask import session
from patreon.app.web.view import base
from patreon.constants.money import ANNUAL_TAXABLE_EARNINGS
from patreon.model.manager import pledge_mgr, rewards_give_mgr, tax_form_mgr, user_mgr
from patreon.util.format import comma_list

MAX_AVATARS = 8
MAX_NAMES = 3

class Alert(base.View):
    def data(self):
        failed_creators = []
        if session.user_id:
            failed_pledges = pledge_mgr.find_invalid_by_user_id(session.user_id)
            creator_ids = [pledge.creator_id for pledge in failed_pledges]
            failed_creators = user_mgr.get_users(creator_ids)

        current_year_earnings = 0
        tax_form = False
        if session.user_id:
            current_year_earnings = rewards_give_mgr.find_pledge_sum_by_creator_for_year(session.user_id)
            if current_year_earnings >= ANNUAL_TAXABLE_EARNINGS:
                tax_form = tax_form_mgr.get(session.user_id) is not None

        creator_names = [_.full_name for _ in failed_creators]
        if len(creator_names) > MAX_NAMES:
            creator_names = creator_names[:(MAX_NAMES-1)] \
                            + ["{} others".format(len(creator_names) - (MAX_NAMES-1))]

        pledge_list = "Your pledge{s} to {names}".format(
            s=('s' if len(creator_names) > 1 else ''),
            names=comma_list(creator_names)
        )

        return {
            'failed_creators': failed_creators[:MAX_AVATARS],
            'show_failed_creators': len(creator_names) > 0,
            'show_tax_forms': not tax_form and (current_year_earnings > 0),
            'pledge_list': pledge_list
        }

    def template(self):
        return "Alert/alert_view.html"
