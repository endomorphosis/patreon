import pprint
import socket

import os
from patreon.app.web.view import base
from flask import request, session




# ===============
# Darkmode Index
# ===============
from patreon.model.manager import user_mgr


class FlubstepDebug(base.View):
    def data(self):
        server_hostname = socket.gethostname()
        server_process_id = os.getpid()

        session_object_id = id(session._get_current_object())
        session_user_id = session.user_id
        if session_user_id:
            session_fullname = user_mgr.get(user_id=session_user_id).full_name
        else:
            session_fullname = "Logged-out user"
        session_info = session

        request_object_id = id(request._get_current_object())
        request_headers = dict(request.headers)
        # Remove cookie so it's inaccessible outside of secure context
        request_headers['Cookie'] = '********'

        info = pprint.pformat(sorted(locals().items()), width=80, indent=4)
        return {'info': info}

    def template(self):
        return "FlubstepDebug/flubstep_debug.html"