import patreon
from patreon.app.web.view import base


class ForgotPass(base.View):

    def template(self):
        return "ForgotPass/forgot_pass.html"