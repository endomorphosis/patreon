from flask import request
from patreon.app.web.view import base


class PaymentDeclined(base.View):
    def init(self, creator, rejected_card_id):
        self.creator = creator
        self.rejected_card_id = rejected_card_id

    def data(self):
        return {
            'declined_card_id': self.rejected_card_id,
            'redirect_url': request.args.get('ru') or '/',
            'image_url': self.creator.thumb_url,
            'creator_name': self.creator.full_name
        }

    def template(self):
        return 'PaymentDeclined/payment_declined.html'
