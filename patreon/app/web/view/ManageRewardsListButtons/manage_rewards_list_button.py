from flask import request
from patreon.app.web.view import base


class ManageRewardsListButtons(base.View):
    def data(self):
        return {
            'get_c': request.args.get('c'),
            'get_hid': request.args.get('hid')
        }

    def template(self):
        return "ManageRewardsListButtons/manage_rewards_list_button.html"
