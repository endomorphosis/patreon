from flask import request

from patreon.app.web.view import base
from patreon.model.manager import activity_mgr, pledge_mgr, rewards_give_mgr


class ManageRewards(base.View):
    def init(self, current_user, campaign):
        self.current_user = current_user
        self.campaign = campaign

    def data(self):
        activities = activity_mgr.find_paid_by_campaign_id(self.campaign.campaign_id).all()

        paid_one = None
        paid_two = None
        paid_three = None

        if not self.campaign.is_monthly:
            pledges = pledge_mgr.find_all_by_creator_id(self.current_user.UID)
            current_sums = rewards_give_mgr.find_pledge_sums_for_creator_by_user_id_and_date(self.current_user.UID)
            paid_one = get_paid_post_value(pledges, current_sums)
            paid_two = get_paid_post_value(pledges, current_sums)
            paid_three = get_paid_post_value(pledges, current_sums)

        return {
            'current_user': self.current_user,
            'show_alert': request.args.get('alert') == "1",
            'deleted_creator_id': request.args.get('cid'),
            'is_monthly': self.campaign.is_monthly,
            'paid_one': paid_one,
            'paid_two': paid_two,
            'paid_three': paid_three,
            'paid_posts': activities,
            'total_lifetime': rewards_give_mgr.get_lifetime_by_creator_id(self.current_user.UID)
        }

    def template(self):
        return "ManageRewards/manage_rewards.html"


def get_paid_post_value(pledges, current_sums):
    running_patron_count = 0
    running_sum = 0
    for pledge in pledges:
        user_id = pledge.UID
        limit = pledge.MaxAmount
        amount = pledge.Pledge

        if current_sums.get(user_id) is None:
            current_sums[user_id] = 0
        elif current_sums.get(user_id) == -1:
            continue

        if limit and current_sums[user_id] >= limit:
            current_sums[user_id] = -1
            continue
        elif limit and current_sums[user_id] + amount >= limit:
            amount = limit - current_sums[user_id]
            current_sums[user_id] = -1
        elif limit:
            current_sums[user_id] += amount

        running_patron_count += 1
        running_sum += amount
    return {'amount': running_sum / 100,
            'patron_count': running_patron_count}
