from patreon.app.web.view import base
from patreon.constants import image_sizes
from patreon.model.type import PostType


class PostInline(base.View):
    def init(self, is_page_owner, is_patron, rewards):
        self.is_page_owner = is_page_owner
        self.is_patron = is_patron
        self.rewards = rewards
        return

    def data(self):
        return {
            'share_type_photo': PostType.IMAGE.value,
            'share_type_link': PostType.LINK.value,
            'share_type_note': PostType.TEXT_ONLY.value,
            'share_image_large': image_sizes.POST_LARGE.width,
            'is_page_owner': self.is_page_owner,
            'is_patron': self.is_patron,
            'rewards': self.rewards
        }

    def template(self):
        return "IncPostInline/inc_post_inline.html"
