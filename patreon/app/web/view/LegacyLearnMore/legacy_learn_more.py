from patreon.app.web.view import base


class LegacyLearnMore(base.View):
    def template(self):
        return "LegacyLearnMore/legacy_learn_more.html"
