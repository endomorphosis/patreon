from patreon.app.web.view import base
from patreon.model.manager import campaign_mgr, user_mgr


class DisplayBoxCreatorsBrowse(base.View):
    def init(self, creator_id):
        self.creator_id = creator_id

    def data(self):
        creator = user_mgr.get(self.creator_id)
        campaign = campaign_mgr.try_get_campaign_by_user_id_hack(self.creator_id)

        return {
            'creator_full_name': creator.full_name,
            'creator_url': creator.patreon_url(),
            'creator_normal_dimension': 320,
            'small_image_url': campaign.image_small_url,
            'creating': campaign.pay_per_name,
            'creator_one_line': campaign.one_liner or ''
        }

    def template(self):
        return "DisplayBoxCreatorsBrowse/display_box_creators_browse.html"
