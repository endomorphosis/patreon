from patreon.app.web.view import base


class CondensedReward(base.View):
    def init(self, reward, campaign):
        self.reward = reward
        self.campaign = campaign

    def data(self):
        limit = self.reward.UserLimit or 0

        return {
            'reward': self.reward,
            'limit': limit,
            'remaining': limit - self.reward.count,
            'campaign': self.campaign
        }

    def template(self):
        return "CondensedReward/condensed_reward.html"
