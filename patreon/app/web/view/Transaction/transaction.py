from patreon.app.web.view import base
from patreon.util.format import cents_as_dollar_string


class Transaction(base.View):
    def init(self, transaction):
        self.transaction = transaction

    def data(self):
        return {
            'transaction': self.transaction,
            'type': self.transaction["Type"],
            'amount': cents_as_dollar_string(self.transaction["Amount"])
        }

    def template(self):
        return "Transaction/transaction.html"
