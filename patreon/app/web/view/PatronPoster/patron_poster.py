from patreon.app.web.view import base
from patreon.model.type import PostType


class PatronPoster(base.View):
    def init(self, creator_id, current_user_id=None):
        self.creator_id = creator_id
        self.current_user_id = current_user_id

    def data(self):
        return {
            'creator_id': self.creator_id,
            'is_page_owner': self.creator_id == self.current_user_id,
            'share_type_note': PostType.TEXT_ONLY.value
        }

    def template(self):
        return 'PatronPoster/patron_poster.html'
