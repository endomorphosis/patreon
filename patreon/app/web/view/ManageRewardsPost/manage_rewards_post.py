from patreon.model.manager import rewards_give_mgr
from patreon.model.table import RewardsGive
from patreon.app.web.view import base


class ManageRewardsPost(base.View):
    def init(self, activity):
        self.activity = activity

    def data(self):
        pledge_sum, patron_count = rewards_give_mgr.find_sum_patron_count_all_by_activity_id(self.activity.activity_id)
        successful_sum, \
        successful_patrons = rewards_give_mgr.find_sum_patron_count_success_by_activity_id(self.activity.activity_id)
        return {
            'pledge_sum': pledge_sum,
            'patron_count': patron_count,
            'successful_sum': successful_sum,
            'successful_patrons': successful_patrons,
            'activity': self.activity
        }

    def template(self):
        return "ManageRewardsPost/manage_rewards_post.html"
