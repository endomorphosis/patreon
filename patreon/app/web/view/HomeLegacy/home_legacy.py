from flask import request, session

from patreon.app.web.view import base
from patreon.model.manager import user_mgr, feature_flag
from patreon.routing import redirect

home_posts_per_page_hack = 10


class HomeLegacy(base.View):
    react_dependencies = ['alertCreatorSignup']

    def init(self):
        pass

    def data(self):
        # 1. Current User for ShareBox.
        current_user_id = session.user_id
        if not current_user_id:
            redirect('/login')
        current_user = user_mgr.get_user(current_user_id)

        # 2. Post Params.
        is_by_creator_filter = request.args.get('ty') != 'a'
        get_rgsn = request.args.get('rgsn') == '1'

        home_next_url = "/homeNext?p=2"
        if not is_by_creator_filter:
            home_next_url += "&ty=a"

        # 3. Top bar.
        no_posts = False
        if is_by_creator_filter:
            creator_post_only_active = 'active'
            all_post_active = ''
        else:
            creator_post_only_active = ''
            all_post_active = 'active'

        return {
            'meta': session.meta,
            'current_user_id': current_user_id,
            'current_user': current_user,
            'is_creation_filter': is_by_creator_filter,
            'get_rgsn': get_rgsn,
            'no_posts': no_posts,
            'creator_post_only_active': creator_post_only_active,
            'all_post_active': all_post_active,
            'home_next_url': home_next_url,
            'alert_creator_signup': feature_flag.is_enabled(
                'alert_creator_signup',
                group_id=request.ab_test_group_id,
                user_id=session.user_id),
        }

    def template(self):
        return "HomeLegacy/home.html"
