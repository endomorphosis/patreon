from urllib.parse import urlparse
import patreon

from patreon.app.web.view import base

from flask import request


class DarkmodeResponseContent(base.View):
    def init(self):
        self.response_id = int(request.args.get('response_id'))

    def data(self):
        response = patreon.model.internal.darkmode.get_response(self.response_id)
        url_elements = urlparse(response['url'])
        response['url'] = "https://patreon.com" + url_elements[2] + '?' + url_elements[3]
        if not response:
            patreon.routing.redirect('/internal/darkmode')
        previous_response, next_response = patreon.model.internal.darkmode.get_nearby_responses(self.response_id)
        return locals()

    def template(self):
        return "DarkmodeResponse/darkmode_response.html"