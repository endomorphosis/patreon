from flask import session, request

from patreon import globalvars
from patreon.app.web.view import base
from patreon.constants import user_hacks
from patreon.model.manager import complete_pledge_mgr, feature_flag, pledge_mgr,\
    reward_mgr, settings_mgr, user_mgr
from patreon.model.type import SortType
from patreon.services.caching import memcached
from patreon.util import format


@memcached(time_to_cache=180)
def get_patron_count(page_user_id):
    return pledge_mgr.get_patron_count_by_creator_id(page_user_id)


class UserCampaign(base.View):
    page_load_event_name = 'Campaign Page'

    def init(self, page_user_id):
        self.page_user_id = int(page_user_id)

    def data(self):
        page_user = user_mgr.get(user_id=self.page_user_id)
        page_campaign = page_user.campaigns[0]
        current_user = user_mgr.get(user_id=session.user_id)

        tab_type = request.args.get('ty')
        is_default_tab = tab_type not in ['a', 'p', 'c']
        sort = SortType.LATEST
        if tab_type == 'c':
            sort = SortType.CONTENT_LATEST

        pledge_total = pledge_mgr.get_pledge_total_by_creator_id(self.page_user_id)

        if user_hacks.is_plural_hack(page_user.user_id):
            headline_verb = 'are'
        else:
            headline_verb = 'is'

        if session.user_id == self.page_user_id:
            user_extra = settings_mgr.get_user_extra(user_id=page_user.user_id)
            if user_extra is None:
                flag_youtube = 0
            else:
                flag_youtube = user_extra.FlagYoutube
        else:
            flag_youtube = 0

        default_pledge = reward_mgr.get_default_pledge_by_creator_id(self.page_user_id)
        if default_pledge is None or float(default_pledge) < 1.0:
            default_pledge_value = 1.0
        else:
            default_pledge_value = float(default_pledge)

        rewards = []
        if tab_type == 'h':
            rewards = sorted(page_user.rewards or [], key=lambda reward: reward.Amount)

        return {
            'meta': session.meta,
            'page_user': page_user,
            'current_user': current_user,
            'campaign': page_campaign,
            'is_page_owner': session.user_id == self.page_user_id,
            'is_admin': session.is_admin,
            'headline_verb': headline_verb,
            'is_patron': complete_pledge_mgr.get_pledge_by_patron_and_campaign_or_creator(
                patron_id=session.user_id, creator_id=self.page_user_id
            ).legacy is not None,
            'pledge_total': pledge_total or 0,
            'default_pledge': format.format_money(default_pledge_value, 0),
            'page_user_has_default_image': globalvars.get('IMG_PROFILE_DEFAULT_LARGE') == page_user.ImageUrl,
            'is_launched': page_campaign.is_launched,
            'flag_youtube': flag_youtube == 1 or flag_youtube == 2,
            'creation_id': request.args.get('hid'),
            'get_msg': request.args.get('msg'),
            'tab_type': tab_type,
            'is_default_tab': is_default_tab,
            'rewards': rewards,
            'sort': sort.value,
            'patron_count': get_patron_count(self.page_user_id),
            'original_user_id': session.original_user_id,
            'hide_default_pledge_input': feature_flag.get_abtest_assignment(
                'hide_default_pledge_input', request.ab_test_group_id, session.user_id
            )
        }

    def template(self):
        return "UserCampaign/user_campaign.html"

    def render(self, cached=None):
        result = super().render(cached)
        return result
