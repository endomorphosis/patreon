from patreon.app.web.view import base
import requests

class CreatorQueue(base.View):
    def template(self):
        return "CreatorQueue/creator_queue.html"
