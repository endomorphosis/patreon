from flask import session
from patreon.app.web.view import base
from patreon.model.manager import activity_mgr, follow_mgr
from patreon.util import social


class CampaignNavigation(base.View):
    def init(self, page_user, campaign, tab_type, is_default_tab, is_patron, patron_count):
        self.page_user = page_user
        self.campaign = campaign
        self.tab_type = tab_type
        self.is_default_tab = is_default_tab
        self.is_patron = is_patron
        self.patron_count = patron_count

    def data(self):
        creator_id = self.page_user.user_id
        campaign_id = self.campaign.campaign_id
        current_user_is_follower = follow_mgr.get(
            followed_id=creator_id,
            follower_id=session.user_id
        ) is not None

        return {
            'page_user': self.page_user,
            'tab_type': self.tab_type,
            'is_default_tab': self.is_default_tab,
            'is_patron': self.is_patron,
            'is_page_owner': session.user_id == creator_id,
            'patron_count': self.patron_count,
            'current_user_is_follower': current_user_is_follower,
            'total_creation_count': activity_mgr.get_creator_post_count_by_campaign_id(campaign_id),
            'total_activity_count': activity_mgr.get_post_count_by_campaign_id(campaign_id),
            'twitter_text': social.twitter_text(self.page_user, self.campaign),
            'meta': session.meta
        }

    def template(self):
        return "CampaignNavigation/campaign_navigation.html"
