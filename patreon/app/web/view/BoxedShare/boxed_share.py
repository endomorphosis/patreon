from patreon.app.web.view import base
from patreon.model.type import PostType, post_type_enum


class BoxedShare(base.View):
    def init(self, share, size, link='/creation'):
        self.activity = share
        self.size = size
        self.link = link

    def data(self):
        return {
            'activity': self.activity,
            'size': self.size,
            'link': self.link
        }

    def template(self):
        post_type = PostType.get(self.activity.activity_type)

        if post_type in post_type_enum.IMAGE_TYPES:
            return "BoxedShare/photo_share.html"
        elif post_type in post_type_enum.EMBED_TYPES:
            return "BoxedShare/link_share.html"
        elif post_type is PostType.TEXT_ONLY \
                or post_type is PostType.AUDIO_FILE:
            return "BoxedShare/note_share.html"
        else:
            raise Exception('Invalid post_type: {}'.format(self.activity.activity_type))
