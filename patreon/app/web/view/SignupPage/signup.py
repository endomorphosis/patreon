import patreon

from patreon.app.web.view import base

from flask import session, request
from patreon.services import captcha


class Signup(base.View):
    def init(self):
        if session.user_id:
            redirect_url = patreon.routing.sanitize_redirect_url(request.args.get('ru'))
            patreon.routing.redirect(redirect_url)

    def data(self):
        show_captcha = captcha.is_enabled('signup') and not request.args.get('magic_captcha_hide')

        return {
            'patreon_use_recaptcha': 'patreon-use-recaptcha' if show_captcha else ''
        }

    def template(self):
        return "SignupPage/signup.html"
