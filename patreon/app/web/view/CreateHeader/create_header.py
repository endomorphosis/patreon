from patreon.app.web.view import base

steps = [
    {
        'title': "Who you are",
        'subtitle': "Start here",
        'link': '/create'
    },
    {
        'title': "Your Patreon Page",
        'subtitle': "Describe what you're doing",
        'link': '/create2'
    },
    {
        'title': "Your Goals",
        'subtitle': "Setup funding milestones",
        'link': '/create3'
    },
    {
        'title': "Reward",
        'subtitle': "Reward your patrons",
        'link': '/create4'
    }
]

class CreateHeader(base.View):
    def init(self, step):
        self.step = step

    def data(self):
        for step_ in steps:
            step_['current'] = step_['link'] == self.step

        return { 'steps': steps }

    def template(self):
        return "CreateHeader/create_header.html"
