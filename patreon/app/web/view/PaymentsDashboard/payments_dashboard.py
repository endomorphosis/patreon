from patreon.app.web.view import base


class PaymentsDashboard(base.View):
    def init(self):
        pass

    def data(self):
        return {}

    def template(self):
        return "PaymentsDashboard/payments_dashboard.html"
