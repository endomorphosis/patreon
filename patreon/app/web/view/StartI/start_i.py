from flask import session
from patreon.app.web.view import base
from patreon.model.manager import user_mgr
from patreon.model.table import User

class StartI(base.View):
    def data(self):
        current_user = user_mgr.get(user_id=session.user_id)
        return {'first_name': current_user.FName}

    def template(self):
        return "StartI/start_i.html"
