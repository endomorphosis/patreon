from patreon.app.web.view import base
from patreon.constants import image_sizes


class BoxCreatorsBrowse(base.View):

    def init(self, featured):
        self.featured = featured

    def data(self):
        return {
            'featured': self.featured,
            'featured_url': self.featured.canonical_url(),
            'featured_campaign': self.featured.campaigns[0],
            'creator_image_normal': image_sizes.CREATOR_NORMAL.width
        }

    def template(self):
        return "BoxCreatorsBrowse/box_creators_browse.html"
