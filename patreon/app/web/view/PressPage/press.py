from patreon.app.web.view import base

presses = [
    {
        'org': "NBC News",
        'link': "http://www.nbcnews.com/feature/shortcuts/crowdfund-your-artistic-dreams-n83556",
        'title': "Crowdfund your Artistic Dreams"
    },
    {
        'org': "Techcrunch",
        'link': "http://techcrunch.com/2014/06/23/patreon-raises-15-million-series-a-revamps-site-to-showcase-artist-content/",
        'title': "Patreon Raises $15 Million Series A, Revamps Site To Focus More On Content",
        'excerpt': "Company revenues grew 10x in the last five months, allowing the founders some room to choose whom they would like to take investment from."
    },
    {
        'org': "Inc.",
        'link': "http://www.inc.com/jill-krasny/35-under-35-patreon-crowdfunding-for-long-haul.html",
        'title': "Patreon: Crowdfunding for the Long Haul",
        'excerpt': "...the main goal is scaling and sending as much money to creators as possible."
    },
    {
        'org': "Huffington Post",
        'link': "http://www.huffingtonpost.com/turnstyle/crowdfunder-patreon-raise_b_5522599.html",
        'title': "Crowdfunder Patreon Raises $15 Million Series A, Plots Mobile Expansion",
        'excerpt': "'There are a lot of creators who are making thousands of dollars on the site, and this is their rent money,' said Conte. 'Their grocery money.'"
    },
    {
        'org': "Wired",
        'link': "http://www.wired.com/2014/05/patreon/",
        'title': "The Crowdfunding Upstart That’s Turning Freelancers Into Superstars",
        'excerpt': "It's hard to get anyone I interview to say a truly unkind word about the service. 'I think they’re so great that I’ve convinced at least six friends and (passively convinced) two bitter enemies to join. And I don’t even get a kickback.'"
    },
    {
        'org': "LA Times",
        'link': "http://www.latimes.com/business/technology/la-fi-tn-patreon-crowdfunding-20140627-story.html",
        'title': "Crowdfunding site Patreon aims to help artists actually make a living",
        'excerpt': "'The playing field has been leveled,' Conte said of how Patreon will help creators. 'Anyone can be an artist now. Anyone can make a living being an artist if you work hard enough. It's just as possible as growing up to be a doctor or a lawyer.'"
    },
    {
        'org': "Re/Code",
        'link': "http://recode.net/2014/06/23/creative-patronage-startup-patreon-raises-15m/",
        'title': "Creative Patronage Startup Patreon Raises $15 Million",
        'excerpt': "Because we're so cognizant of how venture capital can also be a dangerous thing, we do want to make sure our creators know how thoughtful this round was. We spent a long time meeting a lot of folks."
    },
    {
        'org': "Wired",
        'link': "http://www.wired.com/2014/06/patreon-series-a-funding/",
        'title': "What Crowdfunding Site Patreon Will Do With That $15M in Funding",
        'excerpt': "The influx of cash means Patreon can begin expanding their offerings for the artists, musicians, writers, filmmakers and other creatives who use the service to get income from fans."
    },
    {
        'org': "Time",
        'link': "http://time.com/2934093/youtube-crowdfunding/",
        'title': "YouTube Is About to Change Drastically",
        'excerpt': "Patreon, a newer startup that was launched by a YouTube creator seeking more revenue, has generated $2 million for creators since it launched early in 2013."
    },
    {
        'org': "Bloomberg",
        'link': "http://www.bloomberg.com/video/youtube-stars-the-new-content-kings-vOL6HQJvQ52ZNobD6XYqqg.html",
        'title': "YouTube Stars: The New Content Kings",
    },
    {
        'org': "Forbes",
        'link': "http://www.forbes.com/sites/sarahmckinney/2014/04/10/patreon-a-fast-growing-marketplace-for-creators-and-patrons-of-the-arts/",
        'title': "Patreon: A Fast-Growing Marketplace For Creators And Patrons Of The Arts",
        'excerpt': "In less than one year, Patreon has distributed over $1 million to the artists using their platform, with some of the most popular ones making more than $100,000."
    },
    {
        'org': "Techcrunch",
        'link': "http://techcrunch.com/2014/03/09/patreonomics/",
        'title': "What Games Are: Patreonomics And \"Supposed To Be\"",
        'excerpt': "Patreonomics may well be the last great disruption, a combination of all the steps we've learned along the way."
    },
    {
        'org': "Huffington Post",
        'link': "http://www.huffingtonpost.com/turnstyle/crowdfundings-patreon-tak_b_4785138.html?utm_hp_ref=tw",
        'title': "Crowdfunding's Patreon Takes Aim At YouTube's Business Model",
        'excerpt': "The creation page is modeled after the YouTube watch page with one major difference: no ads."
    },
    {
        'org': "Fast Company",
        'link': "http://www.fastcompany.com/3027335/most-innovative-companies-2014/the-worlds-top-10-most-innovative-companies-in-crowdfunding",
        'title': "The World's Top 10 Most Innovative Companies in Crowdfunding",
        'excerpt': "Patreon is instead designed to keep creative people doing what they’re doing."
    },
    {
        'org': "Time",
        'link': "http://business.time.com/2013/12/04/business/slide/top-10-exciting-startups/",
        'title': "Top 10 Exciting Startups of 2013",
        'excerpt': "[Artists] are essentially paid for their work regularly instead of having to pitch a big new idea to potential crowd funders each time."
    },
    {
        'org': "Fortune",
        'link': "http://tech.fortune.cnn.com/2014/04/17/why-investors-are-pouring-millions-into-crowdfunding/",
        'title': "Why investors are pouring millions into crowdfunding",
        'excerpt': "I believe that the democratization of fundraising through crowdfunding is an incredible breakthrough for entrepreneurs and nonprofits."
    },
    {
        'org': "Wired",
        'link': "http://www.wired.com/business/2013/10/big-idea-patreon/",
        'title': "The Next Big Thing You Missed: 'Eternal Kickstarter' Reinvents Indie Art ",
        'excerpt': "The idea came to Jack Conte at the nadir of his financial and artistic desperation."
    },
    {
        'org': "Fast Company",
        'link': "http://www.fastcompany.com/3018613/fast-talk/jack-conte-from-patreon-turns-to-crowdfunding-to-make-money",
        'title': "The Right to be Heard (and Paid) ",
        'excerpt': "Artists deserve more than a check cut in half eight times by publishers."
    },
    {
        'org': "Ars Technica",
        'link': "http://arstechnica.com/business/2013/12/new-site-gives-internet-creatives-the-hope-of-a-regular-paycheck/",
        'title': "New site gives Internet creatives the hope of a regular paycheck",
        'excerpt': "The entire appeal of Patreon to me is the level of stability it seems to promise."
    },
    {
        'org': "Huffington Post",
        'link': "http://www.huffingtonpost.com/david-shing/sic-shingerview-jack-cont_b_4214447.html",
        'title': "Jack Conte, Musician, CrowdFunder",
        'excerpt': "The only reason why a company like Patreon works is because of transparency, being open with people and telling them what's going on, and having them support you."
    },
    {
        'org': "Billboard",
        'link': "http://www.billboard.com/biz/articles/news/digital-and-mobile/5336841/patreon-lands-2-million-in-venture-patronage",
        'title': "Patreon Lands $2 Million in Venture Patronage",
        'excerpt': "We originally had musicians and YouTube creators in mind, but it's become a tool for anybody who's creating stuff."
    },
    {
        'org': "Forbes",
        'link': "http://www.forbes.com/sites/johnhancock/2013/06/18/startup-turns-followers-into-funding-for-more-than-just-one-project/",
        'title': "Startup Turns Followers Into Funding -- For More Than Just One Project",
        'excerpt': "Without aristocratic patronage, Mozart might have had to resort to bar tending on the side; during the Renaissance, Michelangelo might have had to deliver bread to pay the bills during the four years he painted the Sistine Chapel’s ceiling."
    },
    {
        'org': "Techcrunch",
        'link': "http://techcrunch.com/2013/05/08/pomplamoose-jack-conte/",
        'title': "Pomplamoose’s Jack Conte Creates A Subscription-Based Funding Site For Artists",
        'excerpt': "Jack Conte is an Internet musician who became an Internet sensation and now, thanks to Patreon, he’s an Internet entrepreneur."
    },
    {
        'org': "AllThingsD",
        'link': "http://allthingsd.com/20130507/patreon-is-a-recurring-tip-jar-for-fans-who-love-everything-you-make/",
        'title': "Patreon Is a Recurring Tip Jar for Fans Who Love Everything You Make",
        'excerpt': "Kickstarter funding might be a good fit for an independent artist making a big project like a movie. But on YouTube, there are many creators who dribble out new projects as they make them."
    },
    {
        'org': "PBS",
        'link': "http://www.pbs.org/mediashift/2013/09/donation-patron-services-help-fans-support-their-favorite-authors/",
        'title': "Donation, Patron Services Help Fans Support Their Favorite Authors",
        'excerpt': "The site’s mission is to enable artists of all sorts — be they musicians, authors, comic book writers, photographers — to be more adequately compensated for the great works they’re contributing to the world."
    },
    {
        'org': "Hypebot",
        'link': "http://www.hypebot.com/hypebot/2013/08/patreon-receives-21-million-in-funding-jack-conte-currently-supported-at-6153-per-video.html",
        'title': "Patreon Receives $2.1 Million In Funding, Jack Conte Currently Supported At $6,153 Per Video",
        'excerpt': "It's become quite obvious from crowdfunding that people want to support artists they care about."
    },
    {
        'org': "Quartz",
        'link': "http://qz.com/96859/new-crowdfunding-site-gets-youtubers-a-regular-paycheck/",
        'title': "New crowdfunding site gets YouTubers a regular paycheck",
        'excerpt': "Conte hopes to address what he feels is a lag between the way art is made and the way it’s paid for."
    },
    {
        'org': "Forbes",
        'link': "http://www.forbes.com/sites/zackomalleygreenburg/2014/03/26/why-wu-tang-will-release-just-one-copy-of-its-secret-album/",
        'title': "Why Wu-Tang Will Release Just One Copy Of Its Secret Album",
        'excerpt': "The plan almost resembles a Kickstarter campaign in search of a single, super-wealthy backer; there are also parallels with Jack Conte’s Patreon."
    },
    {
        'org': "Lifehacker",
        'link': "http://www.lifehacker.com.au/2014/03/start-your-online-content-creating-with-patreon/",
        'title': "Start Your Online Content Creating With Patreon",
        'excerpt': "The point about being lost in a sea of noise is something Patreon attempts to address, though."
    }

]

class PressPage(base.View):
    def data(self):
        return { 'presses': presses }

    def template(self):
        return "PressPage/press.html"
