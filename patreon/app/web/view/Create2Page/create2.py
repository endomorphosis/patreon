from flask import request, session
from patreon.model.manager import feature_flag
from patreon.app.web.view import base
from patreon.util.unsorted import jsencode


class Create2Page(base.View):
    def init(self, current_user, campaign):
        self.current_user = current_user
        self.campaign = campaign

    def data(self):
        campaign_id = None
        if self.campaign is not None:
            campaign_id = self.campaign.campaign_id

        return {
            'current_user': self.current_user,
            'campaign': self.campaign,
            'js_campaign_id': jsencode(campaign_id),
            'js_campaign_launched': jsencode(self.campaign is not None and self.campaign.is_launched),
            'main_video_embed': self.campaign.main_video_embed or '',
            'main_video_url': self.campaign.main_video_url or '',
            'summary': self.campaign.summary or ''
        }

    def template(self):
        template_version = 'old'
        if feature_flag.is_enabled('creator_page_v2', group_id=request.ab_test_group_id, user_id=session.user_id):
            template_version = 'creator_page_v2'
        return 'Create2Page/create2.{0}.html'.format(template_version)
