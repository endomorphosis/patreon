from patreon.model.manager import pledge_mgr
from patreon.model.table import User
from patreon.app.web.view import base
import random


class DisplayTopPatrons(base.View):
    def init(self, page_user_id):
        self.page_user_id = page_user_id

    def cache_key(self):
        return 'DisplayTopPatrons2:0:{}'.format(self.page_user_id)

    def get_top_patrons(self):
        top_patrons = pledge_mgr.find_top_patrons_by_creator_id(self.page_user_id)[:20]
        for patron in top_patrons:
            User.get.prime(patron)
        random.shuffle(top_patrons)
        return top_patrons

    def data(self):
        return {
            'top_patrons': self.get_top_patrons(),
            'page_user': User.get(self.page_user_id),
            'patron_count': pledge_mgr.get_patron_count_by_creator_id(self.page_user_id)
        }

    def template(self):
        return "DisplayTopPatrons/display_top_patrons.html"
