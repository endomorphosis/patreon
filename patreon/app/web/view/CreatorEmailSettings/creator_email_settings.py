from flask import session
from patreon.app.web.view import base
from patreon.model.manager import settings_mgr, user_mgr


class CreatorEmailSettings(base.View):
    def init(self, creator_id):
        self.creator = user_mgr.get(creator_id)

    def data(self):
        return {
            'creator': self.creator,
            'campaign_settings': settings_mgr.get_campaign_settings(session.user_id, self.creator.user_id)
        }

    def template(self):
        return "CreatorEmailSettings/creator_email_settings.html"