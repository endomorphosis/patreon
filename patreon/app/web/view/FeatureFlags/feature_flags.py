from patreon.app.web.view import base
from patreon.util.unsorted import ternary
from flask import request, session


class FeatureFlags(base.View):
    def init(self, flag_info):
        self.flag_info = flag_info

    def data(self):
        return {
            "flags": self.flag_info,
            "ternary": ternary
        }

    def template(self):
        return "FeatureFlags/feature_flags.html"
