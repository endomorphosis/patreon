from patreon.app.web.view import base


class Message(base.View):
    def init(self, message, senders_by_id):
        self.message = message
        self.senders_by_id = senders_by_id

    def data(self):
        return {
            'message': self.message,
            'sender': self.senders_by_id.get(self.message.sender_id)
        }

    def template(self):
        # TODO(postport): There is an extra closing div in the template.
        return "Message/message.html"
