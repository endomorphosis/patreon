from patreon.app.web.view import base
from patreon.model.manager import goal_mgr


class CampaignMilestones(base.View):
    def init(self, campaign, pledge_total):
        self.campaign = campaign
        self.pledge_total = pledge_total

    def data(self):
        campaign_id = self.campaign.campaign_id
        return {
            'pay_per_name': self.campaign.pay_per_name,
            'met_goals': goal_mgr.find_met_goals_by_campaign_id(campaign_id, self.pledge_total),
            'unmet_goals': goal_mgr.find_unmet_goals_by_campaign_id(campaign_id, self.pledge_total)
        }

    def template(self):
        return "CampaignMilestones/campaign_milestones.html"
