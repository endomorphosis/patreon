from patreon.app.web.view import base


class CreatorPage(base.View):
    react_dependencies = ['creatorPage']

    def template(self):
        return "CreatorPage/creator_page.html"
