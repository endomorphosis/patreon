import time

from patreon.app.web.view import base
import patreon
from flask import request, session
from patreon.model.dbm import card_info_dbm
from patreon.model.manager import reward_mgr, user_mgr, complete_pledge_mgr, rewards_give_mgr
from patreon.util.unsorted import get_int


class BePatronConfirmPage(base.View):
    def data(self):
        current_user_id = session.user_id
        edit = request.args.get('edit')
        creator_id = request.args.get('u')
        reward_id = request.args.get('selectReward', request.args.get('rid'))
        amount = request.args.get('amt', request.args.get('patAmt'))

        cards = card_info_dbm.get_cards(current_user_id)
        all_rewards = reward_mgr.find_by_creator_id(creator_id)
        creator = user_mgr.get(creator_id)
        campaign = creator.campaigns[0]
        user_pledge = complete_pledge_mgr.get_pledge_by_patron_and_campaign_or_creator(
            creator_id=creator_id, patron_id=current_user_id
        ).legacy
        current_user = user_mgr.get(current_user_id)

        if user_pledge:
            amount = amount or (user_pledge.Pledge / 100.0)
        else:
            amount = amount or 1.00

        select_reward_id = request.args.get('selectReward', request.args.get('rid', 0))
        if not select_reward_id and user_pledge:
            select_reward_id = user_pledge.reward_id or 0
            reward_id = user_pledge.reward_id or 0

        select_reward = None
        for one_reward in all_rewards:
            if one_reward.reward_id == get_int(select_reward_id):
                select_reward = one_reward

        if user_pledge:
            s_street = request.args.get('sStreet', user_pledge.Street)
            s_street2 = request.args.get('sStreet2', user_pledge.Street2)
            s_city = request.args.get('sCity', user_pledge.City)
            s_state = request.args.get('sState', user_pledge.State)
            s_zip = request.args.get('sZip', user_pledge.Zip)
            s_country = request.args.get('sCountry', user_pledge.Country)

        all_pending_rewards = rewards_give_mgr.find_pledge_sums_for_creator_by_user_id_and_date(creator_id)
        sum_rewards = all_pending_rewards.get(current_user_id, 0)

        all_countries = patreon.constants.countries.countries
        current_year = time.localtime().tm_year

        def sort_united_states_first(country_codes):
            try:
                found_index = country_codes.index('US')
            except ValueError:
                return country_codes
            return [country_codes[found_index]] + country_codes[:found_index] + country_codes[found_index+1:]

        errors = request.args.get('err')

        # TODO(port): Make explicit.
        return locals()

    def template(self):
        return "BePatronConfirmPage/be_patron_confirm.html"
