from flask import session
from patreon.app.web.view import base
from patreon.model.manager import user_mgr


class IncCurrentUser(base.View):
    def data(self):
        user = user_mgr.get(user_id=session.user_id)
        return {
            'thumb_url': user and user.thumb_url or '',
            'full_name': user and user.full_name or '',
            'user_id': user and user.user_id or '',
        }

    def template(self):
        return "IncCurrentUser/inc_current_user.html"
