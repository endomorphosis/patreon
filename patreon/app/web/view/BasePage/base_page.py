"""
Code specific to routing webpages.
"""
import patreon
from patreon.model.manager.feature_flag import enabled_features_for_user_as_string
from patreon.util.unsorted import url_relative_to_full

from flask import session, request, render_template


def render_page(
        base_template_path='BasePage/base_page.html',
        inner=None,
        hide_meta=False,
        no_share=False,
        show_big=False,
        fb_done=False,
        static_reduced=False,
        os_page=None,
        no_footer=False,
        title='Patreon',
        desc='Patreon is empowering a new generation of creators.\nSupport and engage with artists and creators as they live out their passions!',
        key='artists, crowdfunding, patron, sponsor, music, videos, fundraise',
        url=None,
        image_url=None,
        viewport='1020',
        height=2000,
        no_legacy_javascript=False,
        no_legacy_stylesheets=False,
        patreon_fonts_override=False,
        **kwargs
):
    temp_meta = {
        'hide_meta': hide_meta,
        'no_share': no_share,
        'show_big': show_big,
        'fb_done': fb_done,
        'static_reduced': static_reduced,
        'os_page': os_page,
        'no_footer': no_footer,
        'title': title,
        'desc': desc,
        'key': key,
        'viewport': viewport,
        'height': height,
        'url': request.url,
        'enabled_features': enabled_features_for_user_as_string(session),
        'no_legacy_javascript': no_legacy_javascript,
        'no_legacy_stylesheets': no_legacy_stylesheets,
        'patreon_fonts_override': patreon_fonts_override
    }

    if session.meta is not None:
        session.meta.update(temp_meta)
    else:
        session.meta = temp_meta

    session.meta['react_dependencies'] = getattr(inner, 'react_dependencies', [])
    session.meta['page_load_event_name'] = getattr(inner, 'page_load_event_name', None)

    # TODO(postport): modify meta.html to ensure None is valid
    if url:
        session.meta['url'] = url
    if image_url:
        session.meta['image_url'] = url_relative_to_full(image_url)

    # Don't allow Google to index things directly from development environments.
    if request.headers.get('X-Forwarded-Host') != 'www.patreon.com':
        session.meta['private'] = True

    session.meta['webclient_s3_prefix'] = patreon.services.asset_prefix.webclient_prefix()
    session.meta['react_s3_prefix'] = patreon.services.asset_prefix.react_prefix()

    return render_template(
        base_template_path,
        session=session,
        patreon=patreon,
        inner=inner(**kwargs).render(),
        no_footer=no_footer,
        **patreon.globalvars.to_dict()
    )


def render_react_page(**kwargs):
    kwargs['no_footer'] = True
    return render_page('BasePage/react_base_page.html', **kwargs)
