from collections import defaultdict

from patreon import globalvars
from patreon.app.web.view import base
from patreon.model.manager import attachment_mgr, reward_mgr, user_mgr, \
    complete_pledge_mgr, campaign_mgr
from patreon.model.type import PostType, post_type_enum
from patreon.constants import image_sizes
from patreon.util import format

default_url = "https://s3.amazonaws.com/patreon_public_assets/ooX0e_patreon_logo_shine.png"


class ShareBox(base.View):
    def init(self, post, current_user, commenters, user_likes=None,
             rel='Shares', force_columns=None, no_creator_name=True,
             show_comments=True):
        self.current_user = current_user
        self.post = post
        self.rel = rel
        self.force_columns = force_columns
        self.no_creator_name = no_creator_name
        self.commenters = commenters
        self.user_likes = user_likes or defaultdict(lambda: None)
        self.show_comments = show_comments

    def data(self):
        # 1. Short circuit for bad post.
        if self.post is None:
            return { 'self.post': None }

        # 2. Determine post type and flags.
        post_type = PostType.get(self.post.activity_type)
        is_embed = post_type in post_type_enum.EMBED_TYPES
        is_image = post_type in post_type_enum.IMAGE_TYPES
        is_text_only = post_type is PostType.TEXT_ONLY
        is_audio_only = post_type is PostType.AUDIO_FILE and \
                        not self.post.thumbnail_url

        # 3. Get current user pledge status.
        creator = campaign_mgr.try_get_user_by_campaign_id_hack(self.post.campaign_id)
        if self.current_user is not None:
            current_user_pledge = complete_pledge_mgr.get_pledge_by_patron_and_campaign_or_creator(
                creator_id=creator.user_id,
                patron_id=self.current_user.user_id
            ).legacy
        else:
            current_user_pledge = None

        # 4. Determine permissions.
        can_show = (
            (
                self.post is not None
                and self.post.min_cents_pledged_to_view == 0
            ) or (
                current_user_pledge is not None
                and current_user_pledge.amount >= self.post.min_cents_pledged_to_view
            ) or (
                self.current_user is not None
                and self.post.user_id == self.current_user.user_id
            ) or (
                self.current_user is not None
                and creator.user_id == self.current_user.user_id
            )
        )

        can_edit = (
            not self.post.is_monthly
            and self.current_user
            and self.current_user.user_id == creator.user_id
            and self.current_user.user_id == self.post.user_id
        )

        can_delete = (
            not self.post.is_monthly
            and self.current_user
            and (
                self.current_user.user_id == creator.user_id
                or self.current_user.user_id == self.post.user_id
            )
        )

        # 5. Decide between single or double width.
        if not self.force_columns:
            is_double_width = self.post.is_paid
        elif self.force_columns > 1:
            is_double_width = True
        else:
            is_double_width = False

        # 6. Text-only type hack.
        box_wrapper_add = []
        if is_text_only:
            box_wrapper_add.append('note')
        else:
            box_wrapper_add.append('photo')

        # 7. Audio hack.
        if is_audio_only:
            audio_file_name = self.post.file_name
        else:
            audio_file_name = None

        # 8. Set image.
        image_width = self.post.image_width
        image_height = self.post.image_height

        if self.post.photo_key:
            if is_double_width:
                image_url = self.post.photo_url_size(image_sizes.DEPRECATED_POST_DOUBLE)
            else:
                image_url = self.post.photo_url_size(image_sizes.DEPRECATED_POST_SINGLE)
        elif self.post.thumbnail_key:
            large_size = image_sizes.POST_THUMB_LARGE
            image_url = self.post.thumbnail_url_size(large_size)
            image_width, image_height = large_size.dimensions
        else:
            image_url = self.post.image_large_url

        # 9. Fix image dimensions
        if is_double_width:
            box_wrapper_add.append('double')
            max_width = image_sizes.DEPRECATED_POST_DOUBLE.width
        else:
            max_width = image_sizes.DEPRECATED_POST_SINGLE.width

        if image_width > max_width:
            image_height = image_height * max_width / image_width
            image_width = max_width

        # 10. Get attachments.
        attachments = attachment_mgr.get_attachments_by_activity_id(
            self.post.activity_id
        )

        # 11. Hover bar.
        tab_tri_add = []
        tab_tri_title = []

        if can_show:
            if self.post.min_cents_pledged_to_view == 1:
                tab_tri_title.append('This is a Patrons-only post.')
            elif self.post.min_cents_pledged_to_view > 1:
                dollars = format.cents_as_dollar_string(
                    self.post.min_cents_pledged_to_view
                )
                tab_tri_title.append(
                    'This is a {}+ Patrons-only post.'.format(dollars)
                )

        if attachments:
            tab_tri_add.append('attach')
            tab_tri_title.append('This post has an attachment.')
        elif can_show and self.post.min_cents_pledged_to_view == 0:
            tab_tri_add.append('hiddenDisplay')

        if not self.post.comment_count:
            comment_count = ''
        elif self.post.comment_count > 99:
            comment_count = '99+'
        else:
            comment_count = self.post.comment_count

        return {
            'is_embed': is_embed,
            'is_image': is_image,
            'is_text_only': is_text_only,
            'is_audio_only': is_audio_only,
            'attachments': attachments,
            'post': self.post,
            'current_user': self.current_user,
            'creator': creator,
            'user_liked': self.user_likes[self.post.activity_id] is not None,
            'box_wrapper_add': ' '.join(box_wrapper_add),
            'tabTri_add': ' '.join(tab_tri_add),
            'tabTri_title': ' '.join(tab_tri_title),
            'audio_file_name': audio_file_name,
            'can_show': can_show,
            'can_edit': can_edit,
            'can_delete': can_delete,
            'comment_count': comment_count,
            'image': {
                'height': image_height,
                'half_height': max(0, (image_height - 80) / 2),
                'width': image_width,
                'url': image_url or default_url
            },
            'one_column': is_double_width,
            'share_user': user_mgr.get(user_id=self.post.user_id),
            'max_comments': globalvars.get('COMMENTS_BEFORE_HIDE'),
            'default_thumb': globalvars.get('IMG_PROFILE_DEFAULT'),
            'no_creator_name': self.no_creator_name,
            'user_pledge': current_user_pledge,
            'commenters': self.commenters,
            'show_comments': self.show_comments,
            'default_pledge': reward_mgr.get_default_pledge_by_creator_id(
                creator.user_id
            ),
            'rel': self.rel
        }

    def template(self):
        return "ShareBox/share_box.html"
