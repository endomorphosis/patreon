from patreon.app.web.view import base
from patreon.model.manager.csrf import get_csrf_ticket, get_null_csrf_ticket
from flask import request, session
from patreon.util.unsorted import jsencode, jsdecode


class CSRFFields(base.View):
    def init(self):
        pass

    def data(self):
        if not session.user_id:
            return get_null_csrf_ticket()

        path = request.path
        if '/user' in request.path and request.args.get('v'):
            path = '/{}'.format(request.args.get('v'))
        ticket = get_csrf_ticket(path, session.user_id, session.csrf_token)
        return jsdecode(jsencode(ticket))

    def template(self):
        return "CSRFFields/csrf_fields.html"