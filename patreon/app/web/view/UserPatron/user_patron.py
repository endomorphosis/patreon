from flask import session, request

from patreon.app.web.view import base
from patreon.model.type import SortType
from patreon.model.manager import action_mgr, activity_mgr, user_mgr, \
    pledge_mgr, rewards_give_mgr, comment_mgr


class UserPatron(base.View):
    page_load_event_name = 'Patron Page'

    def init(self, page_user_id, tab_type):
        self.page_user_id = page_user_id
        self.tab_type = tab_type

    def data(self):
        page_user = user_mgr.get(user_id=self.page_user_id)
        current_user = user_mgr.get(user_id=session.user_id)

        is_default_tab = self.tab_type not in ['a', 'l', 'c']
        sort = SortType.LATEST

        if self.tab_type == 'c':
            sort = SortType.CONTENT_LATEST
            creations = rewards_give_mgr.find_supported_posts_by_user_id_paginated(page_user.user_id)

        elif self.tab_type == 'a':
            creations = activity_mgr.find_posts_by_user_id_paginated(page_user.user_id)

        elif self.tab_type == 'l':
            activity_ids = action_mgr.find_likes_by_user_id_paginated(page_user.user_id)
            creations = [
                activity_mgr.get(activity_id)
                for activity_id in activity_ids
            ]
        else:
            creations = []

        creations = [creation for creation in creations if creation]
        post_ids = [post.activity_id for post in creations]

        commenters = comment_mgr.get_commenters_dict_for_posts(post_ids)

        page_user_supporting = pledge_mgr.find_by_patron_id(page_user.user_id)
        page_user_supporting_ids = [element.CID for element in page_user_supporting]
        past_supporting = [
            give.creator_id
            for give in rewards_give_mgr.find_processed_reward_gives_from_active_creators_for_patron(page_user.user_id)
            if give.creator_id not in page_user_supporting_ids
        ]

        return {
            'page_user': page_user,
            'current_user': current_user,
            'meta': session.meta,
            'tab_type': self.tab_type,
            'is_default_tab': is_default_tab,
            'get_msg': request.args.get('msg'),
            'deleted_creator': user_mgr.get(user_id=''),
            'user_likes': action_mgr.find_likes_by_user_id_creation_dict(session.user_id),
            'sort': sort.value,
            'creations': creations,
            'pledge_total': rewards_give_mgr.pledge_sums_by_user_id(self.page_user_id),
            'is_page_private': page_user.is_private,
            'page_user_supporting': page_user_supporting,
            'page_user_past_pledges': list(set(past_supporting)),
            'is_page_owner': session.user_id == self.page_user_id,
            'is_admin': session.is_admin,
            'commenters': commenters
        }

    def template(self):
        return "UserPatron/user_patron.html"
