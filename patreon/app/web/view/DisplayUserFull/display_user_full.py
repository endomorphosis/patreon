from patreon.model.manager import user_location_mgr, user_mgr, pledge_mgr
from patreon.app.web.view import base


class DisplayUserFull(base.View):
    def init(self, user_id, creator):
        self.user = user_mgr.get(user_id=user_id)
        self.creator = creator

    def data(self):
        locations = user_location_mgr.find_by_user_id(self.user.user_id).all()
        location = None
        if locations is not None and len(locations) > 0:
            location = locations[0].display_name

        supported_creators_count = len(pledge_mgr.find_by_patron_id(self.user.user_id))

        return {
            'user': self.user,
            'creator': self.creator,
            'support_count': supported_creators_count,
            'location': location,
            'support_string': "creator{s}".format(
                s='s' if supported_creators_count != 1 else ''
            ),
            'image_url': self.user.thumbnail_url or self.user.thumb_url,
            'full_name': self.user.full_name,
            'url': self.user.patreon_url()
        }

    def template(self):
        return "DisplayUserFull/display_user_full.html"
