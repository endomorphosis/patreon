"""
### Ported from index.php
### bb39889891d3f38bb6d5ca44d77c3944beb48968
"""
from flask import request

from patreon.app.web.view import base
from patreon.model.type import Category
from patreon.model.manager import curation_mgr, category_mgr
from patreon.util.unsorted import jsencode


class IndexContent(base.View):
    def init(self, in_home_v2_test):
        self.in_home_v2_test = in_home_v2_test

    def data(self):
        if self.in_home_v2_test:
            return {}

        featured_posts = curation_mgr.find_posts_by_single_category(
            Category.FRONT_PAGE.category_id
        )

        return {
            'all_featured': featured_posts,
            'categories_data': category_mgr.as_legacy_dict(),
            'rgsn': request.args.get('rgsn'),
            'f': request.args.get('f'),
            'js_category_id': jsencode(Category.FRONT_PAGE.category_id)
        }

    def template(self):
        if self.in_home_v2_test:
            return "IndexPage/index2.html"
        else:
            return "IndexPage/index.html"
