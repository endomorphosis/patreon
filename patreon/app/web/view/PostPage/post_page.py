import random
from flask import session, request

from patreon.app.web.view import base
from patreon.constants import is_user_hack
from patreon.model.manager import action_mgr, activity_mgr, campaign_mgr, \
    complete_pledge_mgr, link_pledge_activity_mgr, pledge_mgr, reward_mgr, \
    user_mgr
from patreon.model.table import User, Attachments
from patreon.model.type import post_type_enum, PostType, ActionType
from patreon.util.unsorted import ternary, get_int, jsencode


class PostPage(base.View):
    page_load_event_name = 'Post Page'

    def init(self, page_user, activity):
        self.page_user = page_user
        self.post = activity

    def data(self):
        # 1. Get current user.
        current_user = user_mgr.get(session.user_id)

        # 2. Get creator.
        creator = campaign_mgr.try_get_user_by_campaign_id_hack(self.post.campaign_id)

        # 3. Get current pledge.
        current_pledge = complete_pledge_mgr.get_pledge_by_patron_and_campaign_or_creator(
            creator_id=self.page_user.user_id, patron_id=session.user_id
        ).legacy
        current_user_pledge = current_pledge.amount if current_pledge else 0
        is_patron = current_user_pledge > 0

        # 4. Get permissions.
        is_owner = current_user and current_user.user_id == self.post.user_id
        is_campaign_owner = current_user and current_user.user_id in self.post.campaign_user_ids
        is_private = bool(
            not is_owner
            and not is_campaign_owner
            and self.post.min_cents_pledged_to_view
            and self.post.min_cents_pledged_to_view > current_user_pledge
        )

        # 5. Get post_type.
        post_type = PostType.get(self.post.activity_type)

        # 6. Get post image.
        image_url = ternary(
            post_type is PostType.IMAGE_FILE,
            self.post.file_url,
            self.post.image_large_url
        )

        # 7. Get attachments.
        attachments = Attachments.find_by_activity_id(self.post.activity_id)
        audio_attachments = Attachments.find_music(self.post.activity_id)

        # 8. Setup audio player.
        is_audio_type = post_type is PostType.AUDIO_FILE
        has_mp3 = bool(audio_attachments or is_audio_type)

        # 9. Setup default pledge.
        default_pledge = reward_mgr.get_default_pledge_by_creator_id(self.page_user.user_id)
        if default_pledge is None or float(default_pledge) < 1.0:
            default_pledge_value = 1.0
        else:
            default_pledge_value = float(default_pledge)

        # 10. Get top patrons.
        top_patrons = pledge_mgr.find_top_patrons_by_creator_id(self.page_user.user_id)
        random.shuffle(top_patrons)
        for patron_id in top_patrons:
            User.get.prime(patron_id)

        # 11. Setup meta url.
        session.meta['url'] = self.post.canonical_url()

        return {
            'page_user': self.page_user,
            'activity': self.post,
            'image_url': image_url,
            'current_user': current_user,
            'campaign': self.post.campaign,
            'is_image_type': post_type in post_type_enum.IMAGE_TYPES,
            'is_embed_type': post_type in post_type_enum.EMBED_TYPES,
            'is_audio_type': is_audio_type,
            'is_private': is_private,
            'is_owner': is_owner,
            'is_patron': is_patron,
            'is_user_hack': is_user_hack(self.page_user.user_id),
            'default_pledge': float(default_pledge_value),
            'pledge_total': pledge_mgr.get_pledge_total_by_creator_id(self.page_user.user_id) or 0,
            'patron_count': pledge_mgr.get_patron_count_by_creator_id(self.page_user.user_id),
            'new_pledges': len(link_pledge_activity_mgr.find_by_activity_id(self.post.activity_id)),
            'other_activities': activity_mgr.find_creations_by_campaign_id(self.post.campaign.campaign_id).all(),
            'rewards': reward_mgr.find_by_creator_id(self.page_user.user_id),
            'current_user_pledge': current_user_pledge,
            'top_patrons': top_patrons,
            'has_liked': action_mgr.get_like(session.user_id, self.post.activity_id) is not None,
            'attachments': attachments,
            'audio_attachments': audio_attachments,
            'has_mp3': has_mp3,
            'get_alert': get_int(request.args.get('alert')),
            'creator': creator,
            'action_like_enum': ActionType.LIKE.value,
            'jsencode': jsencode
        }

    def template(self):
        return "PostPage/post_page.html"
