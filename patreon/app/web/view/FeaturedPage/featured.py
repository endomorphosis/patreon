"""
featured.py
"""
from flask import request, session

from patreon.app.web.view import base
from patreon.model.manager import curation_mgr, pledge_mgr, category_mgr
from patreon.model.type import SortType, Category
from patreon.util.unsorted import get_int

### Ported from featured.php
### c8147d4519f707c77ac92bd293d0d77991849b56


class FeaturedContent(base.View):
    def data(self):
        display_note = "display:none;"  # TODO(implement)
        alert_note = ""
        success_error = ""

        sort_by = request.args.get('srt')
        ty = request.args.get('ty')
        cat = get_int(request.args.get('cat', 0))

        # viewer_support is a dictionary from creator_id to pledge amount
        page_user_supporting = pledge_mgr.find_by_patron_id(session.user_id)
        viewer_support = {
            pledge.creator_id: pledge.amount
            for pledge in page_user_supporting
        }

        # default category to show all categories
        CATEGORY_ALL = Category.ALL.category_id
        cat = cat or CATEGORY_ALL

        #
        if cat == CATEGORY_ALL:
            all_shares = curation_mgr.find_all_posts()
        else:
            all_shares = curation_mgr.find_posts_by_category(cat)

        #
        user_shares = None
        categories_data = category_mgr.as_legacy_dict()

        # TODO(implement): This needs to have side effects on the page
        no_footer = (ty or all_shares)

        #
        if sort_by == SortType.LATEST.value:
            sort_text = 'Most Recent'
        else:
            sort_text = 'Recently Active'

        # TODO(postport): Seriously what the hell is this
        arg_for_cat = 'cat={}&'.format(cat) if cat else ''
        arg_for_ty = 'ty={}&'.format(ty) if ty else ''
        arg_for_sort_by = 'srt={}&'.format(sort_by) if sort_by else ''
        args_for_creations_url = arg_for_cat + arg_for_sort_by
        args_for_js_redirect = arg_for_ty + arg_for_cat
        args_for_type_url = arg_for_ty + arg_for_sort_by

        return locals()

    def template(self):
        return "FeaturedPage/featured.html"
