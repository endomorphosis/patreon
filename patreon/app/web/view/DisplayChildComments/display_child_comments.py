from patreon.app.web.view import base


class DisplayChildComments(base.View):

    def init(self, current_user_id, comment_heads, child_comments, creator,
             commenters, total_comments, level=0, comments_displayed=[]):
        self.current_user_id = current_user_id
        self.child_comments = child_comments
        self.comment_heads = comment_heads
        self.creator = creator
        self.level = level
        self.total_comments = total_comments
        self.comments_displayed = comments_displayed
        self.commenters = commenters

    def data(self):
        thread = ''
        if self.level < 9:
            thread = 'thread '
        return {
            'level': self.level,
            'comment_heads': self.comment_heads,
            'child_comments': self.child_comments,
            'thread': thread,
            'indent_px': self.level*10,
            'current_user_id': self.current_user_id,
            'creator': self.creator,
            'comments_displayed': self.comments_displayed,
            'total_comments': self.total_comments,
            'commenters': self.commenters

        }

    def template(self):
        return 'DisplayChildComments/display_child_comments.html'
