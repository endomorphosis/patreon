from flask import session, request

from patreon.app.web.view import base
from patreon.util.unsorted import get_query_string


class HeaderIframe(base.View):
    def data(self):
        session.meta['query_string'] = get_query_string(request)
        return session.meta

    def template(self):
        return "HeaderIframe/header_iframe.html"