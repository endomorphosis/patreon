from patreon.app.web.view import base
from patreon.model.manager import complete_pledge_mgr, user_mgr
from patreon.util.format import cents_as_dollar_string


class SidebarCreatorSupport(base.View):
    def init(self, patron_id, creator_id):
        self.patron_id = patron_id
        self.creator_id = creator_id

    def data(self):
        creator = user_mgr.get_user(self.creator_id)
        pledge = complete_pledge_mgr.get_pledge_by_patron_and_campaign_or_creator(
            patron_id=self.patron_id, creator_id=self.creator_id
        ).legacy

        return {
            'creator_id': self.creator_id,
            'creator': creator,
            'pledge_amount_string': cents_as_dollar_string(pledge.amount),
            'edit_url': pledge.edit_url
        }

    def template(self):
        return "SidebarCreatorSupport/sidebar_creator_support_partial.html"
