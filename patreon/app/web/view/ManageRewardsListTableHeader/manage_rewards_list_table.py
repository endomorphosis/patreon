from flask import request
from patreon.app.web.view import base


class ManageRewardsListTableHeader(base.View):
    def data(self):
        return {
            'get_srt': request.args.get('srt'),
            'get_up': request.args.get('up'),
            'get_hid': request.args.get('hid'),
            'get_c': request.args.get('c')
        }

    def template(self):
        return "ManageRewardsListTableHeader/manage_rewards_list_table.html"
