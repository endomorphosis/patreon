import patreon
from patreon.app.web.view import base
from patreon.model.internal.placard import *

class GenericDashboardPage(base.View):

    def init(self, card_rows, page_title='Untitled Dashboard'):
        self.page_title = page_title
        self.card_rows = card_rows

    def data(self):
        return {
            'title': self.page_title,
            'auth_token': patreon.services.firebase.FIREBASE_TOKEN,
            'stage': patreon.config.stage,
            'card_rows': [[card.to_dict() for card in row] for row in self.card_rows]
        }

    def template(self):
        return 'GenericDashboardPage/generic_dashboard_page.html'
