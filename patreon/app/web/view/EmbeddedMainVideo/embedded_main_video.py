from patreon.app.web.view import base

"""
Handles the MainVideo - both the displayed image and the embedding
"""
class EmbeddedMainVideo(base.View):
    def init(self, main_video_url, main_video_embed, image_url, is_page_owner, page_user_full_name):
        self.main_video_embed = main_video_embed
        self.main_video_url = main_video_url
        self.image_url = image_url
        self.is_page_owner = is_page_owner
        self.full_name = page_user_full_name

    def data(self):
        video = set_autoplay(self.main_video_embed)
        video = set_height(video, 402)

        return {
            'main_video_url': self.main_video_url,
            'main_video_embed': video,
            'image_url': self.image_url,
            'is_page_owner': self.is_page_owner,
            'full_name': self.full_name
        }

    def template(self):
        return "EmbeddedMainVideo/embedded_main_video.html"


"""
Methods to manipulate the embedded_video
"""
def set_autoplay(embed_video):
    if embed_video is None:
        return embed_video

    if embed_video.find('autoplay') > -1:
        # If autoplay is already there, make sure it's set to 1
        embed_video = embed_video.replace("autoplay=0", "autoplay=1")
    else:
        # Otherwise add autoplay to the schema
        embed_video = embed_video.replace('schema=youtube', 'schema=youtube&autoplay=1')
        embed_video = embed_video.replace('schema=vimeo', 'schema=vimeo&autoplay=1')

    return embed_video


def set_height(embed_video, height):
    if embed_video is None:
        return embed_video

    if embed_video.find('height=') > -1:
        left_side, intermediate = embed_video.split('height=')
        height_val, right_side = intermediate.split('"', 1)

        result = left_side + 'height="' + str(height) + '"' + right_side

        return result
    else:
        return embed_video


def set_width(embed_video, width):
    if embed_video is None:
        return embed_video

    if embed_video.find('width=') > -1:
        left_side, intermediate = embed_video.split('width=')
        height_val, right_side = intermediate.split('"', 1)

        result = left_side + 'width="' + str(width) + '"' + right_side

        return result
    else:
        return embed_video
