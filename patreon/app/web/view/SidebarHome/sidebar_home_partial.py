from flask import session
from patreon.app.web.view import base
from patreon.model.manager import follow_mgr, pledge_mgr, rewards_give_mgr, \
    user_mgr
from patreon.util import format


class SidebarHome(base.View):
    def init(self):
        pass

    def data(self):
        # 1. Current User Section.
        current_user_id = session.user_id
        current_user = user_mgr.get_user(current_user_id)

        # 2. Current Pledges Section.
        pledges_to_creators = pledge_mgr.find_by_patron_id(current_user_id)
        pledge_sum = pledge_mgr.get_pledge_total_by_patron_id(current_user_id)
        pledge_sum_string = format.cents_as_dollar_string(int(pledge_sum))

        # 3. Pledge History Section.
        all_past_user_ids = rewards_give_mgr.find_processed_creators_by_user_id(
            current_user_id
        )
        supported_creators_count = len(all_past_user_ids)
        supported_creators_count_string = "{n} Creator{s}".format(
            n=supported_creators_count,
            s='s' if supported_creators_count != 1 else ''
        )

        # 4. Following Section.
        creator_follows = follow_mgr.find_by_follower_id(current_user_id)
        following_users_ids = [follow.followed_id for follow in creator_follows]
        following_users_count = len(creator_follows)
        following_users_count_string = "{n} Creator{s}".format(
            n=following_users_count,
            s='s' if following_users_count != 1 else ''
        )

        return {
            'current_user_id': current_user_id,
            'current_user': current_user,
            'pledges_to_creators': pledges_to_creators,
            'pledge_sum_string': pledge_sum_string,
            'all_past_user_ids': all_past_user_ids,
            'supported_creators_count_string': supported_creators_count_string,
            'following_users_ids': following_users_ids,
            'following_users_count_string': following_users_count_string
        }

    def template(self):
        return "SidebarHome/sidebar_home_partial.html"
