from flask import session

from patreon.model.manager import user_mgr
from patreon.app.web.view import base


class RewardsBox(base.View):
    def init(self, reward, create_text):
        self.reward = reward
        self.create_text = create_text

    def data(self):
        current_user = user_mgr.get(session.user_id)
        is_waiting = (
            current_user
            and current_user.rewards_wait_ids
            and self.reward.RID in current_user.rewards_wait_ids
        )

        return {
            'reward': self.reward,
            'create_text': self.create_text,
            'remaining': self.reward.remaining(),
            'is_waiting': is_waiting
        }

    def template(self):
        return "RewardsBox/rewards_box.html"
