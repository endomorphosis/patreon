from flask import session, request
from patreon.app.web.view import base
from patreon.model.manager import comment_mgr, feature_flag
from patreon.model.table import Comment


class DisplayComments(base.View):
    def init(self, current_user_id, activity_id, creator, commenters,
             total_comments, level=0):
        self.current_user_id = current_user_id
        self.activity_id = activity_id
        self.creator = creator
        self.level = level
        self.total_comments = total_comments
        self.commenters = commenters

    def data(self):
        raw_comments = Comment.find_by_activity_id(self.activity_id)
        if feature_flag.is_enabled('flat_comments', group_id=request.ab_test_group_id, user_id=session.user_id):
            comments = comment_mgr.raw_comments_to_structured_comments_with_max_depth(raw_comments)
        else:
            comments = comment_mgr.raw_comments_to_structured_comments(raw_comments)
        thread = ''
        if self.level < 9:
            thread = 'thread '

        heads = comments['parents'][-3:]

        return {
            'level': self.level,
            'comment_heads': heads,
            'child_comments': comments['children'],
            'thread': thread,
            'indent_px': self.level * 10,
            'current_user_id': self.current_user_id,
            'creator': self.creator,
            'comments_displayed': [],
            'total_comments': count_truncated_comments(heads, comments['children']),
            'commenters': self.commenters
        }

    def template(self):
        return 'DisplayComments/display_comments.html'


def count_truncated_comments(heads, children, count=0):
    for head in heads:
        count += 1
        if children[head.CID]:
            count_truncated_comments(children[head.CID], children, count)

    return count
