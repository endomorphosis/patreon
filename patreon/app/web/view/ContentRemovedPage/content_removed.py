from patreon.app.web.view import base


class ContentRemovedPage(base.View):
    def template(self):
        return "ContentRemovedPage/content_removed.html"
