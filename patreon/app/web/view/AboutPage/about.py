from patreon.app.web.view import base


class AboutPage(base.View):
    def template(self):
        return "AboutPage/about.html"
