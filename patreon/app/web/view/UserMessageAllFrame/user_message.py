from flask import session

import patreon
from patreon.app.web.view import base
from patreon.model.manager import message_mgr, user_mgr


class UserMessageAllContent(base.View):
    def data(self):
        if not session.user_id:
            patreon.routing.redirect('/login')

        all_messages = message_mgr.get_messages_and_clear_unread(session.user_id)
        count_all_messages = len(all_messages)

        senders_by_id = {}
        for message in all_messages:
            sender_id = message.sender_id
            senders_by_id[sender_id] = user_mgr.get(user_id=sender_id)

        return locals()

    def template(self):
        return "UserMessageAllFrame/user_message_all.html"


