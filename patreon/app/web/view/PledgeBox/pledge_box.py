from patreon.app.web.view import base


class PledgeBox(base.View):
    def init(self, creator_id, is_patron, pay_per_name, default_pledge):
        self.creator_id = creator_id
        self.is_patron = is_patron
        self.pay_per_name = pay_per_name
        self.default_pledge = default_pledge

    def data(self):
        return {
            'creator_id': self.creator_id,
            'is_patron': self.is_patron,
            'pay_per_name': self.pay_per_name,
            'default_pledge': self.default_pledge,
            'edit_pledge_url': "/bePatronConfirm?edit=1&u={}".format(self.creator_id)
        }

    def template(self):
        return "PledgeBox/pledge_box.html"
