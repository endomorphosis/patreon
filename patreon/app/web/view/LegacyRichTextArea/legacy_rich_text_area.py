from flask import request, session
from patreon.model.manager import feature_flag
from patreon.app.web.view import base


class LegacyRichTextArea(base.View):
    def init(self, name, value):
        self.name = name
        self.value = value

    def data(self):
        return {
            'name': self.name,
            'value': self.value
        }

    def template(self):
        template_version = 'old'
        if feature_flag.is_enabled('creator_page_v2', group_id=request.ab_test_group_id, user_id=session.user_id):
            template_version = 'creator_page_v2'
        return 'LegacyRichTextArea/legacy_rich_text_area.{0}.html'.format(template_version)
