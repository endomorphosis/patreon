import json
from decimal import Decimal, InvalidOperation
from urllib import parse
from flask import request

import patreon
from patreon.app.web.view import base
from patreon.model.manager import user_mgr


class BePatronSinglePage(base.View):
    def init(self, internal_page):
        # handle the case where user cancels out of paypal on the paypal site
        if request.args.get('paypal_cancel'):
            paypal_cancel_params = {
                'u': request.args.get('u'),
                'rid': request.args.get('rewardId')
            }

            if request.args.get('amount'):
                paypal_cancel_params['patAmt'] = str(Decimal(request.args.get('amount')) / 100)

            patreon.routing.redirect('/bePatronConfirm?' + parse.urlencode(paypal_cancel_params))

        if request.args.get('u'):
            self.creator_uid = request.args.get('u')
        else:
            # paypal redirect might not use 'u' param
            self.creator_uid = request.args.get('creatorId')

        self.pledge_amount = request.args.get('amt', request.args.get('patAmt', None))

        if self.pledge_amount is not None:
            try:
                self.pledge_amount = Decimal(self.pledge_amount)
            except InvalidOperation:
                self.pledge_amount = None

        self.edit = request.args.get('edit')
        self.internal_page = internal_page
        self.stripe_key = patreon.config.stripe['publishable_key']

        self.reward_id = request.args.get('rid', request.args.get('selectReward', None))

        # these keys only come from a redirect from the paypal process
        # and are for skipping directly to the end and pledging
        # we use the presence of cardId to branch the behavior
        if request.args.get('cardId'):
            self.paypal_redirect = "true"

            paypal_redirect_keys = [
                'street1',
                'street2',
                'city',
                'state',
                'zip',
                'country',
                'rewardId',
                'amount',
                'creatorId',
                'pledgeCap',
                'cardId'
            ]

            self.paypal_redirect_vars = { key: request.args.get(key) for key in paypal_redirect_keys }
            self.paypal_redirect_vars = json.dumps(self.paypal_redirect_vars)
        else:
            self.paypal_redirect = "false"
            self.paypal_redirect_vars = "{}"

    def data(self):
        if not self.creator_uid:
            patreon.routing.redirect('/')

        creator = user_mgr.get(self.creator_uid)

        if not creator or not creator.is_active_creator:
            patreon.routing.redirect('/')

        campaign = creator.campaigns[0]

        output = {
            'campaign_id': campaign.campaign_id,
            'edit': self.edit,
            'pledge_amount': self.pledge_amount or 'null',
            'reward_id': self.reward_id or 'null',
            'creator_id': self.creator_uid,
            'paypal_redirect_vars': self.paypal_redirect_vars,
            'paypal_redirect': self.paypal_redirect,
            'stripe_key': self.stripe_key,
            # 1 or 2, this means to start on reward selection or confirmation steps
            'internal_page': self.internal_page
        }

        return output

    def template(self):
        return "BePatronSinglePage/be_patron.html"
