from patreon.app.web.view import base

pages = {
    'about': {
        'image': 'about',
        'title': "About Us",
        'subtitle': "Patreon enables fans to give ongoing<br/>support to their favorite creators."
    },
    'careers': {
        'image': 'contact',
        'title': "Move the Creative World Forward",
        'subtitle': "Join us in building a brighter future for Creators of all kinds."
    },
    'press': {
        'image': 'about',
        'title': "Press",
        'subtitle': "Please feel free to email us directly at"
                    " <a href=\"mailto:press@patreon.com\">press@patreon.com</a>"
    }
}


class AboutBanner(base.View):
    def init(self, page):
        self.page = page

    def data(self):
        return pages[self.page]

    def template(self):
        return "AboutBanner/about_banner.html"
