from patreon.app.web.view import base


class PostEditor(base.View):
    def init(self, post_id):
        self.post_id = post_id

    def data(self):
        return {
            'post_id': self.post_id
        }

    def template(self):
        return 'PostEditor/post_editor.html'
