from patreon.app.web.view import base
from patreon.constants import image_sizes


class ProfileUpload(base.View):
    def init(self, user):
        self.user = user

    def data(self):
        if self.user.photo_key:
            photo_key = self.user.photo_key
            image_url = self.user.photo_url_size(image_sizes.USER_LARGE)
        else:
            photo_key = ''
            image_url = self.user.image_url

        thumbnail_url = self.user.thumbnail_url \
                    or self.user.thumb_url
        return {
            'photo_key': photo_key,
            'image_url': image_url,
            'thumbnail_url': thumbnail_url,
            'facebook_id': self.user.facebook_id
        }

    def template(self):
        return "ProfileUpload/profile_upload.html"
