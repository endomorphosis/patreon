from patreon.app.web.view import base


class InternalGraphs(base.View):
    def data(self):
        return {
            'graphs': [
                {'id': 'average_pledge_amount', 'label': 'Average Pledge Amount'},
                {'id': 'pledges_per_patron', 'label': 'Pledges Per Patron'},
            ],
            'tables': [
                {'id': 'all_stats', 'label': 'All Growth Stats'},
                {'id': 'payments_stats', 'label': 'Payments Stats'},
                {'id': 'patron_stats', 'label': 'Patron Stats'},
                {'id': 'creator_100_stats', 'label': 'Creators With At Least $100 Stats'},
                {'id': 'creator_stats', 'label': 'Creator Stats'},
                {'id': 'global_stats', 'label': 'Global Stats'},
                {'id': 'pledge_stats', 'label': 'Pledge Stats'},
                {'id': 'engagement_stats', 'label': 'Engagement Stats'},
            ]}

    def template(self):
        return 'InternalGraphs/internal_graphs.html'
