import patreon

from flask import session, render_template


def render_iframe(
        inner=None,
        hide_meta=False,
        no_share=False,
        show_big=False,
        fb_done=False,
        static_reduced=False,
        os_page=None,
        no_footer=False,
        i_bg_color='#FFF',
        title='Patreon',
        desc='Patreon is empowering a new generation of creators.\nSupport and engage with artists and creators as they live out their passions!',
        key='artists, crowdfunding, patron, sponsor, music, videos, fundraise',
        url=None,
        image_url=None,
        viewport='1020'
):
    session.meta = {
        'hide_meta': hide_meta,
        'no_share': no_share,
        'show_big': show_big,
        'fb_done': fb_done,
        'static_reduced': static_reduced,
        'os_page': os_page,
        'no_footer': no_footer,
        'title': title,
        'desc': desc,
        'key': key,
        'viewport': viewport,
        'i_bg_color': i_bg_color
    }
    # TODO(postport): modify meta.html to ensure None is valid
    if url:
        session.meta['url'] = url
    if image_url:
        session.meta['image_url'] = image_url

    return render_template(
        'BasePageIframe/base_page_iframe.html',
        session=session,
        patreon=patreon,
        inner=inner().render(),
        no_footer=no_footer,
        **patreon.globalvars.to_dict()
    )
