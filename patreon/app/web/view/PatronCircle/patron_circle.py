from patreon.app.web.view import base
from patreon.model.manager import user_mgr


class PatronCircle(base.View):
    def init(self, page_user, patron_id):
        self.page_user = page_user
        self.patron = user_mgr.get(user_id=patron_id)

    def data(self):
        return {
            'page_user': self.page_user,
            'patron': self.patron
        }

    def template(self):
        return 'PatronCircle/patron_circle.html'