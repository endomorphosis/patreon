from datetime import datetime

from patreon.app.web.view import base
from patreon.model.manager import rewards_give_mgr, payment_mgr
from patreon.services.caching import memcache
from patreon.util.format import format_money


def cron_status_data():
    now = datetime.now()

    month_end = datetime(year=now.year, month=now.month, day=1, hour=0, minute=0, second=0)
    month_start = datetime(year=now.year, month=(now.month - 1 or 12), day=1, hour=0, minute=0, second=0)
    month_string = month_end.strftime("%B")

    process_stats = payment_mgr.get_charges_setup_stats()

    total_set = rewards_give_mgr.get_entry_type_between_dates(None, month_start, month_end)
    pending_set = rewards_give_mgr.get_entry_type_between_dates(0, month_start, month_end)
    successful_set = rewards_give_mgr.get_entry_type_between_dates(1, month_start, month_end)
    declined_set = rewards_give_mgr.get_entry_type_between_dates(2, month_start, month_end)

    running_decline_rate = declined_set.sum / (successful_set.sum + declined_set.sum)

    data = {
        'month_string': month_string,
        # This is what the cron is actually queuing up
        'in_process': {
            'count': process_stats.count,
            'sum': format_money(process_stats.sum / 100)
        },
        # This is everything we plan to process
        'total': {
            'count': total_set.count,
            'dollars': format_money(total_set.sum / 100),
        },
        # This is what is marked as pending in the table, SLIGHTLY different than in_process
        'pending': {
            'count': pending_set.count,
            'dollars': format_money(pending_set.sum / 100),
            'percent_charges': "%.2f" % ((pending_set.count / total_set.count) * 100),
            'percent_dollars': "%.2f" % ((pending_set.sum / total_set.sum) * 100),
        },
        # This finished, was declined
        'declined': {
            'count': declined_set.count,
            'dollars': format_money(declined_set.sum / 100),
            'percent_charges': "%.2f" % ((declined_set.count / total_set.count) * 100),
            'percent_dollars': "%.2f" % ((declined_set.sum / total_set.sum) * 100),
            'running_rate': "%.2f" % (running_decline_rate * 100)
        },
        # This finished, has succeeded
        'successful': {
            'count': successful_set.count,
            'dollars': format_money(successful_set.sum / 100),
            'projected_dollars': format_money(((1 - running_decline_rate) * total_set.sum) / 100),
            'percent_charges': "%.2f" % ((successful_set.count / total_set.count) * 100),
            'percent_dollars': "%.2f" % ((successful_set.sum / total_set.sum) * 100),
        }

    }

    memcache.set('payment_cron_status_dashboard_data', data)
    return data


class InternalPaymentCronStatus(base.View):
    def data(self):
        return memcache.get('payment_cron_status_dashboard_data') or cron_status_data()

    def template(self):
        return 'InternalPaymentCronStatus/internal_payment_cron_status.html'
