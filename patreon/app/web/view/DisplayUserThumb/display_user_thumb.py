from patreon.app.web.view import base


class DisplayUserThumb(base.View):
    def init(self, url=None, thumb_url=None, full_name=None, extra_class=None):
        self.extra_class = extra_class
        self.thumb_url = thumb_url
        self.url = url
        self.full_name = full_name

    def data(self):
        return {
            'extra_class': self.extra_class,
            'thumb_url': self.thumb_url,
            'full_name': self.full_name,
            'url': self.url
        }

    def template(self):
        return "DisplayUserThumb/display_user_thumb.html"
