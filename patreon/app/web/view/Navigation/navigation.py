import patreon

from flask import request, session

from patreon.app.web.view import base
from patreon.model.manager import user_mgr, message_mgr, \
    feature_flag


def current_protocol():
    return request.url.partition(':')[0]


class Navigation(base.View):
    def init(self, no_share=False):
        self.no_share = no_share

    def data(self):
        current_user = user_mgr.get(user_id=session.user_id)
        if current_user is None:
            image_sizes = {
                'fullsize_url': None,
                'thumbnail_url': None,
            }
            is_creator = False
            logged_in = None
        else:
            image_sizes = {
                'fullsize_url': current_user.image_url,
                'thumbnail_url': current_user.thumb_url,
            }
            is_creator = current_user.is_active_creator
            no_share = self.no_share
            logged_in = str(session.user_id)

        display_name = ''
        if session.user_id and user_mgr.get(user_id=session.user_id):
            display_name = user_mgr.get(user_id=session.user_id).full_name

        return {
            'navigation': {
                'is_logged_in': logged_in,
                'is_https': current_protocol(),
                'is_creator': is_creator,
                'message_amount': message_mgr.get_unread(session.user_id),
                'image': image_sizes,
                'display_name': display_name,
                'no_share': self.no_share,
                'is_vanity': session.meta.get('is_vanity', False),
                'page_user_id': session.meta.get('page_user_id'),
                'referer_url': request.headers.get('Referer')
            }
        }

    def template(self):
        return "Navigation/navigation.html"
