from patreon.app.web.view import base
from flask import session
from patreon.model.manager import user_location_mgr, user_mgr, complete_pledge_mgr


class Sidebar(base.View):
    def init(self, page_user_id, user_patron_following=False):
        self.page_user_id = page_user_id
        self.user_patron_following = user_patron_following

    def data(self):
        page_user = user_mgr.get(user_id=self.page_user_id)
        current_user = user_mgr.get(user_id=session.user_id)

        return {
            'page_user': page_user,
            'current_user': current_user,
            'is_page_owner': session.user_id == page_user.UID,
            'locations': user_location_mgr.find_by_user_id(self.page_user_id).all(),
            'user_patron_following': self.user_patron_following,
            'page_user_has_default_image': page_user.has_default_image,
            'is_patron': complete_pledge_mgr.get_pledge_by_patron_and_campaign_or_creator(
                creator_id=page_user.UID, patron_id=session.user_id
            ).legacy is not None
        }

    def template(self):
        return "UserLeftSidebar/user_left_sidebar_partial.html"
