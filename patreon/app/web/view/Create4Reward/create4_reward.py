from patreon.app.web.view import base


class CreateReward(base.View):
    def init(self, reward=None, deleteable=False):
        self.reward = reward
        self.deleteable = deleteable

    def data(self):
        return {
            'reward': self.reward,
            'deleteable': self.deleteable
        }

    def template(self):
        return "Create4Reward/create4_reward.html"
