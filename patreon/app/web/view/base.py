"""
Base classes of views for extending. The classes here are:

Page - used to indicate an endpoint for a given route. pages have headers and footers.
View - similar to partial templates in other web frameworks
PageIFrame - similar to page without a header or footer

"""
import patreon

from flask import session, render_template
from jinja2 import Markup
from patreon.util import html_cleaner, format
from patreon.services.caching import memcache


class View(object):

    def __init__(self, *args, **kwargs):
        self._args = args
        self._kwargs = kwargs
        self.init(*args, **kwargs)

    def init(self):
        pass

    def data(self):
        return {}

    def cache_key(self):
        # Override in the subclass if we want to render this with
        # the memcache caching. Eventually this should be calculated
        # automatically, but for now we have to reconcile between
        # SQLAlchemy models being passed in here.
        raise NotImplemented()

    def template(self):
        # Override in subclass.
        return None

    def context(self):
        context = {}
        context.update(patreon.globalvars.to_dict())
        context.update(self.data())
        context.update({
            'patreon': patreon,
            'session': session, # TODO(postport): See if we can remove this later.
            'ternary': patreon.util.unsorted.ternary, # TODO(postport): Figure out a better way for this to inject.
            'format_money': format.format_money, # TODO(postport): Figure out a better way for this to inject.
            'format_cents_value_as_money': format.format_cents_value_as_money, # TODO(postport): Figure out a better way for this to inject.
        })
        return context

    def render(self, cached=None):
        if cached:
            return self.render_with_cache(cached)
        else:
            template = self.template()
            if template is not None:
                context = self.context()
                html = render_template(template, **context)
                html = html_cleaner.angular_escape(html)
                return Markup(html)
            else:
                raise Error("Unable to render view: {0}".format("TODO"))

    def render_with_cache(self, cached):
        if not isinstance(cached, int):
            raise Error("Cached parameter in render() must be an integer of seconds.")
        cache_key = self.cache_key()
        maybe_html = memcache.get(cache_key)
        if maybe_html:
            return Markup(maybe_html)
        else:
            markup = self.render()
            html = markup.__html__()
            memcache.set(cache_key, html, expires_seconds=cached)
            return markup
