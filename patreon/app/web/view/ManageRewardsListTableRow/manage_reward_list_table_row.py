import datetime


from patreon.app.web.view import base


class ManageRewardsListTableRow(base.View):
    def init(self, row_user, reward_id, status, monthly_max, pledge, lifetime, street1, street2,
             city, state, zip, country, date, dark):
        self.row_user = row_user
        self.reward_id = reward_id
        self.status = status
        self.monthly_max = monthly_max
        self.pledge = pledge
        self.lifetime = lifetime

        if street1:
            if street2:
                self.street = street1 + street2
            else:
                self.street = street1
        else:
            self.street = None

        self.city = city
        self.state = state
        self.zip = zip
        self.country = country
        self.date = date or datetime.datetime.now()
        self.dark = dark

    def data(self):
        return {
            'row_user': self.row_user,
            'reward_id': self.reward_id,
            'status': self.status,
            'monthly_max': self.monthly_max,
            'pledge': self.pledge,
            'lifetime': self.lifetime,
            'street': self.street,
            'city': self.city or '',
            'state': self.state or '',
            'zip': self.zip or '',
            'country': self.country or '',
            'date': self.date.strftime('%Y-%m-%d'),
            'dark': self.dark
        }

    def template(self):
        return "ManageRewardsListTableRow/manage_reward_list_table_row.html"
