from patreon.app.web.view import base


class CreateGoal(base.View):
    def init(self, goal=None, deleteable=False):
        self.goal = goal
        self.deleteable = deleteable

    def data(self):
        return {
            'goal': self.goal,
            'deleteable': self.deleteable
        }

    def template(self):
        return "Create3Goal/create3_goal.html"