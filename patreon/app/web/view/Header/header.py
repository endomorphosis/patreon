import patreon

from flask import session, request

from patreon.app.web.view import base
from patreon.model.manager import user_mgr
from patreon.util.unsorted import get_query_string


def current_protocol():
    return request.url.partition(':')[0]


def current_page_url(use_get=True):
    if use_get:
        return patreon.globalvars.get('MAIN_SERVER') + request.path + get_query_string(request)
    else:
        return patreon.globalvars.get('MAIN_SERVER') + request.path


class Header(base.View):
    def __init__(self, patreon_fonts_override=False):
        session.meta['patreon_fonts_override'] = patreon_fonts_override

    def data(self):
        session.meta['query_string'] = get_query_string(request)
        session.meta['current_user'] = user_mgr.get(user_id=session.user_id)
        session.meta['api_server'] = patreon.config.api_server
        session.meta['bonfire_host'] = patreon.config.bonfire_host

        session.meta['is_admin'] = session.is_admin == 1

        return session.meta

    def template(self):
        return "Header/header.html"
