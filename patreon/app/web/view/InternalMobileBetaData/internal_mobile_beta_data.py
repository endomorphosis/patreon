from urllib import parse
from patreon.app.web.view import base
from patreon.stats import mobile_beta_data


class InternalMobileBetaData(base.View):
    def data(self):
        keys = mobile_beta_data.find_uploaded_data_keys()
        urls = [(key, '/internal/mobile_beta_data/data?key={}'.format(parse.quote(key))) for key in keys]
        return {
            'urls': urls
        }

    def template(self):
        return 'InternalMobileBetaData/internal_mobile_beta_data.html'
