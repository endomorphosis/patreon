import patreon
from patreon.app.web.view import base, CreationCommentDivLegacy

from flask import session
from patreon.model.manager import user_mgr, comment_mgr


class CreationPrototypeCommentDiv(base.View):
    def init(self, share, page_user):
        self.share = share
        self.page_user = page_user

    def render(self):
        share = self.share

        info = {
            'page_user': self.page_user.full_name,
            'prototype': True,
            'comment_id': 1,
            'score': 0,
            'created': 0,
            'message': '',
            'voted': 0,
            'deleted': False,
            'edited': False,
            'thread_depth': 0
        }
        if session.user_id:
            current_user = user_mgr.get(user_id=session.user_id)
            info['user'] = {
                'id': current_user.UID,
                'url': '',
                'name': current_user.full_name,
                'thumb': current_user.thumb_url,
                'merits': []
            }
        else:
            info['user'] = {
                'id': 0,
                'url': '',
                'name': '',
                'thumb': patreon.globalvars.get('IMG_PROFILE_DEFAULT'),
                'merits': []
            }

        return CreationCommentDivLegacy(share, info).render()
