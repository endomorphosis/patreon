from patreon.app.web.view import base


class ChannelPage(base.View):
    def template(self):
        return "ChannelPage/channel.html"
