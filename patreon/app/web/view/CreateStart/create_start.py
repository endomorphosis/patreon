from patreon.app.web.view import base


class CreateStart(base.View):
    def data(self):
        return {}

    def template(self):
        return "CreateStart/create_start.html"
