from flask import session
from patreon.app.web.view import base
from patreon.model.manager import rewards_give_mgr, payment_mgr


class Transactions(base.View):
    def init(self):
        self.transactions = get_all_transactions(session.user_id)

    def data(self):
        return {
            'transactions': self.transactions
        }

    def template(self):
        return "Transactions/transactions.html"


def get_all_transactions(user_id):
    payouts = payment_mgr.get_all_payouts_for_creator(user_id)
    owed = rewards_give_mgr.get_transactions_for_creator_totalled_by_month(user_id)

    transactions = sorted(payouts + owed, key=lambda t: t["Datetime"], reverse=True)

    return transactions
