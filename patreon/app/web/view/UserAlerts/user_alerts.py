from flask import session, request
from patreon import globalvars
from patreon.app.web.view import base
from patreon.model.manager import user_mgr, complete_pledge_mgr, rewards_give_mgr


class UserAlerts(base.View):
    def init(self, page_user, page_campaign=None):
        self.page_user = page_user
        self.page_campaign = page_campaign


    def data(self):

        alert_type = request.args.get('alert')
        try:
            alert_type = int(alert_type)
        except ValueError:
            alert_type = None
        except TypeError:
            alert_type = None

        return {
            'is_page_owner': session.user_id == self.page_user.UID,
            'alert_type': alert_type,
            'page_campaign': self.page_campaign,
            'page_user': self.page_user,
            'globalvars': globalvars,
            'is_default_tab': request.args.get('ty') in (None, 'h'),
            'total_pledged': rewards_give_mgr.pledge_sums_by_creator_id_and_user_id(
                creator_id=session.user_id, user_id=self.page_user.UID
            ),
            'current_pledge': complete_pledge_mgr.get_pledge_by_patron_and_campaign_or_creator(
                patron_id=self.page_user.UID, creator_id=session.user_id
            ).legacy,
            'deleted_creator': user_mgr.get(user_id=request.args.get('cid'))
        }

    def template(self):
        return "UserAlerts/user_alerts.html"
