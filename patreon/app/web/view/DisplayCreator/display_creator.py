from patreon.constants import image_sizes
from patreon.model.manager import campaign_mgr, user_mgr, rewards_give_mgr, complete_pledge_mgr
from patreon.app.web.view import base


class DisplayCreator(base.View):
    def init(self, page_user, creator_id, is_current_patron=True):
        self.page_user = page_user
        self.creator = user_mgr.get(user_id=creator_id)
        self.is_current_patron = is_current_patron

    def data(self):
        campaign_id = campaign_mgr.get_campaigns_users(user_id=self.creator.UID).campaign_id
        campaign = campaign_mgr.get(campaign_id=campaign_id)

        good = rewards_give_mgr.pledge_sums_by_creator_id_and_user_id(
            creator_id=self.creator.UID,
            user_id=self.page_user.UID
        ) / 10
        if self.is_current_patron:
            pledge_obj = complete_pledge_mgr.get_pledge_by_patron_and_campaign_or_creator(
                creator_id=self.creator.UID, patron_id=self.page_user.UID
            ).legacy

            pledge = pledge_obj.Pledge
            if good != 0:
                days = pledge_obj.days_supported
            else:
                days = 0
        else:
            pledge = 0
            days = 0

        refers = 0

        return {
            'page_user': self.page_user,
            'creator': self.creator,
            'can_edit': self.is_current_patron,
            'days': days,
            'good': good,
            'refers': refers,
            'comments': 0,
            'likes': 0,
            'views': 0,
            'pledge': pledge,
            'creator_image_normal': image_sizes.CREATOR_NORMAL.width,
            'campaign': campaign
        }

    def template(self):
        return "DisplayCreator/display_creator.html"
