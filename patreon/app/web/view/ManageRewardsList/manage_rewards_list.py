from flask import request
from patreon.app.web.view import base
from patreon.model.manager import post_mgr, pledge_mgr


class ManageRewardsList(base.View):
    def init(self, current_user, campaign):
        self.current_user = current_user
        self.campaign = campaign

    def data(self):
        post_id = request.args.get('hid')
        return {
            'current_user': self.current_user,
            'campaign': self.campaign,
            'alert_type': request.args.get('alert'),
            'deleted_creator_id': request.args.get('cid'),
            'activity': post_mgr.get_post(post_id),
            'get_c': request.args.get('c'),
            'get_ty': request.args.get('ty'),
            'patron_count': pledge_mgr.get_patron_count_by_creator_id(self.current_user.user_id)
        }

    def template(self):
        return "ManageRewardsList/manage_rewards_list.html"
