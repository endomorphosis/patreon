from patreon.app.web.view import base


class RewardProcessedTransaction(base.View):
    def init(self, transaction):
        self.transaction = transaction

    def data(self):
        return {
            'transaction': self.transaction,
            'amount': abs(self.transaction["Amount"]),
            'pledge': self.transaction["Pledge"],
            'fee': self.transaction["Fee"],
            'patreon_fee': self.transaction["PatreonFee"]
        }

    def template(self):
        return "RewardProcessedTransaction/reward_processed_transaction.html"
