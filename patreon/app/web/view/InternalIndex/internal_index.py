from patreon.app.web import web_app
from patreon.app.web.view import base


class InternalIndex(base.View):
    def data(self):
        return {
            'categories': web_app.internal_page_registry.keys(),
            'registry': web_app.internal_page_registry
        }

    def template(self):
        return 'InternalIndex/internal_index.html'
