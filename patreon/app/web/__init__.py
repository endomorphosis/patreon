"""
Code specific to routing webpages.
"""
import html
from urllib import parse
import socket

import flask
import newrelic.agent
from flask import session, request, redirect, Flask
from patreon.services.caching import MemoryBytecodeCache
from werkzeug._compat import BytesIO
from werkzeug.exceptions import RequestEntityTooLarge
import patreon
from patreon.exception.auth_errors import CSRFFailed, CSRFExpired
from patreon.model.request.csrf import handle_csrf_check
from patreon.model.manager import csrf, feature_flag, user_mgr
from patreon.util.collections import sort_dict_by_values
from patreon.util.format import cents_as_dollar_string, format_money, truncate
from patreon.util.html_cleaner import clean, clean_and_remove_links
from patreon.util.unsorted import append_get_var, build_darkmode_dict, \
    get_float, insert_links_in_text, url_is_patreon
from patreon.services.logging.unsorted import log_response, log_darkmode


# TODO(postport): Gotta be a better way than relative paths...
web_app = Flask(__name__, template_folder="view", static_url_path='', static_folder='../../../static')
# Use custom PHP-style sessions.
web_app.session_interface = patreon.model.request.session.PatreonSessionInterface()
web_app.request_class = patreon.model.request.PatreonRequest
# Initialize Rollbar error reporting if enabled.
patreon.services.logging.rollbar.initialize(web_app)

# Any errors that we handle with .errorhandler have to be explicitly added to
# newrelic.ini's "error_collector.ignore_errors" config. New Relic hijacks the
# `handle_user_exception` method in Flask that enables this feature, and reports
# exceptions even when we handle them ourselves
@web_app.errorhandler(patreon.routing.Redirect)
def handle_redirect_exception(error):
    return redirect(error.location)

# Initialize New Relic after Rollbar
if socket.gethostname() in patreon.config.newrelic_servers:
    web_app = newrelic.agent.WSGIApplicationWrapper(web_app)

web_app.jinja_env.trim_blocks = True
web_app.jinja_env.lstrip_blocks = True
web_app.jinja_env.globals.update(cents_as_dollar_string=cents_as_dollar_string)
web_app.jinja_env.globals.update(format_money=format_money)
web_app.jinja_env.globals.update(insert_links_into_text=insert_links_in_text)
web_app.jinja_env.globals.update(truncate=truncate)
web_app.jinja_env.globals.update(print=print)
web_app.jinja_env.globals.update(unescape=html.unescape)
web_app.jinja_env.globals.update(urlencode=parse.quote_plus)
web_app.jinja_env.globals.update(str=str)
web_app.jinja_env.globals.update(get_float=get_float)
web_app.jinja_env.globals.update(sort_dict=sort_dict_by_values)

web_app.jinja_env.filters['clean'] = clean
web_app.jinja_env.filters['clean_and_remove_links'] = clean_and_remove_links

if not patreon.is_dev_environment():
    web_app.jinja_env.cache = {}
    web_app.jinja_env.cache_size = -1
    web_app.jinja_env.bytecode_cache = MemoryBytecodeCache()
    web_app.jinja_env.auto_reload = False

web_app.internal_page_registry = {}


@web_app.before_request
def preserve_raw_post():
    if request.method == "POST" and request.content_length < patreon.config.max_attachment_bytes:
        request.raw_cache = request.get_data()


@web_app.before_request
def check_https():
    proto = request.headers.get('X-Forwarded-Proto')
    if proto == 'http':
        if request.query_string:
            fullpath = request.path + '?' + request.query_string.decode("utf-8", "ignore")
            fullpath.replace('%EF%BF%BD', '')
        else:
            fullpath = request.path
        return redirect('https://{hostname}{fullpath}'.format(
            hostname=patreon.config.main_server,
            fullpath=fullpath
        ))
    else:
        # Continue as always.
        return None


@web_app.before_request
def update_pstore():
    patreon.pstore.update()


@web_app.before_request
def record_darkmode():
    if request.method == "GET" and patreon.config.aws and patreon.config.darkmode_enabled:
        data = build_darkmode_dict(request.args, request.cookies, request.path)
        log_darkmode(data)


@web_app.after_request
def per_request_callbacks(response):
    result = patreon.model.request.call_deferred_functions(response)
    return result


@web_app.after_request
def add_response_headers(response):
    response.headers['Cache-control'] = 'private'
    response.headers['P3P'] = 'CP="IDC DSP COR ADM DEVi TAIi PSA PSD IVAi IVDi CONi HIS OUR IND CNT"'
    response.headers['X-Frame-Options'] = 'sameorigin'
    response.headers['X-Content-Type-Options'] = 'nosniff'
    response.headers['Strict-Transport-Security'] = 'max-age=31536000; includeSubDomains'
    if response.mimetype == 'text/html':
        response.headers['Content-Type'] = 'text/html; charset=utf-8'
    # todo: php parity in x-xss
    if 'creation' not in request.path:
        response.headers['X-XSS-Protection'] = '1; mode=block'
    else:
        response.headers['X-XSS-Protection'] = '0'
    return response


@web_app.after_request
def log_response_object(response):
    if response.mimetype == 'text/html':
        log_response(session, request, response)
    return response


@web_app.teardown_request
def kill_sqlalchemy_session(exception=None):
    from patreon.services.db import db_session, log_db_session

    db_session.commit()
    db_session.close()

    if log_db_session:
        log_db_session.commit()
        log_db_session.close()

@web_app.before_request
def show_2fa_settings():
    if session.is_admin:
        user = user_mgr.get(session.original_user_id)
        if not user.two_factor_secret:
            register_admin_data('2fa', True)


@web_app.before_request
def set_csrf_token():
    if session.user_id != 0 and session.csrf_token is None:
        session.csrf_token = csrf.get_user_secret(session.user_id)


@web_app.before_request
def csrf_protect():
    csrf_uri = request.form.get('CSRFToken[URI]')
    csrf_signature = request.form.get('CSRFToken[token]')
    csrf_time = request.form.get('CSRFToken[time]')
    if request.method == "POST":
        if csrf.is_web_csrf_protection_disabled(request.path):
            csrf.log_csrf_event('csrf_protection_disabled', csrf_time, csrf_uri, csrf_signature)
            return

        try:
            handle_csrf_check(csrf_uri, csrf_signature, csrf_time)
        except CSRFExpired:
            if request.referrer and url_is_patreon(request.referrer):
                return redirect(append_get_var(request.referrer, 'csrfExpired=1'))
            return redirect('/')
        except CSRFFailed:
            return redirect('/')


@web_app.before_request
def assign_ab_test_id():
    cookie_group_id = request.ab_test_group_id
    user_group_id = feature_flag.get_or_set_group_id_for_user(session.user_id, cookie_group_id)
    if cookie_group_id != user_group_id:
        feature_flag.set_group_id_cookie(user_group_id)
        temp_cookies = dict(request.cookies)
        temp_cookies['group_id'] = user_group_id
        request.cookies = temp_cookies


def get_qqfile():
    if request.content_length > patreon.config.max_attachment_bytes:
        raise RequestEntityTooLarge

    raw_data = request.raw_cache
    if raw_data:
        return BytesIO(raw_data)

    raise Exception('File Missing')


def register_admin_data(key, value):
    admin_data = flask.g.get('admin_data', {})
    admin_data[key] = value
    flask.g.admin_data = admin_data


from . import view, pages

web_app.jinja_env.globals.update(view=view)
