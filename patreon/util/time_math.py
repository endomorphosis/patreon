import datetime
from dateutil.relativedelta import relativedelta

def first_of_month_for_date(date):
    return date.replace(day=1)


def first_of_month_following_date(date):
    return first_of_month_for_date(date) + relativedelta(months = 1)


def first_of_previous_month_for_date(date):
    return first_of_month_for_date(date) - relativedelta(months = 1)


def first_of_month_for_datetime(dt):
    return first_of_month_for_date(dt.date())


def is_in_current_month(dt):
    now = datetime.datetime.now()
    return first_of_month_for_datetime(now) == first_of_month_for_datetime(dt)


def start_of_day(date):
    return datetime.datetime.combine(date, datetime.time(0))


def column_during_month_for_date(column, date):
    return (column >= start_of_day(first_of_month_for_date(date))) \
           & (column < start_of_day(first_of_month_following_date(date)))


def today_in_timezone(timezone):
    return datetime.now().astimezone(timezone).date()


