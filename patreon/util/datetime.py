from datetime import datetime, time, timezone, date, timedelta

from sqlalchemy import func
import re


def datetime_at_midnight_on_date(date_, tz=None):
    naive_midnight = datetime.combine(date_, time())
    if tz:
        return (tz.localize(naive_midnight)).astimezone(timezone.utc)
    else:
        return naive_midnight


def zone_string_for_timezone(timezone_=None):
    zone_str = 'UTC'
    if timezone_:
        zone_str = timezone_.tzname(None)
        if re.match(r'UTC[+-]', zone_str):
            zone_str = zone_str.split('UTC')[-1]
    return zone_str


def timezone_aware_sql_date_func(column, timezone_=None):
    return func.date(func.convert_tz(column, 'UTC', zone_string_for_timezone(timezone_)))


def timezone_aware_sql_month_func(column, timezone_=None):
    return func.date(
        func.date_format(func.convert_tz(column, 'UTC', zone_string_for_timezone(timezone_)), '%y-%m-01 00:00:00'))


def as_utc(dt):
    if hasattr(dt, 'tzinfo'):
        if dt.tzinfo:
            return dt.astimezone(timezone.utc)
        else:
            return dt.replace(tzinfo=timezone.utc)
    return dt


def offset_from_time(base_time=None, **kwargs):
    time = base_time or datetime.now()
    return time + timedelta(**kwargs)


def make_naive(dt):
    return as_utc(dt).replace(tzinfo=None)


def first_of_the_month(date_=None):
    if not date_:
        date_ = datetime.now()
    return date(year=date_.year, month=date_.month, day=1)


def beginning_of_this_month():
    return datetime_at_midnight_on_date(first_of_the_month(datetime.now()))


def seconds_since_epoch(dt):
    return int((make_naive(dt) - datetime(1970, 1, 1)).total_seconds())
