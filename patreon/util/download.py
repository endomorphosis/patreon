import sys
import requests
from flask import Response

import patreon
from patreon.services import logging


def downloadable_response(file_name, file_url):
    if not file_url:
        return ''

    try:
        resp = requests.head(file_url)
    except:
        return ''

    file_size = int(resp.headers["Content-Length"])
    if file_size > patreon.config.max_attachment_bytes:
        return ''

    @patreon.model.request.call_after_request
    def set_file_headers(response):
        response.headers['Content-Type'] = "application/octetstream"
        response.headers['Content-Disposition'] = 'attachment; filename="{}"'.format(file_name or '')
        response.headers['Content-Length'] = file_size
        return response

    def generate():
        chunk_size = 1500
        for chunk in requests.get(file_url, stream=True).iter_content(chunk_size):
            yield chunk

    try:
        return Response(generate())
    except:
        logging.rollbar.log(sys.exc_info(), 'error')
        return ''
