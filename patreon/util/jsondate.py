"""
Functions that convert times into json date formats. All times in
here are assumed to be milliseconds.
"""

import time
import datetime


SECONDS = 1000
MINUTES = 60 * SECONDS
HOURS = 60 * MINUTES
DAYS = 24 * HOURS
WEEKS = 7 * DAYS


def iso_now():
    return datetime.datetime.now().isoformat().split('.')[0]


def now():
    return int(time.time() * 1000)


def diff(seconds=0, minutes=0, hours=0, days=0, weeks=0):
    ret = 0
    ret += seconds * SECONDS
    ret += minutes * MINUTES
    ret += hours * HOURS
    ret += days * DAYS
    ret += weeks * WEEKS
    return ret


def date(second=0, minute=0, hour=0, day=0, month=0, year=0):
    d = datetime.datetime(year=year, month=month, day=day, hour=hour, minute=minute, second=second)
    return from_datetime(d)


def today(second=0, minute=0, hour=0):
    now = datetime.datetime.now()
    d = datetime.datetime(year=now.year, month=now.month, day=now.day, hour=hour, minute=minute, second=second)
    return from_datetime(d)


def from_datetime(d):
    return from_unix(d.timestamp())


def from_unix(s):
    return int(s * SECONDS)


def coerce(obj):
    """Try to figure out the type of the object and convert it into
    milliseconds since epoch. 90% of the time, it works all of the
    time."""
    if obj is None:
        return 0
    elif isinstance(obj, datetime.datetime):
        return from_datetime(obj)
    elif isinstance(obj, float) or isinstance(obj, int):
        # Should cover us for about 200 years.
        if obj > 10000000000:
            return int(obj)
        else:
            return from_unix(obj)
