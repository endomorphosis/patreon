"""
util/__init__.py

A dumping place for random utilities that are useful around the
codebase. Once this file gets too large, it may make sense to
break it apart.

This module is meant to be standalone. That is, it should not
import patreon or require the patreon module anywhere.
"""
import json
import time
import datetime
import math
from urllib.parse import parse_qs, quote_plus, urlparse, urlunparse, urlsplit, urlencode

import base64
import re
import patreon


def now():
    # Cast to an integer so that it is not implicitly cast by MySQL.
    return datetime.datetime.fromtimestamp(int(time.time()))


def jsencode(d):
    return json.dumps(d, sort_keys=True, indent=4, separators=(',', ': '))


def jsdecode(s):
    return json.loads(s)


def underscore_url(url):
    url = "".join(urlsplit(url)[1:])
    return re.sub(r"[^A-z\-0-9]+", "_", url)


def get_int(val):
    try:
        return int(val)
    except:
        return None

def get_float(val):
    try:
        return float(val)
    except:
        return None


def base64encode(data):
    return base64.b64encode(data.encode('utf-8')).decode('utf-8')


def ternary(condition, iftrue, iffalse=''):
    return iftrue if condition else iffalse


def insert_links_in_text(text):
    # TODO(postport): Do this in JS instead.
    return re.sub(
        # Crazy bad looking regex for http://links.com
        r'(?<!["=\'])(((f|ht)tp(s)?://)[-a-zA-Z?-??-?()0-9@:%_+.~#?&;//=]+)',
        r'<a target="_blank" href="\1">\1</a>',
        text
    )


def get_utf8_string(input):
    return str(input).encode('utf-8')


def url_is_patreon(url):
    hostname = urlparse(url).hostname
    if hostname is None:
        return True

    host_parts = hostname.split('.')

    return host_parts[-2].lower() == 'patreon'


def url_relative_to_full(url):
    result = urlparse(url)
    scheme = result.scheme or 'https'
    hostname = result.hostname or patreon.config.main_server
    new_url = urlunparse((
        scheme, hostname, result.path, result.params, result.query,
        result.fragment
    ))
    return new_url


def append_get_var(url, param):
    binding_char = '?'
    if '?' in url:
        binding_char = '&'

    return url + binding_char + param


def build_url_from_parameters(url, params):
    params = _flatten_get_parameter_dict(params)
    running_url = url
    for key in params.keys():
        value = params[key]
        running_url = append_get_var(running_url, '{}={}'.format(key, value))

    return running_url


def _flatten_get_parameter_dict(input_dictionary, key_prefix='', return_dict=None):
    return_dict = return_dict or {}
    for key in input_dictionary.keys():
        new_key = key if not key_prefix else key_prefix + '[{}]'.format(key)
        value = input_dictionary[key]
        if type(value) != dict:
            return_dict[new_key] = value
            continue

        return_dict = _flatten_get_parameter_dict(value, new_key, return_dict)

    return return_dict


def merge_sqlalchemy_results(result_list):
    return_dict = {}
    for result in result_list:
        if result is None:
            continue
        return_dict.update(result.__dict__)

    return return_dict


def get_query_string(local_request):
    return '?' + urlparse(local_request.url).query


def dollar_string_to_cents(val):
    try:
        val = float(val.replace('$', '').replace(',', '').strip()) * 100
        if math.isnan(val):
            return None
        return val
    except ValueError:
        return None
    except TypeError:
        return None


def build_darkmode_dict(get_params, cookies, path):
    get_param_string = ''
    for key, value in get_params.items():
        get_param_string += key + "=" + quote_plus(value) + "&"

    request_uri = path
    if get_param_string:
        request_uri += "?" + get_param_string[:-1]

    server = {
        'REQUEST_URI': request_uri
    }

    return {
        'get': dict(get_params),
        'server': server,
        'cookie': dict(cookies),
        'post': {}
    }


def strip_vanity_from_query_string(query_string):
    return '&'.join([
                        variable
                        for variable in query_string.split('&')
                        if not variable.startswith('v=') or variable.startswith('u=')
                        ])


def set_query_string_part(query_string, key, value):
    query_dict = parse_qs(query_string)
    query_dict[key] = value

    # doseq because parse_qs/urlencode isn't symmetric by default
    return urlencode(query_dict, doseq=True)


def split_name(full_name):
    if not full_name:
        return '', ''

    name_parts = full_name.strip().split(' ', 1)
    if len(name_parts) == 2:
        first_name = name_parts[0]
        last_name = name_parts[1]
    else:
        first_name = name_parts[0]
        last_name = ''
    return first_name, last_name
