import re
import bleach
from urllib.parse import urlparse


class HTMLCleaner:
    def allowed_tags(self):
        return [
            'a', 'b', 'blockquote', 'br', 'del', 'div', 'em', 'font', 'h1',
            'h2', 'h3', 'h4', 'i', 'iframe', 'img', 'input', 'li', 'p', 'pre',
            'span', 'strike', 'strong', 'u', 'ul', 'ol'
        ]

    def iframe_attribute_parser(self, name, value):
        if name in ['allowfullscreen',
                    'frameborder',
                    'height',
                    'mozallowfullscreen',
                    'seamless',
                    'webkitallowfullscreen',
                    'width',
                    'scrolling']:
            return True
        if name == 'src':
            valid_domains = [
                'youtube.com',
                'player.vimeo.com',
                'hulu.com',
                'twitch.tv',
                'cdn.embedly.com',
                'vine.co',
                'w.soundcloud.com',
                'bandcamp.com',
                'podcastone.com',
                'ccv.adobe.com',
                'dailymotion.com',
                'archive.org',
                'drive.google.com'
            ]
            url_parts = urlparse(value)
            if not url_parts.netloc:
                return False

            cleaned_netloc = url_parts.netloc \
                .replace('www.', '') \
                .replace('http://', '') \
                .replace('https://', '') \
                .replace('//', '')
            if cleaned_netloc in valid_domains:
                return True
        return False

    def a_attribute_parser(self, name, value):
        if name in ['href', 'rel', 'class']:
            return True
        if name == 'target':
            if value in ['_blank', '_self', '_target', '_top']:
                return True

        return False

    def allowed_attributes(self):
        return {
            '*': ['width', 'height', 'style', 'class', 'title'],
            'iframe': self.iframe_attribute_parser,
            'a': self.a_attribute_parser,
            'img': ['src'],
            'input': ['type', 'value', 'class', 'title']
        }

    def allowed_styles(self):
        return [
            'background-color', 'color', 'float', 'font-weight', 'height',
            'line-height', 'margin', 'margin-bottom', 'margin-left',
            'margin-right', 'margin-top', 'text-align', 'width'
        ]

    def strip_tags(self):
        return False

    def clean(self, text):
        return bleach.clean(
            text, self.allowed_tags(), self.allowed_attributes(),
            self.allowed_styles(), strip=self.strip_tags()
        )


class LinkRemovingCleaner(HTMLCleaner):
    def strip_tags(self):
        return True

    def allowed_tags(self):
        return [tag for tag in super().allowed_tags() if tag not in {'a'}]


class RewardDescriptionCleaner(LinkRemovingCleaner):
    def allowed_styles(self):
        return [
            'background-color', 'color', 'float', 'font-weight', 'line-height',
            'margin', 'margin-bottom', 'margin-left', 'margin-right',
            'margin-top', 'text-align'
        ]


class OnlyStripLinksCleaner():
    def clean(self, text):
        text = LinkRemovingCleaner().clean(text)
        return text


def clean(text):
    return HTMLCleaner().clean(text)


def clean_and_remove_links(text):
    return OnlyStripLinksCleaner().clean(text)


def clean_reward_description(text):
    return RewardDescriptionCleaner().clean(text)


# See: http://www.phpliveregex.com/p/9Y8 and https://gist.github.com/zsennenga/03d85924808d748eb4f9
def angular_escape(text):
    pattern1 = "((&(amp;|#38;|#26;)*)?({|#123;|#x7b;|lclub;)){2,2}"
    pattern2 = "((&(amp;|#38;|#26;)*)?(}|#125;|#x7d;|rclub;)){2,2}"
    text = re.sub(pattern1, '', text)
    return re.sub(pattern2, '', text)
