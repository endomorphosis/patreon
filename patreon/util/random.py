import random
import string
from OpenSSL import rand
from _sha256 import sha256


def randomstring(count=10):
    return ''.join(random.choice(string.ascii_lowercase) for _ in range(count))


def random_int(min=1, max=100000):
    return random.randint(min, max)


def get_verification_token():
    token = sha256(str(rand.bytes(128)).encode('utf-8')).hexdigest()

    return token[:32]


def random_key(count=64):
    return ''.join(
        random.choice(string.ascii_letters + string.digits)
        for _ in range(count)
    )
