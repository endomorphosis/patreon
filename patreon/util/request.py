from tempfile import NamedTemporaryFile
from urllib.request import urlopen, Request

fake_agent_1 = "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_9_3)" \
    " AppleWebKit/537.36 (KHTML, like Gecko)" \
    " Chrome/35.0.1916.47 Safari/537.36"

patreon_user_agent = "Patreon HTTP Robot"


def get_data_from_url(url, headers=None):
    if not headers:
        headers = {}
    headers['User-Agent'] = patreon_user_agent

    try:
        return urlopen(Request(url, headers=headers))
    except:
        return None


def get_content_type_of_url(url, headers=None):
    data = get_data_from_url(url, headers)
    if data:
        return data.getheader('content-type')
    return None


def get_simple_content_type_of_url(url, headers=None):
    content_type = get_content_type_of_url(url, headers)
    if content_type:
        return content_type.split(';')[0]
    return None


def get_url_contents(url, headers=None):
    data = get_data_from_url(url, headers)
    if data:
        try:
            return data.read()
        except:
            pass
    return None


def retrieve_url(url, headers=None):
    data = get_data_from_url(url, headers)
    if data:
        with NamedTemporaryFile(delete=False) as file:
            file.write(data.read())
            return file.name
    return None
