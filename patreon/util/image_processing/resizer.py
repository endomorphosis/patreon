from PIL import Image


def crop_and_resize(image, destination_width, destination_height,
                    top_offset=None):
    source_width, source_height = image.size

    source_ratio = source_width / source_height
    destination_ratio = destination_width / destination_height

    if source_ratio > destination_ratio:
        resize = resize_to_height(image, destination_height)
        return crop_for_too_wide(resize, destination_width)
    elif source_ratio < destination_ratio:
        resize = resize_to_width(image, destination_width)
        top_offset = calculate_top_offset(
            top_offset, source_width, source_height, destination_width,
            destination_height
        )
        return crop_for_too_high(resize, destination_height, top_offset)
    elif source_height != destination_height \
            or source_width != destination_width:
        return image.resize(
            (destination_width, destination_height), Image.ANTIALIAS
        )
    else:
        return image


def crop_for_too_wide(image, destination_width):
    source_width, source_height = image.size
    left = int((source_width - destination_width) / 2)
    return image.crop((
        left,
        0,
        left + destination_width,
        source_height
    ))


def calculate_top_offset(top_offset, source_width, source_height,
                         destination_width, destination_height,
                         window_width=640):
    source_ratio = source_height / source_width
    destination_ratio = destination_height / destination_width

    if source_ratio < destination_ratio:
        return 0

    maximum = window_width * (source_ratio - destination_ratio)

    if top_offset is None:
        top_offset = maximum / 2

    if top_offset <= 0:
        top_offset = 0

    if top_offset > maximum:
        top_offset = maximum

    return int(destination_width * top_offset / window_width)


def crop_for_too_high(resize, destination_height, top_offset):
    resize_width, resize_height = resize.size
    return resize.crop((
        0,
        top_offset,
        resize_width,
        top_offset + destination_height
    ))


def resize_to_height(image, destination_height):
    source_width, source_height = image.size
    return image.resize((
        int(destination_height * source_width / source_height),
        destination_height
    ), Image.ANTIALIAS)


def resize_to_width(image, destination_width):
    source_width, source_height = image.size
    return image.resize((
        destination_width,
        int(destination_width * source_height / source_width)
    ), Image.ANTIALIAS)


def shrink_to_width(image, destination_width):
    source_width, source_height = image.size
    if source_width <= destination_width:
        # TODO MARCOS log that we haven't resized.
        return image

    return resize_to_width(image, destination_width)


def arbitrary_crop_and_resize(image, destination_width, destination_height,
                              crop_width, crop_height=None, x=None, y=None):
    source_width, source_height = image.size

    if not crop_width or not crop_height:
        crop_width = crop_height = min(source_height, source_width)

    if x is None:
        x = int((source_width - crop_width) / 2)

    if y is None:
        y = int((source_height - crop_height) / 2)

    crop = image.crop((x, y, x + crop_width, y + crop_height))

    return crop.resize((destination_width, destination_height), Image.ANTIALIAS)
