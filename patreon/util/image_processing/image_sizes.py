"""
In this enumeration, there are three arguments: name, width, and height.
The name is used as a suffix on the image url.
If the height is None, then it just scales with the image.
"""


class ImageSizes():
    def __init__(self, name, width, height):
        self.name = name
        self.width = width
        self.height = height

    @property
    def dimensions(self):
        return self.width, self.height
