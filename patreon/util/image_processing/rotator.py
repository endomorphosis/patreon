from PIL import Image


def correct_file_rotation(filename, destination_format='JPEG'):
    with open(filename, 'rb') as file:
        try:
            image = Image.open(file).convert('RGB')
            corrected = correct_image_rotation(image)
            if corrected:
                # If we don't change the image, then don't overwrite.
                corrected.save(filename, destination_format)
        except OSError:
            # Not an image file. Do not rotate.
            pass


def correct_image_rotation(image):
    try:
        exif = image._getexif()
        orientation = exif.get(274)
    except AttributeError:
        # If the image doesn't have EXIF data.
        orientation = 1

    if orientation == 6:
        return image.rotate(90)
    elif orientation == 3:
        return image.rotate(180)
    elif orientation == 8:
        return image.rotate(270)

    # Horizontal (normal)
    if orientation is 1:
        pass

    # Mirrored horizontal (selfie)
    elif orientation is 2:
        return image.transpose(Image.FLIP_LEFT_RIGHT)

    # Rotated 180
    elif orientation is 3:
        return image.rotate(180)

    # Mirrored vertical
    elif orientation is 4:
        return image.rotate(180).transpose(Image.FLIP_LEFT_RIGHT)

    # Mirrored horizontal then rotated 90 CCW
    elif orientation is 5:
        return image.rotate(-90).transpose(Image.FLIP_LEFT_RIGHT)

    # Rotated 90 CCW
    elif orientation is 6:
        return image.rotate(-90)

    # Mirrored horizontal then rotated 90 CW
    elif orientation is 7:
        return image.rotate(90).transpose(Image.FLIP_LEFT_RIGHT)

    # Rotated 90 CW
    elif orientation is 8:
        return image.rotate(90)

    return None
