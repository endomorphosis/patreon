from PIL import Image

_content_type_format_map = {
    'image/gif':        'gif',
    'image/jpeg':       'jpeg',
    'image/pjpeg':      'jpeg',
    'image/png':        'png',
    'image/bmp':        'bmp',
    'image/svg+xml':    'svg',
    'image/tiff':       'tiff'
}

_format_map = {
    # bitmap
    'bmp':  'JPEG',
    'dib':  'JPEG',

    # postscript
    'eps':  'JPEG',
    'ps':   'JPEG',

    # jpeg
    'jpg':  'JPEG',
    'jpeg': 'JPEG',
    'jpe':  'JPEG',

    # portable pixelmap
    'ppm':  'JPEG',
    'pgm':  'JPEG',
    'pbm':  'JPEG',

    # gif
    'gif':  'PNG',

    # png
    'png':  'PNG',

    # tiff
    'tiff': 'JPEG',
    'tif':  'JPEG',

    # other
    'im':   'JPEG',
    'ico':  'JPEG',
    'pdf':  'JPEG',
    'psd':  'JPEG',
    'webp': 'JPEG'
}


def content_type_to_extension(content_type):
    format_ = _content_type_format_map.get(content_type)
    return '.' + format_ if format_ else None


def convert_format(source_format):
    if not source_format:
        return None

    # Take off '.' if it exists.
    if source_format[0] == '.':
        source_format = source_format[1:]

    return _format_map.get(source_format.lower())


def is_convertible_format(source_format):
    # Take off '.' if it exists.
    if source_format and source_format[0] == '.':
        source_format = source_format[1:]
    return source_format.lower() in _format_map.keys()


def is_image_file(filename):
    with open(filename, 'rb') as file:
        try:
            Image.open(file).convert('RGB')
            return True
        except OSError:
            return False
