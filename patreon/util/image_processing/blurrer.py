from PIL import Image, ImageFilter

def blur(image):
    source_width, source_height = image.size
    blur_radius = source_width // 16
    blur_filter = ImageFilter.GaussianBlur(radius=blur_radius)
    return image.filter(blur_filter)
