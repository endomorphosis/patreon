import locale
from _ctypes import ArgumentError
from decimal import Decimal
from pytz import timezone

try:
    locale.setlocale(locale.LC_ALL, 'en_US')
except locale.Error:
    locale.setlocale(locale.LC_ALL, 'en_US.UTF8')


def cents_as_dollar_string(cents):
    return '$' + format_cents_value_as_money(cents)


def format_cents_no_comma(cents_value):
    if not isinstance(cents_value, int):
        raise ArgumentError("cents must be an integer")

    return locale.format('%.2f', cents_value / 100.0)


def format_cents_value_as_money(cents_value):
    if isinstance(cents_value, Decimal):
        # Pledge totals are Decimals, so we handle them here ಠ_ಠ
        if cents_value != cents_value.to_integral_value():
            raise ArgumentError("cents must not contain a fractional value")
        cents_value = int(cents_value)
    if not isinstance(cents_value, int):
        raise ArgumentError("cents must be an integer")
    dollars, cents = divmod(cents_value, 100)
    s = format_money_whole(dollars)
    if cents:
        s += "." + locale.format("%02d", cents)
    return s


def format_money(number, cents=1):
    # TODO(postport): don't use this cents parameter
    if number is None:
        return None
    if isinstance(number, str):
        try:
            number = locale.atof(number)
        except ValueError:
            # User input error, default to 0 since this is only used
            # in output formatting (accuracy is not critical).
            number = 0
    if cents == 2:
        return format_money_with_cents(number)
    else:
        return format_money_whole(number)


def format_money_whole(number):
    return locale.format('%d', int(number), grouping=True)


def format_money_with_cents(number):
    return locale.format('%.2f', number, grouping=True)


def format_date(d):
    return d.strftime('%B %-d, %Y')


def php_date(date):
    utc_time = timezone('UTC').localize(date)
    return utc_time.strftime('%B %-d, %Y %H:%M:%S')


def truncate(s, length=120, ellipses='...'):
    if s and len(s) > length:
        return s[:length - len(ellipses)] + ellipses
    else:
        return s


def comma_list(list_, sep=',', conjunction='and'):
    if not list_:
        return ''
    list_ = [str(_) for _ in list_]
    if len(list_) == 1:
        return list_[0]
    if len(list_) == 2:
        return "{head} {conjunction} {tail}".format(
            head=list_[0],
            conjunction=conjunction,
            tail=list_[1]
        )
    return "{head}{sep} {conjunction} {tail}".format(
        head="{} ".format(sep).join(list_[:-1]),
        sep=sep,
        conjunction=conjunction,
        tail=list_[-1]
    )


def uncomma_list(list_, sep=','):
    if not list_:
        return []
    return list_.split(sep)


def uncomma_ints(list_, sep=','):
    int_list = []
    for i in uncomma_list(list_, sep):
        try:
            int_list.append(int(i))
        except ValueError:
            pass
    return int_list
