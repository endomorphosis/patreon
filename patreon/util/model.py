def dictionary_by_id(rows, id_getter="id"):
    return {
        getattr(row, id_getter): row
        for row in rows
    }


def order_by_list_of_ids(ids, rows, id_getter="id"):
    row_by_id = dictionary_by_id(rows, id_getter)
    return [row_by_id[id_] for id_ in ids]


def missing_ids(ids, rows, id_getter="id"):
    row_by_id = dictionary_by_id(rows, id_getter)
    return [id_ for id_ in ids if id_ not in row_by_id]
