from enum import Enum
from urllib.parse import quote, urlencode


class SocialCode(Enum):
    FACEBOOK    = 1
    TWITTER     = 2
    TUMBLR      = 3
    PIN         = 4
    GOOGLE      = 5


def twitter_text(user, campaign):
    twitter_name = ''
    if user.twitter:
        twitter_name += '@' + user.twitter
    if user.full_name:
        twitter_name += ' ' + user.full_name
    if not twitter_name:
        twitter_name += 'this creator'

    return "Support {0} creating {1} @Patreon".format(
        twitter_name, campaign.creation_name or 'things'
    )


def share_twitter_link(creator):
    user_url = creator.canonical_url('ty=' + str(SocialCode.TWITTER.value))
    creator_twitter = creator.Twitter

    if creator_twitter:
        text = "I'm supporting @{handle} {fullname} @Patreon".format(
            handle=creator_twitter,
            fullname=creator.full_name)
    else:
        text = "I'm supporting {fullname} @Patreon".format(
            fullname=creator.full_name)

    twitter_url = "https://twitter.com/share?" + urlencode({
        'url': quote(user_url),
        'text': text}).replace('%27', "%2527")

    return "javascript:sharePage({twitter_url}, 'twitter')".format(
        twitter_url=repr(twitter_url)
    )


def share_facebook_link(creator, text):
    user_url = creator.canonical_url('ty=' + str(SocialCode.FACEBOOK.value))
    facebook_url = "https://www.facebook.com/sharer.php?" + urlencode({
        'u': user_url,
        't': text
    }).replace('%27', "%2527")
    return "javascript:sharePage({facebook_url}, 'facebook')".format(
        facebook_url=repr(facebook_url)
    )
