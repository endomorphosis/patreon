# coding: utf-8
import patreon
from patreon.services import db
from sqlalchemy import Column, DateTime, Index, Integer, SmallInteger, ForeignKey
from sqlalchemy.ext.hybrid import hybrid_property
from sqlalchemy.orm import relationship


class Actions(db.Model):
    __tablename__ = 'tblActions'
    __table_args__ = (
        Index(
            'tblActions_UID_Type',
            'UID', 'Type'
        ),
    )

    UID     = Column(Integer, ForeignKey('tblUsers.UID'), primary_key=True, nullable=False)
    HID     = Column(Integer, ForeignKey('activities.activity_id'), primary_key=True, nullable=False, index=True)
    Type    = Column(SmallInteger, primary_key=True, nullable=False)
    Created = Column(DateTime, nullable=False, index=True)

    post    = relationship('Activity', uselist=False)
    user    = relationship('User', uselist=False)

    @property
    def user_id(self):
        return self.UID

    @property
    def post_id(self):
        return self.HID

    @property
    def activity_id(self):
        return self.post_id

    @hybrid_property
    def action_type(self):
        return self.Type

    @hybrid_property
    def created_at(self):
        return self.Created

    @hybrid_property
    def action_id(self):
        return '{0}-{1}-{2}'.format(self.action_type, self.user_id, self.activity_id)

    @staticmethod
    @patreon.services.caching.multiget_cached(object_key=('UID', 'HID', 'Type'))
    def get(user_id=None, activity_id=None, type_=None):
        return Actions.query\
            .filter(Actions.UID.in_(user_id)) \
            .filter(Actions.HID.in_(activity_id)) \
            .filter(Actions.Type.in_(type_))\
            .all()
