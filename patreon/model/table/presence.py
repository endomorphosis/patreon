# coding: utf-8
import datetime
from patreon.services import db
from sqlalchemy import BigInteger, Column, DateTime, String
from sqlalchemy.ext.hybrid import hybrid_property


class Presence(db.Model):
    __tablename__ = 'presence'

    presence_id     = Column(BigInteger, primary_key=True, nullable=False)
    user_id         = Column(BigInteger, nullable=False, index=True)
    room            = Column(String, nullable=False, index=True)
    last_seen       = Column(DateTime, nullable=False)
    expiration_time = Column(DateTime, nullable=False)

    @hybrid_property
    def is_live(self):
        return self.expiration_time > datetime.datetime.now()

    def activity_id(self):
        return Presence.activity_id_for_room_name(str(self.room))

    @staticmethod
    def activity_id_for_room_name(room_name):
        if 'post:' in room_name:
            return int(room_name.split('post:')[-1])
        return None

    @staticmethod
    def room_name_for_activity_id(activity_id):
        return 'post:'+str(activity_id)

    @staticmethod
    def get(presence_id):
        if presence_id is not None:
            return Presence.query.filter_by(presence_id=presence_id).first()
        return None
