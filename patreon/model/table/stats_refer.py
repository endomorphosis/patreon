# coding: utf-8
from patreon.services import db
from patreon.services.db import db_session
from sqlalchemy import Column, Integer


class StatsRefer(db.Model):
    __tablename__ = 'tblStatsRefer'

    UID     = Column(Integer, primary_key=True, nullable=False)
    CID     = Column(Integer, primary_key=True, nullable=False)
    HID     = Column(Integer, primary_key=True, nullable=False)
    Count   = Column(Integer, nullable=False)

    @property
    def patron_id(self):
        return self.UID

    @property
    def creator_id(self):
        return self.CID

    @property
    def activity_id(self):
        return self.HID

    @property
    def count(self):
        return self.Count

    @staticmethod
    def get(user_id=None, creator_id=None, activity_id=None):
        if user_id is not None and creator_id is not None:
            return db_session.query(StatsRefer).filter_by(UID=user_id, CID=creator_id).first()
        elif user_id is not None and activity_id is not None:
            return db_session.query(StatsRefer).filter_by(UID=user_id, HID=creator_id).first()
        return None
