# coding: utf-8
from patreon.services import db
from sqlalchemy import Column, Integer, Text, ForeignKey


class TwoFactorSecrets(db.Model):
    __tablename__ = 'two_factor_codes'

    two_factor_id   = Column(Integer, primary_key=True, nullable=False)
    user_id         = Column(Integer, ForeignKey("tblUsers.UID"), nullable=False)
    type            = Column(Integer, nullable=False, default=1)
    secret          = Column(Text, nullable=False)
