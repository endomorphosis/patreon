import re

from patreon import globalvars
from patreon.services import db
from sqlalchemy import Integer, Column, DateTime, String, text
from sqlalchemy.ext.hybrid import hybrid_property


class Payout(db.Model):
    __tablename__ = 'tblPayouts'

    PayID       = Column(Integer, primary_key=True)
    UID         = Column(Integer, nullable=False, index=True)
    Amount      = Column(Integer, nullable=False)
    Created     = Column(DateTime, nullable=False)
    Type        = Column(Integer, nullable=False, index=True, server_default=text("'0'"))
    TransID     = Column(String(32))
    Fee         = Column(Integer, nullable=False, server_default=text("'0'"))
    CreditUsed  = Column(Integer, nullable=False, server_default=text("'0'"))

    @hybrid_property
    def amount_cents(self):
        return self.Amount

    @hybrid_property
    def transaction_type(self):
        return self.Type

    @hybrid_property
    def user_id(self):
        return self.UID

    @hybrid_property
    def created_at(self):
        return self.Created

    @hybrid_property
    def is_payout(self):
        generic_payout_type = globalvars.get('PAYOUT_PAID')
        direct_deposit_type = globalvars.get('PAYOUT_DD')
        return (self.transaction_type == generic_payout_type) | (self.transaction_type == direct_deposit_type)

    @hybrid_property
    def external_id(self):
        return self.TransID

    def external_id_looks_like_paypal(self):
        return self.external_id and re.match('[A-Z0-9]{17}', self.external_id)
