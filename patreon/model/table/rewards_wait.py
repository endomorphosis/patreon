# coding: utf-8
from patreon.services import db
from sqlalchemy import Column, Integer, ForeignKey


class RewardsWait(db.Model):
    __tablename__ = 'tblRewardsWait'

    RID = Column(Integer, primary_key=True, nullable=False)
    UID = Column(Integer, ForeignKey("tblUsers.UID"), primary_key=True, nullable=False)

    @staticmethod
    def get(reward_id=None, user_id=None):
        return RewardsWait.query \
            .filter(RewardsWait.RID == reward_id) \
            .filter(RewardsWait.UID == user_id) \
            .first()
