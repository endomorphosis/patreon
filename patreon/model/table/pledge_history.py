from sqlalchemy.ext.hybrid import hybrid_property
from sqlalchemy import Column, DateTime, Integer
from patreon.services import db


class PledgesHistory(db.Model):
    __tablename__ = 'tblPledgesHistory'

    CID         = Column(Integer, primary_key=True, nullable=False)
    UID         = Column(Integer, primary_key=True, nullable=False)
    Pledge      = Column(Integer, nullable=False)
    MaxAmount   = Column(Integer)
    Created     = Column(DateTime, primary_key=True, nullable=False)

    @hybrid_property
    def patron_id(self):
        return self.UID

    @hybrid_property
    def creator_id(self):
        return self.CID

    @hybrid_property
    def amount(self):
        return self.Pledge

    @hybrid_property
    def pledge_cap(self):
        return self.MaxAmount

    @hybrid_property
    def created_at(self):
        return self.Created

    @staticmethod
    def get(user_id, creator_id):
        if user_id and creator_id:
            return PledgesHistory.query.filter_by(CID=creator_id, UID=user_id).first()
        return None
