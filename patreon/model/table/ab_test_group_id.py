# coding: utf-8
from patreon.services import db
from sqlalchemy import BigInteger, Column, String, ForeignKey


class ABTestAssignments(db.Model):
    __tablename__ = 'a_b_test_assignments'

    a_b_test_assignment_id = Column(BigInteger, primary_key=True)
    user_id     = Column(BigInteger, ForeignKey('tblUsers.UID'), nullable=False)
    group_id    = Column(String(255), nullable=False, index=True)
