# coding: utf-8
from patreon.services import db
from patreon.services.db import db_session
from sqlalchemy import Column, DateTime, Integer, ForeignKey, String
from sqlalchemy.orm import relationship


class UsersPushInfo(db.Model):
    __tablename__ = 'users_push_info'

    users_push_info_id  = Column(Integer, primary_key=True, index=True, nullable=False, autoincrement=True)
    user_id             = Column(Integer, ForeignKey('tblUsers.UID'), index=True, nullable=False)
    bundle_id           = Column(String(255), index=True)
    token               = Column(String(255))
    sns_arn             = Column(String(255))
    created_at          = Column(DateTime, nullable=False)
    edited_at           = Column(DateTime, nullable=False)

    user                = relationship('User', uselist=False)

    @staticmethod
    def get(user_id=None, bundle_id=None):
        if user_id is None and bundle_id is None:
            return None

        query = db_session.query(UsersPushInfo)
        if user_id is not None:
            query = query.filter_by(user_id=user_id)
        if bundle_id is not None:
            query = query.filter_by(bundle_id=bundle_id)

        if user_id is not None and bundle_id is not None:
            return query.first()
        else:
            return query.all()
