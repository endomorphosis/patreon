# coding: utf-8
from patreon.services import db
from sqlalchemy import Column, DateTime, Integer, String


class ChangeEmail(db.Model):
    __tablename__ = 'tblChangeEmail'

    Verification    = Column(String(32), primary_key=True)
    Email           = Column(String(128), nullable=False)
    UID             = Column(Integer, nullable=False)
    Created         = Column(DateTime, nullable=False, index=True)

    @property
    def verification_token(self):
        return self.Verification

    @property
    def email(self):
        return self.Email

    @property
    def user_id(self):
        return self.UID

    @property
    def created_at(self):
        return self.Created

    @staticmethod
    def get(verification=None):
        if verification is not None:
            return ChangeEmail.query.filter_by(Verification=verification).first()
        return None

    def update(self, update_values):
        ChangeEmail.query.filter_by(Verification=self.verification_token).update(update_values)
