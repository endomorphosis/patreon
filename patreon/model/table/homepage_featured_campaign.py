from patreon.services import db
from sqlalchemy import Column, DateTime, Integer, Index


class HomepageFeaturedCampaign(db.Model):
    __tablename__ = 'homepage_featured_campaigns'

    feature_id     = Column(Integer, primary_key=True)
    campaign_id     = Column(Integer, nullable=False, index=True)
    approval_status = Column(Integer, nullable=False, index=True)
    launched_at     = Column(DateTime, nullable=False)
