from patreon.model.table.column_types import HTMLString
from patreon.services import db
from patreon.services.db import db_session
from sqlalchemy import BigInteger, Column, DateTime, Integer, String, ForeignKey


class Goal(db.Model):
    __tablename__ = 'goals'

    goal_id         = Column(BigInteger, primary_key=True)
    campaign_id     = Column(Integer, ForeignKey('campaigns.campaign_id'), nullable=False, index=True)
    amount_cents    = Column(Integer, nullable=False)
    goal_title      = Column(String(256), nullable=False)
    goal_text       = Column(HTMLString)
    created_at      = Column(DateTime, nullable=False)
    reached_at      = Column(DateTime)

    @staticmethod
    def get(goal_id=None):
        if goal_id is not None:
            return db_session.query(Goal)\
                .filter_by(goal_id=goal_id)\
                .first()
        return None
