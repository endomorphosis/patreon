# coding: utf-8
from patreon.services import db
from sqlalchemy import Column, DateTime, Integer, String
from sqlalchemy.ext.hybrid import hybrid_property


class ForgotPassword(db.Model):
    __tablename__ = 'tblForgotPassword'

    SecretID = Column(String(32), primary_key=True)
    UID = Column(Integer, nullable=False, index=True)
    Created = Column(DateTime, nullable=False)

    @hybrid_property
    def token(self):
        return self.SecretID

    @hybrid_property
    def user_id(self):
        return self.UID

    @hybrid_property
    def created_at(self):
        return self.Created

    @staticmethod
    def get(user_id=None, token=None):
        if user_id and token:
            return ForgotPassword.query.filter_by(UID=user_id, SecretID=token).first()
        return None
