import datetime

from patreon.services import db
from sqlalchemy import BigInteger, Column, DateTime, Integer, String, text, ForeignKey


class ExternalPayment(db.Model):
    __tablename__ = 'external_payments'

    external_payment_id     = Column(BigInteger, primary_key=True)
    txn_id                  = Column(BigInteger, ForeignKey('txns.txn_id'), index=True)
    payment_instrument_id   = Column(BigInteger, nullable=False)
    type                    = Column(String(64))
    external_id             = Column(String(255), index=True)
    amount_cents            = Column(Integer, nullable=False)
    fee_cents               = Column(Integer, nullable=False, server_default=text("'0'"))
    created_at              = Column(DateTime, default=datetime.datetime.utcnow, nullable=False)
    succeeded_at            = Column(DateTime)
    pending_at              = Column(DateTime)
    failed_at               = Column(DateTime)

    @property
    def is_successful(self):
        return self.succeeded_at is not None

    @property
    def is_failed(self):
        return self.failed_at is not None
