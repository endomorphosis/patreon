# coding: utf-8
from patreon.services import db
from sqlalchemy import BigInteger, Column, DateTime, text
from sqlalchemy.ext.hybrid import hybrid_property


class Follow(db.Model):
    __tablename__ = 'follows'

    follower_id = Column(BigInteger, primary_key=True, nullable=False, server_default=text("'0'"))
    followed_id = Column(BigInteger, primary_key=True, nullable=False, index=True, server_default=text("'0'"))
    created_at  = Column(DateTime, nullable=False)

    @hybrid_property
    def follow_id(self):
        return '{0}-{1}'.format(self.follower_id, self.followed_id)

    @staticmethod
    def get(follower_id, followed_id):
        if follower_id is not None and followed_id is not None:
            return Follow.query.filter_by(followed_id=followed_id, follower_id=follower_id).first()
        return None
