# coding: utf-8
from sqlalchemy.ext.hybrid import hybrid_property
from sqlalchemy import Column, Integer, String, ForeignKey

import patreon
from patreon.services import db
from patreon.services.db import db_session
from patreon.services.file_helper import get_extension


class Attachments(db.Model):
    __tablename__ = 'tblSharesAttach'

    ID      = Column(Integer, primary_key=True)
    HID     = Column(Integer, ForeignKey('activities.activity_id'), nullable=False, index=True)
    Url     = Column(String(255), nullable=False)
    Name    = Column(String(128))

    # backref Activity

    @property
    def attachment_id(self):
        return self.ID

    @property
    def activity_id(self):
        return self.HID

    @property
    def url(self):
        # TODO MARCOS HACK temporarily fix urls
        # return self.Url
        return self.Url.replace("https://us-west-1", "//s3-us-west-1")

    @property
    def download_url(self):
        return "/posts/{0}/attachments/{1}"\
            .format(self.activity_id, self.attachment_id)

    @property
    def name(self):
        return self.Name

    @property
    def extension(self):
        return get_extension(self.name).lower()

    @hybrid_property
    def is_mp3(self):
        return self.extension == '.mp3'

    @is_mp3.expression
    def is_mp3(self):
        return self.Name.like("%.mp3")

    @staticmethod
    @patreon.services.caching.request_cached()
    def get(attachment_id=None):
        if attachment_id is not None:
            return db_session.query(Attachments) \
                .filter_by(ID=attachment_id) \
                .first()
        return None

    @staticmethod
    @patreon.services.caching.request_cached()
    def find_by_activity_id(activity_id):
        return db_session.query(Attachments) \
            .filter_by(HID=activity_id) \
            .all()

    @staticmethod
    def find_music(activity_id):
        return Attachments.query\
            .filter_by(HID=activity_id)\
            .filter(Attachments.is_mp3)\
            .all()
