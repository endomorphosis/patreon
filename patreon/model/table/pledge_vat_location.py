from patreon.services import db
from sqlalchemy import BigInteger, Column, DateTime, String


class PledgeVatLocation(db.Model):
    __tablename__ = 'pledge_vat_locations'

    pledge_vat_location_id  = Column(BigInteger, primary_key=True, nullable=False)
    ip_address              = Column(String(45))
    geolocation_country     = Column(String(2))
    selected_country        = Column(String(2))
    created_at              = Column(DateTime, nullable=False)
