# coding: utf-8
from patreon.services import db
from patreon.services.db import db_session
from sqlalchemy import Column, Integer, DateTime, SmallInteger, text, ForeignKey
from sqlalchemy.orm import relationship


class Curation(db.Model):
    __tablename__ = 'tblFeatured'

    HID         = Column(Integer, ForeignKey('activities.activity_id'), primary_key=True, nullable=False)
    Category    = Column(SmallInteger, primary_key=True, nullable=False, server_default=text("'0'"))
    Weight      = Column(Integer, nullable=False)
    Created     = Column(DateTime)

    activity     = relationship('Activity', uselist=True)

    @property
    def post_id(self):
        return self.HID

    @property
    def category(self):
        return self.Category

    @property
    def weight(self):
        return self.Weight

    @property
    def created_at(self):
        return self.Created

    @staticmethod
    def get(curation_id, category):
        if not curation_id or not category:
            return None

        return db_session.query(Curation)\
            .filter_by(HID=curation_id, Category=category)\
            .first()
