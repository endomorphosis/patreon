import datetime

from patreon.services import db
from sqlalchemy import BigInteger, Column, DateTime, ForeignKey, String
from sqlalchemy.ext.associationproxy import association_proxy


class CreditPurchase(db.Model):
    __tablename__ = 'credit_purchase'

    credit_purchase_id      = Column(BigInteger, primary_key=True)
    txn_id                  = Column(BigInteger, ForeignKey('txns.txn_id'))
    amount_cents            = Column(BigInteger, nullable=False)
    payment_instrument_id   = Column(String(128), nullable=False)
    purchasing_user_id      = Column(BigInteger, nullable=False)
    campaign_id             = Column(BigInteger, nullable=False)
    created_at              = Column(DateTime, default=datetime.datetime.utcnow, nullable=False)
    succeeded_at            = association_proxy('txn', 'succeeded_at')
    failed_at               = association_proxy('txn', 'failed_at')

    @property
    def is_successful(self):
        return self.succeeded_at is not None

    @property
    def is_failed(self):
        return self.failed_at is not None
