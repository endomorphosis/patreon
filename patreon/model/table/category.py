# coding: utf-8
from patreon.services import db
from patreon.services.db import db_session
from sqlalchemy import Column, Index, Integer, SmallInteger, ForeignKey


class Category(db.Model):
    __tablename__ = 'tblCategories'
    __table_args__ = (
        Index('tblCategories_Category_UID', 'Category', 'UID'),
    )

    UID         = Column(Integer, ForeignKey('tblUsers.UID'), primary_key=True, nullable=False)
    Category    = Column(SmallInteger, primary_key=True, nullable=False)

    @property
    def user_id(self):
        return self.UID

    @property
    def category_id(self):
        return self.Category

    @staticmethod
    def get(user_id, category_id):
        if user_id is not None and category_id is not None:
            return db_session.query(Category)\
                .filter_by(UID=user_id, Category=category_id)\
                .first()
        return None
