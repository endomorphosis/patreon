import patreon
from patreon.services import db
from sqlalchemy import Integer, Column, text
from sqlalchemy.ext.hybrid import hybrid_property


class UserCampaignSetting(db.Model):
    __tablename__ = 'user_campaign_settings'

    user_id             = Column(Integer, primary_key=True, nullable=False)
    campaign_id         = Column(Integer, primary_key=True, nullable=False)
    email_new_comment   = Column(Integer, nullable=False, server_default=text("'1'")) # TODO: is this used?
    email_post          = Column(Integer, nullable=False, server_default=text("'1'"))
    email_paid_post     = Column(Integer, nullable=False, server_default=text("'1'"))
    push_new_comment    = Column(Integer, nullable=False, server_default=text("'1'"))
    push_post           = Column(Integer, nullable=False, server_default=text("'1'"))
    push_paid_post      = Column(Integer, nullable=False, server_default=text("'1'"))
    push_creator_live   = Column(Integer, nullable=False, server_default=text("'1'"))

    @hybrid_property
    def settings_id(self):
        return '{}-{}-follow-settings'.format(self.user_id, self.campaign_id)

    @staticmethod
    @patreon.services.caching.multiget_cached(object_key=('user_id', 'campaign_id'))
    def get(user_ids, campaign_ids):
        return UserCampaignSetting.query \
            .filter(UserCampaignSetting.user_id.in_(user_ids)) \
            .filter(UserCampaignSetting.campaign_id.in_(campaign_ids)) \
            .all()

    @staticmethod
    def find(user_id, campaign_id):
        if user_id and campaign_id:
            return UserCampaignSetting.query\
                .filter_by(user_id=user_id, campaign_id=campaign_id)\
                .first()
        elif user_id:
            return UserCampaignSetting.query\
                .filter_by(user_id=user_id)\
                .all()
        return None
