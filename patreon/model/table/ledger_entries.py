import datetime
from patreon.services import db
from sqlalchemy import BigInteger, Column, DateTime, Integer, ForeignKey


class LedgerEntry(db.Model):
    __tablename__ = 'ledger_entries'

    ledger_entry_id     = Column(BigInteger, primary_key=True)
    txn_id              = Column(BigInteger, ForeignKey('txns.txn_id'), nullable=False, index=True)
    account_id          = Column(Integer, nullable=False)
    amount_cents        = Column(Integer, nullable=False)
    created_at          = Column(DateTime, default=datetime.datetime.utcnow, nullable=False)
    squared_at          = Column(DateTime)
    squared_by_txn_id   = Column(BigInteger)
