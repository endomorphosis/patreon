# coding: utf-8
import patreon
from patreon.constants import image_sizes

from patreon.model.table import ABTestAssignments, Activity, CampaignsUsers, \
    Category, Pledge, Reward, RewardsGive, RewardsWait, ShippingAddress, \
    TblUsersSetting, TwoFactorSecrets, UserSetting
from patreon.model.table.column_types import HTMLString
from patreon.model.type import photo_key_type, file_bucket_enum
from patreon.services import db
from sqlalchemy import BigInteger, Column, DateTime, Integer, String, text
from sqlalchemy.ext.associationproxy import association_proxy
from sqlalchemy.ext.hybrid import hybrid_property
from sqlalchemy.orm import relationship


class User(db.Model):
    __tablename__ = 'tblUsers'

    UID             = Column(Integer, primary_key=True)
    Email           = Column(String(128), unique=True)
    FName           = Column(String(64))
    LName           = Column(String(64))
    Password        = Column(String(64))
    Created         = Column(DateTime, nullable=False)
    FBID            = Column(BigInteger, unique=True)
    ImageUrl        = Column(String(128))
    ThumbUrl        = Column(String(128))
    photo_key       = Column(String(128))
    thumbnail_key   = Column(String(128))
    Gender          = Column(Integer, nullable=False, server_default=text("'0'"))
    Status          = Column(Integer, nullable=False, server_default=text("'0'"))
    Vanity          = Column(String(64), index=True)
    About           = Column(HTMLString)
    Youtube         = Column(String(64))
    Twitter         = Column(String(64))
    Facebook        = Column(String(64))
    is_suspended    = Column(Integer, nullable=False, server_default=text("'0'"))
    is_deleted      = Column(Integer, nullable=False, server_default=text("'0'"))
    is_nuked        = Column(Integer, nullable=False, server_default=text("'0'"))
    is_admin        = Column(Integer, nullable=False, server_default=text("'0'"))
    two_factor_enabled = Column(Integer, default=0)

    tbl_user_settings   = relationship(TblUsersSetting, uselist=False, backref="user")
    user_settings       = relationship(UserSetting, uselist=False, backref="user")
    pledges_out         = relationship(Pledge, backref="user", uselist=True, foreign_keys="Pledge.UID")
    pledges_in          = relationship(Pledge, backref="creator", uselist=True, foreign_keys="Pledge.CID")
    campaigns_users     = relationship(CampaignsUsers, backref="user", uselist=True)
    posts               = relationship(Activity, backref="user", uselist=True)
    categories          = relationship(Category, backref="user", uselist=True, lazy='dynamic')
    rewards_give        = relationship(RewardsGive, backref="user", uselist=True, foreign_keys="RewardsGive.UID")
    rewards_given_as_creator = relationship(RewardsGive, backref="creator",
                                            uselist=True, foreign_keys="RewardsGive.CID")
    rewards_wait        = relationship(RewardsWait, backref="user", uselist=True)
    rewards             = relationship(Reward, backref="creator", uselist=True)
    group_id            = relationship(ABTestAssignments, backref="user", uselist=False)
    two_factor_info     = relationship(TwoFactorSecrets, backref="user", uselist=False)
    addresses           = relationship(ShippingAddress, backref="user", uselist=True)

    rewards_wait_ids    = association_proxy('rewards_wait', 'RID')
    campaigns           = association_proxy('campaigns_users', 'campaign')
    creators_pledged_to = association_proxy('pledges_out', 'creator')
    two_factor_secret   = association_proxy('two_factor_info', 'secret')

    @hybrid_property
    def user_id(self):
        return self.UID

    @hybrid_property
    def email(self):
        return self.Email

    @hybrid_property
    def first_name(self):
        return self.FName

    @hybrid_property
    def last_name(self):
        return self.LName

    @hybrid_property
    def gender(self):
        return self.Gender

    @hybrid_property
    def status(self):
        return self.Status

    @hybrid_property
    def vanity(self):
        return self.Vanity

    @hybrid_property
    def about(self):
        return self.About

    @hybrid_property
    def facebook_id(self):
        return self.FBID

    @hybrid_property
    def youtube(self):
        return self.Youtube

    @hybrid_property
    def twitter(self):
        return self.Twitter

    @hybrid_property
    def facebook(self):
        return self.Facebook

    @hybrid_property
    def created_at(self):
        return self.Created

    @staticmethod
    @patreon.services.caching.multiget_cached(object_key='user_id', result_fields='Privacy')
    def is_private_paramaterized(user_ids):
        return TblUsersSetting.query.filter(TblUsersSetting.UID.in_(user_ids)).all()

    @hybrid_property
    def is_private(self):
        return self.is_private_paramaterized(self.user_id)

    @is_private.expression
    def is_private(self):
        return self.tbl_user_settings.Privacy == 1 if self.tbl_user_settings else True

    @staticmethod
    @patreon.services.caching.multiget_cached(object_key='UID')
    def get(user_id=None):
        return User.query.filter(User.UID.in_(user_id)).all()

    @staticmethod
    @patreon.services.caching.request_cached()
    def get_unique(vanity=None, email=None, facebook_id=None):
        if vanity is not None:
            return User.query.filter_by(Vanity=vanity).first()
        elif email is not None:
            return User.query.filter_by(Email=email).first()
        elif facebook_id is not None:
            return User.query.filter_by(FBID=facebook_id).first()
        return None

    def patreon_url(self, get_params=''):
        if self.Vanity is None:
            user_path = 'user?u=' + str(self.UID)
            if get_params:
                user_path += '&'
        else:
            user_path = self.Vanity
            if get_params:
                user_path += '?'

        return '/' + user_path + get_params

    def canonical_url(self, get_params=''):
        domain = 'https://' + patreon.config.main_server

        return domain + self.patreon_url(get_params)

    def messages_url(self):
        return self.canonical_url('msg=1')

    @property
    def full_name(self):
        return " ".join(filter(None, [self.first_name, self.last_name]))

    @property
    def image_url(self):
        return self.ImageUrl \
               or 'https://' + patreon.config.main_server \
                  + patreon.globalvars.get('IMG_PROFILE_DEFAULT_LARGE')

    @property
    def has_default_image(self):
        return self.ImageUrl is None or self.image_url.strip() != ''

    @property
    def thumb_url(self):
        return self.ThumbUrl \
               or 'https://' + patreon.config.main_server \
                  + patreon.globalvars.get('IMG_PROFILE_DEFAULT')

    @property
    def thumbnail_url(self):
        return self.thumbnail_url_size()

    def thumbnail_url_size(self, size=None):
        if not self.thumbnail_key:
            return None

        return "https:" + photo_key_type.get_image_url(
            file_bucket_enum.config_bucket_user,
            self.thumbnail_key,
            size or image_sizes.USER_THUMB_LARGE
        )

    @property
    def photo_url(self):
        return self.photo_url_size()

    def photo_url_size(self, size=None):
        if not self.photo_key:
            return None

        return "https:" + photo_key_type.get_image_url(
            file_bucket_enum.config_bucket_user,
            self.photo_key,
            size or image_sizes.USER_LARGE_2
        )

    @property
    def is_creator(self):
        return (len(self.campaigns) > 0) and self.campaigns[0].creation_name

    @property
    def is_active_creator(self):
        for campaign in self.campaigns:
            if campaign.published_at:
                return True
        return False

    def is_a_patron_of(self, creator):
        return Pledge.get(self.user_id, creator.user_id) is not None

    @property
    def campaign_id(self):
        if self.campaigns_users:
            return self.campaigns_users[0].campaign_id
        return None

    @property
    def email_patreon(self):
        return self.user_settings.email_patreon

    @property
    def should_email_on_new_patron(self):
        if self.campaigns_users:
            return self.campaigns_users[0].should_email_on_new_patron
        return 0

    @property
    def should_email_on_patron_post(self):
        if self.campaigns_users:
            return self.campaigns_users[0].should_email_on_patron_post
        return 0

    @property
    def email_new_comment(self):
        return self.user_settings.email_new_comment

    @property
    def email_goal(self):
        return self.user_settings.email_goal
