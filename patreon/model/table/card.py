from sqlalchemy import Column, Date, Integer, DateTime, SmallInteger, String, \
    text
import patreon
from patreon.services import db
from patreon.model.type import Address
import patreon.constants.cards as card_types


class Card(db.Model):
    __tablename__ = 'tblCardInfo'

    RID             = Column(String(32), primary_key=True, nullable=False)
    UID             = Column(Integer, primary_key=True, nullable=False, index=True)
    Name            = Column(String(255))
    Street          = Column(String(128))
    Street2         = Column(String(128))
    City            = Column(String(64))
    State           = Column(String(32))
    Zip             = Column(String(32))
    ExpirationDate  = Column(Date)
    Number          = Column(SmallInteger)
    Type            = Column(Integer)
    Created         = Column(DateTime)
    Country         = Column(String(32))
    Verified        = Column(Integer, nullable=False, server_default=text("'0'"))

    @property
    def card_id(self):
        return self.RID

    @property
    def user_id(self):
        return self.UID

    @property
    def name(self):
        return self.Name

    @property
    def address(self):
        return Address(
            self.Street,
            self.Street2,
            self.Zip,
            self.City,
            self.State,
            self.Country
        )

    @property
    def expiration_date(self):
        return self.ExpirationDate

    @property
    def number(self):
        return self.Number

    @property
    def type(self):
        return self.Type

    @property
    def is_paypal(self):
        return self.type == card_types.PAYPAL \
               or self.RID.startswith(card_types.PAYPAL_PREFIX)

    @property
    def is_stripe(self):
        return self.type != card_types.PAYPAL \
               and self.RID.startswith(card_types.STRIPE_PREFIX)

    @property
    def is_verified(self):
        return self.Verified

    @property
    def created_at(self):
        return self.Created

    @staticmethod
    @patreon.services.caching.request_cached()
    def get(card_id):
        return Card.query.filter_by(RID=card_id).first()

    @property
    def type_string(self):
        if self.is_paypal:
            return 'paypal'

        return 'stripe'
