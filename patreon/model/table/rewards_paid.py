# coding: utf-8
from patreon.services import db
from sqlalchemy import Column, DateTime, Integer, String, ForeignKey, SmallInteger
from sqlalchemy.ext.hybrid import hybrid_property


class RewardsPaid(db.Model):
    __tablename__ = 'tblRewardsPaid'

    ID              = Column(Integer, ForeignKey('tblRewardsGive.ID'), primary_key=True)
    ChargeID        = Column(String(128), nullable=False, index=True)
    Type            = Column(SmallInteger)
    ChargeDetailsID = Column(Integer, index=True)
    Created         = Column(DateTime, nullable=False)
    Refunded        = Column(DateTime)

    @hybrid_property
    def created_at(self):
        return self.Created

    @hybrid_property
    def refunded_at(self):
        return self.Refunded
