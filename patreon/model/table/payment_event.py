from patreon.services import db
from sqlalchemy import Column, Integer, DateTime, String
from sqlalchemy.dialects.postgresql import JSON


class PaymentEvent(db.LoggingModel):
    __tablename__ = 'payment_events'

    payment_event_id    = Column(Integer, primary_key=True)
    type                = Column(String(255), nullable=False, index=True)  # TODO make as enum
    subtype             = Column(String(255), nullable=False, index=True)  # TODO make as enum
    json_data           = Column(JSON, nullable=False)
    created_at          = Column(DateTime, nullable=False, index=True)

    @property
    def user_id(self):
        return self.get_data_member('user_id')

    def get_data_member(self, key):
        return self.json_data.get(key)
