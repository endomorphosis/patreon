from patreon.services import db
from sqlalchemy import BigInteger, Column, DateTime, ForeignKey


class TxnHistory(db.Model):
    __tablename__ = 'txn_history'

    txn_history_id          = Column(BigInteger, primary_key=True)
    txn_id                  = Column(BigInteger, ForeignKey('txns.txn_id'), primary_key=True)
    txn_succeeded_at        = Column(DateTime)
    txn_pending_at          = Column()
    txn_failed_at           = Column(DateTime)
    txn_began_processing_at = Column(DateTime)
    txn_created_at          = Column(DateTime)
