# coding: utf-8
import patreon
from patreon.model.table import Pledge
from patreon.model.table.column_types import HTMLString
from patreon.services import db
from patreon.services.db import db_session
from patreon.util.html_cleaner import clean_and_remove_links
from sqlalchemy import Column, DateTime, Integer, text, ForeignKey, func
from sqlalchemy.ext.associationproxy import association_proxy
from sqlalchemy.ext.hybrid import hybrid_property
from sqlalchemy.orm import relationship


class Reward(db.Model):
    __tablename__ = 'tblRewards'

    RID         = Column(Integer, primary_key=True)
    UID         = Column(
                    Integer,
                    ForeignKey('campaigns_users.user_id'),
                    ForeignKey('tblUsers.UID'), nullable=False, index=True
                )
    Amount      = Column(Integer, nullable=False, server_default=text("'0'"))
    UserLimit   = Column(Integer)
    Description = Column(HTMLString(clean_and_remove_links))
    Shipping    = Column(Integer, nullable=False, server_default=text("'0'"))
    Created     = Column(DateTime, nullable=False)

    pledges     = relationship(Pledge, backref='reward', uselist=True)
    # backref from campaignsusers
    campaign    = association_proxy('campaigns_users', 'campaign')

    @hybrid_property
    def reward_id(self):
        return self.RID

    @hybrid_property
    def creator_id(self):
        return self.UID

    @hybrid_property
    def amount(self):
        return self.Amount

    @hybrid_property
    def description(self):
        return self.Description

    @hybrid_property
    def requires_shipping(self):
        return self.Shipping == 1

    @hybrid_property
    def created_at(self):
        return self.Created

    @staticmethod
    @patreon.services.caching.multiget_cached(object_key='RID')
    def get(reward_id):
        return Reward.query \
            .filter(Reward.RID.in_(reward_id))

    @staticmethod
    @patreon.services.caching.multiget_cached(object_key='RID', result_fields='count', default_result=0)
    def _count_parameterized(reward_ids):
        return db_session.query(Pledge.RID, func.count(Pledge.RID).label('count')) \
            .filter(Pledge.RID.in_(reward_ids)) \
            .group_by(Pledge.RID)

    @hybrid_property
    def count(self):
        return self._count_parameterized(self.RID)

    @count.expression
    def count(self):
        return Reward.query\
            .join(Reward.pledges)\
            .filter_by(RID=self.RID)\
            .count()

    @property
    def user_limit(self):
        return self.UserLimit or 0

    def remaining(self):
        if self.user_limit:
            remaining = self.user_limit - self.count
            return max(remaining, 0)
        else:
            return None

    @property
    def url(self):
        return "/bePatron?rid={0}&patAmt={1}&u={2}".format(
            self.reward_id,
            self.amount / 100,
            self.creator_id
        )

    @property
    def confirm_url(self):
        return "/bePatronConfirm?selectReward={0}&amt={1}&u={2}".format(
            self.reward_id,
            self.amount / 100,
            self.creator_id
        )
