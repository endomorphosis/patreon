from patreon.model.table import TaxForm
from patreon.model.table.column_types.encrypted_data import EncryptedData
from sqlalchemy import Column, String


class W9TaxForm(TaxForm):
    BusinessName    = Column(String(256), nullable=False)
    State           = Column(EncryptedData, nullable=False)
    Zip             = Column(EncryptedData, nullable=False)
    TaxClass        = Column(String(32), nullable=False)

    @property
    def state(self):
        return self.State

    @property
    def tax_class(self):
        return self.TaxClass
