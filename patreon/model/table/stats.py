# coding: utf-8
from patreon.services import db
from sqlalchemy import Column, Integer, Numeric, String, DateTime


class Stats(db.Model):
    __tablename__ = 'stats_averages'

    ID      = Column(Integer, primary_key=True, nullable=False)
    Date    = Column(DateTime, primary_key=True, nullable=False, index=True)
    Value   = Column(Numeric(), nullable=False)
    Key     = Column(String(64), nullable=False, index=True)

    @property
    def id(self):
        return self.ID

    @property
    def date(self):
        return self.Date

    @property
    def value(self):
        return self.Value

    @property
    def key(self):
        return self.Key
