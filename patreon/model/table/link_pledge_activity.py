from patreon.services import db
from sqlalchemy import Column, Integer


class LinkPledgeActivity(db.Model):
    __tablename__ = 'tblLinkPledgeShare'

    HID = Column(Integer, primary_key=True, nullable=False)
    UID = Column(Integer, primary_key=True, nullable=False)
    RID = Column(Integer, index=True)

    @property
    def user_id(self):
        return self.UID

    @property
    def activity_id(self):
        return self.HID

    @property
    def reward_id(self):
        return self.RID
