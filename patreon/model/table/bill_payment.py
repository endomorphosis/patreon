from patreon.services import db
from sqlalchemy import DateTime, Column, Integer, BigInteger


class BillPayment(db.Model):
    __tablename__ = 'bill_payments'

    bill_payment_id                 = Column(BigInteger, primary_key=True)
    bill_id                         = Column(BigInteger, primary_key=True)
    transaction_id                  = Column(BigInteger, nullable=False)
    pledge_amount_cents             = Column(Integer, nullable=False)
    fee_amount_cents                = Column(Integer, default=0 ,nullable=False)
    vat_amount_cents                = Column(Integer, default=0, nullable=False)
    external_payment_id             = Column(BigInteger, nullable=False)
    external_payment_amount_cents   = Column(Integer, default=0, nullable=False)
    credit_amt                      = Column(Integer, default=0, nullable=False)
    created_at                      = Column(DateTime, nullable=False)
    attempted_at                    = Column(DateTime)
    failed_at                       = Column(DateTime)
    succeeded_at                    = Column(DateTime)
