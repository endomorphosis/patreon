# coding: utf-8
import datetime

from sqlalchemy import Column, DateTime, Integer, String, text, ForeignKey
from sqlalchemy.ext.associationproxy import association_proxy
from sqlalchemy.ext.hybrid import hybrid_property
from sqlalchemy.orm import relationship
import patreon
from patreon.model.table import TblUsersSetting
from patreon.model.type import Address
from patreon.services import db


class Pledge(db.Model):
    __tablename__ = 'tblPledges'

    CID         = Column(Integer, ForeignKey('tblUsers.UID'), primary_key=True, nullable=False)
    UID         = Column(Integer, ForeignKey('tblUsers.UID'), primary_key=True, nullable=False, index=True)
    Pledge      = Column(Integer, nullable=False, index=True)
    MaxAmount   = Column(Integer)
    RID         = Column(Integer, ForeignKey('tblRewards.RID'), index=True)
    address_id  = Column(Integer, ForeignKey('shipping_addresses.address_id'))
    CardID      = Column(String(32), nullable=False, server_default=text("''"))
    Street      = Column(String(128))
    Street2     = Column(String(128))
    City        = Column(String(64))
    State       = Column(String(32))
    Zip         = Column(String(32))
    Country     = Column(String(32))
    EmailPost   = Column(Integer, nullable=False, server_default=text("'1'"))
    EmailPaid   = Column(Integer, nullable=False, server_default=text("'1'"))
    Created     = Column(DateTime, nullable=False, index=True)
    LastUpdated = Column(DateTime)
    Invalid     = Column(DateTime, index=True)
    # PatronPaysFees indicates that the patron pays the fees, rather
    # than having the creator pay them (default, legacy option)
    PatronPaysFees = Column(Integer)

    tbl_user_settings   = association_proxy('user', 'tbl_user_settings')
    shipping_address    = relationship('ShippingAddress', uselist=False)

    @hybrid_property
    def patron_id(self):
        return self.UID

    @hybrid_property
    def creator_id(self):
        return self.CID

    @hybrid_property
    def amount(self):
        return self.Pledge

    @hybrid_property
    def pledge_cap(self):
        return self.MaxAmount

    @hybrid_property
    def reward_id(self):
        return self.RID

    @hybrid_property
    def created_at(self):
        return self.Created

    @hybrid_property
    def updated_at(self):
        return self.LastUpdated

    @hybrid_property
    def invalidated_at(self):
        return self.Invalid

    @hybrid_property
    def card_id(self):
        return self.CardID

    @property
    def pledge_id(self):
        return '{uid}-{cid}'.format(cid=self.creator_id, uid=self.patron_id)

    @property
    def has_address(self):
        return self.Street or self.Street2 or self.Zip or self.City or self.State or self.Country

    @property
    def address(self):
        return Address(
            self.Street,
            self.Street2,
            self.Zip,
            self.City,
            self.State,
            self.Country
        )

    @hybrid_property
    def notify_all_activity(self):
        return self.EmailPost

    @hybrid_property
    def notify_paid_creation(self):
        return self.EmailPaid

    @staticmethod
    @patreon.services.caching.multiget_cached(object_key=('UID', 'CID'))
    def get(patron_id, creator_id):
        return Pledge.query\
            .filter(Pledge.UID.in_(patron_id))\
            .filter(Pledge.CID.in_(creator_id))\
            .all()

    @hybrid_property
    def is_private(self):
        return self.tbl_user_settings.has(TblUsersSetting.Privacy == 1)

    @property
    def days_supported(self):
        return (datetime.datetime.now() - self.Created).days

    @property
    def edit_url(self):
        return "/bePatronConfirm?edit=1&u=" + str(self.creator_id)
