# coding: utf-8
from datetime import timedelta

import patreon
from patreon.model.table.column_types import UTFMultibyteString
from patreon.services import db
from patreon.services.db import db_session
from patreon.util import format
from sqlalchemy import Column, DateTime, Index, Integer, ForeignKey, text, desc
from sqlalchemy.ext.hybrid import hybrid_property, hybrid_method
from sqlalchemy.orm import relationship, joinedload, backref, aliased


class Comment(db.Model):
    __tablename__ = 'tblComments'
    __table_args__ = (
        Index('tblComments_HID_Created', 'HID', 'Created'),
    )

    CID         = Column(Integer, primary_key=True)
    UID         = Column(Integer, ForeignKey('tblUsers.UID'), nullable=False, index=True)
    HID         = Column(Integer, ForeignKey('activities.activity_id'), nullable=False)
    Created     = Column(DateTime, nullable=False)
    Comment     = Column(UTFMultibyteString, nullable=False)
    ThreadID    = Column(Integer, ForeignKey('tblComments.CID'))
    DeletedAt   = Column(DateTime)
    EditedAt    = Column(DateTime)
    vote_sum    = Column(Integer, default=0)
    is_spam     = Column(Integer, nullable=False, server_default=text("'0'"))

    post        = relationship('Activity', uselist=False)
    commenter   = relationship('User', uselist=False)
    children    = relationship('Comment', backref=backref("parent", remote_side=[CID]), uselist=True)

    @hybrid_property
    def comment_id(self):
        return self.CID

    @hybrid_property
    def commenter_id(self):
        return self.UID

    @hybrid_property
    def post_id(self):
        return self.HID

    @hybrid_property
    def comment_text(self):
        return self.Comment

    @hybrid_property
    def thread_id(self):
        return self.ThreadID

    @hybrid_property
    def created_at(self):
        return self.Created

    @hybrid_property
    def deleted_at(self):
        return self.DeletedAt

    @hybrid_property
    def edited_at(self):
        return self.EditedAt

    @property
    def patreon_url(self):
        return '/posts/{}?cid={}'.format(self.post_id, self.comment_id)

    def canonical_url(self, get_params=''):
        domain = 'https://' + patreon.config.main_server
        params = '&' + get_params if get_params else ''
        return domain + self.patreon_url + params

    @staticmethod
    @patreon.services.caching.multiget_cached(object_key='CID')
    def get(comment_id):
        return Comment.query.filter(Comment.CID.in_(comment_id)).all()

    @property
    def has_children(self):
        return db_session.query(Comment).filter_by(ThreadID=self.CID).first() is not None

    @staticmethod
    def find_by_user_id(user_id):
        return db_session.query(Comment).filter_by(UID=user_id).all()

    @staticmethod
    @patreon.services.caching.multiget_cached(object_key='HID', default_result=list)
    def find_by_activity_id(activity_id):
        return db_session.query(Comment).filter(Comment.HID.in_(activity_id)).filter_by(is_spam=0).all()

    @staticmethod
    def find_thread_heads_by_activity_id(activity_id):
        return db_session.query(Comment).filter_by(HID=activity_id, ThreadID=None, is_spam=0).all()

    @staticmethod
    def find_thread_children_by_comment_id(comment_id):
        return db_session.query(Comment).filter_by(ThreadID=comment_id).all()

    @staticmethod
    def get_spam_count():
        return db_session.query(Comment).filter_by(is_spam=1).count()

    @hybrid_method
    def was_posted_by_user_id(self, user_id):
        return self.commenter_id == user_id

    @hybrid_method
    def is_toplevel(self):
        return self.thread_id.is_(None)

    @hybrid_method
    def is_a_reply(self):
        return ~self.is_toplevel()

    def is_by_poster(self):
        return self.commenter_id == self.post.user_id

    def is_by_campaign_owner(self):
        return self.commenter_id in self.post.campaign_user_ids

    def is_by_creator(self):
        return self.commenter.is_creator and (self.is_by_poster() or self.is_by_campaign_owner())

    def is_by_patron(self):
        return any([self.commenter.is_a_patron_of(creator) for creator in self.post.campaign.users])

    ## Date filtering

    @hybrid_method
    def was_posted_on_or_before_date(self, date, tz):
        end_of_day_timestamp = patreon.util.datetime.datetime_at_midnight_on_date(date + timedelta(1), tz)
        return self.created_at < end_of_day_timestamp

    @hybrid_method
    def was_posted_on_or_after_date(self, date, tz):
        start_of_day_timestamp = patreon.util.datetime.datetime_at_midnight_on_date(date, tz)
        return self.created_at >= start_of_day_timestamp

    @hybrid_method
    def was_posted_on_date(self, date, tz):
        # as a Venn diagram, this logic feels silly,
        # but I think it really is the most straightforward way to write the condition
        return self.was_posted_on_or_after_date(date, tz) & self.was_posted_on_or_before_date(date, tz)

    @property
    def php_date(self):
        return format.php_date(self.created_at)

    ## Parent post filtering

    @staticmethod
    def get_commenter_ids_by_activity_ids(activity_ids):
        if not activity_ids:
            return []
        result = db_session.query(Comment.UID).filter(Comment.HID.in_(activity_ids)).all()
        return [element[0] for element in result]

    @staticmethod
    def is_on_a_post_by_user_id(user_id):
        # This is made redundant by filter_on_posts_by_user_id
        # which is more performant.
        # I've left this in because I prefer the subquery style
        # over the join style. ~jesse
        # TODO: make this a hybrid_method
        return (db_session.query(patreon.model.table.Activity).correlate(Comment)
                .filter(patreon.model.table.Activity.activity_id == Comment.post_id)
                .filter(patreon.model.table.Activity.user_id == user_id)
                ).exists()

    @staticmethod
    def is_on_a_post_in_a_campaign_by_creator_id(creator_id):
        # I wrote this for notifications API but turns out I don't need it.
        # It was a pain to write though, and it works ~jesse

        # TODO: make this a hybrid_method
        # TODO: push this logic into Activity
        return (db_session.query(patreon.model.table.Activity).correlate(Comment)
                .join(patreon.model.table.Campaign)
                .join(patreon.model.table.CampaignsUsers)
                .join(patreon.model.table.User)
                .filter(patreon.model.table.Activity.activity_id == Comment.post_id)
                .filter(patreon.model.table.User.user_id == creator_id)
                ).exists()

    @staticmethod
    def is_on_a_creator_post_by_creator_id(creator_id):
        # TODO: make this a hybrid_method
        return (
            Comment.is_on_a_post_by_user_id(creator_id) &
            Comment.is_on_a_post_in_a_campaign_by_creator_id(creator_id)
        )

    @staticmethod
    def commenter_is_patron_of_user(user_id):
        # TODO: make this a hybrid_method
        # TODO: push this logic into Pledge
        return (db_session.query(patreon.model.table.Pledge)
                .filter(patreon.model.table.Pledge.creator_id == user_id)
                .filter(patreon.model.table.Pledge.patron_id == Comment.commenter_id)
                ).exists()

    @staticmethod
    def user_is_patron_of_commenter(user_id):
        # TODO: make this a hybrid_method
        # TODO: push this logic into Pledge
        return (db_session.query(patreon.model.table.Pledge)
                .filter(patreon.model.table.Pledge.patron_id == user_id)
                .filter(patreon.model.table.Pledge.creator_id == Comment.commenter_id)
                ).exists()

    @staticmethod
    def on_posts_by_user_id_from_their_patrons(user_id):
        return (
            Comment.is_on_a_post_by_user_id(user_id)
            & Comment.is_toplevel()
            & ~Comment.was_posted_by_user_id(user_id)
            & Comment.commenter_is_patron_of_user(user_id)
            & ~Comment.user_is_patron_of_commenter(user_id)
        )

    @staticmethod
    def on_posts_by_user_id_from_their_creators(user_id):
        return (
            Comment.is_on_a_post_by_user_id(user_id)
            & Comment.is_toplevel()
            & ~Comment.was_posted_by_user_id(user_id)
            & Comment.user_is_patron_of_commenter(user_id)
        )

    @staticmethod
    def on_posts_by_user_id_from_nonpatrons(user_id):
        return (
            Comment.is_on_a_post_by_user_id(user_id)
            & Comment.is_toplevel()
            & ~Comment.was_posted_by_user_id(user_id)
            & ~Comment.commenter_is_patron_of_user(user_id)
            & ~Comment.user_is_patron_of_commenter(user_id)
        )

    @staticmethod
    def find_dates_with_comments_on_posts_by_user_id_matching_filter(user_id, conditions, limit=10, timezone_=None):
        created_date = patreon.util.datetime.timezone_aware_sql_date_func(Comment.created_at, timezone_=timezone_)
        query = db_session.query(created_date)
        query = Comment.filter_on_posts_by_user_id(query, user_id)
        query = query.filter(conditions)
        query = query.order_by(desc(Comment.created_at))
        query = query.group_by(created_date).limit(limit)
        rows = query.all()

        return [
            row[0]
            for row in rows
            if row[0] is not None
        ]

    @staticmethod
    def find_on_posts_by_user_id_with_filter(user_id, conditions, limit=None):
        query = db_session.query(Comment)
        query = Comment.filter_on_posts_by_user_id(query, user_id)
        query = query.filter(conditions)
        query = query.options(joinedload('parent'), joinedload('children'))
        query = query.order_by(desc(Comment.created_at))
        if limit:
            query = query.limit(limit)
        return query.all()

    @staticmethod
    def count_on_posts_by_user_id_with_filter(user_id, conditions):
        query = db_session.query(Comment)
        query = Comment.filter_on_posts_by_user_id(query, user_id)
        query = query.filter(conditions)
        return query.count()

    @staticmethod
    def filter_on_posts_by_user_id(query, user_id):
        return query \
            .join(Comment.post) \
            .filter(patreon.model.table.Activity.user_id == user_id)

    ## Parent comment filtering

    @staticmethod
    def on_comments_by_user_id_from_their_patrons(user_id):
        return (
            Comment.is_a_reply_to_a_comment_by_user_id(user_id)
            & ~Comment.was_posted_by_user_id(user_id)
            & Comment.commenter_is_patron_of_user(user_id)
            & ~Comment.user_is_patron_of_commenter(user_id)
        )

    @staticmethod
    def on_comments_by_user_id_from_their_creators(user_id):
        return (
            Comment.is_a_reply_to_a_comment_by_user_id(user_id)
            & ~Comment.was_posted_by_user_id(user_id)
            & Comment.user_is_patron_of_commenter(user_id)
        )

    @staticmethod
    def on_comments_by_user_id_from_nonpatrons(user_id):
        return (
            Comment.is_a_reply_to_a_comment_by_user_id(user_id)
            & ~Comment.was_posted_by_user_id(user_id)
            & ~Comment.commenter_is_patron_of_user(user_id)
            & ~Comment.user_is_patron_of_commenter(user_id)
        )

    @staticmethod
    def find_dates_with_replies_to_comments_by_user_id_matching_filter(user_id, conditions, limit=10, timezone_=None):
        created_date = patreon.util.datetime.timezone_aware_sql_date_func(Comment.created_at, timezone_=timezone_)
        query = db_session.query(created_date)
        query = Comment.filter_on_comments_by_user_id(query, user_id)
        query = query.filter(conditions)
        query = query.order_by(desc(Comment.created_at))
        query = query.group_by(created_date).limit(limit)
        rows = query.all()

        return [
            row[0]
            for row in rows
            if row[0] is not None
        ]

    @staticmethod
    def find_on_comments_by_user_id_with_filter(user_id, conditions, limit=None):
        query = db_session.query(Comment)
        query = Comment.filter_on_comments_by_user_id(query, user_id)
        query = query.filter(conditions)
        query = query.options(joinedload('parent'), joinedload('children'))
        query = query.order_by(desc(Comment.created_at))
        if limit:
            query = query.limit(limit)
        return query.all()

    @staticmethod
    def count_on_comments_by_user_id_with_filter(user_id, conditions):
        query = db_session.query(Comment)
        query = Comment.filter_on_comments_by_user_id(query, user_id)
        query = query.filter(conditions)
        return query.count()

    @staticmethod
    def filter_on_comments_by_user_id(query, user_id):
        comment_aliased = aliased(Comment)
        return query \
            .join(comment_aliased, Comment.parent) \
            .filter(comment_aliased.commenter_id == user_id)

    @hybrid_method
    def is_a_reply_to_a_comment_by_user_id(self, user_id):
        return self.is_a_reply() & self.parent.commenter_id == user_id

    @is_a_reply_to_a_comment_by_user_id.expression
    def is_a_reply_to_a_comment_by_user_id(self, user_id):
        comment_aliased = aliased(Comment)
        return Comment.query \
            .join(comment_aliased, Comment.parent) \
            .filter(comment_aliased.commenter_id == user_id) \
            .exists()

    def replies_from_user(self, user):
        return [child for child in self.children if child.commenter == user]
