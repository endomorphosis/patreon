import datetime

from patreon.services import db
from sqlalchemy import Column, String, Integer, DateTime, Text, text


class Session(db.Model):
    __tablename__ = 'sessions_new'

    session_token           = Column(String(128), primary_key=True)
    user_id                 = Column(Integer, index=True)
    csrf_token              = Column(String(64))
    csrf_token_expires_at   = Column(DateTime)
    is_admin                = Column(Integer, server_default=text("'0'"))
    extra_data_json         = Column(Text)
    created_at              = Column(DateTime)
    expires_at              = Column(DateTime, index=True)

    @staticmethod
    def get(session_id):
        return Session.query.filter_by(session_token=session_id) \
            .filter(Session.expires_at > datetime.datetime.now())\
            .first()
