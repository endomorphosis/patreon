# coding: utf-8
from patreon.services import db
from sqlalchemy import Column, DateTime, Integer, ForeignKey
from sqlalchemy.ext.hybrid import hybrid_property
from sqlalchemy.orm import relationship


class NewPledge(db.Model):
    __tablename__ = 'pledges'

    pledge_id               = Column(Integer, primary_key=True, nullable=False)
    user_id                 = Column(Integer, ForeignKey('tblUsers.UID'), nullable=False, index=True)
    campaign_id             = Column(Integer, ForeignKey('campaigns.campaign_id'), nullable=False, index=True)
    amount_cents            = Column(Integer, nullable=False)
    pledge_cap_amount_cents = Column(Integer)
    payment_instrument_id   = Column(Integer)  # not implemented
    reward_tier_id          = Column(Integer, ForeignKey('tblRewards.RID'))
    mailing_address_id      = Column(Integer, ForeignKey('shipping_addresses.address_id'))
    pledge_vat_location_id  = Column(Integer, ForeignKey('pledge_vat_locations.pledge_vat_location_id'))
    created_at              = Column(DateTime, nullable=False)
    deleted_at              = Column(DateTime)
    # PatronPaysFees indicates that the patron pays the fees, rather
    # than having the creator pay them (default, legacy option)
    patron_pays_fees        = Column(Integer)

    campaign                = relationship('Campaign', uselist=False)
    pledge_vat_location     = relationship('PledgeVatLocation', backref='pledge')
    shipping_address        = relationship('ShippingAddress', uselist=False)

    @hybrid_property
    def is_deletion(self):
        return self.deleted_at != None

    def modified_at(self):
        if self.deleted_at is None:
            return self.created_at
        return max(self.created_at, self.deleted_at)

    def __hash__( self ):
        return id( self ) if self.pledge_id is None else hash( self.pledge_id )
