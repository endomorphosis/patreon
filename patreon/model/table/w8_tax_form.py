from patreon.model.table import TaxForm
from patreon.model.table.column_types.encrypted_data import EncryptedData
from sqlalchemy import Column, String


class W8TaxForm(TaxForm):
    TaxID           = Column(EncryptedData, nullable=False)
    CountryCitizen  = Column(String(256), nullable=False)
    Reference       = Column(String(64))
    TreatyArticle   = Column(String(32))
    TreatyPercent   = Column(String(32))
    TreatyIncome    = Column(String(32))
    TreatyReason    = Column(String(32))
    ActingCapacity  = Column(String(256))
