from sqlalchemy import DateTime, Column, Integer, BigInteger, ForeignKey
from patreon.services import db
from patreon.model.table.column_types import EnumType
from patreon.model.type import BillType


class Bills(db.Model):
    __tablename__ = 'bills'

    bill_id         = Column(BigInteger, primary_key=True, autoincrement=True)
    user_id         = Column(BigInteger, ForeignKey('tblUsers.UID'), nullable=False)
    campaign_id     = Column(BigInteger, ForeignKey('campaigns.campaign_id'), nullable=False)
    pledge_id       = Column(BigInteger, ForeignKey('pledges.pledge_id'), nullable=False) # Dependency on new_pledge table.
    vat_charge_id   = Column(BigInteger)
    amount_cents    = Column(Integer, ForeignKey('pledges.amount_cents'), nullable=False)
    type            = Column(EnumType("Bills.type", BillType), nullable=False) #Friendly description of what's going on. Required!
    post_id         = Column(BigInteger)
    billing_cycle   = Column(DateTime)
    created_at      = Column(DateTime, nullable=False)
    attempted_at    = Column(DateTime)
    failed_at       = Column(DateTime)
    succeeded_at    = Column(DateTime)
    deleted_at      = Column(DateTime)
