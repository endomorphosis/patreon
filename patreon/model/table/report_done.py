from patreon.services import db
from sqlalchemy import Integer, Column


class ReportDone(db.Model):
    __tablename__ = 'tblReportDone'

    UID = Column(Integer, primary_key=True)

    @staticmethod
    def get(user_id):
        if user_id:
            return ReportDone.query\
                .filter_by(UID=user_id)\
                .first()
        return None
