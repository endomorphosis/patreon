from patreon.constants import TxnTypes
from patreon.model.table.column_types import EnumType
from patreon.model.table.credit_adjustment import CreditAdjustment
from patreon.model.table import CreditPurchase, ExternalPayment, LedgerEntry, TxnHistory
from patreon.services import db
from patreon.util import datetime
from sqlalchemy import BigInteger, Column, DateTime
from sqlalchemy.ext.associationproxy import association_proxy
from sqlalchemy.orm import relationship


class Txn(db.Model):
    __tablename__ = 'txns'

    txn_id              = Column(BigInteger, primary_key=True)
    type                = Column(EnumType('type', TxnTypes))
    created_at          = Column(DateTime, default=datetime.datetime.utcnow, nullable=False)
    succeeded_at        = Column(DateTime)
    failed_at           = Column(DateTime)
    pending_at          = Column(DateTime)
    began_processing_at = Column(DateTime)

    ledger_entries      = relationship(LedgerEntry, uselist=True, backref="txn")
    external_payments   = relationship(ExternalPayment, backref="txn", uselist=True,
                                       order_by=ExternalPayment.created_at.desc())
    history_events      = relationship(TxnHistory, backref="txn", uselist=True)

    @property
    def is_successful(self):
        return self.succeeded_at is not None

    @property
    def is_failed(self):
        return self.failed_at is not None

    @property
    def is_pending(self):
        return self.pending_at is not None

    @property
    def is_processing(self):
        return self.began_processing_at is not None

    @property
    def most_recent_external_payment(self):
        if not self.external_payments:
            return None

        return self.external_payments[0]


class SubtypedTxn(Txn):
    __txn_type__ = None

    @classmethod
    def __get(cls, txn_id):
        return Txn.query.filter_by(txn_id=txn_id, type=cls.__txn_type__)


class CreditPurchaseTxn(SubtypedTxn):
    __txn_type__ = TxnTypes.credit_purchase

    credit_purchase = relationship(CreditPurchase, uselist=False, backref="txn")
    user_id = association_proxy('credit_purchase', 'purchasing_user_id')
    campaign_id = association_proxy('credit_purchase', 'campaign_id')
    payment_instrument_id = association_proxy('credit_purchase', 'payment_instrument_id')
    amount_cents = association_proxy('credit_purchase', 'amount_cents')


class CreditAdjustmentTxn(SubtypedTxn):
    __txn_type__ = TxnTypes.credit_adjustment
    credit_adjustment = relationship(CreditAdjustment, uselist=False, backref="txn")
    user_id = association_proxy('credit_adjustment', 'adjuster_user_id')
    amount_cents = association_proxy('credit_adjustment', 'amount_cents')
