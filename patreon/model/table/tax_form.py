import datetime

from patreon.model.table.column_types.encrypted_data import EncryptedData
from patreon.services import db
from sqlalchemy import Column, Integer, String, text, DateTime


class TaxForm(db.Model):
    __tablename__ = 'taxForms'

    UID = Column(Integer, primary_key=True)

    FullName        = Column(String(256))
    BirthDate       = Column(EncryptedData)
    Address         = Column(EncryptedData)
    AddressLocation = Column(EncryptedData)
    USTaxID         = Column(EncryptedData)
    ElectronicForms = Column(Integer)
    Signature       = Column(String(256))
    Country         = Column(String(256))
    Type            = Column(Integer, nullable=False, server_default=text("'0'"))
    Created         = Column(DateTime, default=datetime.datetime.now)

    @property
    def is_w8(self):
        return self.Type == 6

    @property
    def is_w9(self):
        return self.Type == 7

    @property
    def full_name(self):
        return self.FullName
