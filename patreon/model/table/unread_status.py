# coding: utf-8
import patreon
from patreon.services import db
from sqlalchemy import Column, DateTime, Index, Integer, ForeignKey, String
from sqlalchemy.ext.hybrid import hybrid_property
from sqlalchemy.orm import relationship


class UnreadStatus(db.Model):
    __tablename__ = 'unread_status'
    __table_args__ = (
        Index('user_id','unread_key'),
    )

    user_id         = Column(Integer, ForeignKey('tblUsers.UID'), primary_key=True, nullable=False, index=True)
    unread_key      = Column(String(255), primary_key=True, nullable=False, index=True)
    read_timestamp  = Column(DateTime, nullable=False)

    user        = relationship('User', uselist=False)

    @hybrid_property
    def unread_status_id(self):
        return '{0}-{1}'.format(self.user_id, self.unread_key)

    @staticmethod
    @patreon.services.caching.multiget_cached(object_key=('user_id', 'unread_key'))
    def get(user_id=None, unread_key=None):
        if user_id is not None and unread_key is not None:
            return UnreadStatus.query.filter(UnreadStatus.user_id.in_(user_id)) \
                .filter(UnreadStatus.unread_key.in_(unread_key)).all()
