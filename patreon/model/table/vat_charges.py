# coding: utf-8
from patreon.services import db
from sqlalchemy import Column, Integer, String, DateTime


class VatCharges(db.Model):
    __tablename__ = 'vat_charges'

    vat_charge_id           = Column(Integer, primary_key=True, nullable=False, autoincrement=True)
    vat_amount_cents        = Column(Integer, nullable=False)
    rate_basis_points       = Column(Integer, nullable=False)
    rate_type               = Column(String(32), nullable=False)
    vat_rules_version_id    = Column(Integer, nullable=False)
    vat_product_category    = Column(Integer)
    vat_remittance_batch_id = Column(Integer)
    rewards_give_id         = Column(Integer)
    txn_id                  = Column(Integer)
    created_at              = Column(DateTime, nullable=False)
