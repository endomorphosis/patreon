# coding: utf-8
import patreon
from patreon.model.table import RewardsPaid
from patreon.model.table.column_types import HTMLString
from patreon.model.type import Address
from patreon.services import db
from patreon.services.db import db_session
from sqlalchemy import Column, DateTime, Index, Integer, String, text, ForeignKey
from sqlalchemy.ext.hybrid import hybrid_property
from sqlalchemy.orm import relationship

REWARD_DECLINED = patreon.globalvars.get('REWARD_DECLINED')


class RewardsGive(db.Model):
    __tablename__ = 'tblRewardsGive'
    __table_args__ = (
        Index(
            'tblRewardsGive_CID_Status',
            'CID', 'Status'
        ),
    )

    ID              = Column(Integer, primary_key=True)
    UID             = Column(Integer, ForeignKey('tblUsers.UID'), nullable=False, index=True)
    CID             = Column(Integer, ForeignKey('tblUsers.UID'), nullable=False, index=True)
    HID             = Column(Integer, ForeignKey('activities.activity_id'), nullable=False, index=True)
    RID             = Column(Integer, nullable=False)
    address_id      = Column(Integer, ForeignKey('shipping_addresses.address_id'))
    RDescription    = Column(HTMLString)
    Status          = Column(Integer, nullable=False, index=True)
    Created         = Column(DateTime)
    Pledge          = Column(Integer)
    CardID          = Column(String(32))
    Street          = Column(String(128))
    Street2         = Column(String(128))
    City            = Column(String(64))
    State           = Column(String(32))
    Zip             = Column(String(32))
    Country         = Column(String(32))
    Fee             = Column(Integer, nullable=False, server_default=text("'0'"))
    PatreonFee      = Column(Integer, nullable=False, server_default=text("'0'"))
    PayID           = Column(Integer)
    Complete        = Column(Integer, nullable=False, server_default=text("'0'"))

    RewardsPaid         = relationship(RewardsPaid, backref='RewardsGive', uselist=False)
    shipping_address    = relationship('ShippingAddress', uselist=False)

    @hybrid_property
    def bill_id(self):
        return self.ID

    @hybrid_property
    def patron_id(self):
        return self.UID

    @hybrid_property
    def creator_id(self):
        return self.CID

    @hybrid_property
    def activity_id(self):
        return self.HID

    @hybrid_property
    def reward_id(self):
        return self.RID

    @hybrid_property
    def reward_description(self):
        return self.RDescription

    @hybrid_property
    def status(self):
        return self.Status

    @hybrid_property
    def amount(self):
        return self.Pledge

    @hybrid_property
    def card_id(self):
        return self.CardID

    @property
    def address(self):
        return Address(
            self.Street,
            self.Street2,
            self.Zip,
            self.City,
            self.State,
            self.Country
        )

    @hybrid_property
    def fee(self):
        return self.Fee

    @hybrid_property
    def patreon_fee(self):
        return self.PatreonFee

    @hybrid_property
    def pay_id(self):
        return self.PayID

    @hybrid_property
    def is_completed(self):
        return self.Complete

    @property
    def declined(self):
        return self.Status == REWARD_DECLINED

    @hybrid_property
    def created_at(self):
        return self.Created

    def as_dict(self):
        # Overrides Model.as_dict.
        return {
            "bill_id": self.bill_id,
            "patron_id": self.patron_id,
            "creator_id": self.creator_id,
            "activity_id": self.activity_id,
            "reward_id": self.reward_id,
            "reward_description": self.reward_description,
            "status": self.status,
            "amount": self.amount,
            "card_id": self.card_id,
            "address": self.address,
            "fee": self.fee,
            "patreon_fee": self.patreon_fee,
            "pay_id": self.pay_id,
            "is_completed": self.is_completed,
            "created_at": self.created_at,
        }

    @staticmethod
    @patreon.services.caching.request_cached()
    def get(patron_id=None, creator_id=None, id_=None):
        if id_ is not None:
            return RewardsGive.query.filter_by(ID=id_).first()
        if patron_id is not None and creator_id is not None:
            return db_session.query(RewardsGive)\
                .filter_by(CID=creator_id, UID=patron_id)\
                .first()
        elif patron_id is not None:
            return db_session.query(RewardsGive)\
                .filter_by(UID=patron_id)\
                .first()
        elif creator_id is not None:
            return db_session.query(RewardsGive)\
                .filter_by(CID=creator_id)\
                .first()
        return None
