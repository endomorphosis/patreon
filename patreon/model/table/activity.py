# coding: utf-8
import datetime
import html
from slugify import slugify_url
from sqlalchemy import BigInteger, Column, DateTime, Index, Integer, \
    SmallInteger, String, Text, ForeignKey, text
from sqlalchemy.ext.associationproxy import association_proxy
from sqlalchemy.ext.hybrid import hybrid_property
from sqlalchemy.orm import relationship, backref
import patreon
from patreon import globalvars
from patreon.constants import image_sizes
from patreon.model.table.column_types import HTMLString
from patreon.model.type import photo_key_type, file_bucket_enum
from patreon.services import db, file_helper
from patreon.services.db import db_session
from patreon.util import format

SLUG_LIMIT = 16


class Activity(db.Model):
    __tablename__ = 'activities'
    __table_args__ = (
        Index(
            'paid_popularity',
            'min_cents_pledged_to_view', 'like_count', 'comment_count'
        ),
        Index(
            'paid_creation_popularity',
            'is_creation', 'min_cents_pledged_to_view', 'like_count', 'comment_count'
        )
    )

    activity_id                 = Column(BigInteger, primary_key=True)
    user_id                     = Column(
                                    Integer,
                                    ForeignKey('tblUsers.UID'),
                                    nullable=False, index=True
                                )
    campaign_id                 = Column(
                                    Integer,
                                    ForeignKey('campaigns.campaign_id'),
                                    ForeignKey('campaigns_users.campaign_id'),
                                    nullable=False, index=True
                                )
    activity_type               = Column(Integer, nullable=False, index=True)
    post_type                   = Column(String(32), nullable=False, index=True)
    activity_title              = Column(String(512))
    activity_content            = Column(HTMLString)
    category                    = Column(SmallInteger)
    photo_key                   = Column(String(128))
    photo_extension             = Column(String(32))
    thumbnail_key               = Column(String(128))
    file_key                    = Column(String(128))
    file_name                   = Column(String(128))
    comment_count               = Column(SmallInteger, nullable=False, index=True, server_default=text("'0'"))
    min_cents_pledged_to_view   = Column(Integer, nullable=False, index=True, server_default=text("'0'"))
    like_count                  = Column(SmallInteger, nullable=False, index=True, server_default=text("'0'"))
    image_url                   = Column(String(255))
    image_large_url             = Column(String(255))
    image_thumb_url             = Column(String(255))
    image_height                = Column(SmallInteger, nullable=False, server_default=text("'0'"))
    image_width                 = Column(SmallInteger, nullable=False, server_default=text("'0'"))
    link_url                    = Column(String(255))
    link_domain                 = Column(String(128))
    link_subject                = Column(String(255))
    link_description            = Column(Text)
    link_embed                  = Column(String(1024))
    is_paid                     = Column(Integer, nullable=False, server_default=text("'0'"))
    is_creation                 = Column(Integer, nullable=False, server_default=text("'0'"))
    cents_pledged_at_creation   = Column(Integer, nullable=False, server_default=text("'0'"))
    created_at                  = Column(DateTime, nullable=False, index=True)
    published_at                = Column(DateTime)
    edited_at                   = Column(DateTime)
    deleted_at                  = Column(DateTime)

    rewards             = relationship('RewardsGive', backref=backref("post"), uselist=True)
    attachments         = relationship('Attachments', backref=backref("post"), uselist=True)
    campaigns_users     = relationship('CampaignsUsers', uselist=True)
    campaign_user_ids   = association_proxy('campaigns_users', 'user_id')
    campaign_nsfw       = association_proxy('campaign', 'is_nsfw')

    def as_search_dict(self):
        return {
            'objectID': self.algolia_id(),

            # indexed
            'title': html.unescape(self.search_title() or ''),
            'description': html.unescape(self.search_description() or ''),

            # non-indexed
            'patreon_url': self.patreon_url,
            'image_url': self.image_large_url,

            # numerically indexed
            'cents_pledged_at_creation': self.cents_pledged_at_creation,
            'published_at': self.published_at,  # appears as unittime, which is Algolia-recommended

            'campaign': {
                # indexed
                'name': html.unescape(self.campaign.creator_name() or ''),
                'vanity': self.campaign.vanity(),
                'patron_snapshot': patreon.model.manager.pledge_mgr.get_patron_count_by_creator_id(
                    self.creator_id),

                # non-indexed
                'url': self.campaign.canonical_url(),
            }
        }

    @staticmethod
    @patreon.services.caching.multiget_cached(object_key='activity_id')
    def get(activity_id):
        return db_session.query(Activity) \
            .filter(Activity.activity_id.in_(activity_id)) \
            .filter(Activity.deleted_at.is_(None)) \
            .filter(Activity.published_at.isnot(None))

    @staticmethod
    @patreon.services.caching.request_cached()
    def get_activity_count_by_campaign_id(campaign_id):
        return db_session.query(Activity) \
            .filter_by(campaign_id=campaign_id) \
            .filter(Activity.deleted_at.is_(None)) \
            .filter(Activity.published_at.isnot(None)) \
            .count()

    @staticmethod
    @patreon.services.caching.request_cached()
    def find_activities_creations_by_user_id_paginated(user_id, page=1):
        offset = (page - 1) * globalvars.get('SHARES_PER_PAGE')
        limit = globalvars.get('SHARES_PER_PAGE')
        return db_session.query(Activity)\
            .filter_by(user_id=user_id) \
            .order_by(Activity.created_at.desc())\
            .offset(offset)\
            .limit(limit)\
            .all()

    @staticmethod
    def get_count_paid_posts_in_last_year_by_campaign_id(campaign_id):
        one_year_ago = datetime.datetime.utcnow() - datetime.timedelta(weeks=52)
        return db_session.query(Activity) \
            .filter(Activity.campaign_id == campaign_id) \
            .filter(Activity.is_paid == 1) \
            .filter(Activity.created_at > one_year_ago) \
            .count()

    @staticmethod
    @patreon.services.caching.request_cached()
    def get_creation_count_by_campaign_id(campaign_id):
        return db_session.query(Activity) \
            .filter_by(campaign_id=campaign_id, is_creation=1) \
            .filter(Activity.deleted_at.is_(None)) \
            .filter(Activity.published_at.isnot(None)) \
            .count()

    @hybrid_property
    def is_searchable(self):
        return (self.min_cents_pledged_to_view == 0) \
               & (self.is_nsfw == 0) \
               & self.was_posted_by_owner \
               & (self.published_at is not None)

    @hybrid_property
    def is_blank_draft(self):
        # '==' are required for this to work. 'is' fails.
        return (self.activity_title == None) \
            & (self.activity_content == None) \
            & (self.file_key == None) \
            & (self.link_url == None) \
            & (self.image_url == None) \
            & (self.published_at == None) \
            & (self.deleted_at == None) \
            & (~self.has_attachments)

    @hybrid_property
    def is_deleted(self):
        return self.deleted_at is not None

    @hybrid_property
    def is_published(self):
        return self.published_at is not None

    @hybrid_property
    def is_public(self):
        return (self.published_at != None) & (self.deleted_at == None)

    @hybrid_property
    def was_posted_by_owner(self):
        return self.user_id in self.campaign_user_ids

    @was_posted_by_owner.expression
    def was_posted_by_owner(self):
        return self.campaign_user_ids.contains(self.user_id)

    @hybrid_property
    def has_attachments(self):
        return len(self.attachments) > 0

    @has_attachments.expression
    def has_attachments(self):
        return self.attachments.any()

    @property
    def php_date(self):
        return format.php_date(self.published_at)

    def thumb_url(self):
        """ deprecated """
        url = self.image_thumb_url
        if not url:
            return None
        if url.startswith("//"):
            url = "https:" + url

        return url

    def title(self):
        if self.activity_title:
            return self.activity_title
        if self.link_subject:
            return self.link_subject
        return None

    @property
    def is_monthly(self):
        return self.is_paid and self.activity_content == 'Thank you so much for the support this month!'

    def algolia_id(self):
        return Activity.algolia_id_for_id(self.activity_id)

    @staticmethod
    def algolia_id_for_id(id_):
        return "post_" + str(id_)

    def search_title(self):
        return self.title() or "Untitled"

    @hybrid_property
    def creator_id(self):
        # TODO: kill this with fire
        from patreon.model.manager import campaign_mgr
        return campaign_mgr.try_get_user_id_by_campaign_id_hack(self.campaign_id)

    @creator_id.expression
    def creator_id(self):
        return self.campaign_user_ids[0]

    def search_description(self):
        if self.activity_content:
            return self.activity_content
        if self.link_description:
            return self.link_description
        return ""

    @property
    def patreon_url(self):
        title = self.activity_title or self.link_subject
        slug = slugify_url(title, to_lower=True, max_length=SLUG_LIMIT) + '-' if title else ''
        return "/posts/{slug}{post_id}".format(
            post_id=self.activity_id,
            slug=slug
        )

    @hybrid_property
    def is_nsfw(self):
        return self.campaign_nsfw

    def canonical_url(self, get_params=''):
        domain = 'https://' + patreon.config.main_server
        params = '?' + get_params if get_params else ''
        return domain + self.patreon_url + params

    def pledge_url(self, min_pledge=None):
        return "/bePatron?patAmt={0}&u={1}".format(
            min_pledge or (self.min_cents_pledged_to_view / 100),
            self.creator_id
        )

    @property
    def edit_url(self):
        return self.patreon_url + '/edit'

    @property
    def image_dimensions(self):
        return self.image_width, self.image_height

    @property
    def file_url(self):
        if not self.file_name:
            return None

        return 'https:' + photo_key_type.get_file_url(
            file_bucket_enum.config_bucket_post,
            self.file_key,
            file_helper.get_extension(self.file_name)
        )

    @property
    def thumbnail_url(self):
        return self.thumbnail_url_size()

    @property
    def blurred_thumbnail_url(self):
        if not self.thumbnail_key:
            return None

        return "https:" + photo_key_type.get_image_url(
            file_bucket_enum.config_bucket_post,
            self.thumbnail_key,
            'blurred'
        )

    def thumbnail_url_size(self, size=None):
        if not self.thumbnail_key:
            return None

        return "https:" + photo_key_type.get_image_url(
            file_bucket_enum.config_bucket_post,
            self.thumbnail_key,
            size or image_sizes.POST_THUMB_SMALL_2
        )

    @property
    def photo_url(self):
        return self.photo_url_size()

    def photo_url_size(self, size=None):
        if not self.photo_key:
            return None

        return "https:" + photo_key_type.get_image_url(
            file_bucket_enum.config_bucket_post,
            self.photo_key,
            size or image_sizes.POST_LARGE_2,
            self.photo_extension
        )
