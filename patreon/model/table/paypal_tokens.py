from patreon.model.table.column_types.json_string import JSONString
from patreon.services import db
from sqlalchemy import Column, Integer, String, DateTime


class PaypalToken(db.Model):
    __tablename__ = 'paypal_tokens'

    user_id         = Column(Integer, primary_key=True)
    paypal_token    = Column(String(255), nullable=False)
    post_info_json  = Column(JSONString, nullable=False)
    created_at      = Column(DateTime)
