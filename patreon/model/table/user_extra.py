# coding: utf-8
from patreon.services import db
from patreon.services.db import db_session
from patreon.model.type import Address
from sqlalchemy import Column, DateTime, Integer, String, text, SmallInteger
from sqlalchemy.ext.hybrid import hybrid_property


class UserExtra(db.Model):
    __tablename__ = 'tblUsersExtra'

    UID                 = Column(Integer, primary_key=True)
    LastNewsUpdate      = Column(DateTime, nullable=False, server_default=text("'2013-03-05 00:00:00'"))
    NewMsgs             = Column(Integer, nullable=False, server_default=text("'0'"))
    PaypalEmail         = Column(String(128))
    PayStreet           = Column(String(128))
    PayStreet2          = Column(String(128))
    PayCity             = Column(String(64))
    PayState            = Column(String(32))
    PayZip              = Column(String(32))
    PayCountry          = Column(String(32))
    FlagYoutube         = Column(Integer, nullable=False, server_default=text("'0'"))
    StripeRecipientID   = Column(String(128))
    IsUS                = Column(Integer, nullable=False, server_default=text("'0'"))
    StripeLastFour      = Column(SmallInteger)
    StripeName          = Column(String(128))
    IsCorp              = Column(Integer, nullable=False, server_default=text("'0'"))
    AutoPay             = Column(Integer, nullable=False, server_default=text("'0'"))
    PayMethod           = Column(Integer, nullable=False, server_default=text("'0'"))
    PayoneerSetup       = Column(Integer, nullable=False, server_default=text("'0'"))

    @hybrid_property
    def user_id(self):
        return self.UID

    @property
    def news_last_updated(self):
        return self.LastNewsUpdate

    @property
    def new_message_count(self):
        return self.NewMsgs

    @property
    def paypal_email(self):
        return self.PaypalEmail

    @property
    def address(self):
        return Address(
            self.PayStreet,
            self.PayStreet2,
            self.PayZip,
            self.PayCity,
            self.PayState,
            self.PayCountry
        )

    @property
    def is_youtube_flagged(self):
        return self.FlagYoutube

    @property
    def stripe_recipient_id(self):
        return self.StripeRecipientID

    @property
    def is_us(self):
        return self.IsUS

    @property
    def stripe_last_four(self):
        return self.StripeLastFour

    @property
    def stripe_name(self):
        return self.StripeName

    @property
    def is_corporation(self):
        return self.IsCorp

    @hybrid_property
    def is_auto_pay(self):
        return self.AutoPay

    @property
    def payment_method(self):
        return self.PayMethod

    @property
    def is_payoneer_setup(self):
        return self.PayoneerSetup

    @staticmethod
    def get(user_id=None):
        if user_id is not None:
            return db_session.query(UserExtra).filter_by(UID=user_id).first()
        return None
