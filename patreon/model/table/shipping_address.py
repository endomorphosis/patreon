from patreon.model.type import Address
from patreon.services import db
from sqlalchemy import Column, Integer, String, DateTime, ForeignKey


class ShippingAddress(db.Model):
    __tablename__ = 'shipping_addresses'

    address_id  = Column(Integer, primary_key=True)
    user_id     = Column(Integer, ForeignKey('tblUsers.UID'), nullable=False, index=True)
    name        = Column(String(128))
    addressee   = Column(String(128))
    line_1      = Column(String(128))
    line_2      = Column(String(128))
    postal_code = Column(String(32))
    city        = Column(String(64))
    state       = Column(String(32))
    country     = Column(String(32))
    created_at  = Column(DateTime)
    deleted_at  = Column(DateTime)

    @property
    def street(self):
        return " ".join(filter(None, [self.line_1, self.line_2]))

    @property
    def address(self):
        return Address(
            self.line_1,
            self.line_2,
            self.postal_code,
            self.city,
            self.state,
            self.country
        )
