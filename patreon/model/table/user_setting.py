# coding: utf-8
from patreon.services import db
from patreon.services.db import db_session
from sqlalchemy import Column, Integer, text, ForeignKey


class UserSetting(db.Model):
    __tablename__ = 'user_settings'

    user_id                 = Column(Integer, ForeignKey('tblUsers.UID'), primary_key=True)
    email_new_comment       = Column(Integer, server_default=text("'1'"))
    email_goal              = Column(Integer, server_default=text("'1'"))
    email_patreon           = Column(Integer, server_default=text("'1'"))
    push_new_comment        = Column(Integer, server_default=text("'1'"))
    push_goal               = Column(Integer, server_default=text("'1'"))
    push_patreon            = Column(Integer, server_default=text("'1'"))
    disable_email_post      = Column(Integer, server_default=text("'0'"))
    disable_email_paid_post = Column(Integer, server_default=text("'0'"))

    @staticmethod
    def get(user_id=None):
        if user_id is not None:
            return db_session.query(UserSetting).filter_by(user_id=user_id).first()
        return None
