# coding: utf-8
from patreon.services import db
from patreon.services.db import db_session
from sqlalchemy import Column, Integer, text, String, ForeignKey


class ZohoCampaigns(db.Model):
    __tablename__ = 'zoho_campaigns'

    campaign_id         = Column(Integer, ForeignKey("campaigns.campaign_id"), primary_key=True)
    zoho_id             = Column(String(255))
    is_converted        = Column(Integer, nullable=False, server_default=text("'0'"))

    @staticmethod
    def get(campaign_id=None, zoho_id=None):
        if campaign_id is not None:
            return db_session.query(ZohoCampaigns).filter_by(campaign_id=campaign_id).first()
        elif zoho_id is not None:
            return db_session.query(ZohoCampaigns).filter_by(zoho_id=zoho_id).first()
        elif campaign_id is not None and zoho_id is not None:
            return db_session.query(ZohoCampaigns).filter_by(campaign_id=campaign_id, zoho_id=zoho_id).first()
        return None
