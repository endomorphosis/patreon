from patreon.services import db
from sqlalchemy import BigInteger, Column, DateTime, Index, String, Text


class Message(db.Model):
    __tablename__ = 'messages'
    __table_args__ = (
        Index(
            'unique_index',
            'sender_id', 'recipient_id', 'sent_at', unique=True
        ),
    )

    message_id = Column(BigInteger, primary_key=True)
    sender_id = Column(BigInteger, nullable=False, index=True)
    recipient_id = Column(BigInteger, nullable=False, index=True)
    subject = Column(String(255))
    content = Column(Text, nullable=False)
    sent_at = Column(DateTime, nullable=False)
    opened_at = Column(DateTime)

    @property
    def created_at(self):
        return self.sent_at

    @property
    def message_text(self):
        return self.content

    def __eq__(self, other):
        # TODO(port): using custom equals because datetimes don't compare nicely
        return (
            type(other) == type(self) and
            other.sender_id == self.sender_id and
            other.recipient_id == self.recipient_id and
            other.sent_at == self.sent_at and
            other.content == self.content
        )


