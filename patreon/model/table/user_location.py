# coding: utf-8
from patreon.model.table.column_types import HTMLString
from patreon.services import db
from patreon.services.db import db_session
from sqlalchemy import Column, Integer, LargeBinary


class UserLocation(db.Model):
    __tablename__ = 'user_locations'

    user_id         = Column(Integer, primary_key=True, nullable=False)
    lat_long        = Column(LargeBinary, primary_key=True, nullable=False, index=True)
    display_name    = Column(HTMLString)  # 255

    @staticmethod
    def get(user_id=None):
        if user_id is not None:
            return db_session.query(UserLocation.user_id, UserLocation.display_name)\
                .filter_by(user_id=user_id)\
                .first()
        return None

