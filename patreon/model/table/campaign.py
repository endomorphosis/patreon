# coding: utf-8
import html

import patreon
from patreon.model.table import Activity, CampaignsUsers, ZohoCampaigns, Goal
from patreon.model.table.column_types import HTMLString
from patreon.services import db
from sqlalchemy import Column, DateTime, Integer, String, text
from sqlalchemy.ext.associationproxy import association_proxy
from sqlalchemy.ext.hybrid import hybrid_property
from sqlalchemy.orm import relationship, backref


class Campaign(db.Model):
    __tablename__ = 'campaigns'

    campaign_id         = Column(Integer, primary_key=True)
    summary             = Column(HTMLString)
    creation_name       = Column(String(64))
    pay_per_name        = Column(String(64))
    one_liner           = Column(String(256))
    main_video_embed    = Column(HTMLString) # 512
    main_video_url      = Column(String(128))
    image_small_url     = Column(String(128))
    image_url           = Column(String(128))
    thanks_video_url    = Column(String(128))
    thanks_embed        = Column(HTMLString) # 512
    thanks_msg          = Column(HTMLString)
    is_monthly          = Column(Integer, nullable=False, server_default=text("'0'"))
    is_nsfw             = Column(Integer, nullable=False, index=True, server_default=text("'0'"))
    created_at          = Column(DateTime, nullable=False)
    published_at        = Column(DateTime, index=True)

    activities          = relationship(Activity, backref=backref('campaign'), uselist=True)
    campaigns_users     = relationship(CampaignsUsers, backref=backref("campaign"), uselist=True)
    goals               = relationship(Goal, backref=backref("campaign"), uselist=True)
    rewards             = association_proxy('campaigns_users', 'rewards')
    zoho_map            = relationship(ZohoCampaigns, backref=backref("campaign"), uselist=False)
    users               = association_proxy('campaigns_users', 'user')
    zoho_id             = association_proxy('zoho_map', 'zoho_id')

    def as_search_dict(self):
        return {
            'objectID': self.algolia_id(),

            # indexed
            'creator_name': html.unescape(self.creator_name() or ''),
            'creation_name': html.unescape(self.creation_name or ''),
            'summary': html.unescape(self.summary or ''),
            'vanity': html.unescape(self.vanity() or ''),
            'pay_per_name': html.unescape(self.pay_per_name or ''),
            'thumb': self.users[0].thumb_url,
            'image': self.users[0].image_url,

            # non-indexed
            'url': self.canonical_url(),

            # numerically indexed
            'pledges_total_cents': patreon.model.manager.pledge_mgr.get_pledge_total_by_creator_id(self.creator_id),
            'patron_count': patreon.model.manager.pledge_mgr.get_patron_count_by_creator_id(self.creator_id),
        }

    @staticmethod
    @patreon.services.caching.multiget_cached(object_key='campaign_id')
    # TODO refactor rename
    # This name is not plural for legacy reasons. this should be refactored maybe eventually
    def get(campaign_id):
        return Campaign.query.filter(Campaign.campaign_id.in_(campaign_id)).all()

    @property
    def categories(self):
        return [category.category_id for category in self.users[0].categories]

    @hybrid_property
    def creator_id(self):
        return self.users[0].UID

    @hybrid_property
    def is_launched(self):
        # Intentional. Do not remove. Hybrid propery requires this.
        return self.published_at != None

    @hybrid_property
    def is_searchable(self):
        return self.is_launched & ~self.is_nsfw

    def algolia_id(self):
        return Campaign.algolia_id_for_id(self.campaign_id)

    @staticmethod
    def algolia_id_for_id(id_):
        return "campaign_" + str(id_)

    def creator_name(self):
        return self.users[0].full_name

    def vanity(self):
        return self.users[0].vanity

    def canonical_url(self):
        return self.users[0].canonical_url()

    @property
    def pledge_url(self):
        return "/bePatron?patAmt={0}&u={1}".format(1, self.creator_id)
