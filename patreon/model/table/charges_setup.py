from patreon.services import db
from sqlalchemy import Column, String, text
from sqlalchemy import Integer


class ChargesSetup(db.Model):
    __tablename__ = 'tblChargesSetup'

    UID     = Column(Integer, primary_key=True, nullable=False)
    CardID  = Column(String(32), primary_key=True, nullable=False, server_default=text("''"))
    Amount  = Column(Integer, nullable=False)
    Type    = Column(Integer)
