from patreon.services import db
from sqlalchemy.ext.hybrid import hybrid_property
from sqlalchemy import Column, DateTime, Integer


class PledgesDelete(db.Model):
    __tablename__ = 'tblPledgesDelete'

    CID     = Column(Integer, primary_key=True, nullable=False)
    UID     = Column(Integer, primary_key=True, nullable=False)
    Amount  = Column(Integer, nullable=False)
    Created = Column(DateTime, nullable=False)

    @hybrid_property
    def patron_id(self):
        return self.UID

    @hybrid_property
    def creator_id(self):
        return self.CID

    @hybrid_property
    def amount(self):
        return self.Amount

    @hybrid_property
    def created_at(self):
        return self.Created

    @staticmethod
    def get(creator_id, user_id):
        return PledgesDelete.query.filter_by(CID=creator_id, UID=user_id).first()
