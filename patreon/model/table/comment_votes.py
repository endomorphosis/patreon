# coding: utf-8
from patreon.services import db
from patreon.services.db import db_session
from sqlalchemy import Column, Integer
from sqlalchemy.ext.hybrid import hybrid_property


class CommentVote(db.Model):
    __tablename__ = 'comment_votes'

    comment_id  = Column(Integer, primary_key=True, nullable=False)
    activity_id = Column(Integer, nullable=False)
    user_id     = Column(Integer, primary_key=True, nullable=False)
    vote        = Column(Integer, nullable=False)

    @hybrid_property
    def comment_vote_id(self):
        return "comment-vote-{}-{}".format(self.comment_id, self.user_id)

    @staticmethod
    def get(comment_id, user_id):
        return db_session.query(CommentVote)\
            .filter(CommentVote.comment_id == comment_id, CommentVote.user_id == user_id)\
            .first()
