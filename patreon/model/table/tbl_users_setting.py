# coding: utf-8

from patreon.services import db
from sqlalchemy import Column, Integer, text, ForeignKey


class TblUsersSetting(db.Model):
    __tablename__ = 'tblUsersSettings'

    UID         = Column(Integer, ForeignKey('tblUsers.UID'), primary_key=True)
    FBImport    = Column(Integer, nullable=False, server_default=text("'1'"))
    Privacy     = Column(Integer, nullable=False, server_default=text("'0'"))

    @property
    def user_id(self):
        return self.UID

    @property
    def fb_import(self):
        return self.FBImport

    @property
    def privacy(self):
        return self.Privacy

    @staticmethod
    def get(user_id=None):
        if user_id is not None:
            return TblUsersSetting.query.filter_by(UID=user_id).first()
        return None
