from patreon.services import db
from sqlalchemy import BigInteger, Column, DateTime, Index, String, Text
from sqlalchemy.ext.hybrid import hybrid_property


class YoutubeImport(db.Model):
    __tablename__ = 'tblYoutubeImport'
    __table_args__ = (
        Index(
            'unique_index',
            'UID', 'VideoID', unique=True
        ),
    )

    ID          = Column(BigInteger, primary_key=True)
    UID         = Column(BigInteger, nullable=False, index=True)
    Title       = Column(String(255))
    Content     = Column(Text)
    Url         = Column(String(255))
    ImageUrl    = Column(String(255))
    Created     = Column(DateTime)
    VideoID     = Column(String(16), nullable=False, index=True)

    @hybrid_property
    def user_id(self):
        return self.UID

    @property
    def title(self):
        return self.Title

    @property
    def content(self):
        return self.Content

    @property
    def url(self):
        return self.Url

    @property
    def thumbnail(self):
        return self.ImageUrl

    @property
    def created_at(self):
        return self.Created

    @property
    def video_id(self):
        return self.VideoID
