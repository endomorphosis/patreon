import datetime

from patreon.constants.account_types import PaymentAccountTypes, SystemAccounts
from patreon.model.table.column_types import EnumType
from patreon.services import db
from sqlalchemy import BigInteger, Column, DateTime, String


class Account(db.Model):
    __tablename__ = 'accounts'

    account_id              = Column(BigInteger, primary_key=True)
    type                    = Column(EnumType('type', PaymentAccountTypes), nullable=False)
    user_id                 = Column(BigInteger, default=0, index=True, nullable=False)
    payment_instrument_id   = Column(String(255), default='', index=True, nullable=False)
    campaign_id             = Column(BigInteger, default=0, index=True, nullable=False)
    system_classification   = Column(EnumType('classification', SystemAccounts), default='', nullable=False)
    created_at              = Column(DateTime, default=datetime.datetime.utcnow, nullable=False)
    deleted_at              = Column(DateTime)
