from patreon.services import encryption
from sqlalchemy import types


class EncryptedData(types.TypeDecorator):
    def python_type(self):
        return str

    impl = types.Text

    def process_bind_param(self, value, dialect):
        if encryption.looks_encrypted(value):
            return value
        return encryption.encrypt(value)

    def process_literal_param(self, value, dialect):
        return self.process_bind_param(value, dialect)

    def process_result_value(self, value, dialect):
        return value
