from .html_string import HTMLString


class UTFMultibyteString(HTMLString):
    def process_bind_param(self, value, dialect):
        clean_html = super().process_bind_param(value, dialect)
        entity_string = ''.join([
                                    c if ord(c) < 240 else c.encode('ascii', errors='xmlcharrefreplace').decode('utf-8')
                                    for c in clean_html
                                    ])
        return entity_string

    def process_result_value(self, value, dialect):
        return super().process_result_value(value, dialect)
