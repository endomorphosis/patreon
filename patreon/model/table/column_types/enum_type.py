from patreon.exception.db_errors import BadEnumType
from sqlalchemy import types


class EnumType(types.TypeDecorator):
    impl = types.Text

    def __init__(self, column_name, enumType):
        super().__init__()
        self.column_name = column_name  # Column name

        self.allowed_vals = set()  # Set of allowed values
        self.enumType = enumType

    # Goes from python to SQL
    def process_bind_param(self, value, dialect):
        if not value:
            return value
        if isinstance(value, self.enumType):
            return value.value
        else:
            raise BadEnumType(str(value), self.column_name)

    def process_literal_param(self, value, dialect):
        return self.process_bind_param(value, dialect)

    # Goes from SQL to python
    def process_result_value(self, value, dialect):
        if not value:
            return value
        try:
            return self.enumType(value)
        except ValueError:
            raise BadEnumType(value, self.column_name)
