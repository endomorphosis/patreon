from patreon.util.html_cleaner import clean
from sqlalchemy import types


class HTMLString(types.TypeDecorator):
    def python_type(self):
        return str

    impl = types.Text

    def __init__(self, callback=None):
        if not callback:
            self.clean = clean
        else:
            self.clean = callback
        super().__init__()

    def process_bind_param(self, value, dialect):
        if value is None:
            return None
        return self.clean(value)

    def process_literal_param(self, value, dialect):
        return self.process_bind_param(value, dialect)

    def process_result_value(self, value, dialect):
        if value is None:
            return None
        return self.clean(value)
