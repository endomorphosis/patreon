from patreon.util.unsorted import jsencode, jsdecode
from sqlalchemy import types


class JSONString(types.TypeDecorator):
    def python_type(self):
        return str

    impl = types.Text

    def process_bind_param(self, value, dialect):
        return jsencode(value)

    def process_literal_param(self, value, dialect):
        return self.process_bind_param(value, dialect)

    def process_result_value(self, value, dialect):
        return jsdecode(value)
