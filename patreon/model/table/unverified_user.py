# coding: utf-8
from patreon.services import db
from patreon.services.db import db_session
from sqlalchemy import Column, DateTime, Integer, String, text


class UnverifiedUser(db.Model):
    __tablename__ = 'tblUnverifiedUsers'

    Verification    = Column(String(32), primary_key=True, server_default=text("''"))
    Email           = Column(String(128), nullable=False)
    FName           = Column(String(64))
    LName           = Column(String(64))
    Password        = Column(String(64))
    Created         = Column(DateTime, nullable=False)
    FBID            = Column(Integer)
    ImageUrl        = Column(String(128))
    ThumbUrl        = Column(String(128))
    Gender          = Column(Integer, nullable=False, server_default=text("'0'"))
    Vanity          = Column(String(64))

    @property
    def verification_token(self):
        return self.Verification

    @property
    def email(self):
        return self.Email

    @property
    def first_name(self):
        return self.FName

    @property
    def last_name(self):
        return self.LName

    @property
    def gender(self):
        return self.Gender

    @property
    def vanity(self):
        return self.Vanity

    @property
    def password(self):
        return self.Password

    @property
    def facebook_id(self):
        return self.FBID

    @property
    def image_url(self):
        return self.ImageUrl

    @property
    def thumb_url(self):
        return self.ThumbUrl

    @property
    def created_at(self):
        return self.Created

    def as_dict(self):
        # Overrides Model.as_dict.
        # Password not included.
        return {
            'verification_token': self.verification_token,
            'email': self.email,
            'first_name': self.first_name,
            'last_name': self.last_name,
            'gender': self.gender,
            'vanity': self.vanity,
            'facebook_id': self.facebook_id,
            'image_url': self.image_url,
            'thumb_url': self.thumb_url,
            'created_at': self.created_at
        }

    @staticmethod
    def get(verification=None):
        if verification is not None:
            return db_session.query(UnverifiedUser)\
                .filter_by(Verification=verification)\
                .first()
        return None
