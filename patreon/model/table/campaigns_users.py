# coding: utf-8
from patreon.model.table import Reward
from patreon.services import db
from patreon.services.db import db_session
from sqlalchemy import Column, DateTime, Integer, ForeignKey, text
from sqlalchemy.orm import relationship, backref
from sqlalchemy.ext.hybrid import hybrid_property


class CampaignsUsers(db.Model):
    __tablename__ = 'campaigns_users'

    user_id                     = Column(Integer, ForeignKey("tblUsers.UID"), primary_key=True, nullable=False)
    campaign_id                 = Column(Integer, ForeignKey("campaigns.campaign_id"),
                                         primary_key=True, nullable=False, index=True)
    is_admin                    = Column(Integer, nullable=False, server_default=text("'1'"))
    joined_at                   = Column(DateTime, nullable=False)
    should_email_on_patron_post = Column(Integer, nullable=False, server_default=text("'1'"))
    should_email_on_new_patron  = Column(Integer, nullable=False, server_default=text("'1'"))
    should_push_on_patron_post  = Column(Integer, nullable=False, server_default=text("'1'"))
    should_push_on_new_patron   = Column(Integer, nullable=False, server_default=text("'1'"))

    rewards                     = relationship(Reward, backref=backref("campaigns_users"), uselist=True)

    @hybrid_property
    def settings_id(self):
        return '{}-{}-campaign-settings'.format(self.user_id, self.campaign_id)

    @staticmethod
    def get(user_id=None, campaign_id=None):
        if user_id is not None and campaign_id is not None:
            return db_session.query(CampaignsUsers)\
                .filter_by(user_id=user_id) \
                .filter_by(campaign_id=campaign_id)\
                .first()
        elif user_id is not None:
            return db_session.query(CampaignsUsers)\
                .filter_by(user_id=user_id)\
                .first()
        elif campaign_id is not None:
            return db_session.query(CampaignsUsers)\
                .filter_by(campaign_id=campaign_id)\
                .first()
        return None
