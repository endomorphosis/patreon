from patreon.model.table import UnverifiedUser


def find_by_email(email):
    return UnverifiedUser.query.filter_by(Email=email).all()
