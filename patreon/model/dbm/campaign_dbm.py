from patreon.model.table import Campaign


def find_searchable():
    return Campaign.query \
        .filter(Campaign.is_searchable) \
        .join(*Campaign.users.attr)


def find_all():
    return Campaign.query


def find_by_campaign_id_list(campaign_ids):
    return Campaign.query.filter(Campaign.campaign_id.in_(campaign_ids))


def find_searchable_by_id_list(campaign_ids):
    return Campaign.query \
        .filter(Campaign.is_searchable) \
        .filter(Campaign.campaign_id.in_(campaign_ids)) \
        .all()
