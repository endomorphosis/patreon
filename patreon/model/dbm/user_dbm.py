from patreon.model.table import User, ReportDone


def get_user(user_id=None):
    return User.get(user_id)


def get_users(user_ids):
    return User.query.filter(User.UID.in_(user_ids))



# TODO MARCOS do it with types

def get_user_by_vanity(vanity):
    return User.get_unique(vanity=vanity)


def get_user_by_facebook_id(facebook_id):
    return User.get_unique(facebook_id=facebook_id)


def get_user_by_email(email):
    return User.get_unique(email=email)


def mark_users_as_checked(user_ids):
    # TODO MARCOS check to see if mark_users_as_checked works
    for user_id in user_ids:
        ReportDone.insert({'UID': user_id})


def get_unchecked_creator_ids():
    pass


def create_user():
    # TODO MARCOS 2.6 implement create_user
    pass


def get_suspended_status(user_id):
    return User.get(user_id=user_id).is_suspended


def suspend_user(user_id):
    return User.get(user_id=user_id).update({'is_suspended': 1})


'''
public function getUncheckedCreatorIDs() {
    $subQuery = $this->queryBuilder()
        ->select("UID")
        ->table("tblReportDone");

    $query = $this->queryBuilder()
        ->select("user_id")
        ->table("campaigns_users")
        ->join("campaigns", "campaigns.campaign_id", "=", "campaigns_users.campaign_id")
        ->where("campaigns.creation_name", "IS NOT", null)
        ->where("user_id", "NOT IN", $subQuery);

    $output = $this->database->select_qb($query);
    call_user_func_array("array_merge", $output);
    return $output;
}

public function createUser(FirstName $firstName, LastName $lastName = null,
    EmailAddress $email, Vanity $vanity = null, Gender $gender, $isVerified,
    URL $imageURL = null, URL $thumbURL = null, $about = null,
    DateTime $createdAt
) {
    $userID = $this->queryBuilder()
        ->table("tblUsers")
        ->insert(self::modelToColumns(
            $firstName, $lastName, $email, $vanity, $gender, $isVerified,
            $imageURL, $thumbURL, $about, $createdAt
        ));

    return new User(
        $userID, $firstName, $lastName, $email, $vanity, $gender,
        $isVerified, $imageURL, $thumbURL, $about, $createdAt
    );
}

'''
