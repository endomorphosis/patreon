from patreon.model.table import Follow


def find_by_follower_id(follower_id):
    return Follow.query\
        .filter_by(follower_id=follower_id)


def find_by_followed_id(followed_id):
    return Follow.query\
        .filter_by(followed_id=followed_id)


def get(follower_id, followed_id):
    return Follow.query\
        .filter_by(follower_id=follower_id)\
        .filter_by(followed_id=followed_id)


def find_by_follower_ids(follower_ids):
    return Follow.query\
        .filter(Follow.follower_id.in_(follower_ids))
