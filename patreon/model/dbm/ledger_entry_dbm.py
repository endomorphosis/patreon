from patreon.model.dbm.base_dbm import BaseDBM
from patreon.model.table import LedgerEntry
from sqlalchemy import func


class LedgerEntryDBM(BaseDBM):
    def __init__(self):
        super().__init__(LedgerEntry)

    def get_monetary_sum(self, label):
        return self.with_entity(func.sum(LedgerEntry.amount_cents).label(label))

    def for_account_id(self, account_id):
        return self.filter(LedgerEntry.account_id == account_id)
