from patreon.model.table import Goal


def find_by_campaign_id(campaign_id):
    return Goal.query.filter_by(campaign_id=campaign_id).order_by(Goal.amount_cents.asc())


def find_unmet_goals_by_campaign_id(campaign_id, pledge_total_cents):
    query = find_by_campaign_id(campaign_id)
    return query.filter(Goal.amount_cents > (pledge_total_cents or 0))


def find_unreached_goals_by_campaign_id(campaign_id):
    query = find_by_campaign_id(campaign_id)
    return query.filter(Goal.reached_at.is_(None))


def find_met_goals_by_campaign_id(campaign_id, pledge_total_cents):
    query = find_by_campaign_id(campaign_id)
    return query.filter(Goal.amount_cents <= (pledge_total_cents or 0))


def get_by_goal_id(goal_id):
    return Goal.query.filter_by(goal_id=goal_id)
