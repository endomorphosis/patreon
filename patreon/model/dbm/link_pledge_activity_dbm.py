from patreon.model.table import LinkPledgeActivity


def find_by_activity_id(activity_id):
    return LinkPledgeActivity.query.filter_by(HID=activity_id)


def find_by_user_id(user_id):
    return LinkPledgeActivity.query.filter_by(UID=user_id)
