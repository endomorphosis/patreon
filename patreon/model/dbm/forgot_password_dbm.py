from patreon.model.table import ForgotPassword


def find_by_user_id(user_id):
    return ForgotPassword.query.filter_by(UID=user_id)
