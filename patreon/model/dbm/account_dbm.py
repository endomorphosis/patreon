from patreon.constants.account_types import PaymentAccountTypes
from patreon.model.dbm.base_dbm import BaseDBM
from patreon.model.table.account import Account


class AccountDBM(BaseDBM):
    def __init__(self):
        super().__init__(Account)

    def account_type_is(self, account_type):
        return self.filter(self.table.type == account_type)

    def is_payment_instrument_account(self):
        return self.account_type_is(PaymentAccountTypes.USER_PAYMENT_INSTRUMENT)

    def belongs_to_user(self, user_id):
        return self.filter(self.table.user_id == user_id)

    def has_payment_instrument(self, payment_instrument_token):
        return self.filter(self.table.payment_instrument_id == payment_instrument_token)

    def is_credit_account(self):
        return self.account_type_is(PaymentAccountTypes.USER_CREDIT)

    def belongs_to_campaign(self, campaign_id):
        return self.filter(self.table.campaign_id == campaign_id)

    def is_system_class(self, system_classification):
        return self.filter(self.table.system_classification == system_classification)
