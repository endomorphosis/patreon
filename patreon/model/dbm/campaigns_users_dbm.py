from patreon.model.table import CampaignsUsers


def find_campaigns_users_by_campaign_id(campaign_id):
    return CampaignsUsers.query.filter_by(campaign_id=campaign_id).all()


def find_by_campaign_id_list(campaign_ids):
    return CampaignsUsers.query.filter(CampaignsUsers.campaign_id.in_(campaign_ids))


def find_campaigns_users_by_user_id(user_id):
    return CampaignsUsers.query \
        .filter(CampaignsUsers.user_id == user_id) \
        .all()


def find_campaigns_users_by_user_ids(user_ids):
    return CampaignsUsers.query \
        .filter(CampaignsUsers.user_id.in_(user_ids)) \
        .all()


def get_by_id(settings_id):
    user_id, campaign_id = settings_id.split('-')[:2]
    return CampaignsUsers.get(user_id=user_id, campaign_id=campaign_id)
