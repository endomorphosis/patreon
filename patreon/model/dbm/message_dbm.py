from patreon.model.table import Message
from patreon.services import db
from patreon.services.db import db_session
from patreon.util.unsorted import now
from sqlalchemy import func


def get(message_id):
    return Message.query.filter_by(message_id=message_id).first()


def get_new_message_count_by_sender_id(sender_id):
    return db_session.query(func.count(Message.message_id).label('count')) \
        .filter_by(sender_id=sender_id, opened_at=None).first()


def get_conversation(user_id1, user_id2):
    return Message.query \
        .filter(
        (
            (Message.recipient_id == user_id1) & (Message.sender_id == user_id2)
        )
        |
        (
            (Message.recipient_id == user_id2) & (Message.sender_id == user_id1)
        )
    )


def find_by_recipient_id(user_id):
    return Message.query.filter_by(recipient_id=user_id)


def add_message(sender_id, recipient_id, content):
    message_id = Message.insert({
        'sender_id': sender_id,
        'recipient_id': recipient_id,
        'content': content,
        'sent_at': now()
    })[0]
    return get(message_id)


def increment_unread_count(user_id):
    return db.query("""
        INSERT INTO tblUsersExtra(UID, NewMsgs)
        VALUES ('%s', 1)
        ON DUPLICATE KEY
        UPDATE NewMsgs = NewMsgs + 1
    """, [user_id])
