from patreon.model.dbm.base_dbm import BaseDBM
from patreon.model.table import Actions, Activity
from patreon.model.type import ActionType


class ActionDBM(BaseDBM):
    def __init__(self):
        super(ActionDBM, self).__init__(Actions)

    def likes_by_user(self, user_id):
        return self.user_id_is(user_id).type_is_like()

    def user_id_is(self, user_id):
        return self.filter(self.table.UID == user_id)

    def action_type_is(self, action_type):
        return self.filter(self.table.Type == action_type)

    def type_is_like(self):
        return self.action_type_is(ActionType.LIKE.value)

    def post_id_is(self, post_id):
        return self.filter(self.table.HID == post_id)

    def created_before(self, time):
        return self.filter(self.table.created_at < time)

    def created_on_or_after(self, time):
        return self.filter(self.table.created_at >= time)

    def join_posts(self):
        self.working_query = self.working_query.join(self.table.post)
        return self

    def likes_on_posts_by_user(self, user_id):
        return self.type_is_like().join_posts().filter(Activity.user_id == user_id)
