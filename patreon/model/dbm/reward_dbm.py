import patreon
from patreon.model.table import Reward
from patreon.services.db import db_session
from sqlalchemy import func


def find_by_creator_id_ordered(creator_id):
    return Reward.query \
        .filter(Reward.UID == creator_id) \
        .order_by(Reward.Amount)


def find_by_creator_id(creator_id):
    return Reward.query \
        .filter(Reward.UID == creator_id)


def get_by_amount_and_creator_id(creator_id, amount):
    return Reward.query \
        .filter_by(UID=creator_id, Amount=amount) \
        .order_by(Reward.Amount) \
        .first()

@patreon.services.caching.request_cached()
def get_minimum_reward_amount_by_creator_id(creator_id):
    return db_session.query(func.min(Reward.Amount).label('min')) \
        .filter_by(UID=creator_id) \
        .first()
