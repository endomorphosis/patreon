from patreon.model.table import Presence


def get(presence_id):
    return Presence.query.filter_by(presence_id=presence_id).first()


def get_by_user_id_and_room(user_id, room):
    return Presence.query.filter_by(user_id=user_id, room=room).first()


def get_by_user_id_and_activity_id(user_id, activity_id):
    room = Presence.room_name_for_activity_id(activity_id)
    return get_by_user_id_and_room(user_id=user_id, room=room)


def find_by_user_id(user_id):
    return Presence.query.filter_by(user_id=user_id)


def find_by_user_id_list(user_ids):
    return Presence.query.filter(Presence.user_id.in_(user_ids))
