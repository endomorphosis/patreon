from patreon.model.table import Card, PaypalToken


def get(card_id):
    return Card.query.filter_by(RID=card_id).first()


def find_by_user_id(user_id):
    return Card.query.filter_by(UID=user_id)


def find_by_user_id_list(user_ids):
    return Card.query.filter(Card.UID.in_(user_ids))


def find_paypal_state_by_user_id(user_id):
    return PaypalToken.query.filter_by(user_id=user_id).all()
