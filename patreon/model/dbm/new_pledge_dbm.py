import datetime

from patreon.model.table import NewPledge, Campaign
from patreon.services import db


def all_historic_pledges_by_patron_and_campaign(patron_id, campaign_id):
    return NewPledge.query \
        .filter_by(user_id=patron_id, campaign_id=campaign_id)


def all_undeleted_pledges_by_patron_and_campaign(patron_id, campaign_id):
    return all_historic_pledges_by_patron_and_campaign(patron_id, campaign_id) \
        .filter_by(deleted_at=None)


def all_undeleted_pledges_to_campaign(campaign_id):
    return NewPledge.query \
        .filter(NewPledge.deleted_at.is_(None)) \
        .filter(NewPledge.campaign_id == campaign_id)


def all_historic_pledges_by_patron_and_campaign_in_order(patron_id, campaign_id):
    return all_historic_pledges_by_patron_and_campaign(patron_id, campaign_id) \
        .order_by(NewPledge.created_at.desc())


def get_pledge_by_patron_and_campaign(patron_id, campaign_id):
    return all_undeleted_pledges_by_patron_and_campaign(patron_id, campaign_id) \
        .order_by(NewPledge.created_at.desc()) \
        .first()


def delete_by_patron_and_campaign(patron_id, campaign_id, deleted_at=None):
    if deleted_at is None:
        deleted_at = datetime.datetime.now()
    all_undeleted_pledges_by_patron_and_campaign(patron_id, campaign_id) \
        .update({"deleted_at": deleted_at})


def make_or_update_pledge(user_id, campaign_id, amount_cents, pledge_cap_amount_cents=None,
                          reward_tier_id=None, pledge_vat_location_id=None, created_at=None,
                          patron_pays_fees=0, address_id=None):
    if created_at is None:
        created_at = datetime.datetime.now()

    with db.db_session.begin_nested():
        delete_by_patron_and_campaign(
            patron_id=user_id,
            campaign_id=campaign_id,
            deleted_at=created_at
        )

        if amount_cents > 0:
            NewPledge.insert({
                'user_id': user_id,
                'campaign_id': campaign_id,
                'amount_cents': amount_cents,
                'pledge_cap_amount_cents': pledge_cap_amount_cents,
                'reward_tier_id': reward_tier_id,
                'pledge_vat_location_id': pledge_vat_location_id,
                'created_at': created_at,
                'patron_pays_fees': (patron_pays_fees == 1),
                'mailing_address_id': address_id
            })


def patron_has_any_vat_pledges(patron_id):
    return NewPledge.query \
        .filter(NewPledge.user_id == patron_id) \
        .filter(NewPledge.pledge_vat_location_id.isnot(None)) \
        .limit(1).count()  # always return 0 or 1


def patron_has_any_pledges_to_monthly_creators(patron_id):
    return NewPledge.query \
        .filter(NewPledge.user_id == patron_id) \
        .join(NewPledge.campaign).filter(Campaign.is_monthly == 1) \
        .limit(1).count()  # always return 0 or 1
