from patreon.model.table import Category


def find_by_category_id(category_id):
    return Category.query \
        .filter_by(Category=category_id)


def find_by_user_id(user_id):
    return Category.query \
        .filter_by(UID=user_id)
