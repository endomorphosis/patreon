from patreon.model.dbm.base_dbm import BaseDBM
from patreon.model.table import PaymentEvent
from sqlalchemy import Integer


class PaymentEventDBM(BaseDBM):
    def __init__(self):
        super().__init__(PaymentEvent)

    def occurred_before(self, time):
        return self.filter(self.table.created_at < time)

    def occurred_on_or_before(self, time):
        return self.filter(self.table.created_at <= time)

    def occurred_after(self, time):
        return self.filter(self.table.created_at > time)

    def occured_on_or_after(self, time):
        return self.filter(self.table.created_at >= time)

    def order_by_created(self):
        return self.order_by('created_at', descending=True)

    def is_type(self, type):
        return self.filter(self.table.type == type)

    def is_subtype(self, subtype):
        return self.filter(self.table.subtype == subtype)

    def is_user_id(self, user_id):
        return self.filter(self.table.json_data['user_id'].cast(Integer) == user_id)

    def occurred_before_event_id(self, cursor):
        event = PaymentEvent.get(cursor)
        event_time = event.created_at

        return self.filter((self.table.created_at < event_time) |
                           ((self.table.created_at == event_time)
                            &
                            (self.table.payment_event_id < cursor)))

    def order_by_created_then_id(self):
        return self.order_by_created().order_by('payment_event_id', descending=True)
