from patreon.model.table import Activity
from sqlalchemy import func


def find_public_by_campaign_id(campaign_id):
    return Activity.query\
        .filter_by(campaign_id=campaign_id)\
        .filter(Activity.is_public)


def find_all_by_user_id(user_id):
    return Activity.query.filter_by(user_id=user_id)


def find_public_by_user_id(user_id):
    return find_all_by_user_id(user_id).filter(Activity.is_public)


def get_public_activity(activity_id):
    return Activity.query\
        .filter_by(activity_id=activity_id)\
        .filter(Activity.is_public)


def find_searchable():
    return Activity.query\
        .filter(Activity.is_public)\
        .filter(Activity.is_searchable)


def find_by_activity_id_list(activity_ids):
    return Activity.query\
        .filter(Activity.activity_id.in_(activity_ids))\
        .filter(Activity.is_public) \
        .order_by(func.field(Activity.activity_id, *activity_ids))
