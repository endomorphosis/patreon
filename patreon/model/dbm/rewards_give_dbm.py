from datetime import date

from sqlalchemy import func, extract
import patreon
from patreon import globalvars
from patreon.constants import RewardStatus, MemCachedKey
from patreon.util import datetime
from patreon.model.table import RewardsGive
from patreon.services.db import db_session
from patreon.services import db
from patreon.services.caching import memcache


def get_bills_for_post(post_id):
    return RewardsGive.query \
        .filter(RewardsGive.HID == post_id) \
        .all()


def create_bill(creator_id, post_id):
    result = db.query("""
        INSERT INTO tblRewardsGive (
            UID, HID, RID, RDescription, CID, Status, Created, Pledge, CardID, Street,
            Street2, City, State, Zip, Country
        )
        SELECT
            p.UID, %s, p.RID, r.Description, p.CID, %s, NOW(), p.Pledge, p.CardID,
            p.Street, p.Street2, p.City, p.State, p.Zip, p.Country
        FROM
            tblPledges p
        LEFT JOIN
            tblRewards r
            ON p.RID = r.RID
        LEFT JOIN
            (
                SELECT
                    rg.UID, rg.CID, SUM(rg.Pledge) as Pledged
                FROM
                    tblRewardsGive rg
                WHERE
                    rg.CID = %s
                    AND rg.Status = %s
                    AND CONVERT_TZ(rg.Created, 'GMT', 'US/Pacific')
                        > DATE_FORMAT(CONVERT_TZ(NOW(), 'GMT', 'US/Pacific'), '%%Y-%%m-01')
                GROUP BY rg.UID, rg.CID
            ) as already
            ON p.UID = already.UID
            AND p.CID = already.CID
        WHERE
            p.CID = %s
            AND (
                p.MaxAmount IS NULL
                OR
                ((IFNULL(already.Pledged, 0) + p.Pledge) <= p.MaxAmount)
            )
        """, [
        post_id,
        RewardStatus.REWARD_PENDING.value,
        creator_id,
        RewardStatus.REWARD_PENDING.value,
        creator_id
    ])
    clear_post_cache(post_id)
    if patreon.is_test_environment():
        db.test_dirty_table.add(RewardsGive)
    return result  # TODO MARCOS 1.0


def clear_post_cache(post_id):
    memcache.delete(MemCachedKey.SHARE.value + str(post_id))
    memcache.delete(MemCachedKey.SHARE_PATRONS.value + str(post_id))


@patreon.services.caching.request_cached()
def pledge_sums_by_creator_id_and_user_id(creator_id, user_id):
    result = db_session.query(
        RewardsGive.CID,
        func.sum(RewardsGive.Pledge).label('PledgeSum')
    ) \
        .filter_by(
        CID=creator_id,
        UID=user_id,
        Status=globalvars.get('REWARD_PROCESSED')
    ) \
        .group_by(RewardsGive.CID) \
        .first()

    if result is None:
        return 0
    return result.PledgeSum


def pledge_sums_by_creator_id_for_year(creator_id):
    result = db_session.query(
        RewardsGive.CID,
        func.sum(RewardsGive.Pledge).label('PledgeSum')
    ) \
        .filter_by(
        CID=creator_id,
        Status=globalvars.get('REWARD_PROCESSED')
    ) \
        .filter(extract('year', RewardsGive.Created) == date.today().year) \
        .group_by(RewardsGive.CID) \
        .first()
    if result is None:
        return 0
    return result.PledgeSum


@patreon.services.caching.request_cached()
def pledge_sums_for_users_by_creator_id(creator_id):
    result = db_session.query(
        RewardsGive.UID,
        func.sum(RewardsGive.Pledge).label('PledgeSum')
    ) \
        .filter_by(
        CID=creator_id,
        Status=globalvars.get('REWARD_PROCESSED')
    ) \
        .group_by(RewardsGive.UID) \
        .all()

    return {element.UID: element.PledgeSum for element in result}


def pledge_sums_by_creator_id(creator_id, user_id):
    result = db_session.query(
        RewardsGive.CID,
        func.sum(RewardsGive.Pledge).label('PledgeSum')
    ) \
        .filter_by(
        CID=creator_id,
        UID=user_id,
        Status=globalvars.get('REWARD_PROCESSED')
    ) \
        .group_by(RewardsGive.CID) \
        .first()

    if result is None:
        return 0
    return result.PledgeSum


@patreon.services.caching.request_cached()
def pledge_sums_by_user_id(user_id):
    result = db_session.query(
        RewardsGive.CID,
        func.sum(RewardsGive.Pledge).label('PledgeSum')
    ) \
        .filter_by(
        UID=user_id,
        Status=globalvars.get('REWARD_PROCESSED')
    ) \
        .group_by(RewardsGive.UID).first()

    if result is None:
        return 0
    return result.PledgeSum


@patreon.services.caching.request_cached()
def find_supported_creation_ids_by_user_id_paginated(user_id, page=1):
    number_per_page = globalvars.get('SHARES_PER_PAGE')
    offset = (page - 1) * number_per_page
    tuples = db_session.query(RewardsGive.HID) \
        .filter_by(UID=user_id) \
        .order_by(RewardsGive.Created.desc()) \
        .offset(offset) \
        .limit(number_per_page) \
        .all()

    return [tuple[0] for tuple in tuples]


@patreon.services.caching.request_cached()
def find_failed_and_pending_by_creator_id_user_id(creator_id, user_id):
    return RewardsGive.query \
        .filter_by(
            CID=creator_id,
            UID=user_id
        ) \
        .filter((RewardsGive.Status == globalvars.get('REWARD_PENDING')) |
                (RewardsGive.Status == globalvars.get('REWARD_DECLINED'))) \
        .order_by(RewardsGive.Created.desc()) \
        .all()


@patreon.services.caching.request_cached()
def find_processed_by_creator_id_user_id(creator_id, user_id):
    return RewardsGive.query \
        .filter_by(
            CID=creator_id,
            UID=user_id,
            Status=globalvars.get('REWARD_PROCESSED')
        ) \
        .order_by(RewardsGive.Created.desc()) \
        .all()


@patreon.services.caching.request_cached()
def find_pending_and_failed_creators_by_user_id(user_id):
    result = db_session.query(RewardsGive.CID) \
        .filter_by(
            UID=user_id
        ) \
        .filter((RewardsGive.Status == globalvars.get('REWARD_PENDING')) |
                (RewardsGive.Status == globalvars.get('REWARD_DECLINED'))) \
        .order_by(RewardsGive.Created.desc()) \
        .all()

    return [element.CID for element in result]


@patreon.services.caching.request_cached()
def find_processed_creators_by_user_id(user_id):
    bills = db_session.query(RewardsGive.CID) \
        .filter_by(
        UID=user_id,
        Status=globalvars.get('REWARD_PROCESSED')
    ) \
        .order_by(RewardsGive.Created.desc()) \
        .all()

    # TODO MARCOS SQL UNIQUE
    creator_id_list = [bill.CID for bill in bills]
    return list(set(creator_id_list))


def delete_by_user_id_creator_id(user_id, creator_id):
    RewardsGive.query \
        .filter_by(UID=user_id, CID=creator_id) \
        .delete()


def delete_by_post_id(post_id):
    deletable_statuses = [
        RewardStatus.REWARD_PENDING.value,
        RewardStatus.REWARD_DECLINED.value
    ]
    for status in deletable_statuses:
        RewardsGive.query \
            .filter_by(HID=post_id, Status=status) \
            .delete()


def find_sum_patron_count_all_by_activity_id(activity_id):
    result = db_session.query(
        func.sum(RewardsGive.Pledge).label('sum'),
        func.count(RewardsGive.Pledge).label('count')
    ) \
        .filter_by(HID=activity_id) \
        .first()

    return result.sum, result.count


def find_sum_patron_count_success_by_activity_id(activity_id):
    result = db_session.query(
        func.sum(RewardsGive.Pledge).label('sum'),
        func.count(RewardsGive.Pledge).label('count')
    ) \
        .filter_by(
        HID=activity_id,
        Status=globalvars.get('REWARD_PROCESSED')
    ) \
        .first()

    return result.sum, result.count


def get_lifetime_by_creator_id(creator_id):
    return db_session.query(func.sum(RewardsGive.Pledge).label('sum')) \
        .filter_by(
        CID=creator_id,
        Status=globalvars.get('REWARD_PROCESSED')
    ) \
        .first().sum


def find_pledge_sums_for_creator_by_user_id_and_date(creator_id):
    month = datetime.datetime.now().month
    year = datetime.datetime.now().year
    result = db_session.query(
        RewardsGive.UID,
        func.sum(RewardsGive.Pledge).label('sum')
    ) \
        .filter(RewardsGive.CID == creator_id) \
        .filter(extract('year', RewardsGive.Created) == year) \
        .filter(extract('month', RewardsGive.Created) == month) \
        .group_by(RewardsGive.UID) \
        .all()

    return {element.UID: element.sum for element in result}


def find_pledge_sums_for_creator_by_user_id(creator_id):
    result = db_session.query(
        RewardsGive.UID,
        func.sum(RewardsGive.Pledge).label('sum')
    ) \
        .filter(RewardsGive.CID == creator_id) \
        .filter(RewardsGive.Status == 1) \
        .group_by(RewardsGive.UID) \
        .all()

    return {element.UID: element.sum for element in result}


def set_failed_payments_on_card_to_pending(user_id, card_id):
    rewards = RewardsGive.query \
        .filter_by(UID=user_id, CardID=card_id) \
        .filter(RewardsGive.Status != globalvars.get('REWARD_PROCESSED')) \
        .filter(RewardsGive.Status != globalvars.get('REWARD_REFUNDED')) \
        .all()

    for reward in rewards:
        reward.update({'Status': globalvars.get('REWARD_PENDING')})


def move_failed_payments_for_patron_and_creator_to_card(user_id, creator_id, card_id):
    rewards = RewardsGive.query \
        .filter_by(UID=user_id, CID=creator_id) \
        .filter(RewardsGive.Status != globalvars.get('REWARD_PROCESSED')) \
        .filter(RewardsGive.Status != globalvars.get('REWARD_REFUNDED')) \
        .all()

    for reward in rewards:
        reward.update({'CardID': card_id})


def find_pledge_sums(user_id):
    return db_session.query(
        func.sum(RewardsGive.Pledge).label('Pledge'),
        func.sum(RewardsGive.Fee).label('Fee'),
        func.sum(RewardsGive.PatreonFee).label('PatreonFee')
    ) \
        .filter(RewardsGive.CID == user_id) \
        .filter(RewardsGive.Status == globalvars.get('REWARD_PROCESSED')) \
        .first()


@patreon.services.caching.request_cached()
def query_by_patron_and_creator(patron_id, creator_id):
    return RewardsGive.query \
        .filter_by(CID=creator_id, UID=patron_id)


@patreon.services.caching.request_cached()
def get_most_recent_invoice_for_patron_and_creator(patron_id, creator_id):
    return query_by_patron_and_creator(patron_id=patron_id,
                                       creator_id=creator_id) \
        .order_by(RewardsGive.Created.desc()) \
        .first()


def get_transactions_for_creator(creator_id):
    return db_session.query(
        RewardsGive.Created,
        RewardsGive.Pledge,
        RewardsGive.Fee,
        RewardsGive.PatreonFee,
        RewardsGive.Status
    )\
        .filter(RewardsGive.CID == creator_id)\
        .filter(RewardsGive.Status == 1)\
        .order_by(RewardsGive.Created.desc()) \
        .all()
