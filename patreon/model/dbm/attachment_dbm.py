from patreon.model.table import Attachments


def find_by_id_list(attachment_ids):
    return Attachments.query \
        .filter(Attachments.ID.in_(attachment_ids))

def find_by_activity_id_list(activity_ids):
    return Attachments.query \
        .filter(Attachments.HID.in_(activity_ids))


def find_by_activity_id(activity_id):
    return Attachments.query \
        .filter(Attachments.HID == activity_id)


def find_by_activity_id_attachment_id_list(activity_id, attachment_ids):
    return Attachments.query \
        .filter(Attachments.HID == activity_id) \
        .filter(Attachments.ID.in_(attachment_ids))
