from patreon.model.dbm.base_dbm import BaseDBM
from patreon.model.table import HomepageFeaturedCampaign

class HomepageFeaturedCampaignDBM(BaseDBM):
    def __init__(self):
        super().__init__(HomepageFeaturedCampaign)

    def approved(self):
        return self.filter(self.table.approval_status == 1)

    def disapproved(self):
        return self.filter(self.table.approval_status == 0)

    def latest(self):
        return self.order_by('launched_at', descending=True)
