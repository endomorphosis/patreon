from sqlalchemy import func


class BaseDBM:
    def __init__(self, table, per_page=25):
        self.working_query = table.query
        self.table = table
        self.order_by_property = []
        self.group_by_property = None
        self.per_page = per_page
        self.limit_value = None
        self.offset_value = None

    def __iter__(self):
        return self.prepare_query().__iter__()

    # Group by and order by clauses always have to be applied last, so a query must be 'prepared' before use
    def prepare_query(self):
        prepared_query = self.working_query
        if self.order_by_property:
            prepared_query = prepared_query.order_by(*self.order_by_property)

        if self.group_by_property is not None:
            prepared_query = prepared_query.group_by(self.group_by_property)

        if self.offset_value is not None:
            prepared_query = prepared_query.offset(self.offset_value)

        if self.limit_value is not None:
            prepared_query = prepared_query.limit(self.limit_value)

        return prepared_query

    def filter(self, *args, **kwargs):
        self.working_query = self.working_query.filter(*args, **kwargs)
        return self

    @property
    def query(self):
        self.working_query = self.prepare_query()
        self.order_by_property = None
        self.group_by_property = None
        self.limit_value = None
        self.offset_value = None
        return self

    # We use the query property of tables, so to include sums and such we have to add entities to the result
    def add_function(self, function_name, field_name, label=None):
        label = label or function_name
        function = getattr(func, function_name)
        field = getattr(self.table, field_name)
        return self.with_entity(function(field).label(label))

    def with_entity(self, entity):
        self.working_query = self.working_query.with_entities(entity)
        return self

    def all(self):
        return self.prepare_query().all()

    def first(self):
        return self.prepare_query().first()

    def count(self):
        return self.prepare_query().count()

    def paged(self, page):
        self.offset((page - 1) * self.per_page)
        return self.limit(self.per_page)

    def order_by(self, property, descending=False):
        column = getattr(self.table, property)
        if descending:
            self.order_by_property.append(column.desc())
        else:
            self.order_by_property.append(column.asc())
        return self

    def group_by(self, property):
        self.group_by_property = getattr(self.table, property)
        return self

    def limit(self, limit):
        self.limit_value = limit
        return self

    def offset(self, offset):
        self.offset_value = offset
        return self
