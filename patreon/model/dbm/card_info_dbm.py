"""
model/card.py
"""

from patreon.services import db
from patreon.services.logging.unsorted import log_action
from patreon.constants.cards import card_types


def get_cards(uid):
    if uid is None: return None

    card_info = {}
    for row in db.query("""
            SELECT RID, Number, ExpirationDate, Type, Verified
            FROM tblCardInfo
            WHERE UID = %s
            ORDER BY Created DESC
            """,
            [uid]):

        # TODO(postport): Remove this entry smudging.
        row['TypeNum'] = int(row['Type'])
        row['Type'] = card_types.get(int(row['Type']), 'Card')
        row['Number'] = str(row['Number']).zfill(4)
        card_info[row['RID']] = row

    return card_info


def save_card(user_id, card):
    card_id = card['RID']
    name = card.get('Name')
    country = card.get('Country')
    street = card.get('Street')
    street2 = card.get('Street2')
    city = card.get('City')
    state = card.get('State')
    zip_ = card.get('Zip')
    expiration_date = card.get('ExpirationDate')
    number = card['Number']
    type_ = card['Type']

    log_action('save-card', card=card)

    db.query("""
        INSERT IGNORE INTO tblCardInfo
            (RID, UID, Name, Street, Street2, City, State, Zip, ExpirationDate, Number, Type, Created, Country)
        VALUES
            (%s,       %s,      %s,   %s,     %s,      %s,   %s,    %s,   %s,              %s,     %s,   NOW(),      %s)
        """, [card_id, user_id, name, street, street2, city, state, zip_, expiration_date, number, type_, country])
