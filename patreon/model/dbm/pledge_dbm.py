import datetime

from sqlalchemy import func
from patreon import globalvars
from patreon.model.table import Pledge
from patreon.services.db import db_session


def get(patron_id=None, creator_id=None):
    if creator_id is None or patron_id is None:
        return None
    return Pledge.get(patron_id, creator_id)


def find_by_creator_id(creator_id):
    return Pledge.query \
        .filter(
            Pledge.CID == creator_id,
            Pledge.Invalid.is_(None)
        )


def find_all_by_creator_id(creator_id):
    return Pledge.query.filter(Pledge.CID == creator_id)


def find_by_creator_id_and_above_cents(creator_id, cents):
    return find_by_creator_id(creator_id) \
        .filter(Pledge.Pledge >= cents)


def find_days_supported_by_creator_id(creator_id):
    pledges = db_session.query(Pledge.UID, Pledge.Created) \
        .filter(
            Pledge.CID == creator_id,
            Pledge.Invalid.is_(None)
        ).all()

    now = datetime.datetime.now()
    return {
        pledge.UID: (now - pledge.Created).days
        for pledge in pledges
    }


def find_by_patron_ids(patron_ids):
    return Pledge.query.filter(Pledge.UID.in_(patron_ids))


def find_invalid_by_patron_id(patron_id):
    return db_session.query(Pledge.CID) \
        .filter(
            Pledge.UID == patron_id,
            Pledge.Invalid.isnot(None)
        )


def find_top_patrons_by_creator_id(creator_id):
    result = Pledge.query \
        .filter(
            Pledge.CID == creator_id,
            Pledge.Invalid.is_(None),
            Pledge.is_private != True
        ) \
        .order_by(Pledge.Pledge.desc()) \
        .limit(20)
    if result is None:
        return []
    return [ pledge.patron_id for pledge in result ]


def find_by_creator_id_paginated(creator_id, page=1):
    offset = (page - 1) * globalvars.get('SHARES_PER_PAGE')
    limit = globalvars.get('SHARES_PER_PAGE')
    return Pledge.query \
        .filter(
            Pledge.CID == creator_id,
            Pledge.Invalid.is_(None),
            Pledge.is_private != True
        ) \
        .order_by(Pledge.Created.desc()) \
        .limit(limit).offset(offset)


def get_patron_count_by_creator_id(creator_id):
    return Pledge.query\
        .filter_by(CID=creator_id)\
        .count()


def get_patron_count_by_creator_ids(creator_ids):
    return db_session.query(Pledge.CID, func.count().label('count')) \
        .filter(Pledge.CID.in_(creator_ids))\
        .group_by(Pledge.CID)


def get_pledge_total_by_creator_id(creator_ids):
    return db_session.query(Pledge.CID, func.sum(Pledge.Pledge).label('sum')) \
        .filter(Pledge.CID.in_(creator_ids)) \
        .group_by(Pledge.CID) \
        .all()


def get_pledge_total_by_patron_id(patron_ids):
    return db_session.query(Pledge.UID, func.sum(Pledge.Pledge).label('sum')) \
        .filter(Pledge.UID.in_(patron_ids)) \
        .group_by(Pledge.UID) \
        .all()


def get_invalid_card_ids(user_id):
    bad_cards = Pledge.query \
        .filter_by(UID=user_id) \
        .filter(Pledge.Invalid != None)

    # list(set(list())) dedupes the central list
    return list(set([card.CardID for card in bad_cards]))


def find_suggested_by_creator_id(creator_id):
    subquery = db_session.query(Pledge.UID).filter(Pledge.CID == creator_id)
    return Pledge.query \
        .filter(Pledge.UID.in_(subquery)) \
        .group_by(Pledge.CID) \
        .order_by(func.count(1).desc()) \
        .limit(7) \
        .all()


def move_pledges_from_card_to_card(user_id, old_card_id, new_card_id):
    pledges = Pledge.query.filter_by(CardID=old_card_id, UID=user_id)
    for pledge in pledges:
        pledge.update({'CardID': new_card_id, 'Invalid': None})


def invalidate_pledge(patron_id, creator_id, invalidated_at):
    return Pledge.query \
        .filter(Pledge.patron_id == patron_id) \
        .filter(Pledge.creator_id == creator_id) \
        .filter(Pledge.Invalid.is_(None)) \
        .update({"invalidated_at": invalidated_at})


def update_or_insert(values):
    pledge = Pledge.get(
        patron_id=values.get('UID'),
        creator_id=values.get('CID')
    )
    if pledge:
        values.pop('UID')
        values.pop('CID')
        values.pop('Created')
        values['LastUpdated'] = datetime.datetime.now()
        pledge.update(values)
    else:
        Pledge.insert(values)


def get_recent_pledges_from_all_users(count=20):
    return Pledge.query \
        .order_by(
            Pledge.Created.desc()
        ).limit(count)

