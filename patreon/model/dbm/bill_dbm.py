from patreon.constants import MemCachedKey
from patreon.model.table import Bills
from patreon.services.caching import memcache
import datetime


def get_bills_for_campaign(campaign_id):
    return Bills.query\
        .filter(Bills.campaign_id == campaign_id)\
        .all()


def get_bills_for_post(post_id):
    return Bills.query\
        .filter(Bills.post_id == post_id)\
        .all()


def get_bills_for_patron(user_id):
    return Bills.query\
        .filter(Bills.user_id == user_id)\
        .all()


def create_bill(user_id, campaign_id, pledge_id, vat_charge_id, amount_cents,
                bill_type, post_id, billing_cycle, created_at, attempted_at,
                failed_at, succeeded_at):
    # Returns the bill_id
    return Bills.insert({
        'user_id': user_id,
        'campaign_id': campaign_id,
        'pledge_id': pledge_id,
        'vat_charge_id': vat_charge_id,
        'amount_cents': amount_cents,
        'type': bill_type,
        'post_id': post_id,
        'billing_cycle': billing_cycle,
        'created_at': created_at,
        'attempted_at': attempted_at,
        'failed_at': failed_at,
        'succeeded_at': succeeded_at
    })[0]


def delete_by_user_id_campaign_id(user_id, campaign_id):
    Bills.query\
        .filter_by(user_id=user_id, campaign_id=campaign_id)\
        .update({"deleted_at": datetime.datetime.now()})


def delete_by_post_id(post_id):
    Bills.query\
        .filter_by(post_id=post_id)\
        .update({"deleted_at": datetime.datetime.now()})


def clear_post_cache(post_id):
    memcache.delete(MemCachedKey.SHARE.value + str(post_id))
    memcache.delete(MemCachedKey.SHARE_PATRONS.value + str(post_id))
