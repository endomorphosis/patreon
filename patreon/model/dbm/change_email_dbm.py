from patreon.model.table import ChangeEmail


def find_by_user_id(user_id):
    return ChangeEmail.query.filter_by(UID=user_id).all()
