from patreon import globalvars
from patreon.model.table import Payout
from patreon.services.db import db_session
from sqlalchemy import func


def find_payout_sum(user_id):
    return db_session.query(func.sum(Payout.Amount).label('Payments')).filter(Payout.UID == user_id) \
        .filter((Payout.Type != globalvars.get('PAYOUT_DD_FAILED')) & (Payout.Type != globalvars.get('PAYOUT_CREDIT'))) \
        .first()


def find_credit_sums(user_id):
    return db_session.query(func.sum(Payout.Amount).label('Credit'), func.sum(Payout.CreditUsed).label('CreditUsed')) \
        .filter(Payout.UID == user_id).filter(Payout.Type == globalvars.get('PAYOUT_CREDIT')).first()

def get_all_payouts_for_creator(user_id):
    return db_session.query(
            Payout.Created,
            Payout.Amount,
            Payout.Fee,
            Payout.Type)\
        .filter(Payout.UID == user_id)\
        .order_by(Payout.Created.desc())\
        .all()

