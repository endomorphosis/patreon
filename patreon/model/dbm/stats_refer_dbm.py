from patreon.model.table import StatsRefer
from patreon.services.db import db_session
from sqlalchemy import func


def find_by_user_id(user_id):
    return StatsRefer.filter_by(UID=user_id).all()


def find_by_creator_id(creator_id):
    return StatsRefer.filter_by(CID=creator_id).all()


def find_by_activity_id(activity_id):
    return StatsRefer.filter_by(HID=activity_id).all()


def get_count_by_user_id_and_creator_id(self):
    return db_session.query(func.sum(StatsRefer.Count).label('count')) \
        .filter_by(UID=self.UID, CID=self.CID) \
        .group_by(StatsRefer.CID) \
        .first()
