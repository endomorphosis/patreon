import patreon
from patreon.model.table import UserLocation
from patreon.services import db
from patreon.services.db import db_session


def find_by_user_id(user_id):
    return db_session.query(UserLocation.user_id, UserLocation.display_name).filter_by(user_id=user_id)


# these methods are explicit exceptions to the raw sql rule because sqlalchemy + circle dies with location columns
def insert(user_id, latitude, longitude, location_name):
    latitude = float(latitude)
    longitude = float(longitude)
    db.query("""
         INSERT IGNORE INTO user_locations(user_id, lat_long, display_name)
         VALUES(%s, GeomFromText('POINT(%s %s)'), %s)
         """, [user_id, longitude, latitude, location_name])
    if patreon.is_test_environment():
        patreon.services.db.test_dirty_table.add(UserLocation)


def delete_by_location(user_id, latitude, longitude):
    latitude = float(latitude)
    longitude = float(longitude)
    db.query("""
          DELETE FROM user_locations
          WHERE user_id = %s AND lat_long = GeomFromText('POINT(%s %s)')
         """, [user_id, longitude, latitude])


def find_by_user_id_legacy(user_id):
    return db.query("""
          SELECT X(lat_long) as latitude, Y(lat_long) as longitude, display_name
          FROM user_locations WHERE user_id = '%s'
         """, [user_id])
