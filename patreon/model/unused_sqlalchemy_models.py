# coding: utf-8
# pragma: no cover
from patreon.services import db
from patreon.services.db import PatreonModel
from sqlalchemy import BigInteger, Column, Date, DateTime, Float, Index, \
    Integer, SmallInteger, String, Table, Text, text
from sqlalchemy.ext.declarative import declarative_base

metadata = declarative_base(cls=PatreonModel).metadata


class ChargeRefund(db.Model):
    __tablename__ = 'charge_refunds'

    charge_refund_id = Column(BigInteger, primary_key=True)
    txn_id = Column(BigInteger, nullable=False, index=True)
    charge_id = Column(BigInteger)
    payment_instrument_id = Column(BigInteger, nullable=False)
    type = Column(String(64))
    external_id = Column(String(255), index=True)
    amount_cents = Column(Integer, nullable=False)
    fee_cents = Column(Integer, nullable=False, server_default=text("'0'"))
    created_at = Column(DateTime, nullable=False)
    processed_at = Column(DateTime)
    failed_at = Column(DateTime)



class CreatorSetting(db.Model):
    __tablename__ = 'creator_settings'

    user_id = Column(Integer, primary_key=True)
    payout_method = Column(String(32), nullable=False)
    is_auto_pay = Column(Integer, nullable=False, server_default=text("'0'"))


class Email(db.Model):
    __tablename__ = 'emails'

    email = Column(String(128), primary_key=True, nullable=False)
    user_id = Column(Integer, primary_key=True, nullable=False)
    created_at = Column(DateTime, nullable=False)
    active_at = Column(DateTime)
    verified_at = Column(DateTime)
    activation_hash = Column(String(128), nullable=False)


class EscrowHold(db.Model):
    __tablename__ = 'escrow_holds'

    escrow_hold_id = Column(BigInteger, primary_key=True)
    txn_id = Column(BigInteger)
    type = Column(String(64), nullable=False)
    user_id = Column(Integer, nullable=False)
    amount_cents = Column(Integer, nullable=False)
    fee_cents = Column(Integer, nullable=False)
    created_at = Column(DateTime)
    processed_at = Column(DateTime)
    failed_at = Column(DateTime)


class EscrowRelease(db.Model):
    __tablename__ = 'escrow_releases'

    escrow_release_id = Column(BigInteger, primary_key=True)
    txn_id = Column(BigInteger)
    type = Column(String(64), nullable=False)
    user_id = Column(Integer, nullable=False)
    amount_cents = Column(Integer, nullable=False)
    fee_cents = Column(Integer, nullable=False)
    created_at = Column(DateTime)
    processed_at = Column(DateTime)
    failed_at = Column(DateTime)





class MailQueue(db.Model):
    __tablename__ = 'mail_queue'

    id = Column(BigInteger, primary_key=True, index=True, server_default=text("'0'"))
    create_time = Column(DateTime, nullable=False, server_default=text("'0000-00-00 00:00:00'"))
    time_to_send = Column(DateTime, nullable=False, index=True, server_default=text("'0000-00-00 00:00:00'"))
    sent_time = Column(DateTime)
    id_user = Column(BigInteger, nullable=False, index=True, server_default=text("'0'"))
    ip = Column(String(20), nullable=False, server_default=text("'unknown'"))
    sender = Column(String(50), nullable=False, server_default=text("''"))
    recipient = Column(Text, nullable=False)
    headers = Column(Text, nullable=False)
    body = Column(String, nullable=False)
    try_sent = Column(Integer, nullable=False, server_default=text("'0'"))
    delete_after_send = Column(Integer, nullable=False, server_default=text("'1'"))


class MailQueueSeq(db.Model):
    __tablename__ = 'mail_queue_seq'

    sequence = Column(Integer, primary_key=True)


class MonthlySnapshot(db.Model):
    __tablename__ = 'monthlySnapshot'

    UID = Column(Integer, primary_key=True, nullable=False)
    Type = Column(Integer, nullable=False)
    Count = Column(Integer, nullable=False)
    Created = Column(Date, primary_key=True, nullable=False)


class OauthAccessToken(db.Model):
    __tablename__ = 'oauth_access_tokens'

    id = Column(Integer, primary_key=True)
    access_token = Column(String(255), unique=True)
    client_id = Column(String(255))
    user_id = Column(String(255))
    expires = Column(DateTime)
    scope = Column(String(255))


class OauthAuthorizationCode(db.Model):
    __tablename__ = 'oauth_authorization_codes'

    id = Column(Integer, primary_key=True)
    authorization_code = Column(String(255), unique=True)
    client_id = Column(String(255))
    user_id = Column(Integer)
    redirect_uri = Column(String(255))
    expires = Column(DateTime)
    scope = Column(String(255))


class OauthClient(db.Model):
    __tablename__ = 'oauth_clients'

    id = Column(Integer, primary_key=True)
    client_id = Column(String(255), nullable=False, unique=True)
    client_secret = Column(String(255))
    redirect_uri = Column(String(255))
    grant_types = Column(String(255))
    scope = Column(String(255))
    user_id = Column(String(255))
    public_key = Column(String(255))


class PaymentInstrument(db.Model):
    __tablename__ = 'payment_instruments'

    payment_instrument_id = Column(BigInteger, primary_key=True)
    type = Column(String(64), nullable=False)
    external_id = Column(String(1024))
    user_id = Column(BigInteger, nullable=False, index=True)
    created_at = Column(DateTime, nullable=False)
    verified_at = Column(DateTime)
    deleted_at = Column(DateTime)


class PaymentRefund(db.Model):
    __tablename__ = 'payment_refunds'

    payment_refund_id = Column(BigInteger, primary_key=True)
    txn_id = Column(BigInteger, nullable=False, index=True)
    payment_id = Column(BigInteger, nullable=False, index=True)
    amount_cents = Column(Integer, nullable=False)
    external_fee_cents = Column(Integer, nullable=False, server_default=text("'0'"))
    patreon_fee_cents = Column(Integer, nullable=False, server_default=text("'0'"))
    created_at = Column(DateTime, nullable=False)
    processed_at = Column(DateTime)
    failed_at = Column(DateTime)


class PaymentRefundsForPledgeBill(db.Model):
    __tablename__ = 'payment_refunds_for_pledge_bills'

    payment_refund_id = Column(BigInteger, primary_key=True, nullable=False)
    pledge_bill_id = Column(BigInteger, primary_key=True, nullable=False, index=True)


class Payment(db.Model):
    __tablename__ = 'payments'

    payment_id = Column(BigInteger, primary_key=True)
    txn_id = Column(BigInteger, nullable=False, index=True)
    type = Column(String(64))
    patron_id = Column(Integer, nullable=False)
    creator_id = Column(Integer, nullable=False)
    amount_cents = Column(Integer, nullable=False)
    external_fee_cents = Column(Integer, nullable=False, server_default=text("'0'"))
    patreon_fee_cents = Column(Integer, nullable=False, server_default=text("'0'"))
    created_at = Column(DateTime, nullable=False)
    processed_at = Column(DateTime)
    failed_at = Column(DateTime)


class PaymentsForPledgeBill(db.Model):
    __tablename__ = 'payments_for_pledge_bills'

    payment_id = Column(BigInteger, primary_key=True, nullable=False)
    pledge_bill_id = Column(BigInteger, primary_key=True, nullable=False, index=True)


class PayoutFailure(db.Model):
    __tablename__ = 'payout_failures'

    payout_failure_id = Column(BigInteger, primary_key=True)
    txn_id = Column(BigInteger, index=True)
    payout_id = Column(BigInteger, nullable=False, index=True)
    external_id = Column(String(255), index=True)
    created_at = Column(DateTime, nullable=False)


class Payout(db.Model):
    __tablename__ = 'payouts'

    payout_id = Column(BigInteger, primary_key=True)
    txn_id = Column(BigInteger)
    payment_instrument_id = Column(BigInteger, nullable=False)
    type = Column(String(64))
    external_id = Column(String(1024))
    amount_cents = Column(Integer, nullable=False)
    fee_cents = Column(Integer, nullable=False, server_default=text("'0'"))
    created_at = Column(DateTime, nullable=False)
    processed_at = Column(DateTime)
    failed_at = Column(DateTime)

class PopularCreation(db.Model):
    __tablename__ = 'popular_creations'

    category = Column(Integer, primary_key=True, nullable=False, server_default=text("'0'"))
    rank = Column(Integer, primary_key=True, nullable=False)
    creation_id = Column(Integer, nullable=False)
    score = Column(Float(asdecimal=True))


class SchemaVersion(db.Model):
    __tablename__ = 'schema_version'

    version_rank = Column(Integer, nullable=False, index=True)
    installed_rank = Column(Integer, nullable=False, index=True)
    version = Column(String(50), primary_key=True)
    description = Column(String(200), nullable=False)
    type = Column(String(20), nullable=False)
    script = Column(String(1000), nullable=False)
    checksum = Column(Integer)
    installed_by = Column(String(100), nullable=False)
    installed_on = Column(DateTime, nullable=False, server_default=text("CURRENT_TIMESTAMP"))
    execution_time = Column(Integer, nullable=False)
    success = Column(Integer, nullable=False, index=True)


class TblAPIKey(db.Model):
    __tablename__ = 'tblAPIKeys'

    UID = Column(Integer, primary_key=True, nullable=False)
    APIKey = Column(String(64), primary_key=True, nullable=False)
    Created = Column(DateTime, nullable=False)


class TblAPIToken(db.Model):
    __tablename__ = 'tblAPITokens'

    UID = Column(Integer, primary_key=True, nullable=False)
    Token = Column(String(32), primary_key=True, nullable=False)
    Created = Column(DateTime, nullable=False, server_default=text("CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP"))


class TblAlert(db.Model):
    __tablename__ = 'tblAlerts'

    AlertID = Column(Integer, primary_key=True)
    AlertType = Column(Integer)
    Begins = Column(DateTime)
    Expires = Column(DateTime)
    CustomText = Column(Text)


t_tblAlertsDismissed = Table(
    'tblAlertsDismissed', metadata,
    Column('AlertID', Integer),
    Column('UID', Integer),
    Index('AlertID', 'AlertID', 'UID')
)


class TblAlreadyPaid(db.Model):
    __tablename__ = 'tblAlreadyPaid'

    ID = Column(Integer, primary_key=True)


t_tblBackupPledges = Table(
    'tblBackupPledges', metadata,
    Column('CID', Integer),
    Column('UID', Integer),
    Column('Pledge', Integer),
    Column('MaxAmount', Integer),
    Column('RID', Integer),
    Column('Invalid', DateTime),
    Column('Created', DateTime),
    Column('CardID', String(32)),
    Column('LastUpdated', DateTime),
    Column('Street', String(128)),
    Column('Street2', String(128)),
    Column('City', String(64)),
    Column('State', String(32)),
    Column('Zip', String(32)),
    Column('Country', String(32))
)


class TblBackupUser(db.Model):
    __tablename__ = 'tblBackupUsers'

    UID = Column(Integer, primary_key=True)
    Email = Column(String(128))
    FName = Column(String(64))
    LName = Column(String(64))
    Password = Column(String(64))
    Created = Column(DateTime)
    FBID = Column(BigInteger)
    ImageUrl = Column(String(128))
    ThumbUrl = Column(String(128))
    Gender = Column(Integer, server_default=text("'0'"))
    Status = Column(Integer, server_default=text("'0'"))
    Vanity = Column(String(64))
    About = Column(Text)
    IsCreator = Column(Integer, server_default=text("'0'"))
    CreatorImageUrl = Column(String(128))
    CreatorOneLine = Column(String(255))
    CreatorCreate = Column(String(64))
    CreatorCreateSingle = Column(String(64))
    CreatorImageSmallUrl = Column(String(128))
    CreatorVideoUrl = Column(String(128))
    CreatorEmbed = Column(String(255))
    Youtube = Column(String(64))
    Twitter = Column(String(64))
    Facebook = Column(String(64))
    CreatorAbout = Column(Text)


class TblChargeDetail(db.Model):
    __tablename__ = 'tblChargeDetails'

    ChargeDetailsID = Column(Integer, primary_key=True)
    ChargeID = Column(String(128), index=True)
    Amount = Column(Integer)
    Fee = Column(Integer)


class TblCharge(db.Model):
    __tablename__ = 'tblCharges'

    CID = Column(String(32), primary_key=True)
    UID = Column(Integer, nullable=False)
    SID = Column(Integer, nullable=False)
    Amount = Column(Integer, nullable=False)
    Fee = Column(Integer, nullable=False)
    Created = Column(DateTime, nullable=False)





class TblChurnTest(db.Model):
    __tablename__ = 'tblChurnTest'

    UID = Column(Integer, primary_key=True, nullable=False)
    CID = Column(Integer, primary_key=True, nullable=False)
    LastCreated = Column(DateTime)
    LastDeleted = Column(DateTime)


class TblCredit(db.Model):
    __tablename__ = 'tblCredits'

    CID = Column(Integer, primary_key=True)
    UID = Column(Integer, nullable=False)
    SID = Column(Integer)
    Amount = Column(Integer, nullable=False)
    Fee = Column(Integer, nullable=False, server_default=text("'0'"))
    Created = Column(DateTime, nullable=False)
    PatreonFee = Column(Integer, nullable=False)
    Type = Column(Integer, nullable=False, server_default=text("'1'"))


class TblDatum(db.Model):
    __tablename__ = 'tblData'

    ID = Column(Integer, primary_key=True, nullable=False)
    type = Column(Integer, primary_key=True, nullable=False)
    data = Column(Integer)


class TblExternalToken(db.Model):
    __tablename__ = 'tblExternalTokens'

    UID = Column(Integer, primary_key=True, nullable=False)
    ExternalType = Column(Integer, primary_key=True, nullable=False)
    Created = Column(DateTime, nullable=False)
    Token = Column(String(128), nullable=False)


class TblLoginToken(db.Model):
    __tablename__ = 'tblLoginTokens'

    UID = Column(Integer, primary_key=True, nullable=False)
    Series = Column(String(32), primary_key=True, nullable=False)
    Token = Column(String(32), nullable=False)
    Updated = Column(DateTime, nullable=False, server_default=text("CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP"))


class TblNew(db.Model):
    __tablename__ = 'tblNews'

    NID = Column(Integer, primary_key=True)
    Type = Column(SmallInteger, nullable=False)
    UID = Column(Integer, nullable=False)
    OtherUID = Column(Integer)
    HID = Column(Integer)
    SID = Column(Integer)
    Created = Column(DateTime, nullable=False)
    Viewed = Column(Integer, nullable=False, server_default=text("'0'"))


class TblPayRequested(db.Model):
    __tablename__ = 'tblPayRequested'

    UID = Column(Integer, primary_key=True)
    Created = Column(DateTime)


class TblSearchText(db.Model):
    __tablename__ = 'tblSearchText'

    UID = Column(Integer, primary_key=True)
    SearchText = Column(String(256), nullable=False, index=True)
    Score = Column(Integer, nullable=False, index=True)


class TblShare(db.Model):
    __tablename__ = 'tblShares'
    __table_args__ = (
        Index('tblShares_IsContent_Privacy_LikeCount_CommentCount', 'IsContent', 'Privacy', 'LikeCount',
              'CommentCount'),
        Index('tblShares_Privacy_LikeCount_CommentCount', 'Privacy', 'LikeCount', 'CommentCount')
    )

    HID = Column(Integer, primary_key=True)
    UID = Column(Integer, nullable=False, index=True)
    SID = Column(Integer, nullable=False, index=True, server_default=text("'0'"))
    Type = Column(Integer, nullable=False, index=True)
    Title = Column(String(512))
    Content = Column(Text)
    Created = Column(DateTime, nullable=False, index=True)
    CommentCount = Column(SmallInteger, nullable=False, index=True, server_default=text("'0'"))
    Privacy = Column(Integer, nullable=False, index=True, server_default=text("'0'"))
    LikeCount = Column(SmallInteger, nullable=False, index=True, server_default=text("'0'"))
    ImageUrl = Column(String(255))
    ImageLargeUrl = Column(String(255))
    ImageThumbUrl = Column(String(255))
    ImageHeight = Column(SmallInteger, nullable=False, server_default=text("'0'"))
    ImageWidth = Column(SmallInteger, nullable=False, server_default=text("'0'"))
    Url = Column(String(255))
    Domain = Column(String(128))
    Subject = Column(String(255))
    Description = Column(Text)
    Embed = Column(String(1024))
    NewContent = Column(Integer, nullable=False, server_default=text("'0'"))
    IsContent = Column(Integer, nullable=False, server_default=text("'0'"))
    SnapshotPledge = Column(Integer, nullable=False, server_default=text("'0'"))
    published_at = Column(DateTime)


class TblUsersExternal(db.Model):
    __tablename__ = 'tblUsersExternal'

    UID = Column(Integer, primary_key=True, nullable=False)
    Type = Column(Integer, primary_key=True, nullable=False)
    ExternalID = Column(String(32), nullable=False)
    Access = Column(String(256), nullable=False, index=True)
    LastUpdated = Column(Integer, nullable=False, server_default=text("'0'"))
    LastItem = Column(String(128))
    Active = Column(Integer, nullable=False, server_default=text("'0'"))


class TblUsersExtra(db.Model):
    __tablename__ = 'tblUsersExtra'

    UID = Column(Integer, primary_key=True)
    LastNewsUpdate = Column(DateTime, nullable=False, server_default=text("'2013-03-05 00:00:00'"))
    TaxName = Column(String(128))
    TaxTIN = Column(String(128))
    NewMsgs = Column(Integer, nullable=False, server_default=text("'0'"))
    PaypalEmail = Column(String(128))
    PayStreet = Column(String(128))
    PayStreet2 = Column(String(128))
    PayCity = Column(String(64))
    PayState = Column(String(32))
    PayZip = Column(String(32))
    PayCountry = Column(String(32))
    FlagYoutube = Column(Integer, nullable=False, server_default=text("'0'"))
    StripeRecipientID = Column(String(128))
    IsUS = Column(Integer, nullable=False, server_default=text("'0'"))
    StripeLastFour = Column(SmallInteger)
    StripeName = Column(String(128))
    IsCorp = Column(Integer, nullable=False, server_default=text("'0'"))
    AutoPay = Column(Integer, nullable=False, server_default=text("'0'"))
    PayMethod = Column(Integer, nullable=False, server_default=text("'0'"))
    PayoneerSetup = Column(Integer, nullable=False, server_default=text("'0'"))


class TblYoutubeImport(db.Model):
    __tablename__ = 'tblYoutubeImport'
    __table_args__ = (
        Index('tblYoutubeImport_UIDVideoID', 'UID', 'VideoID', unique=True),
    )

    ID = Column(Integer, primary_key=True)
    UID = Column(Integer, nullable=False)
    Title = Column(String(512))
    Content = Column(Text)
    Url = Column(String(255))
    ImageUrl = Column(String(255))
    Created = Column(DateTime)
    VideoID = Column(String(16), nullable=False, server_default=text("''"))
