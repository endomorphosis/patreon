from enum import Enum


class AlertType(Enum):
    SETUP         = 1
    DONE          = 2
    POST          = 3
    DELETE_PLEDGE = 10
    INVALID       = 11
