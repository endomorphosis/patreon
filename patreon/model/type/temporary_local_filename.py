import os


class TemporaryLocalFilename(object):
    """
    A wrapper for filenames so that they can be used in with statements.
    """
    def __init__(self, filename):
        self.filename = filename

    def __enter__(self):
        return self.filename

    def __exit__(self, exc_type, exc_val, exc_tb):
        os.remove(self.filename)
