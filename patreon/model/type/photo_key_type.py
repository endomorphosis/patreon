
class PhotoKey(object):
    def __init__(self, bucket, key, extension):
        self.bucket = bucket
        self.key = key
        self.extension = extension

    def image_url(self, image_size):
        return get_image_url(self.bucket, self.key, image_size, self.extension)


def _get_url(bucket, name):
    return "//{endpoint}.amazonaws.com/{bucket_name}/{file_name}".format(
        endpoint=bucket.endpoint, bucket_name=bucket.bucket, file_name=name
    )


def get_image_url(bucket, key, image_size, extension='.jpeg'):
    return _get_url(bucket, key + '_' + image_size.name + extension)


def get_file_url(bucket, key, extension):
    return _get_url(bucket, key + extension)
