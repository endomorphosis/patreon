from patreon.util.random import random_key


class TemporaryUploadCollection(object):
    """
    This interface is for when multiple files will be uploaded to the same key.
    """

    def __init__(self, resizer, destination_bucket):
        destination_format = resizer.destination_format.lower()

        self.key = random_key()
        self.members = [
            TemporaryUpload(
                source_filename,
                "{0}_{1}.{2}".format(self.key, name, destination_format),
                destination_bucket
            )
            for name, source_filename in resizer.resizes().items()
        ]

    def __iter__(self):
        return iter(self.members)


class TemporaryUpload(object):
    """
    This is an interface used in three places:
    1. MultiResizer
    2. Attachments
    3. Post files

    source_filename: the tempfile on disk that I'll be uploading.
    destination_filename: {key}_{name}.{format}, the key that will be accessible.
    destination_bucket: the bucket that the file will live on.
    """

    def __init__(self, source_filename, destination_filename, destination_bucket):
        self.source_filename = source_filename
        self.destination_filename = destination_filename
        self.destination_bucket = destination_bucket




