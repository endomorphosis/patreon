from enum import Enum


class SortType(Enum):
    POPULAR         = 1
    LATEST          = 2
    FOLLOW          = 3
    CONTENT_POPULAR = 4
    CONTENT_LATEST  = 5
