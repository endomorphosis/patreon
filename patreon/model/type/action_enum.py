from enum import Enum


class ActionType(Enum):
    LIKE    = 1
    UPLOAD  = 2
    COMMENT = 3
