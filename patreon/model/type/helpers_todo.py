from sqlalchemy import TypeDecorator



class TypedColumnType(TypeDecorator):  # Type helper for SqlAlchemy ORM
    def __init__(self, impl, klass, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.klass = klass
        self.impl = impl

    def process_literal_param(self, value, dialect):
        if self.value:
            return self.value.toSQL()
        else:
            return None

    def process_bind_param(self, value, dialect):
        if value:
            return value.toSQL()
        else:
            return None

    def process_result_value(self, value, dialect):
        return self.klass.fromSQL(value)

    @property
    def python_type(self):
        return self.klass


class Optional:  # Type wrapper for optional types
    def __init__(self, klass):
        self.klass = klass

    def from_sql(self, value):
        if value:
            return self.klass.fromSQL(value)
        else:
            return None
