# TODO: make this a superclass of all the other notifications
# doing repeated work & indicating the abstract methods they are all required to implement
from enum import IntEnum


class NotificationSortOrder(IntEnum):
    PaymentsProcessingNotificationCreator   = -1
    PaymentsProcessingNotificationPatron    = -2
    MonthlyPledgesNotification              = -3
    DailyCommentsFromMyCreatorsNotification = -4
    DailyCommentsFromMyPatronsNotification  = -5
    DailyCommentsFromOtherUsersNotification = -6
    DailyLikesNotification                  = -7
