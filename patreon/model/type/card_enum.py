# TODO MARCOS Make EmailTarget an Enum.


class CardType:
    def __init__(self, value, display_name):
        self.value = value
        self.display_name = display_name

    @staticmethod
    def from_id(card_id):
        return {
            1: CardType.VISA,
            2: CardType.MASTERCARD,
            3: CardType.DISCOVER,
            4: CardType.DINERS,
            5: CardType.JCB,
            6: CardType.AMERICAN_EXPRESS,
            10: CardType.PAYPAL,
            99: CardType.OTHER
        }[int(card_id)]

CardType.VISA             = CardType(1, 'Visa')
CardType.MASTERCARD       = CardType(2, 'MasterCard')
CardType.DISCOVER         = CardType(3, 'Discover')
CardType.DINERS           = CardType(4, 'Diners Club')
CardType.JCB              = CardType(5, 'JCB')
CardType.AMERICAN_EXPRESS = CardType(6, 'American Express')
CardType.PAYPAL           = CardType(10, 'PayPal')
CardType.OTHER            = CardType(99, 'Card')
