from enum import Enum
import patreon

class S3Bucket(Enum):
    #                  endpoint         bucket               region
    PATREON         = ("s3",            "patreon",          'us-east-1')
    PROFILE_PHOTO   = ("s3-us-west-1",  "patreon.user",     'us-west-1')
    POST            = ("s3-us-west-1",  "patreon.posts",    'us-west-1')
    COMMENT         = ("s3-us-west-1",  "patreon.comment",  'us-west-1')
    TEST            = ("s3-us-west-1",  "patreon.posts",     'us-west-1')

    @property
    def endpoint(self):
        return self.value[0]

    @property
    def bucket(self):
        return self.value[1]

    @property
    def region(self):
        return self.value[2]


def _get_bucket(bucket_name):
    return {
        'patreon': S3Bucket.PATREON,
        'profile_photo': S3Bucket.PROFILE_PHOTO,
        'post': S3Bucket.POST,
        'comment': S3Bucket.COMMENT,
        'test': S3Bucket.TEST
    }.get(bucket_name)

config_bucket_post = _get_bucket(patreon.config.bucket_post)
config_bucket_user = _get_bucket(patreon.config.bucket_user)
config_bucket_campaign = _get_bucket(patreon.config.bucket_user)
config_bucket_comment = _get_bucket(patreon.config.bucket_comment)
