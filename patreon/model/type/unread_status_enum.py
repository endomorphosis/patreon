from enum import Enum

class UnreadStatusType(Enum):
    notifications = 1
    creations = 2
