from enum import Enum


class BillType(Enum):
    monthly     = 'monthly'
    per_post    = 'per_post'
    other       = 'other'
