
class Address(object):
    def __init__(self, address_line_1, address_line_2, address_zip, city, state,
                 country):
        self.address_line_1 = address_line_1
        self.address_line_2 = address_line_2
        self.address_zip = address_zip

        self.city = city
        self.state = state
        self.country = country

    @property
    def street(self):
        return " ".join(filter(None, [self.address_line_1, self.address_line_2]))

    def as_dict(self):
        return {
            "address_line_1": self.address_line_1,
            "address_line_2": self.address_line_2,
            "address_zip": self.address_zip,
            "city": self.city,
            "state": self.state,
            "country": self.country
        }

    def has_all_required_fields(self):
        if not self.country:
            return False
        if self.country == "US":
            return self.address_line_1 and self.address_zip and self.city and self.state
        return True
