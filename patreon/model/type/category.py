"""
This is not a table yet, but I want to expose it as if it were.
For now, it is going to be an enumeration
"""

from enum import Enum


class Category(Enum):
    #                          id   name                    color       offset  is_user_category
    FRONT_PAGE              = (0,   'Front Page',           '#ffffff',  391,    False)
    VIDEO_AND_FILM          = (1,   'Video & Film',         '#d73437',  0,      True)
    MUSIC                   = (2,   'Music',                '#116ca7',  28,     True)
    WRITING                 = (3,   'Writing',              '#eb652c',  56,     True)
    COMICS                  = (4,   'Comics',               '#682671',  84,     True)
    DRAWING_AND_PAINTING    = (5,   'Drawing & Painting',   '#f5c723',  112,    True)
    ANIMATION               = (6,   'Animation',            '#e64518',  139,    True)
    PODCASTS                = (7,   'Podcasts',             '#6f75a7',  167,    True)
    GAMES                   = (8,   'Games',                '#1b3097',  195,    True)
    PHOTOS                  = (9,   'Photography',          '#aadbfc',  223,    True)
    COMEDY                  = (10,  'Comedy',               '#d60000',  251,    True)
    SCIENCE                 = (11,  'Science',              '#82a62f',  279,    True)
    EDUCATION               = (12,  'Education',            '#2f8660',  307,    True)
    CRAFTS_AND_DIY          = (13,  'Crafts & DIY',         '#80e7b3',  335,    True)
    DANCE_AND_THEATER       = (14,  'Dance & Theater',      '#8e6ec1',  363,    True)
    # OTHER                 = (15,  'Other',                '#ffffff',  391,    True)
    ALL                     = (99,  'All',                  '#ffffff',  391,    False)

    # def __init__(self, value, a):
    #     self.value = value[0]
    #     self.display_name = value[1]

    @property
    def category_id(self):
        return self.value[0]

    @property
    def name(self):
        return self.value[1]

    @property
    def color(self):
        return self.value[2]

    @property
    def offset(self):
        return self.value[3]

    @property
    def is_user_category(self):
        return self.value[4]

    @staticmethod
    def from_id(category_id):
        try:
            return {
                0: Category.FRONT_PAGE,
                1: Category.VIDEO_AND_FILM,
                2: Category.MUSIC,
                3: Category.WRITING,
                4: Category.COMICS,
                5: Category.DRAWING_AND_PAINTING,
                6: Category.ANIMATION,
                7: Category.PODCASTS,
                8: Category.GAMES,
                9: Category.PHOTOS,
                10: Category.COMEDY,
                11: Category.SCIENCE,
                12: Category.EDUCATION,
                13: Category.CRAFTS_AND_DIY,
                14: Category.DANCE_AND_THEATER,
                # 15: Category.OTHER,
                99: Category.ALL
            }.get(int(category_id), None)
        except (ValueError, TypeError):
            return None
