from enum import Enum
from patreon.exception.post_errors import BadPostType

# todo(postpost): migrate from integers to strings.


class PostType(Enum):
    IMAGE       = 1
    TEXT_ONLY   = 2
    LINK        = 3
    AUDIO_EMBED = 11
    AUDIO_FILE  = 12
    VIDEO_EMBED = 21
    VIDEO_FILE  = 22
    IMAGE_EMBED = 31
    IMAGE_FILE  = 32

    @staticmethod
    def get(post_type_raw):
        if isinstance(post_type_raw, int):
            return PostType.from_id(post_type_raw)
        elif isinstance(post_type_raw, str):
            return PostType.from_name(post_type_raw)
        raise BadPostType(post_type_raw)

    @staticmethod
    def from_id(share_type):
        return {
            1: PostType.IMAGE,
            2: PostType.TEXT_ONLY,
            3: PostType.LINK,
            11: PostType.AUDIO_EMBED,
            12: PostType.AUDIO_FILE,
            21: PostType.VIDEO_EMBED,
            22: PostType.VIDEO_FILE,
            31: PostType.IMAGE_EMBED,
            32: PostType.IMAGE_FILE
        }.get(share_type, None)

    @staticmethod
    def from_name(share_type):
        return {
            'image': PostType.IMAGE,
            'text_only': PostType.TEXT_ONLY,
            'link': PostType.LINK,
            'audio_embed': PostType.AUDIO_EMBED,
            'audio_file': PostType.AUDIO_FILE,
            'video_embed': PostType.VIDEO_EMBED,
            'video_file': PostType.VIDEO_FILE,
            'image_embed': PostType.IMAGE_EMBED,
            'image_file': PostType.IMAGE_FILE
        }.get(share_type, None)

UPLOAD_TYPES = [
    PostType.AUDIO_FILE,
    PostType.VIDEO_FILE,
    PostType.IMAGE_FILE
]

EMBED_TYPES = [
    PostType.LINK,
    PostType.AUDIO_EMBED,
    PostType.VIDEO_EMBED,
    PostType.IMAGE_EMBED
]

IMAGE_TYPES = [
    PostType.IMAGE,
    PostType.IMAGE_EMBED,
    PostType.IMAGE_FILE
]
