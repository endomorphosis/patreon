import abc


class Type(object):
    __metaclass__ = abc.ABCMeta

    def __init__(self, value):
        self._value = value

    @abc.abstractmethod
    def to_sql(self):
        """ :return: A string representation of this type. """
        return

    @classmethod
    @abc.abstractmethod
    def from_sql(cls, value):
        """ :return: Construct an instance of this type from a string. """
        return

    def __repr__(self):
        return repr(self._value)
