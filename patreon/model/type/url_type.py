from urllib.parse import urlsplit, urlunsplit


class URL(object):
    def __init__(self, value):
        self.value = value

    def get_host_string(self):
        return urlsplit(self.value).hostname

    def to_https(self):
        split = urlsplit(self.value).__dict__
        split["scheme"] = "https"
        return URL(urlunsplit(list(split.values())))


def https_string(url_string):
    split = urlsplit(url_string).__dict__
    split["scheme"] = "https"
    return urlunsplit(list(split.values()))
