"""
model/internal/darkmode.py
"""

import patreon

from patreon.services import db


def get_recent_responses(limit=50):
    return db.query_log("SELECT * FROM darkmode_response ORDER BY response_id DESC LIMIT %s", [limit])


def get_response(response_id):
    return db.query_log("SELECT * FROM darkmode_response WHERE response_id = %s", [response_id]).first()


def get_nearby_responses(response_id):
    """Returns the previous and the next response, probably."""
    responses = db.query_log("""
        SELECT * FROM darkmode_response
        WHERE response_id IN (%s, %s)
        """, [response_id-1, response_id+1])

    if len(responses) == 2:
        if responses[0]['response_id'] < response_id:
            return (responses[0], responses[1])
        else:
            return (responses[1], responses[0])
    else:
        previous_response = db.query_log("""
            SELECT * FROM darkmode_response
            WHERE response_id < %s AND response_id > %s
            ORDER BY response_id DESC
            LIMIT 1
            """, [response_id, response_id - 10]).first()
        next_response = db.query_log("""
            SELECT * FROM darkmode_response
            WHERE response_id > %s AND response_id < %s
            ORDER BY response_id ASC
            LIMIT 1
            """, [response_id, response_id + 10]).first()
        return (previous_response, next_response)


def get_response_html(response_id):
    row = db.query_log("""
        SELECT response_key FROM darkmode_response
        WHERE response_id = %s
        """, [response_id]).first()
    if row:
        bucket = patreon.services.aws.S3Bucket("patreon-darkmode-logs")
        html = bucket.get(row['response_key'])
        return html
    else:
        return '<h1>Error getting response HTML.</h1>'
