import patreon
from patreon.services import firebase

class DashCard(object):

    def __init__(self, firebase_key, default_title='Untitled', default_big_text=None):
        self.firebase_key = firebase_key
        existing = firebase.patreon_internal.child('dashCards').child(self.firebase_key).get()
        existing = existing or {} # Firebase turns empty dictionaries into 'null' objects
        self.title = existing.get('title', default_title)
        self.big_text = existing.get('bigText', default_big_text)
        self.small_text_rows = existing.get('smallTextRows', [])

    def set_title(self, title):
        self.title = title
        self._update_firebase()

    def set_big_text(self, big_text):
        self.big_text = big_text
        self._update_firebase()

    def set_small_text_rows(self, small_text_rows):
        self.small_text_rows = small_text_rows
        self._update_firebase()

    def _update_firebase(self):
        data = {
            'title': self.title,
            'bigText': self.big_text,
            'smallTextRows': self.small_text_rows
        }
        firebase.patreon_internal.child('dashCards').child(self.firebase_key).set(data)

    def to_dict(self):
        return {
            'prefix': self.firebase_key
        }


PaymentInProcessCard = DashCard('paymentsInProcess', default_title='In Process')
PaymentTotalCard = DashCard('paymentsTotal', default_title='Total')
PaymentPendingCard = DashCard('paymentsPending', default_title='Pending')
PaymentDeclinedCard = DashCard('paymentsDeclined', default_title='Declined')
PaymentSuccessfulCard = DashCard('paymentsSuccessful', default_title='Successful')

CRPledgeAddedCards = DashCard('crPledgesAdded', default_title='Highest Increase')
CRPledgeDeletedCards = DashCard('crPledgesDeleted', default_title='Highest Decrease')
CRAllPledges = DashCard('crAllPledges', default_title='All Pledges')
