from patreon.model.push_notifications import AdHocUserGroupPushPublisher


class NewCreatorPostPush(AdHocUserGroupPushPublisher):
    # user_ids should already be filtered on
    # campaigns_users.should_push_on_patron_post or
    # user_campaign_settings.push_post or user_campaign_settings.push_paid_post (as appropriate)
    def __init__(self, author, post, user_ids):
        super().__init__()
        self._user_ids = user_ids
        self.author = author
        self.post = post

    def user_ids(self):
        return self._user_ids

    def raw_message(self):
        if self.post.activity_title:
            return "{} published a new post: \"{}\"".format(self.author.full_name, self.post.activity_title)
        else:
            return "New post by {}".format(self.author.full_name, self.post.activity_title)

    def deep_link_url(self):
        return "patreon://posts/{}".format(self.post.activity_id)
