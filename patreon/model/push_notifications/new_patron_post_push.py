from patreon.model.push_notifications import AdHocUserGroupPushPublisher


class NewPatronPostPush(AdHocUserGroupPushPublisher):
    # user_ids should already be filtered on
    # campaigns_users.should_push_on_patron_post
    def __init__(self, post, author, user_ids):
        super().__init__()
        self._user_ids = user_ids
        self.post = post
        self.author = author

    def user_ids(self):
        return self._user_ids

    def raw_message(self):
        return "{} posted to your creator page".format(self.author.full_name)

    def deep_link_url(self):
        return "patreon://posts/{}".format(self.post.activity_id)
