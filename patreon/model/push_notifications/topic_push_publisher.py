from patreon.model.push_notifications import PushPublisher
from patreon.services.aws import SNS


class TopicPushPublisher(PushPublisher):
    def raw_message(self):
        """Override this in a subclass to return the message to be sent to the topic"""
        raise NotImplementedError()

    def topic_arn_suffix(self):
        """Override this in a subclass to return the topic suffix"""
        raise NotImplementedError()

    def topic_arn(self, suffix=None):
        if suffix is None:
            suffix = self.topic_arn_suffix()
        return SNS.arn_prefix() + suffix

    def publish_to_topic(self, topic_arn_suffix=None, raw_message=None, json_str=None):
        self.publish(
            endpoint=self.topic_arn(suffix=topic_arn_suffix),
            raw_message=raw_message,
            json_str=json_str
        )
