from patreon.model.push_notifications import AdHocUserGroupPushPublisher


class NewCommentPush(AdHocUserGroupPushPublisher):
    # user_ids should already be filtered on user_settings.push_new_comment
    def __init__(self, user_ids, comment, commenter, is_reply):
        super().__init__()
        self._user_ids = user_ids
        self.comment = comment
        self.commenter = commenter
        self.is_reply = is_reply

    def user_ids(self):
        return self._user_ids

    def raw_message(self):
        # TODO: if post.user_id != post.creator_id, different text?
        if self.is_reply:
            return "{} replied to your comment".format(self.commenter.full_name)
        else:
            return "{} commented on your post".format(self.commenter.full_name)

    def deep_link_url(self):
        return "patreon://posts/{}#comment-{}".format(self.comment.post_id, self.comment.comment_id)
