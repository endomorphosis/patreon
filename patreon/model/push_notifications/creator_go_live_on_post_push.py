from patreon.model.push_notifications import AdHocUserGroupPushPublisher
from patreon.model.manager import push_mgr


# For now, this will ad-hoc push to each user individually.
# If Amazon adds a batch-subscribe-to-topic endpoint,
#   migrate AdHocUserGroupPushPublisher to subclassing TopicPushPublisher
# If this gets too expensive before then, migrate this class to subclassing TopicPushPublisher
#   with a topic per group of users with the same pledge amount to the same creator
#   adding and removing users as their pledges change and their push settings change


class CreatorGoLiveOnPostPush(AdHocUserGroupPushPublisher):
    def __init__(self, post):
        super().__init__()
        self.post = post

    def user_ids(self):
        return push_mgr.get_user_ids_pushed_for_presence_on_post(self.post)

    def raw_message(self):
        message = "{} is chatting live right now".format(self.post.campaign.creator_name())
        if self.post.activity_title:
            message += ": {}".format(self.post.activity_title)
        else:
            message += "!"
        return message

    def deep_link_url(self):
        return "patreon://posts/{}?live=true".format(self.post.activity_id)
