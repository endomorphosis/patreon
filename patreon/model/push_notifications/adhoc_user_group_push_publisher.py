from patreon.model.push_notifications import PushPublisher
from patreon.model.manager import push_mgr
from patreon.services import async

# For now, this will ad-hoc push to each user individually.
# If Amazon adds a batch-subscribe-to-topic endpoint,
#   migrate it to subclassing TopicPushPublisher and spinning up an ad-hoc topic


class AdHocUserGroupPushPublisher(object):
    def __init__(self):
        super().__init__()
        self.publisher = PushPublisher()

    def user_ids(self):
        """Override this in a subclass to return the list of users you want subscribed to this topic"""
        raise NotImplementedError()

    def raw_message(self):
        """Override this in a subclass to return the message to be wrapped into json"""
        raise NotImplementedError()

    def deep_link_url(self):
        """Override this in a subclass to return a deep-linking URL"""
        return None

    def user_arns(self):
        push_infos = push_mgr.push_infos_for_user_ids(self.user_ids())
        return [
            push_info.sns_arn
            for push_info in push_infos
        ]

    @async.task
    def publish(self, raw_message=None, json_str=None, url=None):
        if json_str is None:
            if url is None:
                url = self.deep_link_url()
            if raw_message is None:
                raw_message = self.raw_message()
            json_str = self.publisher.json_str_message(raw_message=raw_message, url=url)
        for user_arn in self.user_arns():
            self.publisher.publish(
                endpoint=user_arn,
                json_str=json_str
            )
