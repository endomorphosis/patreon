import json
import patreon
from patreon.services.aws import SNS
from patreon.services.logging import rollbar


class PushPublisher(object):
    def __init__(self):
        self._connection = None

    def get_connection(self):
        if self._connection is None:
            self._connection = SNS()
        return self._connection

    def raw_message(self):
        """Override this in a subclass to return the message to be wrapped into json"""
        raise NotImplementedError()

    def deep_link_url(self):
        """Override this in a subclass to return a deep-linking URL"""
        return None

    def json_str_message(self, raw_message=None, url=None):
        if raw_message is None:
            raw_message = self.raw_message()
        if url is None:
            url = self.deep_link_url()
        return self.jsonify_message(message=raw_message, url=url)

    @staticmethod
    def jsonify_message(message, collapse_key=None, url=None):
        apns_dict = {
            "aps": {
                "alert": message,
                # "badge" : unread_status_mgr.count,
                "sound" : "default",
            },
            # "url": url
        }
        gcm_dict = {
            # "collapse_key": collapse_key,
            "data": {
                "message": message,
                "sound": "default",
                # "url": url
            }
        }
        if collapse_key:
            gcm_dict["collapse_key"] = collapse_key
        if url:
            apns_dict["url"] = url
            gcm_dict["data"]["url"] = url
        json_dict = {
            "default": message,
            "APNS": json.dumps(apns_dict),
            "GCM": json.dumps(gcm_dict)
        }
        return json.dumps(json_dict)

    def publish(self, endpoint, raw_message=None, json_str=None):
        if not patreon.config.push_notifications_enabled:
            return
        if json_str is None:
            json_str = self.json_str_message(raw_message=raw_message)
        try:
            self.get_connection().publish(
                target_arn=endpoint,
                message=json_str,
                message_structure='json'
            )
        except Exception as e:
            message = "SNS Error for endpoint {}: {}".format(str(endpoint), str(e))
            rollbar.log(message)
