import os

from patreon.constants import image_sizes
from patreon.model.manager import user_mgr
from patreon.model.type import photo_key_type, file_bucket_enum, TemporaryUploadCollection
from patreon.services import uploader, image_getter
from patreon.services.image_resizer import ArbitraryResizer, TopOffsetResizer


def upload_profile_image(image_path):
    with TopOffsetResizer(image_path, image_sizes.USER_SIZES) as resizer:
        return uploader.upload_files(
            TemporaryUploadCollection(resizer, file_bucket_enum.config_bucket_user)
        )


def update_profile_image(user_id, photo_key, legacy_image_url, width, height,
                         x, y):
    # If we get a facebook graph image, download it and get resizes.
    if not photo_key and legacy_image_url:
        temp_filename = image_getter.fetch_image(legacy_image_url)
        photo_key = upload_profile_image(temp_filename)
        os.remove(temp_filename)

    image_url = "https:" + photo_key_type.get_image_url(
        file_bucket_enum.config_bucket_user,
        photo_key,
        image_sizes.USER_LARGE
    )
    thumbnail_key = _convert_profile_image_to_thumb(
        image_url, width, height, x, y
    )
    _set_profile_images(user_id, photo_key, thumbnail_key)


def _convert_profile_image_to_thumb(image_url, width, height, x, y):
    temp_filename = image_getter.fetch_image(image_url)

    with ArbitraryResizer(temp_filename, image_sizes.USER_THUMB_SIZES,
                          source_width=width, source_height=height,
                          source_x=x, source_y=y) as thumbnail_resizer:
        key = uploader.upload_files(
            TemporaryUploadCollection(thumbnail_resizer, file_bucket_enum.config_bucket_user)
        )

    os.remove(temp_filename)

    return key


def _set_profile_images(user_id, photo_key, thumbnail_key):
    if not photo_key or not thumbnail_key:
        raise Exception('Invalid Images')

    # Deprecated thumbnail image url.
    image_url = photo_key_type.get_image_url(
        file_bucket_enum.config_bucket_user,
        photo_key,
        image_sizes.USER_LARGE_2
    )
    thumb_url = photo_key_type.get_image_url(
        file_bucket_enum.config_bucket_user,
        thumbnail_key,
        image_sizes.USER_THUMB_LARGE_3
    )

    user_mgr.get_user_or_throw(user_id).update({
        'ImageUrl': image_url,
        'ThumbUrl': thumb_url,
        'photo_key': photo_key,
        'thumbnail_key': thumbnail_key,
    })
