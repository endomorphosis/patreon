from patreon.constants.account_types import PaymentAccountTypes, SystemAccounts
from patreon.exception.payment_errors import AccountNotFound
from patreon.model.dbm.account_dbm import AccountDBM
from patreon.model.table import Account


def get_or_create_account_id(type, user_id=0, payment_instrument_id='', campaign_id=0, system_classification=''):
    try:
        account_id = get_account_id_or_throw(type, user_id, payment_instrument_id, campaign_id, system_classification)
    except AccountNotFound:
        account_id = create_account(type, user_id, payment_instrument_id, campaign_id, system_classification)

    return account_id


def get_account_id_or_throw(type, user_id=0, payment_instrument_id='', campaign_id=0, system_classification=''):
    dbm = AccountDBM()

    dbm.account_type_is(type)
    if user_id:
        dbm.belongs_to_user(user_id)
    if payment_instrument_id:
        dbm.has_payment_instrument(payment_instrument_id)
    if campaign_id:
        dbm.belongs_to_campaign(campaign_id)
    if system_classification:
        dbm.is_system_class(system_classification)

    account = dbm.first()
    if not account:
        if system_classification:
            raise AccountNotFound(type.name, user_id, payment_instrument_id, campaign_id, system_classification.name)
        raise AccountNotFound(type.name, user_id, payment_instrument_id, campaign_id, system_classification)

    return account.account_id


def create_account(type, user_id=0, payment_instrument_id='', campaign_id=0, system_classification=''):
    return Account.insert_ignore({
        'type': type,
        'user_id': user_id,
        'payment_instrument_id': payment_instrument_id,
        'campaign_id': campaign_id,
        'system_classification': system_classification
    })[0]


def get_user_credit_account_id_or_throw(user_id):
    return get_account_id_or_throw(type=PaymentAccountTypes.USER_CREDIT, user_id=user_id)


def get_external_payment_account_id_or_throw(user_id, payment_instrument_token):
    return get_account_id_or_throw(type=PaymentAccountTypes.USER_PAYMENT_INSTRUMENT, user_id=user_id,
                                   payment_instrument_id=payment_instrument_token)


def get_system_account_or_throw(type_):
    return get_account_id_or_throw(PaymentAccountTypes.SYSTEM_ACCOUNT, system_classification=type_)


def get_patron_fee_sink_account_id_or_throw():
    return get_system_account_or_throw(SystemAccounts.PATRON_FEE_SINK)


def get_patreon_external_transaction_fee_faucet_account_id_or_throw():
    return get_system_account_or_throw(SystemAccounts.PATREON_EXTERNAL_TRANSACTION_FEE_FAUCET)


def get_external_transaction_fee_sink_account_id_or_throw():
    return get_system_account_or_throw(SystemAccounts.EXTERNAL_TRANSACTION_FEE_SINK)


def get_patreon_credit_adjustment_faucet_id_or_throw():
    return get_system_account_or_throw(SystemAccounts.PATREON_CREDIT_ADJUSTMENT_FAUCET)

def get_patreon_credit_adjustment_sink_id_or_throw():
    return get_system_account_or_throw(SystemAccounts.PATREON_CREDIT_ADJUSTMENT_SINK)


def touch_user_credit_account_id(user_id):
    return get_or_create_account_id(type=PaymentAccountTypes.USER_CREDIT, user_id=user_id)


def touch_external_payment_account_id(user_id, payment_instrument_id):
    return get_or_create_account_id(type=PaymentAccountTypes.USER_PAYMENT_INSTRUMENT, user_id=user_id,
                                    payment_instrument_id=payment_instrument_id)


def touch_system_account(type_):
    return get_or_create_account_id(PaymentAccountTypes.SYSTEM_ACCOUNT, system_classification=type_)


def touch_patron_fee_sink_account_id():
    return touch_system_account(SystemAccounts.PATRON_FEE_SINK)


def touch_patreon_external_transaction_fee_faucet_account_id():
    return touch_system_account(SystemAccounts.PATREON_EXTERNAL_TRANSACTION_FEE_FAUCET)


def touch_external_transaction_fee_sink_account_id():
    return touch_system_account(SystemAccounts.EXTERNAL_TRANSACTION_FEE_SINK)


def touch_patreon_credit_adjustment_faucet():
    return touch_system_account(SystemAccounts.PATREON_CREDIT_ADJUSTMENT_FAUCET)

def touch_patreon_credit_adjustment_sink():
    return touch_system_account(SystemAccounts.PATREON_CREDIT_ADJUSTMENT_SINK)
