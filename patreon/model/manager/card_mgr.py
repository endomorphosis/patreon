import datetime

import patreon
from patreon.constants.money import PAYPAL_DEFAULT_AMOUNT
from patreon.exception.payment_errors import BadCardAuthorizationToken, \
    ExternalPaymentAPIException, InvalidCardAuthorization, InvalidCardToken
from patreon.model.dbm import card_dbm
from patreon.model.manager import user_mgr
from patreon.model.manager.payment_event_mgr import capture_exception_payment_events
from patreon.model.table import Card, PaypalToken
from patreon.services.logging.unsorted import log_state_change
from patreon.services.payment_apis import StripeAPI, PayPalAPI
from patreon.util.format import format_money


def get(card_id):
    return card_dbm.get(card_id)


def get_card_by_user_id(user_id):
    result = find_by_user_id(user_id)
    if result:
        return result[0]
    return None


@patreon.services.caching.multiget_cached(object_key='UID', default_result=list)
def find_by_user_id(user_ids):
    return card_dbm.find_by_user_id_list(user_ids).all()


def add_card(user_id, card_id, last_4, type, country, expiration):
    log_state_change('card', user_id, 'create', 'add_card')
    Card.insert({
        'RID': card_id,
        'UID': user_id,
        'Number': last_4,
        'Type': type,
        'Country': country,
        'ExpirationDate': expiration,
        'Created': datetime.datetime.now()
    })


@capture_exception_payment_events
def get_payment_api_for_card_auth(card_authorization_token):
    if card_authorization_token.startswith('tok_'):
        return StripeAPI()
    elif card_authorization_token.startswith('EC-'):
        return PayPalAPI()

    raise InvalidCardAuthorization


@capture_exception_payment_events
def get_payment_api_for_card_token(payment_instrument_id):
    if card_token_is_stripe(payment_instrument_id):
        return StripeAPI()
    elif card_token_is_paypal(payment_instrument_id):
        return PayPalAPI()

    raise InvalidCardToken


@capture_exception_payment_events
def save_card_from_payment_api(user_id, card_authorization_token):
    api = get_payment_api_for_card_auth(card_authorization_token)
    try:
        result = api.add_instrument(card_authorization_token)
    except ExternalPaymentAPIException as e:
        raise api.raise_generic_api_exception(e)

    if result:
        card_id, last_4, type, country, expire = result
        add_card(user_id, card_id=card_id, last_4=last_4, type=type, country=country, expiration=expire)
        return card_id
    else:
        raise BadCardAuthorizationToken


def verify_card(card):
    card.update({
        'Verified': 1
    })


def card_token_is_paypal(card_token):
    return card_token.startswith('B-')


def card_token_is_stripe(card_token):
    return card_token.startswith('cus')


def get_card_type_string(card_token):
    if card_token_is_paypal(card_token):
        return 'paypal'
    elif card_token_is_stripe(card_token):
        return 'stripe'

    raise InvalidCardToken


def save_paypal_state(user_id, form_data, paypal_token):
    existing_paypal_state = retrieve_paypal_state(user_id)
    if existing_paypal_state:
        existing_paypal_state.delete()

    PaypalToken.insert({
        'user_id': user_id,
        'paypal_token': paypal_token,
        'post_info_json': form_data,
        'created_at': datetime.datetime.now()
    })


def get_card_by_user_id_card_id(user_id, card_id):
    return Card.query.filter_by(RID=card_id, UID=user_id).first()


def retrieve_paypal_state(user_id):
    return PaypalToken.get(user_id)


@capture_exception_payment_events
def get_express_checkout_token(creator_id, amount):
    creator = user_mgr.get(creator_id)

    return_url = 'https://{}/paypalReturn?suc=1'.format(patreon.config.main_server)
    cancel_url = 'https://{}/paypalReturn?err=1'.format(patreon.config.main_server)

    if creator:
        amount_cents = amount * 100
        name = creator.full_name
        description = "Recurring pledge to $creatorName of $%s per %s. Cancelable anytime." \
            .format(format_money(amount_cents), creator.campaigns[0].pay_per_name)

        return_url += '&cid=' + str(creator.user_id)
        cancel_url += '&cid=' + str(creator.user_id)

    else:
        amount_cents = PAYPAL_DEFAULT_AMOUNT
        name = 'Patreon'
        description = 'Adding PayPal account to Patreon for recurring pledges. Cancelable anytime.'
        return_url += '&addnew=1'
        cancel_url += '&addnew=1'

    try:
        return PayPalAPI().add_instrument_start(
            amount_cents=amount_cents, name=name, description=description,
            return_url=return_url, cancel_url=cancel_url
        )
    except Exception as e:
        PayPalAPI().raise_generic_api_exception(e)
