import datetime
from patreon.model.table import PledgesHistory


def insert_pledge_history(creator_id, user_id, pledge, max_amount):
    PledgesHistory.insert({
        'CID': creator_id,
        'UID': user_id,
        'Pledge': pledge,
        'MaxAmount': max_amount,
        'Created': datetime.datetime.now()
    })


def _get_patron_history(patron_id):
    return PledgesHistory.query.filter_by(UID=patron_id)


def get_patron_history(patron_id):
    return _get_patron_history(patron_id).all()


def delete_patron_history(patron_id):
    return _get_patron_history(patron_id).delete()
