from patreon.model.table import YoutubeImport

def add(video_info):
    return YoutubeImport.insert_ignore(video_info)

def count_for_user(user_id):
    return YoutubeImport.query.filter(YoutubeImport.UID == user_id).count()
