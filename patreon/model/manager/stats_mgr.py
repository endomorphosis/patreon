from sqlalchemy import or_, func, text

from patreon.services.db import db_session
from patreon.model.table import Stats


def get_range(key=None, date=None, date2=None):
    if key and date and date2:
        return db_session.query(Stats).filter(
            Stats.Key == key,
            Stats.Date >= date,
            Stats.Date <= date2
        ).order_by(Stats.Date)

    return []


def get_multiple_keys_range_date_sub(*keys, interval=None):
    query = [or_(*[Stats.Key == k for k in keys])]
    if interval and interval > 0:
        query.append(Stats.Date >= func.date_sub(func.now(), text('interval {:d} month'.format(interval))))

    return db_session.query(Stats)\
        .filter(*query) \
        .order_by(Stats.Date)


def get_multiple_keys(*keys, date1=None, date2=None):
    query = [or_(*[Stats.Key == k for k in keys])]
    if date1 is not None and date2 is not None:
        _date1 = min(date1, date2)
        _date2 = max(date1, date2)
        query.extend([Stats.Date >= _date1, Stats.Date <= _date2])

    return db_session.query(Stats)\
        .filter(*query) \
        .order_by(Stats.Date)


def create_or_update(key, date, value):
    stat = db_session.query(Stats)\
        .filter(Stats.Key == key, Stats.Date == date) \
        .first()
    if not stat:
        stat = Stats(Key=key, Date=date, Value=value)
        db_session.add(stat)
        db_session.flush()
    else:
        if value != stat.Value:
            stat.update({'Value': value})

    return stat
