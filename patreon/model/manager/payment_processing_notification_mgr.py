import calendar
import datetime
import dateutil.parser
import itertools

from sqlalchemy import func
from sqlalchemy import desc

from patreon import globalvars
from patreon.services.db import db_session
from patreon.model.manager import rewards_give_mgr, user_mgr, payment_mgr, campaign_mgr, new_pledge_mgr
from patreon.model.table import RewardsPaid, RewardsGive, Payout
from patreon.util import time_math
from patreon.util import datetime as datetime_helper

def charging_will_start_soon():
    return datetime.datetime.now().day > charging_start_day()

def charging_start_day():
    days_before_end_of_month = 3
    now = datetime.datetime.now()
    (_, last_day_of_month) = calendar.monthrange(now.year, now.month)
    return last_day_of_month - days_before_end_of_month

def successful_payments_cutoff_day_and_time():
    cutoff_day_of_month = 5
    cutoff_time_of_day_string = "07:05"
    cutoff_time_of_day = dateutil.parser.parse(cutoff_time_of_day_string).time()
    return (cutoff_day_of_month, cutoff_time_of_day)


def automatic_payouts_cutoff_day_and_time():
    cutoff_day_of_month = 6
    cutoff_time_of_day_string = "00:00"
    cutoff_time_of_day = dateutil.parser.parse(cutoff_time_of_day_string).time()
    return (cutoff_day_of_month, cutoff_time_of_day)


def successful_payments_cutoff_for_month_containing_date(date):
    day, time = successful_payments_cutoff_day_and_time()
    return datetime.datetime.combine(date.replace(day=day), time)


def automatic_payouts_cutoff_for_month_containing_date(date):
    day, time = automatic_payouts_cutoff_day_and_time()
    return datetime.datetime.combine(date.replace(day=day), time)


def currently_charging():
    now = datetime.datetime.now()
    cutoff = successful_payments_cutoff_for_month_containing_date(now.date())
    return now < cutoff


def automatic_payouts_have_finished():
    now = datetime.datetime.now()
    cutoff = automatic_payouts_cutoff_for_month_containing_date(now.date())
    return now >= cutoff


def mysql_date_format_string_for_day_of_month(day):
    day_with_leading_zero = str(day).zfill(2)
    return "%y-%m-" + day_with_leading_zero


def mysql_date_format_string_for_day_of_month_at_time(day, time):
    date_part = mysql_date_format_string_for_day_of_month(day)
    time_part = time.isoformat()
    return " ".join([date_part, time_part])


def timestamp_of_format_for_column(format_string, column):
    return func.timestamp(func.date_format(column, format_string))


def first_of_month_date_for_column(column):
    return timestamp_of_format_for_column(mysql_date_format_string_for_day_of_month(1), column)


def timestamp_at_day_and_time_for_month_of_column(day, time, column):
    return func.timestamp(func.date_format(column, mysql_date_format_string_for_day_of_month_at_time(day, time)))


def autopay_start_timestamp_for_month_of_column(column):
    day, time = successful_payments_cutoff_day_and_time()
    return timestamp_at_day_and_time_for_month_of_column(day, time, column)


def autopay_end_timestamp_for_month_of_column(column):
    day, time = automatic_payouts_cutoff_day_and_time()
    return timestamp_at_day_and_time_for_month_of_column(day, time, column)


def column_is_before_autopay_start_for_its_month(column):
    return (column < autopay_start_timestamp_for_month_of_column(column))


def rewards_paid_is_in_month_following_rewards_give():
    # TODO: seriously move to DBM
    # our payouts cronjob batches by month according to US/Pacific time, not the db's UTC time
    give_created_at_pacific = func.convert_tz(RewardsGive.created_at, 'UTC', 'US/Pacific')
    paid_created_at_pacific = func.convert_tz(RewardsPaid.created_at, 'UTC', 'US/Pacific')
    return (
        # either it's the same year and one month in the future
        ( (func.year(give_created_at_pacific) == func.year(paid_created_at_pacific))
        & (func.month(give_created_at_pacific) + 1 == func.month(paid_created_at_pacific)) )
        |
        # or the it's december/january of one year and the next
        ( (func.year(give_created_at_pacific) + 1 == func.year(paid_created_at_pacific))
        & (func.month(give_created_at_pacific) == 12) & (func.month(paid_created_at_pacific) == 1) )
    )

def successful_charge_totals_query_for_column(user_id, column, count=10):
    # TODO: move to DBM?
    created_cutoff = autopay_start_timestamp_for_month_of_column(RewardsPaid.created_at)
    most_recently_paid_datetime = func.max(RewardsPaid.Created)
    total_amount = func.sum(RewardsGive.amount)
    total_fee = func.sum(RewardsGive.fee)
    total_patreon_fee = func.sum(RewardsGive.patreon_fee)

    query = db_session.query(most_recently_paid_datetime, total_amount, total_fee, total_patreon_fee)
    query = query.join(RewardsGive.RewardsPaid)
    query = query.filter(column_is_before_autopay_start_for_its_month(RewardsPaid.Created))
    query = query.filter(rewards_paid_is_in_month_following_rewards_give())
    query = query.filter(column == user_id)
    query = query.filter(RewardsGive.status == globalvars.get('REWARD_PROCESSED'))
    query = query.group_by(created_cutoff).order_by(desc(RewardsPaid.created_at)).limit(count)
    return query


def successful_charge_totals_query_for_creator_id(user_id, count=10):
    return successful_charge_totals_query_for_column(user_id, RewardsGive.creator_id, count)


def successful_charge_totals_query_for_patron_id(user_id, count=10):
    return successful_charge_totals_query_for_column(user_id, RewardsGive.patron_id, count)


def find_successful_charge_totals_for_creator_id(user_id, count=10, cursor_date=None, timezone=None):
    successful_charge_totals = successful_charge_totals_query_for_creator_id(user_id, count).all()

    # filter by cursor date on the latest successful charge in a batch,
    # so that we don't split one batch into multiple notifications
    # if the cursor date is in the middle of a batch
    if cursor_date:
        cursor_timestamp = datetime_helper.as_utc(datetime_helper.datetime_at_midnight_on_date(cursor_date + datetime.timedelta(1), timezone))
        successful_charge_totals = [successful_charge_total
                                    for successful_charge_total in successful_charge_totals
                                    if datetime_helper.as_utc(successful_charge_total[0]) < cursor_timestamp]

    # if we're currently charging and there are uncollected pledges for the last month for this creator,
    # then that last total and timestamp isn't canonical and should be discarded.
    most_recent_charge_datetime = successful_charge_totals[0][0] if successful_charge_totals else None
    if most_recent_charge_datetime and time_math.is_in_current_month(most_recent_charge_datetime) and currently_charging():
        if rewards_give_mgr.any_pending_or_failed_payments_for_creator_in_month_exist(user_id, time_math.first_of_previous_month_for_date(cursor_date or datetime.date.today())):
            successful_charge_totals.pop(0)

    return successful_charge_totals


def find_successful_charge_totals_for_patron_id(user_id, count=10, cursor_date=None, timezone=None):
    successful_charge_totals = successful_charge_totals_query_for_patron_id(user_id, count).all()

    # filter by cursor date on the latest successful charge in a batch,
    # so that we don't split one batch into multiple notifications
    # if the cursor date is in the middle of a batch
    if cursor_date:
        cursor_timestamp = datetime_helper.as_utc(datetime_helper.datetime_at_midnight_on_date(cursor_date + datetime.timedelta(1), timezone))
        successful_charge_totals = [successful_charge_total
                                    for successful_charge_total in successful_charge_totals
                                    if datetime_helper.as_utc(successful_charge_total[0]) < cursor_timestamp]

    return successful_charge_totals


def datetime_column_is_during_autopay_window(column):
    return (column >= autopay_start_timestamp_for_month_of_column(column)) & (column < autopay_end_timestamp_for_month_of_column(column))


def find_automatic_payout_amounts_for_user_id(user_id, count=10, cursor_date=None, timezone=None):
    # hypothesis: automatic payouts are distinguished by being between the automatic payout cron start time and the end of that day.
    # that's not great but it's what we've got.
    query = Payout.query.filter(
        Payout.is_payout
        & datetime_column_is_during_autopay_window(Payout.created_at)
        & Payout.user_id == user_id
    )
    if cursor_date:
        # TODO: extract this into a method
        newest_timestamp = datetime_helper.datetime_at_midnight_on_date(cursor_date + datetime.timedelta(1), timezone)
        query = query.filter(Payout.created_at < newest_timestamp)
    query = query.order_by(desc(Payout.created_at)).limit(count)

    return (
        (payout.created_at, payout.amount_cents, "paypal" if payout.external_id_looks_like_paypal() else "direct_deposit")
        for payout in query
    )


def find_payment_processing_events_for_user_id(user_id, count=10, cursor_date=None, timezone=None):
    # we only want one notification card per month.
    # so if there's a payout, then it replaces the successful charges

    # TODO: create PaymentsProcessingNotification objects instead of tuples

    successful_charge_your_patrons_events = (
        (dt, int(amount - fee - patreon_fee), "charge", None) for (dt, amount, fee, patreon_fee) in
        find_successful_charge_totals_for_creator_id(user_id, count, cursor_date, timezone)
    )
    successful_charge_you_events = (
        (dt, int(amount), "charge_you", None) for (dt, amount, fee, patreon_fee) in
        find_successful_charge_totals_for_patron_id(user_id, count, cursor_date, timezone)
    )
    automatic_payout_events = (
        (dt, int(amount), "payout", payout_type) for (dt, amount, payout_type) in
        find_automatic_payout_amounts_for_user_id(user_id, count, cursor_date, timezone)
    )

    month_of_event_lambda = lambda event: time_math.first_of_month_for_datetime(event[0])
    events = sorted(itertools.chain(automatic_payout_events, successful_charge_your_patrons_events, successful_charge_you_events), key=month_of_event_lambda, reverse=True)
    events = [list(group)[0] for (k, group) in itertools.groupby(events, key=month_of_event_lambda)]

    if events:
        (dt, amount, event_type, payout_type) = events[0]
        # we give a different message if we think there's a payout coming soon.
        if event_type == 'charge' and time_math.is_in_current_month(dt) and not automatic_payouts_have_finished() \
                and user_mgr.user_has_enabled_automatic_payouts(user_id):
            payout_amount = payment_mgr.get_credit_total_legacy(user_id)
            if int(payout_amount) > 0:
                events[0] = (dt, int(payout_amount), 'payout_soon', None)

    if not cursor_date: # the top of your feed
        if charging_will_start_soon(): # the end of a month
            campaign = campaign_mgr.try_get_campaign_by_user_id_hack(user_id)
            if campaign:
                if campaign.is_monthly or rewards_give_mgr.any_pending_or_failed_payments_for_creator_in_month_exist(user_id, datetime.date.today()):
                    event = (datetime.datetime.now(), None, "start_soon_creator", None)
                    events.append(event)
            if new_pledge_mgr.patron_has_any_pledges_to_monthly_creators(user_id) or rewards_give_mgr.any_pending_payments_for_patron_in_month_exist(user_id, datetime.date.today()):
                event = (datetime.datetime.now(), None, "start_soon_patron", None)
                events.append(event)
        elif currently_charging(): # the beginning of a month
            last_month = time_math.first_of_previous_month_for_date(datetime.date.today())
            if rewards_give_mgr.any_pending_or_failed_payments_for_creator_in_month_exist(user_id, last_month):
                event = (datetime.datetime.now(), None, "currently_charging_creator", None)
                events.append(event)
            if rewards_give_mgr.any_pending_payments_for_patron_in_month_exist(user_id, last_month):
                event = (datetime.datetime.now(), None, "currently_charging_patron", None)
                events.append(event)

    return events
