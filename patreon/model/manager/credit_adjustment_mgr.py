from patreon.constants.transaction_types import TxnTypes
from patreon.exception.payment_errors import TransactionAlreadyFailed, \
    TransactionInProcess
from patreon.model.manager import txn_mgr, account_mgr, ledger_entry_mgr
from patreon.model.table.credit_adjustment import CreditAdjustment
from patreon.model.table.transactions import CreditAdjustmentTxn
from patreon.services.db import db_session

__author__ = 'zennenga'


def create_transaction():
    return txn_mgr.create_transaction(CreditAdjustmentTxn.__txn_type__)


def create_obligation(transaction_id, user_id, amount_cents):
    return CreditAdjustment.insert({
        'txn_id': transaction_id,
        'amount_cents': amount_cents,
        'adjuster_user_id': user_id
    })


def initialize(user_id, amount_cents):
    transaction_id = create_transaction()
    create_obligation(transaction_id, user_id, amount_cents)

    return transaction_id


def touch_accounts(transaction_id):
    transaction = CreditAdjustmentTxn.get(transaction_id)
    user_id = transaction.user_id

    account_mgr.touch_user_credit_account_id(user_id)
    account_mgr.touch_patreon_credit_adjustment_faucet()
    account_mgr.touch_patreon_credit_adjustment_sink()


def mark_transaction_as_processing(transaction_id):
    txn_mgr.mark_as_processing(transaction_id)


def mark_transaction_as_successful(transaction_id):
    txn_mgr.mark_as_succeeded(transaction_id)


def mark_transaction_as_failed(transaction_id):
    txn_mgr.mark_as_failed(transaction_id)


def clear_processing(transaction_id):
    txn_mgr.clear_processing(transaction_id)


def insert_ledger_entries(transaction_id):
    transaction = CreditAdjustmentTxn.get(transaction_id)

    credit_amount = transaction.credit_adjustment.amount_cents
    user_id = transaction.credit_adjustment.user_id

    user_credit_account = account_mgr.get_user_credit_account_id_or_throw(user_id)

    patreon_credit_faucet = account_mgr.get_patreon_credit_adjustment_faucet_id_or_throw()
    patreon_credit_sink = account_mgr.get_patreon_credit_adjustment_sink_id_or_throw()

    if credit_amount > 0:
        # Give the user the credit
        ledger_entry_mgr.move_money_between_accounts(transaction_id, from_account=patreon_credit_faucet,
                                                     to_account=user_credit_account, amount_cents=credit_amount)
    elif credit_amount < 0:
        credit_amount *= -1
        ledger_entry_mgr.move_money_between_accounts(transaction_id, from_account=user_credit_account,
                                                     to_account=patreon_credit_sink, amount_cents=credit_amount)


def process_transaction(transaction_id):
    with db_session.begin_nested():
        transaction = CreditAdjustmentTxn.get_with_lock(transaction_id)
        if transaction.is_successful:
            return transaction_id
        elif transaction.is_failed and transaction.most_recent_external_payment.is_failed:
            raise TransactionAlreadyFailed(transaction_id, TxnTypes.credit_purchase.value)
        elif transaction.is_processing:
            raise TransactionInProcess(transaction_id, TxnTypes.credit_purchase.value)

        touch_accounts(transaction_id)
        mark_transaction_as_processing(transaction_id)

    # This commits
    #    1. The external payment (if new)
    #    2. The began_processing_at flag on txn
    db_session.commit()
    db_session.expire_all()
    CreditAdjustmentTxn.get_with_lock(transaction_id)

    with db_session.begin_nested():
        insert_ledger_entries(transaction_id)
        ledger_entry_mgr.assert_transaction_ledger_sane(transaction_id)
        mark_transaction_as_successful(transaction_id)
        clear_processing(transaction_id)
    db_session.commit()
    db_session.expire_all()

    return transaction_id
