import datetime

import patreon
from patreon.model.dbm import follow_dbm
from patreon.model.table import Follow


def find_by_followed_id(followed_id):
    return follow_dbm.find_by_followed_id(followed_id).all()


@patreon.services.caching.multiget_cached(object_key='follower_id', default_result=list)
def find_by_follower_id(follower_ids):
    return follow_dbm.find_by_follower_ids(follower_ids).all()


def insert(follower_id, followed_id):
    return Follow.insert({
        'follower_id': follower_id,
        'followed_id': followed_id,
        'created_at': datetime.datetime.now()
    })


def get(follower_id, followed_id):
    return Follow.get(followed_id=followed_id, follower_id=follower_id)


def delete(follower_id, followed_id):
    result = follow_dbm.get(follower_id, followed_id)
    if result:
        result.delete()


def delete_by_followed_id(followed_id):
    follow_dbm.find_by_followed_id(followed_id).delete()


def follower_and_followed_ids_for_follow_id(follow_id):
    return follow_id.split('-')
