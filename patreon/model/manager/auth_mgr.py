import pyotp
import qrcode
from six import BytesIO
from passlib.apps import phpass_context
from hashlib import md5

import patreon
from patreon.exception.auth_errors import BlankPassword
from patreon.model.table import Session, TwoFactorSecrets, User
from patreon.services.logging.unsorted import log_json


def hash_password(password):
    return phpass_context.encrypt(password)


def deprecated_hash_password_md5(password):
    # This magic number is the salt used on all our md5 password hashes
    # This should NOT be moved to a constant because it should NOT be used anywhere but here
    return md5((password + 'wRz9MO9i25').encode('utf-8')).hexdigest()


def deprecated_verify_hash_hybrid(password, password_hash):
    md5_hash = deprecated_hash_password_md5(password)
    return verify_hash_bcrypt(md5_hash, password_hash)


def deprecated_verify_hash_md5(password, password_hash):
    return deprecated_hash_password_md5(password) == password_hash


def verify_hash_bcrypt(password, password_hash):
    if password_hash is None:
        # Possible if user signed up with Facebook only.
        return False
    try:
        return phpass_context.verify(password, password_hash)
    except ValueError:
        # This happens if the hash passed is not a bcrypt hash
        return False


def check_admin_password(password):
    # These magic numbers refer to the "universal password". It needs to die.
    # These should NOT be moved into constants lest they infect good code
    return md5((password + "5e22a7bd9b7eae5d5f03").encode('utf-8')).hexdigest() == "a8f496d0b6ada987d88771c3f7a82d3e"


def check_password(user_id, password):
    if password is None:
        return False
    result = User.get(user_id=user_id)
    if result is None:
        return False

    password_hash = result.Password

    if verify_hash_bcrypt(password, password_hash):
        return True
    # check hybrid hash and md5 hash
    elif deprecated_verify_hash_hybrid(password, password_hash) \
            or deprecated_verify_hash_md5(password, password_hash):
        change_password(user_id, password)
        return True

    return False


def change_password(user_id, password):
    hashed_password = hash_password(password)
    User.get(user_id=user_id).update({'Password': hashed_password})


def login_user_fb(facebook_id):
    if facebook_id is None:
        return None

    result = User.get_unique(facebook_id=facebook_id)
    if result is None:
        return None
    return result.UID


def verify_two_factor_code(secret, code, for_time=None):
    totp = pyotp.TOTP(secret)
    return totp.verify(code, for_time=for_time)


def setup_two_factor_auth(user, secret):
    TwoFactorSecrets.insert({
        'user_id': user.user_id,
        'secret': secret
    })
    user.update({
        'two_factor_enabled': 1
    })
    Session.query.filter_by(user_id=user.user_id).delete()


def get_authenticator_uri(secret, email):
    totp = pyotp.TOTP(secret)
    return totp.provisioning_uri(email, issuer_name='Patreon')


def get_user_by_email_and_password(email, password):
    if email is None or password is None:
        return None

    result = User.get_unique(email=email)
    if result is None:
        return None

    user_id = result.UID

    if (patreon.config.debug or patreon.config.stage == 'development') \
            and check_admin_password(password):
        log_json({
            'verb': 'univ_password_use',
            'target': user_id
        })
        return user_id

    if check_password(user_id, ''):
        raise BlankPassword()

    if check_password(user_id, password):
        return user_id

    return None


def get_qr_code_for_authenticator(uri):
    image = qrcode.make(uri)

    img_io = BytesIO()
    image.save(img_io, 'PNG')
    img_io.seek(0)

    return img_io
