import os
from werkzeug.utils import secure_filename

import patreon
from patreon.constants import mc_key
from patreon.exception.attachment_errors import AttachmentNotFound
from patreon.model.dbm import attachment_dbm
from patreon.model.type import TemporaryUpload, file_bucket_enum
from patreon.model.table import Attachments
from patreon.services import uploader
from patreon.services.caching import memcache
from patreon.util.random import random_key


def attach_creations_file(post_id, temp_filename, source_filename):
    file_key = random_key()
    _, filename = os.path.split(source_filename)
    destination_filename = file_key + "/" + secure_filename(filename)

    url = uploader.upload_file(TemporaryUpload(
        temp_filename, destination_filename, file_bucket_enum.config_bucket_post
    ))

    memcache.delete(mc_key.share(post_id))
    return create_attachment(post_id, url, source_filename)


def get_attachment_or_throw(post_id, attachment_id):
    attachment = Attachments.query \
        .filter(Attachments.HID == post_id) \
        .filter(Attachments.ID == attachment_id) \
        .first()

    if not attachment:
        raise AttachmentNotFound(attachment_id)

    return attachment


def get_attachment(attachment_id):
    return Attachments.get(attachment_id=attachment_id)


def get_attachments(attachment_ids):
    return attachment_dbm.find_by_id_list(attachment_ids).all()


@patreon.services.caching.multiget_cached(object_key='HID', default_result=list)
def get_attachments_by_activity_id(activity_ids):
    return attachment_dbm.find_by_activity_id_list(activity_ids).all()


def create_attachment(activity_id, url, name):
    attachment_id = Attachments.insert(dict(
        HID=activity_id,
        Url=url,
        Name=name
    ))
    return get_attachment(attachment_id)


def delete_attachment(post_id, attachment_id):
    return attachment_dbm\
        .find_by_activity_id_attachment_id_list(post_id, [attachment_id]) \
        .delete(synchronize_session=False)


def delete_attachments(activity_id, attachment_ids):
    return attachment_dbm\
        .find_by_activity_id_attachment_id_list(activity_id, attachment_ids) \
        .delete(synchronize_session=False)
