import re
import datetime
from urllib import parse

import patreon
from patreon.exception.user_errors import UserNotFound
from patreon.model.dbm import user_dbm, unverified_user_dbm, pledge_dbm
from patreon.model.manager import settings_mgr, change_email_mgr, \
    user_location_mgr, auth_mgr
from patreon.model.table import User, UnverifiedUser, UserExtra
from patreon.services import mail
from patreon.services.db import db_session
from patreon.services.mail.templates import ChangeEmail
from patreon.services.logging.unsorted import log_anomaly, log_state_change
from patreon.util import collections, random


@patreon.services.caching.request_cached()
def get(user_id=None):
    return get_user(user_id)


def get_unique(vanity=None, email=None, facebook_id=None):
    return get_user_unique(vanity, email, facebook_id)


def get_user_or_throw(user_id):
    user = get_user(user_id)
    if user is None:
        raise UserNotFound(user_id)
    return user


@patreon.services.caching.request_cached()
def get_user(user_id=None):
    return user_dbm.get_user(user_id)


def get_user_unique(vanity=None, email=None, facebook_id=None):
    return User.get_unique(vanity=vanity, email=email, facebook_id=facebook_id)


def get_users(user_ids):
    if not user_ids:
        return []
    return user_dbm.get_users(user_ids).all()


@patreon.services.caching.request_cached()
def is_user_private(user_id):
    return User.get(user_id=user_id).is_private


@patreon.services.caching.request_cached()
def get_user_by_email(email):
    return user_dbm.get_user_by_email(email)


@patreon.services.caching.request_cached()
def get_unverified_user(verification):
    return UnverifiedUser.get(verification)


@patreon.services.caching.request_cached()
def find_unverified_user_by_email(email):
    return unverified_user_dbm.find_by_email(email)


def insert_unverified_user(email):
    verification = random.get_verification_token()
    UnverifiedUser.insert({
        'Email': email,
        'Verification': verification,
        'Created': datetime.datetime.now()
    })
    return verification


def create_user(first_name, last_name, email, vanity, gender, is_verified,
                image_url, thumb_url, about, password, facebook=None,
                twitter=None, youtube=None, is_suspended=0, is_deleted=0,
                is_nuked=0):
    with db_session.begin_nested():
        User.insert({
            'Email': email,
            'FName': first_name,
            'LName': last_name,
            'Vanity': vanity,
            'Password': auth_mgr.hash_password(password),
            'Gender': gender.value,
            'ImageUrl': image_url,
            'ThumbUrl': thumb_url,
            'Status': is_verified,
            'About': about,
            "Created": datetime.datetime.now(),
            "Facebook": facebook,
            "Twitter": twitter,
            "Youtube": youtube,
            "is_suspended": is_suspended,
            "is_deleted": is_deleted,
            "is_nuked": is_nuked
        })

        new_user = User.get_unique(email=email)

        # SQLAlchemy takes care of defaults
        settings_mgr.insert_old_user_settings(new_user.user_id)
        settings_mgr.insert_user_settings(new_user.user_id)
        settings_mgr.insert_user_extra(new_user.user_id)

    return new_user


def register_user(email, password, vanity, first_name, last_name, facebook_id=None):
    if email is None:
        return None
    if not re.match(r"[^@]+@[^@]+\.[^@]+", email):
        return None
    # TODO(postport) check for disposable emails
    # Technically we do this in php but the list has 1 domain.

    email = email.strip()

    # Users who register with facebook don't have a password
    # Users who register with a password don't provide a facebook_id during registration
    if facebook_id is None and password is not None:
        password = auth_mgr.hash_password(password)
    elif facebook_id is not None:
        password = None
    else:
        return None

    if vanity is not None and User.get_unique(vanity=vanity) is not None:
        vanity = None

    if User.get_unique(email=email) is not None:
        return None

    if facebook_id is not None and User.get_unique(facebook_id=facebook_id) is not None:
        return None

    User.insert({
        'Email': email,
        'FName': first_name,
        'LName': last_name,
        'Password': password,
        'Vanity': vanity,
        'Created': datetime.datetime.now(),
        'FBID': facebook_id
    })

    verification_token = insert_unverified_user(email)

    new_user_id = User.get_unique(email=email).UID

    settings_mgr.insert_old_user_settings(new_user_id)
    settings_mgr.insert_user_settings(new_user_id)
    settings_mgr.insert_user_extra(new_user_id)

    # todo send email
    return verification_token


def confirm_user(verification_token):
    unverified_user = UnverifiedUser.get(verification=verification_token)
    if unverified_user is None:
        return None

    unverified_email = unverified_user.Email

    user = User.get_unique(email=unverified_email)
    if user is None:
        return None

    user.update({'Status': 1})

    UnverifiedUser.query.filter_by(Email=unverified_email).delete()

    return User.get_unique(email=unverified_email)


def resend_confirm(user_id):
    if user_id is None:
        return None

    result = User.get(user_id=user_id)
    if result is None:
        return None

    email = result.Email
    verification_token = random.get_verification_token()
    UnverifiedUser.insert({
        'Verification': verification_token,
        'Email': email,
        'Created': datetime.datetime.now()
    })
    # todo send email
    return verification_token


def setup_update_email(user_id, email):
    if user_id is None or email is None:
        return None

    if not re.match(r"[^@]+@[^@]+\.[^@]+", email):
        return None

    if User.get(user_id=user_id) is None:
        return None

    if User.get_unique(email=email) is not None:
        log_anomaly('user', user_id, user_id, 'update', 'email duplicated')
        return None

    return change_email_mgr.insert(user_id=user_id, email=email)


def update_email(verification_token):
    result = change_email_mgr.get(verification=verification_token)
    if result is None:
        return None

    user_id = result.UID
    email = result.Email

    User.get(user_id=user_id).update({'Email': email, 'Status': 1})

    result.delete()

    # todo send email
    return User.get_unique(email=email)


def is_authorized(user_id, actor_id):
    return user_id == actor_id


def update_password(user, old_password=None,
                    new_password=None, new_password_confirmation=None,
                    execute_update=True):
    if not old_password:
        if new_password:
            log_anomaly('password', user.user_id, user.user_id, 'update',
                        'no old password for change password')
            return 'no old password for change password'
        return 'missing field: old_password'
    if not new_password:
        return 'missing field: new_password'
    if not new_password_confirmation:
        return 'missing field: new_password_confirmation'

    if new_password != new_password_confirmation:
        error_msg = 'confirmation mismatch on password change'
        log_anomaly('password', user.user_id, user.user_id, 'update',
                    error_msg)
        return error_msg
    elif not auth_mgr.check_password(user.user_id, old_password):
        error_msg = 'old password mismatch on change'
        log_anomaly('password', user.user_id, user.user_id, 'update',
                    error_msg)
        return error_msg
    elif len(new_password) < 6:
        return 'new password was less than 6 characters'

    if execute_update:
        update_user(user_id=user.user_id, actor_id=user.user_id,
                    password=new_password)


def sanitize_and_update_user_params(user, vanity=None, fname=None, lname=None, about=None,
                                    facebook=None, twitter=None, youtube=None, new_password=None,
                                    old_password=None, new_password_confirmation=None, email=None, longitudes=None,
                                    latitudes=None, location_names=None):
    extra_redirect_args = ''

    if vanity == 'Username (Optional)':
        vanity = None
    if get_unique(vanity=vanity):
        vanity = None
    if isinstance(vanity, str):
        vanity = vanity.strip()

    if facebook and not facebook.startswith('http'):
        facebook = 'http://' + facebook

    if youtube and not youtube.startswith('http'):
        youtube = 'http://' + youtube
    if youtube and youtube != user.Youtube:
        settings_mgr.reset_youtube_flag(user.user_id)

    if twitter:
        twitter = twitter.replace('@', '')

    if email != user.email:
        token = setup_update_email(user.user_id, email)
        log_state_change('change_email', token, 'create', '/processSettings')
        if token:
            extra_redirect_args += 'emc=1&email=' + parse.quote_plus(email)
            send_change_email(user.user_id, email, token)
        else:
            log_anomaly('email', user.user_id, user.user_id, 'update', 'unable to reset')
            extra_redirect_args += 'err=1&'

    error_msg = update_password(user, old_password=old_password,
                                new_password=new_password, new_password_confirmation=new_password_confirmation,
                                execute_update=False)
    if error_msg:
        new_password = None
        if error_msg in ['confirmation mismatch on password change',
                         'missing field: new_password_confirmation',
                         'old password mismatch on change',
                         'new password was less than 6 characters']:
            extra_redirect_args += 'passError=1&'
        elif error_msg == 'no old password for change password':
            extra_redirect_args += 'passError=2&'

    log_state_change('user', user.user_id, 'update', '/processSettings')
    update_user(user_id=user.user_id, actor_id=user.user_id,
                vanity=vanity, first_name=fname, last_name=lname, about=about,
                youtube=youtube, twitter=twitter, facebook=facebook,
                password=new_password)

    if latitudes and longitudes and location_names and len(latitudes) == len(longitudes) == len(location_names):
        user_location_mgr.delete_by_user_id(user.user_id)
        log_state_change('user_locations', user.user_id, 'delete_all', '/processSettings')
        for latitude, longitude, name in zip(latitudes, longitudes, location_names):
            log_state_change('user_locations', user.user_id, 'create', '/processSettings')
            user_location_mgr.add_user_location(user.user_id, latitude, longitude, name)

    elif not latitudes and not longitudes and not location_names:
        user_location_mgr.delete_by_user_id(user.user_id)
        log_state_change('user_locations', user.user_id, 'delete_all', '/processSettings')

    return extra_redirect_args


def update_user(user_id, actor_id, vanity=None, first_name=None,
                last_name=None, about=None, youtube=None, twitter=None,
                facebook=None, image_url=None, thumb_url=None, facebook_id=None,
                gender=None, password=None):
    if not is_authorized(user_id, actor_id):
        raise Exception('Not authorized to edit')

    if user_id is None:
        return None

    if password is not None:
        password = auth_mgr.hash_password(password)

    settings_dict = collections.filter_dict({
        'Vanity': vanity,
        'FName': first_name,
        'LName': last_name,
        'About': about,
        'Youtube': youtube,
        'Twitter': twitter,
        'Facebook': facebook,
        'FBID': facebook_id,
        'Gender': gender,
        'Password': password,
        'ImageUrl': image_url,
        'ThumbUrl': thumb_url
    })
    if settings_dict != {}:
        User.get(user_id=user_id).update(settings_dict)
    return User.get(user_id=user_id)


def remove_fb(user_id):
    if user_id is None:
        return None

    User.get(user_id=user_id).update({'FBID': None})
    return User.get(user_id=user_id)


def merge_user_fb(password_user_id, facebook_id):
    if password_user_id is None or facebook_id is None:
        return None

    result = User.get_unique(facebook_id=facebook_id)
    if result is not None:
        return None

    User.get(user_id=password_user_id).update({'FBID': facebook_id})
    return User.get(user_id=password_user_id)


def find_suggest_creators_by_creator_id(creator_id):
    suggested_pledges = pledge_dbm.find_suggested_by_creator_id(creator_id)
    suggested_user_ids = [pledge.CID for pledge in suggested_pledges if pledge.CID != creator_id]
    return get_users(suggested_user_ids)


def send_change_email(user_id, email, token):
    return mail.send_template_email(
        ChangeEmail(
            get(user_id), email,
            'https://' + patreon.config.main_server + '/changeEmail?ver=' + token
        )
    )


def user_has_enabled_automatic_payouts(user_id):
    user_extra = UserExtra.query.filter(UserExtra.user_id == user_id).first()
    if not user_extra:
        return False
    return user_extra.is_auto_pay
