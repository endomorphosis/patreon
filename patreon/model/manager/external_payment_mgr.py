import datetime
from patreon.model.manager import card_mgr
from patreon.model.table import ExternalPayment


def insert_minimal(transaction_id, payment_instrument_id, amount_cents):
    return ExternalPayment.insert({
        'txn_id': transaction_id,
        'payment_instrument_id': payment_instrument_id,
        'type': card_mgr.get_card_type_string(payment_instrument_id),
        'amount_cents': amount_cents
    })


def update_success(external_payment_id, external_id, fee_cents):
    external_payment = ExternalPayment.get(external_payment_id)
    external_payment.update({
        'external_id': external_id,
        'fee_cents': fee_cents,
        'succeeded_at': datetime.datetime.now()
    })


def update_failed(external_payment_id):
    external_payment = ExternalPayment.get(external_payment_id)
    external_payment.update({
        'failed_at': datetime.datetime.now()
    })


def update_pending(external_payment_id):
    external_payment = ExternalPayment.get(external_payment_id)
    external_payment.update({
        'pending_at': datetime.datetime.now()
    })
