import datetime
from sqlalchemy import desc

from patreon.model.dbm import new_pledge_dbm
from patreon.model.manager import campaign_mgr, pledge_helper
from patreon.model.table import NewPledge, Campaign, CampaignsUsers
from patreon.services.db import db_session
from patreon.services.logging.unsorted import log_state_change
from patreon.util import datetime as datetime_helper


def make_or_update_pledge(user_id, campaign_id, amount_cents, pledge_cap_amount_cents=None,
                          reward_tier_id=None, pledge_vat_location_id=None, created_at=None,
                          patron_pays_fees=0, address_id=None):
    pledge_helper.check_pledge_cap_amount_is_multiple_of_pledge(amount_cents, pledge_cap_amount_cents)

    new_pledge_dbm.make_or_update_pledge(
        user_id=user_id,
        campaign_id=campaign_id,
        amount_cents=amount_cents,
        pledge_cap_amount_cents=pledge_cap_amount_cents,
        reward_tier_id=reward_tier_id,
        pledge_vat_location_id=pledge_vat_location_id,
        created_at=created_at,
        patron_pays_fees=patron_pays_fees,
        address_id=address_id
    )
    log_state_change(
        'new_pledge', str((user_id, campaign_id)), 'replace',
        'create_or_update_pledge'
    )

    return get_pledge_by_patron_and_campaign(user_id, campaign_id)


def delete_pledge(user_id, campaign_id):
    log_state_change(
        'pledge', str((user_id, campaign_id)), 'delete', 'delete_pledge'
    )
    return new_pledge_dbm.make_or_update_pledge(
        user_id=user_id,
        campaign_id=campaign_id,
        amount_cents=0
    )


def get_pledge_by_patron_and_campaign(patron_id, campaign_id):
    return new_pledge_dbm.get_pledge_by_patron_and_campaign(
        patron_id, campaign_id
    )


def historic_pledges_by_patron_and_campaign(patron_id, campaign_id):
    return new_pledge_dbm.all_historic_pledges_by_patron_and_campaign_in_order(
        patron_id, campaign_id
    )


def find_pledges_to_user_id_between_timestamps(
        user_id, start_time=None, end_time=None):
    creations_and_edits = find_pledge_creations_or_edits_to_user_id_between_timestamps(
        user_id, start_time, end_time
    )
    deletions = find_pledge_deletions_to_user_id_between_timestamps(
        user_id, start_time, end_time
    )
    return list(set(creations_and_edits + deletions))


def find_pledge_creations_or_edits_to_user_id_between_timestamps(
        user_id, start_time=None, end_time=None):
    return _find_pledges_to_user_id_between_timestamps_for_column(
        user_id, NewPledge.created_at, start_time, end_time
    )


def find_pledge_deletions_to_user_id_between_timestamps(
        user_id, start_time=None, end_time=None):
    return _find_pledges_to_user_id_between_timestamps_for_column(
        user_id, NewPledge.deleted_at, start_time, end_time
    )


def _find_pledges_to_user_id_between_timestamps_for_column(
        user_id, column_name, start_time=None, end_time=None):
    query = NewPledge.query
    if start_time:
        query = query.filter(column_name >= start_time)
    if end_time:
        query = query.filter(column_name < end_time)
    return query\
        .join(NewPledge.campaign)\
        .join(Campaign.campaigns_users) \
        .filter(CampaignsUsers.user_id == user_id)\
        .all()


def find_months_with_pledges_to_user_id(
        user_id, limit=10, cursor_date=None, timezone_=None):
    created_or_edited_months = find_months_with_created_or_edited_pledges_to_user_id(
        user_id, limit, cursor_date, timezone_
    )
    deleted_months = find_months_with_deleted_pledges_to_user_id(
        user_id, limit, cursor_date, timezone_
    )
    all_months = list(set(created_or_edited_months + deleted_months))
    sorted_months = sorted(all_months, reverse=True)
    return sorted_months[:limit]


def find_months_with_created_or_edited_pledges_to_user_id(
        user_id, limit=10, cursor_date=None, timezone_=None):
    return _find_months_with_pledges_to_user_id_for_column(
        user_id, NewPledge.created_at, limit, cursor_date, timezone_
    )


def find_months_with_deleted_pledges_to_user_id(
        user_id, limit=10, cursor_date=None, timezone_=None):
    return _find_months_with_pledges_to_user_id_for_column(
        user_id, NewPledge.deleted_at, limit, cursor_date, timezone_
    )


def _find_months_with_pledges_to_user_id_for_column(
        user_id, column_name, limit=10, cursor_date=None, timezone_=None):
    created_month = datetime_helper.timezone_aware_sql_month_func(
        column_name, timezone_=timezone_
    )
    query = db_session.query(created_month) \
        .join(NewPledge.campaign)\
        .join(Campaign.campaigns_users)\
        .filter(CampaignsUsers.user_id == user_id)

    if cursor_date:
        newest_timestamp = datetime_helper.datetime_at_midnight_on_date(
            cursor_date + datetime.timedelta(1), timezone_)
        query = query.filter(column_name < newest_timestamp)

    query = query\
        .order_by(desc(column_name)) \
        .group_by(created_month)\
        .limit(limit)
    rows = query.all()
    return [row[0] for row in rows if row[0] is not None]


def any_pledges_to_creator_exist(user_id):
    campaign_id = campaign_mgr.try_get_campaign_id_by_user_id_hack(user_id)
    return new_pledge_dbm.all_undeleted_pledges_to_campaign(campaign_id) \
        .limit(1).count()


def pledger_ids_to_campaign_gte_amount(campaign_id, amount):
    pledges = new_pledge_dbm.all_undeleted_pledges_to_campaign(campaign_id) \
        .filter(NewPledge.amount_cents >= amount)\
        .all()
    return [pledge.user_id for pledge in pledges]


def get_patron_ids_for_campaign_id(campaign_id):
    return pledger_ids_to_campaign_gte_amount(campaign_id, 1)


def patron_has_any_vat_pledges(patron_id):
    if not patron_id:
        return False
    return new_pledge_dbm.patron_has_any_vat_pledges(patron_id)


def patron_has_any_pledges_to_monthly_creators(patron_id):
    return new_pledge_dbm.patron_has_any_pledges_to_monthly_creators(patron_id)
