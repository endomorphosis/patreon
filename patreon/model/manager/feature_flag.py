from _sha256 import sha256

from OpenSSL import rand
import hashlib
import patreon
from patreon.model.table import ABTestAssignments
from patreon.services.caching import request_cached
from patreon.services.db import db_session
from patreon.util.unsorted import get_int
from patreon.services import analytics


# These accounts are used for testing, and shouldn't see test behavior, unless
# the test is at 100%
EXCEPTED_ACCOUNTS = [
    642597,  # no-reply+test-integration@patreon.com
    651369,  # no-reply+test-creator@patreon.com
    916656,  # no-reply+test-patron-of-creator@patreon.com
]


def get_abtest_assignment(feature_name, *args, **kwargs):
    in_test_group = is_enabled(feature_name, *args, **kwargs)

    # we'll consider the a/b test to be stopped under one of two conditions
    # it's turned off explicitly (global flag)
    # or turned off implicitly (cutoff of zero)
    if is_globally_disabled(feature_name):
        return in_test_group

    if get_percentage_enabled(feature_name) == 0:
        return in_test_group

    analytics.log_patreon_event('ABTest: assign', event_properties={
        "test_name": feature_name,
        "in_test_group": in_test_group
    })

    return in_test_group


def is_enabled(feature_name, group_id=None, user_id=None):
    if feature_name not in publicly_visible_feature_flags():
        raise Exception('"{feature_name}" isn\'t a known feature flag'.format(
            feature_name=feature_name
        ))

    if group_id is None and user_id is None:
        raise Exception("A group_id or a user_id is needed to check feature flags")

    group_id = group_id or get_group_id_for_user(user_id)

    if is_globally_disabled(feature_name):
        return False

    if is_user_enabled(feature_name, user_id):
        return True

    cutoff = get_percentage_enabled(feature_name)

    if cutoff >= 1:
        return True
    elif user_id in EXCEPTED_ACCOUNTS:
        return False
    elif cutoff > 0:
        hash_seed = '{0}-{1}'.format(feature_name, group_id)
        hash_obj = hashlib.md5(hash_seed.encode('ascii'))
        # Grab the first 4 bytes of the hash and turn it into an int.
        int_value = int(hash_obj.hexdigest()[:8], 16)
        # Convert 32-bit integer to ratio.
        fractional_value = int_value / (1 << 32)
        # Make sure it's underneath the cutoff.
        return fractional_value <= cutoff
    else:
        return False


def is_user_enabled(feature_name, user_id):
    """
    Test specifically for whether a user is forced into a test group
    """
    key = _enabled_user_key(feature_name, user_id)
    return patreon.pstore.get(key, False)


def is_globally_disabled(feature_name):
    key = _disabled_key(feature_name)
    return patreon.pstore.get(key, False)


def set_user_enabled(feature_name, user_id):
    user_id = get_int(user_id)
    if user_id == 0:
        raise Exception("Can't set a user flag without a real user id")
    key = _enabled_user_key(feature_name, user_id)
    patreon.pstore.set(key, True)


def remove_user_enabled(feature_name, user_id):
    user_id = get_int(user_id)
    if user_id == 0:
        raise Exception("Can't set a user flag without a real user id")
    key = _enabled_user_key(feature_name, user_id)
    patreon.pstore.set(key, False)


def set_percentage_enabled(feature_name, percentage):
    key = _enabled_user_percentage_key(feature_name)
    patreon.pstore.set(key, percentage)


def get_percentage_enabled(feature_name):
    key = _enabled_user_percentage_key(feature_name)
    return patreon.pstore.get(key, 0.0)


def set_global_disabled_state(feature_name, is_disabled):
    key = _disabled_key(feature_name)
    patreon.pstore.set(key, is_disabled)


def _enabled_user_key(feature_name, user_id):
    group_id = get_group_id_for_user(user_id)
    return 'feature_flag:{0}:{1}'.format(feature_name, group_id)


def _enabled_user_percentage_key(feature_name):
    return 'feature_flag:{0}:percentage'.format(feature_name)


def _disabled_key(feature_name):
    return 'feature_flag:{0}:disabled'.format(feature_name)


def _identifier_for_session_or_user(session, user_id):
    if user_id is not None:
        identifier = user_id
    elif session.user_id:
        identifier = session.user_id
    else:
        identifier = 'session:' + session.session_id
    return identifier


def publicly_visible_feature_flags():
    return [
        'vatmoss',
        'monthly_pledge_notifications',
        'post_page_v2',
        'real_time_comments',
        'be_patron_single_page',
        'comment_reply_notifications',
        'dummy_test',
        'flat_comments',
        'navigation_discover',
        'dmca_takedown',
        'log_to_database',
        'hide_default_pledge_input',
        'creator_page_v2',
        'alert_creator_signup',
        'alert_verify_email',
        'home_page_v2',
        'no_creator_popup'
    ]


def enabled_features_for_user(session=None, user_id=None):
    user_id = user_id or session.user_id
    return [
        feature
        for feature in publicly_visible_feature_flags()
        if is_enabled(feature, user_id=user_id)
    ]


def enabled_features_for_user_as_string(session=None, user_id=None):
    user_id = user_id or session.user_id
    return ", ".join(enabled_features_for_user(session, user_id))


@request_cached()
def get_group_id_for_user(user_id):
    row = ABTestAssignments.query.filter_by(user_id=user_id).first()
    if not row:
        return None

    return row.group_id


def insert_group_id(user_id, group_id):
    ABTestAssignments.insert_ignore({
        'user_id': user_id,
        'group_id': group_id
    })
    db_session.commit()


def get_or_set_group_id_for_user(user_id, set_group_id):
    new_group_id = set_group_id or sha256(rand.bytes(2560)).hexdigest()
    if user_id:
        old_group_id = get_group_id_for_user(user_id)
        if not old_group_id:
            insert_group_id(user_id, new_group_id)
        new_group_id = get_group_id_for_user(user_id)

    return new_group_id


def set_group_id_cookie(user_group_id):
    @patreon.model.request.call_after_request
    def set_group_id(response):
        if patreon.config.main_server.endswith('patreon.com'):
            domain = 'patreon.com'
        else:
            # Use default value (current hostname)
            domain = None

        # TWENTY YEARS
        max_age = 20 * 365 * 24 * 3600
        response.set_cookie('group_id', user_group_id, max_age=max_age, httponly=True, secure=True, domain=domain)
        return response
