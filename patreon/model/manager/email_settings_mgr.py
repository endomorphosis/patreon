from patreon.model.manager import campaign_mgr
from patreon.model.table import UserSetting
from patreon.services import db


def get_all_campaign_email_settings_for_user(user_id):
    # TODO MARCOS also get global settings
    return UserSetting.query\
        .filter(UserSetting.user_id == user_id)


def should_creator_receive_email_on_patron_post(creator_id, campaign_id):
    return campaign_mgr.get_campaign_user(user_id=creator_id, campaign_id=campaign_id)\
        .should_email_on_patron_post


def get_campaign_email_settings_for_user(campaign_id, users_id):
    return get_campaign_email_settings_for_users(campaign_id, [users_id])[0]


def get_email_settings_for_users(user_ids):
    return UserSetting.query\
        .filter(UserSetting.user_id.in_(user_ids))\
        .all()


def get_campaign_email_settings_for_users(campaign_id, user_ids):
    if not user_ids:
        return {}

    user_id_list = "(" + ", ".join([str(user_id) for user_id in user_ids]) + ")"

    return db.query("""
        SELECT
            us.`user_id`                      AS `user_id`,
            %s                                AS `campaign_id`,
            us.`email_new_comment`            AS `email_new_comment`,
            us.`email_goal`                   AS `email_goal`,
            us.`email_patreon`                AS `email_patreon`,
            us.`disable_email_post`           AS `disable_email_post`,
            us.`disable_email_paid_post`      AS `disable_email_paid_post`,
            ucs.`email_post`                  AS `email_post`,
            ucs.`email_paid_post`             AS `email_paid_post`,
            cu.`should_email_on_patron_post`  AS `should_email_on_patron_post`,
            cu.`should_email_on_new_patron`   AS `should_email_on_new_patron`
        FROM
            `user_settings` AS us
        LEFT JOIN
            `user_campaign_settings` AS ucs
        ON
            us.`user_id` = ucs.`user_id`
        LEFT JOIN
            `campaigns_users` AS cu
        ON
             us.`user_id` = cu.`user_id`
        WHERE
            us.`user_id` IN """ + user_id_list + """
        AND
            (
                ucs.`campaign_id` = %s
            OR
                cu.`campaign_id` = %s
            )
        """, [campaign_id, campaign_id, campaign_id])
