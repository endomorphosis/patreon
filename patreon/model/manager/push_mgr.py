from patreon.model.manager import campaign_mgr, new_pledge_mgr
from patreon.model.table import UsersPushInfo, UserCampaignSetting
from patreon.services import db
from patreon.services.aws import SNS
from datetime import datetime


def push_infos_for_user_ids(user_ids):
    return UsersPushInfo.query.filter(UsersPushInfo.user_id.in_(user_ids)).all()


def get_with_token(user_id, bundle_id, token):
    return UsersPushInfo.query.filter_by(user_id=user_id, bundle_id=bundle_id, token=token).first()


def update_or_create(user_id, bundle_id, token):
    push_info = UsersPushInfo.get(user_id=user_id, bundle_id=bundle_id)
    if push_info and push_info.token == token:
        return push_info

    values_dict = {
        'token': token,
        'edited_at': datetime.now()
    }
    if push_info:
        update_user_endpoint(push_info, token)
        push_info.update(values_dict)
    else:
        sns_arn = create_user_endpoint(user_id, bundle_id, token)
        values_dict.update({
            'user_id': user_id,
            'bundle_id': bundle_id,
            'sns_arn': sns_arn,
            'created_at': values_dict['edited_at']
        })
        UsersPushInfo.insert(values_dict)

    return UsersPushInfo.get(user_id=user_id, bundle_id=bundle_id)


def application_arn_for_bundle_id(bundle_id):
    suffix = None
    if bundle_id == 'com.patreon.iphone.beta':
        suffix = 'app/APNS/Patreon-iPhone-Beta'
    elif bundle_id == 'com.patreon.android.dev':
        suffix = 'app/GCM/Patreon-Android-Dev'
    elif bundle_id in ['com.patreon.android', 'com.patreon.android.beta']:
        suffix = 'app/GCM/Patreon-Android-Prod'
    if suffix:
        return SNS.arn_prefix() + suffix
    else:
        return None


def update_user_endpoint(push_info, token):
    SNS().set_endpoint_attributes(
        endpoint_arn=push_info.sns_arn,
        attributes={'Token': token}
    )


def create_user_endpoint(user_id, bundle_id, token):
    response_json = SNS().create_platform_endpoint(
        platform_application_arn=application_arn_for_bundle_id(bundle_id),
        token=token,
        custom_user_data=str(user_id)
    )
    return response_json['CreatePlatformEndpointResponse']['CreatePlatformEndpointResult']['EndpointArn']


# taken from email_settings_mgr
def should_creator_receive_push_on_patron_post(creator_id, campaign_id):
    return campaign_mgr.get_campaign_user(user_id=creator_id, campaign_id=campaign_id) \
        .should_push_on_patron_post


def get_user_ids_pushed_for_presence_on_post(post):
    pledger_ids = new_pledge_mgr.pledger_ids_to_campaign_gte_amount(
        post.campaign_id, post.min_cents_pledged_to_view
    )
    campaign_id = post.campaign_id
    for pledger_id in pledger_ids:
        UserCampaignSetting.get.prime(pledger_id, campaign_id)
    settings = [UserCampaignSetting.get(pledger_id, campaign_id)
                for pledger_id in pledger_ids]
    return [setting.user_id
            for setting in settings
            if setting and setting.push_creator_live]


def get_count_of_users_pushed_for_presence_on_post(post):
    user_ids = get_user_ids_pushed_for_presence_on_post(post)
    return UsersPushInfo.query \
        .filter(UsersPushInfo.user_id.in_(user_ids)) \
        .group_by(UsersPushInfo.user_id) \
        .count()


# taken from email_settings_mgr
def get_campaign_push_settings_for_users(campaign_id, user_ids):
    if not user_ids:
        return {}

    user_id_list = "(" + ", ".join([str(user_id) for user_id in user_ids]) + ")"

    return db.query("""
        SELECT
            us.`user_id`                     AS `user_id`,
            %s                               AS `campaign_id`,
            us.`push_new_comment`            AS `push_new_comment`,
            us.`push_goal`                   AS `push_goal`,
            us.`push_patreon`                AS `push_patreon`,
            ucs.`push_post`                  AS `push_post`,
            ucs.`push_paid_post`             AS `push_paid_post`,
            cu.`should_push_on_patron_post`  AS `should_push_on_patron_post`,
            cu.`should_push_on_new_patron`   AS `should_push_on_new_patron`
        FROM
            `user_settings` AS us
        LEFT JOIN
            `user_campaign_settings` AS ucs
        ON
            us.`user_id` = ucs.`user_id`
        LEFT JOIN
            `campaigns_users` AS cu
        ON
             us.`user_id` = cu.`user_id`
        WHERE
            us.`user_id` IN """ + user_id_list + """
        AND
            (
                ucs.`campaign_id` = %s
            OR
                cu.`campaign_id` = %s
            )
        """, [campaign_id, campaign_id, campaign_id])
