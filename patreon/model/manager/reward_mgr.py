import datetime

import collections
import patreon
from patreon.constants import money
from patreon.exception.reward_errors import RewardNotFound, RewardEditForbidden
from patreon.model.dbm import reward_dbm
from patreon.model.table import Reward
from patreon.services.logging.unsorted import log_state_change, log_anomaly
from patreon.util.collections import filter_dict
from patreon.util.html_cleaner import clean
from patreon.util.unsorted import get_int, dollar_string_to_cents


def get(reward_id):
    return Reward.get(reward_id=reward_id)


def get_reward_or_throw(reward_id):
    reward = get(reward_id)
    if reward is None:
        raise RewardNotFound(reward_id)
    return reward


@patreon.services.caching.multiget_cached(object_key='creator_id', default_result=list)
def find_by_creator_id(creator_id):
    return Reward.query.filter(Reward.UID.in_(creator_id)) \
        .order_by(Reward.Amount).all()


def find_highest_reward_by_amount(creator_id, amount):
    rewards = reward_dbm.find_by_creator_id(creator_id).order_by(Reward.Amount.desc())
    for reward in rewards:
        if reward.Amount <= amount and (reward.UserLimit is None or reward.count < reward.UserLimit):
            return reward
    return None


def get_default_pledge_by_creator_id(creator_id):
    result = reward_dbm.get_minimum_reward_amount_by_creator_id(creator_id)
    if result is None or result.min is None:
        return None

    if result.min and result.min < 1:
        return format(1, '.2f')
    return format(result.min / 100, '.2f')


def insert(amount, description, shipping, limit, user_id):
    return Reward.insert({
        'Amount': amount,
        'Description': description,
        'Shipping': shipping,
        'UserLimit': get_int(limit),
        'UID': user_id,
        'Created': datetime.datetime.now()
    })


def update(reward_id, actor_id, amount=None, description=None, shipping=None, limit=None):
    if not is_authorized(reward_id, actor_id):
        raise RewardEditForbidden(reward_id)

    reward_dict = filter_dict({
        'Amount': amount,
        'Description': description,
        'Shipping': shipping,
        'Created': datetime.datetime.now()
    })
    reward_dict['UserLimit'] = get_int(limit)

    get(reward_id).update(reward_dict)


def delete(reward_id, actor_id):
    if not is_authorized(reward_id, actor_id):
        raise RewardEditForbidden(reward_id)

    get(reward_id).delete()


def create_or_update_reward(user_id, reward_id, amount, description, shipping, limit):
    # Handle mysql overflow case
    amount = min(amount, money.MAX_DB_CENTS)

    if not reward_id:
        reward_id = insert(amount, description, shipping, limit, user_id)
        log_state_change('reward', reward_id, 'create', '/create4')
        return True

    reward = get(reward_id=reward_id)
    if not reward:
        log_anomaly('reward', reward_id, None, 'update', 'nonexistent')
        return False

    if reward.creator_id != user_id:
        log_anomaly('reward', reward_id, reward.creator_id, 'update', 'unauthorized')
        return False

    update(reward_id, user_id, amount, description, shipping, limit)

    log_state_change('reward', reward_id, 'update', '/create4')
    return True


def set_rewards(user_id, reward_dicts):
    if not reward_dicts:
        return False

    reward_ids = [reward_dict['reward_id'] for reward_dict in reward_dicts]
    delete_missing_rewards(user_id, reward_ids)

    for reward_dict in reward_dicts:
        amount = dollar_string_to_cents(reward_dict['amount'])
        description = clean(reward_dict['description'])

        if not amount:
            continue

        success = create_or_update_reward(
            user_id,
            reward_id=reward_dict['reward_id'],
            amount=amount,
            description=description,
            shipping=reward_dict['shipping'],
            limit=reward_dict['limit']
        )
        if not success:
            return False
    return True


def delete_missing_rewards(user_id, reward_ids):
    current_rewards = find_by_creator_id(user_id)
    for reward in current_rewards:
        if str(reward.reward_id) not in reward_ids:
            delete(reward.reward_id, user_id)
            log_state_change('reward', reward.reward_id, 'delete', '/create4')


def is_authorized(reward_id, actor_id):
    reward = get_reward_or_throw(reward_id)

    authorized_users = reward.campaign.campaigns_users
    if isinstance(authorized_users, collections.Iterable):
        authorized_user_ids = [element.user_id for element in authorized_users]
    else:
        authorized_user_ids = [authorized_users.user_id]
    return get_int(actor_id) in authorized_user_ids
