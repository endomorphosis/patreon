import datetime
import os
import random
from werkzeug.utils import secure_filename

from patreon.constants import image_sizes, mc_key
from patreon.exception.post_errors import BadEmbed
from patreon.model.manager import action_mgr, activity_mgr, bill_mgr, \
    campaign_mgr, email_settings_mgr, pledge_mgr, post_mgr, push_mgr, \
    queue_mgr, rewards_give_mgr, user_mgr
from patreon.model.table import Activity
from patreon.model.type import ActionType, photo_key_type, post_type_enum, \
    PostType, file_bucket_enum, TemporaryUploadCollection, TemporaryUpload, \
    SortType
from patreon.model.push_notifications import NewCreatorPostPush, NewPatronPostPush
from patreon.services import embedder, file_helper, image_getter, mail
from patreon.services.image_blurrer import Blurrer
from patreon.services.caching import memcache
from patreon.services.image_resizer import TopOffsetResizer
from patreon.services.mail.templates import NotifyNewPost
from patreon.services.uploader import uploader
from patreon.util.image_processing import converter, rotator


def patron_post(user_id, campaign_id, post_type, title, content_html, embed_raw,
                min_cents_pledged_to_view=None):
    created_at = datetime.datetime.now()

    # Patrons can choose between public and patron-only.
    if not min_cents_pledged_to_view:
        min_cents_pledged_to_view = 0
    elif min_cents_pledged_to_view < 0:
        min_cents_pledged_to_view = 0
    elif min_cents_pledged_to_view > 0:
        min_cents_pledged_to_view = 1

    post_id = Activity.insert(dict(
        user_id=user_id,
        campaign_id=campaign_id,
        activity_type=post_type.value,
        post_type=post_type.name.lower(),
        activity_title=title,
        activity_content=content_html,
        category=None,
        min_cents_pledged_to_view=min_cents_pledged_to_view,
        is_paid=False,
        is_creation=False,
        photo_key=None,
        created_at=created_at,
        edited_at=created_at
    ))

    # TODO MARCOS HACK Right now, the image setters are built around drafts.
    # I want a more functional style implementation of handle embed, but for
    # now, I'll just have two DB writes.
    if post_type in post_type_enum.EMBED_TYPES:
        embed_dict = _handle_embed(
            post_id, post_type, embed_raw, {}, {}
        )
        if not embed_dict:
            embed_dict = _embed_fallback(embed_raw)
            post_type = PostType.LINK
    else:
        embed_dict = {}

    activity_mgr.get_activity(post_id).update({
        'activity_type': post_type.value,
        'post_type': post_type.name.lower(),
        'link_url': embed_dict.get('url'),
        'link_subject': embed_dict.get('subject'),
        'link_domain': embed_dict.get('provider'),
        'link_description': embed_dict.get('description'),
        'link_embed': embed_dict.get('html'),
    })

    creator_id = campaign_mgr.try_get_user_id_by_campaign_id_hack(campaign_id)
    _publish(post_id, creator_id)
    action_mgr.insert(creator_id, post_id, ActionType.UPLOAD.value)

    post = post_mgr.get_post(post_id)

    # Send Emails.
    current_user = user_mgr.get_user(user_id)
    if current_user.user_id == creator_id:
        _create_bill_and_notify_patrons(current_user, post)
    else:
        _try_notify_creator(current_user, post)

    # Clear the news feed.
    _clear_activity_cache(campaign_id, creator_id)

    # Refresh search.
    queue_mgr.enqueue_post_for_update(post_id)

    return post


def save_post(post_id, user_id, post_type, title, content_html, thumbnail,
              embed_raw, min_cents_pledged_to_view, category, is_paid,
              will_publish):
    """
    When this is and upload type, assume that set_post_file was just called.
    """
    post = activity_mgr.get_activity_or_throw(post_id)
    creator_id = campaign_mgr.try_get_user_id_by_campaign_id_hack(
        post.campaign_id
    )

    if post_type in post_type_enum.EMBED_TYPES:
        _delete_images(post_id)
        embed_dict = _handle_embed(
            post_id, post_type, embed_raw, thumbnail, post.thumbnail_url
        )
        if not embed_dict:
            embed_dict = _embed_fallback(embed_raw)
            post_type = PostType.LINK

    elif post_type in post_type_enum.UPLOAD_TYPES:
        embed_dict = None
    else:
        _delete_images(post_id)
        embed_dict = None

    # `is_paid` is the only field that cannot be changed after publish.
    if post.is_published:
        is_paid = post.is_paid

    old_min_cents_pledged_to_view = post.min_cents_pledged_to_view

    activity_mgr.update_activity(
        post_id=post_id,
        post_type=post_type,
        title=title,
        content=content_html,
        min_cents_pledged_to_view=min_cents_pledged_to_view,
        embed_dict=embed_dict,
        category=category,
        is_paid=is_paid,
    )

    # Editing a post.
    if post.is_published:
        _set_legacy_image_urls(post_id)

        post = post_mgr.get_post(post_id) # Get fresh version of the post.
        current_user = user_mgr.get_user(user_id)
        _notify_patrons_of_update(current_user, post, old_min_cents_pledged_to_view)
        # Refresh search.
        queue_mgr.enqueue_post_for_update(post_id)

    # Publish!
    elif will_publish:
        _publish(post_id, creator_id)
        action_mgr.insert(creator_id, post_id, ActionType.UPLOAD.value)

        post = post_mgr.get_post(post_id) # Get fresh version of the post.

        current_user = user_mgr.get_user(user_id)
        _create_bill_and_notify_patrons(current_user, post)

        _clear_activity_cache(creator_id, creator_id)

        # Refresh search.
        queue_mgr.enqueue_post_for_update(post_id)

    # If this is just saving a draft, short circuit.
    else:
        # Return fresh version of the post.
        post = activity_mgr.get_activity(post_id)

    return post


def set_post_file(post_id, temp_filename, source_filename, source_extension,
                  top_offset=None):
    source_filename = secure_filename(source_filename)
    file_key = str(random.getrandbits(64))

    if converter.is_image_file(temp_filename):
        destination_format = converter.convert_format(source_extension)
        rotator.correct_file_rotation(temp_filename, destination_format)
        _delete_images(post_id)
        set_image(post_id, temp_filename, source_extension)
        set_thumbnail(post_id, temp_filename, top_offset)

    uploader.upload_file(TemporaryUpload(
        temp_filename, file_key + source_extension, file_bucket_enum.config_bucket_post
    ))
    activity_mgr.update_activity_file(post_id, source_filename, file_key)


def set_image(post_id, temp_filename, source_extension):
    destination_format = converter.convert_format(source_extension)

    rotator.correct_file_rotation(temp_filename, destination_format)

    with TopOffsetResizer(temp_filename, image_sizes.POST_SIZES,
                          destination_format) as resizer:
        image_width, image_height = resizer.original_size
        extension = '.' + resizer.destination_format.lower()
        key = uploader.upload_files(
            TemporaryUploadCollection(resizer, file_bucket_enum.config_bucket_post)
        )

        activity_mgr.update_activity_image(
            post_id, image_width, image_height, key, extension
        )


def set_thumbnail(post_id, temp_filename, top_offset=None):
    rotator.correct_file_rotation(temp_filename)

    with TopOffsetResizer(temp_filename, image_sizes.POST_THUMB_SIZES,
                          top_offset=top_offset) as thumbnail_resizer:
        thumbnail_key = uploader.upload_files(
            TemporaryUploadCollection(thumbnail_resizer, file_bucket_enum.config_bucket_post)
        )
        _delete_thumbnail(post_id)
        activity_mgr.update_activity_thumbnail(post_id, thumbnail_key)

        blur_thumbnail_size = image_sizes.POST_THUMB_SQUARE_LARGE
        _store_blurred_thumbnail(
            thumbnail_resizer.resizes()[blur_thumbnail_size.name],
            thumbnail_key
        )


def _store_blurred_thumbnail(temp_filename, thumbnail_key):
    blurrer = Blurrer(temp_filename, thumbnail_key)
    blurred_filename = blurrer.blur()
    uploader.upload_file(TemporaryUpload(
        blurred_filename,
        '{key}_blurred.{ext}'.format(
            key=thumbnail_key,
            ext=blurrer.destination_format.lower()
        ),
        file_bucket_enum.config_bucket_post
    ))


def _publish(post_id, owner_id):
    _set_photo_key_if_missing(post_id)
    _set_legacy_image_urls(post_id)

    published_at = datetime.datetime.now()
    snapshot_pledge = pledge_mgr.get_pledge_total_by_creator_id(owner_id)
    comment_count = 0
    like_count = 0

    activity_mgr.get_activity(post_id).update({
        'published_at': published_at,
        'edited_at': published_at,
        'cents_pledged_at_creation': snapshot_pledge,
        'comment_count': comment_count,
        'like_count': like_count
    })


def _set_legacy_image_urls(post_id):
    """ If they exist, take photo_key and thumbnail_key and write urls to DB."""
    post = activity_mgr.get_activity_or_throw(post_id)

    # defaults
    image_normal_url = image_original_url = image_thumb_url = None
    width, height = post.image_dimensions

    if post.photo_key:
        image_normal_url = photo_key_type.get_image_url(
            file_bucket_enum.config_bucket_post,
            post.photo_key,
            image_sizes.DEPRECATED_POST_SINGLE,
            post.photo_extension
        )
        image_original_url = photo_key_type.get_image_url(
            file_bucket_enum.config_bucket_post,
            post.photo_key,
            image_sizes.POST_LARGE,
            post.photo_extension
        )
        image_thumb_url = image_normal_url

    if post.thumbnail_key:
        image_thumb_url = photo_key_type.get_image_url(
            file_bucket_enum.config_bucket_post,
            post.thumbnail_key,
            image_sizes.DEPRECATED_POST_THUMB
        )

        if not image_normal_url:
            image_normal_url = image_original_url = photo_key_type.get_image_url(
                file_bucket_enum.config_bucket_post,
                post.thumbnail_key,
                image_sizes.POST_THUMB_LARGE_2
            )
            width, height = image_sizes.POST_THUMB_LARGE_2.dimensions

    activity_mgr.get_activity(post_id).update({
        'image_url': image_normal_url,
        'image_large_url': image_original_url,
        'image_thumb_url': image_thumb_url,
        'image_width': width,
        'image_height': height
    })


def _set_photo_key_if_missing(post_id):
    """ If image type but photo_key not set, use post_file or thumbnail. """
    post = activity_mgr.get_activity_or_throw(post_id)
    post_type = PostType.get(post.post_type)
    if post_type in post_type_enum.IMAGE_TYPES and not post.photo_key:
        if post.file_url:
            url = post.file_url
            source_extension = file_helper.get_extension(post.file_name)
        elif post.thumbnail_key:
            url = post.thumbnail_url_size(image_sizes.POST_THUMB_LARGE_2)
            source_extension = '.jpeg'
        else:
            return

        temp_filename = image_getter.fetch_image(url)
        set_image(post_id, temp_filename, source_extension)
        os.remove(temp_filename)


def _reget_embed(embed_raw):
    if not embed_raw or not embed_raw.get('url'):
        raise BadEmbed(None)

    url = embed_raw.get('url')
    embed_json = embedder.get_embed(url)

    if not embed_json:
        raise BadEmbed(url)

    html = embed_json.get('object').get('html')
    images = embed_json.get('images', [{}])
    image_url = images[0].get('url') if len(images) > 0 else None

    return {
        'url':          url,
        'provider':     embed_json.get('provider_name'),
        'provider_url': embed_json.get('provider_url'),
        'subject':      embed_json.get('title'),
        'description':  embed_json.get('description'),
        'html':         html or image_url,
        'image_url':    image_url
    }


def _embed_fallback(embed_raw):
    """ If the handle_embed call returns None, then fallback to LINK. """
    return _reget_embed(embed_raw)


def _handle_embed(post_id, post_type, embed_raw, thumbnail, original_thumbnail):
    """
    This just exist to pull files and thumbnails from urls. If there are any
    error, return null and assume no images were saved.
    """

    embed_dict = _reget_embed(embed_raw)
    embed_image_url = embed_dict.get('image_url')

    if post_type is PostType.IMAGE_EMBED:
        # Upload original file to s3
        if embed_image_url:
            temp_filename = image_getter.fetch_image(embed_image_url)
            content_type = image_getter.content_type_from_url(embed_image_url)
            source_extension = converter.content_type_to_extension(content_type)

            if not temp_filename:
                return None

            if not source_extension:
                os.remove(temp_filename)
                return None

            # We need to preserve the post_file in case it's a GIF.
            embed_file_name = embed_raw.get('subject')

            set_post_file(
                post_id, temp_filename, embed_file_name, source_extension
            )

            # Get image resizes. Use content-type to get destination extension.
            set_image(post_id, temp_filename, source_extension)

            if thumbnail.get('url') is None and original_thumbnail is None:
                top_offset = thumbnail.get('top_offset')
                set_thumbnail(post_id, temp_filename, top_offset)
            os.remove(temp_filename)

    elif thumbnail and original_thumbnail != thumbnail.get('url'):
        embed_thumbnail_url = thumbnail.get('url')
        top_offset = thumbnail.get('top_offset')

        thumbnail_filename = image_getter.fetch_image(embed_thumbnail_url)
        if thumbnail_filename:
            set_thumbnail(post_id, thumbnail_filename, top_offset)
            os.remove(thumbnail_filename)

    return embed_dict


def _clear_activity_cache(campaign_id, owner_id):
    memcache.delete(mc_key.user_shares(owner_id, ActionType.UPLOAD)) # deleting user shares
    memcache.delete(mc_key.user_activity(owner_id, SortType.CONTENT_POPULAR))
    memcache.delete(mc_key.user_activity(campaign_id, SortType.CONTENT_LATEST))
    memcache.delete(mc_key.user_activity(campaign_id, SortType.POPULAR))
    memcache.delete(mc_key.user_activity(campaign_id, SortType.LATEST)) # deleting artist shares
    memcache.delete(mc_key.count_creations(campaign_id))
    memcache.delete(mc_key.count_activity(campaign_id))


def _try_notify_creator(current_user, post):
    creator_id = post.creator_id
    creator = user_mgr.get_user(creator_id)
    campaign_id = post.campaign_id
    if creator.email:
        if email_settings_mgr.should_creator_receive_email_on_patron_post(
                creator_id=creator_id, campaign_id=campaign_id):
            mail.send_template_email(
                NotifyNewPost(current_user, post, [creator])
            )
    if push_mgr.should_creator_receive_push_on_patron_post(
            creator_id=creator_id, campaign_id=campaign_id):
        NewPatronPostPush(post=post, author=current_user, user_ids=[creator_id]).publish()


def _create_bill_and_notify_patrons(creator, post):
    pledge_table = pledge_mgr.get_patrons_and_pledges_for_creator(
        creator.user_id
    )
    if post.is_paid:
        _create_bill(creator.user_id, post.activity_id, pledge_table)
    _notify_patrons(creator, post, pledge_table)


def _create_bill(creator_id, post_id, pledge_table):
    # If this is a paid post, insert a row into tblRewardsGive.
    rewards_give_mgr.create_bill(creator_id, post_id)

    # Create a bill for every patron in the pledge table
    bill_mgr.create_bill_for_every_patron(
        pledge_table,
        creator_id=creator_id,
        post_id=post_id
    )


def _notify_patrons_of_update(creator, post, old_min_cents_pledged_to_view):
    # 1. If the price increased, do nothing.
    if post.min_cents_pledged_to_view >= old_min_cents_pledged_to_view:
        return

    # 2. Get the list of patrons who "probably already got the email".
    # If any users have changed their pledge, then we can get into a weird state.
    old_pledges = pledge_mgr.find_all_by_creator_id_and_above_cents(
        creator.user_id, old_min_cents_pledged_to_view
    )
    notified_user_ids = [
        pledge.patron_id
        for pledge in old_pledges
    ]

    # 3. Get the pledge table.
    pledge_table = pledge_mgr.get_patrons_and_pledges_for_creator(
        creator.user_id
    )

    # 4. Just using set difference, pass it to notify_patrons.
    filtered_pledge_table = {
        patron_id: row
        for patron_id, row in pledge_table.items()
        if patron_id not in notified_user_ids
    }

    _notify_patrons(creator, post, filtered_pledge_table)


def _notify_patrons(creator, post, pledge_table):
    patron_ids = pledge_table.keys()

    if post.is_paid:
        disable_email_key = 'disable_email_paid_post'
        email_key = 'email_paid_post'
        push_key = 'push_paid_post'
    else:
        disable_email_key = 'disable_email_post'
        email_key = 'email_post'
        push_key = 'push_post'

    for patron_id in patron_ids:
        memcache.delete(mc_key.user_support_creation(patron_id))

    # This can be empty if there are no patrons.
    email_settings = email_settings_mgr.get_campaign_email_settings_for_users(
        post.campaign_id, patron_ids
    )

    email_recipients = []
    for email_setting in email_settings:
        patron_id = email_setting['user_id']

        # used for email
        patron = pledge_table[patron_id]["patron"]

        # used for min_cents_pledged_to_view
        pledge = pledge_table[patron_id]["pledge"]

        if (patron
            and patron.email
            and patron.user_id != creator.user_id
            and pledge.amount >= post.min_cents_pledged_to_view
            and not email_setting[disable_email_key]
            and email_setting[email_key]):
            email_recipients.append(patron)

    if email_recipients:
        mail.send_template_email(NotifyNewPost(creator, post, email_recipients))

    push_settings = push_mgr.get_campaign_push_settings_for_users(
        post.campaign_id, patron_ids
    )
    push_recipients = []
    for push_setting in push_settings:
        patron_id = push_setting['user_id']

        # used for email
        patron = pledge_table[patron_id]["patron"]

        # used for min_cents_pledged_to_view
        pledge = pledge_table[patron_id]["pledge"]

        if (patron
            and patron.user_id != creator.user_id
            and pledge.amount >= post.min_cents_pledged_to_view
            and push_setting[push_key]):
            push_recipients.append(patron)

    if push_recipients:
        NewCreatorPostPush(
            author=creator,
            post=post,
            user_ids=[recipient.user_id for recipient in push_recipients]
        ).publish()


def _delete_images(post_id):
    _delete_post_file(post_id)
    _delete_thumbnail(post_id)
    _delete_legacy_images(post_id)


def _delete_post_file(post_id):
    # TODO MARCOS delete from s3
    activity_mgr.update_activity_file(post_id, None, None)


def _delete_legacy_images(post_id):
    # TODO MARCOS delete from s3
    activity_mgr.update_activity_image(post_id, 0, 0, None, None)


def _delete_thumbnail(post_id):
    # TODO MARCOS delete from s3
    activity_mgr.update_activity_thumbnail(post_id, None)
