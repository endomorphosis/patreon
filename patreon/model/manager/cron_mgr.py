from patreon.services import db
from patreon.services.logging import rollbar
from patreon.util import datetime


def run_cron(function, args=None, kwargs=None, owner=None, ping_on_failure=True):
    if args is None:
        args = ()
    if kwargs is None:
        kwargs = {}
    start_time = datetime.datetime.now()
    success = True
    try:
        function(*args, **kwargs)
    except Exception as e:
        success = False
        rollbar.log_current_exception("Exception raised while running cron")

    status = 1 if success else 0
    end_time = datetime.datetime.now()
    function_module = function.__module__
    function_name = function.__name__

    try:
        db.query_log("""
            INSERT INTO cron_log (
                owner, function_module, function_name,
                started_at, finished_at,
                status
            ) VALUES (
                %s, %s, %s,
                %s, %s,
                %s
                )
            """, (
                owner, function_module, function_name,
                start_time, end_time,
                status
        ))
    except Exception as e:
        rollbar.log_current_exception("Exception raised attempting to log to cron_log")
