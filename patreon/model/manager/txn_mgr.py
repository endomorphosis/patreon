import datetime
from patreon.model.manager.payment_event_mgr import capture_exception_payment_events
from patreon.model.table import TxnHistory, Txn


@capture_exception_payment_events
def mark_as_processing(transaction_id):
    transaction = Txn.get(transaction_id)
    process_time = datetime.datetime.now()
    transaction.update({
        'failed_at': None,
        'pending_at': None,
        'began_processing_at': process_time
    })
    TxnHistory.insert({
        'txn_id': transaction_id,
        'txn_began_processing_at': process_time
    })


@capture_exception_payment_events
def mark_as_succeeded(transaction_id):
    transaction = Txn.get(transaction_id)
    succeeded_at = datetime.datetime.now()
    transaction.update({
        'began_processing_at': None,
        'succeeded_at': succeeded_at
    })
    TxnHistory.insert({
        'txn_id': transaction_id,
        'txn_succeeded_at': succeeded_at
    })


@capture_exception_payment_events
def mark_as_failed(transaction_id):
    transaction = Txn.get(transaction_id)
    failed_at = datetime.datetime.now()
    transaction.update({
        'began_processing_at': None,
        'failed_at': failed_at
    })
    TxnHistory.insert({
        'txn_id': transaction_id,
        'txn_failed_at': failed_at
    })


@capture_exception_payment_events
def create_transaction(type):
    txn_id = Txn.insert({
        'type': type
    })
    transaction = Txn.get(txn_id)
    TxnHistory.insert({
        'txn_id': txn_id,
        'txn_created_at': transaction.created_at
    })
    return txn_id


def mark_as_pending(transaction_id):
    transaction = Txn.get(transaction_id)
    succeeded_at = datetime.datetime.now()
    transaction.update({
        'began_processing_at': None,
        'pending_at': succeeded_at
    })
    TxnHistory.insert({
        'txn_id': transaction_id,
        'txn_pending_at': succeeded_at
    })


def clear_processing(transaction_id):
    transaction = Txn.get(transaction_id)
    transaction.update({
        'began_processing_at': None
    })
