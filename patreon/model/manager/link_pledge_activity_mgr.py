from patreon.model.dbm import link_pledge_activity_dbm
from patreon.model.table import LinkPledgeActivity


def get(activity_id, user_id):
    return LinkPledgeActivity.query.\
        filter_by(HID=activity_id, UID=user_id)\
        .first()


def find_by_activity_id(activity_id):
    return link_pledge_activity_dbm.find_by_activity_id(activity_id).all()


def find_by_user_id(user_id):
    return link_pledge_activity_dbm.find_by_user_id(user_id).all()


def insert(activity_id, user_id, reward_id):
    return LinkPledgeActivity.insert({
        'HID': activity_id,
        'UID': user_id,
        'RID': reward_id
    })


def update(activity_id, user_id, reward_id):
    link = get(activity_id, user_id)
    if not link:
        return

    link.update({
        'RID': reward_id
    })
