from patreon.constants import TxnTypes
from patreon.exception.payment_errors import ExternalPaymentAlreadyFailed, TransactionAlreadyFailed, \
    TransactionInProcess, PendingPaypalTransaction
from patreon.model.manager import external_payment_mgr, txn_mgr, card_mgr, account_mgr, ledger_entry_mgr, \
    payment_event_mgr
from patreon.model.table import CreditPurchase, CreditPurchaseTxn
from patreon.services.db import db_session


def create_transaction():
    return txn_mgr.create_transaction(CreditPurchaseTxn.__txn_type__)


def create_obligation(transaction_id, payment_instrument_id, amount_cents, user_id, campaign_id):
    return CreditPurchase.insert({
        'txn_id': transaction_id,
        'payment_instrument_id': payment_instrument_id,
        'amount_cents': amount_cents,
        'purchasing_user_id': user_id,
        'campaign_id': campaign_id
    })


def touch_accounts(transaction_id):
    transaction = CreditPurchaseTxn.get(transaction_id)
    payment_instrument_id = transaction.payment_instrument_id
    user_id = transaction.user_id

    account_mgr.touch_user_credit_account_id(user_id)
    account_mgr.touch_external_payment_account_id(user_id, payment_instrument_id)

    account_mgr.touch_patreon_external_transaction_fee_faucet_account_id()
    account_mgr.touch_external_transaction_fee_sink_account_id()
    account_mgr.touch_patron_fee_sink_account_id()


def initialize(payment_instrument_id, amount_cents, user_id, campaign_id):
    transaction_id = create_transaction()
    create_obligation(transaction_id, payment_instrument_id, amount_cents, user_id, campaign_id)
    return transaction_id


def find_or_create_external_payment_for(transaction_id):
    transaction = CreditPurchaseTxn.get(transaction_id)

    amount_cents = transaction.amount_cents
    payment_instrument_id = transaction.payment_instrument_id

    if not transaction.external_payments:
        return external_payment_mgr.insert_minimal(transaction_id, payment_instrument_id, amount_cents)

    external_payment = transaction.most_recent_external_payment
    return external_payment.external_payment_id


def execute(transaction_id):
    transaction = CreditPurchaseTxn.get(transaction_id)

    if transaction.most_recent_external_payment:
        if transaction.most_recent_external_payment.is_successful:
            return transaction.most_recent_external_payment.external_payment_id, \
                transaction.most_recent_external_payment.fee_cents
        elif transaction.most_recent_external_payment.is_failed:
            raise ExternalPaymentAlreadyFailed

    external_payment_id = transaction.most_recent_external_payment.external_payment_id
    payment_instrument_id = transaction.payment_instrument_id
    amount_cents = transaction.amount_cents

    external_api = card_mgr.get_payment_api_for_card_token(payment_instrument_id)

    return external_api.charge_instrument(
        external_payment_id, payment_instrument_id, amount_cents, 'Patreon Credit Purchase'
    )


def mark_transaction_as_processing(transaction_id):
    txn_mgr.mark_as_processing(transaction_id)


def mark_external_payment_as_successful(transaction_id, external_id, external_fee):
    transaction = CreditPurchaseTxn.get(transaction_id)
    external_payment_id = transaction.most_recent_external_payment.external_payment_id
    if transaction.most_recent_external_payment.is_successful:
        # This happens if we have a case where the transaction failed after the charge was marked successful
        return
    external_payment_mgr.update_success(external_payment_id, external_id, external_fee)


def mark_transaction_as_successful(transaction_id):
    txn_mgr.mark_as_succeeded(transaction_id)


def mark_external_payment_as_failed(transaction_id):
    transaction = CreditPurchaseTxn.get(transaction_id)
    external_payment_id = transaction.most_recent_external_payment.external_payment_id
    external_payment_mgr.update_failed(external_payment_id)


def mark_transaction_as_failed(transaction_id):
    txn_mgr.mark_as_failed(transaction_id)


def mark_external_payment_as_pending(transaction_id):
    transaction = CreditPurchaseTxn.get(transaction_id)
    external_payment_id = transaction.most_recent_external_payment.external_payment_id
    external_payment_mgr.update_pending(external_payment_id)


def mark_transaction_as_pending(transaction_id):
    txn_mgr.mark_as_pending(transaction_id)


def clear_processing(transaction_id):
    txn_mgr.clear_processing(transaction_id)


def insert_ledger_entries(transaction_id):
    transaction = CreditPurchaseTxn.get(transaction_id)

    charge_amount = transaction.most_recent_external_payment.amount_cents
    fee_amount = transaction.most_recent_external_payment.fee_cents
    user_id = transaction.user_id
    payment_instrument_id = transaction.payment_instrument_id

    user_credit_account = account_mgr.get_user_credit_account_id_or_throw(user_id)
    user_payment_instrument_account = account_mgr.get_external_payment_account_id_or_throw(
        user_id, payment_instrument_id
    )

    patreon_fee_faucet_account = account_mgr.get_patreon_external_transaction_fee_faucet_account_id_or_throw()
    external_fee_sink_account = account_mgr.get_external_transaction_fee_sink_account_id_or_throw()

    # We charged a payment instrument
    ledger_entry_mgr.insert_ledger_entry(transaction_id, user_payment_instrument_account, -1 * charge_amount)
    # User gets credit minus fees
    ledger_entry_mgr.insert_ledger_entry(transaction_id, user_credit_account, charge_amount - fee_amount)
    # Fees go to an external fee sink
    ledger_entry_mgr.insert_ledger_entry(transaction_id, external_fee_sink_account, fee_amount)
    # We pay the user back for the fees, as we account for this during the payments cron instead of now.
    ledger_entry_mgr.move_money_between_accounts(transaction_id, from_account=patreon_fee_faucet_account,
                                                 to_account=user_credit_account, amount_cents=fee_amount)


def process_transaction(transaction_id):
    with db_session.begin_nested():
        transaction = CreditPurchaseTxn.get_with_lock(transaction_id)
        if transaction.is_successful:
            return transaction_id
        elif transaction.is_failed and transaction.most_recent_external_payment.is_failed:
            raise TransactionAlreadyFailed(transaction_id, TxnTypes.credit_purchase.value)
        elif transaction.is_processing:
            raise TransactionInProcess(transaction_id, TxnTypes.credit_purchase.value)

        find_or_create_external_payment_for(transaction_id)
        mark_transaction_as_processing(transaction_id)
        touch_accounts(transaction_id)
    try:
        # This commits
        #    1. The external payment (if new)
        #    2. The began_processing_at flag on txn
        db_session.commit()
        db_session.expire_all()
        CreditPurchaseTxn.get_with_lock(transaction_id)

        # No, this does not handle the pending paypal case yet
        try:
            external_transaction_id, external_fee = execute(transaction_id)
        except PendingPaypalTransaction as e:
            payment_event_mgr.add_exception_event(e)
            # This is the wrong way to handle this, but it is the expected product effect.
            # The correct thing to do is commented out below
            external_fee = 0

        with db_session.begin_nested():
            mark_external_payment_as_successful(transaction_id,
                                                external_transaction_id,
                                                external_fee)
        # This commits
        #   1. Marking the external payment as successful
        db_session.commit()
        db_session.expire_all()
        CreditPurchaseTxn.get_with_lock(transaction_id)

        with db_session.begin_nested():
            insert_ledger_entries(transaction_id)
            ledger_entry_mgr.assert_transaction_ledger_sane(transaction_id)
            mark_transaction_as_successful(transaction_id)
        return transaction_id
    # This is what we SHOULD be doing with PendingPaypalTransactions but... not today
    # except PendingPaypalTransaction as e:
    #    with db_session.begin_nested():
    #        mark_external_payment_as_pending(transaction_id)
    #
    #    db_session.commit()
    #    db_session.expire_all()
    #    CreditPurchaseTxn.get_with_lock(transaction_id)
    #
    #    with db_session.begin_nested():
    #        mark_transaction_as_pending(transaction_id)
    #    raise e
    except Exception as e:
        CreditPurchaseTxn.get_with_lock(transaction_id)

        with db_session.begin_nested():
            mark_external_payment_as_failed(transaction_id)

        db_session.commit()
        db_session.expire_all()
        CreditPurchaseTxn.get_with_lock(transaction_id)

        with db_session.begin_nested():
            mark_transaction_as_failed(transaction_id)
        raise e
    finally:
        # If the external payment FAILED this commits:
        #   1. Marking the external payment as failed
        #   2. Marking the transaction and object as failed
        # If the external payment SUCCEEDED this commits:
        #   1. Inserting ledger entries
        #   2. Marking the transacation and object as successful
        clear_processing(transaction_id)
        db_session.commit()
        db_session.expire_all()
