from patreon.model.dbm import user_location_dbm


def find_by_user_id(user_id):
    return user_location_dbm.find_by_user_id(user_id)


def find_by_user_id_legacy(user_id):
    return user_location_dbm.find_by_user_id_legacy(user_id)


def delete_by_user_id(user_id):
    if not user_id:
        return None
    find_by_user_id(user_id).delete()
    return find_by_user_id(user_id).all()


def delete_by_location(user_id, latitude, longitude):
    if user_id is None:
        return None
    try:
        latitude = float(latitude)
        longitude = float(longitude)
    except:
        return None
    user_location_dbm.delete_by_location(user_id, latitude, longitude)
    return find_by_user_id(user_id).all()


def insert(user_id, latitude, longitude, display_name):
    if user_id is None:
        return None
    try:
        latitude = float(latitude)
        longitude = float(longitude)
    except:
        return None
    user_location_dbm.insert(user_id, latitude, longitude, display_name)
    return find_by_user_id(user_id).all()


def add_user_location(user_id, latitude, longitude, location_name):
    if user_id is None or latitude is None or longitude is None:
        return None

    try:
        latitude = float(latitude)
        longitude = float(longitude)
    except:
        return None

    insert(user_id, latitude, longitude, location_name)

    return find_by_user_id(user_id).all()