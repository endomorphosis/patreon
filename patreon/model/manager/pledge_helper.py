from collections import namedtuple
from functools import wraps
from patreon.constants.money import BIG_MONEY_CENTS
from patreon.exception.pledge_errors import InvalidCard, \
    ShippingAddressRequired, VatCountryMismatch, InvalidPledgeCap, \
    RewardNotAvailable, InsufficientPledgeForReward, NSFWPaypalPledge
from patreon.model.manager import campaign_mgr, vat_mgr, rewards_wait_mgr, \
    reward_mgr, card_mgr, user_mgr
from patreon.model.type import Address
from patreon.util.unsorted import get_int


def creator_and_campaign_from_campaign_or_creator_relationship(posted_pledge):
    creator_relationship_id = posted_pledge.relationship_id("creator")
    if creator_relationship_id:
        creator_id = creator_relationship_id.assert_type("user").resource_id()
        creator = user_mgr.get(creator_id)
        if not creator:
            raise Exception("Creator not found")

        campaign = campaign_mgr.try_get_campaign_by_user_id_hack(creator.user_id)
        if not campaign:
            raise Exception("Campaign not found")

        return creator, campaign

    campaign_relationship_id = posted_pledge.relationship_id("campaign")
    if campaign_relationship_id:
        campaign_id = campaign_relationship_id.assert_type("campaign").resource_id()
        campaign = campaign_mgr.get(campaign_id)
        if not campaign:
            raise Exception("Campaign not found")

        creator_id = campaign.creator_id
        creator = user_mgr.get(creator_id)
        if not creator:
            raise Exception("Creator not found")

        return creator, campaign

    raise Exception("A relationship to either a creator or a campaign is required")


def reward_id_from_relationship(posted_pledge):
    reward_relationship_id = posted_pledge.relationship_id("reward")
    if reward_relationship_id:
        reward_id = get_int(reward_relationship_id.assert_type("reward").resource_id())
        if reward_id:
            return reward_id
        else:
            return None


def card_id_from_relationship(posted_pledge):
    card_relationship_id = posted_pledge.relationship_id("card")
    if card_relationship_id:
        return card_relationship_id.assert_type("card").resource_id()


def get_valid_card_for_user_or_raise_exception(card_id, user_id):
    card = card_mgr.get_card_by_user_id_card_id(card_id=card_id, user_id=user_id)
    if not card:
        raise InvalidCard()
    return card


def address_from_relationship(posted_pledge):
    address_resource = posted_pledge.related_resource("address")
    if not address_resource:
        return None

    return Address(
        address_resource.attribute("line_1"),
        address_resource.attribute("line_2"),
        address_resource.attribute("postal_code"),
        address_resource.attribute("city"),
        address_resource.attribute("state"),
        address_resource.attribute("country")
    )


def address_is_sufficient_for_reward_tier_id(address, reward_tier_id):
    reward = reward_mgr.get_reward_or_throw(reward_tier_id)
    if not reward.requires_shipping:
        return True
    if not address:
        return False
    if address.has_all_required_fields():
        return True
    return False


def raise_unless_address_is_sufficient_for_reward_tier_id(address, reward_id):
    if not address_is_sufficient_for_reward_tier_id(address, reward_id):
        raise ShippingAddressRequired()


def is_big_money(posted_pledge):
    return posted_pledge.attribute("amount_cents") >= BIG_MONEY_CENTS


def big_money_charge_accepted(posted_pledge):
    return posted_pledge.attribute("accepted_big_money_charge")


def to_legacy_and_new_pledge_tuple(func):
    @wraps(func)
    def toTuple(*args, **kwargs):
        val = func(*args, **kwargs)
        Pledges = namedtuple("Pledges", ["legacy", "new"])
        return Pledges(val[0], val[1])

    return toTuple


def get_pledge_vat_location_id(ip_address, vat_country):
    # Check that ip_address indicates that user is in the same country as vat_country
    if ip_address:
        geoip_country = vat_mgr.get_country_by_ip_address(ip_address)
        if vat_mgr.standard_vat_rate_in_bps_for_country_code(geoip_country):
            # it's in Europe, so we're required to match
            if geoip_country != vat_country:
                raise VatCountryMismatch()

    vat_mgr.check_vat_country_matches_geoip(vat_country, ip_address)

    # Find and return a vat location id
    if vat_country:
        return vat_mgr.create_pledge_vat_location(ip_address, geoip_country, vat_country)
    else:
        return None


def check_pledge_cap_amount_is_multiple_of_pledge(pledge_amount, pledge_cap):
    if pledge_cap is not None:
        if not pledge_amount \
                or pledge_cap < pledge_amount \
                or pledge_cap % pledge_amount != 0:
            raise InvalidPledgeCap()


def check_reward_tier(user_id, amount_cents, reward_tier_id, current_reward_tier=None):
    # Manage waitlists
    if reward_tier_id:
        rewards_wait_mgr.delete(user_id=user_id, reward_id=reward_tier_id)
        # This was never actually implemented and I'm not going to bother as
        # part of this refactor
        # patreon.model.reward.reward_wait_notify(user_id, creator_id, reward_id)

        reward = reward_mgr.get_reward_or_throw(reward_tier_id)
        remaining_reward_slots = reward.remaining()
        if remaining_reward_slots is not None and remaining_reward_slots <= 0 \
                and str(reward_tier_id) != str(current_reward_tier):
            raise RewardNotAvailable()

        if reward.amount > amount_cents:
            raise InsufficientPledgeForReward()


def check_nsfw_does_not_use_paypal(campaign_id, card_id):
    campaign = campaign_mgr.get(campaign_id)
    if campaign.is_nsfw and card_mgr.card_token_is_paypal(card_id):
        raise NSFWPaypalPledge()
