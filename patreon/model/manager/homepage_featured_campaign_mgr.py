from patreon.constants import user_hacks
from patreon.model.dbm.homepage_featured_campaign_dbm import \
    HomepageFeaturedCampaignDBM
from patreon.model.manager import campaign_mgr
from patreon.model.table import HomepageFeaturedCampaign
from patreon.services import db

def approve_campaign(campaign):
    HomepageFeaturedCampaign.insert_on_duplicate_key_update({
        'campaign_id': campaign.campaign_id,
        'approval_status': 1,
        'launched_at': campaign.published_at
    })


def disapprove_campaign(campaign):
    HomepageFeaturedCampaign.insert_on_duplicate_key_update({
        'campaign_id': campaign.campaign_id,
        'approval_status': 0,
        'launched_at': campaign.published_at
    })


def get_campaign_status(campaign_id):
    rows = HomepageFeaturedCampaign.query.filter_by(
        campaign_id=campaign_id
    ).all()
    if not rows:
        return None
    return { 'approval_status': rows[0].approval_status }


def reset_campaign_status(campaign_id):
    HomepageFeaturedCampaign.query.filter_by(campaign_id=campaign_id).delete()


def ordered_campaigns_from_id(campaign_ids):
    if not campaign_ids:
        return []
    return sorted(
        campaign_mgr.find_searchable_by_id_list(campaign_ids),
        key=lambda x: x.campaign_id,
        reverse=True
    )


def get_temporary_homepage_campaigns():
    creator_ids = user_hacks.featurable_creators
    campaign_ids = campaign_mgr.get_all_campaign_ids_for_user_ids(creator_ids)
    return ordered_campaigns_from_id(campaign_ids)


def get_qualified_campaigns(cent_cutoff=100000, patron_cutoff=20):
    query = '''select campaigns_users.user_id, campaigns_users.campaign_id from
    (select CID as creator_id, sum(Pledge) as basis_amount
     from tblPledges
     group by CID
     having basis_amount >= %s
     and count(*) >= %s) as innr
    join campaigns_users
    on campaigns_users.user_id = innr.creator_id
    left outer join homepage_featured_campaigns
    on homepage_featured_campaigns.campaign_id = campaigns_users.campaign_id
    where homepage_featured_campaigns.campaign_id is null
    order by creator_id desc
    limit 30;'''
    campaigns = db.query(query, [cent_cutoff, patron_cutoff])
    return ordered_campaigns_from_id([row['campaign_id'] for row in campaigns])


def get_approved_campaigns(page=1):
    dbm = HomepageFeaturedCampaignDBM()
    feature_rows = dbm.approved().latest().paged(page).all()
    return ordered_campaigns_from_id([row.campaign_id for row in feature_rows])


def get_disapproved_campaigns(page=1):
    dbm = HomepageFeaturedCampaignDBM()
    feature_rows = dbm.disapproved().latest().paged(page).all()
    return ordered_campaigns_from_id([row.campaign_id for row in feature_rows])
