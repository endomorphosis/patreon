from patreon.model.table import Category as Category_table
from patreon.model.type import Category
from patreon.model.dbm import category_dbm
from patreon.util.collections import filter_list


def get_category(category_id):
    return Category.from_id(category_id)


def user_categories():
    return [category for category in Category if category.is_user_category]


def all_categories():
    return [category for category in Category]


def find_by_category_id(category_id):
    return category_dbm.find_by_category_id(category_id)


def find_by_user_id(user_id):
    return category_dbm.find_by_user_id(user_id)


def delete_all_by_user_id(user_id):
    find_by_user_id(user_id).delete()


def insert(user_id, category):
    return Category_table.insert({
        'UID': user_id,
        'Category': category
    })


def add_category(user_id, category_id):
    category = Category.from_id(category_id)
    if category:
        insert(user_id, category.category_id)
    return category


def set_categories(user_id, category_ids):
    delete_all_by_user_id(user_id)

    return filter_list([
        add_category(user_id, category_id)
        for category_id in category_ids
    ])


def as_legacy_dict():
    return {
        category.category_id: {
            'Name': category.name,
            'Color': category.color,
        }
        for category in user_categories()
    }
