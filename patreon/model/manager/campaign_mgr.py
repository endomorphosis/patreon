import datetime

import patreon
from patreon.constants import image_sizes
from patreon.exception.campaign_errors import CampaignNotFound
from patreon.model.dbm import campaigns_users_dbm, campaign_dbm
from patreon.model.manager import category_mgr, reward_mgr, queue_mgr
from patreon.model.table import Campaign, CampaignsUsers, User, UserExtra
from patreon.model.type import photo_key_type, file_bucket_enum, TemporaryUploadCollection
from patreon.services import file_helper, mail, uploader
from patreon.services.db import db_session
from patreon.services.logging.unsorted import log_state_change
from patreon.services.image_resizer import TopOffsetResizer
from patreon.services.mail.templates import CreatorWelcome
from patreon.util.image_processing import converter
from patreon.util.collections import filter_dict


def get(campaign_id):
    return Campaign.get(campaign_id)


@patreon.services.caching.request_cached()
def find_searchable():
    return campaign_dbm.find_searchable()


@patreon.services.caching.request_cached()
def find_all_paginated(limit, offset):
    return campaign_dbm.find_all().limit(limit).offset(offset)


@patreon.services.caching.request_cached()
def create_campaign(user_id):
    created_at = patreon.util.unsorted.now()
    campaign_id = Campaign.insert(dict(created_at=created_at))[0]
    connect_campaign_user(campaign_id, user_id)

    user = patreon.model.manager.user_mgr.get_user(user_id)
    if user.email and user.email_patreon:
        mail.send_template_email(CreatorWelcome(user))

    log_state_change('campaign', campaign_id, 'create', '/create')
    log_state_change('campaigns_users', campaign_id, 'update', '/create')

    return campaign_id


@patreon.services.caching.request_cached()
def connect_campaign_user(campaign_id, user_id):
    joined_at = patreon.util.unsorted.now()
    is_admin = True
    should_email_on_patron_post = True
    should_email_on_new_patron = True

    return insert_campaigns_users(
        campaign_id=campaign_id,
        user_id=user_id,
        is_admin=is_admin,
        should_email_on_patron_post=should_email_on_patron_post,
        should_email_on_new_patron=should_email_on_new_patron,
        joined_at=joined_at
    )


def get_campaign_or_throw(campaign_id):
    campaign = get_campaign(campaign_id)
    if campaign is None:
        raise CampaignNotFound(campaign_id)
    return campaign


def get_campaign(campaign_id):
    return get(campaign_id)


def get_campaigns(campaign_ids):
    return campaign_dbm.find_by_campaign_id_list(campaign_ids)


def find_searchable_by_id_list(campaign_ids):
    return campaign_dbm.find_searchable_by_id_list(campaign_ids)


@patreon.services.caching.request_cached()
def get_offers_by_creator_id(creator_id):
    return reward_mgr.find_by_creator_id(creator_id)


def can_edit(user_id, campaign):
    return CampaignsUsers.get(
        user_id=user_id, campaign_id=campaign.campaign_id
    ) is not None


def can_patron_post(user_id, campaign):
    from patreon.model.manager.pledge_mgr import is_patron
    return can_edit(user_id, campaign) or is_patron(user_id, campaign.creator_id)


def get_all_campaign_ids_for_user_ids(user_ids):
    if not user_ids:
        return []

    return [
        campaign_user.campaign_id
        for campaign_user in
        campaigns_users_dbm.find_campaigns_users_by_user_ids(user_ids)
    ]


def try_get_campaign_id_by_user_id_hack(user_id):
    campaign_ids = get_campaign_ids_by_user_id(user_id)
    return None if not campaign_ids else campaign_ids[0]


@patreon.services.caching.multiget_cached(
    object_key='user_id', result_fields='campaign_id', default_result=list
)
def get_campaign_ids_by_user_id(user_ids):
    return CampaignsUsers.query \
        .filter(CampaignsUsers.user_id.in_(user_ids)) \
        .all()


def try_get_campaign_by_user_id_hack(user_id):
    campaigns = get_campaigns_by_user_id(user_id)
    return None if not campaigns else campaigns[0]


@patreon.services.caching.multiget_cached(
    object_key='user_id', result_fields='Campaign',
    join_table_name='CampaignsUsers', default_result=list
)
def get_campaigns_by_user_id(user_ids):
    return db_session.query(Campaign, CampaignsUsers) \
        .join(Campaign.campaigns_users) \
        .filter(CampaignsUsers.user_id.in_(user_ids)) \
        .all()


def try_get_user_id_by_campaign_id_hack(campaign_id):
    user_ids = get_user_ids_by_campaign_id(campaign_id)
    return None if not user_ids else user_ids[0]


@patreon.services.caching.multiget_cached(
    object_key='campaign_id', result_fields='user_id', default_result=list
)
def get_user_ids_by_campaign_id(campaign_ids):
    return CampaignsUsers.query \
        .filter(CampaignsUsers.campaign_id.in_(campaign_ids)) \
        .all()


def try_get_user_by_campaign_id_hack(campaign_id):
    users = get_users_by_campaign_id(campaign_id)
    return None if not users else users[0]


@patreon.services.caching.multiget_cached(
    object_key='campaign_id', result_fields='User',
    join_table_name='CampaignsUsers', default_result=list
)
def get_users_by_campaign_id(campaign_ids):
    return db_session.query(User, CampaignsUsers) \
        .join(User.campaigns_users) \
        .filter(CampaignsUsers.campaign_id.in_(campaign_ids)) \
        .all()


def get_campaign_and_creator_ids(campaign_id=None, creator_id=None):
    if campaign_id:
        # If the campaign_id was provided, find the creator_id.
        creator_id = try_get_user_id_by_campaign_id_hack(campaign_id=campaign_id)
        if creator_id is None:
            raise ValueError("Unable to convert campaign_id={} to creator_id.", campaign_id)

    elif creator_id:
        # If the creator_id was provided, find the campaign_id
        campaign_id = try_get_campaign_id_by_user_id_hack(user_id=creator_id)

        # FIXME: Eventually these lines should be included. However, at the moment some code
        # assumes that if a user_id has no campaign_id, that means the user is not a creator.
        # Example: line 32 of user_alerts.py
        """
        if campaign_id is None:
            raise ValueError("Unable to convert creator_id={} to campaign_id.", creator_id);
        """
    # else:
    #    raise ValueError("No campaign_id or creator_id was provided.");

    return campaign_id, creator_id


@patreon.services.caching.request_cached()
def get_campaign_user(campaign_id, user_id):
    return CampaignsUsers.get(user_id=user_id, campaign_id=campaign_id)


@patreon.services.caching.request_cached()
def get_campaigns_users(user_id=None, campaign_id=None):
    return CampaignsUsers.get(user_id, campaign_id)


def algolia_id_for_id(id_):
    return "campaign_" + str(id_)


def find_by_campaign_id_list(campaign_ids):
    return campaigns_users_dbm.find_by_campaign_id_list(campaign_ids)


def upload_campaign_image(image_path, source_filename):
    source_extension = file_helper.get_extension(source_filename)
    destination_format = converter.convert_format(source_extension)
    with TopOffsetResizer(image_path, image_sizes.CREATOR_SIZES, destination_format) \
            as thumbnail_resizer:
        campaign_image_key = uploader.upload_files(
            TemporaryUploadCollection(
                thumbnail_resizer, file_bucket_enum.config_bucket_campaign
            )
        )
        image_url = photo_key_type.get_image_url(
            file_bucket_enum.config_bucket_campaign,
            campaign_image_key,
            image_sizes.CREATOR_LARGE_2,
            '.' + destination_format.lower()
        )
        image_small_url = photo_key_type.get_image_url(
            file_bucket_enum.config_bucket_campaign,
            campaign_image_key,
            image_sizes.CREATOR_NORMAL_2,
            '.' + destination_format.lower()
        )
        return image_url, image_small_url


def update_campaign_image(campaign_id, image_url, image_small_url):
    campaign = get_campaign_or_throw(campaign_id)
    campaign.update({
        'image_url': image_url,
        'image_small_url': image_small_url
    })


def unpublish_campaign(campaign_id):
    campaign = get_campaign_or_throw(campaign_id)
    campaign.update({
        'published_at': None,
        'creation_name': None
    })


def publish_campaign(campaign_id, user_id):
    campaign = get_campaign_or_throw(campaign_id)
    campaign.update({
        'published_at': datetime.datetime.now(),
    })
    UserExtra.insert_ignore({
        'UID': user_id
    })


def update_campaigns_users(user_id, campaign_id, is_admin=None, joined_at=None,
                           should_email_on_patron_post=None,
                           should_email_on_new_patron=None):
    return get_campaigns_users(user_id, campaign_id).update(
        filter_dict({
            'is_admin': is_admin,
            'joined_at': joined_at,
            'should_email_on_patron_post': should_email_on_patron_post,
            'should_email_on_new_patron': should_email_on_new_patron
        })
    )


def insert_campaigns_users(user_id, campaign_id, is_admin=0, joined_at=None,
                           should_email_on_patron_post=1,
                           should_email_on_new_patron=1):
    if joined_at is None:
        joined_at = datetime.datetime.now()
    return CampaignsUsers.insert({
        'user_id': user_id,
        'campaign_id': campaign_id,
        'is_admin': is_admin,
        'joined_at': joined_at,
        'should_email_on_patron_post': should_email_on_patron_post,
        'should_email_on_new_patron': should_email_on_new_patron
    })


def find_all():
    return campaign_dbm.find_all()


def create_or_update_campaign(
        user_id, first_name, last_name, one_liner, pay_per_name,
        creation_name, is_monthly, is_nsfw, image_url, image_small_url,
        category_ids_raw):
    # 1. Get or create campaign.
    campaign_id = (
        try_get_campaign_id_by_user_id_hack(user_id)
        or create_campaign(user_id)
    )

    # 2. Set user name.
    patreon.model.manager.user_mgr.update_user(
        user_id, user_id, first_name=first_name, last_name=last_name
    )
    log_state_change('user', user_id, 'update', '/create')

    # 3. Set categories.
    categories = category_mgr.set_categories(user_id, category_ids_raw)
    log_state_change('category', user_id, 'delete_all', '/create')
    for _ in categories:
        log_state_change('category', user_id, 'create', '/create')

    # 4. Update campaign.
    update_campaign(
        campaign_id, one_liner, pay_per_name, creation_name, is_monthly,
        is_nsfw, image_url, image_small_url, summary=None, video_url=None,
        video_embed=None
    )
    log_state_change('campaign', campaign_id, 'update', '/create')
    queue_mgr.enqueue_campaign_for_update(campaign_id)


def get_pay_per_name(is_monthly, pay_per_name):
    if is_monthly:
        return 'month'
    elif pay_per_name and not (pay_per_name.startswith('month') or pay_per_name.strip() == ''):
        # Non-monthly campaigns are not allowed to be "per month".
        return pay_per_name
    else:
        # Overwrite as NULL.
        return None


def update_campaign(campaign_id, one_liner=None, pay_per_name=None,
                    creation_name=None, is_monthly=None, is_nsfw=None,
                    image_url=None, image_small_url=None, summary=None,
                    video_url=None, video_embed=None, thanks_message=None,
                    thanks_video_url=None, thanks_embed=None):
    # 1. Set images.
    if image_url and image_small_url:
        log_state_change('campaign_image', campaign_id, 'update', '/create')
        update_campaign_image(campaign_id, image_url, image_small_url)

    # 2. Set campaign.
    if not thanks_video_url:
        thanks_embed = None

    campaign_update_dict = filter_dict({
        'one_liner': one_liner,
        'creation_name': creation_name,
        'is_monthly': is_monthly,
        'is_nsfw': is_nsfw,
        'summary': summary,
        'main_video_url': video_url,
        'main_video_embed': video_embed,
        'thanks_msg': thanks_message,
        'thanks_video_url': thanks_video_url,
        'thanks_embed': thanks_embed
    })

    # 3. Determine par_per_name.
    if is_monthly is not None:
        campaign_update_dict['pay_per_name'] = get_pay_per_name(is_monthly, pay_per_name)

    if campaign_update_dict:
        get(campaign_id).update(campaign_update_dict)

    # 4. Update search.
    queue_mgr.enqueue_campaign_for_update(campaign_id)
