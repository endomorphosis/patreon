from flask import request, session
import time
from hashlib import sha256
from OpenSSL import rand
from patreon.util.unsorted import get_utf8_string
from patreon.services.logging.unsorted import log_json

CSRF_TOKEN_LIFETIME_MINUTES = 120

CSRF_EXCEPTIONS_WEB = (
    "ShareImage",
    "ShareFile",
    "processSignup",
    "callToAct",
    "nuke",
    "oauth",
    "auth/login",
    "lookup",
    "creatorReport",
    "DismissAlert",
    "CheckVanity",
    "forgot_password",
    "forgot_password_reset",
    "facebookSignup",
    "legacyCampaignUpload",
    "legacyUserUpload"
)

CSRF_EXCEPTIONS_API = (
    '/login',
    '/user'
)


def get_csrf_time():
    return time.time()*1000


def get_csrf_context_hash(uri, user_id, request_time):
    return sha256(get_utf8_string(uri) + get_utf8_string(user_id) + get_utf8_string(request_time)).hexdigest()


def calculate_csrf_signature(uri, user_id, token, request_time):
    context_hash = get_csrf_context_hash(uri, user_id, request_time)
    return sha256(get_utf8_string(token) + get_utf8_string(context_hash)).hexdigest()


def get_user_secret(user_id):
    return sha256(get_utf8_string(rand.bytes(128)) +
                  get_utf8_string(get_csrf_time()) +
                  get_utf8_string(user_id)).hexdigest()


def is_api_csrf_protection_disabled(uri):
    return is_csrf_protection_disabled(uri, CSRF_EXCEPTIONS_API)


def is_web_csrf_protection_disabled(uri):
    return is_csrf_protection_disabled(uri, CSRF_EXCEPTIONS_WEB)


def is_csrf_protection_disabled(uri, exceptions):
    for endpoint in exceptions:
        if endpoint == uri or endpoint in uri:
            return True
    return False


def is_expired(request_time):
    difference = abs(get_csrf_time() - request_time)
    # TODO(constants) change this to use a time constant when those are ported
    return difference > (60 * CSRF_TOKEN_LIFETIME_MINUTES * 1000)


def is_invalid_referrer(referrer='', uri=''):
    referrer = referrer.lower()
    uri = uri.lower()
    if referrer == uri or uri in referrer or uri == '/user':
        return False

    return True


def is_invalid_csrf_token(user_id, token, request_time, uri, input_signature):
    return calculate_csrf_signature(str(uri), str(user_id), str(token), str(request_time)) != str(input_signature)


def get_csrf_ticket(uri, user_id, csrf_token):
    request_time = str(get_csrf_time())
    return {'token': calculate_csrf_signature(str(uri), str(user_id), str(csrf_token), str(request_time)),
            'time': request_time,
            'URI': uri}


def get_null_csrf_ticket():
    return {
        'token': "",
        'time': "",
        'URI': ""
    }


def log_csrf_event(reason, time, uri, token, expected_token=None):
    log_json({
        "verb": "csrf_checked",
        "request": {
            "referrer": request.referrer,
            "target": request.path
        },
        "reason": reason,
        "tokenParameters": {
            "userID": session.user_id,
            "time": time,
            "secret": session.csrf_token,
            "tokenURI": uri,
            "expectedToken": expected_token,
            "inputToken": token,
        }
    })
