import datetime
import functools
from collections import defaultdict
from operator import attrgetter

import patreon
from patreon.exception import ParameterInvalid
from patreon.exception.comment_errors import CommentNotFound, InvalidParent, \
    InvalidComment, CommentDeleteForbidden
from patreon.exception.common_errors import LoginRequired
from patreon.exception.post_errors import PostViewForbidden, PostNotFound
from patreon.model.dbm import presence_dbm
from patreon.model.push_notifications.new_comment_push import NewCommentPush
from patreon.model.table import Comment, CommentVote, UserSetting, User
from patreon.model.manager import feature_flag, pagination, post_mgr, sorting, user_mgr
from patreon.services import bonfire, mail, spam
from patreon.services.logging.unsorted import log_anomaly, log_state_change
from patreon.services.logging.views import log_comment, log_spam
from patreon.services.mail.templates import NotifyNewComment


def get_comment_or_throw(comment_id):
    comment = Comment.get(comment_id=comment_id)
    if comment is None:
        raise CommentNotFound(comment_id)
    return comment


def save_comment(commenter_id, post_id, comment_text, thread_id=None):
    from patreon.app.api.resources.api_resource import ApiResource
    post = post_mgr.get_post_or_throw(post_id)
    creator = user_mgr.get_user_or_throw(post.creator_id)
    commenter = user_mgr.get_user_or_throw(commenter_id)

    # You cannot post a comment to a post you cannot read.
    if not post_mgr.can_view(commenter_id, post):
        raise PostViewForbidden(post_id)

    # You cannot chose a parent comment on a different post.
    if thread_id:
        parent = get_comment_or_throw(thread_id)
        if str(parent.post_id) != str(post_id):
            raise InvalidParent(thread_id)

    if not comment_text:
        raise InvalidComment(comment_text)

    is_spam = check_spam(creator, commenter, comment_text, post_id)
    comment_id = insert_comment(
        commenter_id, post, comment_text, thread_id, is_spam
    )

    # Check that the comment was saved correctly.
    comment = get_comment_or_throw(comment_id)

    # Ping the real-time chat server
    if post.user_id == post.creator_id \
            and feature_flag.is_enabled('real_time_comments', user_id=post.user_id):
        comment_json = patreon.app.api.resources.comments.CommentResource(comment) \
            .as_json_api_document(version=ApiResource.JSON_API_STABLE)
        bonfire.post('/comment_added', comment_json)

    if not is_spam:
        notify_about_comment(comment)

    log_comment(
        post_id, creator, comment_id, thread_id, commenter_id, comment_text
    )

    return comment


def check_spam(campaign_creator, commenter, comment_text, activity_id):
    is_spam = campaign_creator.user_id != commenter.user_id \
              and spam.is_spam(comment_text, commenter.email, commenter.full_name)

    if is_spam:
        log_spam(
            commenter.user_id, commenter.email, commenter.full_name,
            commenter.vanity, activity_id, comment_text
        )

    return is_spam


def insert_comment(user_id, post, comment_text, thread_id, is_spam):
    if thread_id == 0:
        thread_id = None

    comment_id = Comment.insert({
        'UID': user_id,
        'HID': post.activity_id,
        'Created': datetime.datetime.now(),
        'Comment': comment_text,
        'ThreadID': thread_id,
        'is_spam': is_spam
    })

    log_state_change('comment', comment_id, 'create', '/processComment')

    if comment_id and not is_spam:
        log_state_change('activity', post.activity_id, 'update', '/processComment')
        post.update({
            'comment_count': post.comment_count + 1
        })

    return comment_id


def notify_about_comment(comment):
    post = post_mgr.get_post_or_throw(comment.post_id)

    # don't notify about live chat comments
    presence = presence_dbm.get_by_user_id_and_activity_id(post.user_id, post.activity_id)
    if presence is not None and presence.expiration_time > datetime.datetime.now():
        return

    for user_id in [comment.commenter_id, post.user_id, post.creator_id]:
        User.get.prime(user_id)
    commenter = user_mgr.get_user_or_throw(comment.commenter_id)
    parent_commenter = None

    # notify the parent comment
    if comment.thread_id is not None:
        parent_comment = Comment.get(comment.thread_id)
        if parent_comment is not None and parent_comment.commenter_id != comment.commenter_id:
            user_settings = UserSetting.get(parent_comment.commenter_id)
            if user_settings is not None:
                if user_settings.email_new_comment:
                    creator = user_mgr.get_user_or_throw(post.creator_id)
                    parent_commenter = user_mgr.get_user_or_throw(parent_comment.commenter_id)
                    mail.send_template_email(
                        NotifyNewComment(
                            recipient=parent_commenter,
                            creator=creator,
                            commenter=commenter,
                            comment=comment,
                            is_reply=True
                        )
                    )
                if user_settings.push_new_comment:
                    NewCommentPush(
                        user_ids=[parent_comment.commenter_id],
                        comment=comment,
                        commenter=commenter,
                        is_reply=True
                    ).publish()

    if post.user_id != comment.commenter_id \
            and (not parent_commenter or post.user_id != parent_commenter.user_id):
        user_settings = UserSetting.get(post.user_id)
        if user_settings is not None:
            if user_settings.email_new_comment:
                creator = user_mgr.get_user_or_throw(post.creator_id)
                post_author = user_mgr.get_user_or_throw(post.user_id)
                mail.send_template_email(
                    NotifyNewComment(
                        recipient=post_author,
                        creator=creator,
                        commenter=commenter,
                        comment=comment,
                        is_reply=False
                    )
                )
            if user_settings.push_new_comment:
                NewCommentPush(
                    user_ids=[post.user_id],
                    comment=comment,
                    commenter=commenter,
                    is_reply=False
                ).publish()


def delete_comment_as_user(comment_id, user_id):
    user = user_mgr.get(user_id=user_id)
    comment = Comment.get(comment_id=comment_id)
    if not user:
        owner = None
        if comment:
            owner = comment.commenter_id
        log_anomaly('comment', comment_id, owner, 'delete', 'user nonexistant')
        raise LoginRequired()

    if not comment:
        log_anomaly('comment', comment_id, None, 'delete', 'nonexistant')
        raise CommentNotFound(comment_id)

    post = post_mgr.get_post(comment.post_id)
    if not post:
        log_anomaly('comment', comment_id, comment.commenter_id, 'delete', 'activity nonexistant')
        raise PostNotFound(comment.post_id)

    if user.user_id != comment.commenter_id \
            and post.campaign_user_ids[0] != user_id \
            and post.user_id != user_id:
        log_anomaly('comment', comment.comment_id, comment.commenter_id, 'delete', 'unauthorized')
        raise CommentDeleteForbidden(comment.comment_id)

    if comment.has_children:
        log_state_change('comment', comment_id, 'nullify', '/processCommentDelete')
        comment.update({
            'Comment': '',
            'DeletedAt': datetime.datetime.now()
        })
    else:
        log_state_change('activity', post.activity_id, 'update', '/processCommentDelete')
        comment.delete()
        if not comment.is_spam:
            post.update({
                'comment_count': post.comment_count - 1
            })

    comment = Comment.get(comment_id=comment_id)

    return comment


def raw_comments_to_structured_comments(raw_comments):
    # my dictionary-comprehension-fu is weak
    return_dict = defaultdict(list)
    return_dict['children'] = defaultdict(list)
    for comment in raw_comments:
        if comment.thread_id is None:
            return_dict['parents'].append(comment)
        else:
            return_dict['children'][comment.thread_id].append(comment)

    return return_dict


def raw_comments_to_structured_comments_with_max_depth(raw_comments):
    """Returns a structured list of the comments as with
    raw_comments_to_structured_comments, but instead of having the actual
    parent structure, all comments above depth 2 are flattened to have
    the greatest ancestor be the comment parent."""
    return_dict = defaultdict(list)
    return_dict['children'] = defaultdict(list)

    # Flatten all comment parentage by just tracing the comment up until it
    # reaches the topmost comment (with None as parent.) Theoretically this
    # could also use union find or whatever; I'm not too concerned about
    # speed until it starts to be a CPU problem. Most comment threads are
    # something like 200 comments max.
    child_id_to_parent_id = {
        comment.comment_id: comment.thread_id
        for comment in raw_comments
    }

    def get_greatest_ancestor(comment_id):
        if comment_id not in child_id_to_parent_id:
            # This will trigger if the parent comment isn't in the activity lookup,
            # which only happens right now if the parent comment is marked is_spam
            return None
        if child_id_to_parent_id[comment_id] is None:
            return comment_id
        else:
            return get_greatest_ancestor(child_id_to_parent_id[comment_id])

    for comment in raw_comments:
        if comment.thread_id is None:
            return_dict['parents'].append(comment)
        else:
            thread_id = get_greatest_ancestor(comment.thread_id)
            if thread_id is not None:
                return_dict['children'][thread_id].append(comment)
    return return_dict


def get_structured_comments_for_post(post_id, user_id=None, flattened=False):
    """
    Structured comments look like:
    {
        'parents': <list of comments who are the top of a thread>
        'children: {
              <comment_id>: <list of children>
              ...
        }
    }
    """
    raw_comments = Comment.find_by_activity_id(post_id)
    # if user_id:
    #     for comment in raw_comments:
    #         CommentVote.get.prime(comment_id=comment.comment_id, user_id=user_id)
    if flattened:
        comments = raw_comments_to_structured_comments_with_max_depth(raw_comments)
        child_sorting_function = lambda x: sorted(x, key=attrgetter('Created'), reverse=False)
    else:
        comments = raw_comments_to_structured_comments(raw_comments)
        child_sorting_function = None
    functions = [lambda x: sorted(x, key=attrgetter('vote_sum'), reverse=True),
                 lambda x: sorted(x, key=attrgetter('Created'), reverse=True)]

    results = []
    for function in functions:
        sorted_comments = sort_structured_comments_by_function(comments, function, child_sorting_function)
        sorted_tree = structured_comments_to_tree(
            sorted_comments['parents'], sorted_comments
        )
        results.append(sorted_tree)
    return results[0], results[1]


@patreon.services.caching.request_cached()
def get_flattened_comments_for_post(post_id):
    """Returns a mapping of comment id -> descendent comment objects."""
    # TODO(refactor): These classes of functions are getting worse for understandability.
    comment_map = {}
    comment_tree, _ = get_structured_comments_for_post(post_id, flattened=True)
    for structured_comment in comment_tree:
        [parent, child_trees] = structured_comment
        children = [child for (child, _) in child_trees or []]
        comment_map[parent.comment_id] = children
    return comment_map


def structured_comments_to_tree(thread_parents, all_comments):
    """
    The 'tree' isn't a real tree, but it's closer than 'structured' comments
    It's structured like:
    [
      [
        <comment>,
        [<Same tree structure representing comments in the same thread>]
      ],
      ...
    ]
    This structure makes recursion super straightforward.
    """

    if not thread_parents:
        return None

    return [
        [
            comment,
            structured_comments_to_tree(
                all_comments['children'][comment.comment_id], all_comments
            )
        ]
        for comment in thread_parents
    ]


def sort_structured_comments_by_function(comments, sorting_function, child_sorting_function=None):
    """
    This sorts an array of 'structured' comments as described above via a
    provided function.
    """
    comments['parents'] = sorting_function(comments['parents'])
    child_sorting_function = child_sorting_function or sorting_function
    for comment_id, list in comments['children'].items():
        comments['children'][comment_id] = child_sorting_function(list)

    return comments


def get_vote_for_comment_by_user_id(comment_id, user_id):
    result = CommentVote.get(comment_id=comment_id, user_id=user_id)
    if result:
        return result.vote
    return 0


def get_comment_votes_by_comment_id(comment_id):
    return CommentVote.query.filter_by(comment_id=comment_id).all()


def update_comment_vote_sum(comment_id):
    new_sum = functools.reduce(lambda memo, vote: memo + vote.vote,
                               get_comment_votes_by_comment_id(comment_id), 0)
    Comment.get(comment_id).update({'vote_sum': new_sum})


def add_or_update_comment_vote(comment_id, user_id, vote):
    extant_vote = CommentVote.get(comment_id=comment_id, user_id=user_id)
    if extant_vote:
        extant_vote.update({
            'vote': vote
        })
    else:
        comment = Comment.get(comment_id)
        CommentVote.insert({
            'vote': vote,
            'comment_id': comment_id,
            'activity_id': comment.post_id,
            'user_id': user_id
        })
    update_comment_vote_sum(comment_id)
    return CommentVote.get(comment_id=comment_id, user_id=user_id)


@patreon.services.caching.multiget_cached(object_key='thread_id', default_result=list)
def find_by_parent_comment_id(parent_comment_ids):
    return Comment.query \
        .filter(Comment.ThreadID.in_(parent_comment_ids)) \
        .all()


def find_thread_heads_by_activity_id(activity_id, sort=None):
    thread_heads = Comment.find_thread_heads_by_activity_id(activity_id)
    if sort:
        sort_param, ascending = sorting.parse_sort_string(sort)[0]
        reverse = (not ascending)
        if sort_param == 'created':
            thread_heads.sort(key=lambda comment: comment.created_at, reverse=reverse)
        elif sort_param == 'vote_sum':
            thread_heads.sort(key=lambda comment: comment.vote_sum, reverse=reverse)
        else:
            raise ParameterInvalid('sort', sort)
    return thread_heads


def find_thread_heads_by_activity_id_paged(activity_id, cursor=None, offset=None, per_page=10, sort=None):
    query = Comment.query.filter_by(HID=activity_id, ThreadID=None, is_spam=0)
    sort_default = 'created'
    sort_map = {'created': Comment.CID,
                'vote_sum': Comment.vote_sum}
    if offset is not None:
        sort_commands = sorting.sort_order_from_sort_string(sort, sort_map, sort_default)
        return pagination.paginate_offset(
            query=query,
            sort_commands=sort_commands,
            offset=offset,
            per_page=per_page
        )
    else:
        column, ascending = sorting.sort_order_tuples(sort, sort_map, sort_default)[0]
        return pagination.paginate_cursor(
            query=query,
            sort_column=column,
            ascending=ascending,
            cursor=cursor,
            per_page=per_page
        )


def count_by_activity_id(activity_id):
    # TODO: Move to a caching system independent of the MySQL table
    return post_mgr.get_post(activity_id).comment_count


def get_commenters_dict_for_posts(post_ids):
    commenter_ids = Comment.get_commenter_ids_by_activity_ids(post_ids)
    commenters = user_mgr.get_users(commenter_ids)
    return {
        commenter.user_id: commenter
        for commenter in commenters
    }
