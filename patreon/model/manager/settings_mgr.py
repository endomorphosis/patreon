from patreon.model.manager import user_campaign_settings_mgr, campaign_mgr
from patreon.model.table import TblUsersSetting, Pledge, UserExtra, \
    UserSetting, User, UserCampaignSetting
from patreon.util.collections import filter_dict
from patreon.model.settings.settings import Settings


def get_user_extra(user_id):
    return UserExtra.get(user_id)


def get_old_settings(user_id):
    return TblUsersSetting.get(user_id)


def get_settings(user_id):
    return UserSetting.get(user_id)


def is_authorized(user_id, actor_id):
    return user_id == actor_id


def get_all_settings(user_id):
    user_settings = get_settings(user_id)
    old_settings = get_old_settings(user_id)
    # TODO: instead of wrapping this in an array,
    # get_campaigns_users should return the array of all campaigns the user is a member of
    one_result = campaign_mgr.get_campaigns_users(user_id=user_id)
    campaign_settings = [one_result] if one_result is not None else []
    follow_settings = user_campaign_settings_mgr.get(user_id=user_id)
    return Settings(user_settings=user_settings, old_settings=old_settings,
                    campaign_settings=campaign_settings, follow_settings=follow_settings)


def update_settings(user_id, actor_id, privacy=None, fb_import=None,
                    email_update=None, email_new_comment=None, email_goal=None,
                    push_update=None, push_new_comment=None, push_goal=None,
                    email_new_patron=None, email_patron_post=None,
                    push_new_patron=None, push_patron_post=None,
                    email_creator_ids=None, email_campaign_ids=None,
                    email_creator_paid=None,
                    email_creator_post=None, email_creator_comment=None,
                    push_creator_paid=None,
                    push_creator_post=None, push_creator_comment=None,
                    push_creator_live=None):
    if not is_authorized(user_id, actor_id):
        raise Exception('Not authorized to edit')

    if user_id is None:
        return None

    update_user_settings(user_id, privacy, fb_import)
    update_campaign_settings(user_id,
                             email_new_patron=email_new_patron, email_patron_post=email_patron_post,
                             push_new_patron=push_new_patron, push_patron_post=push_patron_post)
    update_patron_settings(user_id,
                           email_new_comment=email_new_comment, email_update=email_update, email_goal=email_goal,
                           push_new_comment=push_new_comment, push_update=push_update, push_goal=push_goal)
    update_campaign_notification_settings(user_id,
                                          email_creation_ids=email_creator_ids,
                                          email_campaign_ids=email_campaign_ids,
                                          email_creation_paid=email_creator_paid,
                                          email_creation_post=email_creator_post,
                                          email_creation_comment=email_creator_comment,
                                          push_creation_paid=push_creator_paid,
                                          push_creation_post=push_creator_post,
                                          push_creation_comment=push_creator_comment,
                                          push_creation_live=push_creator_live
                                          )

    return User.get(user_id=user_id)


def update_user_settings(user_id, privacy=None, fb_import=None):
    if user_id is None:
        return None
    user_settings = {'Privacy': privacy,
                     'FBImport': fb_import}

    user_settings = filter_dict(user_settings)
    if user_settings != {}:
        TblUsersSetting.get(user_id=user_id).update(user_settings)

    return TblUsersSetting.get(user_id=user_id)


def update_campaign_settings(user_id, email_new_patron, email_patron_post, push_new_patron=None, push_patron_post=None):
    if user_id is None:
        return None

    campaign_settings = {'should_email_on_new_patron': email_new_patron,
                         'should_email_on_patron_post': email_patron_post,
                         'should_push_on_new_patron': push_new_patron,
                         'should_push_on_patron_post': push_patron_post}

    campaign_settings = filter_dict(campaign_settings)

    if campaign_settings != {}:
        campaign_mgr.get_campaigns_users(user_id=user_id).update(campaign_settings)

    return campaign_mgr.get_campaigns_users(user_id=user_id)


def update_patron_settings(user_id, email_new_comment=None, email_update=None, email_goal=None,
                           push_new_comment=None, push_update=None, push_goal=None):
    if user_id is None:
        return None

    patron_settings = {'email_new_comment': email_new_comment,
                       'email_patreon': email_update,
                       'email_goal': email_goal,
                       'push_new_comment': push_new_comment,
                       'push_patreon': push_update,
                       'push_goal': push_goal}

    patron_settings = filter_dict(patron_settings)

    if patron_settings != {}:
        UserSetting.get(user_id=user_id).update(patron_settings)

    return UserSetting.get(user_id=user_id)


def update_campaign_notification_settings(user_id, email_creation_ids, email_campaign_ids,
                                          email_creation_paid=None,
                                          email_creation_post=None,
                                          email_creation_comment=None,
                                          push_creation_paid=None,
                                          push_creation_post=None,
                                          push_creation_comment=None,
                                          push_creation_live=None):
    if email_creation_ids is None and email_campaign_ids is None:
        return User.get(user_id=user_id)

    if email_campaign_ids is None:
        email_campaign_ids = [campaign_mgr.try_get_campaign_id_by_user_id_hack(creator_id)
                              for creator_id in email_creation_ids]

    for i in range(len(email_campaign_ids)):
        campaign_id = email_campaign_ids[i]
        email_paid = email_creation_paid[i] if email_creation_paid else None
        email_post = email_creation_post[i] if email_creation_post else None
        email_comment = email_creation_comment[i] if email_creation_comment else None
        push_paid = push_creation_paid[i] if push_creation_paid else None
        push_post = push_creation_post[i] if push_creation_post else None
        push_comment = push_creation_comment[i] if push_creation_comment else None
        push_creator_live = push_creation_live[i] if push_creation_live else None

        creator_id = campaign_mgr.try_get_user_id_by_campaign_id_hack(campaign_id)
        pledge = Pledge.get(patron_id=user_id, creator_id=creator_id)
        if pledge:
            pledge.update({'EmailPaid': email_paid,
                           'EmailPost': email_post})

        user_campaign_settings_mgr.update(
            user_id, campaign_id,
            email_paid_post=email_paid, email_post=email_post, email_new_comment=email_comment,
            push_paid_post=push_paid, push_post=push_post, push_new_comment=push_comment,
            push_creator_live=push_creator_live
        )
    return User.get(user_id=user_id)


def update_user_extra(user_id, paypal_email=None, auto_pay=None,
                      pay_method=None, stripe_recipient_id=None,
                      stripe_last_four=None, stripe_name=None,
                      flag_youtube=None):
    if not user_id:
        return None
    user_extra = get_user_extra(user_id)
    if not user_extra:
        # Weird legacy case
        insert_user_extra(user_id)
        user_extra = get_user_extra(user_id)

    settings_dict = {'PaypalEmail': paypal_email,
                     'AutoPay': auto_pay,
                     'PayMethod': pay_method,
                     'StripeRecipientID': stripe_recipient_id,
                     'StripeLastFour': stripe_last_four,
                     'StripeName': stripe_name,
                     'FlagYoutube': flag_youtube
                     }
    settings_dict = filter_dict(settings_dict)
    if settings_dict != {}:
        user_extra.update(settings_dict)
    return user_extra


def insert_user_extra(user_id):
    if user_id is None:
        return None
    UserExtra.insert({
        'UID': user_id
    })


def insert_user_settings(user_id):
    if user_id is None:
        return None
    UserSetting.insert({
        'user_id': user_id
    })


def insert_old_user_settings(user_id):
    if user_id is None:
        return None
    TblUsersSetting.insert({
        'UID': user_id
    })


def get_campaign_settings(user_id, creator_id):
    campaign_id = campaign_mgr.try_get_campaign_id_by_user_id_hack(creator_id)
    settings = UserCampaignSetting.find(user_id=user_id, campaign_id=campaign_id)
    return settings


def reset_youtube_flag(user_id):
    return update_user_extra(user_id, flag_youtube=0)
