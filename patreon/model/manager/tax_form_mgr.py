from patreon.model.table import TaxForm, W8TaxForm, W9TaxForm
from patreon.util.collections import filter_dict


def get(user_id):
    base_tax_form = TaxForm.get(user_id)
    if not base_tax_form:
        return None

    if base_tax_form.is_w9:
        return W9TaxForm.get(user_id)
    elif base_tax_form.is_w8:
        return W8TaxForm.get(user_id)


def get_tax_form_class(type):
    if type == 7:
        return W9TaxForm
    elif type == 6:
        return W8TaxForm


def insert_or_update_w9(user_id, full_name, birth_date, address, city,
                        us_tax_id, signature, country, business_name, state,
                        zip, tax_class):
    insert_dict = {
        'UID': user_id,
        'FullName': full_name,
        'BirthDate': birth_date,
        'Address': address,
        'AddressLocation': city,
        'USTaxID': us_tax_id,
        'Signature': signature,
        'Country': country,
        'BusinessName': business_name,
        'State': state,
        'Zip': zip,
        'TaxClass': tax_class,
        'Type': 7
    }
    insert_dict = filter_dict(insert_dict)
    tax_form = W9TaxForm.get(user_id)
    if tax_form:
        tax_form.update(insert_dict)
    else:
        W9TaxForm.insert(insert_dict)


def insert_or_update_w8(user_id, full_name, birth_date, address, city, us_tax_id, signature, country, tax_id,
                        country_citizen, reference_id=None, treaty_article=None, treaty_percent=None,
                        treaty_income=None, treaty_reason=None, acting_capacity=None):
    insert_dict = {
        'UID': user_id,
        'FullName': full_name,
        'BirthDate': birth_date,
        'Address': address,
        'AddressLocation': city,
        'USTaxID': us_tax_id,
        'Signature': signature,
        'Country': country,
        'TaxID': tax_id,
        'CountryCitizen': country_citizen,
        'Type': 6
    }
    insert_dict = filter_dict(insert_dict)
    optional_params = {
        'Reference': reference_id,
        'TreatyArticle': treaty_article,
        'TreatyPercent': treaty_percent,
        'TreatyIncome': treaty_income,
        'TreatyReason': treaty_reason,
        'ActingCapacity': acting_capacity
    }
    insert_dict.update(optional_params)
    tax_form = W8TaxForm.get(user_id)
    if tax_form:
        tax_form.update(insert_dict)
    else:
        W8TaxForm.insert(insert_dict)
