from _sha256 import sha256
import datetime

from OpenSSL import rand
from patreon.model.dbm import forgot_password_dbm
from patreon.model.table import ForgotPassword


def generate_token():
    return sha256(rand.bytes(2560)).hexdigest()[:32]


def delete_all_user_tokens(user_id):
    forgot_password_dbm.find_by_user_id(user_id).delete()


def insert(user_id):
    token = generate_token()

    ForgotPassword.insert({
        'SecretID': token,
        'UID': user_id,
        'Created': datetime.datetime.now()
    })

    return token


def get(user_id, token):
    return ForgotPassword.get(user_id=user_id, token=token)