from sqlalchemy import desc
from patreon import globalvars
from patreon.exception.post_errors import PostNotFound
from patreon.model.dbm import activity_dbm
from patreon.model.manager import campaign_mgr, user_mgr, complete_pledge_mgr
from patreon.model.table import Activity, Campaign, User, Pledge


def can_edit(user_id, activity):
    # If you are the creator of this post.
    return activity.user_id == user_id


# TODO: this logic echoes the SQL-level filtering in `get_posts_by_campaign_id_paginated`
def can_view(user_id, post):
    if can_edit(user_id, post):
        return True

    # If you can't edit, then you can't see drafts.
    if not post.is_published:
        return False

    # Anyone can see a public post.
    if not post.min_cents_pledged_to_view:
        return True

    # If not public, then you must be logged in.
    if not user_id:
        return False

    # If it's a patron post on your feed.
    creator_id = campaign_mgr.try_get_user_id_by_campaign_id_hack(post.campaign_id)
    if creator_id == user_id:
        return True

    # Only active patrons will have pledges.
    pledge = complete_pledge_mgr.get_pledge_by_patron_and_campaign_or_creator(
        patron_id=user_id, campaign_id=post.campaign_id
    ).legacy

    return pledge and pledge.amount >= post.min_cents_pledged_to_view


def can_delete(user_id, activity):
    if can_edit(user_id, activity):
        return True

    creator_id = campaign_mgr.try_get_user_id_by_campaign_id_hack(
        activity.campaign_id
    )

    return creator_id == user_id


def get_last_published_post(campaign_id):
    return Activity.query \
        .filter(Activity.campaign_id == campaign_id) \
        .filter(Activity.is_published) \
        .order_by(desc(Activity.published_at)) \
        .first()


def get_post_or_throw(post_id):
    post = get_post(post_id)
    if post is None:
        raise PostNotFound(post_id)
    return post


def get_post(post_id):
    return activity_dbm.get_public_activity(post_id).first()


def get_posts(post_ids):
    if not post_ids:
        return []

    return Activity.query \
        .filter(Activity.activity_id.in_(post_ids)) \
        .filter(Activity.is_public) \
        .all()


def get_posts_visible_to_user(post_ids, visible_to_user_id):
    # TODO: do this in SQL
    return [post
            for post in get_posts(post_ids)
            if can_view(visible_to_user_id, post)]


def get_posts_by_owner_id(owner_id):
    return activity_dbm.find_public_by_user_id(owner_id).all()


def get_posts_by_campaign_id(campaign_id):
    return activity_dbm.find_public_by_campaign_id(campaign_id).all()


def get_posts_by_campaign_id_paginated(
        campaign_id, cursor=None, per_page=None, is_by_creator=None, is_creation=None,
        visible_to_user_id=None):
    return get_posts_by_campaign_ids_paginated(
        [campaign_id], cursor=cursor, per_page=per_page,
        is_by_creator=is_by_creator, is_creation=is_creation, visible_to_user_id=visible_to_user_id
    )


def get_posts_by_campaign_ids_paginated(
        campaign_ids, cursor=None, per_page=None, is_by_creator=None, is_creation=None,
        visible_to_user_id=None, allow_community=True, offset_=None):
    if not per_page:
        per_page = globalvars.get('SHARES_PER_PAGE')
    per_page = int(per_page)

    if not offset_:
        offset_ = 0
    offset_ = int(offset_)

    posts = []
    # prime our gets
    if visible_to_user_id or (is_by_creator is not None):
        for campaign_id in campaign_ids:
            Campaign.get.prime(campaign_id)
            creator_id = campaign_mgr.try_get_user_id_by_campaign_id_hack(
                campaign_id
            )
            Pledge.get.prime(visible_to_user_id, creator_id)
            User.get.prime(creator_id)
    for campaign_id in campaign_ids:
        query = activity_dbm.find_public_by_campaign_id(campaign_id)
        if cursor:
            query = query.filter(Activity.published_at <= cursor)
        if is_by_creator is not None:
            creator_id = campaign_mgr.try_get_user_id_by_campaign_id_hack(campaign_id)
            if is_by_creator:
                query = query.filter(Activity.user_id == creator_id)
            else:
                query = query.filter(Activity.user_id != creator_id)
        if is_creation is not None:
            query = query.filter(Activity.is_creation == is_creation)
        if visible_to_user_id:
            campaign = campaign_mgr.get_campaign(campaign_id)
            pledge_val = 0
            pledge = complete_pledge_mgr.get_pledge_by_patron_and_campaign_or_creator(
                patron_id=visible_to_user_id, creator_id=campaign.creator_id
            ).legacy
            if pledge:
                pledge_val = pledge.amount
            if allow_community:
                creator = user_mgr.get_user(
                    campaign_mgr.try_get_user_id_by_campaign_id_hack(
                        campaign_id
                    )
                )
                query = query \
                    .filter((Activity.user_id == visible_to_user_id)  # if you are the author
                            | ((pledge_val >= Activity.min_cents_pledged_to_view)  # or you've pledged enough to view it
                               & ((Activity.user_id == campaign.creator_id)  # and it's by the campaign owner
                                  | ((Activity.user_id != campaign.creator_id)  # or it's a community post...
                                     & (not creator.is_private))  # ...which is not private
                                  )
                               )
                            )
            else:
                query = query \
                    .filter((Activity.user_id == visible_to_user_id)  # if you are the author
                            | ((pledge_val >= Activity.min_cents_pledged_to_view)  # or you've pledged enough to view it
                               & (Activity.user_id == campaign.creator_id)  # and it's by the campaign owner
                               )
                            )
        query = query.order_by(Activity.published_at.desc())
        if per_page == 'infinity':
            posts += query.all()
        else:
            posts += query.limit(per_page).offset(offset_).all()

    posts.sort(key=lambda post: post.published_at, reverse=True)
    if per_page == 'infinity':
        return posts
    else:
        return posts[:per_page]


def get_count_of_creations_after_timestamp(campaign_id, timestamp=None):
    query = activity_dbm.find_public_by_campaign_id(campaign_id) \
        .filter(Activity.is_creation == True)
    if timestamp:
        query = query.filter(Activity.published_at >= timestamp)
    return query.count()


def is_unread(post_id, user_id):
    from patreon.model.manager import unread_status_mgr

    unread_timestamp = unread_status_mgr.latest_read_time(user_id, 'creations')
    if unread_timestamp:
        post = get_post_or_throw(post_id)
        return post.published_at >= unread_timestamp
    else:
        return True
