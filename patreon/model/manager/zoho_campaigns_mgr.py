from patreon.model.table import ZohoCampaigns


def get(campaign_id, zoho_id):
    return ZohoCampaigns.get(campaign_id, zoho_id)


def update_zoho_id(old_zoho_id, new_zoho_id, is_converted):
    obj = get(None, old_zoho_id)
    obj.update({
        'zoho_id': new_zoho_id,
        'is_converted': is_converted
    })


def update(campaign_id, zoho_id, is_converted):
    obj = get(campaign_id, zoho_id)
    if not obj:
        insert(campaign_id, zoho_id, is_converted)
    else:
        obj.update({
            'zoho_id': zoho_id,
            'is_converted': is_converted
        })


def insert(campaign_id, zoho_id, is_converted):
    obj = get(campaign_id, zoho_id)
    if obj:
        update(campaign_id, zoho_id, is_converted)
    else:
        ZohoCampaigns.insert({
            'campaign_id': campaign_id,
            'zoho_id': zoho_id,
            'is_converted': is_converted
        })
