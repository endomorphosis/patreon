from patreon.model.table import UserCampaignSetting
from patreon.util.collections import filter_dict


def insert_or_update(user_id, campaign_id, email_new_comment=None, email_post=None, email_paid_post=None):
    settings = UserCampaignSetting.find(user_id=user_id, campaign_id=campaign_id)
    if settings:
        update(user_id, campaign_id, email_new_comment, email_post, email_paid_post)
    else:
        insert(user_id, campaign_id, email_new_comment, email_post, email_paid_post)


def insert_if_new(user_id, campaign_id, email_new_comment=None, email_post=None, email_paid_post=None):
    settings = UserCampaignSetting.find(user_id=user_id, campaign_id=campaign_id)
    if not settings:
        insert(user_id, campaign_id, email_new_comment, email_post, email_paid_post)


def update(user_id, campaign_id,
           email_new_comment=None, email_post=None, email_paid_post=None,
           push_new_comment=None, push_post=None, push_paid_post=None,
           push_creator_live=None):
    settings = UserCampaignSetting.find(user_id=user_id, campaign_id=campaign_id)
    if not settings:
        raise Exception('user-campaign settings do not exist')

    update_dict = {
        'email_new_comment': email_new_comment,
        'email_post': email_post,
        'email_paid_post': email_paid_post,
        'push_new_comment': push_new_comment,
        'push_post': push_post,
        'push_paid_post': push_paid_post,
        'push_creator_live': push_creator_live
    }
    update_dict = filter_dict(update_dict)
    settings.update(update_dict)


def insert(user_id, campaign_id, email_new_comment=None, email_post=None, email_paid_post=None):
    insert_dict = {
        'user_id': user_id,
        'campaign_id': campaign_id,
        'email_new_comment': email_new_comment,
        'email_post': email_post,
        'email_paid_post': email_paid_post
    }
    insert_dict = filter_dict(insert_dict)
    UserCampaignSetting.insert(insert_dict)


def get(user_id, campaign_id=None):
    return UserCampaignSetting.find(user_id, campaign_id)


def get_by_id(settings_id):
    user_id, campaign_id = settings_id.split('-')[:2]
    return UserCampaignSetting.find(user_id, campaign_id=campaign_id)
