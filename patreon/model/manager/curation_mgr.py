import datetime

import patreon
from patreon.model.type import Category
from patreon.model.table import Curation, Activity
from patreon.model.manager import post_mgr
from patreon.services.db import db_session


@patreon.services.caching.multiget_cached(object_key='Category', default_result=list)
def find_by_category(category_ids):
    return Curation.query \
        .filter(Curation.Category.in_(category_ids)) \
        .order_by(Curation.Weight.desc()) \
        .all()


@patreon.services.caching.multiget_cached(
    object_key='Category', join_table_name='Curation', result_fields='Activity',
    default_result=list
)
def find_posts_by_category(categories):
    return db_session.query(Activity, Curation) \
        .join(Curation.activity) \
        .filter(Curation.Category.in_(categories)) \
        .order_by(Curation.Weight.desc()) \
        .all()


# The below functions exist because they don't function under multiget
def find_posts_by_single_category(category):
    if category is None or category == Category.ALL.category_id:
        return find_all_posts()

    rows = db_session.query(Activity, Curation) \
        .join(Curation.activity) \
        .filter(Curation.Category == category) \
        .order_by(Curation.Weight.desc()) \
        .all()

    return [row.Activity for row in rows]


def find_by_single_category(category):
    if category is None or category == Category.ALL.category_id:
        return find_all_curations()

    return Curation.query \
        .filter_by(Category=category) \
        .order_by(Curation.Weight.desc()) \
        .all()


def find_all_curations(limit=None):
    query = Curation.query \
        .order_by(Curation.Weight.desc())

    if limit:
        query = query.limit(limit)

    return query.all()


def find_all_posts(limit=None):
    curations = find_all_curations(limit)

    post_ids = [curation.post_id for curation in curations]

    posts = post_mgr.get_posts(post_ids)
    posts_dict = {
        post.activity_id: post
        for post in posts
    }
    return [
        posts_dict[curation.post_id]
        for curation in curations
        if curation.post_id in posts_dict
    ]


def set_curated_post(post_id, category, weight):
    return Curation.insert({
        'HID': post_id,
        'Category': category,
        'Weight': weight,
        'Created': datetime.datetime.now()
    })


def remove_curated_post(post_id):
    patreon.services.clear_request_cache()
    Curation.query \
        .filter_by(HID=post_id) \
        .delete()
