from datetime import timedelta
from lazysorted import LazySorted
from patreon.model.notifications import \
    DailyCommentsFromMyCreatorsNotification, \
    DailyCommentsFromMyPatronsNotification, \
    DailyCommentsFromOtherUsersNotification, \
    DailyLikesNotification, MonthlyPledgesNotification, \
    PaymentsProcessingNotification
from patreon.model.manager import action_mgr, feature_flag, new_pledge_mgr, \
    payment_processing_notification_mgr
from patreon.model.table import Comment
from patreon.util import datetime

COMMENT_NOTIFICATION_CLASSES = [
    DailyCommentsFromMyCreatorsNotification,
    DailyCommentsFromMyPatronsNotification,
    DailyCommentsFromOtherUsersNotification
]

OTHER_NOTIFICATION_CLASSES = [
    DailyLikesNotification,
    MonthlyPledgesNotification
]

NOTIFICATION_CLASSES = COMMENT_NOTIFICATION_CLASSES + OTHER_NOTIFICATION_CLASSES


def find_notifications_for_user_paginated(user, per_page, cursor_date, timezone, items_per_card):
    models = []
    for cls in COMMENT_NOTIFICATION_CLASSES:
        if user_should_see_notification_class(user, cls):
            models += find_comments_notification_with_class_paginated(
                user, cls, per_page, cursor_date, timezone, items_per_card
            )
    models += find_likes_notification_paginated(
        user, per_page, cursor_date, timezone, items_per_card=items_per_card
    )
    models += find_payments_processing_notification_paginated(user, per_page, cursor_date, timezone)

    if user_should_see_notification_class(user, MonthlyPledgesNotification):
        # some pledges notifications get filtered out post-SQL
        models = fetch_pledge_notifications_handling_empties(
            user, per_page, cursor_date, timezone, models, items_per_card=items_per_card
        )

    sorted_models = LazySorted(models, key=lambda model: model.sort_key(), reverse=True)

    count = min(per_page, len(sorted_models))
    # Return extra cards per page to put the page breaks at midnight
    # it just makes a lot of things easier.
    while (count + 1 < len(sorted_models) and
                   sorted_models[count - 1].date == sorted_models[count].date):
        count += 1

    return sorted_models[:count]


def fetch_pledge_notifications_handling_empties(user, per_page, cursor_date, timezone, models, items_per_card=5):
    new_cursor = cursor_date
    while True:
        pledge_notifications, empty_pledge_notifications = find_pledges_notification_paginated(
            user, (per_page * 2), new_cursor, timezone, items_per_card=items_per_card
        )
        models += pledge_notifications
        sorted_full = sorted(pledge_notifications, key=lambda model: model.sort_key())
        sorted_empty = sorted(empty_pledge_notifications, key=lambda model: model.sort_key())
        old_cursor = new_cursor
        if sorted_full:
            if sorted_empty:
                new_cursor = min(sorted_full[0].start_time(), sorted_empty[0].start_time())
            else:
                new_cursor = sorted_full[0].start_time()
        elif sorted_empty:
            new_cursor = sorted_empty[0].start_time()
        else:
            new_cursor = None
        if not (len(models) < per_page and len(empty_pledge_notifications) > 0 and old_cursor != new_cursor):
            break
    return models


def user_should_see_notification_class(user, cls):
    if user.is_creator and cls == DailyCommentsFromOtherUsersNotification:
        return False
    if cls == MonthlyPledgesNotification:
        return feature_flag.is_enabled('monthly_pledge_notifications', user_id=user.user_id)
    return True


def find_comments_notification_with_class_paginated(user, notification_class, count=10, cursor_date=None,
                                                    timezone=None, items_per_card=5, comment_id_cursor=None):
    posts_comments_filter = notification_class.posts_comments_filter(user.user_id)
    replies_comments_filter = notification_class.replies_comments_filter(user.user_id)
    if cursor_date:
        posts_comments_filter = posts_comments_filter & Comment.was_posted_on_or_before_date(cursor_date, timezone)
        replies_comments_filter = replies_comments_filter & Comment.was_posted_on_or_before_date(cursor_date, timezone)
    dates = notification_class.dates_matching_filter(
        user.user_id, posts_comments_filter, replies_comments_filter, count, timezone
    )

    return [
        notification_class(user, date_, timezone, items_per_card, comment_id_cursor)
        for date_ in dates
    ]


def find_likes_notification_paginated(user, count=10, cursor_date=None, timezone=None, items_per_card=5, cursor=None):
    dates = action_mgr.find_dates_with_likes_on_posts_by_user_id(user.user_id, count, cursor_date, timezone)
    return [
        DailyLikesNotification(user, date_, timezone, items_per_card, cursor)
        for date_ in dates
    ]


def find_pledges_notification_paginated(user, count=10, cursor_date=None, timezone=None, items_per_card=5, cursor=None):
    dates = new_pledge_mgr.find_months_with_pledges_to_user_id(user.user_id, count, cursor_date, timezone)
    # MonthlyPledgeNotifications take in a date which is the first of the following month
    # 33 days will always get us into the month after the 1st of a month
    # (and find_months_with_pledges_to_user_id returned all 1st-of-the-months)
    dates = [datetime.first_of_the_month(date_ + timedelta(days=33)) for date_ in dates]
    midnights = [datetime.datetime_at_midnight_on_date(date_, tz=timezone) for date_ in dates]
    if cursor_date is None:
        cursor_time = datetime.as_utc(datetime.datetime.now())
    else:
        cursor_time = datetime.datetime_at_midnight_on_date(cursor_date, tz=timezone)
    cards = []
    for i, date_ in enumerate(dates):
        if midnights[i] <= cursor_time:
            cards.append(MonthlyPledgesNotification(user, date_, timezone, items_per_card, cursor))
    full_cards = [card for card in cards if card.total_count() > 0]
    empty_cards = [card for card in cards if card.total_count() == 0]
    return full_cards, empty_cards


def find_payments_processing_notification_paginated(user, count=10, cursor_date=None, timezone=None):
    events = payment_processing_notification_mgr.find_payment_processing_events_for_user_id(
        user.user_id, count, cursor_date, timezone
    )
    return [
        PaymentsProcessingNotification(
            user, event_type, amount_cents, payout_type, event_datetime, timezone
        )
        for (event_datetime, amount_cents, event_type, payout_type) in events
    ]


def get_count_of_notifications_for_user_after_timestamp(user_id, timestamp=None):
    post_comment_filters = [
        klass.posts_comments_filter(user_id)
        for klass in COMMENT_NOTIFICATION_CLASSES
    ]
    if timestamp:
        post_comment_filters = [
            (post_comment_filter & (Comment.created_at >= timestamp))
            for post_comment_filter in post_comment_filters
        ]
    post_comment_counts = [
        Comment.count_on_posts_by_user_id_with_filter(user_id, comment_filter)
        for comment_filter in post_comment_filters
    ]

    reply_comment_counts = [0]
    if feature_flag.is_enabled('comment_reply_notifications', user_id=user_id):
        reply_comment_filters = [
            klass.replies_comments_filter(user_id)
            for klass in COMMENT_NOTIFICATION_CLASSES
        ]
        if timestamp:
            reply_comment_filters = [
                (reply_comment_filter & (Comment.created_at >= timestamp))
                for reply_comment_filter in reply_comment_filters
            ]
        reply_comment_counts = [
            Comment.count_on_comments_by_user_id_with_filter(user_id, comment_filter)
            for comment_filter in reply_comment_filters
        ]
    comments_count = sum(post_comment_counts + reply_comment_counts)

    likes_count = len(action_mgr.find_likes_on_posts_by_user_id_between_timestamps(
        user_id, start_time=timestamp
    ))

    pledges_count = len(new_pledge_mgr.find_pledges_to_user_id_between_timestamps(
        user_id, start_time=timestamp
    ))
    return comments_count + likes_count + pledges_count
