import datetime
from patreon.model.dbm import change_email_dbm
from patreon.model.table import ChangeEmail
from patreon.util.random import get_verification_token


def insert(email, user_id):
    verification = get_verification_token()
    ChangeEmail.insert({
        'UID': user_id,
        'Email': email,
        'Verification': verification,
        'Created': datetime.datetime.now()
    })
    return verification


def get(verification):
    return ChangeEmail.get(verification=verification)


def find_by_user_id(user_id):
    return change_email_dbm.find_by_user_id(user_id)
