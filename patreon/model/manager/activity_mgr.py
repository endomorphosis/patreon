import patreon
from patreon.constants import mc_key
from patreon.exception.post_errors import PostNotFound
from patreon.model.dbm import activity_dbm, rewards_give_dbm
from patreon.model.manager import action_mgr, category_mgr, rewards_give_mgr, bill_mgr
from patreon.model.table import Activity
from patreon.services.caching import memcache
from patreon.util import datetime

SHARES_PER_PAGE = patreon.globalvars.get('SHARES_PER_PAGE')


@patreon.services.caching.request_cached()
def get(activity_id):
    return activity_dbm.get_public_activity(activity_id).first()


@patreon.services.caching.request_cached()
def find_by_id_list(activity_ids):
    if not activity_ids:
        return []

    return activity_dbm\
        .find_by_activity_id_list(activity_ids)\
        .all()


@patreon.services.caching.request_cached()
def find_posts_by_user_id_paginated(user_id, page=1):
    limit = SHARES_PER_PAGE
    offset = (page - 1) * SHARES_PER_PAGE
    return activity_dbm\
        .find_public_by_user_id(user_id) \
        .order_by(Activity.published_at.desc())\
        .offset(offset)\
        .limit(limit)


@patreon.services.caching.request_cached()
def find_posts_by_user_id_and_link_urls(user_id, link_urls):
    return activity_dbm \
        .find_all_by_user_id(user_id) \
        .filter(Activity.link_url.in_(link_urls)) \
        .all()

@patreon.services.caching.request_cached()
def find_posts_by_campaign_id_paginated(campaign_id, page=1):
    if page > 1:
        limit = SHARES_PER_PAGE
        offset = 5 + max(0, (page - 2)) * SHARES_PER_PAGE
    else:
        # SPEEDHAX
        limit = 5
        offset = 0
    return activity_dbm\
        .find_public_by_campaign_id(campaign_id) \
        .order_by(Activity.published_at.desc()) \
        .offset(offset) \
        .limit(limit)


@patreon.services.caching.request_cached()
def find_creator_posts_by_campaign_id_paginated(campaign_id, page=1):
    if page > 1:
        limit = SHARES_PER_PAGE
        offset = 5 + max(0, (page - 2)) * SHARES_PER_PAGE
    else:
        # SPEEDHAX
        limit = 5
        offset = 0

    return activity_dbm\
        .find_public_by_campaign_id(campaign_id) \
        .filter(Activity.was_posted_by_owner) \
        .order_by(Activity.published_at.desc()) \
        .offset(offset) \
        .limit(limit)


@patreon.services.caching.request_cached()
def find_activities_by_campaign_id_paginated(campaign_id, page=1):
    if page > 1:
        limit = SHARES_PER_PAGE
        offset = 5 + max(0, (page - 2)) * SHARES_PER_PAGE
    else:
        # SPEEDHAX
        limit = 5
        offset = 0

    return activity_dbm\
        .find_public_by_campaign_id(campaign_id) \
        .filter_by(is_creation=0) \
        .order_by(Activity.published_at.desc()) \
        .offset(offset) \
        .limit(limit)


@patreon.services.caching.request_cached()
def find_creations_by_campaign_id(campaign_id):
    return activity_dbm\
        .find_public_by_campaign_id(campaign_id)\
        .filter_by(is_creation=1)

@patreon.services.caching.request_cached()
def find_creator_posts_by_campaign_id(campaign_id):
    return activity_dbm\
        .find_public_by_campaign_id(campaign_id)\
        .filter(Activity.was_posted_by_owner)

@patreon.services.caching.request_cached()
def find_activities_by_campaign_id(campaign_id):
    return activity_dbm\
        .find_public_by_campaign_id(campaign_id)\
        .filter_by(is_creation=0)


@patreon.services.caching.request_cached()
def find_paid_by_campaign_id(campaign_id):
    return activity_dbm\
        .find_public_by_campaign_id(campaign_id)\
        .filter_by(is_paid=1)


@patreon.services.caching.request_cached()
def get_creator_post_count_by_campaign_id(campaign_id):
    return find_creator_posts_by_campaign_id(campaign_id).count()


@patreon.services.caching.request_cached()
def get_activity_count_by_campaign_id(campaign_id):
    return find_activities_by_campaign_id(campaign_id).count()


@patreon.services.caching.request_cached()
def get_post_count_by_campaign_id(campaign_id):
    return activity_dbm\
        .find_public_by_campaign_id(campaign_id)\
        .count()


@patreon.services.caching.request_cached()
def find_searchable_with_limit(limit, offset):
    return activity_dbm\
        .find_searchable()\
        .limit(limit).offset(offset)


@patreon.services.caching.request_cached()
def get_count_paid_posts_in_last_year_by_campaign_id(campaign_id):
    return find_paid_by_campaign_id(campaign_id).count()


def find_searchable_by_id_list(activity_ids):
    return activity_dbm\
        .find_searchable()\
        .filter(Activity.activity_id.in_(activity_ids))


@patreon.services.caching.request_cached()
def update_likes(activity_id):
    activity = activity_dbm.get_public_activity(activity_id)
    if not activity:
        return
    count = action_mgr.find_likes_by_activity_id(activity_id).count()
    activity.update({
        'like_count': count
    })


def algolia_id_for_id(id_):
    return "post_" + str(id_)


def get_deleted_activity_or_throw(post_id):
    activity = Activity.query \
        .filter(Activity.activity_id == post_id) \
        .filter(Activity.deleted_at.isnot(None)) \
        .first()

    if activity is None:
        raise PostNotFound(post_id)
    return activity


def get_activity_or_throw(post_id):
    activity = get_activity(post_id)
    if activity is None:
        raise PostNotFound(post_id)
    return activity


def get_activity(activity_id):
    return Activity.query \
        .filter(Activity.activity_id == activity_id) \
        .filter(Activity.deleted_at.is_(None)) \
        .first()


def delete_activity(post_id):
    deleted_at = patreon.util.unsorted.now()
    get_activity(post_id).update({
        'deleted_at': deleted_at,
        'edited_at': deleted_at
    })
    rewards_give_mgr.delete_by_post_id(post_id)
    bill_mgr.delete_by_post_id(post_id)

    return post_id


def undelete_activity(post_id):
    undeleted_at = patreon.util.unsorted.now()
    get_deleted_activity_or_throw(post_id).update({
        'deleted_at': None,
        'edited_at': undeleted_at
    })


def update_activity(post_id, post_type, title, content,
                    min_cents_pledged_to_view, embed_dict, category, is_paid):
    edited_at = datetime.datetime.now()

    embed_dict = embed_dict or {}

    unsafe_category = category_mgr.get_category(category)
    safe_category = unsafe_category if unsafe_category and unsafe_category.is_user_category else None

    get_activity(post_id).update({
        'activity_type': post_type.value,
        'post_type': post_type.name.lower(),
        'activity_title': title,
        'activity_content': content,
        'min_cents_pledged_to_view': min_cents_pledged_to_view or 0,
        'link_url': embed_dict.get('url'),
        'link_subject': embed_dict.get('subject'),
        'link_domain': embed_dict.get('provider'),
        'link_description': embed_dict.get('description'),
        'link_embed': embed_dict.get('html'),
        'category': safe_category.category_id if safe_category else None,
        'is_paid': is_paid,
        'edited_at': edited_at
    })
    memcache.delete(mc_key.share(post_id))


def update_activity_image(post_id, image_width, image_height, key, extension):
    edited_at = datetime.datetime.now()

    # todo(postpost): Move validations to another layer.
    safe_width = max(int(image_width or 0), 0)
    safe_height = max(int(image_height or 0), 0)

    get_activity(post_id).update({
        'image_width': safe_width,
        'image_height': safe_height,
        'photo_key': key,
        'photo_extension': extension,
        'edited_at': edited_at
    })
    memcache.delete(mc_key.share(post_id))


def update_activity_thumbnail(post_id, thumbnail_key):
    edited_at = datetime.datetime.now()

    get_activity(post_id).update({
        'thumbnail_key': thumbnail_key,
        'edited_at': edited_at
    })
    memcache.delete(mc_key.share(post_id))


def update_activity_file(post_id, file_name, file_key):
    edited_at = datetime.datetime.now()

    get_activity(post_id).update({
        'file_key': file_key,
        'file_name': file_name,
        'edited_at': edited_at
    })
    memcache.delete(mc_key.share(post_id))


@patreon.services.caching.request_cached()
def is_blocked_due_to_failed_payment(post_id, patron_id):
    post = Activity.get(post_id)
    if not post.is_paid:
        return False

    invoice = rewards_give_dbm.get_most_recent_invoice_for_patron_and_creator(
        patron_id=patron_id, creator_id=post.user_id
    )

    return (
        invoice is not None
        and invoice.created_at > datetime.beginning_of_this_month()
        and invoice.declined
    )
