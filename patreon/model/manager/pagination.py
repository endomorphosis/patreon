from patreon.exception import ParameterInvalid
from sqlalchemy import asc, desc


def paginate_cursor(query, sort_column, ascending=True, cursor=None, per_page=10):
    if cursor:
        if ascending:
            query = query.filter(sort_column > cursor)
        else:
            query = query.filter(sort_column < cursor)

    query = query.order_by(asc(sort_column) if ascending else desc(sort_column))

    if per_page != 'infinity':
        query = query.limit(per_page)

    return query.all()


def paginate_offset(query, sort_commands, offset=0, per_page=10):
    query = query.order_by(*sort_commands)

    if offset:
        query = query.offset(offset)

    if per_page != 'infinity':
        query = query.limit(per_page)

    return query.all()
