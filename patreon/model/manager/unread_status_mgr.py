import datetime
from patreon.model.table import UnreadStatus
from patreon.model.type import UnreadStatusType
from patreon.model.manager import post_mgr, campaign_mgr, pledge_mgr, follow_mgr


def mark_as_read(user_id, unread_key_enum, timestamp):
    unread_key_str = get_key_as_string(unread_key_enum)
    unread_status = UnreadStatus.get(user_id=user_id, unread_key=unread_key_str)
    if unread_status:
        unread_status.update({
            'read_timestamp': timestamp
        })
    else:
        UnreadStatus.insert({
            'user_id': user_id,
            'unread_key': unread_key_str,
            'read_timestamp': timestamp
        })


def mark_as_read_now(user_id, unread_key_enum):
    mark_as_read(user_id, unread_key_enum, datetime.datetime.now())


def get_or_create(user_id, unread_key_enum):
    unread_key_str = get_key_as_string(unread_key_enum)
    unread_status = UnreadStatus.get(user_id=user_id, unread_key=unread_key_str)
    if unread_status:
        return unread_status
    else:
        UnreadStatus.insert({
            'user_id': user_id,
            'unread_key': unread_key_str,
            'read_timestamp': datetime.datetime(1970, 1, 1)
        })
        return UnreadStatus.get(user_id=user_id, unread_key=unread_key_str)


def latest_read_time(user_id, unread_key_enum):
    return get_or_create(user_id, unread_key_enum).read_timestamp


def get_unread_count_for_campaign_creations(campaign_id, user_id):
    unread_timestamp = latest_read_time(user_id, UnreadStatusType.creations)
    return post_mgr.get_count_of_creations_after_timestamp(campaign_id, unread_timestamp)


def unread_count(user_id, unread_key_enum):
    unread_key_str = get_key_as_string(unread_key_enum)
    if unread_key_str == UnreadStatusType.notifications.name:
        return unread_count_for_notifications(user_id)
    elif unread_key_str == UnreadStatusType.creations.name:
        return unread_count_for_creations(user_id)


def unread_count_for_notifications(user_id):
    from patreon.model.manager import notifications_mgr
    unread_timestamp = latest_read_time(user_id, UnreadStatusType.notifications)
    return notifications_mgr.get_count_of_notifications_for_user_after_timestamp(user_id, unread_timestamp)


def unread_count_for_creations(user_id):
    unread_timestamp = latest_read_time(user_id, UnreadStatusType.creations)
    followed_users = [pledge.creator_id for pledge in pledge_mgr.find_by_patron_id(user_id)]
    followed_users += [follow.followed_id for follow in follow_mgr.find_by_follower_id(user_id)]
    followed_campaigns = [campaign_id
        for user in followed_users
        for campaign_id in campaign_mgr.get_campaign_ids_by_user_id(user.user_id)]
    count = 0
    for campaign_id in followed_campaigns:
        count += post_mgr.get_count_of_creations_after_timestamp(campaign_id, unread_timestamp)
    return count


def get_key_as_string(unread_key_enum):
    try:
        unread_key_str = unread_key_enum.name
    except AttributeError:
        unread_key_str = unread_key_enum
    return unread_key_str
