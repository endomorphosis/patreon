import datetime
from collections import defaultdict

import patreon
from patreon.model.dbm.action_dbm import ActionDBM
from patreon.model.table import Actions
from patreon.model.type import ActionType
from patreon.services.logging.unsorted import log_json
from patreon.util import datetime as datetime_helper


def get_like(user_id, activity_id):
    return Actions.get(user_id, activity_id, ActionType.LIKE.value)


def find_likes_by_user_id(user_id):
    if not user_id:
        return []
    return ActionDBM().likes_by_user(user_id)

def execute_like(user_id, activity_id, add_like):
    change = 0
    existing = get_like(user_id=user_id, activity_id=activity_id)
    if not add_like and existing:
        existing.delete()
        change = -1
    elif add_like and not existing:
        change = 1
        like_activity(user_id, activity_id)

    patreon.model.manager.activity_mgr.update_likes(activity_id)
    log_json({
        'verb': 'like',
        'user_id': user_id,
        'creation_id': activity_id,
        'change': change
    })

    return get_like(user_id=user_id, activity_id=activity_id)


def find_likes_by_user_id_creation_dict(user_id):
    likes = find_likes_by_user_id(user_id)
    return_dict = defaultdict(lambda: None)
    for like in likes:
        return_dict[like.post_id] = like

    return return_dict


def find_likes_by_activity_id(activity_id):
    return ActionDBM().type_is_like().post_id_is(activity_id)


def find_likes_by_user_id_paginated(user_id, page=1):
    result = find_likes_by_user_id(user_id)\
        .order_by('created_at', descending=True)\
        .paged(page)\
        .all()

    return [element.post_id for element in result]


def find_likes_by_activity_id_paginated_by_cursor(activity_id, cursor_date=None, per_page=25):
    query = find_likes_by_activity_id(activity_id)
    if cursor_date is not None:
        query = query.filter(Actions.created_at < cursor_date)
    return query \
        .order_by('created_at', descending=True) \
        .limit(per_page) \
        .all()


def like_activity(user_id, activity_id):
    return insert(user_id, activity_id)


def insert(user_id, activity_id, action_type=ActionType.LIKE.value):
    Actions.insert({
        'UID': user_id,
        'HID': activity_id,
        'Type': action_type,
        'Created': datetime.datetime.now()
    })
    return Actions.get(user_id, activity_id, action_type)


def find_likes_on_posts_by_user_id_between_timestamps(user_id, start_time=None, end_time=None):
    dbm = ActionDBM().likes_on_posts_by_user(user_id)
    if start_time:
        dbm.created_on_or_after(start_time)
    if end_time:
        dbm.created_before(end_time)
    return dbm.all()


def find_dates_with_likes_on_posts_by_user_id(user_id, limit=10, cursor_date=None, timezone_=None):
    created_date = datetime_helper.timezone_aware_sql_date_func(Actions.created_at, timezone_=timezone_)
    dbm = ActionDBM().likes_on_posts_by_user(user_id) \
        .with_entity(created_date.label('date')) \
        .order_by('created_at', descending=True) \
        .join_posts()

    if cursor_date:
        newest_timestamp = patreon.util.datetime.datetime_at_midnight_on_date(
            cursor_date + datetime.timedelta(1), timezone_
        )
        dbm.created_before(newest_timestamp)

    rows = dbm.prepare_query().group_by(created_date).limit(limit).all()

    return [
        row.date
        for row in rows
        if row.date is not None
    ]
