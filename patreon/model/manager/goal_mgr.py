import datetime
from patreon.constants import money

from patreon.model.dbm import goal_dbm
from patreon.model.table import Goal
from patreon.model.manager import pledge_mgr, new_pledge_mgr, email_settings_mgr, user_mgr, campaign_mgr
from patreon.services.logging.unsorted import log_state_change, log_anomaly
from patreon.services.mail import send_template_email
from patreon.services.mail.templates import GoalReached
from patreon.util.html_cleaner import clean
from patreon.util.collections import filter_dict
from patreon.util.unsorted import get_int, dollar_string_to_cents


def get(goal_id):
    return Goal.get(goal_id=goal_id)


def get_goal_or_throw(goal_id):
    goal = get(goal_id)
    if goal is None:
        raise Exception('Goal does not exist')
    return goal


def delete(goal_id, actor_id):
    if not is_authorized(goal_id, actor_id):
        raise Exception('Not authorized')

    goal_dbm.get_by_goal_id(goal_id).delete()


def find_by_campaign_id(campaign_id):
    if campaign_id is None:
        return None

    return goal_dbm.find_by_campaign_id(campaign_id).all()


def find_unmet_goals_by_campaign_id(campaign_id, pledge_total_cents):
    return goal_dbm.find_unmet_goals_by_campaign_id(campaign_id, pledge_total_cents).all()


def find_unreached_goals_by_campaign_id(campaign_id):
    return goal_dbm.find_unreached_goals_by_campaign_id(campaign_id).all()


def find_met_goals_by_campaign_id(campaign_id, pledge_total_cents):
    if not pledge_total_cents:
        return []
    return goal_dbm.find_met_goals_by_campaign_id(campaign_id, pledge_total_cents).all()


def insert(campaign_id, amount_cents, goal_title, goal_text=None):
    if not get_int(amount_cents):
        raise TypeError('amount_cents')

    return Goal.insert({
        'campaign_id': campaign_id,
        'amount_cents': get_int(amount_cents),
        'goal_title': goal_title,
        'goal_text': goal_text,
        'created_at': datetime.datetime.now()
    })


def reach_goal(goal_id):
    update_dict = {
        'reached_at': datetime.datetime.now()
    }
    __update(goal_id, update_dict)


def update(goal_id, actor_id, amount_cents=None, goal_title=None, goal_text=None):
    if not is_authorized(goal_id, actor_id):
        raise Exception('Not authorized')

    update_dict = filter_dict({
        'amount_cents': amount_cents,
        'goal_title': goal_title,
        'goal_text': goal_text
    })
    __update(goal_id, update_dict)


def is_authorized(goal_id, actor_id):
    goal = get_goal_or_throw(goal_id)

    authorized_users = goal.campaign.campaigns_users
    authorized_user_ids = [element.user_id for element in authorized_users]
    return get_int(actor_id) in authorized_user_ids


def __update(goal_id, update_dict):
    get_goal_or_throw(goal_id).update(update_dict)


def delete_missing_goals(campaign_id, user_id, goal_ids):
    current_goals = find_by_campaign_id(campaign_id)
    for goal in current_goals:
        if str(goal.goal_id) not in goal_ids:
            log_state_change('goal', goal.goal_id, 'delete', '/create3')
            delete(goal.goal_id, user_id)


def set_goals(campaign_id, user_id, goal_dicts):
    if not goal_dicts:
        return False

    goal_ids = [goal_dict['goal_id'] for goal_dict in goal_dicts]

    delete_missing_goals(campaign_id, user_id, goal_ids)

    for goal_dict in goal_dicts:
        amount = dollar_string_to_cents(goal_dict['goal_amount'])

        if not amount:
            continue

        success = create_or_update_goal(
            campaign_id,
            user_id,
            goal_id=goal_dict['goal_id'],
            goal_title=goal_dict['goal_title'],
            goal_desc=goal_dict['goal_desc'],
            goal_amount=amount
        )

        if not success:
            return False
    return True


def create_or_update_goal(campaign_id, user_id, goal_id, goal_title, goal_desc, goal_amount):
    # Handle mysql overflow case
    amount = min(goal_amount, money.MAX_DB_CENTS)

    if not goal_id:
        goal_id = insert(campaign_id, amount, goal_title, goal_desc)
        log_state_change('goal', goal_id, 'create', '/create3')
        return True

    goal_desc = clean(goal_desc)
    goal_title = clean(goal_title)

    goal = get(goal_id=goal_id)
    if not goal:
        log_anomaly('goal', goal_id, None, 'edit', 'nonexistent')
        return False
    if goal.campaign_id != campaign_id:
        log_anomaly('goal', goal_id, goal.campaign_id, 'edit', 'unauthorized')
        return False

    update(goal_id, user_id, amount, goal_title, goal_desc)
    log_state_change('goal', goal.goal_id, 'update', '/create3')
    return True


def get_goal_email_recipients_for_campaign(campaign_id):
    patron_ids = new_pledge_mgr.get_patron_ids_for_campaign_id(campaign_id)
    email_settings = email_settings_mgr.get_email_settings_for_users(patron_ids)
    user_ids_allowing_email_goal = [
        email_setting.user_id
        for email_setting in email_settings
        if email_setting.email_goal
    ]
    recipients = user_mgr.get_users(user_ids_allowing_email_goal)
    return recipients


def check_if_goal_reached(campaign_id):
    unreached_goals = find_unreached_goals_by_campaign_id(campaign_id)
    if not unreached_goals:
        return
    unreached_goal = unreached_goals[0]
    if unreached_goal.amount_cents <= pledge_mgr.get_pledge_total_by_campaign_id(campaign_id):
        recipients = get_goal_email_recipients_for_campaign(campaign_id)
        creator = campaign_mgr.get(campaign_id).users[0]
        send_template_email(
            GoalReached(
                recipients, creator, unreached_goal.amount_cents,
                unreached_goal.goal_title
            )
        )

        reach_goal(unreached_goal.goal_id)

