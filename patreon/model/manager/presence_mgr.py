import patreon
from patreon.model.push_notifications.creator_go_live_on_post_push import CreatorGoLiveOnPostPush
from patreon.model.table import Presence, Activity
from patreon.model.dbm import presence_dbm
from patreon.services.logging.unsorted import log_state_change
from patreon.util import datetime


def get(presence_id):
    return presence_dbm.get(presence_id)


@patreon.services.caching.multiget_cached(object_key='user_id', default_result=list)
def find_by_user_id(user_ids):
    return presence_dbm.find_by_user_id_list(user_ids).all()


def find_live_by_user_ids(user_ids):
    return presence_dbm.find_by_user_id_list(user_ids) \
        .filter_by(is_live=True).all()


def find_by_activity_id(activity_id):
    return find_by_room(Presence.room_name_for_activity_id(activity_id))


@patreon.services.caching.multiget_cached(object_key='room')
def find_by_room(rooms):
    return Presence.query \
        .filter(Presence.room.in_(rooms)) \
        .all()


def update_presence(user_id, room=None, last_seen=None, expiration_time=None):
    if room is None:
        room = 'global'
    if last_seen is None:
        last_seen = datetime.datetime.now()
    if expiration_time is None:
        expiration_time = last_seen + datetime.timedelta(minutes=3)
    log_state_change('presence', user_id, 'create', 'add_presence')
    db_dict = {
        'user_id': user_id,
        'room': room,
        'last_seen': last_seen,
        'expiration_time': expiration_time
    }
    presence = presence_dbm.get_by_user_id_and_room(user_id, room)
    if presence:
        # only push if our old presence has expired
        should_push = presence.expiration_time < datetime.datetime.now() < expiration_time
        presence.update(db_dict)
        presence_id = presence.presence_id
    else:
        should_push = datetime.datetime.now() < expiration_time
        res = Presence.insert(db_dict)
        presence_id = res[0] if res else None

    if should_push and Presence.activity_id_for_room_name(room):
        CreatorGoLiveOnPostPush(Activity.get(Presence.activity_id_for_room_name(room))).publish()

    return presence_id
