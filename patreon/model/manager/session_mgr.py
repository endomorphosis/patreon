from _sha256 import sha256
import datetime
from OpenSSL import rand

from patreon.model.table import Session
from patreon.services.logging.unsorted import log_json


def get_new_session_id():
    sid = 'py' + sha256(rand.bytes(2560)).hexdigest()
    sid = sid[:127]
    log_json({
        'verb': 'session_assign',
        'sid': sid
    })
    while get_session(sid):
        log_json({
            'verb': 'session_collide',
            'sid': sid
        })
        sid = 'py' + sha256(rand.bytes(2560)).hexdigest()
        sid = sid[:127]
    return sid


def next_expiration_time(max_age):
    return datetime.datetime.now() + datetime.timedelta(seconds=max_age)


def update_session(session_id, user_id, csrf_token, csrf_token_expiration, is_admin, extra_data, max_age):
    Session.insert_on_duplicate_key_update({
        'session_token': session_id,
        'user_id': user_id,
        'csrf_token': csrf_token,
        'csrf_token_expires_at': csrf_token_expiration,
        'is_admin': is_admin,
        'extra_data_json': extra_data,
        'created_at': datetime.datetime.now(),
        'expires_at': next_expiration_time(max_age)
    })
    return Session.get(session_id)

def destroy_session(session_id):
    session_instance = Session.get(session_id)
    if session_instance:
        session_instance.delete()


def collect_garbage():
    Session.query.filter(datetime.datetime.now >= Session.expires_at).delete()


def get_session(session_id):
    return Session.get(session_id)


def get_blank_session(session_id):
    blank_session = Session()
    blank_session.session_token = session_id
    blank_session.user_id = 0
    blank_session.csrf_token = None
    blank_session.csrf_token_expires_at = None
    blank_session.created_at = datetime.datetime.now()
    blank_session.expires_at = None
    blank_session.extra_data_json = '{}'
    blank_session.is_admin = 0

    return blank_session
