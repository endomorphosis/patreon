from patreon.exception.payment_errors import LedgerEntrySanityCheckFail
from patreon.model.dbm import LedgerEntryDBM
from patreon.model.manager import account_mgr
from patreon.model.manager.payment_event_mgr import capture_exception_payment_events
from patreon.model.table import LedgerEntry, Txn


def insert_ledger_entry(transaction_id, account_id, amount_cents):
    return LedgerEntry.insert({
        'txn_id': transaction_id,
        'account_id': account_id,
        'amount_cents': amount_cents
    })


def move_money_between_accounts(transaction_id, from_account, to_account, amount_cents):
    LedgerEntry.insert({
        'txn_id': transaction_id,
        'account_id': from_account,
        'amount_cents': -1 * amount_cents
    })
    LedgerEntry.insert({
        'txn_id': transaction_id,
        'account_id': to_account,
        'amount_cents': amount_cents
    })


@capture_exception_payment_events
def assert_transaction_ledger_sane(transaction_id):
    running_total = 0
    transaction = Txn.get(transaction_id)

    for ledger_entry in transaction.ledger_entries:
        running_total += ledger_entry.amount_cents

    if running_total != 0:
        raise LedgerEntrySanityCheckFail(transaction_id, running_total)


def get_total_by_account_id(account_id):
    result = LedgerEntryDBM().get_monetary_sum(label='sum').for_account_id(account_id).first()
    if not result:
        return 0
    return result.sum or 0


def get_credit_total_for_user(user_id):
    account_id = account_mgr.get_user_credit_account_id_or_throw(user_id)
    return get_total_by_account_id(account_id)
