import patreon
from patreon.exception.pledge_errors import PledgeNotExist
from patreon.model.dbm import pledge_dbm, rewards_give_dbm
from patreon.model.manager import address_mgr, campaign_mgr, follow_mgr, \
    pledge_helper, user_campaign_settings_mgr, user_mgr, pledge_history_mgr
from patreon.model.table import Pledge, PledgesDelete
from patreon.services.logging.unsorted import log_state_change
from patreon.util import datetime


def get(patron_id, creator_id):
    return get_pledge(patron_id, creator_id)


def get_pledge(patron_id, creator_id):
    return pledge_dbm.get(patron_id, creator_id)


def is_patron(patron_id, creator_id):
    return get_pledge(patron_id, creator_id) is not None


@patreon.services.caching.multiget_cached(object_key='UID', default_result=list)
def find_by_patron_id(patron_ids):
    return pledge_dbm.find_by_patron_ids(patron_ids).all()


@patreon.services.caching.request_cached()
def find_by_creator_id(user_id):
    return pledge_dbm.find_by_creator_id(user_id).all()


@patreon.services.caching.request_cached()
def find_all_by_creator_id(creator_id):
    return pledge_dbm.find_all_by_creator_id(creator_id).all()


@patreon.services.caching.request_cached()
def find_all_by_creator_id_and_above_cents(creator_id, cents):
    return pledge_dbm.find_by_creator_id_and_above_cents(creator_id, cents).all()


@patreon.services.caching.request_cached()
def find_invalid_by_user_id(user_id):
    return Pledge.query \
        .filter_by(UID=user_id) \
        .filter(Pledge.Invalid != None) \
        .all()


@patreon.services.caching.request_cached()
def find_by_creator_id_paginated(creator_id, page=1):
    return pledge_dbm.find_by_creator_id_paginated(creator_id, page).all()


@patreon.services.caching.request_cached()
def find_top_patrons_by_creator_id(creator_id):
    return pledge_dbm.find_top_patrons_by_creator_id(creator_id)


@patreon.services.caching.multiget_cached(
    object_key='CID', result_fields='sum', default_result=0
)
def get_pledge_total_by_creator_id(creator_ids):
    return pledge_dbm.get_pledge_total_by_creator_id(creator_ids)


def get_pledge_total_by_campaign_id(campaign_id):
    creator_id = campaign_mgr.try_get_user_id_by_campaign_id_hack(campaign_id)
    return get_pledge_total_by_creator_id(creator_id)


@patreon.services.caching.multiget_cached(
    object_key='UID', result_fields='sum', default_result=0
)
def get_pledge_total_by_patron_id(patron_ids):
    return pledge_dbm.get_pledge_total_by_patron_id(patron_ids)


@patreon.services.caching.multiget_cached(
    object_key='CID', result_fields='count', default_result=0
)
def get_patron_count_by_creator_id(creator_id):
    return pledge_dbm.get_patron_count_by_creator_ids(creator_id)


@patreon.services.caching.request_cached()
def find_days_supported_by_creator_id(creator_id):
    return pledge_dbm.find_days_supported_by_creator_id(creator_id)


def get_patrons_and_pledges_for_creator(creator_id):
    """Returns a dictionary with signature: (ID[User]: (User, Pledge))."""
    pledges_dict = {}
    pledges_to_creator = find_by_creator_id(creator_id)

    for pledge in pledges_to_creator:
        pledges_dict[pledge.patron_id] = {"pledge": pledge}

    patrons = user_mgr.get_users(pledges_dict.keys())
    for patron in patrons:
        pledges_dict[patron.user_id]["patron"] = patron

    return pledges_dict


def invalidate_pledge(patron_id, creator_id):
    return pledge_dbm.invalidate_pledge(
        patron_id, creator_id, datetime.datetime.now()
    )


def delete_pledge(patron_id, campaign_id):
    creator_id = campaign_mgr.try_get_user_id_by_campaign_id_hack(campaign_id)
    old_pledge = get(patron_id, creator_id)

    if not old_pledge:
        raise PledgeNotExist

    old_amount = old_pledge.Pledge
    old_pledge.delete()
    pledges_delete = PledgesDelete.get(user_id=patron_id, creator_id=creator_id)
    if pledges_delete:
        # "BURN THE BOOKS" - PHP, circa 2013
        pledges_delete.delete()

    PledgesDelete.insert({
        'CID': creator_id,
        'UID': patron_id,
        'Amount': old_amount,
        'Created': datetime.datetime.now()
    })

    log_state_change('pledge', str((patron_id, creator_id)), 'delete', 'delete_pledge')

    return None


def create_or_update_pledge(patron_id, creator_id, amount, pledge_cap, reward_id, card_id,
                            notify_all_activity=1, notify_paid_creation=1, address=None,
                            created_at=None, email_new_comment=1, patron_pays_fees=0,
                            address_id=None):
    pledge_helper.check_pledge_cap_amount_is_multiple_of_pledge(amount, pledge_cap)

    campaign_id = campaign_mgr.try_get_campaign_id_by_user_id_hack(creator_id)
    if not campaign_id:
        raise ValueError("Invalid campaign for creator_id = {}".format(creator_id))

    pledge_helper.check_nsfw_does_not_use_paypal(campaign_id, card_id)

    # Manage waitlists
    pledge = get(patron_id=patron_id, creator_id=creator_id)
    current_reward_tier = None
    if pledge:
        current_reward_tier = pledge.RID
    pledge_helper.check_reward_tier(patron_id, amount, reward_id, current_reward_tier)

    created_at = created_at or datetime.datetime.now()

    # Actually create the pledge in the pledge table
    pledge_dbm.update_or_insert({
        'CID': creator_id,
        'UID': patron_id,
        'Pledge': amount,
        'MaxAmount': pledge_cap,
        'RID': reward_id,
        'CardID': card_id or '',
        'address_id': address_id,
        'Street': address and address.address_line_1,
        'Street2': address and address.address_line_2,
        'Zip': address and address.address_zip,
        'City': address and address.city,
        'State': address and address.state,
        'Country': address and address.country,
        'Created': created_at,
        'PatronPaysFees': (patron_pays_fees == 1)
    })
    log_state_change('pledge', str((patron_id, creator_id)), 'replace', 'create_or_update_pledge')

    # Initialize campaign settings to email on everything,
    # but only if it doesn't already exist
    user_campaign_settings_mgr.insert_if_new(
        patron_id,
        campaign_id,
        email_new_comment=email_new_comment,
        email_post=notify_all_activity,
        email_paid_post=notify_paid_creation
    )

    # Thought about this for awhile, but might as well update the card info for
    # all the rewards, too
    rewards_give_dbm.move_failed_payments_for_patron_and_creator_to_card(
        patron_id, creator_id, card_id
    )

    pledge_history_mgr.insert_pledge_history(creator_id, patron_id, amount, pledge_cap)

    # Override follows with pledging
    follow = follow_mgr.get(followed_id=creator_id, follower_id=patron_id)
    if follow:
        follow_mgr.delete(follow.follower_id, follow.followed_id)


def get_recent_pledges_from_all_users(count=20):
    return pledge_dbm.get_recent_pledges_from_all_users(count)
