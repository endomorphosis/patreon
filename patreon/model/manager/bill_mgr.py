import datetime
from patreon.model.dbm import bill_dbm
from patreon.model.manager import campaign_mgr, complete_pledge_mgr
from patreon.model.type import BillType


def get_bills_for_campaign(campaign_id):
    return bill_dbm.get_bills_for_campaign(campaign_id)


def get_bills_for_patron(user_id):
    return bill_dbm.get_bills_for_patron(user_id)


def get_bills_for_post(post_id):
    return bill_dbm.get_bills_for_post(post_id)


# Need to add to: make_post_mgr ln 394
def create_bill_for_every_patron(pledge_table, creator_id, post_id):

    # Note: patron_table has patron_ids as keys, with {"pledge": pledge} as the value
    # or {"patron": patron}

    patron_ids = pledge_table.keys()
    campaign_id, creator_id = campaign_mgr.get_campaign_and_creator_ids(creator_id=creator_id)

    for id in patron_ids:
        pledge = pledge_table[id]["pledge"]   # This is a legacy pledge
        patron = pledge_table[id]["patron"]

        # Fixme: Eventually we should change the pledge_table to hold the new_pledge
        # but for now we just look up the new pledge.
        legacy, new_pledge = complete_pledge_mgr.get_pledge_by_patron_and_campaign_or_creator(
            patron_id=pledge.patron_id, creator_id=pledge.creator_id
        )

        pledge_id = new_pledge.pledge_id

        create_bill(
            user_id=id, campaign_id=campaign_id, pledge_id=pledge_id,
            amount_cents=pledge.amount, bill_type=BillType.per_post,
            post_id=post_id, vat_charge_id=None, billing_cycle=None
        )


def create_bill(user_id, campaign_id, pledge_id, amount_cents, bill_type,
                post_id=None, vat_charge_id=None, billing_cycle=None,
                created_at=None, attempted_at=None, failed_at=None, succeeded_at=None):
    created_at = created_at or datetime.datetime.now()

    return bill_dbm.create_bill(
        user_id, campaign_id, pledge_id, vat_charge_id, amount_cents, bill_type,
        post_id, billing_cycle, created_at, attempted_at, failed_at, succeeded_at
    )


# Need to add to: pending.py ln 34
def delete_by_user_id_campaign_id(user_id, campaign_id):
    return bill_dbm.delete_by_user_id_campaign_id(user_id, campaign_id)


# Need to add to: activity_mgr.py ln 183
def delete_by_post_id(post_id):
    return bill_dbm.delete_by_post_id(post_id)


def clear_post_cache(post_id):
    return bill_dbm.clear_post_cache(post_id)
