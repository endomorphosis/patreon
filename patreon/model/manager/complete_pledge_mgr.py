import datetime
from patreon.model.manager import address_mgr, campaign_mgr, new_pledge_mgr, \
    pledge_helper, pledge_mgr, user_mgr, vat_mgr
from patreon.services.db import db_session

"""
pledge_mgr handles creating, deleting, and updating pledges
using both the old and new pledge tables.
"""


@pledge_helper.to_legacy_and_new_pledge_tuple
def create_or_update_pledge(patron_id, amount, pledge_cap, reward_id, card_id, campaign_id=None, creator_id=None,
                            email_new_comment=1, notify_all_activity=1, notify_paid_creation=1, address=None,
                            created_at=None, vat_country=None, ip_address=None, patron_pays_pledge_fees=0):
    # Convert campaign_id to creator_id, or vice versa
    campaign_id, creator_id = campaign_mgr.get_campaign_and_creator_ids(campaign_id, creator_id)

    created_at = created_at or datetime.datetime.now()

    # Check if the user is effected by vat and if so, get their vat_location_id
    pledge_vat_location_id = pledge_helper.get_pledge_vat_location_id(ip_address, vat_country)
    vat_mgr.check_vat_country_matches_geoip(vat_country, ip_address)

    # Check that the pledge_cap is a multiple of the pledge.
    pledge_helper.check_pledge_cap_amount_is_multiple_of_pledge(amount, pledge_cap)

    # Check that the reward_tier is correct.
    existing_pledge = new_pledge_mgr.get_pledge_by_patron_and_campaign(
        patron_id=patron_id, campaign_id=campaign_id
    )
    current_reward_tier = None
    if existing_pledge:
        current_reward_tier = existing_pledge.reward_tier_id
    pledge_helper.check_reward_tier(patron_id, amount, reward_id, current_reward_tier)

    # Check that user is not using paypal if content is NSFW
    pledge_helper.check_nsfw_does_not_use_paypal(campaign_id, card_id)

    address_id = None
    if address:
        patron = user_mgr.get(patron_id)

        address_id = address_mgr.create_address(
            patron_id,
            patron.full_name,
            address,
            name=address.street
        )

    with db_session.begin_nested():
        # Put the pledge into the old pledge tables
        if creator_id:
            pledge_mgr.create_or_update_pledge(
                patron_id=patron_id,
                creator_id=creator_id,
                amount=amount,
                pledge_cap=pledge_cap,
                reward_id=reward_id,
                card_id=card_id,
                notify_all_activity=notify_all_activity,
                notify_paid_creation=notify_paid_creation,
                address=address,
                created_at=created_at,
                email_new_comment=email_new_comment,
                patron_pays_fees=patron_pays_pledge_fees,
                address_id=address_id
            )

        # Put the pledge into the new tables
        if campaign_id:
            # Make/update pledge in new and old tables
            new_pledge_mgr.make_or_update_pledge(
                user_id=patron_id,
                campaign_id=campaign_id,
                amount_cents=amount,
                pledge_cap_amount_cents=pledge_cap,
                reward_tier_id=reward_id,
                pledge_vat_location_id=pledge_vat_location_id,
                created_at=created_at,
                patron_pays_fees=patron_pays_pledge_fees,
                address_id=address_id
            )

    # Get the pledge
    new_pledge = new_pledge_mgr.get_pledge_by_patron_and_campaign(patron_id, campaign_id)
    old_pledge = pledge_mgr.get_pledge(patron_id, creator_id)
    return old_pledge, new_pledge


@pledge_helper.to_legacy_and_new_pledge_tuple
def delete_pledge(patron_id, campaign_id=None, creator_id=None):
    if not campaign_id:
        campaign_id, creator_id = campaign_mgr.get_campaign_and_creator_ids(campaign_id, creator_id)

    with db_session.begin_nested():
        new_pledge = new_pledge_mgr.delete_pledge(patron_id, campaign_id)
        old_pledge = pledge_mgr.delete_pledge(patron_id, campaign_id)

    # Delete pledge from new table and return
    return old_pledge, new_pledge


@pledge_helper.to_legacy_and_new_pledge_tuple
def get_pledge_by_patron_and_campaign_or_creator(patron_id, campaign_id=None, creator_id=None):
    campaign_id, creator_id = campaign_mgr.get_campaign_and_creator_ids(campaign_id, creator_id)

    old_pledge = None
    new_pledge = None

    if creator_id:
        old_pledge = pledge_mgr.get(patron_id, creator_id)

    if campaign_id:
        new_pledge = new_pledge_mgr.get_pledge_by_patron_and_campaign(patron_id, campaign_id)

    return old_pledge, new_pledge
