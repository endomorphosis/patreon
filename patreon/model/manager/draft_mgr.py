import patreon
from patreon.exception.post_errors import PostNotFound
from patreon.model.manager import post_mgr, campaign_mgr
from patreon.model.table import Activity
from patreon.model.type import PostType


def get_draft_or_throw(post_id):
    draft = get_draft(post_id)
    if draft is None:
        raise PostNotFound(post_id)
    return draft


def get_draft(activity_id):
    return Activity.query\
        .filter(Activity.activity_id == activity_id)\
        .filter(Activity.published_at.is_(None))\
        .filter(Activity.deleted_at.is_(None))\
        .first()


def get_drafts(activity_ids):
    return Activity.query\
        .filter(Activity.activity_id.in_(activity_ids))\
        .filter(Activity.published_at.is_(None))\
        .filter(Activity.deleted_at.is_(None))\
        .all()


def get_drafts_by_owner_id(owner_id):
    return Activity.query\
        .filter(Activity.user_id == owner_id)\
        .filter(Activity.published_at.is_(None))\
        .filter(Activity.deleted_at.is_(None))\
        .all()


def get_non_blank_drafts_by_campaign_id(campaign_id):
    return Activity.query \
        .filter(Activity.campaign_id == campaign_id) \
        .filter(Activity.published_at.is_(None)) \
        .filter(Activity.deleted_at.is_(None)) \
        .filter(~Activity.is_blank_draft) \
        .all()


def get_new_draft(campaign_id, user_id):
    last_post = post_mgr.get_last_published_post(campaign_id)

    category = None
    if last_post:
        category = last_post.category
    else:
        campaign = campaign_mgr.get_campaign_or_throw(campaign_id)
        if campaign.categories:
            category = campaign.categories[0]

    created_at = patreon.util.unsorted.now()
    activity_id = Activity.insert(dict(
        campaign_id=campaign_id,
        user_id=user_id,
        created_at=created_at,
        edited_at=created_at,
        activity_type=PostType.TEXT_ONLY.value,
        post_type=PostType.TEXT_ONLY.name.lower(),
        category=category,
        is_creation=True
    ))
    return get_draft(activity_id)


def get_last_empty_draft(campaign_id):
    return Activity.query\
        .filter(Activity.campaign_id == campaign_id)\
        .filter(Activity.is_blank_draft)\
        .order_by(Activity.created_at)\
        .first()
