from patreon.model.table import RewardsWait

def get(user_id, reward_id):
    return RewardsWait.get(user_id=user_id, reward_id=reward_id)


def delete(user_id, reward_id):
    obj = get(user_id, reward_id)
    if not obj:
        return
    obj.delete()


def add(user_id, reward_id):
    return RewardsWait.insert_ignore({
        'UID': user_id,
        'RID': reward_id
    })
