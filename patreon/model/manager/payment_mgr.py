import datetime

from patreon.constants import TxnTypes
from patreon.exception.db_errors import BadEnumType
from patreon.exception.payment_errors import InvalidCardToken, CardNotFound, \
    NotCardOwner, PendingPaypalTransaction, TransactionTypeUnhandled, \
    TransactionNotExist, TransactionTypeDoesntExist, BigMoneyChargeError
from patreon.model.dbm import rewards_give_dbm, payout_dbm
from patreon.model.manager import card_mgr, payment_event_mgr, credit_purchase_mgr, ledger_entry_mgr, \
    credit_adjustment_mgr
from patreon.model.manager.payment_event_mgr import capture_exception_payment_events, add_exception_event
from patreon.model.table import Payout, Txn, ChargesSetup
from patreon.services.db import db_session
from patreon.services.payment_apis import PayPalAPI, StripeAPI
from sqlalchemy import func


@capture_exception_payment_events
def payment_api(card):
    if card.is_paypal:
        return PayPalAPI()
    elif card.is_stripe:
        return StripeAPI()

    raise InvalidCardToken


def get_credit_total_legacy(user_id, for_creator_to_creator=False):
    pledge_sums = rewards_give_dbm.find_pledge_sums(user_id)
    payout_sums = payout_dbm.find_payout_sum(user_id)
    credit_sums = payout_dbm.find_credit_sums(user_id)

    credits_ = 0
    payments = 0
    credits_earned = 0
    credit_used = 0
    if pledge_sums:
        credits_ = (pledge_sums.Pledge or 0) - (pledge_sums.Fee or 0) - (pledge_sums.PatreonFee or 0)

    if credit_sums:
        credits_earned = (credit_sums.Credit or 0)  # this is actually negative when there are credits earned
        credit_used = (credit_sums.CreditUsed or 0)
        payments -= (credit_used or 0)  # credit used already shouldn't count against any payouts

    if payout_sums:
        payments += (payout_sums.Payments or 0)
        if for_creator_to_creator:
            # basically since payments is subtracted from credits for total credit amount,
            # we want to add back in credit outstanding (which would be negative)
            payments += (credits_earned + credit_used)
    return (credits_ or 0) - (payments or 0)


def get_credit_total(user_id):
    return ledger_entry_mgr.get_credit_total_for_user(user_id)


@capture_exception_payment_events
def add_credit_legacy(user_id, amount_cents, external_id, external_fee):
    Payout.insert({
        'UID': user_id,
        'Amount': -1 * amount_cents,
        'Created': datetime.datetime.now(),
        'Type': 10,
        # Magic number for "payout credit" should be extracted to a constant, but this function is hopefully short-lived
        'Fee': external_fee or 0,
        'TransID': external_id
    })


@capture_exception_payment_events
def charge_big_money(user_id, payment_instrument_token, amount_cents):
    try:
        card = card_mgr.get(payment_instrument_token)
        if not card:
            raise CardNotFound

        if card.user_id != user_id:
            raise NotCardOwner

        if card.is_verified:
            return False

        try:
            result = card_mgr.get_payment_api_for_card_token(payment_instrument_token).charge_instrument(
                None,
                payment_instrument_token,
                amount_cents,
                description="Pre-charge for Big Money"
            )
            external_id = result.transaction_id
            external_fee = result.fee_amount_cents
        except PendingPaypalTransaction as e:
            add_exception_event(e)
            external_fee = 0
        except Exception as e:
            raise BigMoneyChargeError(e)

        add_credit_legacy(user_id, amount_cents, external_id, external_fee)
        card_mgr.verify_card(card)
        payment_event_mgr.add_big_money_event(user_id, amount_cents, is_successful=True)

        return True
    except Exception as exception:
        payment_event_mgr.add_big_money_event(user_id, amount_cents, is_successful=False)
        raise exception


def get_all_payouts_for_creator(user_id):
    result = payout_dbm.get_all_payouts_for_creator(user_id)

    # Organize results in list
    transactions = []
    for row in result:
        transactions.append({"Datetime": row[0],
                             "Amount": row[1],
                             "Pledge": 0,
                             "Fee": 0,
                             "PatreonFee": 0,
                             "RewardStatus": 0,
                             "Type": row[3]})

    return transactions


@capture_exception_payment_events
def create_and_process_transaction_and_obligation(transaction_type, *args, **kwargs):
    with db_session.begin_nested():
        if transaction_type == TxnTypes.credit_purchase:
            transaction_id = credit_purchase_mgr.initialize(*args, **kwargs)
        elif transaction_type == TxnTypes.credit_adjustment:
            transaction_id = credit_adjustment_mgr.initialize(*args, **kwargs)
        else:
            raise TransactionTypeUnhandled(transaction_type)
    # This commits
    #    1. New transaction
    #    2. New obligation
    db_session.commit()
    return process_transaction(transaction_id)


@capture_exception_payment_events
def process_transaction(transaction_id):
    try:
        transaction = Txn.get(transaction_id)
    except BadEnumType as e:
        raise TransactionTypeDoesntExist(e.value)
    if not transaction:
        raise TransactionNotExist
    if transaction.type == TxnTypes.credit_purchase:
        return credit_purchase_mgr.process_transaction(transaction_id)
    elif transaction.type == TxnTypes.credit_adjustment:
        return credit_adjustment_mgr.process_transaction(transaction_id)

    raise TransactionTypeUnhandled(transaction.type)


def add_credit(user_id, credit_amount):
    return create_and_process_transaction_and_obligation(TxnTypes.credit_adjustment,
                                                         user_id=user_id,
                                                         amount_cents=credit_amount)


def remove_credit(user_id, credit_amount):
    credit_amount = -1 * min(credit_amount, get_credit_total(user_id))
    return create_and_process_transaction_and_obligation(TxnTypes.credit_adjustment,
                                                         user_id=user_id,
                                                         amount_cents=credit_amount)


def get_charges_setup_stats():
    return ChargesSetup.query.with_entities(func.sum(ChargesSetup.Amount).label('sum'),
                                            func.count().label('count')).first()
