import geoip2.errors
from patreon.constants.countries import countries
from patreon.constants.vat_rates import STANDARD_VAT_RATES_IN_BPS
from patreon.exception.pledge_errors import VatCountryMismatch
from patreon.model.table import PledgeVatLocation
from patreon.services.geoip import get_reader
from patreon.util import datetime


def vat_rules_version():
    # http://ec.europa.eu/taxation_customs/resources/documents/taxation/vat/how_vat_works/rates/vat_rates_en.pdf
    return "2015-01-01"


def standard_vat_rate_in_bps_for_country_code(country_code):
    return STANDARD_VAT_RATES_IN_BPS.get(country_code)


def calculate_vat_on_amount_for_country(amount_cents, country_code):
    vat_rate_in_basis_points = standard_vat_rate_in_bps_for_country_code(country_code)
    if not vat_rate_in_basis_points:
        return 0

    # This math is a little awkward, trying to avoid floating points.
    # The rate is in basis points, i.e. increments of 0.01 percent.
    # so, analogous to
    #   tax = amount * (tax_percent / 100)
    # when using basis points you use ten-thousand instead of one-hundred:
    #   tax = amount * (tax_basis_points / 10,000)
    # division is done *after* the multiplication so we can use integers.
    # also, we want to round to the nearest cent - so we check for whether our fractional part
    # is greater than or equal to a half-cent
    # but we do that in integer-land by checking that the remainder is more then 5,000
    # (because 5,000 over 10,000 equals one-half)
    value = amount_cents * vat_rate_in_basis_points
    vat_cents = value // 10000
    if value % 10000 >= 5000:
        vat_cents += 1
    return vat_cents


def check_vat_country_matches_geoip(vat_country, ip_address):
    geoip_country = get_country_by_ip_address(ip_address)
    if standard_vat_rate_in_bps_for_country_code(geoip_country):
        # it's in Europe, so we're required to match
        if geoip_country != vat_country:
            raise VatCountryMismatch()
    return geoip_country


def get_country_by_ip_address(ip_address):
    try:
        response = get_reader().country(ip_address)
        return response.country.iso_code
    except geoip2.errors.AddressNotFoundError:
        return "??"
    except ValueError:
        return None


def get_friendly_name_by_country_code(iso_name):
    return countries.get(iso_name)


def create_pledge_vat_location(ip_address, geolocation_country, selected_country):
    # returns the id of the new record
    return PledgeVatLocation.insert({
        "ip_address": ip_address,
        "geolocation_country": geolocation_country,
        "selected_country": selected_country,
        "created_at": datetime.datetime.now()
    })


def vat_countries():
    return {
        country_code: get_friendly_name_by_country_code(country_code)
        for country_code in STANDARD_VAT_RATES_IN_BPS.keys()
    }
