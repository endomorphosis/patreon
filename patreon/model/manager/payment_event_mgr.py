from functools import wraps
from patreon.model.dbm.payment_event_dbm import PaymentEventDBM
from patreon.model.table import PaymentEvent
from patreon.util import datetime


def _get_payment_events_after_time(time):
    return PaymentEventDBM().occured_on_or_after(time).order_by_created()


def get_recent_events(limit=100):
    return PaymentEventDBM().order_by_created().limit(limit).all()


def get_all_events_after_time(time):
    return _get_payment_events_after_time(time).all()


def get_events_of_type_after_time(time, type):
    return _get_payment_events_after_time(time).is_type(type).all()


def _exception_to_dictionary(exception):
    if hasattr(exception, 'to_dict'):
        return exception.to_dict()
    else:
        return {}


def _recursive_exception_to_json(exception):
    data = {
        'type': exception.__class__.__name__,
        'description': str(exception)
    }

    data.update(_exception_to_dictionary(exception))

    if hasattr(exception, 'original_exception') and exception.original_exception:
        data['original_exception'] = _recursive_exception_to_json(exception.original_exception)

    return data


def add_exception_event(exception):
    data = _recursive_exception_to_json(exception)

    return add_payment_event('exception', exception.__class__.__name__, data)


def add_big_money_event(user_id, amount_cents, is_successful):
    status = 'failed'

    if is_successful:
        status = 'succeeded'

    data = {
        'user_id': user_id,
        'amount_cents': amount_cents,
        'status': status
    }

    return add_payment_event('payment', 'big_money', data)


def add_payment_event(type, subtype, data, created_at=None):
    if not created_at:
        created_at = datetime.datetime.now()

    # Insert to redshift

    return PaymentEvent.insert({
        'type': type,
        'subtype': subtype,
        'json_data': data,
        'created_at': created_at
    })


def get_user_events(user_id):
    return PaymentEventDBM().order_by_created().is_user_id(user_id).all()


def add_pledge_event(user_id, campaign_id, amount_cents, pledge_cap, reward_id, vat_country, vat_amount, action):
    data = {
        'user_id': user_id,
        'campaign_id': campaign_id,
        'amount_cents': amount_cents,
        'pledge_cap': pledge_cap,
        'reward_id': reward_id,
        'vat_country': vat_country,
        'vat_amount': vat_amount,
        'action': action
    }

    subtype = "vanilla"
    if vat_country:
        subtype = "vat"

    return add_payment_event('pledge', subtype, data)


def capture_exception_payment_events(inner_f):
    @wraps(inner_f)
    def capture_exception(*args, **kwargs):
        try:
            return inner_f(*args, **kwargs)
        except Exception as e:
            add_exception_event(e)
            raise e

    return capture_exception


def get_filtered_events(type=None, subtype=None, earliest_time=None, latest_time=None, limit=100, cursor=None):
    dbm = PaymentEventDBM()

    if type:
        dbm.is_type(type)
    if subtype:
        dbm.is_subtype(subtype)
    if earliest_time:
        dbm.occurred_after(earliest_time)
    if latest_time:
        dbm.occurred_on_or_before(latest_time)

    if cursor:
        dbm.occurred_before_event_id(cursor)

    if limit:
        return dbm.order_by_created_then_id().limit(limit).all()

    return dbm.order_by_created_then_id().all()
