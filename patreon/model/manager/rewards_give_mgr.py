from patreon import globalvars
import patreon
from patreon.model.dbm import rewards_give_dbm
from patreon.model.manager import post_mgr
from patreon.model.table import RewardsGive, User, CampaignsUsers, Campaign
from patreon.services.caching import memcached
from patreon.util import time_math
from patreon.util.datetime import timezone_aware_sql_date_func, offset_from_time
import pytz
from sqlalchemy import func


def get_bills_for_post(post_id):
    return rewards_give_dbm.get_bills_for_post(post_id)


def create_bill(creator_id, post_id):
    return rewards_give_dbm.create_bill(creator_id, post_id)


@patreon.services.caching.request_cached()
def find_supported_posts_by_user_id_paginated(user_id, page=1):
    post_ids = rewards_give_dbm.find_supported_creation_ids_by_user_id_paginated(user_id, page)
    return post_mgr.get_posts(post_ids)


@patreon.services.caching.request_cached()
def pledge_sums_by_creator_id_and_user_id(creator_id, user_id):
    return rewards_give_dbm.pledge_sums_by_creator_id_and_user_id(creator_id=creator_id, user_id=user_id)


@patreon.services.caching.request_cached()
def delete_by_user_id_creator_id(user_id, creator_id):
    return rewards_give_dbm.delete_by_user_id_creator_id(user_id, creator_id)


@patreon.services.caching.request_cached()
def delete_by_post_id(post_id):
    return rewards_give_dbm.delete_by_post_id(post_id)


@patreon.services.caching.request_cached()
def find_pledge_sums_for_creator_by_user_id_and_date(user_id):
    return rewards_give_dbm.find_pledge_sums_for_creator_by_user_id_and_date(user_id)


@patreon.services.caching.request_cached()
def find_pledge_sum_by_creator_for_year(creator_id):
    return rewards_give_dbm.pledge_sums_by_creator_id_for_year(creator_id)


@patreon.services.caching.request_cached()
def get_lifetime_by_creator_id(creator_id):
    return rewards_give_dbm.get_lifetime_by_creator_id(creator_id)


@patreon.services.caching.request_cached()
def find_pledge_sums_for_creator_by_user_id(user_id):
    return rewards_give_dbm.find_pledge_sums_for_creator_by_user_id(user_id)


@patreon.services.caching.request_cached()
def find_sum_patron_count_all_by_activity_id(activity_id):
    return rewards_give_dbm.find_sum_patron_count_all_by_activity_id(activity_id)


@patreon.services.caching.request_cached()
def find_sum_patron_count_success_by_activity_id(activity_id):
    return rewards_give_dbm.find_sum_patron_count_success_by_activity_id(activity_id)


@patreon.services.caching.request_cached()
def find_pending_and_failed_creators_by_user_id(user_id):
    return rewards_give_dbm.find_pending_and_failed_creators_by_user_id(user_id)


@patreon.services.caching.request_cached()
def find_processed_creators_by_user_id(creator_id):
    return rewards_give_dbm.find_processed_creators_by_user_id(creator_id)


@patreon.services.caching.request_cached()
def find_processed_by_creator_id_user_id(creator_id, user_id):
    return rewards_give_dbm.find_processed_by_creator_id_user_id(creator_id, user_id)


@patreon.services.caching.request_cached()
def find_failed_and_pending_by_creator_id_user_id(creator_id, user_id):
    return rewards_give_dbm.find_failed_and_pending_by_creator_id_user_id(creator_id, user_id)


@patreon.services.caching.request_cached()
def pledge_sums_for_users_by_creator_id(creator_id):
    return rewards_give_dbm.pledge_sums_for_users_by_creator_id(creator_id)


@patreon.services.caching.request_cached()
def pledge_sums_by_user_id(user_id):
    return rewards_give_dbm.pledge_sums_by_user_id(user_id)


def find_processed_reward_gives_from_active_creators_for_patron(patron_id):
    return RewardsGive.query \
        .filter_by(UID=patron_id, Status=globalvars.get('REWARD_PROCESSED')) \
        .join(RewardsGive.creator) \
        .join(User.campaigns_users) \
        .join(CampaignsUsers.campaign) \
        .filter(Campaign.published_at.isnot(None)) \
        .order_by(RewardsGive.Created.desc()) \
        .all()


def any_pending_or_failed_payments_for_creator_in_month_exist(creator_id, date_in_month):
    row = RewardsGive.query \
        .filter_by(CID=creator_id) \
        .filter(RewardsGive.status.in_([
        globalvars.get('REWARD_PENDING'),
        globalvars.get('REWARD_DECLINED')
    ])) \
        .filter(time_math.column_during_month_for_date(RewardsGive.created_at, date_in_month)) \
        .first()

    return not not row


def any_pending_payments_for_patron_in_month_exist(patron_id, date_in_month):
    row = RewardsGive.query \
        .filter_by(UID=patron_id) \
        .filter(RewardsGive.status == globalvars.get('REWARD_PENDING')) \
        .filter(time_math.column_during_month_for_date(RewardsGive.created_at, date_in_month)) \
        .first()

    return not not row


@patreon.services.caching.request_cached()
def query_by_patron_and_creator(patron_id, creator_id):
    return rewards_give_dbm.query_by_patron_and_creator(patron_id, creator_id)


@patreon.services.caching.request_cached()
def get_most_recent_invoice_for_patron_and_creator(patron_id, creator_id):
    return rewards_give_dbm.get_most_recent_invoice_for_patron_and_creator(patron_id, creator_id)

def get_entry_type_between_dates(status, start, end):
    base_query = RewardsGive.query
    if status is not None:
        base_query = base_query.filter(RewardsGive.Status == status)
    else:
        # Refunded/Fraud
        base_query = base_query.filter(RewardsGive.status != 10)

    created_with_timezone = timezone_aware_sql_date_func(RewardsGive.Created, pytz.timezone('US/Pacific'))

    base_query = base_query.filter(created_with_timezone >= start)
    base_query = base_query.filter(created_with_timezone <= end)

    base_query = base_query.with_entities(func.count().label('count'), func.sum(RewardsGive.Pledge).label('sum'))

    return base_query.first()


def pending_pledge_sum_for_creator_and_user(creator_id, user_id):
    pending = rewards_give_dbm.find_failed_and_pending_by_creator_id_user_id(creator_id, user_id)
    return sum(pledge.Pledge for pledge in pending)


def get_transactions_for_creator(creator_id):
    result = rewards_give_dbm.get_transactions_for_creator(creator_id)

    # Organize transactions in dictionary
    transactions = []
    for row in result:
        transactions.append({"Datetime": row.Created,
                             "Amount": row.Pledge - row.Fee - row.PatreonFee,
                             "Pledge": row.Pledge,
                             "Fee": row.Fee,
                             "PatreonFee": row.PatreonFee,
                             "RewardStatus": row.Status,
                             "Type": "Owed"})

    return transactions


def get_transactions_for_creator_totalled_by_month(creator_id):
    all_transactions = get_transactions_for_creator(creator_id)

    if (all_transactions is None) or (len(all_transactions) == 0):
        return all_transactions

    # Start the summed transactions list with the first transaction
    summed_transactions = [all_transactions[0]]

    for transaction in all_transactions[1:]:
        # Check if the next transaction is in the same month. If so, sum them
        if (transaction["Datetime"].month == summed_transactions[-1]["Datetime"].month) \
                and \
                        transaction["Datetime"].year == summed_transactions[-1]["Datetime"].year:
            summed_transactions[-1]["Amount"] += transaction["Amount"]
            summed_transactions[-1]["Pledge"] += transaction["Pledge"]
            summed_transactions[-1]["Fee"] += transaction["Fee"]
            summed_transactions[-1]["PatreonFee"] += transaction["PatreonFee"]
        else:
            # If they're in different months
            # Correct the last entries date to the first of month
            summed_transactions[-1]["Datetime"] = time_math.first_of_month_following_date(
                summed_transactions[-1]["Datetime"])

            # Add the new entry to the end
            summed_transactions.append(transaction)

    return summed_transactions
