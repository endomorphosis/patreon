import patreon


def _takedown_key(user_id):
    return 'dmca_takedown:{}'.format(user_id)


def is_user_suspended(user_id):
    return patreon.pstore.get(_takedown_key(user_id))


def set_user_as_suspended(user_id):
    patreon.pstore.set(_takedown_key(user_id), True)


def set_user_as_unsuspended(user_id):
    patreon.pstore.set(_takedown_key(user_id), False)


def _takedown_url_key(user_id):
    return 'dmca_takedown_url:{}'.format(user_id)


def set_takedown_url(user_id, notice_url):
    patreon.pstore.set(_takedown_url_key(user_id), notice_url)


def get_takedown_url(user_id):
    return patreon.pstore.get(_takedown_url_key(user_id))
