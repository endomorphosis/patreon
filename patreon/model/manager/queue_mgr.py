import json

import patreon
from patreon.services.aws import sqs


def enqueue_post_for_update(post_id):
    data = {
        'campaign_id': str(post_id)
    }
    sqs.put('prodweb-post_search_update_queue', json.dumps(data))


def enqueue_campaign_for_update(campaign_id):
    data = {
        'campaign_id': str(campaign_id)
    }
    sqs.put('prodweb-zoho_update', json.dumps(data))
    sqs.put('prodweb-campaign_search_update_queue', json.dumps(data))
