from patreon.constants import mc_key
from patreon.model.dbm import message_dbm
from patreon.model.table import Message, UserExtra
from patreon.model.manager import user_mgr
from patreon.services.db import db_session
from patreon.services.mail import send_template_email
import patreon.services.mail.templates as templates


def get_new_message_count_by_user_id(sender_id):
    count = message_dbm.get_new_message_count_by_sender_id(sender_id)
    if not count:
        return 0
    return count.count


def get_conversation(user_id1, user_id2):
    return message_dbm.get_conversation(user_id1, user_id2).order_by(Message.sent_at.desc())


def get_messages_and_clear_unread(recipient_id):
    clear_unread(recipient_id)
    return message_dbm.find_by_recipient_id(recipient_id).order_by(Message.sent_at.desc()).all()


def get_unread(user_id):
    reference = UserExtra.get(user_id=user_id)
    if reference:
        return reference.NewMsgs
    return 0


def clear_unread(user_id):
    reference = UserExtra.get(user_id=user_id)
    if not reference:
        raise Exception('No userextra reference')

    reference.update({
        'NewMsgs': 0
    })


def add_message(sender_id, recipient_id, message):
    if sender_id is None or recipient_id is None or sender_id == recipient_id:
        return

    message_dbm.add_message(sender_id, recipient_id, message)
    message_dbm.increment_unread_count(recipient_id)


def process_message(sender_id, recipient_ids, message):
    sender = user_mgr.get(sender_id)
    recipients = user_mgr.get_users(recipient_ids)

    with db_session.begin_nested():
        for recipient in recipients:
            add_message(sender_id, recipient.user_id, message)

    send_template_email(templates.Message(sender, message, recipients))
