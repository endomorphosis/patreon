import datetime
import patreon
from patreon.model.table import ShippingAddress


def find_address(address_id):
    return ShippingAddress.query \
        .filter_by(address_id=address_id)


@patreon.services.caching.request_cached()
def get_address(address_id):
    if address_id is not None:
        return find_address(address_id).first()
    return None


@patreon.services.caching.multiget_cached(
    object_key='user_id', default_result=list)
def get_addresses_by_user_id(user_ids):
    return ShippingAddress.query \
        .filter(ShippingAddress.user_id.in_(user_ids)) \
        .filter(ShippingAddress.deleted_at.is_(None)) \
        .all()


def create_address(user_id, addressee, address, name=None):
    return ShippingAddress.insert({
        'user_id': user_id,
        'name': name,
        'addressee': addressee,
        'line_1': address.address_line_1,
        'line_2': address.address_line_2,
        'postal_code': address.address_zip,
        'city': address.city,
        'state': address.state,
        'country': address.country,
        'created_at': datetime.datetime.now()
    })[0]


def delete_address(address_id):
    find_address(address_id).update({
        'deleted_at': datetime.datetime.now()
    })
