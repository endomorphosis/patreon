from patreon.exception import ParameterInvalid
from sqlalchemy import asc, desc


def parse_sort_string(sort_string):
    """Returns a list of (string, boolean) tuples that represent
    the parsed version of the sort_string as passed in through the
    request args. The string represents the sort string and the
    boolean represents whether the intent of the sort is to be
    ascending or descending.

    See: http://jsonapi.org/format/#fetching-sorting
    """
    sort_components = sort_string.split(',')
    sort_tuples = []
    for sort in sort_components:
        if sort and sort[0] == '-':
            sort_tuples.append((sort[1:], False))
        else:
            sort_tuples.append((sort, True))
    return sort_tuples


def sort_order_tuples(sort_string, sort_map, default):
    """Returns a list of (SQLAlchemy column, boolean) sort parameters
    based on the sort_string as passed in through the request args and
    the sort_map. The column represents the column that should be sorted
    on and the boolean represents if the sort order is ascending.

    sort_string: comma-separated list of strings that match JSONAPI format
    sort_map: map of strings -> columns to map the sort_string onto
    default: a string that represents the default for sort_string if it does not exist
    """
    if sort_string is None:
        return [(sort_map[default], True)]
    sort_tuples = []
    for sort, ascending in parse_sort_string(sort_string):
        if sort not in sort_map:
            raise ParameterInvalid('sort', sort)
        column = sort_map[sort]
        sort_tuples.append((column, ascending))
    return sort_tuples


def sort_order_from_sort_string(sort_string, sort_map, default):
    sort_tuples = sort_order_tuples(sort_string, sort_map, default)
    return [
        asc(column) if ascending else desc(column)
        for column, ascending in sort_tuples
        ]
