from patreon.exception.payment_errors import *
from patreon.services.aws import SafeSQS


class PaymentQueueObject:
    queue_name = None

    def __init__(self):
        self.queue = SafeSQS(self.queue_name)

    def __enter__(self):
        return self.queue.get()

    def __exit__(self, exc_type, exc_val, exc_tb):
        if exc_type is None:
            self.queue.delete_message()
            return True

        if issubclass(exc_type, RecoverablePaymentFailure):
            self.queue.requeue()
            self.queue.delete_message()
            return True

        if issubclass(exc_type, ExternalPaymentAPIException):
            # these should be handled by the calling method
            self.queue.delete_message()
            return True

        return False
