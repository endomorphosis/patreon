import urllib.parse
import patreon
import dateutil.parser

from patreon import util
from patreon.model.manager import unread_status_mgr, feature_flag
from patreon.model.table import Comment
from patreon.model.type import UnreadStatusType, NotificationSortOrder


class DailyCommentsNotification(object):
    def __init__(self, current_user, date_, timezone, per_page, cursor):
        self.current_user = current_user
        self.date = date_
        self.timezone = timezone
        self.per_page = per_page
        self.cursor = cursor
        self._comments = None

    # TODO: pull into a mgr or dbm
    @classmethod
    def find_by_id(cls, resource_id, current_user, timezone, per_page=5, cursor=None):
        card_type, user_id_string, subtype, date_string = resource_id.split("+")
        if card_type != "comments":
            raise ValueError("unknown notification type " + card_type)

        class_by_name = {
            "creators": DailyCommentsFromMyCreatorsNotification,
            "patrons": DailyCommentsFromMyPatronsNotification,
            "others": DailyCommentsFromOtherUsersNotification,
        }

        cls = class_by_name[subtype]

        date_ = dateutil.parser.parse(date_string).date()

        if str(current_user.user_id) != user_id_string:
            raise ValueError("User id mismatch")

        return cls(current_user, date_, timezone, per_page, cursor)

    def resource_id(self):
        parts = [
            "comments",
            str(self.current_user.user_id),
            self.comment_notification_subtype(),
            self.date.isoformat()
        ]
        return "+".join(parts)

    def notification_type(self):
        return 'comment'

    @staticmethod
    def posts_comments_filter(user_id):
        raise NotImplemented("Override this in subclass")

    @staticmethod
    def replies_comments_filter(user_id):
        raise NotImplemented("Override this in subclass")

    def comment_notification_subtype(self):
        raise NotImplemented("Override this in subclass")

    @staticmethod
    def dates_matching_filter(user_id, posts_comments_filter, replies_comments_filter, count, tz):
        posts_dates = Comment.find_dates_with_comments_on_posts_by_user_id_matching_filter(
            user_id, posts_comments_filter, count, tz
        )
        replies_dates = []
        if feature_flag.is_enabled('comment_reply_notifications', user_id=user_id):
            replies_dates = Comment.find_dates_with_replies_to_comments_by_user_id_matching_filter(
                user_id, replies_comments_filter, count, tz
            )
        return sorted(set(posts_dates + replies_dates), reverse=True)[:count]

    def posts_comments_filter_for_date(self):
        return self.posts_comments_filter(self.current_user.user_id) \
               & Comment.was_posted_on_date(self.date, self.timezone)

    def replies_comments_filter_for_date(self):
        return self.replies_comments_filter(self.current_user.user_id) \
               & Comment.was_posted_on_date(self.date, self.timezone)

    def comments_with_one_extra(self):
        if not self._comments:
            posts_comments_filter = self.posts_comments_filter_for_date()
            if self.cursor:
                newest_comment_id = int(self.cursor)
                posts_comments_filter &= Comment.comment_id <= newest_comment_id
            post_comments = Comment.find_on_posts_by_user_id_with_filter(
                self.current_user.user_id, posts_comments_filter, self.per_page + 1
            )

            reply_comments = []
            if feature_flag.is_enabled('comment_reply_notifications', user_id=self.current_user.user_id):
                replies_comments_filter = self.replies_comments_filter_for_date()
                if self.cursor:
                    newest_comment_id = int(self.cursor)
                    replies_comments_filter &= Comment.comment_id <= newest_comment_id
                reply_comments = Comment.find_on_comments_by_user_id_with_filter(
                    self.current_user.user_id, replies_comments_filter, self.per_page + 1
                )

            self._comments = sorted(post_comments + reply_comments,
                                    key=lambda comment: comment.created_at, reverse=True)[:self.per_page + 1]
        return self._comments

    def comments(self):
        return self.comments_with_one_extra()[:self.per_page]

    def date_for_json(self):
        return util.datetime.make_naive(self.start_time())

    def start_time(self):
        return util.datetime.datetime_at_midnight_on_date(self.date, self.timezone)

    def next_cursor(self):
        comments = self.comments_with_one_extra()
        if len(comments) <= self.per_page:
            return None
        return str(comments[-1].comment_id)

    def next_page(self):
        cursor = self.next_cursor()
        if not cursor:
            return None
        url = "https://{}/notifications/".format(patreon.config.api_server)
        url += self.resource_id()
        url += "/links/comments"
        url += "?page[count]=" + str(self.per_page)
        url += "&page[cursor]=" + urllib.parse.quote(self.next_cursor())
        return url

    def total_count(self):
        """This number appears in the UI as "You have N total comments" """
        posts_count = Comment.count_on_posts_by_user_id_with_filter(
            self.current_user.user_id, self.posts_comments_filter_for_date()
        )
        replies_count = 0
        if feature_flag.is_enabled('comment_reply_notifications', user_id=self.current_user.user_id):
            replies_count = Comment.count_on_comments_by_user_id_with_filter(
                self.current_user.user_id, self.replies_comments_filter_for_date()
            )
        return posts_count + replies_count

    def unread_count(self):
        """This number appears in the UI as "You have N unread comments" """
        unread_time = unread_status_mgr.latest_read_time(self.current_user.user_id, UnreadStatusType.notifications)
        unread_on_posts_filter = self.posts_comments_filter_for_date()
        if unread_time:
            unread_on_posts_filter &= Comment.created_at >= unread_time
        posts_count = Comment.count_on_posts_by_user_id_with_filter(
            self.current_user.user_id, unread_on_posts_filter
        )
        replies_count = 0
        if feature_flag.is_enabled('comment_reply_notifications', user_id=self.current_user.user_id):
            unread_on_comments_filter = self.posts_comments_filter_for_date()
            if unread_time:
                unread_on_comments_filter &= Comment.created_at >= unread_time
            replies_count = Comment.count_on_comments_by_user_id_with_filter(
                self.current_user.user_id, unread_on_comments_filter
            )
        return posts_count + replies_count

    def from_who_description(self):
        """This phrase optionally appears in the UI as "You have N total comments from [some type of user]" """
        return None


class DailyCommentsFromMyCreatorsNotification(DailyCommentsNotification):
    """Comments on my posts from anyone I support"""

    def comment_notification_subtype(self):
        return "creators"

    def from_who_description(self):
        return "your creators"

    @staticmethod
    def posts_comments_filter(user_id):
        return Comment.on_posts_by_user_id_from_their_creators(user_id)

    @staticmethod
    def replies_comments_filter(user_id):
        return Comment.on_comments_by_user_id_from_their_creators(user_id)

    def sort_key(self):
        return self.date_for_json(), NotificationSortOrder.DailyCommentsFromMyCreatorsNotification


class DailyCommentsFromMyPatronsNotification(DailyCommentsNotification):
    """Comments on my posts from anyone who supports me"""

    def comment_notification_subtype(self):
        return "patrons"

    def from_who_description(self):
        return "your patrons"

    @staticmethod
    def posts_comments_filter(user_id):
        return Comment.on_posts_by_user_id_from_their_patrons(user_id)

    @staticmethod
    def replies_comments_filter(user_id):
        return Comment.on_comments_by_user_id_from_their_patrons(user_id)

    def sort_key(self):
        return self.date_for_json(), NotificationSortOrder.DailyCommentsFromMyPatronsNotification


class DailyCommentsFromOtherUsersNotification(DailyCommentsNotification):
    """Comments on my posts from anyone I don't have a patronage relationship with."""

    def comment_notification_subtype(self):
        return "others"

    @staticmethod
    def posts_comments_filter(user_id):
        return Comment.on_posts_by_user_id_from_nonpatrons(user_id)

    @staticmethod
    def replies_comments_filter(user_id):
        return Comment.on_comments_by_user_id_from_nonpatrons(user_id)

    def sort_key(self):
        return self.date_for_json(), NotificationSortOrder.DailyCommentsFromOtherUsersNotification
