import pytz

from patreon.model.manager import payment_processing_notification_mgr
from patreon.model.type import NotificationSortOrder
from patreon.util import datetime, time_math

class PaymentsProcessingNotification(object):
    def __init__(self, current_user, event_type, amount_cents, payout_type, dt, timezone):
        self.current_user = current_user
        # ['charge', 'payout', 'payout_soon', 'start_soon_creator', 'start_soon_patron',
        # 'currently_charging_creator', 'currently_charging_patron']
        self.event_type = event_type
        self.amount_cents = amount_cents
        self.payout_type = payout_type
        self.datetime = dt
        self.timezone = timezone

        # kludge :-( model.date shouldn't be a public interface but notifications_mgr user it
        self.date = self._date()

    def _date(self):
        # this is defined to be the date-in-the-user's-timezone, not UTC!
        return self.utc_timestamp().astimezone(self.timezone).date()

    def charging_for_month(self):
        # clients want to know which month this charging notification is for
        first_of_month = time_math.first_of_previous_month_for_date(self.datetime.date())
        if self.event_type in ['start_soon_patron', 'start_soon_creator', 'currently_charging_patron', 'currently_charging_creator']:
            if self.datetime.day >= payment_processing_notification_mgr.charging_start_day():
                first_of_month = time_math.first_of_month_for_datetime(self.datetime)
        return datetime.datetime_at_midnight_on_date(first_of_month, tz=self.timezone)

    def sort_key(self):
        if self.shows_as_creator_event():
            return (self.date_for_json(), NotificationSortOrder.PaymentsProcessingNotificationCreator)
        else:
            return (self.date_for_json(), NotificationSortOrder.PaymentsProcessingNotificationPatron)


    def utc_timestamp(self):
        return pytz.utc.localize(self.datetime)

    def timestamp(self):
        return self.datetime

    def start_time(self):
        return datetime.datetime_at_midnight_on_date(self.date, self.timezone)

    def date_for_json(self):
        return datetime.make_naive(self.start_time())

    def resource_id(self):
        parts = [
            "payments-processing-",
            self.notification_type(),
            str(self.current_user.user_id),
            self.charging_for_month().strftime('%Y-%m')
        ]
        return "+".join(parts)

    def is_creator_only_event(self):
        return self.event_type in ["charge", "payout", "start_soon_creator", "currently_charging_creator"]

    def shows_as_creator_event(self):
        return self.is_creator_only_event()

    def notification_type(self):
        if self.shows_as_creator_event():
            return 'charging'
        else:
            return 'charging_patron'

    def charge_state(self):
        if self.event_type in ['start_soon_creator', 'start_soon_patron']:
            return 'scheduled'
        elif self.event_type in ['currently_charging_creator', 'currently_charging_patron']:
            return 'in_progress'
        else:
            return 'done'

    def transfer_state(self):
        if self.event_type == 'payout_soon':
            return 'in_progress'
        if self.event_type == 'payout':
            return 'done'
        else:
            return 'none'

    def transfer_account_description(self):
        if self.event_type == 'payout':
            if self.payout_type == 'paypal':
                return 'PayPal'
            else:
                return 'direct deposit'
