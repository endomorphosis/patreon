import urllib.parse
from datetime import timedelta
import dateutil.parser
import itertools, functools

import patreon
from patreon.model.manager import action_mgr, unread_status_mgr
from patreon.model.type import UnreadStatusType, NotificationSortOrder
from patreon.util import datetime


class DailyLikesNotification(object):
    def __init__(self, current_user, date_, timezone, items_per_card, cursor):
        self.current_user = current_user
        self.date = date_
        self.timezone = timezone
        self.items_per_card = items_per_card
        self.cursor = cursor
        self._likes = None
        self._all_likes_notifications_sorted = None

    # TODO: pull into a mgr or dbm
    @classmethod
    def find_by_id(cls, resource_id, current_user, timezone, items_per_card=5, cursor=None):
        card_type, user_id_string, date_string = resource_id.split("+")
        if card_type != 'likes-on-posts-by':
            raise ValueError("unknown notification type " + card_type)

        date_ = dateutil.parser.parse(date_string).date()

        if str(current_user.user_id) != user_id_string:
            raise ValueError("User id mismatch")

        return cls(current_user, date_, timezone, items_per_card, cursor)

    @staticmethod
    def resource_type():
        return "notification"

    def notification_type(self):
        return 'like'

    def resource_id(self):
        parts = [
            "likes-on-posts-by",
            str(self.current_user.user_id),
            self.date.isoformat()
        ]
        return "+".join(parts)

    def likes(self):
        if not self._likes:
            self._likes = action_mgr.find_likes_on_posts_by_user_id_between_timestamps(
                self.current_user.user_id, self.start_time(), self.end_time()
            )
        return self._likes

    def post_likes_tuples(self):
        all_likes = self.likes()
        tuples = []
        for post, likes_group in itertools.groupby(all_likes, lambda x: x.post):
            likes = [like for like in likes_group]
            tuples.append((post, likes))
        return tuples

    def date_for_json(self):
        return datetime.make_naive(self.start_time())

    def start_time(self):
        return datetime.datetime_at_midnight_on_date(self.date, self.timezone)

    def end_time(self):
        return self.start_time() + timedelta(days=1)

    def sort_key(self):
        return (self.date_for_json(), NotificationSortOrder.DailyLikesNotification)

    def _all_likes_notifications(self):
        if not self._all_likes_notifications_sorted:
            self._all_likes_notifications_sorted = sorted(
                [LikesNotification(post, likes) for post, likes in self.post_likes_tuples()],
                key=lambda notification: notification.latest_like().created_at, reverse=True)
        return self._all_likes_notifications_sorted

    def likes_notifications(self):
        cursor_index = self.cursor_index()
        return self._all_likes_notifications()[cursor_index:cursor_index + self.items_per_card]

    def total_count(self):
        return len(self._all_likes_notifications())

    def total_likes(self):
        notifications = self._all_likes_notifications()
        return functools.reduce(lambda memo, notification: memo + notification.total_likes(), notifications, 0)

    def unread_count(self):
        notifications = self._all_likes_notifications()
        return functools.reduce(lambda memo, notification: memo + notification.unread_count(), notifications, 0)

    def cursor_index(self):
        for i, likes_notification in enumerate(self._all_likes_notifications()):
            if likes_notification.resource_id() == self.cursor:
                return i
        return 0

    def next_cursor(self):
        if self.cursor_index() + self.items_per_card >= self.total_count():
            return None
        return str(self._all_likes_notifications()[self.cursor_index() + self.items_per_card].resource_id())

    def next_page(self):
        cursor = self.next_cursor()
        if not cursor:
            return None
        url = "https://{}/notifications/".format(patreon.config.api_server)
        url += self.resource_id()
        url += "/links/likes-notifications"
        url += "?page[count]=" + str(self.items_per_card)
        url += "&page[cursor]=" + urllib.parse.quote(self.next_cursor())
        return url


class LikesNotification(object):
    def __init__(self, post, likes):
        self.post = post
        self.likes = likes
        self._sorted_likes = None
        self._unread_likes = None

    def resource_id(self):
        parts = [
            "likes-notification",
            str(self.post.activity_id)
        ]
        if self.latest_like():
            parts.append(self.latest_like().created_at.isoformat())
        return "+".join(parts)

    def unread_count(self):
        if not self._unread_likes:
            unread_time = unread_status_mgr.latest_read_time(self.post.user_id, UnreadStatusType.notifications)
            if unread_time:
                first_unread_index = None
                for i, like in enumerate(self.sorted_likes()):
                    if like.created_at < unread_time:
                        first_unread_index = i
                        break
                self._unread_likes = self.sorted_likes()[:first_unread_index]
            else:
                self._unread_likes = self.sorted_likes()
        return len(self._unread_likes)

    def sorted_likes(self):
        if not self._sorted_likes:
            self._sorted_likes = sorted(self.likes, key=lambda like: like.created_at, reverse=True)
        return self._sorted_likes

    def total_likes(self):
        return len(self.sorted_likes())

    def latest_like(self):
        return self.sorted_likes()[0] if self.sorted_likes() else None
