from datetime import timedelta
import itertools
import urllib.parse
import dateutil.parser
import functools

import patreon
from patreon.model.manager import unread_status_mgr, campaign_mgr, new_pledge_mgr
from patreon.model.type import UnreadStatusType, NotificationSortOrder
from patreon.util import datetime as datetime_helper


class MonthlyPledgesNotification(object):
    def __init__(self, current_user, date_, timezone, items_per_card, cursor):
        # TODO: use per_page and cursor
        self.current_user = current_user
        self.date = date_
        self.timezone = timezone
        self.items_per_card = items_per_card
        self.cursor = cursor
        self._pledge_notifications = None

    # TODO: pull into a mgr or dbm
    @classmethod
    def find_by_id(cls, resource_id, current_user, timezone, items_per_card=5, cursor=None):
        card_type, user_id_string, date_string = resource_id.split("+")
        if card_type != 'pledge-notifications':
            raise ValueError("unknown notification type " + card_type)

        date_ = dateutil.parser.parse(date_string).date()

        if str(current_user.user_id) != user_id_string:
            raise ValueError("User id mismatch")

        return cls(current_user, date_, timezone, items_per_card, cursor)

    def notification_type(self):
        return 'pledge'

    def resource_id(self):
        parts = [
            "pledge-notifications",
            str(self.current_user.user_id),
            self.date.isoformat()
        ]
        return "+".join(parts)

    def _all_pledge_notifications(self):
        if not self._pledge_notifications:
            pledges = new_pledge_mgr.find_pledges_to_user_id_between_timestamps(
                self.current_user.user_id,
                self.start_time(),
                self.end_time()
            )
            notifications = []
            sort_by_tuple = sorted(pledges, key=lambda p: (p.user_id, p.campaign_id))
            for group_tuple, pledges_group in itertools.groupby(
                    sort_by_tuple, lambda p: (p.user_id, p.campaign_id)
            ):
                pledges_list = list(pledges_group)
                notification = PledgeNotification(pledges_list, self.start_time(), self.end_time())
                # Note that this means that sometimes we will have a
                # MonthlyPledgesNotification without any PledgeNotifications at all!
                # notifications_mgr is responsible for noticing if this means we'll be short on providing
                # the requested number of items per page, and fetching more notifications if necessary.
                if notification.net_edit() != 0:
                    notifications.append(notification)
            self._pledge_notifications = sorted(notifications, key=lambda n: n.timestamp(), reverse=True)
        return self._pledge_notifications

    def pledge_notifications(self):
        cursor_index = self.cursor_index()
        return self._all_pledge_notifications()[cursor_index:cursor_index + self.items_per_card]

    def date_for_json(self):
        return datetime_helper.make_naive(self.end_time())

    def start_time(self):
        end_of_last_month = self.end_time() + timedelta(days=-1)
        first_of_last_month_date = datetime_helper.first_of_the_month(end_of_last_month)
        return datetime_helper.datetime_at_midnight_on_date(first_of_last_month_date, self.timezone)

    def end_time(self):
        return datetime_helper.datetime_at_midnight_on_date(self.date, self.timezone)

    def sort_key(self):
        return (self.date_for_json(), NotificationSortOrder.MonthlyPledgesNotification)

    def total_count(self):
        return len(self._all_pledge_notifications())

    def unread_count(self):
        return len(list(filter(lambda notification: notification.is_unread(), self._all_pledge_notifications())))

    def net_pledged(self):
        return functools.reduce(lambda memo, notification: memo + notification.net_edit(),
                                self._all_pledge_notifications(), 0)

    def net_num_patrons(self):
        return functools.reduce(lambda memo, notification: memo + notification.net_num_patrons(),
                                self._all_pledge_notifications(), 0)

    def cursor_index(self):
        for i, pledge_notification in enumerate(self._all_pledge_notifications()):
            if pledge_notification.resource_id() == self.cursor:
                return i
        return 0

    def next_cursor(self):
        cursor_index = self.cursor_index()
        if cursor_index + self.items_per_card >= self.total_count():
            return None
        return str(self._all_pledge_notifications()[cursor_index + self.items_per_card].resource_id())

    def next_page(self):
        cursor = self.next_cursor()
        if not cursor:
            return None
        url = "https://{}/notifications/".format(patreon.config.api_server)
        url += self.resource_id()
        url += "/links/pledge-notifications"
        url += "?page[count]=" + str(self.items_per_card)
        url += "&page[cursor]=" + urllib.parse.quote(self.next_cursor())
        return url


class PledgeNotification(object):
    def __init__(self, pledges, start_time, end_time):
        self.pledges = sorted(pledges, key=lambda pledge: pledge.modified_at())
        self.start_time = datetime_helper.as_utc(start_time)
        self.end_time = datetime_helper.as_utc(end_time)

    def resource_id(self):
        parts = [
            "pledge-notification",
            str(self.first_pledge().user_id),
            str(self.first_pledge().campaign_id),
            self.start_time.isoformat(),
            self.timestamp().isoformat()
        ]
        return "+".join(parts)

    def first_pledge(self):
        return self.pledges[0]

    def latest_pledge(self):
        return self.pledges[-1]

    def is_deletion(self):
        return self.latest_pledge().is_deletion \
               and self.start_time <= datetime_helper.as_utc(self.latest_pledge().deleted_at) < self.end_time

    def is_creation(self):
        return self.start_time <= datetime_helper.as_utc(self.first_pledge().created_at) < self.end_time

    def start_amount(self):
        return 0 if self.is_creation() else self.first_pledge().amount_cents

    def start_cap(self):
        return None if self.is_creation() else self.first_pledge().pledge_cap_amount_cents

    def end_amount(self):
        return 0 if self.is_deletion() else self.latest_pledge().amount_cents

    def end_cap(self):
        return None if self.is_deletion() else self.latest_pledge().pledge_cap_amount_cents

    def net_edit(self):
        return self.end_amount() - self.start_amount()

    def net_num_patrons(self):
        if self.is_creation():
            return 1
        elif self.is_deletion():
            return -1
        return 0

    def has_edits(self):
        return self.end_amount() != self.start_amount() or self.start_cap() != self.end_cap()

    def timestamp(self):
        aware_modified = datetime_helper.as_utc(self.latest_pledge().modified_at())
        aware_timestamp = min(aware_modified, self.end_time)
        aware_timestamp = aware_timestamp + timedelta(milliseconds=-1)
        return datetime_helper.as_utc(aware_timestamp).replace(tzinfo=None)

    def is_unread(self):
        user_id = campaign_mgr.try_get_user_id_by_campaign_id_hack(self.first_pledge().campaign_id)
        unread_time = unread_status_mgr.latest_read_time(user_id, UnreadStatusType.notifications)
        return unread_time < self.timestamp()
