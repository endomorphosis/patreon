class Settings(object):
    def __init__(self, user_settings, old_settings,
                 campaign_settings, follow_settings):
        self.user_id = user_settings.user_id

        self.user_settings = user_settings
        self.old_settings = old_settings

        # user's settings as a patron
        self.follow_settings = follow_settings

        # user's settings as a creator
        self.campaign_settings = campaign_settings

    def settings_id(self):
        return 'settings-for-{}'.format(self.user_id)

    # global email overrides
    def email_about_all_new_comments(self):
        return self.user_settings.email_new_comment

    def email_about_milestone_goals(self):
        return self.user_settings.email_goal

    def email_about_patreon_updates(self):
        return self.user_settings.email_patreon

    # global push overrides
    def push_about_all_new_comments(self):
        return self.user_settings.push_new_comment

    def push_about_milestone_goals(self):
        return self.user_settings.push_goal

    def push_about_patreon_updates(self):
        return self.user_settings.push_patreon

    # user's settings as a patron
    def pledges_are_private(self):
        return self.old_settings.Privacy
