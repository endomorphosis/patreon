import json
from urllib.parse import quote_plus
from urllib import parse

import patreon
from functools import wraps
from patreon.exception.auth_errors import UnauthorizedAdminFunction, UnauthorizedAdminAPIRoute
from patreon.routing import redirect
from patreon.services.caching.memcache import Memcached
from patreon.services.logging.unsorted import log_page_anomaly, log_json
import flask
from flask import request, session, make_response, has_request_context


def add_response_headers(headers={}):
    """This decorator adds the headers passed in to the response"""

    def decorator(f):
        @wraps(f)
        def decorated_function(*args, **kwargs):
            resp = make_response(f(*args, **kwargs))
            h = resp.headers
            for header, value in headers.items():
                h[header] = value
            return resp

        return decorated_function

    return decorator


def set_content_type_json(inner_f):
    @wraps(inner_f)
    @add_response_headers({'Content-Type': 'application/json'})
    def decorated_function(*args, **kwargs):
        return inner_f(*args, **kwargs)

    return decorated_function


def add_nocache_headers(inner_f):
    @wraps(inner_f)
    @add_response_headers({'Pragma': 'public', 'Cache-Control': "maxage=" + str(60 * 60 * 24 * 365)})
    def decorated_function(*args, **kwargs):
        return inner_f(*args, **kwargs)

    return decorated_function


def handle_admin_check(inner_f, exception_type, *args, **kwargs):
    if patreon.config.ignore_admin_check:
        # Ignore admin check when developing locally
        return inner_f(*args, **kwargs)

    if not flask.has_request_context():
        log_json({
            'verb': 'admin_function_called',
            'function_name': inner_f.__name__,
            'args': str(args),
            'kwargs': str(kwargs)
        })
        return inner_f(*args, **kwargs)

    if not session.user_id or not session.is_admin:
        log_page_anomaly(request.method, 'admin_unauthorized')
        raise exception_type()
    else:
        log_json({
            'verb': 'admin_function_called',
            'function_name': inner_f.__name__,
            'args': str(args),
            'kwargs': str(kwargs)
        })
    return inner_f(*args, **kwargs)


def require_login(route):
    @wraps(route)
    def logged_in_route(*args, **kwargs):
        if not session.user_id:
            log_page_anomaly(request.method, 'unauthorized')
            if request.method == 'GET':
                path = request.path
                if request.args:
                    path += '?' + parse.urlencode(request.args)
                return redirect('/login?ru=' + quote_plus(path))
            return json.dumps({}), 401
        else:
            kwargs['current_user_id'] = session.user_id
            return route(*args, **kwargs)

    return logged_in_route


def require_admin(inner_f):
    @wraps(inner_f)
    def admin_function(*args, **kwargs):
        return handle_admin_check(inner_f, UnauthorizedAdminFunction, *args, **kwargs)

    return admin_function


def api_require_admin(inner_f):
    @wraps(inner_f)
    def admin_function(*args, **kwargs):
        return handle_admin_check(inner_f, UnauthorizedAdminAPIRoute, *args, **kwargs)

    return admin_function


def catch_admin_error(inner_f):
    @wraps(inner_f)
    def admin_route(*args, **kwargs):
        try:
            return inner_f(*args, **kwargs)
        except UnauthorizedAdminFunction:
            return '', 403

    return admin_route


def unauthed_memcache(inner_f):
    @wraps(inner_f)
    def unauthed_memcache(*args, **kwargs):
        if not has_request_context() or session.is_logged_in:
            return inner_f(*args, **kwargs)
        wrapped = Memcached(inner_f, 600)
        return wrapped(*args, **kwargs)

    return unauthed_memcache


# Web_app use only
def register_internal_page(route, methods=None, name=None, category='Uncategorized', display=True):
    def decorator(f):
        from patreon.app.web import web_app

        methods_ = methods or ['GET']
        name_ = name or route
        display_ = display and 'GET' in methods_
        if display_:
            category_dict = web_app.internal_page_registry.get(category, {})
            category_dict[route] = {
                'route': route,
                'name': name_,
            }
            web_app.internal_page_registry[category] = category_dict

        @web_app.route(route, methods=methods_)
        @catch_admin_error
        @require_admin
        @wraps(f)
        def decorated_function(*args, **kwargs):
            return f(*args, **kwargs)

    return decorator
