from . import route_wrappers
from .patreon_request import PatreonRequest
from . import session
from flask import g

def call_after_request(inner_f):
    """Decorator that takes the Flask request context object
    and indicates that this function should be called after
    the current request context.

    Will throw an error if attempted to use in the same way
    as flask's normal @after_request.
    """
    if not hasattr(g, 'call_after_request'):
        g.call_after_request = []
    g.call_after_request.append(inner_f)
    return inner_f


def call_deferred_functions(response):
    for func in getattr(g, 'call_after_request', ()):
        response = func(response)
    return response