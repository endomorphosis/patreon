from functools import wraps
import patreon

from flask.sessions import SessionInterface, SessionMixin
from flask import request, session
from patreon.model.manager import session_mgr
from patreon.util.datetime import offset_from_time
from patreon.util.unsorted import jsencode, jsdecode
from sqlalchemy.orm import make_transient


class PatreonSessionInterface(SessionInterface):
    def open_session(self, app, request):
        session_id = request.cookies.get('session_id')
        authorization_header = request.headers.get('Authorization')

        session_from_cookie = True

        if patreon.config.allow_authorization_header and session_id is None:
            session_from_cookie = False
            session_id = authorization_header

        if not session_id or not session_id.strip():
            # Create a session ID for the user and set it in the cookies
            # after the request finishes.
            session_id = session_mgr.get_new_session_id()

            @patreon.model.request.call_after_request
            def force_session_cookie(response):
                self.set_session_cookie(response, session_id)
                return response

        return self.get_session(session_id, session_from_cookie)

    def save_session(self, app, session, response):
        max_age = patreon.globalvars.get('SESSION_LIFETIME')

        if not session.user_id or (not session.dirty and \
                                           (session.expires_at and session.expires_at > offset_from_time(
                                               seconds=max_age, minutes=-10))):
            return
        if session.session_id and session.session_id.strip():
            session_mgr.update_session(session.session_id, session.user_id, session.csrf_token,
                                       session.csrf_token_expiration, session.is_admin,
                                       session.extra_data, max_age)

    def get_session(self, session_id, is_cookie_session=True):
        session_instance = session_mgr.get_session(session_id)
        if not session_instance:
            session_instance = session_mgr.get_blank_session(session_id)
        session = PatreonSession(session_instance, is_cookie_session)
        return session

    def delete_session(self, session):
        session_mgr.destroy_session(session.session_id)

    def garbage_collection(self):
        session_mgr.collect_garbage()

    def set_session_cookie(self, response, session_id):
        # Make sure that the cookie is accessible to all domains.
        proto = request.headers.get('X-Forwarded-Proto')
        if proto == 'http' and response.headers.get('Location'):
            # Don't set session cookies if proto is http and Location is redirect.
            return
        if patreon.config.main_server.endswith('patreon.com'):
            domain = 'patreon.com'
        else:
            # Use default value (current hostname)
            domain = None
        max_age = 30 * 24 * 3600
        response.set_cookie('session_id', session_id, max_age=max_age, httponly=True, secure=True, domain=domain)


def login_user(user, application):
    session.login(user.user_id, user.is_admin)
    new_session_id = session_mgr.get_new_session_id()
    session.session_id = new_session_id

    @patreon.model.request.call_after_request
    def force_session_cookie(response):
        application.session_interface.set_session_cookie(response, new_session_id)
        return response


def _extra_json_property(inner_f):
    @wraps(inner_f)
    def handle_json_property(*args, **kwargs):
        self = args[0]
        if self.session.extra_data_json:
            kwargs['json_data'] = jsdecode(self.session.extra_data_json)
        else:
            kwargs['json_data'] = {}
        result = inner_f(*args, **kwargs)
        if isinstance(result, dict):
            self.session.extra_data_json = jsencode(result)
        else:
            return result

    return handle_json_property


def _mark_dirty(inner_f):
    @wraps(inner_f)
    def dirty_session(*args, **kwargs):
        self = args[0]
        self.dirty = True
        inner_f(*args, **kwargs)

    return dirty_session


class PatreonSession(SessionMixin):
    def __init__(self, session, uses_cookie=True):
        super(PatreonSession).__init__()
        make_transient(session)
        self.session = session
        self.uses_cookie = uses_cookie
        self.meta = {}
        self.dirty = False

    @_mark_dirty
    def login(self, user_id, is_admin=0):
        self.user_id = user_id
        self.is_admin = is_admin
        self.original_user_id = user_id

    @_mark_dirty
    def logout(self):
        self.user_id = 0
        self.is_admin = 0
        self.csrf_token = None
        self.original_user_id = None

    @_mark_dirty
    def impersonate(self, user_id):
        self.session.user_id = user_id

    @property
    def is_impersonating(self):
        return self.user_id != self.original_user_id

    @property
    def session_id(self):
        return self.session.session_token

    @session_id.setter
    @_mark_dirty
    def session_id(self, new_session_id):
        self.session.session_token = new_session_id

    @property
    def is_logged_in(self):
        return self.user_id != 0

    @property
    def user_id(self):
        return self.session.user_id or 0

    @user_id.setter
    @_mark_dirty
    def user_id(self, new_user_id):
        self.session.user_id = new_user_id

    @property
    def csrf_token(self):
        return self.session.csrf_token or None

    @csrf_token.setter
    @_mark_dirty
    def csrf_token(self, new_csrf_token):
        self.session.csrf_token = new_csrf_token

    @property
    def csrf_token_expiration(self):
        return self.session.csrf_token_expires_at or None

    @csrf_token_expiration.setter
    @_mark_dirty
    def csrf_token_expiration(self, new_time):
        self.session.csrf_token_expires_at = new_time

    @property
    def is_admin(self):
        return self.session.is_admin == 1

    @is_admin.setter
    @_mark_dirty
    def is_admin(self, new_admin_flag):
        self.session.is_admin = new_admin_flag

    @property
    def extra_data(self):
        return self.session.extra_data_json or '{}'

    @extra_data.setter
    @_mark_dirty
    def extra_data(self, new_extra_data):
        self.session.extra_data_json = new_extra_data

    @property
    @_extra_json_property
    def original_user_id(self, json_data):
        return json_data.get('original_user_id')

    @original_user_id.setter
    @_mark_dirty
    @_extra_json_property
    def original_user_id(self, user_id, json_data):
        json_data['original_user_id'] = user_id
        return json_data

    @property
    def using_cookie_authentication(self):
        return self.uses_cookie

    @property
    def expires_at(self):
        return self.session.expires_at
