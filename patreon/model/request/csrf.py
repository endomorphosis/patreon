from patreon.exception.auth_errors import CSRFExpired, CSRFFailed
from patreon.model.manager import csrf
from flask import session
from flask import request


def handle_csrf_check(csrf_uri, csrf_signature, csrf_time):
    if csrf_uri is None or csrf_signature is None or csrf_time is None:
        csrf.log_csrf_event('csrf_array_missing_values', csrf_time, csrf_uri, csrf_signature)
        raise CSRFFailed()

    try:
        csrf_time = float(csrf_time)
    except:
        csrf.log_csrf_event('csrf_invalid_time', csrf_time, csrf_uri, csrf_signature)
        raise CSRFFailed()

    expected_token = csrf.calculate_csrf_signature(csrf_uri, session.user_id, session.csrf_token, csrf_time)

    if csrf.is_expired(csrf_time):
        csrf.log_csrf_event('token_expired', expected_token, csrf_time, csrf_uri, csrf_signature)
        raise CSRFExpired()

    if not request.referrer or csrf.is_invalid_referrer(request.referrer, csrf_uri):
        csrf.log_csrf_event('token_invalid_referer', expected_token, csrf_time, csrf_uri, csrf_signature)
        raise CSRFFailed()

    if csrf.is_invalid_csrf_token(session.user_id, session.csrf_token, csrf_time, csrf_uri, csrf_signature):
        csrf.log_csrf_event('token_mismatch', expected_token, csrf_time, csrf_uri, csrf_signature)
        raise CSRFFailed()

    csrf.log_csrf_event('csrf_okay', expected_token, csrf_time, csrf_uri, csrf_signature)

    return None
