"""
Helper functions for request-based functionality.
"""

import flask
from flask import Request, session
import parse


class PatreonRequest(Request):
    @property
    def rollbar_person(self):
        """Overrides the request context object that is defined in Flask."""
        if flask.has_request_context() and session.user_id:
            if session.user_id:
                return {
                    'id': str(session.user_id)
                }
        else:
            return {'id': 0}

    def request_ip(self):
        request_ip = self.environ.get('HTTP_X_CLUSTER_CLIENT_IP') or self.environ.get(
            'HTTP_X_FORWARDED_FOR') or self.environ.get('REMOTE_ADDR')
        if request_ip:
            # Remove extraneous X-Forwarded-For parts, only take the last hop.
            request_ip = request_ip.split(',')[0].strip()
        return request_ip

    def dictionary_from_get(self, outer_key):
        params = self.args

        return self.__parse_parameters_to_dictionary(params, outer_key)

    def dictionary_from_post(self, outer_key):
        params = self.form

        return self.__parse_parameters_to_dictionary(params, outer_key)

    @property
    def ab_test_group_id(self):
        return self.cookies.get('group_id')

    def __parse_parameters_to_dictionary(self, params, outer_key):
        key_filter_no_nested = parse.compile('{}[{}]')
        key_filter_single_nested = parse.compile('{}[{}][{}]')
        return_dict = {}

        for key in params.keys():
            parts_no_nested = key_filter_no_nested.parse(key)
            parts_single_nested = key_filter_single_nested.parse(key)

            if parts_single_nested and parts_single_nested.fixed[0] == outer_key:
                inner_dict = return_dict.get(parts_single_nested.fixed[1], {})
                if type(inner_dict) is not dict:
                    continue
                inner_dict[parts_single_nested.fixed[2]] = params[key]
                return_dict[parts_single_nested.fixed[1]] = inner_dict

            elif parts_no_nested and parts_no_nested.fixed[0] == outer_key:
                return_dict[parts_no_nested.fixed[1]] = params[key]

        return return_dict
