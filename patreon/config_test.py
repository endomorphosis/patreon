"""
config_test.py

Default values for test configurations. Assumes that the
local mysql instance has been set up.
"""

config_name = 'Testing'
main_server = 'localhost'

database = {
    'name': 'mysql-testing',
    'enabled': True,
    'parameters': dict(
        host = '127.0.0.1',
        user = 'root',
        db = 'patreon_test',
        ),
    'log_parameters': {
        'protocol': 'postgres',
        'host': 'localhost',
        'port': 5432,
        'user': 'patreon',
        'passwd': '',
        'db': 'postgres',
    }
}

akismet = {
    'key': '1a069252ac93'
}


memcached = {
    'name': 'memcache-testing',
    'enabled': True,
    'servers': ['localhost:11211'],
    'key_prefix': 'test'
}

aws = {
    'prefix': None,
    'region': 'us-west-1',
    'access_key': '',
    'secret_key': ''
}

# Used for stripe auth
stripe = {
    'publishable_key': 'pk_test_J8504y4xwkKHJHa6KLY4nAdZ',
    'secret_key': 'sk_test_7eJdrBTXtvDp8WQP4S64m8t7'
}

paypal = {
    'url': 'https://www.sandbox.paypal.com/',
    'api_url': 'https://api-3t.sandbox.paypal.com/nvp',
    'username': 'sell_api1.patreon.com',
    'password': '1368838954',
    'signature': 'AFcWxV21C7fd0v3bYYYRCpSSRl31AoaP2rNvR01jULXDdbdiqb65N5ou'
}

sqs = {
    'post_queue': '*',
    'campaign_queue': '*'
}

queuer = 'StubSQS'

# S3 buckets
bucket_campaign = 'patreon'
bucket_post = 'test'
bucket_user = 'test'

mandrill_api_key = 'iYfuvhtc9Fn4l9_T9Q-HoA'
mandrill_mailer = 'FauxMandrillMailer'

file_uploader = 'FauxUploader'

embeder = 'StubEmbedder'
embedly_key = '96a16496e36611e091d14040d3dc5c07'

image_getter = 'StubImageGetter'

flume = {
    'enabled': False
}

debug = True

max_attachment_bytes = 30 * 1024
