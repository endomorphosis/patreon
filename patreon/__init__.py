"""
patreon/__init__.py

Base import for patreon. Should do a few things:
1. Determine which environment we are in based on os or sys.argv.
2. Load all the modules.
3. Wait for initialization.
"""

import os
import socket

from . import config_defaults

environment = os.environ.get('PATREON_ENV') or 'default'
repo_root = os.path.dirname(os.path.dirname(__file__))

try:
    if environment == 'test':
        from . import config_test as config
    elif environment == 'test_circle':
        from . import config_test_circle as config
    else:
        from . import config

    for attrname in dir(config_defaults):
        if not hasattr(config, attrname):
            setattr(config, attrname, getattr(config_defaults, attrname))

except ImportError:
    print()
    print("Unable to continue importing patreon module.")
    print("A config.py file is necessary in patreon/ directory.")
    print()
    raise

# See New Relic in 1Password for stats
if socket.gethostname() in config.newrelic_servers:
    import newrelic.agent
    newrelic.agent.initialize(os.path.join(repo_root, 'newrelic.ini'), config.stage)


def is_test_environment():
    return environment.startswith("test")

def is_dev_environment():
    return config.stage == "development"

from . import globalvars
from . import constants
from . import util
from . import routing
from . import services
from . import model
from . import test
from . import stats

# Create the database instances and connections.
services.initialize()

# Initialize this here once all the other things have loaded.
pstore = services.pstore.PStore(services.db)
