"""
test.py

Functions used to aid in writing unit tests.
"""
import json
import uuid
import functools
from flask import session

import patreon
from patreon.model.table import PaymentEvent
from patreon.services import clear_request_cache, db, timer
from patreon.services.encryption import clear_encryption_keys
from patreon.services.file_storer import drop_the_buckets
from patreon.services.mail_file_writer import clean_inboxes
from patreon.services.db import db_session, log_db_session
from patreon.util.unsorted import jsencode


def asserttest():
    def wrap_inner(inner_f):
        @functools.wraps(inner_f)
        def new_inner_f(*args, **kwargs):
            if not patreon.is_test_environment():
                raise ValueError("{0} should only be run on a test environment!".format(inner_f.__name__))
            return inner_f(*args, **kwargs)

        return new_inner_f

    return wrap_inner


def manage():
    def wrap_inner(test_fn):
        @functools.wraps(test_fn)
        @asserttest()
        def new_test_fn():
            db_session.close()
            flush_memcache()
            patreon.pstore.empty()
            clean_inboxes()
            drop_the_buckets()
            clear_request_cache()
            clear_encryption_keys()
            timer.refresh()
            try:
                test_fn()
            finally:
                reset_database()

        return new_test_fn

    return wrap_inner


@asserttest()
def reset_database():
    for element in db.test_dirty_table:
        patreon.services.db.query("truncate table {0}".format(element.__tablename__))
    db.test_dirty_table = set()
    db_session.commit()

    if db.test_payment_events_dirty:
        PaymentEvent.query.filter(PaymentEvent.payment_event_id != None).delete()
        log_db_session.commit()
    db.test_payment_events_dirty = False


@asserttest()
def flush_memcache():
    patreon.services.caching.memcache.flush_all()


class MockSession(object):
    def __init__(self, app):
        self.app = app
        self.session_id = str(uuid.uuid4())
        self.user_id = None
        self.cookie = ''

    def handle_response(self, response):
        if isinstance(response, str):
            response = self.app.make_response(response)
        elif isinstance(response, tuple):
            resp_copy = response
            response = self.app.make_response(response[0])
            response.status_code = resp_copy[1]

        response = self.app.process_response(response)
        setcookies = response.headers.getlist('Set-Cookie')
        for setcookie in setcookies:
            self.cookie += setcookie + "; "
        if 'json' in response.headers.get('Content-Type', 'text/html'):
            response.data_str = response.get_data().decode('utf-8')
            response.data_obj = json.loads(response.data_str)
        return response

    def get(self, url):
        with self.app.test_request_context(url,
                                           method='GET',
                                           headers={'Cookie': self.cookie}):
            resp = self.app.preprocess_request()
            if resp:
                return self.handle_response(resp)
            try:
                ret = self.app.dispatch_request()
            except Exception as e:
                ret = self.app.handle_user_exception(e)
            patreon.services.clear_request_cache()
            return self.handle_response(ret)

    def post(self, url, data, use_form=False, use_csrf=True, nest_json_in_data=True):
        if use_form:
            if use_csrf:
                csrf_data = self.get_csrf_data_web(url)
                data.update(csrf_data)
            with self.app.test_request_context(
                    url,
                    method='POST',
                    data=data,
                    headers={
                        'Content-Type': 'application/x-www-form-urlencoded',
                        'Cookie': self.cookie,
                        'Referer': url
                    }):
                resp = self.app.preprocess_request()
                if resp:
                    return self.handle_response(resp)
                try:
                    ret = self.app.dispatch_request()
                except Exception as e:
                    ret = self.app.handle_user_exception(e)
                patreon.services.clear_request_cache()
                return self.handle_response(ret)
        else:
            uri, time, signature = '', '', ''
            if use_csrf:
                uri, signature, time = self.get_csrf_data_api(url)
            if nest_json_in_data:
                data = {'data': data}
            with self.app.test_request_context(
                    url,
                    method='POST',
                    data=json.dumps(data),
                    headers={
                        'Content-Type': 'application/json',
                        'Cookie': self.cookie,
                        'Referer': url,
                        'X-CSRF-URI': uri,
                        'X-CSRF-Signature': signature,
                        'X-CSRF-Time': time
                    }):
                resp = self.app.preprocess_request()
                if resp:
                    return self.handle_response(resp)
                try:
                    ret = self.app.dispatch_request()
                except Exception as e:
                    ret = self.app.handle_user_exception(e)
                patreon.services.clear_request_cache()
                return self.handle_response(ret)

    def patch(self, url, data, use_csrf=True):
        headers = {'Content-Type': 'application/json'}
        if use_csrf:
            uri, signature, time = self.get_csrf_data_api(url)
            headers.update({'Referer': url,
                            'X-CSRF-URI': uri,
                            'X-CSRF-Signature': signature,
                            'X-CSRF-Time': time
                            })
        return self.manual(url, data=jsencode({'data': data}), method='PATCH', headers=headers)

    def put(self, url, data, use_csrf=True):
        headers = {'Content-Type': 'application/json'}
        if use_csrf:
            uri, signature, time = self.get_csrf_data_api(url)
            headers.update({'Referer': url,
                            'X-CSRF-URI': uri,
                            'X-CSRF-Signature': signature,
                            'X-CSRF-Time': time
                            })
        return self.manual(url, data=jsencode({'data': data}), method='PUT', headers=headers)

    def manual(self, url, data=None, method='GET', headers=None):
        cookie_helpers = {'Cookie': self.cookie}
        if not headers:
            headers = cookie_helpers
        else:
            headers.update(cookie_helpers)

        with self.app.test_request_context(
                url,
                method=method,
                data=data,
                headers=headers):
            resp = self.app.preprocess_request()
            if resp:
                return self.handle_response(resp)
            try:
                ret = self.app.dispatch_request()
            except Exception as e:
                ret = self.app.handle_user_exception(e)
            patreon.services.clear_request_cache()
            return self.handle_response(ret)

    def options(self, url):
        return self.manual(url, method='OPTIONS')

    def delete(self, url, headers=None, data=None, use_csrf=True):
        delete_headers = {'Content-Type': 'application/json'}

        if not headers:
            headers = delete_headers
        else:
            headers.update(delete_headers)

        if use_csrf:
            uri, signature, time = self.get_csrf_data_api(url)
            headers.update({'Referer': url,
                            'X-CSRF-URI': uri,
                            'X-CSRF-Signature': signature,
                            'X-CSRF-Time': time
                            })

        if data:
            data = json.dumps({
                'data': data
            })

        return self.manual(url, data=data, method='DELETE', headers=headers)

    def file(self, url, file, filename, headers=None, data=None, use_csrf=True):
        file_headers = {'Content-Type': 'multipart/form-data'}
        if not headers:
            headers = file_headers
        else:
            headers.update(file_headers)

        if use_csrf:
            uri, signature, time = self.get_csrf_data_api(url)
            headers.update({'Referer': url,
                            'X-CSRF-URI': uri,
                            'X-CSRF-Signature': signature,
                            'X-CSRF-Time': time
                            })

        return self.manual(
            url,
            data={
                'file': (file, filename),
                'data': json.dumps(data or {})
            },
            method='POST',
            headers=headers
        )

    def files(self, url, files_dict, headers=None, data=None, use_csrf=True):
        file_headers = {'Content-Type': 'multipart/form-data'}
        if not headers:
            headers = file_headers
        else:
            headers.update(file_headers)

        if use_csrf:
            uri, signature, time = self.get_csrf_data_api(url)
            headers.update({'Referer': url,
                            'X-CSRF-URI': uri,
                            'X-CSRF-Signature': signature,
                            'X-CSRF-Time': time
                            })

        return self.manual(
            url,
            data={
                'files': [
                    (file, filename)
                    for filename, file in files_dict.items()
                    ],
                'data': json.dumps(data or {})
            },
            method='POST',
            headers=headers
        )

    def get_csrf_data_web(self, path):
        url = '/REST/auth/CSRFTicket?uri=' + path
        resp = self.get(url)
        data = json.loads(resp.get_data(as_text=True))
        return {
            'CSRFToken[URI]': data['URI'],
            'CSRFToken[token]': data['token'],
            'CSRFToken[time]': data['time']
        }

    def get_csrf_data_api(self, path):
        url = '/CSRFTicket?uri=' + path
        resp = self.get(url)
        data = json.loads(resp.get_data(as_text=True))
        return data.get('URI'), data.get('token'), data.get('time')

    def is_admin(self):
        cookie_header = {'Cookie': self.cookie}
        with self.app.test_request_context('/', method='GET', headers=cookie_header):
            return session.is_admin

    def __enter__(self):
        self.context = self.app.test_request_context('/', method='GET', headers={'Cookie': self.cookie})
        return self.context.__enter__()

    def __exit__(self, exc_type, exc_val, exc_tb):
        temp_context = self.context
        self.context = None
        return temp_context.__exit__(exc_type, exc_val, exc_tb)
