import json
import uuid
import time
import base64
import requests

from flask import session, request
import patreon
from patreon.services.logging import rollbar


AMPLITUDE_COOKIE_NAME = 'amplitude_idpatreon.com'
DEVICE_ID_KEY = 'deviceId'


def _get_device_id():
    device_id = None
    cookie_structure = {}
    did_change_cookie = False

    # check to see if an amplitude created device_id exists in the cookie
    cookie_data = request.cookies.get(AMPLITUDE_COOKIE_NAME)
    if cookie_data:
        try:
            parsed_data = json.loads(base64.b64decode(cookie_data.encode('ascii')).decode('utf-8'))
        except ValueError:
            # we shouldn't try to salvage a broken cookie
            return

        cookie_structure = parsed_data
        device_id = parsed_data.get(DEVICE_ID_KEY)

    if device_id is None:
        device_id = str(uuid.uuid4())
        did_change_cookie = True

    if did_change_cookie:
        cookie_structure[DEVICE_ID_KEY] = device_id
        # here we pad the unencoded cookie data with spaces to ensure that
        # there is no padding of '=' at the end. This is because werkzeug
        # will enclose values with equals signs in quotes, and amplitude's
        # javascript library doesn't expect quotes
        cookie_bytes = json.dumps(cookie_structure).encode('utf-8')
        incomplete_chunk_size = len(cookie_bytes) % 3
        if incomplete_chunk_size != 0:
            cookie_bytes += b' ' * (3 - incomplete_chunk_size)
        cookie_data = base64.b64encode(cookie_bytes).decode('ascii')

        @patreon.model.request.call_after_request
        def set_amplitude_cookie(response):
            response.set_cookie(AMPLITUDE_COOKIE_NAME, cookie_data)
            return response

    return device_id


def log_patreon_event(event_type, event_properties=None, revenue=None):
    return user_log_patreon_event(
        user_id=session.user_id,
        event_type=event_type,
        event_properties=event_properties,
        revenue=revenue
    )


def user_log_patreon_event(user_id, event_type, event_properties=None, user_properties=None, revenue=None):
    if patreon.config.amplitude_key is None:
        return None

    event_data = {
        'event_type': event_type,
        'time': int(time.time() * 1000),
        'device_id': _get_device_id()
    }

    if user_id:
        event_data['user_id'] = user_id

    if event_properties is not None:
        event_data['event_properties'] = event_properties

    if user_properties is not None:
        event_data['user_properties'] = user_properties

    if revenue is not None:
        event_data['revenue'] = revenue

    amplitude_data = {
        'api_key': patreon.config.amplitude_key,
        'event': json.dumps(event_data)
    }
    make_async_log_call(amplitude_data)


def make_async_log_call(amplitude_data):
    response = requests.post('https://api.amplitude.com/httpapi', data=amplitude_data)

    if response.status_code == 200:
        return response.text
    else:
        extra_data = {
            'error_description': response.text,
            'error_json_dump': amplitude_data['event']
        }
        rollbar.log(message='Invalid response from Amplitude Logging', extra_data=extra_data)
