import flask
from flask import request
import requests
import patreon

api_key = patreon.config.akismet.get('key')


def __get_akismet_url():
    return "https://{}.rest.akismet.com/1.1/comment-check".format(api_key)


def is_spam(comment_text, email, name):
    if not api_key:
        raise Exception('No Akismet API Key')
    return akismet_check_spam(comment_text, email, name)


def akismet_check_spam(comment_text, email, name):
    if flask.has_request_context():
        ip = request.request_ip()
        user_agent = request.headers.get('User-Agent')
    else:
        ip = '127.0.0.1'
        user_agent = 'localhost'

    params = {
        'blog': 'https://www.patreon.com/',
        'user_ip': ip,
        'user_agent': user_agent,
        'comment_type': 'comment',
        'comment_content': comment_text,
        'comment_author_email': email,
        'comment_author': name
    }
    result = requests.post(__get_akismet_url(), data=params)

    return result and result.text == 'true'
