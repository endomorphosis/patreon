from collections import namedtuple
from .external_payment_api_interface import ExternalPaymentAPIInterface
from functools import wraps
import patreon


def capture_external_payment_api_exception_events(inner_f):
    @wraps(inner_f)
    def capture_exception(*args, **kwargs):
        try:
            return inner_f(*args, **kwargs)
        except Exception as e:
            patreon.model.manager.payment_event_mgr.add_exception_event(e)
            raise e

    return capture_exception

CardInfo = namedtuple("CardInfo", ["card_id", "last_4", "type", "country", "expiration"])

from .stripe_api import StripeAPI
from .paypal_api import PayPalAPI
