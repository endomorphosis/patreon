import patreon
from os import listdir, mkdir
from os.path import isdir, isfile
from shutil import rmtree

inboxes_directory = patreon.repo_root + '/test/inbox/'


def write_html_to_file(recipient, subject, html):
    write_content_to_file(recipient, subject, html, ".html")


def write_txt_to_file(recipient, subject, txt):
    write_content_to_file(recipient, subject, txt, ".txt")


def clean_inboxes():
    if isdir(inboxes_directory):
        rmtree(inboxes_directory)
    mkdir(inboxes_directory)


def unread_count(recipient):
    return len(listdir(get_recipient_directory(recipient)))


def get_recipient_directory(recipient):
    directory_name = inboxes_directory + recipient + "/"

    if not isdir(inboxes_directory):
        mkdir(inboxes_directory)

    if not isdir(directory_name):
        mkdir(directory_name)  # TODO MARCOS handle failure

    return directory_name


def write_content_to_file(recipient, subject, content, suffix):
    directory = get_recipient_directory(recipient)
    filename = subject + suffix
    i = 1
    while isfile(directory + filename):  # while file exists
        filename = subject + "_" + str(i) + suffix
        i += 1

    file = open(directory + filename, "w")
    file.write(str(content))
    file.close()
