import os
from tempfile import NamedTemporaryFile
from PIL import Image

from patreon.exception.post_errors import InvalidImageFormat
from patreon.util.image_processing import resizer, converter


class Resizer(object):
    def __init__(self, image_tempfile_filename, image_sizes,
                 destination_format="JPEG"):
        self.image_sizes = image_sizes
        self._resize_files = []

        with open(image_tempfile_filename, 'rb') as file:
            try:
                mode = 'RGBA' if destination_format == 'PNG' else 'RGB'
                self._image = Image.open(file).convert(mode)
            except OSError:
                raise InvalidImageFormat(destination_format)

            self.original_size = self._image.size
            self.destination_format = destination_format \
                or converter.convert_format(self._image.format)

            # Couldn't determine the file type of this image.
            if not destination_format:
                raise InvalidImageFormat(self._image.format)

            self._resize_files = {
                image_size.name: self._resize_image(image_size)
                for image_size in self.image_sizes
            }

    def __enter__(self):
        return self

    def __exit__(self, exc_type, exc_val, exc_tb):
        self.delete_resizes()

    def _resize_image(self, image_size):
        raise NotImplementedError()

    def delete_resizes(self):
        for name in self._resize_files:
            os.remove(self._resize_files[name])

    def resizes(self):
        return self._resize_files


class TopOffsetResizer(Resizer):
    def __init__(self, image_tempfile_filename, image_sizes,
                 destination_format="JPEG", top_offset=None):
        self.top_offset = top_offset
        super().__init__(image_tempfile_filename, image_sizes, destination_format)

    def _resize_image(self, image_size):
        destination_width, destination_height = image_size.dimensions

        if destination_height:
            scaled = resizer.crop_and_resize(
                self._image, destination_width, destination_height,
                self.top_offset
            )
        else:
            scaled = resizer.shrink_to_width(self._image, destination_width)

        extension = '.' + self.destination_format.lower()
        with NamedTemporaryFile(suffix=extension, delete=False) as file:
            scaled.save(file.name, self.destination_format)
            return file.name


class ArbitraryResizer(Resizer):
    def __init__(self, image_tempfile_filename, image_sizes,
                 destination_format="JPEG", source_width=None,
                 source_height=None, source_x=None, source_y=None):
        self.source_width = source_width
        self.source_height = source_height
        self.source_x = source_x
        self.source_y = source_y
        super().__init__(image_tempfile_filename, image_sizes, destination_format)

    def _resize_image(self, image_size):
        destination_width, destination_height = image_size.dimensions

        scaled = resizer.arbitrary_crop_and_resize(
            self._image, destination_width, destination_height,
            self.source_width, self.source_height, self.source_x, self.source_y
        )

        extension = '.' + self.destination_format.lower()
        with NamedTemporaryFile(suffix=extension, delete=False) as file:
            scaled.save(file.name, self.destination_format)
            return file.name
