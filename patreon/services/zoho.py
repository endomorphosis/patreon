from lxml import etree
from patreon.model.manager import zoho_campaigns_mgr
from patreon.services.logging.unsorted import log_json
import requests

ZOHO_AUTH_TOKEN = {
    'authtoken': "639393545e748d7842b3f669e6ee1e6f",
    'scope': "crmapi",
    'version': '4'
}

ZOHO_BASE_URL = "https://crm.zoho.com/crm/private/json/"

ZOHO_CREATE_ENDPOINT = "/insertRecords"
ZOHO_UPDATE_ENDPOINT = "/updateRecords"


def build_row(number, record, row_text='FL'):
    row = etree.Element('row', no=str(number))
    fields = record.keys()
    for field in fields:
        if field != 'Campaign':
            element = etree.Element(str(row_text), val=str(field))
            element.text = str(record[field])
            row.append(element)
    return row


def build_xml_doc(top_level_attr, records):
    xml_doc = etree.Element(top_level_attr)

    row_count = 0
    for record in records:
        xml_doc.append(build_row(row_count, record))
        row_count += 1

    return etree.tostring(xml_doc, pretty_print=True)


def get_lead_convert_xml():
    root = etree.Element('Potentials')
    data = {
        'createPotential': 'false',
        'assignTo': 'hello@patreon.com',
        'notifyLeadOwner': 'true',
        'notifyNewEntityOwner': 'true'
    }
    row = build_row(1, data, row_text='option')

    root.append(row)

    return etree.tostring(root, pretty_print=True)


def zoho_post(url, document, lead_id=None, use_post=True):
    if len(document) <= 21:
        return {}

    query_vars = ZOHO_AUTH_TOKEN.copy()
    query_vars.update({'xmlData': document})
    query_vars.update({'leadId': lead_id})
    if use_post:
        response = requests.post(url, data=query_vars)
    else:
        response = requests.post(url, params=query_vars)
    return response.json()


def post_update(endpoint, document):
    url = ZOHO_BASE_URL + endpoint + ZOHO_UPDATE_ENDPOINT
    return zoho_post(url, document)


def post_create(endpoint, document):
    url = ZOHO_BASE_URL + endpoint + ZOHO_CREATE_ENDPOINT
    return zoho_post(url, document)


def post_lead_convert(zoho_id):
    url = "https://crm.zoho.com/crm/private/json/Leads/convertLead"

    result = zoho_post(url, get_lead_convert_xml(), zoho_id, False)

    log_json({
        'verb': 'zoho',
        'action': 'convert',
        'campaign_id': zoho_id,
        'input': get_lead_convert_xml().decode(),
        'output': result
    })

    if not result.get('success') or (result.get('response') \
           and result.get('response').get('error') \
           and 'invalid or already' in result.get('response').get('error').get('message')):
        zoho_campaigns_mgr.update(None, zoho_id, 1)
    else:
        new_zoho_id = result['success']['Account']['content']
        zoho_campaigns_mgr.update_zoho_id(zoho_id, new_zoho_id, 1)


def create_map(api_result, record_list, is_converted):
    if api_result == {} or api_result is None:
        return
    if not api_result.get('response').get('result'):
        return

    api_rows = api_result['response']['result']['row']

    if not type(api_rows) is list:
        row = api_rows
        if row.get('success'):
            index = int(row['no'])
            zoho_id = row['success']['details']['FL'][0]['content']
            zoho_campaigns_mgr.insert(record_list[index]['Campaign'].campaign_id, zoho_id, is_converted)
    else:
        for row in api_rows:
            if row.get('success'):
                index = int(row['no'])
                zoho_id = row['success']['details']['FL'][0]['content']
                zoho_campaigns_mgr.insert(record_list[index]['Campaign'].campaign_id, zoho_id, is_converted)
