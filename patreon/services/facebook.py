import requests
import json


def me(access_token):
    response = requests.get(
        "https://graph.facebook.com/v2.2/me",
        params={'access_token': access_token}
    )
    if response.status_code == 200:
        return json.loads(response.text)
    else:
        raise ValueError("Invalid response from Graph API: " + response.text)
