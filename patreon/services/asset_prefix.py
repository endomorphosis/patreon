import patreon
from patreon.services.logging.unsorted import log_anomaly


def webclient_prefix():
    # Check if we are on delta and should use a twin branch.
    delta_prefix = patreon.services.delta.delta_webclient_prefix()
    if patreon.config.local_webclient_prefix_override:
        return patreon.config.local_webclient_prefix_override
    elif delta_prefix:
        return delta_prefix
    else:
        # Production branches are in the 'patreon-webclient-assets' bucket.
        sha = patreon.pstore.get('webclient_deployed_sha')
        if sha is None:
            log_anomaly('pstore', 'webclient_deployed_sha', None, None,
                        'currently deployed webclient SHA not available!')
        return "//s3-us-west-1.amazonaws.com/patreon-webclient-assets/{}".format(sha)


def react_prefix():
    # Check if we are on delta and should use a twin branch.
    delta_prefix = patreon.services.delta.delta_react_prefix()

    if patreon.config.local_react_prefix_override:
        return patreon.config.local_react_prefix_override
    elif delta_prefix:
        return delta_prefix
    else:
        # Production branches are in the 'patreon-webclient-assets-react' bucket.
        sha = patreon.pstore.get('react_deployed_sha')
        if sha is None:
            log_anomaly('pstore', 'react_deployed_sha', None, None,
                        'currently deployed React SHA not available!')
        return "//s3-us-west-1.amazonaws.com/patreon-webclient-assets-react/{}".format(sha)
