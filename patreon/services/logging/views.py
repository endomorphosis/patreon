from patreon.services.logging.unsorted import log_json


def log_comment(activity_id, campaign_creator, comment_id, thread_id, user_id,
                comment_text):
    log_json({
        'verb': 'make-comment',
        'creation': {
            'id': activity_id,
            'creator': campaign_creator.UID
        },
        'comment': {
            'id': comment_id,
            'thread': thread_id,
            'user': user_id,
            'text': comment_text,
            'votes': {
                'up': 0,
                'down': 0,
                'diff': 0,
                'sum': 0
            }
        }
    })


def log_spam(user_id, email, name, vanity, activity_id, comment_text):
    log_json({
        "verb": "spam_detected",
        "reason": 'akismet',
        "spammer": {
            "userID": user_id,
            "email": email,
            "name": name,
            "vanity": vanity
        },
        "spam": {
            "targetCreation": activity_id,
            "text": comment_text
        }
    })
