"""
rollbar.py

If configured to log errors, start logging errors.
"""

import patreon

import sys
import os
import rollbar
import rollbar.contrib.flask
from flask import got_request_exception


def initialize(app=None):
    if patreon.config.rollbar_api_key:

        rollbar.init(
            patreon.config.rollbar_api_key,
            patreon.config.rollbar_environment,
            root = os.path.dirname(os.path.realpath(__file__)),
            allow_logging_basic_config = False)

        if app:
            # Send exceptions from `app` to rollbar, using Flask's signal system.
            got_request_exception.connect(rollbar.contrib.flask.report_exception, app)


def log(message, level='debug', extra_data=None, request=None):
    rollbar.report_message(message, level, extra_data=extra_data, request=request)


def log_current_exception(message=None):
    rollbar.report_exc_info(sys.exc_info(), extra_data={'message': message})
