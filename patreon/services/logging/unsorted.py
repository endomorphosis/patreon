import time
import datetime
import json
import socket
import uuid

from patreon.services.async import task_with_threads, task

import user_agents
import patreon
import flask
import functools
from flask import session, request
from patreon.services import red, aws, async
from patreon.services.logging import rollbar


def record_stats(stats):
    # TODO(implement)
    pass


GET_PARAMS = [
    'api_key',
    'include',
    'filter',
    'page[cursor]',
    'creator',
    'page',
    'q',
    'u',
    'hid',
    'h',
    's',
    'i',
    'response_id',
    'ru',
    'p',
    'ty',
    'vanity',
    'uri',
    'order',
    't',
    'v',
    'rf',
    'edit',
    'selectReward',
    'sStreet',
    'sStreet2',
    'sCity',
    'sState',
    'sZip',
    'sCountry'
    'patAmt',
    'rgsn',
    'rid',
    'hid',
    'alert',
    'cid',
    'srt',
    'cat',
    'sec',
    'f',
    'ev',
    'up',
    'c',
    'msg',
    'sect',
    'x1',
    'y1',
    'w',
    'h'
]

POST_PARAMS = [
    'email',
    'api_key',
    'CSRFToken[URI]',
    'CSRFToken[token]',
    'CSRFToken[time]',
    'fullName',
    'oneline',
    'createSingle',
    'create',
    'monthCheck',
    'nsfwCheck',
    'about',
    'videoUrl',
    'shareEmbed',
    'videoUrl',
    'thanksMsg',
    'deleteCid'
    'hid',
    'on',
    'ty',
    'inputComment',
    'threadid'
    'cid',
    'new_mesage',
    'uid',
    'follow',
    'user_id',
    'security_token',
    'uname',
    'name',
    'settingsAbout',
    'settingsFacebook',
    'settingsYoutube',
    'settingsTwitter',
    'email',
    'patronUpdate',
    'postUpdate',
    'commentUpdate',
    'emailUpdate',
    'privacyUpdate',
    'milesUpdate'
]

POST_LISTS = [
    'categories[]',
    'gid[]',
    'rTitle[]',
    'rDescription[]',
    'rAmount[]',
    'rid[]',
    'rShipping[]',
    'rLimit[]',
    'postlat[]',
    'postlng[]',
    'postlocation[]'
]


def ignore_errors(f):
    @functools.wraps(f)
    def inner_f(*args, **kwargs):
        try:
            return f(*args, **kwargs)
        except Exception as e:
            if not patreon.is_test_environment():
                print("Exception raised while calling {0}: {1}".format(f.__name__, e))

    return inner_f


def get_request_vars():
    if not flask.has_request_context():
        return {}
    parameters = {'post': {}, 'get': {}}

    for key, val in request.args.items():
        if key in GET_PARAMS:
            parameters['get'][key] = val

    for key, val in request.form.items():
        if key in POST_PARAMS:
            parameters['post'][key] = val

    for key in POST_LISTS:
        if key in request.form:
            parameters['post'][key] = request.form.getlist(key)

    return parameters


@async.task
def ping_async():
    log_action('ping_async')


# object_type is a string which identifies the object type. all lower case, usually the same as the sqlalchemy model
# object_id is self explanatory
# action create, update, delete, delete_all or some other descriptive name
# reason Whatever makes sense, I usually use the endpoint.
def log_state_change(object_type, object_id, action, reason):
    if not flask.has_request_context():
        return
    log_json({
        "verb": "state_change",
        "object_type": object_type,
        "object_id": object_id,
        "action": action,
        "reason": reason,
        "changing_user_id": session.user_id,
        "request": {
            "referrer": request.referrer,
            "target": request.path,
            "parameters": get_request_vars()
        }
    })


def log_anomaly(object_type, object_id, object_owner, object_action, description):
    if not flask.has_request_context():
        return
    log_json({
        "verb": "anomalous_event",
        "object_type": object_type,
        "object_id": object_id,
        'object_action': object_action,
        'object_owner': object_owner,
        'description': description,
        "request": {
            "referrer": request.referrer,
            "parameters": get_request_vars()
        }
    })


def log_page_anomaly(object_action, description):
    if not flask.has_request_context():
        return
    log_anomaly('page', request.path, None, object_action, description)


def log_auth_fail(email, reason=None):
    user = patreon.model.manager.user_mgr.get_unique(email=email)
    if user is None:
        user_id = None
        if not reason:
            reason = "invalid_user"
    else:
        user_id = user.UID
        if not reason:
            reason = "invalid_password"

    log_json({
        "verb": "auth_fail",
        "email": email,
        "user_id": user_id,
        "reason": reason,
        "request": {
            "referrer": request.referrer,
            "parameters": get_request_vars()
        }
    })


def log_action(action, **kwargs):
    data = kwargs.copy()
    data['verb'] = action
    return log_json(data)


@ignore_errors
def log_json(data=None):
    if patreon.is_test_environment() or not patreon.config.logging_enabled:
        return
    log_json_(data)


def log_json_(data=None):
    """Log generic JSON data into redis. Metadata included along with the message includes:

    @timestamp: current time in seconds since epoch
    type:       always 'unparsed-json'
    host:       current hostname
    @version:   always 1
    message:    data, including the current time, user information, and url
    path:       always /var/log/json/log.json
    """
    if data is None:
        return

    if flask.has_request_context():
        agent = {
            'ip': request.request_ip(),
            'user': session.user_id if session else None,
            # TODO(port): Save this cookie after the request as well. set_cookie
            'cookie': str(uuid.uuid4())
        }
        root = request.url_root
        url = '/' + request.url[len(root):]
        route = url.split('?')[0].replace('.php', '')
    else:
        agent = {
            'ip': '127.0.0.1',
            'user': 0,
            'cookie': str(uuid.uuid4())
        }
        url = None
        route = None

    message = data.copy()
    message['agent'] = agent
    message['when'] = int(time.time())
    message['url'] = url[:512] if url else url  # Truncate url so it fits in loglines
    message['route'] = route
    message['stage'] = patreon.config.stage
    message['host'] = socket.gethostname()

    log_message_sqs(message)
    log_message_database(message)


@ignore_errors
def log_message_sqs(message):
    aws.sqs.put('json_loglines', json.dumps(message))


@ignore_errors
def log_message_redis(message):
    logline = {
        '@timestamp': datetime.datetime.now().isoformat(),
        'type': 'unparsed-json',
        'host': socket.gethostname(),
        '@version': '1',
        'message': json.dumps(message),  # Double encoding is intentional here.
        'path': '/var/log/json/log.json'
    }
    red.log.rpush('loglines', json.dumps(logline))


@ignore_errors
def log_message_database(message):
    if patreon.pstore.get('loglines_enabled'):
        patreon.services.db.query_log('INSERT INTO loglines (action, data, created_at) VALUES (%s, %s, %s)',
                                      (message.get('verb', 'none'), json.dumps(message), datetime.datetime.now())
                                      )


REMOVED_KEYS = set([
    'taxid',
    'ustaxid',
    'us_tax_id_number',
    'foreign_tax_id_number',
    'fb_access_token'
])


def sanitize_data(obj):
    if isinstance(obj, dict):
        obj = dict(obj)
        for key in obj:
            key_lower = key.lower()
            if 'password' in key_lower or key_lower in REMOVED_KEYS:
                obj[key] = '******'
            else:
                obj[key] = sanitize_data(obj[key])
        return obj
    elif isinstance(obj, list):
        return [sanitize_data(item) for item in obj]
    else:
        return obj


response_bucket = aws.S3Bucket('patreon-responses')

@ignore_errors
@task
def log_darkmode(data):
    patreon.services.aws.sqs.put('prodweb-python-darkmode', json.dumps(data))

@ignore_errors
def log_response(logged_session, logged_request, logged_response):
    """Log all the request and response information from the request. Take
    the response text and upload it to S3, and then pass the S3 url in as
    the response_url."""
    if patreon.pstore.get('response_log_enabled'):
        user_agent = logged_request.headers.get('User-Agent', '')
        if user_agents.parse(user_agent).is_bot:
            return

        if logged_session:
            session_id = logged_session.session_id
            user_id = logged_session.user_id
        else:
            session_id = None
            user_id = None

        response_key = str(uuid.uuid4())
        _log_response_s3(response_key, logged_response.data)

        # Get the query string information as well as the path
        root = logged_request.url_root
        url = '/' + logged_request.url[len(root):]

        request_ip = logged_request.request_ip()
        if logged_request.json:
            request_data = str(sanitize_data(logged_request.json))
        elif logged_request.form:
            request_data = str(sanitize_data(logged_request.form))
        else:
            request_data = ''
        request_method = request.method

        if logged_request.headers:
            request_headers = json.dumps(sanitize_data(dict(logged_request.headers)))
        else:
            request_headers = json.dumps({})

        response_code = logged_response.status_code
        host = logged_request.headers.get('X-Forwarded-Host') or logged_request.headers.get('Host')
        created_at = datetime.datetime.now()

        _log_response(session_id, user_id, url,
                      request_ip, request_data, request_headers,
                      response_code, response_key,
                      host, created_at, request_method)

@ignore_errors
@task_with_threads
def _log_response_s3(response_key, data):
    response_bucket.set(response_key, data)


@ignore_errors
@task_with_threads
def _log_response(session_id, user_id, url, request_ip, request_data, request_headers, response_code, response_key, host, created_at,
                  request_method):
    patreon.services.db.query_log('''
            INSERT INTO response_log (
                session_id, user_id, url,
                request_ip, request_data, request_headers,
                response_code, response_key,
                host, created_at, request_method
            ) VALUES (
                %s, %s, %s,
                %s, %s, %s,
                %s, %s,
                %s, %s, %s)
            ''', (
        session_id, user_id, url,
        request_ip, request_data, request_headers,
        response_code, response_key,
        host, created_at, request_method
    )
                                  )


def log_rollbar(message, level='debug'):
    return rollbar.log(message, level)
