import time

last_time = None


def refresh():
    global last_time
    last_time = time.clock() * 1000


def click():
    global last_time
    now = time.clock() * 1000
    diff = now - last_time
    last_time = now
    return diff
