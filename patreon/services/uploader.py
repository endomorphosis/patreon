from abc import ABCMeta, abstractmethod
import mimetypes

import patreon
from patreon.services import file_storer
from patreon.services.aws import S3Bucket


class Uploader(object):
    __metaclass__ = ABCMeta

    def upload_files(self, temporary_upload_collection):
        for temporary_upload in temporary_upload_collection:
            self.upload_file(temporary_upload)

        return temporary_upload_collection.key

    @abstractmethod
    def upload_file(self, temporary_upload):
        pass

    @abstractmethod
    def delete_file(self, bucket, key):
        pass


class StubUploader(Uploader):
    """
    This uploader was created to take image files and return a fake s3 url.
    It does not use an internet connection.
    """
    def upload_file(self, temporary_upload):
        return temporary_upload.destination_filename

    def delete_file(self, bucket, key):
        return True


class FauxUploader(Uploader):
    def upload_file(self, temporary_upload):
        return file_storer.upload_file(
            temporary_upload.source_filename,
            temporary_upload.destination_filename,
            temporary_upload.destination_bucket
        )

    def delete_file(self, bucket, key):
        return file_storer.delete_file(bucket, key)


class S3Uploader(Uploader):
    def upload_file(self, temporary_upload):
        bucket = temporary_upload.destination_bucket
        key = temporary_upload.destination_filename

        mimetype, _ = mimetypes.guess_type(temporary_upload.destination_filename)
        headers = { 'Content-Type': mimetype }
        s3_bucket = S3Bucket(bucket.bucket, bucket.region)
        s3_bucket.upload(key, temporary_upload.source_filename, headers)

        return "//{0}.amazonaws.com/{1}/{2}".format(
            bucket.endpoint, bucket.bucket, key
        )

    def delete_file(self, bucket, key):
        s3_bucket = S3Bucket(bucket.bucket)

        if s3_bucket.exists(key):
            s3_bucket.delete(key)
            return True
        return False

_uploaders = {
    'StubUploader': StubUploader,
    'FauxUploader': FauxUploader,
    'S3Uploader': S3Uploader
}

uploader_type = patreon.config.file_uploader
uploader = _uploaders[uploader_type]()


def upload_files(temporary_upload_collection):
    return uploader.upload_files(temporary_upload_collection)


def upload_file(temporary_upload):
    return uploader.upload_file(temporary_upload)


def delete_file(bucket, key):
    return uploader.delete_file(bucket, key)
