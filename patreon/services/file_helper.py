import os
from tempfile import NamedTemporaryFile
from patreon.model.type import TemporaryLocalFilename


def get_extension(filename):
    # Returns the extension with the preceding '.'.
    return os.path.splitext(filename)[1]


def file_stream_to_tempfile(file_object):
    with NamedTemporaryFile(delete=False) as file:
        file.write(file_object.read())
        return TemporaryLocalFilename(file.name)


def get_file_contents(filename):
    with open(filename, 'r') as file:
        return " ".join(file.readlines())
