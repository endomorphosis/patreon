import patreon
from patreon.services.mail.mailer import StubMailer, FauxMandrillMailer, MandrillMailer

_mailers = {
    'StubMailer': StubMailer,
    'FauxMandrillMailer': FauxMandrillMailer,
    'MandrillMailer': MandrillMailer
}

api_key = patreon.config.mandrill_api_key
mailer_type = patreon.config.mandrill_mailer
mailer_ = _mailers[mailer_type](api_key=api_key)


def send_template_email(template):
    return mailer_.send_template_email(template)


def update_template(template_name, html, text=None):
    mailer_.update_template(template_name, html, text)
