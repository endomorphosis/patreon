from premailer import Premailer
import patreon
from patreon.model.manager import feature_flag
from patreon.services import db
from patreon.services.file_helper import get_file_contents
from patreon.services.mail import MandrillMailer


def user_to_recipient(user):
    return {
        "email": user.email,
        "name": user.full_name,
        "type": "to"
    }


def user_to_recipient_list(user):
    return users_to_recipient_list([user])


def users_to_recipient_list(users):
    return [user_to_recipient(user) for user in users]


def user_row_to_recipient(user_row):
    return {
        "email": user_row['Email'],
        "name": " ".join(filter(None, [user_row['FName'], user_row['LName']])),
        "type": "to"
    }


def sql_to_recipient_list(users_sql):
    return [
        user_row_to_recipient(user_row)
        for user_row in db.query(users_sql)
    ]


def sql_to_recipient_list_with_flag(users_sql, flag=None):
    if not flag:
        return sql_to_recipient_list(users_sql)

    return [
        user_row_to_recipient(user_row)
        for user_row in db.query(users_sql)
        if feature_flag.is_enabled(flag, user_id=user_row['UID'])
    ]


def combine_templates(outer_template_filename, css_filename, inner_template_html):
    template_html_string = get_file_contents(outer_template_filename)
    css_string = get_file_contents(css_filename)
    return template_html_string\
        .replace('***', css_string)\
        .replace('+++', inner_template_html)


def combined_html_for_template(template, content_html=None, content_text=""):
    # Combine the HTML of this note into the template.
    template_directory = patreon.repo_root + '/patreon/services/mail/views/'

    if content_html is None:
        internal_content_filename = '{}templates/{}.html'.format(template_directory, template.template_name.rstrip('1234567890'))
        content_html = get_file_contents(internal_content_filename)

    external_template_filename = template_directory + 'template.html'
    css_filename = template_directory + 'template.css'
    combined_html = combine_templates(
        external_template_filename, css_filename, content_html
    )

    # Use a CSS inliner to combine the HTML and CSS.
    premailer = Premailer(combined_html)#, base_url=template_directory)
    improperly_escaped_html = premailer.transform()
    improperly_escaped_html = improperly_escaped_html \
        .replace('%2A%7C', '*|') \
        .replace('%7C%2A', '|*')
    if template.merge_language == 'handlebars':
        improperly_escaped_html = improperly_escaped_html \
            .replace('%7B%7B', '{{') \
            .replace('%7D%7D', '}}') \
            .replace('*|', '{{') \
            .replace('|*', '}}')
    return improperly_escaped_html


def upload_template(template, content_html=None, content_text=""):
    finished_template = combined_html_for_template(template, content_html, content_text)

    api_key = patreon.config.mandrill_api_key
    # Upload the finished template to Mandrill.
    MandrillMailer(api_key).update_template(
        template, finished_template, content_text
    )
