from enum import Enum


class MergeVar(Enum):
    ALT_TEXT            = "ALT"  # TODO MARCOS rename in notify new post.
    ACTIVATION_LINK     = "ACTIVATION_URL"
    CONTENT             = "CONTENT"
    COMMENT_URL         = "COMMENT_URL"
    COMMENT_TEXT        = "COMMENT_TEXT"
    CREATION_IMG        = "CREATION_IMG"
    CREATION_TITLE      = "CREATION_TITLE"
    CREATION_URL        = "CREATION_URL"
    CREATOR_LIST        = "CREATOR_LIST"
    CREATOR_NAME        = "CREATOR_NAME"
    CREATOR_IMG         = "CREATOR_IMG"
    CREATOR_FEED_URL    = "CREATOR_FEED_URL"
    CREATOR_URL         = "CREATOR_URL"
    DATE                = "DATE"
    FIRST_NAME          = "FNAME"
    GOAL_AMOUNT         = "GOAL_AMOUNT"
    GOAL_TITLE          = "GOAL_TITLE"
    GREETING            = "GREETING"
    IS_MONTHLY          = "IS_MONTHLY"
    IS_PAID             = "IS_PAID"
    IS_REPLY            = "IS_REPLY"
    MESSAGE             = "MESSAGE"  # TODO MARCOS CONTENT?
    MESSAGE_URL         = "URL"  # TODO MARCOS collision?
    NEW_EMAIL           = "NEW_EMAIL"
    OWN_FEED            = "OWN_FEED"
    OLD_PLEDGE_AMOUNT   = "OLD_PLEDGE_AMOUNT"
    PATRON_IMAGE        = "PATRON_IMG"
    PATRON_NAME         = "PATRON_NAME"
    PATRON_URL          = "PATRON_URL"
    PLEDGE_AMOUNT       = "PLEDGE_AMOUNT"
    PLEDGE_BASIS        = "PLEDGE_BASIS"
    PLEDGE_CAP          = "PLEDGE_CAP"
    PLEDGE_CONFIRMATION_URL = "PLEDGE_CONFIRM_URL"
    PLEDGE_TOTAL        = "PLEDGE_TOTAL"  # TODO MARCOS AMOUNT?
    PLEDGES_URL         = "PLEDGES_URL"
    RECEIPT_LIST_HTML   = "RECEIPT_LIST_HTML"
    RECEIPT_LIST_TXT    = "RECEIPT_LIST_TXT"
    RECIPIENT_EMAIL     = "RECIPIENT"  # TODO MARCOS rename in _every_ template.
    RECIPIENT_NAME      = "RECIPIENT_NAME"
    REWARD_LEVEL        = "REWARD_LEVEL"
    SENDER_NAME         = "SENDER_NAME"
    # TODO MARCOS figure out (Patron, Creator, Sender, Recipient) variable naming.
    # TODO MARCOS see Notify New Comment
    SENDER_URL          = "SENDER_URL"
    SENDER_IMG          = "SENDER_IMG"
    SETTINGS_URL        = "SETTINGS_URL"  # TODO MARCOS i need to know which ENVIRONMENT in order to create URL
    SUPPORTED_CREATIONS = "SUPPORTED_CREATIONS_URL"
    TITLE               = "TITLE"  # TODO MARCOS is this used? should it be CREATION_TITLE?
    VALIDATION_URL      = "VALIDATION_URL"
    VERIFIED            = "VERIFIED"
