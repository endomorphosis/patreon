import random

_greetings = [
    "Hi {},",
    "Hey {},"
]


def next_greeting(name, key):
    return _greetings[key].format(name)


def next_key():
    random.randrange(len(_greetings))
    return 0


class Greeting():
    def __init__(self, name, key=None):
        key = key if key else next_key()
        self.value = next_greeting(name, key)

    def __str__(self):
        return self.value
