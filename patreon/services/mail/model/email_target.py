# TODO MARCOS Make EmailTarget an Enum.


class EmailTarget():
    def __init__(self, _email, name):
        self._email = _email
        self.name = name

    @property
    def email(self):
        return self._email + '@patreon.com'

BIGMONEY        = EmailTarget("bigmoney",           "Patreon Big Money")
BINGO           = EmailTarget("bingo",              "Patreon")
CARDFAIL        = EmailTarget("cardfail",           "Patreon")
COLE            = EmailTarget("cole",               "Cole Palmer")
COPYRIGHT       = EmailTarget("copyright",          "Patreon")
DAVE            = EmailTarget("dave",               "Dave Hunt")
DELETE          = EmailTarget("delete",             "Patreon")
ERROR           = EmailTarget("errorreport",        "Patreon")
FEEDBACK        = EmailTarget("feedback",           "Patreon")
HELLO           = EmailTarget("hello",              "Patreon")
HENRY           = EmailTarget("brohenry",           "Henry Brown")
JACK            = EmailTarget("jack",               "Jack Conte")
JACK_NOREPLY    = EmailTarget("noreply",            "Jack Conte")
JOBS            = EmailTarget("jobs",               "Patreon")
MESSAGE         = EmailTarget("message",            "Patreon Messages")
NOREPLY         = EmailTarget("noreply",            "Patreon")
PASSWORD        = EmailTarget("password",           "Patreon Password Reset")
PAYMENTS        = EmailTarget("payments",           "Patreon Payments")
PAYMENTSREQUEST = EmailTarget("paymentsrequest",    "Patreon Payments")
PRESS           = EmailTarget("press",              "Patreon")
PRIVACY         = EmailTarget("privacy",            "Patreon")
REGISTRATION    = EmailTarget("registration",       "Patreon Registration")
SAM             = EmailTarget("sam",                "Sam Yam")
SUPPORT         = EmailTarget("support",            "Patreon Support")
TARYN           = EmailTarget("noreply",            "Taryn Arnold")
TEAM            = EmailTarget("team",               "Patreon Team")
ZACH            = EmailTarget("zach",               "Zach")
