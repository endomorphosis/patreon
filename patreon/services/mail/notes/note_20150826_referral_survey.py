from patreon.services.mail.model import email_target
from patreon.services.mail.patreon_message import MassMessage
from patreon.services.mail.util import sql_to_recipient_list

_subject = "Help us help you!"

# language=HTML
_html = """
<div class="inner-text-content">
    <h2>Creators!!</h2>
    <p>
        We have been thinking about setting up a referral program open only to
        creators.
    </p>
    <p>
        We want to keep the Patreon community happy and thriving and need your
        feedback to help make this program the perfect fit for our community of
        creators.
    </p>
    <p>
        <strong>The survey should only take about 5 minutes</strong>
        (For real. I just took it and it took me 4 minutes and 9 seconds&hellip;)
        and would help a ton in crafting the best program to do right by our
        creators.
    </p>
    <p style="text-align: center;">
        <b> You can take the survey here:</b>
        <br>
        <a href="https://docs.google.com/forms/d/1B1spc0zlwZ4hur6-kuGB0DvW2SAor9AoTRDfOlYU5SE/viewform?usp=send_form">
          SUPER FUN REFERRAL PROGRAM INPUT SURVEY!!!
        </a>
    </p>
    <p>
        Thank you for your continual help in making Patreon the best place we can
        make it! You all rock!
    </p>
    <div class="signature">
        <div class="signature-face">
            <img src="https://s3.amazonaws.com/patreon_public_assets/signature/signature-sean_300.png"
                 alt="Sean Baeyens"
                 height="70px">
        </div>
        <div class="signature-text">
            <p>Thanks Again!</p>
            <p>Sean Baeyens</p>
            <p>Marketing Associate</p>
        </div>
    </div>
</div>
"""

_text = """
Creators!!

We have been thinking about setting up a referral program open only to creators. It would allow you to be rewarded for recommending the site and allow your friends to get rewarded when they join.

We want to keep the Patreon community happy and thriving and need your feedback to help make this program the perfect fit for our community of creators.

The survey should only take about 5 minutes (For real. I just took it and it took me 4 minutes and 9 seconds...) and would help a ton in crafting the best program to do right by our creators.

You can take the survey here:

https://docs.google.com/forms/d/1B1spc0zlwZ4hur6-kuGB0DvW2SAor9AoTRDfOlYU5SE/viewform?usp=send_form

Thank you for your continual help in making Patreon the best place we can make it! You all rock!

Thanks Again!
Sean Baeyens
Marketing Associate
"""

# Find all creators who have successfully processed payments since the date threshold.
# language=MySQL
_sql = """
SELECT
    users.FName,
    users.LName,
    users.Email
FROM (
        SELECT
            CID,
            MAX(CONVERT_TZ(Created,'GMT','US/Pacific')) AS LastPaymentDate
        FROM tblRewardsGive
        WHERE
          Status = 1
        GROUP BY CID
    ) last_payments
LEFT JOIN tblUsers as users
    ON last_payments.CID = users.UID
LEFT JOIN user_settings
    ON (users.UID = user_settings.user_id)
WHERE
    last_payments.LastPaymentDate >= '2015-06-01'
AND
    user_settings.email_patreon = 1
AND
    users.Email IS NOT NULL
AND
    MOD(users.UID, 5) = 0
ORDER BY
    users.UID
LIMIT 1
OFFSET 0
;
"""


def recipients_function():
    return sql_to_recipient_list(_sql)


template = MassMessage(
    _subject, email_target.NOREPLY, _html, _text, recipients_function
)
