from patreon.model.manager import feature_flag
from patreon.services import db
from patreon.services.mail.model import email_target
from patreon.services.mail.patreon_message import MassMessage
from patreon.services.mail.util import user_row_to_recipient

_subject = "Update: Updates!"

# language=HTML
_html = """
<div class="inner-text-content">
    <p>
        Patreon is where your biggest supporters come together - and we want to make it easier than ever for you to know what&rsquo;s going on.
    </p>
    <p>
        Introducing Dashboard &mdash; a new tab that contains the information
        that&rsquo;s relevant to you as a creator.
    </p>

    <img src="https://s3.amazonaws.com/patreon_public_assets/notifications_launch_screenshot.png"
        title="Updates Screenshot">

    <p>
        Over the next few weeks, we will be rolling out Dashboard to all Patreon creators. You&rsquo;ll be able to see all conversations, likes, and monthly financial updates &mdash; all in one place.
    </p>
    <p>
        Each day, Dashboard will show you the following types of updates:
    </p>
    <ul>
        <li> <b>New comments</b> on all of your posts, grouped by type of user. Read and even reply to all of your new comments across dozens of posts, all from the same place. </li>
        <li> <b>New likes</b> on all of your posts. See which of your posts have been getting love.</li>
        <li> Updates on <b>pledge charging.</b> Understand when we are charging your patrons and how much you earned each month. </li>
    </ul>

    <p>Keep an eye out! We'll be adding more types of updates here over time.</p>
    <p>Try out <a href="https://www.patreon.com/updates">Dashboard</a> today.</p>
    <p>Love,<br>Patreon</p>
</div>
"""

_text = """
Patreon is where your biggest supporters come together - and we want to make it easier than ever for you to know what's going on.

Introducing Dashboard -- a new tab that contains the information that's relevant to you as a creator.

Over the next few weeks, we will be rolling out Dashboard to all Patreon creators. You'll be able to see all conversations, likes, and monthly financial updates -- all in one place.

Each day, Dashboard will show you the following types of updates:

New comments on all of your posts, grouped by type of user. Read and even reply to all of your new comments across dozens of posts, all from the same place.
New likes on all of your posts. See which of your posts have been getting love.
Updates on pledge charging. Understand when we are charging your patrons and how much you earned each month.
Keep an eye out! We'll be adding more types of updates here over time.

Try out Dashboard today.

Love,
Patreon
"""

# language=MySQL
_sql = """
SELECT
  users.UID, users.FName, users.LName, users.Email
FROM
  tblUsers AS users
JOIN
  campaigns_users
  ON (users.UID = campaigns_users.user_id)
JOIN user_settings
  ON (users.UID = user_settings.user_id)
JOIN campaigns
  ON (campaigns.campaign_id = campaigns_users.campaign_id)
WHERE
  user_settings.email_patreon = 1
AND
  campaigns.published_at IS NOT NULL
AND
  users.Email IS NOT NULL
ORDER BY
  users.UID
LIMIT 1
OFFSET 0
;
"""

def recipients_function():
    return [
        user_row_to_recipient(user_row)
        for user_row in db.query(_sql)
        if not feature_flag.is_enabled('home_shows_notifications', user_id=user_row['UID'])
    ]


template = MassMessage(
    _subject, email_target.NOREPLY, _html, _text, recipients_function
)
