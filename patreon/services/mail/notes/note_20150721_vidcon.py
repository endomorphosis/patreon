from patreon.services.mail.model import email_target
from patreon.services.mail.patreon_message import MassMessage
from patreon.services.mail.util import sql_to_recipient_list

_subject = "Quick update for ya!"

# language=HTML
_html = """
<div class="inner-text-content">
    <h2>Creators!!</h2>
        <p>
            It&rsquo;s Taryn from Patreon here! Before I tell you how much
            greatness we have going on, let&rsquo;s get to know each other.
            Make things nice and comfy in this email, you know?
        </p>
        <p>
            <b>3 things about me:</b>
            <ol>
                <li>
                  I&rsquo;m on the Community Happiness team, so I&rsquo;m
                  talking to many of you through our
                  <a href="https://patreon.zendesk.com/hc/en-us">Help Center</a>
                  and
                  <a href="https://twitter.com/patreon">Twitter</a>
                  daily. #EmojisEverywhere
                </li>
                <li>
                  I also do <a href="https://soundcloud.com/patreon/sets/calling-all-creators">Calling All Creators</a>
                  (and a <a href="https://www.youtube.com/watch?v=ZXXkb6-Mh4Q">new live stream</a>
                  with Jack Conte, CEO of Patreon!) because I thrive on getting people excited about Patreon.
                </li>
                <li> I love sushi. Like, so much.</li>
            </ol>
        </p>
        <p>
            <b>3 things about you:</b>
            <ol>
                <li>
                    You are awesomely talented, creative, and compelling.
                    You inspire me every day!
                </li>
                <li>
                    You have seen a lot of changes to Patreon lately&mdash;if
                    you don&rsquo;t know much about them, I&rsquo;ll get into
                    that below.
                </li>
                <li>
                    You&rsquo;re about to see some incredible improvements in
                    the next few months. I can&rsquo;t wait to share them with you.
                </li>
            </ol>
        </p>
        <p>
          Nice to meet you! I speak for the whole Patreon team when I say how
          much we love Patreon&mdash;getting you paid for your epicness gets us
          out of bed every morning. With that, I wanted to write you an email
          to express that while we have been making much needed changes,
          <b>we&rsquo;re only going to get better and faster.</b>
          Our team is hustling to make improvements left and right, as often
          as we can. We want to give you the best experience you can possibly
          have here. We want you to feel like Patreon is home and has
          everything you need to succeed. We have been hiring like <b>crazy</b>
          (we have tripled the size of our engineering team in the last 3
          months&hellip; so much horsepower). We&rsquo;re gaining so much
          bandwidth to improve Patreon.
        </p>
        <p>
          Our team is laser focused on getting you the most money possible,
          and our recent feature rollouts have been all about that.
          <ul>
              <li>
                <b>Notifications Dashboard:</b>
                <a href="https://www.patreon.com/updates">Your Dashboard</a>
                is the new hub for your patron interaction. We&rsquo;ve made
                it infinitely easier for you to connect with your patrons;
                you can answer comments, see likes, and always know
                what&rsquo;s going on with your pledges.
              </li>
              <li>
                <b>Mobile responsiveness:</b>
                Patreon is on its way to becoming fully mobile-responsive, so
                 fans can turn into patrons from the palms of their hands.
               </li>
              <li>
                <b>Make A Post:</b>
                The new <a href="https://www.patreon.com/post">Make A Post</a>
                flow is a beautiful revamp from the prior, with rich text and
                a better, safer, easier experience throughout.
              </li>
          </ul>
        </p>
        <p>
          So yes, we&rsquo;ve been really busy, but we&rsquo;re gladly
          getting busier. Here&rsquo;s what&rsquo;s on the near horizon for you.
        </p>
        <p>
          <b>Mobile app. </b>
          YESSSS. This is a real, actual thing that is going to be in your
          hands in no time. I have it on my phone, and it&rsquo;s stellar.
          <ul>
              <li>
                It&rsquo;s breathtakingly beautiful, so simple and intuitive.
              </li>
              <li>
                You&rsquo;ll be closer to your fans than ever. This app will
                support rich interaction between patrons and creators. Without
                 patrons, what would Patreon be?
               </li>
              <li>
                Oh, and we may or may not have some (INSANELY COOL) surprises
                features built in. Who knows. (We do.)  </li>
              <li>
                And now, the moment you&rsquo;ve been waiting for&hellip; some
                mocks of the app!
              </li>
          </ul>
        </p>
        <div class="images">
            <div class="image">
              <img src="https://s3.amazonaws.com/patreon_public_assets/mobile_preview_1.png"
                   alt="Mobile Mockup 1"
                   height="375">
            </div>
            <div class="image">
              <img src="https://s3.amazonaws.com/patreon_public_assets/mobile_preview_2.png"
                   alt="Mobile Mockup 2"
                   height="375">
            </div>
        </div>
        <p>
          <b>New Creator Page.</b>
          We&rsquo;ve geared our newly-polished creator page to maximize the
          support you get. We aim for this page to be simple, special, and
          useful for both your current patrons and potential new patrons.
        </p>
        <p>
          <b>Payments updates.</b>
          <ul>
              <li>
                <b>VAT:</b>
                Due to a recent change in regulation, online businesses
                (and independent creators) are responsible for paying tax based
                 on the location of EU-based consumers. Instead of making
                 creators like you responsible for collecting and paying VAT,
                 we&rsquo;re taking care of it automatically for your EU-based
                  patrons, and you won&rsquo;t have to do a thing!
              </li>
              <li>
                <b>Patronage Accuracy:</b>
                We completely hear your frustration with your public-facing
                pledge amount being higher than your actual payout, and
                we&rsquo;re gladly fixing that. In our next round of updates,
                we&rsquo;ll make sure fans visiting your page see a dollar
                amount much more reflective of what you&rsquo;re actually
                being paid.
              </li>
          </ul>
        </p>
        <p>
          And this is just the beginning&mdash;we&rsquo;ve got <b>so much more</b>
          up our sleeves for the rest of 2015. Busy bees, but we love it.
          Our team is so jazzed to transform your experience on Patreon for the
          better. I know we haven&rsquo;t done it perfectly, and I&rsquo;m so
          sorry for any trouble this has caused you. But I can assure you that
          we hear every one of your feature requests, complaints, thoughts;
          and we take them so seriously. Thank you for your patience.
          You&rsquo;re a complete rockstar.
        </p>
        <div class="signature">
            <div class="signature-face">
                <img src="https://s3.amazonaws.com/patreon_public_assets/signature_taryn.png"
                     alt="Taryn Arnold"
                     height="70px">
            </div>
            <div class="signature-text">
                <p>Cheers to a better Patreon!</p>
                <h3>Taryn Arnold</h3>
                <p>Community Happiness</p>
            </div>
        </div>
        <p>
          Oh, and if ya need me call me, no matter where ya are, no matter how
          far, don&rsquo;t worry baby! (Actually, we don&rsquo;t have a
          support number, but feel free to send us a note
          <a href="https://patreon.zendesk.com/hc/en-us/requests/new">here</a>!)
        </p>
    </div>
</div>
"""

_text = """
Creators!!

It’s Taryn from Patreon here! Before I tell you how much greatness we have going on, let’s get to know each other. Make things nice and comfy in this email, you know?

3 things about me:
I’m on the Community Happiness team, so I’m talking to many of you through our Help Center and Twitter daily. #EmojisEverywhere
I also do Calling All Creators (and a new live stream with Jack Conte, CEO of Patreon!) because I thrive on getting people excited about Patreon.
I love sushi. Like, so much.

3 things about you:
You are awesomely talented, creative, and compelling. You inspire me every day!
You have seen a lot of changes to Patreon lately—if you don’t know much about them, I’ll get into that below.
You’re about to see some incredible improvements in the next few months. I can’t wait to share them with you.

Nice to meet you! I speak for the whole Patreon team when I say how much we love Patreon—getting you paid for your epicness gets us out of bed every morning. With that, I wanted to write you an email to express that while we have been making much needed changes, we’re only going to get better and faster. Our team is hustling to make improvements left and right, as often as we can. We want to give you the best experience you can possibly have here. We want you to feel like Patreon is home and has everything you need to succeed. We have been hiring like crazy (we have tripled the size of our engineering team in the last 3 months… so much horsepower). We’re gaining so much bandwidth to improve Patreon.

Our team is laser focused on getting you the most money possible, and our recent feature rollouts have been all about that.

Notifications Dashboard: Your Dashboard is the new hub for your patron interaction. We’ve made it infinitely easier for you to connect with your patrons; you can answer comments, see likes, and always know what’s going on with your pledges.
Mobile responsiveness: Patreon is on its way to becoming fully mobile-responsive, so fans can turn into patrons from the palms of their hands.
Make A Post: The new Make A Post flow is a beautiful revamp from the prior, with rich text and a better, safer, easier experience throughout.
So yes, we’ve been really busy, but we’re gladly getting busier. Here’s what’s on the near horizon for you.

Mobile app. YESSSS. This is a real, actual thing that is going to be in your hands in no time. I have it on my phone, and it’s stellar.

It’s breathtakingly beautiful, so simple and intuitive.
You’ll be closer to your fans than ever. This app will support rich interaction between patrons and creators. Without patrons, what would Patreon be?
Oh, and we may or may not have some (INSANELY COOL) surprises features built in. Who knows. (We do.)

New Creator Page. We’ve geared our newly-polished creator page to maximize the support you get. We aim for this page to be simple, special, and useful for both your current patrons and potential new patrons.

Payments updates.

VAT: Due to a recent change in regulation, online businesses (and independent creators) are responsible for paying tax based on the location of EU-based consumers. Instead of making creators like you responsible for collecting and paying VAT, we’re taking care of it automatically for your EU-based patrons, and you won’t have to do a thing!
Patronage Accuracy: We completely hear your frustration with your public-facing pledge amount being higher than your actual payout, and we’re gladly fixing that. In our next round of updates, we’ll make sure fans visiting your page see a dollar amount much more reflective of what you’re actually being paid.

And this is just the beginning—we’ve got so much more up our sleeves for the rest of 2015. Busy bees, but we love it. Our team is so jazzed to transform your experience on Patreon for the better. I know we haven’t done it perfectly, and I’m so sorry for any trouble this has caused you. But I can assure you that we hear every one of your feature requests, complaints, thoughts; and we take them so seriously. Thank you for your patience. You’re a complete rockstar.

Cheers to a better Patreon!
Taryn Arnold
Community Happiness

Oh, and if ya need me call me, no matter where ya are, no matter how far, don’t worry baby! (Actually, we don’t have a support number, but feel free to send us a note here!)
"""

# language=MySQL
_sql = """
SELECT
  users.FName, users.LName, users.Email
FROM
  tblUsers AS users
JOIN
  campaigns_users
  ON (users.UID = campaigns_users.user_id)
JOIN user_settings
  ON (users.UID = user_settings.user_id)
WHERE
  user_settings.email_patreon = 1
AND
  users.Email IS NOT NULL
ORDER BY
  users.UID
LIMIT 1
OFFSET 0
;
"""


def recipients_function():
    return sql_to_recipient_list(_sql)


template = MassMessage(
    _subject, email_target.TARYN, _html, _text, recipients_function
)
