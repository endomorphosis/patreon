from patreon.services.mail.model import email_target
from patreon.services.mail.patreon_message import MassMessage
from patreon.services.mail.util import sql_to_recipient_list

_subject = "Special Announcement - Patreon and Subbable are joining forces!"

# language=HTML
_html = """
<div class="inner-text-content">
    <p>
        Creators and patrons,
    </p>
    <p>
        I&rsquo;m super excited to share some big news with you today!
    </p>
    <p>
        About a year and half ago, Hank and John Green (you may know them as the
         Vlogbrothers on YouTube) launched a subscription funding site called
         Subbable, enabling fans to pay creators on a regular basis.
    </p>
    <p>
        I&rsquo;ve been talking with Hank since before Subbable even launched
        about the possibility of working together, and today I&rsquo;m jazzed to
        announce that <strong>Patreon and Subbable are officially joining
        forces.</strong> WOOT!
    </p>
    <p>
        This means that Subbable&rsquo;s 24 creators will be launching on
        Patreon! Check them out here:
        <br/>
        <a href="http://www.patreon.com/subbable">
            http://www.patreon.com/subbable
        </a>
    </p>
    <p>
        And for more details about the Subbable deal, check out my blog post
        here:
        <br/>
        <a href="http://www.patreon.com/creation?hid=1886807">
            http://www.patreon.com/creation?hid=1886807
        </a>
    </p>
    <p>
        Patrons are now collectively sending over $2 million dollars per month
        to Patreon creators.  This is no small feat.  It&rsquo;s a revolution.
        It&rsquo;s a return to the classical model of patronage, with more reach
        and power than ever before.  And it&rsquo;s working, because of you.
    </p>
    <p>
        ROCK N ROLL.
    </p>
    <p>
        Jack Conte
        <br/>
        CEO and cofounder @ Patreon
    </p>
</div>
"""

_text = """
Creators and Patrons,

I’m super excited to share some big news with you today!

About a year and half ago, Hank and John Green (you may know them as the Vlogbrothers on YouTube) launched a subscription funding site called Subbable, enabling fans to pay creators on a regular basis.

I’ve been talking with Hank since before Subbable even launched about the possibility of working together, and today I’m jazzed to announce that Patreon and Subbable are officially joining forces.  WOOT!

This means that Subbable’s 24 creators will be launching on Patreon!  Check them out here:
http://www.patreon.com/subbable

And for more details about the Subbable deal, check out my blog post here:
http://www.patreon.com/creation?hid=1886807

Patrons are now collectively sending over $2 million dollars per month to Patreon creators.  This is no small feat.  It’s a revolution.  It’s a return to the classical model of patronage, with more reach and power than ever before.  And it’s working, because of you.

ROCK N ROLL.

Jack Conte
CEO and cofounder @ Patreon
"""

# language=MySQL
_sql = """
SELECT
  users.FName, users.LName, users.Email
FROM
  tblUsers AS users
LEFT JOIN
  user_settings settings
  ON (users.UID = settings.user_id)
WHERE
  settings.email_patreon = 1
AND
  users.Email IS NOT NULL
;
"""

def recipients_function():
    return sql_to_recipient_list(_sql)


template = MassMessage(
    _subject, email_target.JACK_NOREPLY, _html, _text, recipients_function
)
