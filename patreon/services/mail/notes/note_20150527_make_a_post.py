from patreon.services.mail.model import email_target
from patreon.services.mail.patreon_message import MassMessage
from services.mail.util import sql_to_recipient_list

_subject = "Feature Update! Our New Posting Tool!"

# language=HTML
_html = """
<div class="inner-text-content">
  <h2>Feature Update! Our New Posting Tool!</h2>
  <p>Dear creators,</p>
  <p>You give a whole lot of light to the world. You deserve a worthy outlet. When we do our job right, tech makes it not only possible but easy for you to get your work out there and shine.</p>
  <p>Introducing our new posting tool: a clean, integral way for you to share your work with fans and patrons on Patreon. The new posting tool includes:</p>
  <ul>
      <li> <b>Drafts:</b> Create something to publish later! No reason to rush or do it all at once. </li>
      <li> <b>Rich text:</b> Write and publish rich text with more options, personality, and visual clarity.</li>
      <li> <b>Customized thumbnails:</b> Choose how your work will show up in search results and emails with unique images. </li>
      <li> <b>Intuitive interface:</b> No more guess work. What you see is what you get, from create to share.</li>
      <li> <b>Clarity on charging:</b> See of how much patrons have pledged, and whether you would like to charge for your post</li>
  </ul>
  <br>
  <img width="100%" src="https://gallery.mailchimp.com/4f580ef4690098f756156d7d8/images/f0f714b4-c56e-4946-8313-d2533076c495.png" style="display: inline-block;">
  <br><br>
  <p>We know creativity calls for evolution.  Our new posting tool is the first of many updates to product at Patreon. Think useful tools, unusual care, and tech that meets your needs.</p>
  <p>Try it out! Be radiant. There's no time like the present to create, post, and get paid for doing your thing.</p>
  <p>Love,<br>Patreon</p>
  <p>P.S. you can learn more about this new tool at our <a href="https://patreon.zendesk.com/hc/en-us/sections/200911469-Making-a-post">Knowledge Base</a></p>
</div>

"""

_text = """
Feature Update! Our New Posting Tool!

Dear creators,

You give a whole lot of light to the world. You deserve a worthy outlet. When we do our job right, tech makes it not only possible but easy for you to get your work out there and shine.

Introducing our new posting tool: a clean, integral way for you to share your work with fans and patrons on Patreon. The new posting tool includes:

Drafts: Create something to publish later! No reason to rush or do it all at once.
Rich text: Write and publish rich text with more options, personality, and visual clarity.
Customized thumbnails: Choose how your work will show up in search results and emails with unique images.
Intuitive interface: No more guess work. What you see is what you get, from create to share.
Clarity on charging: See of how much patrons have pledged, and whether you would like to charge for your post

We know creativity calls for evolution. Our new posting tool is the first of many updates to product at Patreon. Think useful tools, unusual care, and tech that meets your needs.

Try it out! Be radiant. There's no time like the present to create, post, and get paid for doing your thing.

Love,
Patreon

P.S. you can learn more about this new tool at our Knowledge Base
"""

# language=MySQL
_sql = """
SELECT
  users.FName, users.LName, users.Email
FROM
  tblUsers AS users
JOIN
  campaigns_users
  ON (users.UID = campaigns_users.user_id)
JOIN user_settings
  ON (users.UID = user_settings.user_id)
WHERE
  user_settings.email_patreon = 1
AND
  users.Email IS NOT NULL
ORDER BY
  users.UID
LIMIT 1
OFFSET 0
;
"""

def recipients_function():
    return sql_to_recipient_list(_sql)

template = MassMessage(
  _subject, email_target.NOREPLY, _html, _text, recipients_function
)
