*|GREETING|*

*|CREATOR_NAME|* reached their goal of *|GOAL_AMOUNT|* -- *|GOAL_TITLE|*

Thank you! Your pledge helped contribute to reaching this goal -- congratulate *|CREATOR_NAME|* and the community by posting a note!
*|CREATOR_FEED_URL|*

Best,
Patreon Team

p.s. We love hearing from you! Any questions or suggestions, please reach us at hello@patreon.com.
