class MandrillMergeVar():
    def __init__(self, merge_var, value):
        self.merge_var = merge_var
        self.value = str(value)

    def as_dict(self):
        return {
            "name": self.merge_var.value,
            "content": self.value
        }


class MandrillMergeVars():
    def __init__(self, merge_vars=None):
        self.merge_vars = merge_vars or []

    def add_merge_var(self, merge_var, value):
        self.merge_vars.append(MandrillMergeVar(merge_var, value))

    def as_dict(self):
        return [ var.as_dict() for var in self.merge_vars ]


class MandrillCustomMergeVars():
    def __init__(self, email, merge_vars=None):
        merge_vars = merge_vars or MandrillMergeVars()
        self.email = email
        self.merge_vars = merge_vars

    def add_merge_var(self, merge_var, value):
        self.merge_vars.add_merge_var(merge_var, value)

    def as_dict(self):
        return {
            "rcpt": self.email,
            "vars": self.merge_vars.as_dict()
        }
