from patreon.services.mail.mandrill_merge_vars import MandrillMergeVar, \
    MandrillMergeVars, MandrillCustomMergeVars
from patreon.services.mail.model import MergeVar, Greeting, MandrillTemplate


class PatreonMessage(object):
    def __init__(self, subject, from_, recipient_list):
        self.subject = subject
        self.from_ = from_
        self.recipient_list = recipient_list


class TemplateMessage(PatreonMessage):
    def __init__(self, template, subject, from_, recipient_list,
                 change_settings=True, merge_language='mailchimp'):
        super().__init__(subject, from_, recipient_list)
        self.template_name = template.value
        self.merge_vars = MandrillMergeVars()
        self.change_settings = change_settings
        self.custom_merge_vars = {
            recipient['email']: MandrillCustomMergeVars(
                recipient['email'],
                MandrillMergeVars([
                    MandrillMergeVar(
                        MergeVar.RECIPIENT_EMAIL,
                        recipient['email']
                    ),
                    MandrillMergeVar(
                        MergeVar.GREETING,
                        Greeting(recipient['name'])
                    )
                ])
            )
            for recipient in recipient_list
        }
        self.merge_language = merge_language

    def add_merge_var(self, merge_var, value):
        self.merge_vars.add_merge_var(merge_var, value)

    def add_custom_merge_var(self, recipient_email, merge_var, value):
        self.custom_merge_vars[recipient_email].add_merge_var(merge_var, value)


class MassMessage(TemplateMessage):
    def __init__(self, subject, from_, content_html, content_text,
                 recipient_function):
        recipient_list = recipient_function()
        super().__init__(MandrillTemplate.NOTE, subject, from_, recipient_list)
        self.content_html = content_html
        self.content_text = content_text


class HandlebarsMessage(TemplateMessage):
    def __init__(self, template, subject, from_, recipient_list,
                 change_settings=True):
        super().__init__(
            template, subject, from_, recipient_list, change_settings
        )
