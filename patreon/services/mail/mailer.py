import json
import rollbar
from mandrill import Mandrill, Error
from pybars import Compiler

from patreon.services.mail.mandrill_merge_vars import MandrillCustomMergeVars, \
    MandrillMergeVars
from patreon.services.mail_file_writer import write_txt_to_file, \
    write_html_to_file


def combine_merge_vars(merge_vars, custom_merge_vars):
    global_vars = merge_vars.merge_vars
    custom_vars = custom_merge_vars.merge_vars.merge_vars
    return MandrillMergeVars(global_vars + custom_vars).as_dict()


def update_template(mandrill, template_name, html, text):
    try:
        return mandrill.templates.update(
            name=template_name,
            code=html,
            text=text
        )
    except Error as e:
        # Mandrill errors are thrown as exceptions
        rollbar.report_message('A mandrill error occurred: ' + str(e))


class Mailer(object):
    def __init__(self, api_key):
        self.api_key = api_key

    def send_template_email(self, template):
        pass

    def update_template(self, template_name, html, text):
        pass


class StubMailer(Mailer):
    def send_template_email(self, template):
        i = 0
        count = len(template.recipient_list)
        for recipient in template.recipient_list:
            message = {
                'template_name': template.template_name,
                'to': recipient,
                'subject': template.subject,
                'from_email': template.from_.email,
                'from_name': template.from_.name,
                'global_merge_vars': template.merge_vars.as_dict(),
            }

            if count > 0 and count == len(template.custom_merge_vars):
                message['merge_vars'] = list(template.custom_merge_vars.keys())[i]
                i += 1

            json_content = json.dumps(message, sort_keys=True, indent=4)
            write_txt_to_file(recipient['email'], template.subject, json_content)

    def update_template(self, template_name, html, text):
        return html


class MandrillMailer(Mailer):
    def send_template_email(self, template):
        try:
            mandrill = Mandrill(self.api_key)
            message = {
                'subject': template.subject,
                'from_email': template.from_.email,
                'from_name': template.from_.name,
                'to': template.recipient_list,
                'track_opens': True,
                'track_clicks': True,
                'inline_css': None,
                'preserve_recipients': False,
                'global_merge_vars': template.merge_vars.as_dict(),
                'merge_vars': [
                    merge_vars.as_dict()
                    for merge_vars in template.custom_merge_vars.values()
                    ],
                'merge_language': template.merge_language
            }
            async = True  # TODO MARCOS maybe async False for singletons?

            return mandrill.messages.send_template(
                template.template_name, [], message, async
            )
        except Error as e:
            # Mandrill errors are thrown as exceptions
            rollbar.report_message('A mandrill error occurred: ' + str(e))
            # A mandrill error occurred: Mandrill_Unknown_Subaccount - No subaccount exists with the id 'customer-123'

    def update_template(self, template_name, html, text):
        mandrill = Mandrill(self.api_key)
        return update_template(mandrill, template_name, html, text)


class FauxMandrillMailer(Mailer):
    """
    This is a class used exclusively for testing who's job is to, given a template with one or more recipients:

    1. Take the custom merge vars from the first recipient
    2. Add them to the "_global" merge variables
    3. Render the email as html
    4. Return a string containing the html that was rendered

    It will NOT handle multiple recipients beyond the stated behavior.
    """

    def send_template_email(self, template):
        try:
            mandrill = Mandrill(self.api_key)

            custom_merge_vars = MandrillCustomMergeVars("", MandrillMergeVars())
            if len(template.custom_merge_vars) > 0:
                key = list(template.custom_merge_vars.keys())[0]
                custom_merge_vars = template.custom_merge_vars[key]

            merge_vars = combine_merge_vars(template.merge_vars, custom_merge_vars)

            if template.merge_language == 'handlebars':
                result = self.render_handlebars(template, merge_vars)
            else:
                result = mandrill.templates.render(template.template_name, [], merge_vars)

            for recipient in template.recipient_list:
                write_html_to_file(
                    recipient['email'],
                    template.subject,
                    result['html']
                )

            return result

        except Error as e:
            # Mandrill errors are thrown as exceptions
            rollbar.report_message('A mandrill error occurred: ' + str(e))
            # A mandrill error occurred: Mandrill_Unknown_Subaccount - No subaccount exists with the id 'customer-123'
            raise e

    def render_handlebars(self, mail_template, merge_vars):
        from patreon.services.mail.util import combined_html_for_template
        source = combined_html_for_template(mail_template)
        handlebars_template = Compiler().compile(source)
        handlebars_vars = {}
        for merge_var in merge_vars:
            handlebars_vars[merge_var['name']] = merge_var['content']
        return {'html': handlebars_template(handlebars_vars)}

    def update_template(self, template_name, html, text):
        mandrill = Mandrill(self.api_key)
        return update_template(mandrill, template_name, html, text)
