from patreon.services.mail.model import MandrillTemplate, MergeVar
from patreon.services.mail.model.email_target import BINGO
from patreon.services.mail.patreon_message import TemplateMessage
from patreon.services.mail.util import user_to_recipient_list


class Paid(TemplateMessage):
    def __init__(self, recipient, amount, date):
        super().__init__(
            MandrillTemplate.PAID,
            amount + ' was just sent to you from Patreon!',
            BINGO,
            user_to_recipient_list(recipient)
        )

        self.add_merge_var(MergeVar.DATE, date)
        self.add_merge_var(MergeVar.PLEDGE_TOTAL, amount)
