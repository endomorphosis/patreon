from patreon.services.mail.model import MandrillTemplate, MergeVar
from patreon.services.mail.model.email_target import REGISTRATION
from patreon.services.mail.patreon_message import TemplateMessage


class Register(TemplateMessage):
    def __init__(self, email_address, first_name, last_name, activation_link):
        recipient = {
            "email": email_address,
            "name": first_name + " " + last_name if last_name else first_name,
            "type": "to"
        }
        super().__init__(
            MandrillTemplate.REGISTER,
            "Patreon Confirmation",
            REGISTRATION,
            [recipient]
        )
        self.add_merge_var(MergeVar.FIRST_NAME, first_name)
        self.add_merge_var(MergeVar.ACTIVATION_LINK, activation_link)
