from urllib.parse import urlencode

from patreon.constants import image_sizes
from patreon.model.type import url_type
from patreon.services.mail.model import MandrillTemplate, MergeVar
from patreon.services.mail.model.email_target import BINGO
from patreon.services.mail.patreon_message import TemplateMessage
from patreon.services.mail.util import users_to_recipient_list


class NotifyNewPost(TemplateMessage):
    def __init__(self, sender, activity, recipients):
        if activity.activity_title:
            subject = sender.full_name + ' posted "' + activity.activity_title + '"'
        else:
            subject = 'New post by ' + sender.full_name

        super().__init__(
            MandrillTemplate.NOTIFY_NEW_POST,
            subject,
            BINGO,
            users_to_recipient_list(recipients)
        )

        creator_image = url_type.https_string(sender.thumb_url)
        alt_text = activity.activity_title or 'Post Image'

        # Conditional variables:
        url = activity.photo_url_size(image_sizes.POST_MEDIUM) \
            or activity.thumbnail_url_size(image_sizes.POST_THUMB_LARGE)
        if url:
            self.add_merge_var(MergeVar.CREATION_IMG, url)

        if activity.activity_title:
            self.add_merge_var(MergeVar.CREATION_TITLE, activity.activity_title)

        if activity.activity_content:
            self.add_merge_var(MergeVar.CONTENT, activity.activity_content)

        if activity.is_paid:
            self.add_merge_var(MergeVar.IS_PAID, True)

        self.add_merge_var(MergeVar.CREATOR_NAME, sender.full_name)
        self.add_merge_var(MergeVar.CREATOR_URL, sender.canonical_url())
        self.add_merge_var(MergeVar.CREATOR_IMG, creator_image)

        self.add_merge_var(MergeVar.ALT_TEXT, alt_text)
        self.add_merge_var(MergeVar.CONTENT, activity.activity_content)

        for recipient in recipients:
            params = urlencode({ 'login': recipient.email })
            post_url = activity.canonical_url(params)
            self.add_custom_merge_var(recipient.email, MergeVar.CREATION_URL, post_url)
