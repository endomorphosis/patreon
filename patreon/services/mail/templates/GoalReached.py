from patreon.services.mail.model import MandrillTemplate, MergeVar
from patreon.services.mail.model.email_target import BINGO
from patreon.services.mail.patreon_message import TemplateMessage
from patreon.services.mail.util import users_to_recipient_list
from patreon.util.format import cents_as_dollar_string


class GoalReached(TemplateMessage):
    def __init__(self, recipients, creator, goal_amount, goal_title):
        goal_dollar_string = cents_as_dollar_string(goal_amount)

        super().__init__(
            MandrillTemplate.GOAL_REACHED,
            '{name} reached their goal of {dollars}!'.format(
                name=creator.full_name,
                dollars=goal_dollar_string
            ),
            BINGO,
            users_to_recipient_list(recipients)
        )

        self.add_merge_var(MergeVar.CREATOR_URL, creator.canonical_url())
        self.add_merge_var(MergeVar.CREATOR_FEED_URL, creator.canonical_url('ty=a'))
        self.add_merge_var(MergeVar.CREATOR_IMG, creator.image_url)
        self.add_merge_var(MergeVar.CREATOR_NAME, creator.full_name)

        self.add_merge_var(MergeVar.GOAL_AMOUNT, goal_dollar_string)
        self.add_merge_var(MergeVar.GOAL_TITLE, goal_title)
