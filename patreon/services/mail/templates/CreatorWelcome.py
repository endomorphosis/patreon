from patreon.services.mail.model import MandrillTemplate, MergeVar
from patreon.services.mail.model.email_target import NOREPLY
from patreon.services.mail.patreon_message import TemplateMessage
from patreon.services.mail.util import user_to_recipient_list

class CreatorWelcome(TemplateMessage):
    def __init__(self, recipient):
        super().__init__(
            MandrillTemplate.CREATOR_WELCOME,
            "Welcome to Patreon — 2 Quick Things!",
            NOREPLY,
            user_to_recipient_list(recipient)
        )

        self.add_merge_var(MergeVar.CREATOR_NAME, recipient.full_name)
