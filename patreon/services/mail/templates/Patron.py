from patreon.services.mail.model import MandrillTemplate, MergeVar
from patreon.services.mail.model.email_target import BINGO
from patreon.services.mail.patreon_message import TemplateMessage
from patreon.services.mail.util import user_to_recipient_list
from patreon.util import format


class Patron(TemplateMessage):
    def __init__(self, creator, patron, pledge_cents, old_pledge_cents=None):
        dollar_string = format.cents_as_dollar_string(pledge_cents)

        if old_pledge_cents:
            subject = "{name} just edited their pledge to {amount}."
        else:
            subject = "{name} just pledged {amount}!"
        subject = subject.format(name=patron.full_name, amount=dollar_string)

        super().__init__(
            MandrillTemplate.PATRON,
            subject,
            BINGO,
            user_to_recipient_list(creator)
        )

        self.add_merge_var(MergeVar.PATRON_NAME, patron.full_name)
        self.add_merge_var(MergeVar.PATRON_IMAGE, patron.thumbnail_url or patron.thumb_url)
        self.add_merge_var(MergeVar.PATRON_URL, patron.canonical_url())
        self.add_merge_var(MergeVar.PLEDGE_AMOUNT, dollar_string)
        if old_pledge_cents:
            old_dollar_string = format.cents_as_dollar_string(old_pledge_cents)
            self.add_merge_var(MergeVar.OLD_PLEDGE_AMOUNT, old_dollar_string)
