import patreon
from patreon.services.mail.model import MandrillTemplate, MergeVar
from patreon.services.mail.model.email_target import PASSWORD
from patreon.services.mail.patreon_message import TemplateMessage
from patreon.services.mail.util import user_to_recipient_list


class PasswordReset(TemplateMessage):
    def __init__(self, user, token):
        super().__init__(
            MandrillTemplate.PASSWORD_RESET,
            'Patreon Password Reset',
            PASSWORD,
            user_to_recipient_list(user),
            False
        )

        link = 'https://' + patreon.config.main_server + '/forgetPassReset?u=' \
               + str(user.user_id) + '&sec=' + token

        self.add_merge_var(MergeVar.VALIDATION_URL, link)
