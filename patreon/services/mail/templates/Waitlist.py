from patreon.services.mail.model import MandrillTemplate, MergeVar
from patreon.services.mail.model.email_target import BINGO
from patreon.services.mail.patreon_message import TemplateMessage
from patreon.services.mail.util import user_to_recipient_list


class Waitlist(TemplateMessage):
    def __init__(self, recipient, creator, reward_level):
        super().__init__(
            MandrillTemplate.WAITLIST,
            creator.full_name + '\'s ' + reward_level + ' reward just opened up!',
            BINGO,
            user_to_recipient_list(recipient)
        )

        self.add_merge_var(MergeVar.CREATOR_NAME, creator.full_name)
        self.add_merge_var(MergeVar.CREATOR_URL, creator.canonical_url())
        self.add_merge_var(MergeVar.CREATOR_IMG, creator.image_url)
        self.add_merge_var(MergeVar.REWARD_LEVEL, reward_level)
