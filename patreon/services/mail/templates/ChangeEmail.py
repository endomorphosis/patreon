from patreon.services.mail.model import MandrillTemplate, MergeVar
from patreon.services.mail.model.email_target import SUPPORT
from patreon.services.mail.patreon_message import TemplateMessage


class ChangeEmail(TemplateMessage):
    def __init__(self, recipient, new_email, validation_url):
        recipient_array = {
            "email": new_email,
            "name": recipient.full_name,
            "type": "to"
        }
        super().__init__(
            MandrillTemplate.CHANGE_EMAIL,
            "Patreon Email Change",
            SUPPORT,
            [recipient_array]
        )

        self.add_merge_var(MergeVar.VALIDATION_URL, validation_url)
