import patreon
from patreon.services.mail.model import MandrillTemplate, MergeVar
from patreon.services.mail.model.email_target import BINGO
from patreon.services.mail.patreon_message import TemplateMessage
from patreon.services.mail.util import user_to_recipient_list
from patreon.util.format import cents_as_dollar_string


class Receipt(TemplateMessage):
    def __init__(self, recipient, creator, campaign, pledge, is_edit=False,
                 is_verified=False):
        subject = "Here\'s your confirmation for {edit}{name}.".format(
            edit="editing your pledge to " if is_edit else '',
            name=creator.full_name
        )

        super().__init__(
            MandrillTemplate.RECEIPT,
            subject,
            BINGO,
            user_to_recipient_list(recipient)
        )

        link = 'https://{host}/bePatronConfirm?u={creator_id}'.format(
            host=patreon.config.main_server,
            creator_id=creator.user_id
        )

        pledge_amount = cents_as_dollar_string(pledge.amount)

        if is_verified:
            self.add_merge_var(MergeVar.VERIFIED, True)

        if campaign.is_monthly:
            pledge_basis = "month for {}".format(campaign.pay_per_name)
            self.add_merge_var(MergeVar.IS_MONTHLY, True)
        else:
            pledge_basis = "{} created".format(campaign.pay_per_name)
            if pledge.pledge_cap:
                pledge_cap = cents_as_dollar_string(pledge.pledge_cap)
                self.add_merge_var(MergeVar.PLEDGE_CAP, pledge_cap)

        self.add_merge_var(MergeVar.PATRON_NAME, recipient.full_name)
        self.add_merge_var(MergeVar.CREATOR_NAME, creator.full_name)
        self.add_merge_var(MergeVar.PLEDGE_AMOUNT, pledge_amount)
        self.add_merge_var(MergeVar.PLEDGE_BASIS, pledge_basis)
        self.add_merge_var(MergeVar.PLEDGE_CONFIRMATION_URL, link)
