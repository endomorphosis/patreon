from patreon.services.mail.model import MandrillTemplate, MergeVar
from patreon.services.mail.model.email_target import SUPPORT
from patreon.services.mail.patreon_message import TemplateMessage


class EmailChanged(TemplateMessage):
    def __init__(self, recipient, old_email, new_email):
        recipient = {
            "email": old_email,
            "name": recipient.full_name,
            "type": "to"
        }
        super().__init__(
            MandrillTemplate.EMAIL_CHANGED,
            "Patreon Email Change",
            SUPPORT,
            [recipient]
        )

        self.add_merge_var(MergeVar.NEW_EMAIL, new_email)
