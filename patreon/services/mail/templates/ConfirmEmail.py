from patreon.services.mail.model import MandrillTemplate, MergeVar
from patreon.services.mail.model.email_target import SUPPORT
from patreon.services.mail.patreon_message import TemplateMessage
from patreon.services.mail.util import user_to_recipient_list


class ConfirmEmail(TemplateMessage):
    def __init__(self, recipient, confirm_url):
        super().__init__(
            MandrillTemplate.CONFIRM_EMAIL,
            "Patreon Email Confirmation",
            SUPPORT,
            user_to_recipient_list(recipient)
        )

        self.add_merge_var(MergeVar.MESSAGE_URL, confirm_url)
