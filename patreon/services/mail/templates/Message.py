from patreon import globalvars
from patreon.model.type import url_type
from patreon.services.mail.model import MandrillTemplate, MergeVar
from patreon.services.mail.model.email_target import MESSAGE
from patreon.services.mail.patreon_message import TemplateMessage
from patreon.services.mail.util import users_to_recipient_list


class Message(TemplateMessage):
    def __init__(self, sender, message, recipients):
        super().__init__(
            MandrillTemplate.MESSAGE,
            sender.full_name + ' sent you a message.',
            MESSAGE,
            users_to_recipient_list(recipients),
            False
        )

        if sender.thumb_url:
            sender_image = url_type.https_string(sender.thumb_url)
        else:
            sender_image = "https://www.patreon.com" + globalvars.get('IMG_PROFILE_DEFAULT')

        self.add_merge_var(MergeVar.SENDER_NAME, sender.full_name)
        self.add_merge_var(MergeVar.SENDER_IMG, sender_image)
        self.add_merge_var(MergeVar.SENDER_URL, sender.canonical_url())
        self.add_merge_var(MergeVar.MESSAGE, message)

        for recipient in recipients:
            self.add_custom_merge_var(recipient.email, MergeVar.MESSAGE_URL, sender.messages_url())
