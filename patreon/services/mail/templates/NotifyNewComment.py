from patreon.services.mail.model import MandrillTemplate, MergeVar
from patreon.services.mail.model.email_target import BINGO
from patreon.services.mail.patreon_message import TemplateMessage
from patreon.services.mail.util import users_to_recipient_list
from patreon.model.type import url_type


class NotifyNewComment(TemplateMessage):
    def __init__(self, recipient, creator, commenter, comment, is_reply):
        # is_reply is denormalized Comment.get(comment.thread_id).commenter_id == recipient.user_id
        own_feed = creator.user_id == recipient.user_id
        if is_reply:
            if own_feed:
                subject = commenter.full_name + ' replied to your comment.'
            else:
                subject = commenter.full_name + ' replied to your comment in ' + creator.full_name + '\'s Patron Feed.'
        else:
            if own_feed:
                subject = commenter.full_name + ' commented on your post.'
            else:
                subject = commenter.full_name + ' commented on your post in ' + creator.full_name + '\'s Patron Feed.'

        super().__init__(
            MandrillTemplate.NOTIFY_NEW_COMMENT,
            subject,
            BINGO,
            users_to_recipient_list([recipient]),
            merge_language='handlebars'
        )

        self.add_merge_var(MergeVar.SENDER_NAME, commenter.full_name)
        commenter_image = url_type.https_string(commenter.thumb_url)
        self.add_merge_var(MergeVar.SENDER_IMG, commenter_image)
        self.add_merge_var(MergeVar.SENDER_URL, commenter.canonical_url())

        self.add_merge_var(MergeVar.COMMENT_URL, comment.canonical_url())
        self.add_merge_var(MergeVar.COMMENT_TEXT, comment.comment_text)

        self.add_merge_var(MergeVar.CREATOR_NAME, creator.full_name)

        self.add_merge_var(MergeVar.IS_REPLY, is_reply if is_reply else '')
        self.add_merge_var(MergeVar.OWN_FEED, own_feed if own_feed else '')
