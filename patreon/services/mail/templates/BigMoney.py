from patreon.services.mail.model import MandrillTemplate
from patreon.services.mail.model.email_target import HELLO
from patreon.services.mail.patreon_message import TemplateMessage
from patreon.services.mail.util import user_to_recipient_list


class BigMoney(TemplateMessage):
    def __init__(self, recipient):
        super().__init__(
            MandrillTemplate.BIGMONEY,
            "Let me know if there's anything I can do to help!",
            HELLO,
            user_to_recipient_list(recipient)
        )
