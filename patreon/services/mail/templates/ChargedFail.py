from patreon.services.mail.model import MandrillTemplate, MergeVar
from patreon.services.mail.model.email_target import BINGO
from patreon.services.mail.patreon_message import TemplateMessage
from patreon.services.mail.util import user_to_recipient_list


class ChargedFail(TemplateMessage):
    def __init__(self, recipient, pending_link):
        super().__init__(
            MandrillTemplate.CHARGED_FAIL,
            recipient.first_name + ', we ran into an issue trying to process your pledge on Patreon',
            BINGO,
            user_to_recipient_list(recipient)
        )

        self.add_merge_var(MergeVar.SUPPORTED_CREATIONS, pending_link)
