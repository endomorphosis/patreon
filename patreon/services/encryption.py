import rsa
import base64
from os import path

import patreon
from patreon.constants.encryption_contants import MAX_DATA_SIZE
from patreon.exception.encryption_errors import InvalidEncryptData, \
    InvalidPrivateKey, InvalidPublicKey, DecryptionFailure, InvalidDecryptData, \
    EncryptionError


public_key = None
private_key = None


def initialize():
    if patreon.config.private_key_path:
        load_private_key_from_file(patreon.config.private_key_path)

    load_public_key_from_file(path.join(path.dirname(patreon.constants.__file__) + '/taxForm_rsa.pub'))


def clear_encryption_keys():
    global public_key, private_key

    public_key = None
    private_key = None
    initialize()


def load_public_key_from_file(path):
    try:
        global public_key

        with open(path, 'rb') as public_key_file:
            keydata = public_key_file.read()

        public_key = rsa.PublicKey.load_pkcs1(keydata)
    except ValueError as e:
        raise InvalidPublicKey(e)
    except FileNotFoundError as e:
        raise InvalidPublicKey(e)


def load_private_key_from_file(path):
    try:
        global private_key

        with open(path, 'rb') as private_key_file:
            keydata = private_key_file.read()

        private_key = rsa.PrivateKey.load_pkcs1(keydata)
    except ValueError as e:
        raise InvalidPrivateKey(e)
    except FileNotFoundError as e:
        raise InvalidPrivateKey(e)


def encrypt(data):
    if looks_encrypted(data):
        raise InvalidEncryptData('Data already encrypted?')

    data = str(data).encode('utf-8')
    if len(data) > MAX_DATA_SIZE:
        raise InvalidEncryptData('Data too large for keysize')

    try:
        return base64.encodebytes(rsa.encrypt(data, public_key)).decode('utf-8')
    except Exception as e:
        raise EncryptionError(e)


def decrypt(data):
    if not looks_encrypted(data):
        raise InvalidDecryptData("Data not encrypted?")

    data = base64.decodebytes(str(data).encode('utf-8'))
    try:
        return rsa.decrypt(data, private_key).decode('utf-8')
    except Exception as e:
        raise DecryptionFailure(e)


def looks_encrypted(value):
    try:
        data = base64.decodebytes(str(value).encode('utf-8'))
        if data:
            return True
    except:
        return False

    return False

initialize()
