def qualify_events(cls):
    domain = cls.DOMAIN
    for attr, value in dict(cls.__dict__).items():
        if not attr.startswith('__') and isinstance(value, str):
            setattr(cls, attr, '{domain} : {event}'.format(
                domain=domain,
                event=value
            ))
    return cls


@qualify_events
class CommentEvent:
    DOMAIN = 'Comment'
    ADD_EVENT = 'Add'
    EDIT_EVENT = 'Edit'
    DELETE_EVENT = 'Delete'


@qualify_events
class CreatorSettingsEvent:
    DOMAIN = 'Creator Settings'
    CATEGORY_SAVE_EVENT = 'Category Save'


@qualify_events
class LaunchEvent:
    DOMAIN = 'Launch'
    SUCCESS = 'Success'


@qualify_events
class PledgeEvent:
    DOMAIN = 'Pledge'
    ADD_EVENT = 'Add'
    EDIT_EVENT = 'Edit'
    DELETE_EVENT = 'Delete'


@qualify_events
class SignupEvent:
    DOMAIN = 'User'
    ADD_EVENT = 'Add'


@qualify_events
class UserNextEvent:
    DOMAIN = 'User Next'
    FETCH_EVENT = 'Fetch'
