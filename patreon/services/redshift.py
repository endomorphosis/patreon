import patreon
import psycopg2
import psycopg2.extras
import functools


@functools.lru_cache(maxsize=None)
def connection():
    if patreon.config.redshift is None:
        return None
    else:
        return psycopg2.connect(**patreon.config.redshift)


def query(sql):
    conn = connection()
    if conn:
        cursor = conn.cursor(cursor_factory=psycopg2.extras.RealDictCursor)
        if patreon.config.redshift_search_path:
            cursor.execute('SET search_path TO {}'.format(patreon.config.redshift_search_path))
        cursor.execute(sql)
        return cursor.fetchall()
    else:
        return None


def query_single(sql):
    result = query(sql)
    if result is None or len(result) == 0:
        return None
    else:
        return result[0]
