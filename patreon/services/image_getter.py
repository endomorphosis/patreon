import shutil
from tempfile import NamedTemporaryFile
import patreon
from patreon.util import request


class AbstractImageGetter():
    def content_type_from_url(self, image_url):
        raise NotImplementedError

    def fetch_image(self, image_url):
        raise NotImplementedError


class StubImageGetter(AbstractImageGetter):
    def content_type_from_url(self, image_url):
        return 'image/jpeg'

    def fetch_image(self, image_url):
        # TODO MARCOS this causes a circular dependency
        # filename = fixtures.file_directory + 'blank.jpg'
        filename = patreon.repo_root + '/test/fixtures/files/blank.jpg'

        with NamedTemporaryFile(suffix=".jpg", delete=False) as file:
            shutil.copy(filename, file.name)
            return file.name


class ImageGetter(AbstractImageGetter):
    def content_type_from_url(self, image_url):
        return request.get_content_type_of_url(image_url)

    def fetch_image(self, url):
        return request.retrieve_url(url)


_image_getters = {
    'StubImageGetter': StubImageGetter,
    'ImageGetter': ImageGetter
}

image_getter_type = patreon.config.image_getter
_image_getter = _image_getters[image_getter_type]()


def content_type_from_url(image_url):
    return _image_getter.content_type_from_url(image_url)


def fetch_image(image_url):
    return _image_getter.fetch_image(image_url)
