from tempfile import NamedTemporaryFile
from PIL import Image
from patreon.util.image_processing import blurrer

class Blurrer:
    def __init__(self, image_tempfile_filename, thumbnail_key,
                 destination_format='JPEG'):
        with open(image_tempfile_filename, 'rb') as file:
            try:
                mode = 'RGBA' if destination_format == 'PNG' else 'RGB'
                self._image = Image.open(file).convert(mode)
            except OSError:
                raise InvalidImageFormat(destination_format)
        self.destination_format = destination_format

    def blur(self):
        blurred_image = blurrer.blur(self._image)
        extension = '.' + self.destination_format.lower()
        with NamedTemporaryFile(suffix=extension, delete=False) as f:
            blurred_image.save(f.name, self.destination_format)
            return f.name
