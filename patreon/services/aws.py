"""
aws.py

Helper functions for accessing AWS utilities.
"""
import threading

import patreon

import boto
import boto.ec2
import boto.sqs
import boto.s3.bucket
from boto.sns import SNSConnection
from boto.regioninfo import RegionInfo, load_regions

s3 = None
connection = None


class SQS(threading.local):
    def __init__(self):
        if patreon.config.aws:
            self.connection = boto.sqs.connect_to_region(
                patreon.config.aws['region'],
                aws_access_key_id=patreon.config.aws['access_key'],
                aws_secret_access_key=patreon.config.aws['secret_key'],
            )
        else:
            self.connection = None

    def get_queue(self, queue_name):
        if not self.connection:
            raise Exception("AWS SQS not configured, update patreon/config.py")
        return self.connection.get_queue(queue_name)

    def get(self, queue_name, number_messages=1):
        if not self.connection:
            return None

        queue = self.get_queue(queue_name)
        if not queue:
            return None

        messages = self.connection.receive_message(queue, number_messages=number_messages)
        if not messages:
            return None
        if number_messages == 1:
            message = messages[0]
            self.connection.delete_message(queue, message)
            return patreon.util.unsorted.jsdecode(message.get_body())
        else:
            message_return = []
            for message in messages:
                self.connection.delete_message(queue, message)
                message_return.append(patreon.util.unsorted.jsdecode(message.get_body()))
            return message_return

    def put(self, queue_name, message):
        queue = self.get_queue(queue_name)
        return self.connection.send_message(queue, message)


class SafeSQS(threading.local):
    def __init__(self, queue_name):

        self.message = None
        self.queue_name = None

        if patreon.config.aws:
            self.connection = boto.sqs.connect_to_region(
                patreon.config.aws['region'],
                aws_access_key_id=patreon.config.aws['access_key'],
                aws_secret_access_key=patreon.config.aws['secret_key'],
            )
            self.queue_name = queue_name
            self.queue = self.connection.get_queue(self.queue_name)
        else:
            self.connection = None

    def get(self):
        if self.message:
            return self.message

        if not self.queue:
            # raise exception
            pass

        message = self.connection.receive_message(self.queue, number_messages=1)

        if not message:
            return None

        self.message = message[0]
        return patreon.util.unsorted.jsdecode(self.message.get_body())

    def delete_message(self):
        self.connection.delete_message(self.queue, self.message)

    def requeue(self):
        return self.put(self.message)

    def put(self, message):
        return self.connection.send_message(self.queue, message)


class StubSQS(object):
    def __init__(self):
        pass

    def get_queue(self, queue_name):
        return "stub queue " + queue_name

    def get(self, queue_name, number_messages=1):
        body = {'mock message': queue_name}
        if number_messages == 1:
            return body
        else:
            return [body] * number_messages

    def put(self, queue_name, message):
        return 'sent {0} to {1}'.format(message, queue_name)


_queuers = {
    'SQS': SQS,
    'StubSQS': StubSQS
}

sqs = _queuers[patreon.config.queuer]()


class S3Bucket(threading.local):
    def __init__(self, name, region=patreon.config.aws['region']):
        if not patreon.config.aws:
            raise ValueError("Invalid configuration options for S3Bucket (see config.py)")

        connection = boto.s3.connect_to_region(
            region,
            aws_access_key_id=patreon.config.aws['access_key'],
            aws_secret_access_key=patreon.config.aws['secret_key'],
        )
        self.bucket = boto.s3.bucket.Bucket(connection, name)

    def get(self, key):
        s3key = self.bucket.get_key(key)
        return s3key.get_contents_as_string()

    def delete_many(self, key_list):
        return self.bucket.delete_keys(key_list)

    def delete(self, key):
        return self.bucket.delete_key(key)

    def exists(self, key):
        return self.bucket.get_key(key) is not None

    def set(self, key, value):
        s3key = self.bucket.new_key(key)
        s3key.set_contents_from_string(value)

    def upload(self, key, filename, headers=None):
        s3key = self.bucket.new_key(key)
        s3key.set_contents_from_filename(filename, headers)


class SNS(SNSConnection, threading.local):
    def __init__(self):
        if not patreon.config.aws:
            raise ValueError("Invalid configuration options for AWS SNS (see config.py)")
        super().__init__(
            aws_access_key_id=patreon.config.aws['access_key'],
            aws_secret_access_key=patreon.config.aws['secret_key'],
            region=self.hardcoded_region_obj()
        )

    @staticmethod
    def hardcoded_region_obj():
        region_endpoints = load_regions()['sns']
        return RegionInfo(name=SNS.hardcoded_region_str(),
                          endpoint=region_endpoints.get(SNS.hardcoded_region_str()),
                          connection_cls=SNSConnection)

    @staticmethod
    def hardcoded_region_str():
        # Ideally we'd be flexible on region here (patreon.config.aws['region']),
        # but it looks like SNS is bound to particular regions on creation
        return 'us-west-1'

    @staticmethod
    def arn_prefix():
        return "arn:aws:sns:{}:792916230465:".format(SNS.hardcoded_region_str())
