"""
PStore is designed to be a heavily read, low write storage that is shared
across all instances of Python code that is connected to it. It is updated
via a call to .update(), which is meant to be a slow process.

Usage:

pstore = PStore(db)
pstore.set('something', 'something')
pstore.get('something')
=> 'something'
pstore.update()
# If another process has updated 'something' in the meantime
pstore.get('something')
=> 'somethingelse'


Behaviors:

- If you set a key to a value, it will always be that value until you either
  update again or set another key to something else (implicit update).

- All gets are direct memory accesses and eventually consistent as long as
  updates are called on a regular basis. This can be per request or per
  task, as you will.

"""

import json

class PStore(object):

    def __init__(self, db, skip_update=False):
        self.empty()
        self._db = db
        if not skip_update:
            self.update()

    def empty(self):
        self.last_update = 0
        self._inner = {}

    def update(self, max_update=None):
        """Update the pstore up to max_update, or up to the most recent version."""
        if max_update:
            updates = self._db.query("""
                SELECT update_id, key_name, value_json FROM pstore WHERE update_id > %s AND update_id <= %s
                """, [self.last_update, max_update])
        else:
            updates = self._db.query("""
                SELECT update_id, key_name, value_json FROM pstore WHERE update_id > %s
                """, [self.last_update])

        for update in updates:
            key = update['key_name']
            value = json.loads(update['value_json'])
            self._inner[key] = value
            self.last_update = max(self.last_update, update['update_id'])

    def set(self, key, value):
        """Set a key to a particular value across all instances of this pstore."""
        value_json = json.dumps(value)

        # Force REPLACE so we get a new update_id.
        result = self._db.query("""
            REPLACE INTO pstore (key_name, value_json) VALUES (%s, %s)
            """, [key, value_json])

        # Get all the updates that happened that may or may not overwrite the
        # current value we just set.
        self.update(max_update=result.lastrowid)

        # After processing all the updates between here and the insert,
        # we know that the key won't be overwritten by any previous updates.
        # However, it might have been upserted by another process, so we have
        # to make sure to set it here.
        self._inner[key] = value

    def get(self, key, default=None):
        """Get the cached value of a particular key."""
        return self._inner.get(key, default)
