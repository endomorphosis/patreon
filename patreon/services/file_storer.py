import patreon
import os
import shutil

buckets_directory = patreon.repo_root + '/test/image_buckets/'


def drop_the_buckets():
    if os.path.isdir(buckets_directory):
        shutil.rmtree(buckets_directory)
    os.mkdir(buckets_directory)


def drop_the_bucket(bucket):
    if os.path.isdir(buckets_directory + bucket):
        shutil.rmtree(buckets_directory + bucket)
    os.mkdir(buckets_directory + bucket)


def upload_file(source_filename, destination_filename, destination_bucket):
    # todo(postpost): just recursively create directories

    bucket_name = destination_bucket.bucket
    if not os.path.isdir(buckets_directory):
        os.mkdir(buckets_directory)

    bucket_directory = buckets_directory + bucket_name + "/"
    if not os.path.isdir(bucket_directory):
        os.mkdir(bucket_directory)

    # if destination_filename has a slash, make the directory
    directory, _ = os.path.split(destination_filename)
    if directory and not os.path.isdir(bucket_directory + directory):
        os.mkdir(bucket_directory + directory)

    destination_filename = bucket_directory + "/" + destination_filename
    shutil.copy(source_filename, destination_filename)

    return destination_filename


def delete_file(bucket, key):
    path = buckets_directory + bucket.bucket + '/' + str(key)
    try:
        os.remove(path)
    except OSError:
        return False
    return True

