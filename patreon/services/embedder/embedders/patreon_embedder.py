from bs4 import BeautifulSoup
from urllib.parse import urlparse
from patreon.exception.post_errors import BadEmbed
from patreon.services import youtube
from patreon.services.embedder import EmbedlyEmbedder, image_extractor
from patreon.util import request

# This is the whitelist of domains to manually scrape for images.
# If a domain isn't behaving with embed.ly in any way, add it here.
image_embed_override_domains = [
    "dresdencodak.com",
    "nicolehanusekart.com"
]


def should_manual_override(url):
    hostname = urlparse(url).hostname
    if hostname in image_embed_override_domains:
        return request.get_simple_content_type_of_url(url) == 'text/html'
    return False


class PatreonEmbedder(EmbedlyEmbedder):
    def get_embed(self, url):
        if should_manual_override(url):
            return embed_image(url)

        # if youtube.is_youtube(url):
        #     return youtube.embed_youtube(url)

        # Fallback to embedly.
        return super().get_embed(url)


def embed_image(url):
    html = request.get_url_contents(url)
    if not html:
        raise BadEmbed(url)

    soup = BeautifulSoup(html)

    # TODO MARCOS find title by largest text?
    title = soup.title.string
    description = None
    provider_url = urlparse(url).hostname

    images = image_extractor.get_embedded_images(soup)
    image_url = images[0].get('url') if images else None

    return {
        'url': url,
        'provider_name': provider_url,
        'provider_url': provider_url,
        'title': title,
        'description': description,
        'object': {
            'html': image_url,
        },
        'image_url': image_url,
        'images': images
    }
