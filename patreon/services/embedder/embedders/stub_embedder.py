import json
import os

import patreon
from patreon.services.embedder import Embedder
from patreon.services import file_helper
from patreon.util.unsorted import underscore_url

comic_url = "http://dresdencodak.com/2015/07/08/dark-science-49-exode/"
base_directory = patreon.repo_root + "/test/fixtures/stub_embedder/cached_json/"


class StubEmbedder(Embedder):
    def get_embed(self, url):
        filename = base_directory + underscore_url(url) + ".json"
        if not os.path.isfile(filename):
            return self.get_embed(comic_url)

        data = file_helper.get_file_contents(filename)
        return json.loads(data)
