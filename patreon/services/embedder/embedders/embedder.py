class Embedder(object):
    def __init__(self, api_key=None):
        self.api_key = api_key

    def get_embed(self, url):
        raise NotImplementedError
