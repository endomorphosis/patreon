import requests
from patreon.services.embedder import Embedder

base_url = "http://api.embed.ly/1/preview"


class EmbedlyEmbedder(Embedder):
    def get_embed(self, url):
        params = { 'url': url, 'key': self.api_key }
        embedly_response = requests.get(base_url, params=params)

        if embedly_response.status_code == 200:
            return embedly_response.json()
        return None
