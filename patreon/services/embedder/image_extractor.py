import os
from PIL import Image
from patreon.services import file_helper
from patreon.util.image_processing import converter
from patreon.util.request import retrieve_url

MIN_AREA = 30000


class EmbedImage():
    def __init__(self, url, caption=None):
        local_filename = retrieve_url(url)
        image = Image.open(local_filename)
        # If we don't need the exact width and height, I can get away with
        # getting size from the headers.
        self.size = len(image.tobytes())
        self.width, self.height = image.size
        self.area = self.width * self.height if self.width and self.height else 0
        self.url = url
        self.caption = caption

        image.close()
        os.remove(local_filename)

    def to_dict(self):
        return {
            'url': self.url,
            'caption': self.caption,
            'width': self.width,
            'height': self.height,
            'size': self.size
        }


def get_embedded_images(soup_document):
    return _parsed_images(_get_images(soup_document.find_all('img')))


def _get_images(html_images):
    images = []
    for html_image in html_images:
        src = html_image.get('src')
        caption = html_image.get('alt') or html_image.get('title')

        extension = file_helper.get_extension(src)
        if converter.is_convertible_format(extension):
            images.append(EmbedImage(src, caption))

    return images


def _parsed_images(images):
    return [
        image.to_dict()
        for image in sorted(
            list(set(images)),
            key=lambda i: i.area,
            reverse=True
        )
        if image.area > MIN_AREA
    ]
