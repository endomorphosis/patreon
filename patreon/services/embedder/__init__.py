import patreon
from .embedders.embedder import Embedder
from .embedders.stub_embedder import StubEmbedder
from .embedders.embedly_embedder import EmbedlyEmbedder
from .embedders.patreon_embedder import PatreonEmbedder


_embedders = {
    'StubEmbedder': StubEmbedder,
    'EmbedlyEmbedder': EmbedlyEmbedder,
    'PatreonEmbedder': PatreonEmbedder
}

api_key = patreon.config.embedly_key
embedder_type = patreon.config.embeder
embedder = _embedders[embedder_type](api_key=api_key)


def get_embed(url):
    return embedder.get_embed(url)
