"""
services/__init__.py
"""
import threading

import patreon
import functools
from celery import Celery
from functools import wraps


if patreon.config.celery_broker:
    app = Celery('tasks', broker=patreon.config.celery_broker)

    def task(f):
        celery_task = app.task(f)

        @functools.wraps(f)
        def new_f(*args, **kwargs):
            if patreon.config.celery_broker and patreon.pstore.get('async_task_enabled'):
                celery_task.apply_async(args=args, kwargs=kwargs)
            else:
                celery_task.apply(args=args, kwargs=kwargs)

        return new_f

else:
    def task(f):
        return f


def task_with_threads(inner_f):
    @wraps(inner_f)
    def create_wrapper(*args, **kwargs):
        thread = threading.Thread(target=inner_f, args=args, kwargs=kwargs)
        thread.start()
        return thread

    return create_wrapper
