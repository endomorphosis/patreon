import patreon
import requests
import json
from os import path

FIREBASE_TOKEN = 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ2IjowLCJkIjp7InVpZCI6ImFkbWluIn0sImlhdCI6MTQ0MDk1OTgxOH0.zj1vNrTqiQs297frjUixrEZE_4OI1DkY24iwzHDJBU8'

class FirebaseError(Exception):
    pass

class Firebase(object):

    def __init__(self, url):
        self.url = url
        self._json_url = self.url.rstrip('/') + '.json?auth={}'.format(FIREBASE_TOKEN)

    def child(self, child_path):
        return Firebase(path.join(self.url, str(child_path)))

    def get(self):
        response = requests.get(self._json_url)
        if response.status_code == 200:
            return json.loads(response.text)
        else:
            raise FirebaseError("Firebase GET failed: {}".format(response.text))

    def set(self, obj):
        response = requests.put(self._json_url, data=json.dumps(obj))
        if response.status_code != 200:
            raise FirebaseError("Firebase PUT failed: {}".format(response.text))

    def append(self, obj):
        response = requests.post(self._json_url, data=json.dumps(obj))
        if response.status_code != 200:
            raise FirebaseError("Firebase POST failed: {}".format(response.text))


patreon_internal = Firebase('https://patreon-internal.firebaseio.com/{}'.format(patreon.config.stage))


