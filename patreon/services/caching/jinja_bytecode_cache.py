from jinja2 import BytecodeCache


class MemoryBytecodeCache(BytecodeCache):
    def __init__(self):
        self.store = {}

    def load_bytecode(self, bucket):
        key = bucket.key
        bytecode = self.store.get(key)
        if bytecode:
            bucket.bytecode_from_string(bytecode)

    def dump_bytecode(self, bucket):
        key = bucket.key
        self.store[key] = bucket.bytecode_to_string()
