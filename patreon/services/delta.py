from . import aws

import patreon

delta_bucket = aws.S3Bucket('patreon-webclient-assets-delta')
delta_react_bucket = aws.S3Bucket('patreon-webclient-assets-react-delta')
delta_suffix = '-web.delta.patreon.com'


def delta_webclient_prefix():
    if not patreon.config.main_server.endswith(delta_suffix):
        return None

    # Check if app.js and app.css are uploaded. If so, return the prefix to them.
    branchname = patreon.config.main_server[:-len(delta_suffix)]
    appjs = branchname + '/js/app.js'
    appcss = branchname + '/css/app.css'
    if delta_bucket.exists(appjs) and delta_bucket.exists(appcss):
        return "//s3-us-west-1.amazonaws.com/patreon-webclient-assets-delta/{}".format(branchname)
    else:
        return None

def delta_react_prefix():
    if not patreon.config.main_server.endswith(delta_suffix):
        return None

    # Check if react-app.css is uploaded. If so, return the prefix.
    branchname = patreon.config.main_server[:-len(delta_suffix)]
    appcss = branchname + '/react-app.css'
    if delta_react_bucket.exists(appcss):
        return "//s3-us-west-1.amazonaws.com/patreon-webclient-assets-react-delta/{}".format(branchname)
    else:
        return None
