import patreon
import slack
import slack.chat

slack.api_token = patreon.config.slack_key


def send_message(channel, message):
    slack.chat.post_message(channel, message, username='scrapeBot')
