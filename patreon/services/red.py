import patreon
import redis


class RedisPool(object):

    def __init__(self, host, port=6379, password=None):
        self.host = host
        self.port = port
        self.password = password
        if password:
            self.pool = redis.ConnectionPool(host=host, port=port, db=0, password=password)
        else:
            self.pool = redis.ConnectionPool(host=host, port=port, db=0)
        self.client = redis.Redis(connection_pool=self.pool)

    def __getattr__(self, attrname):
        if hasattr(self.client, attrname):
            return getattr(self.client, attrname)
        else:
            raise ValueError("Redis client does not have method: {}".format(attrname))


class NullRedisPool(object):

    def __getattr__(self, attrname):
        # All method calls do nothing.
        return lambda *args, **kwargs: None


log = NullRedisPool()


def initialize():
    global log
    log_parameters = patreon.config.redis.get('log_parameters')
    if log_parameters:
        log = RedisPool(
            host=log_parameters['host'],
            port=log_parameters.get('port', 6379),
            password=log_parameters.get('password')
        )
