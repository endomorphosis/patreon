from urllib.parse import urlparse, parse_qs

from googleapiclient.discovery import build
import re
import patreon

YOUTUBE_API_SERVICE_NAME = "youtube"
YOUTUBE_API_VERSION = "v3"

EMBED_WIDTH, EMBED_HEIGHT = 854, 480


def is_youtube(url):
    return get_youtube_id_from_url(url) is not None


def embed_youtube(url):
    video_id = get_youtube_id_from_url(url)
    video = get_embed_data(video_id)

    return {
        'url': url,
        'provider_name': "YouTube",
        'provider_url': "youtube.com",
        'title': video['title'],
        'description': video['description'],
        'object': {
            'html': get_embed_html(video_id)
        },
        'image_url': video['thumbnails']['high']['url'],
        'images': list(video['thumbnails'].values())
    }


def get_authenticated_service(api_key):
    return build(
        YOUTUBE_API_SERVICE_NAME,
        YOUTUBE_API_VERSION,
        developerKey=api_key
    )


def sanitize_url(url):
    if re.match('(?:http|ftp|https)://', url):
        return url
    return "http://" + url


def get_youtube_id_from_url(url):
    "Borrowed from: http://stackoverflow.com/questions/4356538/how-can-i-extract-video-id-from-youtubes-link-in-python"

    query = urlparse(sanitize_url(url))
    if query.hostname == 'youtu.be':
        return query.path[1:]
    if query.hostname in ('www.youtube.com', 'youtube.com'):
        if query.path == '/watch':
            p = parse_qs(query.query)
            return p['v'][0]
        if query.path[:7] == '/embed/':
            return query.path.split('/')[2]
        if query.path[:3] == '/v/':
            return query.path.split('/')[2]
    # fail?
    return None


def get_embed_data(video_id):
    youtube = get_authenticated_service(patreon.config.google_api_key)
    video = youtube.videos().list(part='snippet', id=video_id).execute()

    items = video['items']
    if items and len(items):
        return items[0]['snippet']
    return None


def get_embed_html(video_id):
    return '<iframe src="https://www.youtube.com/embed/{video_id}"' \
           ' class="embedly-embed"' \
           ' width="{width}"' \
           ' height="{height}"' \
           ' frameborder="0"' \
           ' scrolling="no"' \
           ' allowfullscreen></iframe>'.format(
        video_id=video_id,
        width=EMBED_WIDTH,
        height=EMBED_HEIGHT
    )


def with_youtube_api(inner):
    def wrapper(*args, **kwargs):
        return inner(
            get_authenticated_service(patreon.config.google_api_key),
            *args,
            **kwargs
        )

    return wrapper


@with_youtube_api
def get_user_channel_by_username(youtube, username):
    return youtube.channels().list(
        part='contentDetails',
        forUsername=username
    ).execute()


@with_youtube_api
def get_playlist_items(youtube, playlist_id, next_page_token=None):
    kwargs = {
        'part': 'contentDetails',
        'playlistId': playlist_id,
        'maxResults': 50
    }
    if next_page_token is not None:
        kwargs['pageToken'] = next_page_token
    return youtube.playlistItems().list(**kwargs).execute()


@with_youtube_api
def get_details_for_videos(youtube, video_ids):
    return youtube.videos().list(
        part='snippet',
        id=','.join(video_ids)
    ).execute()


@with_youtube_api
def get_user_channel_by_channel_name(youtube, channel_name):
    search_result = youtube.search().list(
        part='snippet',
        q=channel_name
    ).execute()

    if len(search_result['items']) == 0:
        return None

    channel_id = search_result['items'][0]['snippet']['channelId']

    return youtube.channels().list(
        part='contentDetails',
        id=channel_id
    ).execute()
