import patreon
from patreon.services.logging import rollbar
import requests
import json
from urllib import parse

BONFIRE_SECRET_KEY = 'the-minimum-level-of-security'


def is_enabled():
    return patreon.config.bonfire_host is not None


def post(route, content_json):
    try:
        if is_enabled():
            headers = {
                'Content-Type': 'application/json',
                'X-API-Key': BONFIRE_SECRET_KEY
            }
            bonfire_url = parse.urljoin(patreon.config.bonfire_host, route)
            return requests.post(bonfire_url, data=json.dumps(content_json), headers=headers)
    except Exception as e:
        rollbar.log(message="Bonfire post failed: {}".format(str(e)))
