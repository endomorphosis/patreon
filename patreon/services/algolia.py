from algoliasearch import algoliasearch
import patreon


class Algolia(object):
    def __init__(self, application_id, api_key):
        self.application_id = application_id
        self.api_key = api_key
        self.client = algoliasearch.Client(application_id, api_key)
        self.memoized_indexes = {}

    def index(self, name):
        if name in self.memoized_indexes:
            return self.memoized_indexes[name]
        index = self.client.init_index(name)
        self.memoized_indexes[name] = index
        return index

    def campaigns_index(self):
        return self.index('campaigns')

    def posts_index(self):
        return self.index('posts')

    def update_index_with_models_by_id(self, index, model_class, id_name, ids):
        if not ids:
            return
        model_objects = model_class.find_searchable_by_id_list(ids)

        deleted_ids = patreon.util.model.missing_ids(ids, model_objects, id_name)
        self.delete_from_index_by_model_and_ids(index, model_class, deleted_ids)

        self.save_to_index(index, model_objects)

    def delete_from_index_by_model_and_ids(self, index, model_class, ids):
        deleted_algolia_object_ids = [model_class.algolia_id_for_id(id) for id in ids]
        return index.delete_objects(deleted_algolia_object_ids)

    def save_to_index(self, index, model_objects):
        algolia_object_dicts = [model_object.as_search_dict() for model_object in model_objects]
        return index.save_objects(algolia_object_dicts)

    def update_campaigns_by_ids(self, campaign_ids):
        return self.update_index_with_models_by_id(
            self.campaigns_index(), patreon.model.manager.campaign_mgr, "campaign_id", campaign_ids)

    def update_posts_by_ids(self, post_ids):
        return self.update_index_with_models_by_id(
            self.posts_index(), patreon.model.manager.activity_mgr, "activity_id", post_ids)
