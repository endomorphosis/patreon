"""
services/__init__.py
"""

import flask

from . import db
from . import caching
from .caching import memcache
from .caching import request_cache
from . import flume
from . import aws
from . import red
from . import algolia
from . import logging
from . import mail
from . import ratelimit
from . import facebook
from . import image_resizer
from . import spam
from . import pstore
from . import image_getter
from . import payment_apis
from . import delta
from . import geoip
from . import bonfire
from . import slack
from . import redshift
from . import async
from . import encryption
from . import asset_prefix
from . import firebase


def initialize():
    """Connect to Patreon services."""
    db.initialize()
    flume.initialize()
    memcache.initialize()
    red.initialize()


def get_request_cache():
    # TODO(postport): This is too slow and takes too many calls.
    # this is consistent per request, so ensure that this is
    # set once and correct for all contexts.
    if flask.has_request_context():
        if flask.g.get('request_cache') is None:
            flask.g.request_cache = {}
        return flask.g.request_cache
    else:
        if request_cache.fallback_cache is None:
            request_cache.fallback_cache = {}
        return request_cache.fallback_cache


def clear_request_cache():
    if flask.has_request_context():
        flask.g.request_cache = {}
    request_cache.fallback_cache = {}
