"""
db.py

Declare and initialize the SQLAlchemy engine and base model.
Must be imported before the model modules are imported.
"""

import patreon

from sqlalchemy import create_engine, inspect
from sqlalchemy.orm import scoped_session, sessionmaker
from sqlalchemy.ext.compiler import compiles
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.exc import InvalidRequestError
from sqlalchemy.sql.expression import Insert

test_dirty_table = set()
test_payment_events_dirty = False


def create_path_from_parameters(parameters, **kwargs):
    if not parameters:
        return None
    else:
        parameters.setdefault('port', 3306)
        parameters.setdefault('protocol', 'mysql+pymysql')
        if 'passwd' in parameters:
            path = '{protocol}://{user}:{passwd}@{host}:{port}/{db}'.format(**parameters)
        else:
            path = '{protocol}://{user}@{host}:{port}/{db}'.format(**parameters)
        return path

# OK, we are doing some sort of scary things to make sqlalchemy issue COMMITs at a sensible time.
# Unlike the sqlalchemy docs' recommendation, we do not wrap an entire web request in a SQL transaction.
# When you aren't in a transaction, sqlalchemy pulls a Connection out of the Connection Pool every time you
# touch the database at all. We set strategy=threadlocal so that in a single process, you get the same connection
# each time instead of the default that round-robins. (The default behavior interacts badly with SQL transactions
# because each connection can have issued its implicit BEGIN at differnt times and so see different snapshots of the
# database history. Threadlocal is way less surprising.)
# Attempts to change pool_reset_on_return to None have had ambiguous results.
# ~jesse

# Declare the Model base class before we import the patreon.model
# files. Initialize by calling out to the engine.
dbpath = create_path_from_parameters(patreon.config.database['parameters'])
engine = create_engine(dbpath, convert_unicode=True, pool_recycle=3600, connect_args={'charset': 'utf8'},
                       pool_reset_on_return='commit', strategy='threadlocal')


# Darkmode logging, as well as logline logging.
log_dbpath = create_path_from_parameters(patreon.config.database.get('log_parameters'))
if log_dbpath:
    log_engine = create_engine(log_dbpath, convert_unicode=True, strategy='threadlocal', pool_recycle=3600)
else:
    log_engine = None


# TODO(port): Move this into Flask's custom session object.
db_session = scoped_session(
    sessionmaker(
        autocommit=False,
        autoflush=True,
        expire_on_commit=False,
        bind=engine
    ))

log_db_session = None
if log_engine:
    log_db_session = scoped_session(
        sessionmaker(
            autocommit=False,
            autoflush=True,
            expire_on_commit=False,
            bind=log_engine
        ))


class InsertOnDuplicate(Insert):
    pass


@compiles(InsertOnDuplicate)
def insert_on_duplicate(insert, compiler, **kwargs):
    text = compiler.visit_insert(insert, **kwargs)
    text += ' ON DUPLICATE KEY UPDATE '

    if insert._has_multi_parameters:
        stmt_parameters = insert.parameters[0]
    else:
        stmt_parameters = insert.parameters

    update_columns = []

    # retrieve all relevant columns from table
    for column in insert.table.columns:
        if not column.primary_key and column.key in stmt_parameters:
            update_columns.append(column)

    text += ', '.join(
        '{0} = VALUES({0})'.format(compiler.visit_column(c))
        for c in update_columns
    )

    return text


class PatreonModel(object):
    """Custom base class used as mixin for SQLAlchemy Models."""

    def __columns__(self):
        return {column.name: getattr(self, column.name) for column in self.__table__.columns if
                getattr(self, column.name) is not None}

    def as_dict(self):
        return self.__columns__()

    def __repr__(self):
        attributes = self.__columns__()
        attributes_strings = ['{0}={1}'.format(k, v) for k, v in attributes.items()]
        attributes_combined = ' '.join(attributes_strings)
        return 'Model<{0}>: {1}'.format(self.__class__.__tablename__, attributes_combined)

    def __eq__(self, other):
        return (self.__class__ == other.__class__) and (self.__columns__() == other.__columns__())

    @classmethod
    def __get_primary_keys(cls):
        primary_keys = inspect(cls).primary_key
        return [key.name for key in primary_keys]

    def __get_primary_key_dict(self):
        primary_keys = self.__get_primary_keys()
        return {key: getattr(self, key) for key in primary_keys}

    @classmethod
    def __args_to_valid_kwargs(cls, args, kwargs, primary_keys):
        temp_keys = primary_keys.copy()

        for key in kwargs.keys():
            temp_keys.remove(key)

        if len(args) != len(temp_keys):
            raise Exception("Args and kwargs do not cover all primary keys")

        for key, value in zip(temp_keys, args):
            kwargs[key] = value

        if len(list(kwargs.keys())) != len(primary_keys):
            raise Exception("Args and kwargs do not cover all primary keys")

        for key in kwargs.keys():
            if key not in primary_keys:
                raise Exception(key + " not in primary keys for " + cls.__name__)

        return kwargs

    @classmethod
    def __get(cls, *args, **kwargs):
        primary_keys = cls.__get_primary_keys()

        kwargs = cls.__args_to_valid_kwargs(args, kwargs, primary_keys)

        working_query = cls.query

        for key in kwargs.keys():
            working_query = working_query.filter(getattr(cls, key) == kwargs[key])

        return working_query

    @classmethod
    def get(cls, *args, **kwargs):
        return cls.__get(*args, **kwargs).first()

    @classmethod
    def get_with_lock(cls, *args, **kwargs):
        return cls.__get(*args, **kwargs).with_for_update().first()

    @classmethod
    def insert(cls, insert_values):
        if patreon.is_test_environment():
            test_dirty_table.add(cls)

        patreon.services.clear_request_cache()
        with db_session.begin_nested():
            return db_session.execute(cls.__table__.insert().values(insert_values)).inserted_primary_key

    @classmethod
    def insert_ignore(cls, insert_values):
        if patreon.is_test_environment():
            test_dirty_table.add(cls)
        patreon.services.clear_request_cache()
        with db_session.begin_nested():
            return db_session.execute(
                cls.__table__.insert(prefixes=['ignore']).values(insert_values)).inserted_primary_key

    @classmethod
    def insert_on_duplicate_key_update(cls, insert_values):
        if patreon.is_test_environment():
            test_dirty_table.add(cls)
        patreon.services.clear_request_cache()
        with db_session.begin_nested():
            return db_session.execute(
                InsertOnDuplicate(cls.__table__).values(insert_values)).inserted_primary_key

    def delete(self):
        patreon.services.clear_request_cache()
        self.query.filter_by(**self.__get_primary_key_dict()).delete()

    def update(self, update_values):
        patreon.services.clear_request_cache()
        self.query.filter_by(**self.__get_primary_key_dict()).update(update_values)


class PatreonLoggingModel(PatreonModel):
    @classmethod
    def insert(cls, insert_values):
        if patreon.is_test_environment():
            global test_payment_events_dirty
            test_payment_events_dirty = True

        if not log_engine:
            return None

        patreon.services.clear_request_cache()
        key = log_db_session.execute(cls.__table__.insert().values(insert_values)).inserted_primary_key
        log_db_session.commit()
        return key

    @classmethod
    def insert_ignore(cls, insert_values):
        raise NotImplemented

    @classmethod
    def insert_on_duplicate_key_update(cls, insert_values):
        raise NotImplemented


Model = declarative_base(cls=PatreonModel)
Model.query = db_session.query_property()

LoggingModel = declarative_base(cls=PatreonLoggingModel)
if log_db_session:
    LoggingModel.query = log_db_session.query_property()


def _convert_to_dict(result):
    """Convert a result proxy to a dictionary. Ignore keys that
    have ambiguous references (e.g. JOINs along columns that have
    the same name."""
    try:
        return dict(result)
    except InvalidRequestError:
        return dict(zip(result.keys(), result.values()))


def initialize():
    pass


def query(sql, args=()):
    if patreon.config.sandbox:
        sql_ = sql.lower().strip()
        if sql_.startswith('insert') or sql_.startswith('replace') or sql_.startswith('update'):
            return None
    return query_engine(engine, sql, args)


def query_log(sql, args=()):
    if log_engine:
        return query_engine(log_engine, sql, args)
    else:
        return None


def query_engine(engine_, sql, args):
    result = engine_.execute(sql, tuple(args))
    if result.returns_rows:
        rows = [_convert_to_dict(r) for r in result]
        return QueryResultList(rows)
    else:
        return result


class QueryResultList(list):
    def size(self):
        return len(self)

    def first(self):
        if self.size() > 0:
            return self[0]
        else:
            return None
