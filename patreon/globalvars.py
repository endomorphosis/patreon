"""
global.py

List of all the global variables and configuration options.
"""
import patreon
import time

# TODO(postport): All these need to be documented.
# TODO(postport): Move all the enumerations to their own types
# TODO(postport): Move this to a dynamic system similar to instancevars
# TODO(port): These are all meant to be default values instead of
# outright sets, see if they should be overwritten somewhere.

# ======================
# Control Methods
# ======================

_inner = {}


def define(varname, varvalue):
    _inner[varname] = varvalue


def get(varname):
    return _inner.get(varname)


def to_dict():
    return _inner


# ======================
# Static Globals
# ======================
define('MAIN_SERVER', patreon.config.main_server)

# These tell the javascript which onLoad scripts to run.
define('PAGE_HOME', 'home')
define('PAGE_USER', 'user')
define('PAGE_PATRON', 'upload')
define('PAGE_SHARE', 'share')
define('PAGE_INDEX', 'index')

# defines for 1 and 2 don't exist since they're mapped to
# REWARD_PROCESSED and REWARD_DECLINED
define('PAYOUT_PAID', 0) # unused
define('PAYOUT_TOCREATOR', 3)
define('PAYOUT_DD', 4)
define('PAYOUT_FEE', 5)
define('PAYOUT_DD_FAILED', 6)
define('PAYOUT_CREDIT', 10)

define('ALERT_USER_SETUP', 1)
define('ALERT_USER_DONE', 2)
define('ALERT_USER_POST', 3)
define('ALERT_USER_DELETE_PLEDGE', 10)
define('ALERT_USER_INVALID', 11)

define('COMMENTS_BEFORE_HIDE', 3)
define('SHARES_PER_PAGE', 25)

define('REWARD_PENDING', 0)
define('REWARD_PROCESSED', 1)
define('REWARD_DECLINED', 2)
define('REWARD_REFUNDED', 10)

unix_time = str(int(time.time()))
define('FILE_VERSION', unix_time)
define('VERSION_JS_OURSPOT', unix_time)

# Facebook Keys (used in header and meta)
define('FB_APP_ID', '130127590512253')
define('FB_SECRET', '4d8607c0c51f769083e95a131473ff19')

# Stripe Key
define('STRIPE_KEY_PUBLIC', patreon.config.stripe['publishable_key'])

define('IMG_PROFILE_DEFAULT', '/images/profile_default.png')
define('IMG_PROFILE_DEFAULT_LARGE', '/images/profile_default_large.png')
define('IMG_FAVE_DEFAULT', '/images/fave_default.png')
define('IMG_SHARE_DEFAULT', '/images/share_default.png')
define('IMG_LINK_DEFAULT', '//www.patreon.com/images/link_default.png')

# Used in the session handler, equal to 1 month in seconds
define('SESSION_LIFETIME', 31 * 24 * 60 * 60)
