"""
routing.py

Utility for exception-based system for redirecting a user to
a different URL.
"""
from flask import request
import patreon
from urllib import parse

MAIN_SERVER = patreon.globalvars.get('MAIN_SERVER')


class Redirect(Exception):
    def __init__(self, location):
        self.location = location


def redirect(location):
    raise Redirect(location)


def redirect_to_login(**kwargs):
    kwargs['ru'] = request.full_path
    redirect('/login?ru=' + parse.urlencode(kwargs))


def redirect_to_signup(**kwargs):
    kwargs['ru'] = request.full_path
    redirect('/signup?' + parse.urlencode(kwargs))


def sanitize_redirect_url(redirect_url):
    if not redirect_url:
        return ''
    elif redirect_url == 'https://' + MAIN_SERVER + ':443/':
        # TODO: This was undocumented in PHP, not sure what this is for.
        return 'https://' + MAIN_SERVER + '/home'
    else:
        for sub in ('http://', 'https://', MAIN_SERVER):
            redirect_url = redirect_url.replace(sub, '')
        redirect_url = redirect_url.lstrip('/')
        redirect_url = 'https://' + MAIN_SERVER + '/' + redirect_url
        return redirect_url
