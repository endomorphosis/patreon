import datetime
from sqlalchemy.sql import func

from patreon.model.table import Pledge, Stats
from patreon.model.manager import stats_mgr
from patreon.services.db import db_session
from patreon.stats import stats_dashboard_input


def get_average_pledge_amount(date):
    return db_session.query(func.avg(Pledge.Pledge)) \
                     .filter(Pledge.Created <= date.strftime('%Y-%m-%d')) \
                     .one()[0]


def get_pledges_per_patron(date):
    return db_session.query(func.count(Pledge.UID) / func.count(func.distinct(Pledge.UID))) \
                     .filter(Pledge.Created <= date.strftime('%Y-%m-%d')) \
                     .one()[0]


def get_average_stats(date=None):
    if date is None:
        date = datetime.datetime.now() - datetime.timedelta(days=1)

    for key, result in [('pledges_per_patron', get_pledges_per_patron(date)),
                        ('average_pledge_amount', get_average_pledge_amount(date))]:
        result = result
        Stats.insert({
            'Date': date.strftime('%Y-%m-%d'),
            'Value': result,
            'Key': key
        })


def store_dashboard_data():
    # All dashboard stats
    all_dashboard_stats = stats_dashboard_input.build_dashboard_data()
    # Stats for the entire history
    all_historical_metrics = all_dashboard_stats['All Historical Metrics']
    for month in all_historical_metrics.keys():
        for key in all_historical_metrics[month].keys():
            stats_mgr.create_or_update(
                key,
                datetime.datetime.strptime(month, '%Y-%m'),
                all_historical_metrics[month].get(key)
            )
    # Stats for main company KPIs
    main_company_kpis = all_dashboard_stats['Main Company KPIs']
    for month in main_company_kpis.keys():
        for key in main_company_kpis[month].keys():
            stats_mgr.create_or_update(
                key,
                datetime.datetime.strptime(month, '%Y-%m'),
                main_company_kpis[month].get(key)
            )

    db_session.commit()


if __name__ == '__main__':
    import argparse

    parser = argparse.ArgumentParser()
    parser.add_argument('-p', '--previous-days', action='store', type=int, default=1)

    options = parser.parse_args()

    for i in range(1, options.previous_days + 1):
        date = datetime.datetime.today() - datetime.timedelta(days=i)
        get_average_stats(date)
