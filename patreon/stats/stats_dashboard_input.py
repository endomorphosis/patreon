import calendar
import datetime
from collections import OrderedDict
from patreon.services import db

"""
This file produces data for a data dashboard.
It contains three functions:
    build_dashboard_data
    build_ordered_list_of_dashboard_data_keys_all_historical_metrics
    build_dashboard_data_main_company_kpis
They are documented below.

All dollar amounts are in dollars (not cents)
All growth rates are expressed as decimals (i.e., percent growth of 0.0654
corresponds to 6.54%)
"""


def build_dashboard_data():
    """This function builds a full set of metrics to be shown in a data dashboard.

    :return: An OrderedDict containing {
                'All Historical Metrics': all_historical_metrics,
                'Main Company KPIs': main_company_kpis
            }
            all_historical_metrics - A dictionary, with each key identifying a month:
                all_historical_metrics['2015-07'] = {
                    'metric_name_1': metric1,
                    'metric_name_2: metric2',
                    etc.
                }
    """
    # Define a variable to store all dashboard data
    all_historical_metrics = OrderedDict()

    ############################################################
    # Dashboard section: Payments processed
    ############################################################

    # Define the SQL query
    _sql = """
    SELECT c.Month, IFNULL(c.PaymentsSuccessfullyProcessed,0) - IFNULL(c.RefundsProcessed,0) AS NetPaymentsProcessed, IFNULL(c.PaymentsSuccessfullyProcessed,0) AS PaymentsSuccessfullyProcessed, IFNULL(c.RefundsProcessed,0) AS RefundsProcessed FROM
        (SELECT a.Month, a.PaymentsSuccessfullyProcessed, b.RefundsProcessed FROM
            (SELECT DATE_FORMAT(CONVERT_TZ(Created,'GMT','US/Pacific'),'%%Y-%%m') AS Month, SUM(Pledge/100) AS PaymentsSuccessfullyProcessed FROM tblRewardsGive WHERE Status=1 GROUP BY Month) a
        LEFT JOIN
            (SELECT DATE_FORMAT(CONVERT_TZ(Created,'GMT','US/Pacific'),'%%Y-%%m') AS Month, SUM(Pledge/100) AS RefundsProcessed FROM tblRewardsGive WHERE Status=10 GROUP BY Month) b
        ON a.Month=b.Month) c
    ORDER BY Month;
    """
    # Execute the SQL query
    results = db.engine.execute(_sql)
    # Add the results to the full set of data
    for result in results:
        this_month = result[0]
        if this_month not in all_historical_metrics.keys():
            all_historical_metrics[this_month] = {}
        all_historical_metrics[this_month]['NetPaymentsProcessed'] = result[1]
        all_historical_metrics[this_month]['PaymentsSuccessfullyProcessed'] = result[2]
        all_historical_metrics[this_month]['RefundsProcessed'] = result[3]

    # Fetch all months from the data
    sorted_months = list(all_historical_metrics.keys())
    sorted_months.sort()
    # Add some post-processing
    for i in range(0, len(sorted_months)):
        # If this is the first month, make the percent growth 100%
        if i == 0:
            all_historical_metrics[sorted_months[i]]['PercentGrowth_NetPaymentsProcessed'] = 1
        else:
            # Add percent growth to the results, but only if the payments processed this month > 0
            if all_historical_metrics[sorted_months[i]]['PaymentsSuccessfullyProcessed'] > 0:
                all_historical_metrics[sorted_months[i]]['PercentGrowth_NetPaymentsProcessed'] = (
                    all_historical_metrics[sorted_months[i]][ 'NetPaymentsProcessed']
                    / all_historical_metrics[sorted_months[i - 1]]['NetPaymentsProcessed']
                    - 1) * 100
            else:
                all_historical_metrics[sorted_months[i]]['PercentGrowth_NetPaymentsProcessed'] = 0

    ############################################################
    #  Dashboard section: Creators paid over a threshold
    ############################################################

    # Define the SQL query
    _sql = """
    SELECT b.Month, b.NumCreatorsPaidAtLeast100, e.NumCreatorsPaidAtLeast100ForFirstTime FROM
        (SELECT Month, COUNT(DISTINCT CID) AS NumCreatorsPaidAtLeast100 FROM
            (SELECT DATE_FORMAT(CONVERT_TZ(Created,'GMT','US/Pacific'),'%%Y-%%m') AS Month, CID, SUM(Pledge/100) AS PaymentProcessed FROM tblRewardsGive WHERE Status=1 GROUP BY Month, CID) a
            WHERE PaymentProcessed>=100
            GROUP BY Month
        ) b
    LEFT JOIN
        -- Find out how many creators were first paid above a threshold
        (SELECT d.FirstMonthPaidOverThreshold, COUNT(DISTINCT d.CID) AS NumCreatorsPaidAtLeast100ForFirstTime FROM
            (SELECT CID, MIN(Month) AS FirstMonthPaidOverThreshold FROM
                (SELECT DATE_FORMAT(CONVERT_TZ(Created,'GMT','US/Pacific'),'%%Y-%%m') AS Month, CID, SUM(Pledge/100) AS PaymentProcessed FROM tblRewardsGive WHERE Status=1 GROUP BY Month, CID) c
                WHERE PaymentProcessed>=100
            GROUP BY CID) d
        GROUP BY FirstMonthPaidOverThreshold
        ) e
    ON b.Month=e.FirstMonthPaidOverThreshold
    ORDER BY Month;
    """
    # Execute the SQL query
    results = db.engine.execute(_sql)
    # Store the results in an array
    for result in results:
        this_month = result[0]
        if this_month not in all_historical_metrics.keys():
            all_historical_metrics[this_month] = {}
        all_historical_metrics[this_month]['NumCreatorsProcessedAtLeast100'] = result[1]
        all_historical_metrics[this_month]['NumCreatorsProcessedAtLeast100ForFirstTime'] = result[2]

    # Fetch all months from the data
    sorted_months = list(all_historical_metrics.keys())
    sorted_months.sort()
    # Add some post-processing
    for i in range(0, len(sorted_months)):
        # If this is the first month...
        if i == 0:
            # The percent growth doesn't make sense in the first month, so ignore it
            all_historical_metrics[sorted_months[i]]['PercentGrowth_NumCreatorsProcessedAtLeast100'] = 0
            # The net increase in the number of creators equals the number of creators
            all_historical_metrics[sorted_months[i]]['NetIncreaseInNumCreatorsProcessedAtLeast100'] = \
                all_historical_metrics[sorted_months[i]]['NumCreatorsProcessedAtLeast100']
        else:
            # The percent growth in the number of creators processing at least 100:
            all_historical_metrics[sorted_months[i]]['PercentGrowth_NumCreatorsProcessedAtLeast100'] = (
                all_historical_metrics[sorted_months[i]]['NumCreatorsProcessedAtLeast100']
                / all_historical_metrics[sorted_months[i - 1]]['NumCreatorsProcessedAtLeast100']
                - 1) * 100
            # The net increase in the number of creators processing at least 100:
            all_historical_metrics[sorted_months[i]]['NetIncreaseInNumCreatorsProcessedAtLeast100'] = \
                all_historical_metrics[sorted_months[i]]['NumCreatorsProcessedAtLeast100'] \
                - all_historical_metrics[sorted_months[i - 1]]['NumCreatorsProcessedAtLeast100']

    ############################################################
    # Dashboard section: Patrons
    ############################################################

    # Define the SQL query
    _sql = """
    SELECT f.Month, e.NumPatronsFirstProcessingThisMonth, e.NumPatronsLastProcessingThisMonth, f.NumPatronsProcessingThisMonth FROM
        (SELECT DATE_FORMAT(CONVERT_TZ(Created,'GMT','US/Pacific'),'%%Y-%%m') AS Month, COUNT(DISTINCT UID) AS NumPatronsProcessingThisMonth
            FROM tblRewardsGive WHERE Status=1 AND Pledge>0 GROUP BY Month) f
    LEFT JOIN
        (SELECT b.Month, b.NumPatronsFirstProcessingThisMonth, d.NumPatronsLastProcessingThisMonth FROM
            (SELECT DATE_FORMAT(a.FirstPaymentDate,'%%Y-%%m') AS Month, COUNT(DISTINCT a.UID) AS NumPatronsFirstProcessingThisMonth FROM
                    (SELECT UID, MIN(CONVERT_TZ(Created,'GMT','US/Pacific')) AS FirstPaymentDate FROM tblRewardsGive WHERE Status=1 AND Pledge>0 GROUP BY UID) a
            GROUP BY Month) b
        LEFT JOIN
            (SELECT DATE_FORMAT(c.LastPaymentDate,'%%Y-%%m') AS Month, COUNT(DISTINCT c.UID) AS NumPatronsLastProcessingThisMonth FROM
                -- Find the latest payment date
                (SELECT UID, MAX(CONVERT_TZ(Created,'GMT','US/Pacific')) AS LastPaymentDate FROM tblRewardsGive WHERE Status=1 AND Pledge>0 GROUP BY UID) c
            GROUP BY Month) d
        ON b.Month=d.Month
        ) e
    ON f.Month=e.Month
    ORDER BY Month;
    """
    # Execute the SQL query
    results = db.engine.execute(_sql)
    # Store the results in an array
    for result in results:
        this_month = result[0]
        if this_month not in all_historical_metrics.keys():
            all_historical_metrics[this_month] = {}
        all_historical_metrics[this_month]['NumPatronsProcessingPayments'] = result[3]
        all_historical_metrics[this_month]['NumPatronsMadeFirstPayment'] = result[1]
        all_historical_metrics[this_month]['NumPatronsMadeLastPayment'] = result[2]

    # Fetch all months from the data
    sorted_months = list(all_historical_metrics.keys())
    sorted_months.sort()
    # Add some post-processing
    for i in range(0, len(sorted_months)):
        # If this is the first month...
        if i == 0:
            # The number of financially-active patrons equals the number of
            # patrons who made their first payment this month
            all_historical_metrics[sorted_months[i]]['NumFinanciallyActivePatrons'] = \
                all_historical_metrics[sorted_months[i]]['NumPatronsMadeFirstPayment']
            # The net increase in the number of financially-active patrons
            # equals the number of financially-active patrons
            all_historical_metrics[sorted_months[i]]['NetIncrease_NumFinanciallyActivePatrons'] = \
                all_historical_metrics[sorted_months[i]]['NumFinanciallyActivePatrons']
            # The percent growth doesn't make sense in the first month, so ignore it
            all_historical_metrics[sorted_months[i]]['PercentGrowth_NumPatronsProcessingPayments'] = 0
            all_historical_metrics[sorted_months[i]]['PercentGrowth_NumFinanciallyActivePatrons'] = 0
            # The number churned doesn't make sense in the first month, so ignore it
            all_historical_metrics[sorted_months[i]]['NumPatronsChurned'] = 0
            # The patron churn rate doesn't make sense in the first month, so ignore it
            all_historical_metrics[sorted_months[i]]['PatronChurnRate'] = 0
        else:
            # The number of financially-active patrons equals the number from
            # last month, plus the new patrons, minus the ones we lost after
            # the previous month
            all_historical_metrics[sorted_months[i]]['NumFinanciallyActivePatrons'] = \
                all_historical_metrics[sorted_months[i - 1]]['NumFinanciallyActivePatrons'] \
                + all_historical_metrics[sorted_months[i]]['NumPatronsMadeFirstPayment'] \
                - all_historical_metrics[sorted_months[i - 1]]['NumPatronsMadeLastPayment']
            # The net increase in the number of financially-active patrons
            # equals the difference between this month and the previous month
            all_historical_metrics[sorted_months[i]]['NetIncrease_NumFinanciallyActivePatrons'] = \
                all_historical_metrics[sorted_months[i]]['NumFinanciallyActivePatrons'] \
                - all_historical_metrics[sorted_months[i - 1]]['NumFinanciallyActivePatrons']
            # The percent growth in financially-active and paying patrons:
            all_historical_metrics[sorted_months[i]]['PercentGrowth_NumPatronsProcessingPayments'] = (
                all_historical_metrics[sorted_months[i]]['NumPatronsProcessingPayments']
                / all_historical_metrics[sorted_months[i - 1]]['NumPatronsProcessingPayments']
                - 1) * 100
            all_historical_metrics[sorted_months[i]]['PercentGrowth_NumFinanciallyActivePatrons'] = (
                all_historical_metrics[sorted_months[i]]['NumFinanciallyActivePatrons']
                / all_historical_metrics[sorted_months[i - 1]]['NumFinanciallyActivePatrons']
                - 1) * 100
            # The number churned equals the number of patrons who never paid
            # again after the previous month
            all_historical_metrics[sorted_months[i]]['NumPatronsChurned'] = \
                all_historical_metrics[sorted_months[i - 1]]['NumPatronsMadeLastPayment']
            # The patron churn rate:
            all_historical_metrics[sorted_months[i]]['PatronChurnRate'] = (
                all_historical_metrics[sorted_months[i]]['NumPatronsChurned']
                / all_historical_metrics[sorted_months[i - 1]]['NumFinanciallyActivePatrons']
                * 100)

    ############################################################
    # Dashboard section: Creators
    ############################################################

    # Define the SQL query
    _sql = """
    SELECT f.Month, e.NumCreatorsFirstProcessingThisMonth, e.NumCreatorsLastProcessingThisMonth, f.NumCreatorsProcessingThisMonth FROM
    (SELECT DATE_FORMAT(CONVERT_TZ(Created,'GMT','US/Pacific'),'%%Y-%%m') AS Month, COUNT(DISTINCT CID) AS NumCreatorsProcessingThisMonth
        FROM tblRewardsGive WHERE Status=1 AND Pledge>0 GROUP BY Month) f
    LEFT JOIN
    (SELECT b.Month, b.NumCreatorsFirstProcessingThisMonth, d.NumCreatorsLastProcessingThisMonth FROM
        (SELECT DATE_FORMAT(a.FirstPaymentDate,'%%Y-%%m') AS Month, COUNT(DISTINCT a.CID) AS NumCreatorsFirstProcessingThisMonth FROM
            (SELECT CID, MIN(CONVERT_TZ(Created,'GMT','US/Pacific')) AS FirstPaymentDate FROM tblRewardsGive WHERE Status=1 AND Pledge>0 GROUP BY CID) a
        GROUP BY Month) b
    LEFT JOIN
        (SELECT DATE_FORMAT(c.LastPaymentDate,'%%Y-%%m') AS Month, COUNT(DISTINCT c.CID) AS NumCreatorsLastProcessingThisMonth FROM
            -- Find the latest payment date
            (SELECT CID, MAX(CONVERT_TZ(Created,'GMT','US/Pacific')) AS LastPaymentDate FROM tblRewardsGive WHERE Status=1 AND Pledge>0 GROUP BY CID) c
        GROUP BY Month) d
    ON b.Month=d.Month
    ) e
    ON f.Month=e.Month
    ORDER BY Month
    """
    # Execute the SQL query
    results = db.engine.execute(_sql)
    # Store the results in an array
    for result in results:
        this_month = result[0]
        if this_month not in all_historical_metrics.keys():
            all_historical_metrics[this_month] = {}
        all_historical_metrics[this_month]['NumCreatorsProcessedFirstPayment'] = result[1]
        all_historical_metrics[this_month]['NumCreatorsProcessedAnyAmount'] = result[3]
        all_historical_metrics[this_month]['NumCreatorsProcessedLastPayment'] = result[2]

    # Fetch all months from the data
    sorted_months = list(all_historical_metrics.keys())
    sorted_months.sort()
    # Add some post-processing
    for i in range(0, len(sorted_months)):
        # If this is the first month...
        if i == 0:
            # The number of financially-active creators equals the number of
            # creators who made their first payment this month
            all_historical_metrics[sorted_months[i]]['NumFinanciallyActiveCreators_AnyAmount'] = \
                all_historical_metrics[sorted_months[i]]['NumCreatorsProcessedFirstPayment']
            # The net increase in the number of financially-active creators
            # equals the number of financially-active creators
            all_historical_metrics[sorted_months[i]]['NetIncrease_NumFinanciallyActiveCreators_AnyAmount'] = \
                all_historical_metrics[sorted_months[i]]['NumFinanciallyActiveCreators_AnyAmount']
            # The percent growth doesn't make sense in the first month, so ignore it
            all_historical_metrics[sorted_months[i]]['PercentGrowth_NumFinanciallyActiveCreators_AnyAmount'] = 0
            all_historical_metrics[sorted_months[i]]['PercentGrowth_NumCreatorsProcessedAnyAmount'] = 0
            # The number churned doesn't make sense in the first month, so ignore it
            all_historical_metrics[sorted_months[i]]['NumCreatorsChurned'] = 0
            # The creator churn rate doesn't make sense in the first month, so ignore it
            all_historical_metrics[sorted_months[i]]['CreatorChurnRate'] = 0
        else:
            # The number of financially-active creators equals the number from
            # last month, plus the new creators, minus the ones we lost after
            # the previous month
            all_historical_metrics[sorted_months[i]]['NumFinanciallyActiveCreators_AnyAmount'] = \
                all_historical_metrics[sorted_months[i - 1]]['NumFinanciallyActiveCreators_AnyAmount'] \
                + all_historical_metrics[sorted_months[i]]['NumCreatorsProcessedFirstPayment'] \
                - all_historical_metrics[sorted_months[i - 1]]['NumCreatorsProcessedLastPayment']
            # The net increase in the number of financially-active creators
            # equals the difference between this month and the previous month
            all_historical_metrics[sorted_months[i]]['NetIncrease_NumFinanciallyActiveCreators_AnyAmount'] = \
                all_historical_metrics[sorted_months[i]]['NumFinanciallyActiveCreators_AnyAmount'] \
                - all_historical_metrics[sorted_months[i - 1]]['NumFinanciallyActiveCreators_AnyAmount']
            # The percent growth in financially-active creators:
            all_historical_metrics[sorted_months[i]]['PercentGrowth_NumFinanciallyActiveCreators_AnyAmount'] = (
                all_historical_metrics[sorted_months[i]]['NumFinanciallyActiveCreators_AnyAmount']
                / all_historical_metrics[sorted_months[i - 1]]['NumFinanciallyActiveCreators_AnyAmount']
                - 1) * 100
            all_historical_metrics[sorted_months[i]]['PercentGrowth_NumCreatorsProcessedAnyAmount'] = (
                all_historical_metrics[sorted_months[i]]['NumCreatorsProcessedAnyAmount']
                / all_historical_metrics[sorted_months[i - 1]]['NumCreatorsProcessedAnyAmount']
                - 1) * 100
            # The number churned equals the number of creators who never processed again after the previous month
            all_historical_metrics[sorted_months[i]]['NumCreatorsChurned'] = \
                all_historical_metrics[sorted_months[i - 1]]['NumCreatorsProcessedLastPayment']
            # The creator churn rate:
            all_historical_metrics[sorted_months[i]]['CreatorChurnRate'] = (
                all_historical_metrics[sorted_months[i]]['NumCreatorsChurned']
                / all_historical_metrics[sorted_months[i - 1]]['NumFinanciallyActiveCreators_AnyAmount']
                ) * 100

    ############################################################
    #  Dashboard section: Pledges
    ############################################################

    # Define the SQL query
    _sql = """
    SELECT g.Month, IFNULL(g.NumDistinctPledgesProcessed,0) AS NumDistinctPledgesProcessed, IFNULL(g.NumPledgesFirstPaidThisMonth,0) AS NumPledgesFirstPaidThisMonth, IFNULL(g.NumPledgesLastPaidThisMonth,0) AS NumPledgesLastPaidThisMonth, IFNULL(j.NumPledgesStrictlyChurned,0) AS NumPledgesStrictlyChurned FROM
        -- Find the number of pledges started, paid, and last paid in various months
        (SELECT e.Month, e.NumDistinctPledgesProcessed, f.NumPledgesFirstPaidThisMonth, f.NumPledgesLastPaidThisMonth FROM
            (SELECT DATE_FORMAT(CONVERT_TZ(Created,'GMT','US/Pacific'),'%%Y-%%m') AS Month, COUNT(DISTINCT CONCAT(UID,'-',CID)) AS NumDistinctPledgesProcessed FROM tblRewardsGive WHERE Status=1 AND Pledge>0 	GROUP BY Month) e
        LEFT JOIN
            (SELECT b.Month, b.NumPledgesFirstPaidThisMonth, d.NumPledgesLastPaidThisMonth FROM
                (SELECT DATE_FORMAT(a.FirstPaymentDate,'%%Y-%%m') AS Month, COUNT(DISTINCT a.UID_CID) AS NumPledgesFirstPaidThisMonth FROM
                    (SELECT CONCAT(UID,'-',CID) AS UID_CID, MIN(CONVERT_TZ(Created,'GMT','US/Pacific')) AS FirstPaymentDate FROM tblRewardsGive WHERE Status=1 AND Pledge>0 GROUP BY UID_CID) a
                GROUP BY Month) b
            LEFT JOIN
                -- Find the number of patrons started and still active in various months
                (SELECT DATE_FORMAT(c.LastPaymentDate,'%%Y-%%m') AS Month, COUNT(DISTINCT c.UID_CID) AS NumPledgesLastPaidThisMonth FROM
                    -- Find the latest payment date
                    (SELECT CONCAT(UID,'-',CID) AS UID_CID, MAX(CONVERT_TZ(Created,'GMT','US/Pacific')) AS LastPaymentDate FROM tblRewardsGive WHERE Status=1 AND Pledge>0 GROUP BY UID_CID) c
                GROUP BY Month) d
            ON b.Month=d.Month
            ) f
        ON e.Month=f.Month
        ORDER BY Month) g
    LEFT JOIN
        -- Find the number of pledges churned, and consider the pledge churned in the month after its last payment
        -- These pledges are considered "strictly churned" because the supported creator continued to earn money from other patrons, after the patron in question stopped supporting the creator
        (SELECT DATE_FORMAT(DATE_ADD(j.LatestPaymentDate_ByUID_CID, INTERVAL 1 MONTH),'%%Y-%%m') AS Month, COUNT(DISTINCT j.UID_CID) AS NumPledgesStrictlyChurned FROM
            (SELECT h.UID_CID, h.LatestPaymentDate_ByUID_CID, i.LatestPaymentDate_ByCID FROM
                (SELECT CONCAT(UID,'-',CID) AS UID_CID, CID, MAX(CONVERT_TZ(Created,'GMT','US/Pacific')) AS LatestPaymentDate_ByUID_CID FROM tblRewardsGive WHERE Status=1 AND Pledge>0 GROUP BY UID_CID) h
            LEFT JOIN
                (SELECT CID, MAX(CONVERT_TZ(Created,'GMT','US/Pacific')) AS LatestPaymentDate_ByCID FROM tblRewardsGive WHERE Status=1 AND Pledge>0 GROUP BY CID) i
            ON h.CID=i.CID) j
        -- Only count it as churned if the creator was paid by another patron more than 5 days after this patron's last payment
        WHERE DATEDIFF(j.LatestPaymentDate_ByCID,j.LatestPaymentDate_ByUID_CID)>5
        GROUP BY Month) j
    ON g.Month=j.Month;
    """
    # Execute the SQL query
    results = db.engine.execute(_sql)
    # Store the results in an array
    for result in results:
        this_month = result[0]
        if this_month not in all_historical_metrics.keys():
            all_historical_metrics[this_month] = {}
        all_historical_metrics[this_month]['NumPledgesProcessingPayments'] = result[1]
        all_historical_metrics[this_month]['NumPledgesFirstPaid'] = result[2]
        all_historical_metrics[this_month]['NumPledgesLastPaid'] = result[3]
        all_historical_metrics[this_month]['NumPledgesChurned_StrictlyPledgeChurn'] = result[4]

    # Fetch all months from the data
    sorted_months = list(all_historical_metrics.keys())
    sorted_months.sort()
    # Add some post-processing
    for i in range(0, len(sorted_months)):
        # If this is the first month...
        if i == 0:
            # The number of financially-active pledges equals the number of pledges first paid this month
            all_historical_metrics[sorted_months[i]]['NumFinanciallyActivePledges'] = \
                all_historical_metrics[sorted_months[i]]['NumPledgesFirstPaid']
            # The percent growth doesn't make sense in the first month, so ignore it
            all_historical_metrics[sorted_months[i]]['PercentGrowth_NumFinanciallyActivePledges'] = 0
            all_historical_metrics[sorted_months[i]]['PercentGrowth_NumPledgesProcessingPayments'] = 0
            # The number churned doesn't make sense in the first month, so ignore it
            all_historical_metrics[sorted_months[i]]['NumPledgesChurned_AllCauses'] = 0
            all_historical_metrics[sorted_months[i]]['NumPledgesChurned_StrictlyPledgeChurn'] = 0
            # The churn rate doesn't make sense in the first month, so ignore it
            all_historical_metrics[sorted_months[i]]['PledgeChurnRate_StrictlyPledgeChurn'] = 0
        else:
            # The number of financially-active pledges equals the number from
            # last month, plus the new pledges, minus the ones we lost after
            # the previous month
            all_historical_metrics[sorted_months[i]]['NumFinanciallyActivePledges'] = \
                all_historical_metrics[sorted_months[i - 1]]['NumFinanciallyActivePledges'] \
                + all_historical_metrics[sorted_months[i]]['NumPledgesFirstPaid'] \
                - all_historical_metrics[sorted_months[i - 1]]['NumPledgesLastPaid']
            # The percent growth in financially-active pledges:
            all_historical_metrics[sorted_months[i]]['PercentGrowth_NumFinanciallyActivePledges'] = (
                all_historical_metrics[sorted_months[i]]['NumFinanciallyActivePledges']
                / all_historical_metrics[sorted_months[i - 1]]['NumFinanciallyActivePledges']
                - 1) * 100
            all_historical_metrics[sorted_months[i]]['PercentGrowth_NumPledgesProcessingPayments'] = (
                all_historical_metrics[sorted_months[i]]['NumPledgesProcessingPayments']
                / all_historical_metrics[sorted_months[i - 1]]['NumPledgesProcessingPayments']
                - 1) * 100
            # The number churned equals the number of pledges never processed again after the previous month
            all_historical_metrics[sorted_months[i]]['NumPledgesChurned_AllCauses'] = \
                all_historical_metrics[sorted_months[i - 1]]['NumPledgesLastPaid']
            # The pledge churn rate, strictly pledge churn:
            all_historical_metrics[sorted_months[i]]['PledgeChurnRate_StrictlyPledgeChurn'] = (
                all_historical_metrics[sorted_months[i]]['NumPledgesChurned_StrictlyPledgeChurn']
                / all_historical_metrics[sorted_months[i - 1]]['NumFinanciallyActivePledges']
                * 100)

    ############################################################
    # Dashboard section: Engagement
    ############################################################

    # Define the SQL query
    _sql = """
    SELECT a.Month, IFNULL(a.NumComments,0) AS NumComments, IFNULL(b.NumLikes,0) AS NumLikes, IFNULL(a.NumPostsWithComments,0) AS NumPostsWithComments, IFNULL(b.NumPostsWithLikes,0) AS NumPostsWithLikes FROM
        -- Total comments
        (SELECT DATE_FORMAT(CONVERT_TZ(Created,'GMT','US/Pacific'),'%%Y-%%m') AS Month, COUNT(UID) AS NumComments, COUNT(DISTINCT HID) AS NumPostsWithComments FROM tblComments GROUP BY Month) a
    LEFT JOIN
        -- Total likes
        (SELECT DATE_FORMAT(CONVERT_TZ(Created,'GMT','US/Pacific'),'%%Y-%%m') AS Month, COUNT(UID) AS NumLikes, COUNT(DISTINCT HID) AS NumPostsWithLikes FROM tblActions WHERE Type=1 GROUP BY Month) b
    ON a.Month=b.Month;
    """
    # Execute the SQL query
    results = db.engine.execute(_sql)
    # Store the results in an array
    for result in results:
        this_month = result[0]
        if this_month not in all_historical_metrics.keys():
            all_historical_metrics[this_month] = {}
        all_historical_metrics[this_month]['NumComments'] = result[1]
        all_historical_metrics[this_month]['NumLikes'] = result[2]
        all_historical_metrics[this_month]['NumPledgesLastPaid'] = result[3]

    ############################################################
    # Dashboard section: Productivity
    ############################################################

    # Hard-code the number of employees for now
    results = [
        ['2013-05', 2],
        ['2013-06', 3],
        ['2013-07', 3],
        ['2013-08', 4],
        ['2013-09', 4],
        ['2013-10', 5],
        ['2013-11', 5],
        ['2013-12', 5],
        ['2014-01', 5],
        ['2014-02', 6],
        ['2014-03', 6],
        ['2014-04', 6],
        ['2014-05', 7],
        ['2014-06', 8],
        ['2014-07', 9],
        ['2014-08', 12],
        ['2014-09', 13],
        ['2014-10', 13],
        ['2014-11', 15],
        ['2014-12', 17],
        ['2015-01', 19],
        ['2015-02', 19],
        ['2015-03', 22],
        ['2015-04', 28],
        ['2015-05', 29],
        ['2015-06', 33],
        ['2015-07', 36],
        ['2015-08', 40]
    ]
    # Store the results in a list of dicts
    for result in results:
        this_month = result[0]
        if this_month not in all_historical_metrics.keys():
            all_historical_metrics[this_month] = {}
        all_historical_metrics[this_month]['NumEmployees'] = result[1]

    ############################################################
    # Add some additional metrics
    ############################################################

    # Fetch all months from the data
    sorted_months = list(all_historical_metrics.keys())
    sorted_months.sort()

    # For each set of data
    for i in range(0, len(sorted_months)):

        # Payments processed per creator, for creators who processed payments
        all_historical_metrics[sorted_months[i]]['PaymentsProcessedPerCreator_AllCreatorsWhoProcessedPayments'] = 0
        if ('NumCreatorsProcessedAnyAmount' in all_historical_metrics[sorted_months[i]]) \
                and ('NetPaymentsProcessed' in all_historical_metrics[sorted_months[i]]) \
                and (all_historical_metrics[sorted_months[i]]['NumCreatorsProcessedAnyAmount'] > 0):
            all_historical_metrics[sorted_months[i]]['PaymentsProcessedPerCreator_AllCreatorsWhoProcessedPayments'] = \
                all_historical_metrics[sorted_months[i]]['NetPaymentsProcessed'] \
                / all_historical_metrics[sorted_months[i]]['NumCreatorsProcessedAnyAmount']

        # Payments processed per financially-active creator
        all_historical_metrics[sorted_months[i]]['PaymentsProcessedPerFinanciallyActiveCreator_AnyAmount'] = 0
        if ('NumFinanciallyActiveCreators_AnyAmount' in all_historical_metrics[sorted_months[i]]) \
                and ('NetPaymentsProcessed' in all_historical_metrics[sorted_months[i]]) \
                and (all_historical_metrics[sorted_months[i]]['NumFinanciallyActiveCreators_AnyAmount'] > 0):
            all_historical_metrics[sorted_months[i]]['PaymentsProcessedPerFinanciallyActiveCreator_AnyAmount'] = \
                all_historical_metrics[sorted_months[i]]['NetPaymentsProcessed'] \
                / all_historical_metrics[sorted_months[i]]['NumFinanciallyActiveCreators_AnyAmount']

        # Payments processed per paying patron
        all_historical_metrics[sorted_months[i]]['PaymentsProcessedPerPayingPatron'] = 0
        if ('NumPatronsProcessingPayments' in all_historical_metrics[sorted_months[i]]) \
                and ('NetPaymentsProcessed' in all_historical_metrics[sorted_months[i]]) \
                and (all_historical_metrics[sorted_months[i]]['NumPatronsProcessingPayments'] > 0):
            all_historical_metrics[sorted_months[i]]['PaymentsProcessedPerPayingPatron'] = \
                all_historical_metrics[sorted_months[i]]['NetPaymentsProcessed'] \
                / all_historical_metrics[sorted_months[i]]['NumPatronsProcessingPayments']

        # Payments processed per financially-active patron
        all_historical_metrics[sorted_months[i]]['PaymentsProcessedPerFinanciallyActivePatron'] = 0
        if ('NumFinanciallyActivePatrons' in all_historical_metrics[sorted_months[i]]) \
                and ('NetPaymentsProcessed' in all_historical_metrics[sorted_months[i]]) \
                and (all_historical_metrics[sorted_months[i]]['NumFinanciallyActivePatrons'] > 0):
            all_historical_metrics[sorted_months[i]]['PaymentsProcessedPerFinanciallyActivePatron'] = \
                all_historical_metrics[sorted_months[i]]['NetPaymentsProcessed'] \
                / all_historical_metrics[sorted_months[i]]['NumFinanciallyActivePatrons']

        # Payments processed per paying pledge
        all_historical_metrics[sorted_months[i]]['PaymentsProcessedPerPayingPledge'] = 0
        if ('NumPledgesProcessingPayments' in all_historical_metrics[sorted_months[i]]) \
                and ('NetPaymentsProcessed' in all_historical_metrics[sorted_months[i]]) \
                and (all_historical_metrics[sorted_months[i]]['NumPledgesProcessingPayments'] > 0):
            all_historical_metrics[sorted_months[i]]['PaymentsProcessedPerPayingPledge'] = \
                all_historical_metrics[sorted_months[i]]['NetPaymentsProcessed'] \
                / all_historical_metrics[sorted_months[i]]['NumPledgesProcessingPayments']

        # Average number of paying patrons per processing creator
        all_historical_metrics[sorted_months[i]]['NumPayingPatronsPerProcessingCreator'] = 0
        if ('NumPatronsProcessingPayments' in all_historical_metrics[sorted_months[i]]) \
                and ('NumCreatorsProcessedAnyAmount' in all_historical_metrics[sorted_months[i]]) \
                and (all_historical_metrics[sorted_months[i]]['NumCreatorsProcessedAnyAmount'] > 0):
            all_historical_metrics[sorted_months[i]]['NumPayingPatronsPerProcessingCreator'] = \
                all_historical_metrics[sorted_months[i]]['NumPatronsProcessingPayments'] \
                / all_historical_metrics[sorted_months[i]]['NumCreatorsProcessedAnyAmount']

        # Number of financially-active patrons per financially-active creator
        all_historical_metrics[sorted_months[i]]['NumFinanciallyActivePatronsPerFinanciallyActiveCreator'] = 0
        if ('NumFinanciallyActivePatrons' in all_historical_metrics[sorted_months[i]]) \
                and ('NumFinanciallyActiveCreators_AnyAmount' in all_historical_metrics[sorted_months[i]]) \
                and (all_historical_metrics[sorted_months[i]]['NumFinanciallyActiveCreators_AnyAmount'] > 0):
            all_historical_metrics[sorted_months[i]]['NumFinanciallyActivePatronsPerFinanciallyActiveCreator'] = \
                all_historical_metrics[sorted_months[i]]['NumFinanciallyActivePatrons'] \
                / all_historical_metrics[sorted_months[i]]['NumFinanciallyActiveCreators_AnyAmount']

        # Number of paying pledges per paying patron
        all_historical_metrics[sorted_months[i]]['NumPledgesProcessedPerPayingPatron'] = 0
        if ('NumPledgesProcessingPayments' in all_historical_metrics[sorted_months[i]]) \
                and ('NumPatronsProcessingPayments' in all_historical_metrics[sorted_months[i]]) \
                and (all_historical_metrics[sorted_months[i]]['NumPatronsProcessingPayments'] > 0):
            all_historical_metrics[sorted_months[i]]['NumPledgesProcessedPerPayingPatron'] = \
                all_historical_metrics[sorted_months[i]]['NumPledgesProcessingPayments'] \
                / all_historical_metrics[sorted_months[i]]['NumPatronsProcessingPayments']

        # Number of financially-active pledges per financially-active patron
        all_historical_metrics[sorted_months[i]]['NumFinanciallyActivePledgesPerFinanciallyActivePatron'] = 0
        if ('NumFinanciallyActivePledges' in all_historical_metrics[sorted_months[i]]) \
                and ('NumFinanciallyActivePatrons' in all_historical_metrics[sorted_months[i]]) \
                and (all_historical_metrics[sorted_months[i]]['NumFinanciallyActivePatrons'] > 0):
            all_historical_metrics[sorted_months[i]]['NumFinanciallyActivePledgesPerFinanciallyActivePatron'] = \
                all_historical_metrics[sorted_months[i]]['NumFinanciallyActivePledges'] \
                / all_historical_metrics[sorted_months[i]]['NumFinanciallyActivePatrons']

        # Number of comments per financially-active patron
        all_historical_metrics[sorted_months[i]]['NumCommentsPerFinanciallyActivePatron'] = 0
        if ('NumComments' in all_historical_metrics[sorted_months[i]]) \
                and ('NumFinanciallyActivePatrons' in all_historical_metrics[sorted_months[i]]) \
                and (all_historical_metrics[sorted_months[i]]['NumFinanciallyActivePatrons'] > 0):
            all_historical_metrics[sorted_months[i]]['NumCommentsPerFinanciallyActivePatron'] = \
                all_historical_metrics[sorted_months[i]]['NumComments'] \
                / all_historical_metrics[sorted_months[i]]['NumFinanciallyActivePatrons']

        # Number of likes per financially-active patron
        all_historical_metrics[sorted_months[i]]['NumLikesPerFinanciallyActivePatron'] = 0
        if ('NumLikes' in all_historical_metrics[sorted_months[i]]) \
                and ('NumFinanciallyActivePatrons' in all_historical_metrics[sorted_months[i]]) \
                and (all_historical_metrics[sorted_months[i]]['NumFinanciallyActivePatrons'] > 0):
            all_historical_metrics[sorted_months[i]]['NumLikesPerFinanciallyActivePatron'] = \
                all_historical_metrics[sorted_months[i]]['NumLikes'] \
                / all_historical_metrics[sorted_months[i]]['NumFinanciallyActivePatrons']

        # Payments processed per Patreon employee
        all_historical_metrics[sorted_months[i]]['NetPaymentsProcessedPerEmployee'] = 0
        if ('NumEmployees' in all_historical_metrics[sorted_months[i]]) \
                and ('NetPaymentsProcessed' in all_historical_metrics[sorted_months[i]]) \
                and (all_historical_metrics[sorted_months[i]]['NumEmployees'] > 0):
            all_historical_metrics[sorted_months[i]]['NetPaymentsProcessedPerEmployee'] = \
                all_historical_metrics[sorted_months[i]]['NetPaymentsProcessed'] \
                / all_historical_metrics[sorted_months[i]]['NumEmployees']

    ############################################################
    # Perform some final clean-up on all historical metrics
    ############################################################

    # Pending near-term updates:
    # Add a section for NPS scores
    # Track the number of people who convert their accounts to creators accounts
    # (not made money, but converted their profile)
    # Add a section for creator retention and payments by cohort, and another section for patrons
    # Count the total number of payments processed
    # Account for refunds when computing earnings per creator, particularly when computing the number
    # of hundred-dollar creators

    # Pending long-term updates:
    # Add a section to show breakdowns of pledge amounts, patron payments, and creator payments

    # Only keep data for months in which processed payments are defined
    all_historical_metrics = {
        key: value
        for key, value in all_historical_metrics.items()
        if 'PaymentsSuccessfullyProcessed' in value.keys()
    }
    # Only keep data for months in which processed payments are greater than zero
    all_historical_metrics = {
        key: value
        for key, value in all_historical_metrics.items()
        if value['PaymentsSuccessfullyProcessed'] > 0
    }

    # Sort the result by month
    all_historical_metrics = OrderedDict(sorted(all_historical_metrics.items(), key=lambda x: x[0]))

    ############################################################
    # Add the company's main KPIs
    ############################################################

    # Compute the main company KPIs
    main_company_kpis = build_dashboard_data_main_company_kpis(all_historical_metrics)

    ############################################################
    # Return the final result
    ############################################################
    # Organize all results into a single dict
    return {
        'All Historical Metrics': all_historical_metrics,
        'Main Company KPIs': main_company_kpis
    }


def build_ordered_list_of_dashboard_data_keys_all_historical_metrics():
    """ This function builds an ordered list of historical metrics to be shown
        in a data dashboard. Each item in the list is a key found in the
        dashboardDataArray returned by BuildDashboardData_all_historical_metrics()

        :return: dashboardDataKeys - A list of lists of metric keys, arranged
                according to the order in which metrics display in a dashboard.
                Each outer list (dashboardDataKeys[1:n]) represents a section
                of the dashboard. Each item is a dict containing the metric's
                key and the metric's class (either percent, dollars, or count).

    """
    # Create an dict to store the spec
    dashboard_data_keys = OrderedDict()

    # Section: Payments
    dashboard_data_keys['payments_stats'] = [
        {
            'key': 'PercentGrowth_NetPaymentsProcessed',
            'label': '% Growth in net payments processed',
            'class': 'percent',
            'row_class': 'section header'
        },
        {
            'key': 'NetPaymentsProcessed',
            'label': 'Net Payments Processed',
            'class': 'dollars'
        },
        {
            'key': 'PaymentsSuccessfullyProcessed',
            'label': 'Payments Successfully Processed',
            'class': 'dollars'
        },
        {
            'key': 'RefundsProcessed',
            'label': 'Refunds Processed',
            'class': 'dollars'
        }
    ]
    # Section: Creators processing at least $100
    dashboard_data_keys['creator_100_stats'] = [
        {
            'key': 'PercentGrowth_NumCreatorsProcessedAtLeast100',
            'label': '% Growth in number of creators processing at least $100',
            'class': 'percent', 'row_class': 'section header'
        },
        {
            'key': 'NumCreatorsProcessedAtLeast100',
            'label': 'Number of creators processing at least $100',
            'class': 'count'
        },
        {
            'key': 'NetIncreaseInNumCreatorsProcessedAtLeast100',
            'label': 'Net increase in number of creators processing at least $100',
            'class': 'count'
        },
        {
            'key': 'NumCreatorsProcessedAtLeast100ForFirstTime',
            'label': 'Number of creators processing at least $100 for the first time',
            'class': 'count'
        }
    ]
    # Section: Patrons
    dashboard_data_keys['patron_stats'] = [
        {
            'key': 'PercentGrowth_NumPatronsProcessingPayments',
            'label': '% Growth in the number of patrons processing payments',
            'class': 'percent',
            'row_class': 'section header'
        },
        {
            'key': 'NumPatronsProcessingPayments',
            'label': 'Number of patrons processing payments',
            'class': 'count'
        },
        {
            'key': 'PaymentsProcessedPerPayingPatron',
            'label': 'Payments processed per paying patron',
            'class': 'dollars'
        },
        {
            'key': 'NumPledgesProcessedPerPayingPatron',
            'label': 'Number of pledges processed per paying patron',
            'class': 'count'
        },
        {
            'key': 'PercentGrowth_NumFinanciallyActivePatrons',
            'label': '% Growth in the number of financially-active patrons',
            'class': 'percent'
        },
        {
            'key': 'NumFinanciallyActivePatrons',
            'label': 'Number of financially-active patrons',
            'class': 'count'
        },
        {
            'key': 'PaymentsProcessedPerFinanciallyActivePatron',
            'label': 'Payments processed per financially-active patron',
            'class': 'dollars'
        },
        {
            'key': 'NumFinanciallyActivePledgesPerFinanciallyActivePatron',
            'label': 'Number of financially-active pledges per financially-active patron',
            'class': 'count'
        },
        {
            'key': 'NetIncrease_NumFinanciallyActivePatrons',
            'label': 'Net increase in number of financially-active patrons',
            'class': 'count'
        },
        {
            'key': 'NumPatronsMadeFirstPayment',
            'label': 'Number of patrons paying for the first time',
            'class': 'count'
        },
        {
            'key': 'NumPatronsChurned',
            'label': 'Number of patrons churned',
            'class': 'count'
        },
        {
            'key': 'PatronChurnRate',
            'label': 'Patron churn rate',
            'class': 'percent'
        }
    ]
    # Section: Creators earning any amount
    dashboard_data_keys['creator_stats'] = [
        {
            'key': 'PercentGrowth_NumCreatorsProcessedAnyAmount',
            'label': '% Growth in the number of creators processing any amount',
            'class': 'percent',
            'row_class': 'section header'
        },
        {
            'key': 'NumCreatorsProcessedAnyAmount',
            'label': 'Number of creators processing any amount',
            'class': 'count'
        },
        {
            'key': 'PaymentsProcessedPerCreator_AllCreatorsWhoProcessedPayments',
            'label': 'Payments processed per paid creator (processing any amount)',
            'class': 'dollars'
        },
        {
            'key': 'NumPayingPatronsPerProcessingCreator',
            'label': 'Number of paying patrons per paid creator (processing any amount)',
            'class': 'count'
        },
        {
            'key': 'PercentGrowth_NumFinanciallyActiveCreators_AnyAmount',
            'label': '% Growth in the number of financially-active creators processing any amount',
            'class': 'percent'
        },
        {
            'key': 'NumFinanciallyActiveCreators_AnyAmount',
            'label': 'Number of creators financially-active this month (processing any amount)',
            'class': 'count'
        },
        {
            'key': 'PaymentsProcessedPerFinanciallyActiveCreator_AnyAmount',
            'label': 'Payments processed per financially-active creator (processing any amount)',
            'class': 'dollars'
        },
        {
            'key': 'NumFinanciallyActivePatronsPerFinanciallyActiveCreator',
            'label': 'Number of financially-active patrons per financially-active creator (processing any amount)',
            'class': 'count'
        },
        {
            'key': 'NetIncrease_NumFinanciallyActiveCreators_AnyAmount',
            'label': 'Net increase in the number of creators earning any amount',
            'class': 'count'
        },
        {
            'key': 'NumCreatorsProcessedFirstPayment',
            'label': 'Number of creators processing payments for the first time',
            'class': 'count'
        },
        {
            'key': 'NumCreatorsChurned',
            'label': 'Number of churned creators',
            'class': 'count'
        },
        {
            'key': 'CreatorChurnRate',
            'label': 'Creator churn rate',
            'class': 'percent'
        }
    ]
    # Section: Pledges
    dashboard_data_keys['pledge_stats'] = [
        {
            'key': 'PercentGrowth_NumPledgesProcessingPayments',
            'label': '% Growth in the number of pledges that processed payments',
            'class': 'percent',
            'row_class': 'section header'
        },
        {
            'key': 'NumPledgesProcessingPayments',
            'label': 'Number of pledges that processed payments',
            'class': 'count'
        },
        {
            'key': 'PaymentsProcessedPerPayingPledge',
            'label': 'Payments processed per pledge processed',
            'class': 'dollars'
        },
        {
            'key': 'PercentGrowth_NumFinanciallyActivePledges',
            'label': '% Growth in the number of financially-active pledges',
            'class': 'percent',
            'row_class': 'section header'
        },
        {
            'key': 'NumFinanciallyActivePledges',
            'label': 'Number of pledges financially-active this month',
            'class': 'count'
        },
        {
            'key': 'NumPledgesFirstPaid',
            'label': 'Number of new paying pledges',
            'class': 'count'
        },
        {
            'key': 'NumPledgesChurned_AllCauses',
            'label': 'Number of churned pledges (All pledges that cease to be paid)',
            'class': 'count'
        },
        {
            'key': 'NumPledgesChurned_StrictlyPledgeChurn',
            'label': 'Number of churned pledges (Pledge churn only; excludes creator churn)',
            'class': 'count'
        },
        {
            'key': 'PledgeChurnRate_StrictlyPledgeChurn',
            'label': 'Pledge churn rate (Pledge churn only; excludes creator churn)',
            'class': 'percent'
        }
    ]
    # Section: Engagement
    dashboard_data_keys['engagement_stats'] = [
        {
            'key': 'NumCommentsPerFinanciallyActivePatron',
            'label': 'Number of comments per financially active patron',
            'class': 'count',
            'row_class': 'section header'
        },
        {
            'key': 'NumLikesPerFinanciallyActivePatron',
            'label': 'Number of likes percent financially active patron',
            'class': 'count'
        },
        {
            'key': 'NumComments',
            'label': 'Total number of comments on Patreon',
            'class': 'count'
        },
        {
            'key': 'NumLikes',
            'label': 'Total number of likes on Patreon',
            'class': 'count'
        }
    ]

    # Section: Productivity
    dashboard_data_keys['productivity_stats'] = [
        {
            'key': 'NetPaymentsProcessedPerEmployee',
            'label': 'Net payments processed per Patreon employee',
            'class': 'dollars',
            'row_class': 'section header'
        },
        {
            'key': 'NumEmployees',
            'label': 'Number of Patreon employees',
            'class': 'count'
        }
    ]

    # QA: Test if every key listed in this function appears in the results of the other function
    # for l in dashboard_data_keys:
    #     for d in dashboard_data_keys[l]:
    #         if (d['key'] not in dashboardDataArray[5].keys()):
    #             print('Not found in the array: ', d['key'])

    # Return the result
    return dashboard_data_keys


def build_dashboard_data_main_company_kpis(all_historical_metrics=None):
    """ This function builds an ordered list of metrics to be shown in a
        top-level view of the company's main KPIs. Each item in the list is a
        key found in the dashboardDataArray returned by
        BuildDashboardData_main_company_kpis()

    :param all_historical_metrics:  an ordered dict containing {
                                        '2013-05': {data for that month},
                                        '2013-06': {data for that month},
                                        etc.
                                    }
    :return: main_company_kpis - a single dict containing the metrics to display
    """
    # PAYMENTS PROCESSED:
    # Aug forecast: (X% growth, $Y processed)
    # Aug to-date: (X%, $Y)
    # Jul: (X%, $Y)
    # Jun: (X%, $Y)
    # HUNDRED-DOLLAR CREATORS:
    # Aug forecast: (X% growth, Y creators)
    # Aug to-date: (X%, Y)
    # Jul: (X%, Y)
    # Jun: (X%, Y)
    # NPS AMONG HUNDRED-DOLLAR CREATORS:
    # Aug: X
    # Jul: X
    # Jun: X

    # Determine how far into this month we are (e.g., August 21 is 21 days into the month)
    current_month = datetime.date.today()

    ################################################
    # PAYMENTS FORECAST
    ################################################
    #  Define the date from which data from "pledges" can be trusted
    PLEDGE_DATA_START_DATE = '2015-06-01'
    # Define the maximum number of historical months to be used for a forecast
    MAX_MONTHS_TO_USE_FOR_FORECAST = 4
    # Define a dict to store results needed for the forecast
    forecast_data = OrderedDict()

    # Find the number and value of pledges by month, considering only this much
    # into each month, going back as far as the 'pledges' table allows, or just
    # the preceding N months
    month_i = 0
    while True:
        # Define the SQL query
        if month_i == 0:
            _sql = """SELECT DATE_FORMAT(DATE_ADD(CONVERT_TZ(NOW(),'GMT','US/Pacific'), INTERVAL -%s MONTH), '%%%%Y-%%%%m') AS Month, DATE_FORMAT(CONVERT_TZ(NOW(),'GMT','US/Pacific'),'%%%%d') AS DaysIntoMonth, COUNT(pledge_id) AS NumPledges, SUM(amount_cents/100) AS PledgeAmountDollars
                     FROM pledges WHERE deleted_at IS NULL;""" %(month_i)
        else:
            _sql = """SELECT DATE_FORMAT(DATE_ADD(CONVERT_TZ(NOW(),'GMT','US/Pacific'), INTERVAL -%s MONTH), '%%%%Y-%%%%m') AS Month, DATE_FORMAT(CONVERT_TZ(NOW(),'GMT','US/Pacific'),'%%%%d')  AS DaysIntoMonth, COUNT(pledge_id) AS NumPledges, SUM(amount_cents/100) AS PledgeAmountDollars FROM pledges
                            WHERE created_at<DATE_ADD(CONVERT_TZ(NOW(),'GMT','US/Pacific'), INTERVAL -%s MONTH) AND (deleted_at IS NULL OR deleted_at>=DATE_ADD(CONVERT_TZ(NOW(),'GMT','US/Pacific'), INTERVAL -%s MONTH));""" % (month_i, month_i, month_i)
        # print(_sql)
        # Execute the SQL query
        results = db.engine.execute(_sql)
        # Store the results in an array
        for result in results:
            this_month = result[0]
            if this_month not in forecast_data.keys():
                forecast_data[this_month] = {}
            forecast_data[this_month]['DaysIntoMonth'] = result[1]
            forecast_data[this_month]['NumPledges_ToDate'] = result[2]
            forecast_data[this_month]['PledgeAmountDollars_ToDate'] = result[3]

        # If we are at the pledge data start date, or if we have hit the number
        # of months needed to forecast, exit the loop
        if (str(add_months(current_month, -month_i).strftime("%Y-%m-01")) == PLEDGE_DATA_START_DATE
                or month_i == MAX_MONTHS_TO_USE_FOR_FORECAST):
            break
        # Increment the current month
        month_i += 1

    # Find the number and value of pledges by month, considering complete months
    month_i = 0
    while True:
        # Define the SQL query
        if month_i == 0:
            # If this is month 0, then the month isn't complete yet. Skip it and move to the next month.
            month_i += 1
            continue
        else:
            _sql = """SELECT DATE_FORMAT(DATE_ADD(CONVERT_TZ(NOW(),'GMT','US/Pacific'), INTERVAL -%s MONTH), '%%%%Y-%%%%m') AS Month, COUNT(pledge_id) AS NumPledges_AtMonthEnd, SUM(amount_cents/100) AS PledgeAmountDollars_AtMonthEnd FROM pledges
                            WHERE CONVERT_TZ(created_at,'GMT','US/Pacific')<DATE_FORMAT(DATE_ADD(CONVERT_TZ(NOW(),'GMT','US/Pacific'), INTERVAL -%s MONTH), '%%%%Y-%%%%m-01')
                            AND (deleted_at IS NULL OR CONVERT_TZ(deleted_at,'GMT','US/Pacific')>=DATE_FORMAT(DATE_ADD(CONVERT_TZ(NOW(),'GMT','US/Pacific'), INTERVAL -%s MONTH), '%%%%Y-%%%%m-01'));""" %(month_i, month_i-1, month_i-1)

        # Execute the SQL query
        results = db.engine.execute(_sql)
        # Store the results in an array
        for result in results:
            this_month = result[0]
            if this_month not in forecast_data.keys():
                forecast_data[this_month] = {}
            forecast_data[this_month]['NumPledges_AtMonthEnd'] = result[1]
            forecast_data[this_month]['PledgeAmountDollars_AtMonthEnd'] = result[2]
        # If we are at the pledge data start date, or if we have hit the number
        # of months needed to forecast, exit the loop
        if (str(add_months(current_month, -month_i).strftime("%Y-%m-01")) == PLEDGE_DATA_START_DATE
                or month_i == MAX_MONTHS_TO_USE_FOR_FORECAST):
            break
        # Increment the current month
        month_i += 1

    # Find a typical ratio between total pledges made thus far and the total
    # pledges in the month, and to the net payments processed in the month
    ratio_pledgesToDate_to_pledgesMonthEnd = []
    ratio_pledgesMonthEnd_to_paymentsProcessedMonthEnd = []
    for m in forecast_data.keys():
        # Skip the current month; it isn't complete
        if m == current_month.strftime("%Y-%m"):
            continue
        else:
            # Ratio to total pledges at month end
            ratio_pledgesToDate_to_pledgesMonthEnd.append(
                forecast_data[m]['PledgeAmountDollars_ToDate']
                / forecast_data[m]['PledgeAmountDollars_AtMonthEnd']
            )
            # Ratio to net payments processed at month end
            ratio_pledgesMonthEnd_to_paymentsProcessedMonthEnd.append(
                forecast_data[m]['PledgeAmountDollars_AtMonthEnd']
                / all_historical_metrics[m]['NetPaymentsProcessed']
            )
    # Take the average
    ratio_pledgesToDate_to_pledgesMonthEnd = \
        sum(ratio_pledgesToDate_to_pledgesMonthEnd) \
        / len(ratio_pledgesToDate_to_pledgesMonthEnd)
    ratio_pledgesMonthEnd_to_paymentsProcessedMonthEnd = \
        sum(ratio_pledgesMonthEnd_to_paymentsProcessedMonthEnd) \
        / len(ratio_pledgesMonthEnd_to_paymentsProcessedMonthEnd)

    # Estimate the value of total pledges for this month
    pledge_ValueForecast = \
        forecast_data[current_month.strftime("%Y-%m")]['PledgeAmountDollars_ToDate'] \
        / ratio_pledgesToDate_to_pledgesMonthEnd
    # Estimate the value and growth of net payments processed this month
    netPaymentsProcessed_ValueForecast = \
        pledge_ValueForecast \
        / ratio_pledgesMonthEnd_to_paymentsProcessedMonthEnd
    netPaymentsProcessed_GrowthForecast = (
        netPaymentsProcessed_ValueForecast
        / all_historical_metrics[list(forecast_data.keys())[1]]['NetPaymentsProcessed']
        - 1) * 100

    ################################################
    # HUNDRED-DOLLAR CREATOR FORECAST
    ################################################
    # Define a payments threshold, expressed in dollars
    PAYMENT_THRESHOLD_FOR_CREATOR_KPI_DOLLARS = 100

    # Find the number of creators by month, considering only this much into
    # each month, for the preceding N months
    month_i = 0
    while True:
        # Define the SQL query
        _sql = """SELECT DATE_FORMAT(DATE_ADD(CONVERT_TZ(NOW(),'GMT','US/Pacific'), INTERVAL -%s MONTH), '%%%%Y-%%%%m') AS Month, DATE_FORMAT(CONVERT_TZ(NOW(),'GMT','US/Pacific'),'%%%%d') AS DaysIntoMonth, COUNT(a.CID) AS NumCreators_ToDate FROM
               (SELECT CID, SUM(Pledge/100) AS PaymentsProcessed FROM tblRewardsGive WHERE Status=1 AND CONVERT_TZ(Created,'GMT','US/Pacific')>=DATE_FORMAT(DATE_ADD(CONVERT_TZ(NOW(),'GMT','US/Pacific'), INTERVAL -%s MONTH), '%%%%Y-%%%%m-01')
               AND CONVERT_TZ(Created,'GMT','US/Pacific')<DATE_ADD(CONVERT_TZ(NOW(),'GMT','US/Pacific'), INTERVAL -%s MONTH) GROUP BY CID) a
                WHERE a.PaymentsProcessed>=%s;""" %(month_i,month_i,month_i,PAYMENT_THRESHOLD_FOR_CREATOR_KPI_DOLLARS)

        # Execute the SQL query
        results = db.engine.execute(_sql)
        # Store the results in an array
        for result in results:
            this_month = result[0]
            if this_month not in forecast_data.keys():
                forecast_data[this_month] = {}
            forecast_data[this_month]['NumCreatorsProcessedAtLeastThreshold_ToDate'] = result[2]
        # If we are at the pledge data start date, or if we have hit the number
        # of months needed to forecast, exit the loop
        if month_i == MAX_MONTHS_TO_USE_FOR_FORECAST:
            break
        # Increment the current month
        month_i += 1

    # Find the number of creators by month, considering complete months
    month_i = 0
    while True:
        # Define the SQL query
        if month_i == 0:
            # If this is month 0, then the month isn't complete yet. Skip it and move to the next month.
            month_i += 1
            continue
        else:
            _sql = """SELECT DATE_FORMAT(DATE_ADD(CONVERT_TZ(NOW(),'GMT','US/Pacific'), INTERVAL -%s MONTH), '%%%%Y-%%%%m') AS Month, COUNT(a.CID) AS NumCreators_AtMonthEnd FROM
               (SELECT CID, SUM(Pledge/100) AS PaymentsProcessed FROM tblRewardsGive WHERE Status=1 AND DATE_FORMAT(CONVERT_TZ(Created,'GMT','US/Pacific'),'%%%%Y-%%%%m')=DATE_FORMAT(DATE_ADD(CONVERT_TZ(NOW(),'GMT','US/Pacific'), INTERVAL -%s MONTH), '%%%%Y-%%%%m') GROUP BY CID) a
                WHERE a.PaymentsProcessed>=%s;""" %(month_i,month_i, PAYMENT_THRESHOLD_FOR_CREATOR_KPI_DOLLARS)

        # Execute the SQL query
        results = db.engine.execute(_sql)
        # Store the results in an array
        for result in results:
            this_month = result[0]
            if this_month not in forecast_data.keys():
                forecast_data[this_month] = {}
            forecast_data[this_month]['NumCreatorsProcessedAtLeastThreshold_AtMonthEnd'] = result[1]
        # If we are at the pledge data start date, or if we have hit the number
        # of months needed to forecast, exit the loop
        if month_i == MAX_MONTHS_TO_USE_FOR_FORECAST:
            break
        # Increment the current month
        month_i += 1

    # Define this, last, and 2 months ago
    monthMinus0 = list(forecast_data.keys())[0]
    monthMinus1 = list(forecast_data.keys())[1]
    monthMinus2 = list(forecast_data.keys())[2]

    # Find a typical ratio between number of creators thus far and the total number in the month
    ratio_creatorsToDate_to_creatorsMonthEnd = []
    for m in forecast_data.keys():
        # Skip the current month; it isn't complete
        if m == current_month.strftime("%Y-%m"):
            continue
        else:
            # Ratio to total creators at month end
            ratio_creatorsToDate_to_creatorsMonthEnd.append(
                forecast_data[m]['NumCreatorsProcessedAtLeastThreshold_ToDate']
                / forecast_data[m]['NumCreatorsProcessedAtLeastThreshold_AtMonthEnd']
            )
    # Take the average
    ratio_creatorsToDate_to_creatorsMonthEnd = \
        sum(ratio_creatorsToDate_to_creatorsMonthEnd) \
        / len(ratio_creatorsToDate_to_creatorsMonthEnd)
    # Estimate the number of creators for the current month
    creator_CountForecast = \
        forecast_data[current_month.strftime("%Y-%m")]['NumCreatorsProcessedAtLeastThreshold_ToDate'] \
        / ratio_creatorsToDate_to_creatorsMonthEnd
    creator_GrowthForecast = (
        creator_CountForecast
        / all_historical_metrics[monthMinus1]['NumCreatorsProcessedAtLeast100']
        - 1) * 100
    # Compute the growth to-date
    if forecast_data[monthMinus1]['NumCreatorsProcessedAtLeastThreshold_ToDate'] != 0:
        creator_GrowthThisMonthToDate = (
            forecast_data[monthMinus0]['NumCreatorsProcessedAtLeastThreshold_ToDate']
            / forecast_data[monthMinus1]['NumCreatorsProcessedAtLeastThreshold_ToDate']
            - 1) * 100
    else:
        creator_GrowthThisMonthToDate = 0

    # Compute growth to-date in net payments processed
    _sql = """SELECT c.Month, DATE_FORMAT(CONVERT_TZ(NOW(),'GMT','US/Pacific'),'%%d') AS DaysIntoMonth, IFNULL(c.PaymentsSuccessfullyProcessed,0) - IFNULL(c.RefundsProcessed,0) AS NetPaymentsProcessedToDate FROM
        (SELECT a.Month, a.PaymentsSuccessfullyProcessed, b.RefundsProcessed FROM
            (SELECT DATE_FORMAT(CONVERT_TZ(Created,'GMT','US/Pacific'),'%%Y-%%m') AS Month, SUM(Pledge/100) AS PaymentsSuccessfullyProcessed FROM tblRewardsGive
            WHERE Status=1 AND DATE_FORMAT(CONVERT_TZ(Created,'GMT','US/Pacific'),'%%d %%H:%%i:%%S')<DATE_FORMAT(CONVERT_TZ(NOW(),'GMT','US/Pacific'),'%%d %%H:%%i:%%S') GROUP BY Month) a
        LEFT JOIN
            (SELECT DATE_FORMAT(CONVERT_TZ(Created,'GMT','US/Pacific'),'%%Y-%%m') AS Month, SUM(Pledge/100) AS RefundsProcessed FROM tblRewardsGive
            WHERE Status=10 AND DATE_FORMAT(CONVERT_TZ(Created,'GMT','US/Pacific'),'%%d %%H:%%i:%%S')<DATE_FORMAT(CONVERT_TZ(NOW(),'GMT','US/Pacific'),'%%d %%H:%%i:%%S') GROUP BY Month) b
        ON a.Month=b.Month) c
    WHERE c.Month>=DATE_FORMAT(DATE_ADD(CONVERT_TZ(NOW(),'GMT','US/Pacific'), INTERVAL -1 MONTH),'%%Y-%%m')
    ORDER BY Month DESC;"""
    # Execute the SQL query
    results = db.engine.execute(_sql)
    # Store the results in an array
    for result in results:
        this_month = result[0]
        if this_month not in forecast_data.keys():
            forecast_data[this_month] = {}
        forecast_data[this_month]['NetPaymentsProcessed_ValueToDate'] = result[2]
    # If we don't have payments data for this month, add a zero
    if 'NetPaymentsProcessed_ValueToDate' not in forecast_data[monthMinus0].keys():
        forecast_data[monthMinus0]['NetPaymentsProcessed_ValueToDate'] = 0

    # Create a placeholder for results
    main_company_kpis = OrderedDict()
    main_company_kpis[monthMinus0] = ({
        'NetPaymentsProcessed_Growth': 0,
        'NetPaymentsProcessed_Value': 0,
        'NetPaymentsProcessed_GrowthForecast': netPaymentsProcessed_GrowthForecast,
        'NetPaymentsProcessed_ValueForecast': netPaymentsProcessed_ValueForecast,
        'NetPaymentsProcessed_GrowthToDate': (
            forecast_data[monthMinus0]['NetPaymentsProcessed_ValueToDate']
            / forecast_data[monthMinus1]['NetPaymentsProcessed_ValueToDate']
            - 1) * 100,
        'NetPaymentsProcessed_ValueToDate': forecast_data[monthMinus0]['NetPaymentsProcessed_ValueToDate'],
        'CreatorsOverThreshold_Growth': 0,
        'CreatorsOverThreshold_Count': 0,
        'CreatorsOverThreshold_GrowthForecast': creator_GrowthForecast,
        'CreatorsOverThreshold_CountForecast': creator_CountForecast,
        'CreatorsOverThreshold_GrowthToDate': creator_GrowthThisMonthToDate,
        'CreatorsOverThreshold_CountToDate': forecast_data[monthMinus0]['NumCreatorsProcessedAtLeastThreshold_ToDate'],
        'NPS_CreatorsOverThreshold': 0
    })
    main_company_kpis[monthMinus1] = ({
        'NetPaymentsProcessed_Growth': all_historical_metrics[monthMinus1]['PercentGrowth_NetPaymentsProcessed'],
        'NetPaymentsProcessed_Value': all_historical_metrics[monthMinus1]['NetPaymentsProcessed'],
        'CreatorsOverThreshold_Growth': all_historical_metrics[monthMinus1]['PercentGrowth_NumCreatorsProcessedAtLeast100'],
        'CreatorsOverThreshold_Count': all_historical_metrics[monthMinus1]['NumCreatorsProcessedAtLeast100'],
        'NPS_CreatorsOverThreshold': 0
    })
    main_company_kpis[monthMinus2] = ({
        'NetPaymentsProcessed_Growth': all_historical_metrics[monthMinus2]['PercentGrowth_NetPaymentsProcessed'],
        'NetPaymentsProcessed_Value': all_historical_metrics[monthMinus2]['NetPaymentsProcessed'],
        'CreatorsOverThreshold_Growth': all_historical_metrics[monthMinus2]['PercentGrowth_NumCreatorsProcessedAtLeast100'],
        'CreatorsOverThreshold_Count': all_historical_metrics[monthMinus2]['NumCreatorsProcessedAtLeast100'],
        'NPS_CreatorsOverThreshold': 0
    })

    # Return the result
    return main_company_kpis


################################################################################
# HELPER FUNCTIONS
################################################################################

# A function to add months to a given sourcedate, where sourcedate is a datetime format
def add_months(sourcedate, months):
    month = sourcedate.month - 1 + months
    year = int(sourcedate.year + month / 12)
    month = month % 12 + 1
    day = min(sourcedate.day, calendar.monthrange(year, month)[1])
    return datetime.date(year, month, day)


if __name__ == '__main__':
    # Call the functions
    build_dashboard_data()
    build_ordered_list_of_dashboard_data_keys_all_historical_metrics()
    build_dashboard_data_main_company_kpis()
