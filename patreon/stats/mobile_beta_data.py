import datetime
from patreon.model.manager import pledge_mgr, campaign_mgr, post_mgr
from patreon.model.table import User, UsersPushInfo, UserSetting, \
    UserCampaignSetting, Activity, Comment, Actions
from patreon.model.type import ActionType
from patreon.services import redshift, aws


bucket = aws.S3Bucket('patreon-private')


def get_patron_id_strs_for_creator_id(creator_id):
    pledges = pledge_mgr.find_all_by_creator_id(creator_id)
    return [str(pledge.patron_id) for pledge in pledges]


def get_post_id_strs_for_creator_id(creator_id):
    posts = post_mgr.get_posts_by_owner_id(creator_id)
    return [str(post.activity_id) for post in posts]


def num_patrons_with_enabled_pushes(patron_ids, campaign_id):
    users_with_push = User.query.filter(User.user_id.in_(patron_ids)) \
        .join(UsersPushInfo.user).filter(UsersPushInfo.bundle_id == 'com.patreon.iphone.beta')
    users_with_generic_push_on = users_with_push \
        .join(UserSetting, User.user_id == UserSetting.user_id).filter(UserSetting.push_new_comment == 1) \
        .all()
    users_with_campaign_push_on = users_with_push \
        .join(UserCampaignSetting, User.user_id == UserCampaignSetting.user_id) \
        .filter(UserCampaignSetting.campaign_id == campaign_id) \
        .filter((UserCampaignSetting.push_new_comment == 1)
                | (UserCampaignSetting.push_post == 1)
                | (UserCampaignSetting.push_paid_post == 1)
                | (UserCampaignSetting.push_creator_live == 1)) \
        .all()

    generic_ids = [user.user_id for user in users_with_generic_push_on]
    campaign_push_ids = [user.user_id for user in users_with_campaign_push_on]
    return len(set(generic_ids + campaign_push_ids))


def num_patron_comments_this_week(campaign_id):
    one_week_ago = datetime.datetime.now() - datetime.timedelta(days=7)
    return Comment.query \
        .filter(Comment.created_at >= one_week_ago) \
        .join(Comment.post)\
        .filter(Activity.campaign_id == campaign_id) \
        .count()


def num_patron_likes_this_week(campaign_id):
    one_week_ago = datetime.datetime.now() - datetime.timedelta(days=7)
    return Actions.query \
        .filter(Actions.action_type == ActionType.LIKE.value) \
        .filter(Actions.created_at >= one_week_ago) \
        .join(Actions.post).filter(Activity.campaign_id == campaign_id) \
        .count()


def get_mobile_patron_ids(patron_ids):
    query = """SELECT DISTINCT(user_id) FROM session_start
            WHERE user_id IN ('{}')""" \
        .format("', '".join(patron_ids))
    results = redshift.query(query)
    user_ids = [result.get('user_id') for result in results]
    return user_ids


def num_patrons_used_app_this_week(patron_ids):
    query = """SELECT count(DISTINCT(user_id)) FROM session_start
            WHERE event_time >= (localtimestamp - INTERVAL '7 days') AND user_id IN ('{}')""" \
        .format("', '".join(patron_ids))
    return redshift.query_single(query).get('count', 0)


def num_comments_from_app_this_week(post_ids):
    query = """SELECT count(*) FROM comments___commented
            WHERE event_time > (localtimestamp - INTERVAL '7 days')
            AND platform = 'iOS' AND e_post_id IN ('{}')""" \
        .format("', '".join(post_ids))
    return redshift.query_single(query).get('count', 0)


def num_live_chatters_this_week(patron_ids):
    query = """SELECT count(*) FROM comments___commented
            WHERE event_time > (localtimestamp - INTERVAL '7 days') AND platform = 'iOS'
             AND user_id IN ('{}')""" \
        .format("', '".join(patron_ids))
    # " and creator_is_live like 'true'" + \ once we have that
    return redshift.query_single(query).get('count', 0)


def num_likes_from_app_this_week(creator_id):
    incr_query = """SELECT count(*) FROM post_page___edited_like_on_post
            WHERE event_time > (localtimestamp - INTERVAL '7 days')
            AND e_like LIKE 'true' AND platform = 'iOS' AND e_creator_id = '{}'""" \
        .format(creator_id)
    incr = redshift.query_single(incr_query).get('count', 0)
    decr_query = """SELECT count(*) FROM post_page___edited_like_on_post
            WHERE event_time > (localtimestamp - INTERVAL '7 days')
            AND e_like LIKE 'false' AND platform = 'iOS' AND e_creator_id = '{}'""" \
        .format(creator_id)
    decr = redshift.query_single(decr_query).get('count', 0)
    return incr - decr


def total_pledge_delta(creator_id):
    query = """SELECT sum(e_amount_difference) FROM pledge___edit
            WHERE e_creator_id = {} AND event_time > LOCALTIMESTAMP - INTERVAL '7 days'""" \
        .format(creator_id)
    return redshift.query_single(query).get('sum', 0)


def mobile_cohort_pledge_delta(creator_id, mobile_patron_ids):
    query = """SELECT sum(e_amount_difference) FROM pledge___edit
            WHERE e_creator_id = {} AND event_time > LOCALTIMESTAMP - INTERVAL '7 days'
             AND user_id IN ('{}')""" \
        .format(creator_id, "', '".join(mobile_patron_ids))
    return redshift.query_single(query).get('sum', 0)


def total_pledge_deletions(creator_id):
    query = """SELECT count(*) FROM pledge___delete
            WHERE e_creator_id = {} AND event_time > LOCALTIMESTAMP - INTERVAL '7 days'""" \
        .format(creator_id)
    return redshift.query_single(query).get('count', 0)


def mobile_cohort_pledge_deletions(creator_id, mobile_patron_ids):
    query = """SELECT count(*) FROM pledge___delete
            WHERE e_creator_id = {} AND event_time > LOCALTIMESTAMP - INTERVAL '7 days'
            AND user_id IN ('{}')""" \
        .format(creator_id, "', '".join(mobile_patron_ids))
    return redshift.query_single(query).get('count', 0)


def generate_data_for_creator_id(creator_id):
    creator_id = creator_id
    campaign_id = campaign_mgr.try_get_campaign_id_by_user_id_hack(creator_id)

    data = ""
    patron_id_strs = get_patron_id_strs_for_creator_id(creator_id)
    data += "number of patrons: {}\n".format(len(patron_id_strs))
    mobile_patron_id_strs = [str(mobile_patron_id) for mobile_patron_id in get_mobile_patron_ids(patron_id_strs)]
    data += "number who have launched the app ever: {}\n".format(len(mobile_patron_id_strs))

    data += "number who have launched the app this week: {}\n".format(num_patrons_used_app_this_week(patron_id_strs))
    data += "number who have push notifications enabled: {}\n".format(num_patrons_with_enabled_pushes(patron_id_strs, campaign_id))

    data += "total number of comments this week: {}\n".format(num_patron_comments_this_week(campaign_id))
    data += "number of comments from mobile this week: {}\n".format(num_comments_from_app_this_week(get_post_id_strs_for_creator_id(creator_id)))
    # data += "number of people in live chats this week: {}".format(num_live_chatters_this_week(patron_id_strs))
    data += "total number of likes on your posts this week: {}\n".format(num_patron_likes_this_week(campaign_id))
    data += "number of likes on your posts from mobile this week: {}\n".format(num_likes_from_app_this_week(creator_id))

    data += "total change in pledge amounts this week: {}\n".format(total_pledge_delta(creator_id))
    data += "total change in pledge amounts from mobile patrons this week: {}\n".format(mobile_cohort_pledge_delta(creator_id, mobile_patron_id_strs))
    data += "total number of pledge deletions this week: {}\n".format(total_pledge_deletions(creator_id))
    data += "total number of pledge deletions from mobile patrons this week: {}".format(mobile_cohort_pledge_deletions(creator_id, mobile_patron_id_strs))
    return data


def generate_all_creator_data():
    creators = [
        (291801, 'kinda funny'),
        (428439, 'kinda funny games'),
        (51232, 'smarter every day'),
        (31012, 'peter hollens'),
        (46862, 'evynne hollens'),
        (191934, 'home free'),
        (38093, 'taylor davis'),
        (94868, 'kina grannis'),
        (51472, 'rob cesternino'),
        (30777, 'blimey cow'),
        (291627, 'sexplanations'),
        (30881, 'pomplamoose'),
        (301890, 'the comedy button'),
    ]

    data = ""
    for creator_id, creator_name in creators:
        data += "{}:\n{}\n\n".format(creator_name, generate_data_for_creator_id(creator_id))
    return data


def generate_and_upload_all_creator_data():
    data = generate_all_creator_data()
    datestr = datetime.datetime.now().strftime('%B %d %Y')
    bucket.set('mobile_beta_data/{}'.format(datestr), data)


def find_uploaded_data_keys():
    keys = bucket.bucket.list(prefix='mobile_beta_data')
    return [key.name[len('mobile_beta_data/'):] for key in keys]


def get_uploaded_data(key):
    return bucket.get('mobile_beta_data/{}'.format(key))
