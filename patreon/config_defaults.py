"""
config_defaults.py

Default values for configuration. These are overwritten by
the config.py file when it is provided in the runtime of
patreon/.
"""

config_name = "Default Config"
main_server = "localhost"
api_server = "localhost"
php_repo = None

database = None
memcached = None
flume = None
bonfire_host = None

# This should be either "testing", "development", "production"
stage = 'development'

# Enable this to disable all writes to the database.
sandbox = False

# Enable this to get detailed error messages on 500s.
debug = False

# Enable this to have the iOS app notified on push notifs.
push_notifications_enabled = False

# Set this from https://rollbar.com/flubstep/Patreon-Web-Python/settings/general/
rollbar_api_key = None
rollbar_environment = 'development'

# Connect through Amplitude
redshift = None
redshift_search_path = None

# For webclient deploys.
# Get this from https://circleci.com/account/api
circle_smoketests_api_key = None

# For webclient deploys.
# Get this from https://patreon.slack.com/services/4592713712
slack_api_key = None

# Used for youtube's API
google_api_key = "AIzaSyApiTEYf0Rs8WyqctRnjhf_Ar6xzE3r6tQ"

# Used for recaptcha
recaptcha_key = "6Lex0AsTAAAAAGeoDp1hMgcZR0vfWAqo3IFIhOnA"

# Get this from mandrill
mandrill_api_key = None
mandrill_mailer = "StubMailer"

# Amplitude key
amplitude_key = None

# Upload files to s3
file_uploader = "StubUploader"

# Embed.ly
embeder = 'StubEmbedder'
embedly_key = None

image_getter = 'ImageGetter'

logging_enabled = True
darkmode_enabled = True

# S3 buckets
bucket_campaign = 'patreon'
bucket_post = 'test'
bucket_user = 'test'
bucket_comment = 'test'

queuer = 'SQS'

# Used for asynchronous tasks on the task queue
celery_broker = None

# akismet
akismet = {
    'key': '1a069252ac93'
}

# Determines if this server is used for API requests only or HTML requests only
api = False

queues = {
    'zoho': 'prodweb-zoho_update',
    'search-campaign': 'prodweb-campaign_search_update_queue',
    'search-post': 'prodweb-post_search_update_queue'
}

local_webclient_prefix_override = None
local_react_prefix_override = None

# Used for redis connection on log and production
redis = {}

max_attachment_bytes = 300 * 1024 * 1024
allow_authorization_header = False

geoip_user_id = 102503
geoip_license = 'GMlz4TURtElm'

newrelic_servers = ['webpython1', 'webpython2', 'webapi1', 'webapi2']

slack_key = 'xoxp-2194755956-3039489866-8328630577-f621b2'

private_key_path = None

ignore_admin_check = False
