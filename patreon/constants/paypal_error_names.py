DECLINE_ERRORS = {
    'This transaction cannot be processed.',
    'The transaction cannot complete successfully. Instruct the customer to use a non AMEX card.'
    'Could not process your request to accept/deny the transaction',
    'User does not have a good funding source with which to pay.',
    'This transaction cannot be processed without a Credit Card Verification number.',
    'Maximum number of authorization allowed for the order is reached.',
    'Transaction cannot be processed.',
    'Transaction rejected, please contact the buyer.',
    'Authorization has expired',
    'Authorization voided.',
    'This card authorization verification is not a payment transaction.',
    'This transaction cannot be processed. Please enter a valid credit card number and type.',
    'This transaction cannot be processed. Please contact PayPal Customer Service.',
    'This transaction cannot be processed. Please enter a valid Credit Card Verification Number.',
    'This transaction cannot be processed. Please use a valid credit card.',
    'Instruct the customer to retry the transaction using an alternative payment method from the customer\'s PayPal wallet. The transaction did not complete with the customer\'s selected payment method.',
    'The amount exceeds the maximum amount for a single transaction.',
    'Transaction refused because a confirmed address is not available',
    'Transaction failed because has no funding sources',
    'Transaction failed but user has alternate funding source',
    'User\'s account is closed or restricted',
    'Transaction failed, action required by user',
    'Payment could not be completed due to a sender account issue. Please notify the user to contact PayPal Customer Support.',
    'The account for the counterparty is locked or inactive',
    'Account is restricted.',
    'Account is locked or inactive',
    'A Start Date or Issue Number is required.',
    'The issue number of the credit card is invalid.',
    'Buyer did not accept billing agreement'
}

CANCELED_AGREEMENT = {
    'Billing Agreement was cancelled'
}

FRAUD_ERRORS = {
    'Transaction blocked by your settings in FMF',
    'Payment Pending your review in Fraud Management Filters',
    'Payment declined by your Risk Controls settings: PayPal Risk Model.',
    'Transaction refused due to risk model',
    'The transaction was refused because the maximum amount was excused as a result of your Maximum Amount Risk Control Settings.',
    'The transaction was refused because the country was prohibited as a result of your Country Monitor Risk Control Settings.',
}

MONTHLY_MAX_ERRORS = {
    'Transaction would exceed user\'s monthly maximum',
    'The transaction was blocked as it would exceed the sending limit for this buyer.',
}

EMERGENCY_ERRORS = {
    'The Reference ID refers to a transaction or order that is more than 730 days old',
}

UNKNOWN_ERRORS = {
    'This transaction cannot be completed because it violates the PayPal User Agreement.',
    'This transaction cannot be processed. The shipping country is not allowed by the buyer\'s country of residence.',
    'Warning an internal error has occurred. The transaction id may not be correct',
    'Because a complaint case exists on this transaction, only a refund of the full or full remaining amount of the transaction can be issued',
    'You are over the time limit to perform a refund on this transaction',
    'This transaction already has a chargeback filed',

}

INVALID_REQUEST = {
    'Item Category value is invalid. Only Digital or Physical are allowed.',
    'The shipping address must match the user\'s address in the PayPal account.',
    'Credit card details section must be empty; you cannot specify credit card information in a non-direct payment (DCC) reference transaction.',
    'Billing Agreement Id or transaction Id is not valid',
    'The transaction was refused because you cannot send money to yourself.',
    'This transaction cannot be processed due to an unsupported currency.',
    'This transaction cannot be processed without a valid IP address.',
    'A match of the Shipping Address City, State, and Postal Code failed.',
    'The field Shipping Address Country is required',
    'The field Shipping Address Postal Code is required',
    'The field Shipping Address State is required',
    'The field Shipping Address City is required',
    'The field Shipping Address1 is required',
    'There was an error in the Shipping Address Country field',
    'Currency is not supported.',
    'This transaction cannot be processed. The amount to be charged is zero.',
    'Transaction refused because of an invalid argument. See additional error messages for details.',
    'The NotifyURL element value exceeds maximum allowable length.',
    'Invoice ID value exceeds maximum allowable length.',
    'Tax total is invalid.',
    'Handling total is invalid.',
    'Shipping total is invalid.',
    'Item total is invalid.',
    'The totals of the cart item amounts do not match order amounts.',
    'The PayerID? value is invalid.',
    'Order total is invalid.',
    'Order total is missing.',
    'The soft descriptor passed in contains invalid characters',
    'Non-ASCII invoice id is not supported',
    'Invalid payment type argument',
    'Tax calculation mismatch. The tax amount for the regular non-trial billing period is different than the sum of the tax for each item in the cart.',
    'Cart item calculation mismatch. The regular non-trial billing period amount is different than the sum of the amounts for each item in the cart.',
    'Item name, amount and quantity are required if item category is provided.',
    'Token is missing',
    'Express Checkout token was issued for a merchant account other than yours.',
    'Invalid token',
    'This Express Checkout session has expired. Token value is no longer valid.',
    'Either TransactionID or PayerId is required.',
    'A transaction id is required',
    'The partial refund amount must be a positive amount',
    'You can not specify a partial amount with a full refund',
    'A transaction id is required',
    'The transaction id is not valid',
    'Refund Advice is not supported without TransactionId.',
    'The partial refund amount must be less than or equal to the original transaction amount',
    'The partial refund amount must be less than or equal to the remaining amount',
    'The partial refund amount is not valid',
    'The PayerID is invalid. Make the API call again with a valid PayerID.',
    'Can not do a full refund after a partial refund',
    'You can not refund this type of transaction',
    'You can not do a partial refund on this transaction',
    'Transaction refused because of an invalid transaction id value',
    'The PayerID value is invalid.',
    'Refunds to users without PayPal accounts cannot exceed the transaction amount.'
    'Order is voided.',
    'Order has expired.',
    'ReturnURL is missing.',
    'CancelURL is missing.'
}

IDEMPOTENCY_ERROR = {
    'The specified Message Submission ID (specified in MSGSUBID parameter) is a duplicate; result parameters of the original request are attached.',
    'Payment has already been made for this InvoiceID?.',
    'Transaction was already processed',
    'Transaction refused because of an invalid argument. See additional error messages for details.',
    'This transaction has already been fully refunded',
    'Request for the specified Message Submission ID (specified in MSGSUBID parameter) cannot be started until the previous request finishes.',
    'Original transaction completed successfully; however, this response differs from the original response.',
}

TEMPORARY_OUTAGE = {
    'Reference transaction feature not currently available; try again later',
    'This transaction cannot be processed at this time. Please try again later.',
    'The API call has been denied as it has exceeded the permissible call rate limit.',
    'Internal Error',
    'The transaction could not be loaded.',
    'Transaction failed due to internal error',
}

CONFIG_ERROR = {
    'Merchant not enabled for reference transactions',
    'This merchant account is not permitted to set PaymentAction? to Authorization. Please contact Customer Service.',
    'A profile preference is set to automatically deny certain transactions',
    'Preapproved Payments not enabled.',
    'You are not signed up to accept payment for digitally delivered goods.',
    'You\'ve exceeded the receiving limit. This transaction can\'t be completed.'
    'Authentication/Authorization Failed',
    'Authentication Failed',
    'Authorization Failed',
    'You do not have permission to refund this transaction',
    'You do not have permissions to make this API call',
    'You do not have a verified ACH',
    'You have not filled out the Direct Debit Instruction',
    'You are not enabled for this feature.',
    'The Funding source provided is not usable. Please provide different funding source.',
    'Security header is not valid',
    'The CE agreement for processing Mastercard is not accepted.',
}
