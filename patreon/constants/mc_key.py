from enum import Enum

DELIM = '|*|'


class MemCachedKey(Enum):
    USER                    = 'usermodel:'
    MESSAGE_COUNT           = 'messagecount:'
    SUM_PLEDGES_TO          = 'userrewardsmoney'
    USER_SUPPORT_CREATION   = 'usersupportshare'
    SHARE                   = 'share'
    SHARE_PATRONS           = 'sharepatrons'
    USER_SHARES             = 'usershares'
    USER_ACTIVITY           = 'useractivity'
    COUNT_CREATIONS         = 'countcreations'
    COUNT_ACTIVITY          = 'countactivity'


def user_support_creation(patron_id):
    return MemCachedKey.USER_SUPPORT_CREATION.value + str(patron_id)\
        + DELIM + str(1)


def user_shares(owner_id, action_type):
    return MemCachedKey.USER_SHARES.value + str(owner_id)\
        + DELIM + str(action_type) \
        + DELIM + str(1)


def user_activity(owner_id, sort_type):
    return MemCachedKey.USER_ACTIVITY.value + str(owner_id)\
        + DELIM + str(sort_type) \
        + DELIM + str(1)


def count_creations(campaign_id):
    return MemCachedKey.COUNT_CREATIONS.value + str(campaign_id)


def count_activity(campaign_id):
    return MemCachedKey.COUNT_ACTIVITY.value + str(campaign_id)


def share(post_id):
    return MemCachedKey.SHARE.value + str(post_id)
