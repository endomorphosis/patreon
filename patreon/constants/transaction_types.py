from enum import Enum


class TxnTypes(Enum):
    credit_purchase = 'CreditPurchase'
    credit_adjustment = 'CreditAdjustment'
