from patreon.util.image_processing import ImageSizes

# TODO MARCOS MAX_WIDTH = 32000
# TODO MARCOS MAX_HEIGHT = 32000

DEPRECATED_POST_THUMB       = ImageSizes('old_thumb',       75,  75)
POST_THUMB_SQUARE_SMALL     = ImageSizes('square_small',    44,  44)
POST_THUMB_SQUARE_SMALL_2   = ImageSizes('square_small_2',  88,  88)
POST_THUMB_SQUARE_LARGE     = ImageSizes('square_large',    620,  620)
POST_THUMB_SQUARE_LARGE_2   = ImageSizes('square_large_2',  1240,  1240)

DEPRECATED_POST_THUMB_HUGE  = ImageSizes('huge',    600,    338)
POST_THUMB_SMALL            = ImageSizes('small',   120,    68)
POST_THUMB_SMALL_2          = ImageSizes('small_2', 240,    136)
POST_THUMB_LARGE            = ImageSizes('large',   460,    259)
POST_THUMB_LARGE_2          = ImageSizes('large_2', 920,    518)

# 320*2 + 14*2 + 10
DEPRECATED_POST_SINGLE  = ImageSizes('old_single',  320,    None)
DEPRECATED_POST_DOUBLE  = ImageSizes('old_double',  682,    None)
POST_MEDIUM             = ImageSizes('medium',      620,    None)
POST_MEDIUM_2           = ImageSizes('medium_2',    1240,   None)
POST_LARGE              = ImageSizes('large',       800,    None)
POST_LARGE_2            = ImageSizes('large_2',     1600,   None)

USER_THUMB          = ImageSizes('thumb',           50,     50)
USER_THUMB_2        = ImageSizes('thumb_2',         100,    100)
USER_THUMB_LARGE    = ImageSizes('thumb_large',     120,    120)
USER_THUMB_LARGE_2  = ImageSizes('thumb_large_2',   240,    240)
USER_THUMB_LARGE_3  = ImageSizes('thumb_large_3',   360,    360)

USER_LARGE      = ImageSizes('large',   200,    None)
USER_LARGE_2    = ImageSizes('large_2', 400,    None)

CREATOR_NORMAL      = ImageSizes('normal',      320,    320)
CREATOR_NORMAL_2    = ImageSizes('normal_2',    640,    640)
CREATOR_LARGE       = ImageSizes('large',       714,    None)
CREATOR_LARGE_2     = ImageSizes('large_2',     1428,   None)

POST_SIZES = [
    DEPRECATED_POST_SINGLE,
    DEPRECATED_POST_DOUBLE,
    POST_MEDIUM,
    POST_LARGE,
    POST_MEDIUM_2,
    POST_LARGE_2,
]

POST_THUMB_SIZES = [
    DEPRECATED_POST_THUMB,
    DEPRECATED_POST_THUMB_HUGE,
    POST_THUMB_SQUARE_SMALL,
    POST_THUMB_SQUARE_SMALL_2,
    POST_THUMB_SQUARE_LARGE,
    POST_THUMB_SQUARE_LARGE_2,
    POST_THUMB_SMALL,
    POST_THUMB_SMALL_2,
    POST_THUMB_LARGE,
    POST_THUMB_LARGE_2
]

USER_SIZES = [
    USER_LARGE,
    USER_LARGE_2
]

USER_THUMB_SIZES = [
    USER_THUMB,
    USER_THUMB_2,
    USER_THUMB_LARGE,
    USER_THUMB_LARGE_2,
    USER_THUMB_LARGE_3
]

CREATOR_SIZES = [
    CREATOR_NORMAL,
    CREATOR_NORMAL_2,
    CREATOR_LARGE,
    CREATOR_LARGE_2
]
