import enum


class ErrorLevel(enum.Enum):
    FATAL = 0
    ERROR = 10
    WARNING = 20
    INFO = 30
    DEBUG = 40
