import patreon

VISA = 1
MASTERCARD = 2
DISCOVER = 3
DINERS = 4
JCB = 5
AMERICAN_EXPRESS = 6
PAYPAL = 10
OTHER = 99

REWARD_PENDING = patreon.globalvars.get('REWARD_PENDING')
REWARD_PROCESSED = patreon.globalvars.get('REWARD_PROCESSED')
REWARD_REFUNDED = patreon.globalvars.get('REWARD_REFUNDED')

card_types = {
    1: 'Visa',
    2: 'MasterCard',
    3: 'Discover',
    4: 'Diners Club',
    5: 'JCB',
    6: 'American Express',
    10: 'PayPal',
    99: 'Card'
}

names_to_types = {
    type_name: type_num
    for type_num, type_name in card_types.items()
}

STRIPE_PREFIX = "cus_"
PAYPAL_PREFIX = "B-"
