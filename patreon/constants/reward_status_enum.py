from enum import Enum


class RewardStatus(Enum):
    REWARD_PENDING      = 0
    REWARD_PROCESSED    = 1
    REWARD_DECLINED     = 2
    REWARD_REFUNDED     = 10
