KEY_SIZE = 2048
# You can only encrypt data shorter than your key.
# openssl uses a padding scheme that requires 11 bytes
# I calculate this in bytes rather than bits so I can just strlen to check if a string exceeds this.
MAX_DATA_SIZE = (KEY_SIZE / 8) - 11
CIPHERTEXT_SIZE = 344
