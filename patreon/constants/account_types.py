from enum import Enum

# Terminology:
# Faucets are limitless sources of money. You can't put money back in a faucet, you can only take it out
# Sinks are limitless money holes. You can never take money out of a sink, only dump it in.as
# Accounts with names preceded by 'system_' are not associated with a user, they're usually singletons
# Accounts preceded by user_ are owned by a user (SURPRISING)


class PaymentAccountTypes(Enum):
    USER_CREDIT = 'user_credit'
    USER_PAYMENT_INSTRUMENT = 'user_payment_instrument'
    SYSTEM_ACCOUNT = 'system'


class SystemAccounts(Enum):
    PATREON_EXTERNAL_TRANSACTION_FEE_FAUCET = 'system_patreon_external_transaction_fee_faucet'
    PATRON_FEE_SINK = 'system_patron_fee_sink'
    EXTERNAL_TRANSACTION_FEE_SINK = 'system_external_transaction_fee_sink'
    PATREON_CREDIT_ADJUSTMENT_FAUCET = 'system_patreon_credit_adjustment_faucet'
    PATREON_CREDIT_ADJUSTMENT_SINK = 'system_patreon_credit_adjustment_sink'
