import jinja2

class IECrossDomain(object):
    def render(self):
        env = jinja2.Environment(
            loader=jinja2.FileSystemLoader('iesupport')
        )
        template_name = self.template()
        template = env.get_template(template_name)
        return jinja2.Markup(template.render())

    def template(self):
        return "ie_xdomain.html"

def render_proxy_page():
    page = IECrossDomain()
    return page.render()
