#!/bin/bash
export PYTHONPATH=`pwd`
source venv/bin/activate
forever start -l ./log.log -o ./log.log -e ./log.log -a -p ./ -c python3 scripts/algolia_update.py
forever start -l ./zoho.log -o ./zoho.log -e ./zoho.log -a -p ./ -c python3 scripts/zoho_update.py
