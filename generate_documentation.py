from documentation.src.generate_endpoints_json import generate_and_write_endpoints_json
from documentation.src.generate_resources_json import generate_and_write_resources_json
from documentation.src.generate_html_doc import generate_and_write_html_doc

if __name__ == '__main__':
    generate_and_write_endpoints_json()
    generate_and_write_resources_json()
    generate_and_write_html_doc()