from patreon.model.type import Address


def address():
    return Address("Street", "Avenue", None, None, None, None)


def invalid_address():
    return Address("NotAPlace", None, None, None, None, None)


def patreon_address():
    return Address("100 Patreon St", None, "12345", "San Francisco", "CA", "US")
