from patreon.model.manager import card_mgr, user_mgr
from patreon.model.type.gender_enum import Gender
from patreon.util import datetime
from test.fixtures.campaign_fixtures import create_campaign

inc_counter = 0


def creator(email=None, first_name="Jack", last_name="Conte", vanity=None, is_verified=True,
            image_url="//s3.amazonaws.com/patreon/c923c8f4ada6598a78322c4c3cedb8a2.jpg",
            thumb_url="//s3.amazonaws.com/patreon/5c62fedda46f5048c58e0b3bd1845665.jpg",
            about="About!", password="password", youtube=None, twitter=None, facebook=None, is_suspended=0,
            is_deleted=0, is_nuked=0):
    user_check = user_mgr.get_user_by_email(email)
    if email and user_check:
        return user_check

    global inc_counter
    n = inc_counter
    inc_counter += 1

    email = email or "jack_" + str(n) + "@example.com"
    vanity = vanity or "Jack" + str(n)
    gender = Gender.MALE if (n % 2 == 0) else Gender.FEMALE
    user = user_mgr.create_user(
        first_name, last_name, email, vanity, gender, is_verified, image_url,
        thumb_url, about, password, facebook, twitter, youtube, is_suspended,
        is_deleted, is_nuked
    )

    create_campaign(user.user_id)

    return user


def patron(email=None, first_name="Sam", last_name="Patron", vanity=None, is_verified=True,
           image_url="//s3.amazonaws.com/patreon/c923c8f4ada6598a78322c4c3cedb8a2.jpg",
           thumb_url="//s3.amazonaws.com/patreon/5c62fedda46f5048c58e0b3bd1845665.jpg",
           about="About!", password="password", youtube=None, twitter=None, facebook=None, is_suspended=0,
           is_deleted=0, is_nuked=0):
    user_check = user_mgr.get_user_by_email(email)
    if email and user_check:
        return user_check

    global inc_counter
    n = inc_counter
    inc_counter += 1

    email = email or "jack_" + str(n) + "@example.com"
    vanity = vanity or "Sam" + str(n)
    gender = Gender.MALE if (n % 2 == 0) else Gender.FEMALE

    return user_mgr.create_user(
        first_name, last_name, email, vanity, gender, is_verified, image_url,
        thumb_url, about, password, facebook, twitter, youtube, is_suspended,
        is_deleted, is_nuked
    )


def nullish_patron():
    global inc_counter
    n = inc_counter
    inc_counter += 1

    first_name = "Nancy"
    last_name = None
    email = "nancy_" + str(n) + "@example.com"
    vanity = None
    gender = Gender.MALE if (n % 2 == 0) else Gender.FEMALE
    is_verified = True
    image_url = None
    thumb_url = None
    about = None
    password = "password"

    return user_mgr.create_user(
        first_name, last_name, email, vanity, gender, is_verified,
        image_url, thumb_url, about, password
    )


def patron_with_credit_card():
    global inc_counter
    n = inc_counter
    inc_counter += 1

    patron_ = patron()

    card_id = "B-00000" + str(n) + ""
    user_id = patron_.user_id
    last_4 = "1234"
    type_ = 1
    country = "US"
    expiration = datetime.datetime.now()

    card_mgr.add_card(user_id, card_id, last_4, type_, country, expiration)

    return patron_, card_id
