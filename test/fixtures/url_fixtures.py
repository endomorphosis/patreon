import shutil
import os.path


def user_img_url_string():
    return "//s3.amazonaws.com/patreon/c923c8f4ada6598a78322c4c3cedb8a2.jpg"


def user_thumb_url_string():
    return "//s3.amazonaws.com/patreon/5c62fedda46f5048c58e0b3bd1845665.jpg"


def photo_url_string():
    return "http://www.google.com/images/srpr/logo11w.png"


def uploaded_photo_url_string():
    dir_name = "/var/www/public/imagestemp/"
    filename = "1.jpg"
    if not os.path.isfile(dir_name + filename):
        image_location = "/var/www/public/images/jobs_office.jpg"
        shutil.copyfile(image_location, dir_name + filename)
    return filename


def generic_url_string():
    return "http://www.google.com"


def bad_validation_url_string():
    return "https://patreon.com/notTheRealURL"
