from patreon.util import datetime
from patreon.model.table import Goal
from patreon.util.random import random_int


def create_goal(campaign_id, amount_cents=None, goal_title="", goal_text=None,
                created_at=None, reached_at=None):
    if amount_cents is None:
        amount_cents = 1
    if created_at is None:
        created_at = datetime.datetime.now()

    goal_id = Goal.insert(locals())
    return Goal.get(goal_id=goal_id)
