from patreon.util import datetime
from patreon.model.table import Reward
from random import randint


def create_reward(UID, Amount=None, UserLimit=None, Description="", Shipping=0, Created=None):
    if Created is None:
        Created = datetime.datetime.now()

    if Amount is None:
        Amount = randint(1, 100000)

    reward_dict = locals()
    reward_id = Reward.insert(reward_dict)
    return Reward.get(reward_id=reward_id)