import flask
from nose.tools import *
import patreon
from patreon.app.web import web_app
from patreon.test import MockSession
from test.fixtures.user_fixtures import patron


def create_web_session(user=None, password='password'):
    if not user:
        user = patron(password=password)

    session = MockSession(web_app)
    session.user_id = user.user_id
    session.post('/REST/auth/login', {'email': user.Email, 'password': password}, use_form=True, use_csrf=False)
    resp = session.get('/internal/flubstep_debug')
    assert_equal(resp.status_code, 200)
    assert_in(user.full_name, str(resp.get_data()))
    return session


def create_unauthed_web_session():
    session = MockSession(web_app)
    assert_false(session.is_admin())
    return session


def get_and_follow(session, path):
    resp = session.get(path)
    loop_counter = 0
    while resp.status_code in (301, 302) and loop_counter < 10:
        resp = session.get(resp.headers.get('Location').replace('https://' + patreon.config.main_server, ''))
        loop_counter += 1
    assert_true(loop_counter != 10)
    return resp
