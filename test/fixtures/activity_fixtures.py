import datetime
from patreon.model.table import Activity


def create_post(user_id, campaign_id, activity_type=1, activity_title=None,
                post_type='image', activity_content=None, category=1, photo_key=None,
                photo_extension=None, thumbnail_key=None, file_key=None,
                file_name=None, comment_count=0, min_cents_pledged_to_view=0,
                like_count=0, image_url=None, image_large_url=None,
                image_thumb_url=None, image_height=0, image_width=0, link_url=None,
                link_domain=None, link_subject=None, link_description=None, link_embed=None,
                is_paid=0, is_creation=1, cents_pledged_at_creation=0, created_at=None,
                published_at=None, edited_at=None, deleted_at=None):
    if created_at is None:
        created_at = edited_at = published_at = datetime.datetime.now()

    activity_dict = locals()
    activity_id = Activity.insert(activity_dict)
    return Activity.query.filter_by(activity_id=activity_id).first()
