from patreon.model.manager import follow_mgr
from patreon.util import datetime
from patreon.model.table import Follow


def follow_user(follower_id, followed_id, created_at=None):
    if created_at is None:
        created_at = datetime.datetime.now()

    Follow.insert(locals())
    return Follow.get(follower_id=follower_id, followed_id=followed_id)


def unfollow_user(follower_id, followed_id):
    follow_mgr.delete(follower_id, followed_id)
