from patreon.util import datetime
from patreon.model.table import Actions


def create_like(user_id, activity_id):
    Actions.insert({
        'UID': user_id,
        'HID': activity_id,
        'Created': datetime.datetime.now(),
        'Type': 1
    })