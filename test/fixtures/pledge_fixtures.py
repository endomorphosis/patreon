import patreon
from patreon.util import datetime
from patreon.model.manager import complete_pledge_mgr
from patreon.model.table import RewardsGive


def create_pledge(patron_id, creator_id, amount=None, pledge_cap=None,
                  reward_id=0, card_id=None, address=None,
                  notify_all_activity=None, notify_paid_creation=None,
                  created_at=None, patron_pays_fees=0):
    if amount is None:
        amount = 1

    if created_at is None:
        created_at = datetime.datetime.now()

    complete_pledge_mgr.create_or_update_pledge(
        patron_id=patron_id, creator_id=creator_id, amount=amount, pledge_cap=pledge_cap,
        reward_id=reward_id, card_id=card_id, email_new_comment=1,
        notify_all_activity=notify_all_activity, notify_paid_creation=notify_paid_creation,
        address=address, created_at=created_at, patron_pays_pledge_fees=patron_pays_fees
    )
    return complete_pledge_mgr.get_pledge_by_patron_and_campaign_or_creator(
        patron_id=patron_id, creator_id=creator_id
    ).legacy


def create_pending_pledge(CID, HID, RID, UID, RDescription=None, Created=None,
                          CardID='', Street='', Street2='', City='', State='',
                          Zip='', Country='', Fee=0, PatreonFee=0, PayID=1,
                          Complete=0):
    Status = patreon.globalvars.get('REWARD_PENDING')
    if Created is None:
        Created = datetime.datetime.now()

    RewardsGive.insert(locals())
