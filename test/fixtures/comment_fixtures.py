from patreon.model.manager import comment_mgr
from patreon.model.table import Comment
from patreon.util import datetime


def create_comment(commenter_id, post_id, comment_text='!', thread_id=None):
    return comment_mgr.save_comment(
        commenter_id, post_id, comment_text, thread_id
    )


# Warning: insert_comment can put the test DB in an inconsistent state.
def insert_comment(commenter_id, post_id, comment_text='!', thread_id=None,
                   created_at=None, edited_at=None, deleted_at=None,
                   is_spam=False, vote_sum=0):
    if not created_at:
        created_at = datetime.datetime.now()
        edited_at = created_at

    comment_id = Comment.insert({
        'UID': commenter_id,
        'HID': post_id,
        'Created': created_at,
        'EditedAt': edited_at,
        'DeletedAt': deleted_at,
        'Comment': comment_text,
        'ThreadID': thread_id,
        'is_spam': is_spam,
        'vote_sum': vote_sum
    })

    return comment_mgr.get_comment_or_throw(comment_id)
