from patreon.model.table import Card
from patreon.services.payment_apis import StripeAPI, PayPalAPI


def create_card(user_id, card_id, last_four='1234', card_type='1'):
    Card.insert({
        'UID': user_id,
        'RID': card_id,
        'Number': last_four,
        'Type': card_type
    })


def create_stripe_card(user_id, card_number='4242424242424242'):
    api = StripeAPI()

    token = api.get_test_instrument_id({
        "number": card_number,
        "exp_month": 12,
        "exp_year": 2016,
        "cvc": '123'
    })

    customer_token, _, __, ___, ____ = api.add_instrument(token)

    create_card(user_id, customer_token, '4242', 'Visa')

    return customer_token


def create_paypal_card(user_id):
    api = PayPalAPI()

    billing_token = api.get_test_instrument_id()

    create_card(user_id, billing_token, '4242', 'Visa')

    return billing_token
