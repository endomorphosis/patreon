from patreon.util import datetime
from patreon.model.table import Campaign, CampaignsUsers


def create_campaign(user_id, summary="", creation_name="", pay_per_name="",
                    one_liner="", main_video_embed="", main_video_url="",
                    image_small_url="", image_url="", thanks_video_url="",
                    thanks_embed="", thanks_msg="", is_monthly=0, is_nsfw=0,
                    created_at=None, published_at=None):
    if created_at is None:
        created_at = datetime.datetime.now()
        published_at = created_at

    campaign_dict = locals()
    campaign_dict.pop('user_id')

    campaign_id = Campaign.insert(campaign_dict)

    CampaignsUsers.insert({
        'user_id': user_id,
        'campaign_id': campaign_id,
        'joined_at': created_at
    })
    return Campaign.get(campaign_id=campaign_id)
