from patreon.util import datetime
from patreon.model.table import RewardsGive
from patreon.util.random import random_int


def create_rewards_give(UID, HID, RID, CID, RDescription='', Status=0,
                        Created=None, Pledge=None, CardID='', Street='',
                        Street2='', City='', State='', Zip='', Country = '',
                        Fee=0, PatreonFee=0, PayID=0, Complete=0):
    if Created is None:
        Created = datetime.datetime.now()
    if Pledge is None:
        Pledge = 1

    rewards_give_id = RewardsGive.insert(locals())
    return RewardsGive.get(id_=rewards_give_id)
