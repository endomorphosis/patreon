from patreon.model.table import Attachments


def create_attachment(post_id, url='', name=None):
    attachment_id = Attachments.insert({
        'HID': post_id,
        'Url': url,
        'Name': name
    })
    return Attachments.get(attachment_id=attachment_id)
