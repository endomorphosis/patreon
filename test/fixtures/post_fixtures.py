from patreon.model.manager import draft_mgr, make_post_mgr
from patreon.model.type import PostType

png_url = 'https://staging.patreon.com/images/patreon_navigation_logo_mini_orange.png'


def create_post(campaign_id, user_id, post_type=None, is_paid=True,
                min_cents_pledged_to_view=None):
    # TODO MARCOS other post_types
    data = text_only_data()
    if not post_type:
        post_type = PostType.TEXT_ONLY

    if min_cents_pledged_to_view is None:
        min_cents_pledged_to_view = data['min_cents_pledged_to_view']

    post = draft_mgr.get_new_draft(campaign_id, user_id)
    return make_post_mgr.save_post(
        post_id=post.activity_id,
        user_id=user_id,
        post_type=post_type,
        title=data['title'],
        content_html=data['content'],
        thumbnail=None,
        embed_raw=None,
        min_cents_pledged_to_view=min_cents_pledged_to_view,
        category=data['category'],
        is_paid=is_paid,
        will_publish=True
    )


def create_patron_post(campaign_id, user_id, post_type=None):
    data = text_only_data()
    if not post_type:
        post_type = PostType.TEXT_ONLY

    return make_post_mgr.patron_post(
        user_id=user_id,
        campaign_id=campaign_id,
        post_type=post_type,
        title=data['title'],
        content_html=data['content'],
        embed_raw=None  # TODO MARCOS FIRST
    )

def text_only_patron_post_data():
    return {
        'post_type': 'text_only',
        'title': "Post Title",
        'content': "This is post content.",
        'thumbnail': None,
        'embed': None,
    }


def text_only_data():
    return {
        'post_type': 'text_only',
        'title': "Post Title",
        'content': "This is post content.",
        'min_cents_pledged_to_view': 500,
        'thumbnail': None,
        'tags': None,
        'embed': None,
        'post_file': None,
        'category': 4,
        'is_paid': 'false'
    }


def link_data():
    return {
        "post_type": "link",
        'embed': {
            'html': "<script src=\"https://gist.github.com/meanJim/f5ed8cf2debad162733c.js\"></script>",
            'url': "https://gist.github.com/meanJim/f5ed8cf2debad162733c",
            'provider': "GitHub",
            'provider_url': "http://github.com"
        },
        'thumbnail': {
            'caption': None,
            'height': 640,
            'size': 5983,
            'url': "https://avatars0.githubusercontent.com/u/310223?s=140",
            'width': 640,
            'is_creation': False,
            'top_offset': 0,
            'index': 0
        }
    }


def image_file_data():
    return { 'post_type': 'image_file' }


def audio_file_data():
    return { 'post_type': 'audio_file' }


def video_file_data():
    return { 'post_type': 'video_file' }


def image_embed_data():
    return {
        'post_type': 'image_embed',
        'thumbnail': {
            'top_offset': 0,
        },
        'embed': {
            'provider': "DeviantArt",
            'provider_url': "http://www.deviantart.com",
            'html': (
                "<img class='image-creation' onload='angular.element(this).scop"
                "e().setCreationStateToFinished(event)'src='http://orig08.devia"
                "ntart.net/4e4e/f/2015/116/c/3/c3737a73f33c7aa099252602b22ac4ef"
                "-d8r6dje.jpg'>"
            ),
            'url': "http://sakimichan.deviantart.com/art/White-Deer-529376522",
            'subject': "c3737a73f33c7aa099252602b22ac4ef-d8r6dje.jpg",
            'description': '',
            "image_url": png_url
        }
    }


def video_embed_data():
    return {
        'post_type': 'video_embed',
        'thumbnail': {
            'url': png_url,
            'top_offset': 0
        },
        'embed': {
            'html': '<html>',
            'subject': 'Video Title',
            'description': '',
            'url': 'http://www.youtube.com/watch?v=VpL7gadseDA',
            'provider': 'youtube',
            'provider_url': 'www.youtube.com',
            'image_url': png_url
        }
    }
