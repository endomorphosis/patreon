import patreon
from patreon.routing import sanitize_redirect_url

from nose.tools import *

@patreon.test.manage()
def test_redirect_url():
    MAIN_SERVER = patreon.globalvars.get('MAIN_SERVER')
    base = 'https://' + MAIN_SERVER + '/'

    assert_equal(sanitize_redirect_url(base), base)
    assert_equal(sanitize_redirect_url(base.replace('https', 'http')), base)
    assert_equal(sanitize_redirect_url('//example.com/whatever'), base + 'example.com/whatever')
    assert_equal(sanitize_redirect_url('/'), base)
    assert_equal(sanitize_redirect_url('//////whatever'), base + 'whatever')
    assert_equal(sanitize_redirect_url('noslash'), base + 'noslash')
    assert_equal(sanitize_redirect_url('noslash?with=query'), base + 'noslash?with=query')