from unittest.mock import patch

from nose.tools import *
import patreon
from patreon.app.api import api_app
from patreon.model.manager import user_mgr
from patreon.services import captcha
from patreon.test import MockSession


@patreon.test.manage()
def test_enable_disable():
    service1 = 'test1'
    service2 = 'test2'

    assert_false(captcha.is_enabled(service1))
    captcha.enable_captcha_for_service(service1)
    assert_true(captcha.is_enabled(service1))

    assert_false(captcha.is_enabled(service2, 1))
    captcha.enable_captcha_for_service(service2, 1)
    assert_true(captcha.is_enabled(service2, 1))

    assert_equals([1], captcha.get_ids_for_service(service2))

    captcha.disable_captcha_for_service(service1)
    captcha.disable_captcha_for_service(service2, 1)
    assert_false(captcha.is_enabled(service1))
    assert_false(captcha.is_enabled(service2, 1))


@patreon.test.manage()
@patch('patreon.services.captcha.check_response_against_recaptcha')
def test_signup_flow_captcha(mock):
    mock.return_value = False
    email = "aaaaaa@ccc.com"
    session = MockSession(api_app)
    captcha.enable_captcha_for_service('signup')
    session.post('/user', data={
        "email": email,
        "name": "aaaaaa@ccc.com",
        "password": "aaaaaa@ccc.com",
        "recaptcha_response_field": "something_invalid",
        "redirect": "%2Fsettings",
        "loginRedirect": "null",
        "emailCompare": email
    }
                 )
    assert_equals(None, user_mgr.get_unique(email=email))

    mock.return_value = True
    session.post('/user', data={
        "email": email,
        "name": "aaaaaa@ccc.com",
        "password": "aaaaaa@ccc.com",
        "recaptcha_response_field": "something_valid",
        "redirect": "%2Fsettings",
        "loginRedirect": "null",
        "emailCompare": email
    })
    assert_not_equals(None, user_mgr.get_unique(email=email))

    mock.return_value = False
    email = "different@ccc.com"
    session = MockSession(api_app)
    captcha.disable_captcha_for_service('signup')
    session.post('/user', data={
        "email": email,
        "name": "aaaaaa@ccc.com",
        "password": "aaaaaa@ccc.com",
        "recaptcha_response_field": "something_invalid",
        "redirect": "%2Fsettings",
        "loginRedirect": "null",
        "emailCompare": email
    })
    assert_not_equals(None, user_mgr.get_unique(email=email))
