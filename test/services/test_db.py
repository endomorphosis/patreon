from nose.tools import *
import patreon
from patreon.constants import TxnTypes
from patreon.model.table import UserExtra, Txn
from patreon.services.db import db_session
from patreon.services import async
from sqlalchemy.exc import IntegrityError


@patreon.test.manage()
def test_insert_on_duplicate_key_update_updates():
    user_id = UserExtra.insert({
        'UID': 13,
        'NewMsgs': 10
    })
    # another insert will cause a duplicate key error
    with assert_raises(IntegrityError):
        UserExtra.insert({
            'UID': 13,
            'FlagYoutube': 1
        })

    same_user_id = UserExtra.insert_on_duplicate_key_update({
        'UID': 13,
        'FlagYoutube': 1
    })
    assert_equals(same_user_id, user_id)
    extra_info = UserExtra.get(user_id)
    assert_equals(13, extra_info.UID)
    assert_equals(10, extra_info.NewMsgs)
    assert_equals(1, extra_info.FlagYoutube)


@patreon.test.manage()
def test_insert_on_duplicate_key_update_inserts():
    user_id = UserExtra.insert_on_duplicate_key_update({
        'UID': 13,
        'NewMsgs': 10
    })
    extra_info = UserExtra.get(user_id)
    assert_equals(13, extra_info.UID)
    assert_equals(10, extra_info.NewMsgs)


def _assert_transaction_count(expected_count):
    assert_equals(expected_count, Txn.query.filter(Txn.txn_id != None).count())


@patreon.test.manage()
def test_transactional_logic():
    _assert_transaction_count(0)
    try:
        with db_session.begin_nested():
            Txn.insert({})
            Txn.insert({})
        _assert_transaction_count(2)
        db_session.commit()
        _assert_transaction_count(2)
        with db_session.begin_nested():
            _assert_transaction_count(2)
            with db_session.begin_nested():
                Txn.insert({})
                _assert_transaction_count(3)
            _assert_transaction_count(3)
            try:
                with db_session.begin_nested():
                    Txn.insert({})
                    _assert_transaction_count(4)
                    raise Exception
            except:
                pass
            _assert_transaction_count(3)
            raise Exception
    except:
        pass
    _assert_transaction_count(2)


@async.task_with_threads
def select_lock(id):
    Txn.get_with_lock(id)
    db_session.commit()
    return


@patreon.test.manage()
def test_db_locking():
    txn_id = Txn.insert({
        'type': TxnTypes.credit_purchase
    })
    Txn.get_with_lock(txn_id)
    Txn.get(txn_id)
    thread1 = select_lock(txn_id)
    thread1.join(.2)
    assert_true(thread1.is_alive())
    db_session.commit()
    thread1.join(.2)
    assert_false(thread1.is_alive())
