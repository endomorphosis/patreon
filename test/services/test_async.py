from nose.tools import assert_equals, assert_raises
import patreon
from patreon.services.async import task_with_threads

cat = 6


@task_with_threads
def generic_async_function():
    global cat
    cat = 7
    raise Exception()


@patreon.test.manage()
def test_async():
    thread = generic_async_function()
    thread.join()
    assert_equals(cat, 7)
