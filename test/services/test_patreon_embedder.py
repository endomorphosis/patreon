import patreon
from patreon.services import youtube

from nose.tools import *
from test.fixtures import post_fixtures
from test.test_helpers.post_helpers import publish_post, assert_has_embed, get_blank_draft_id
from test.test_helpers.user_helpers import get_new_logged_in_creator

comic_url = "http://dresdencodak.com/2015/07/08/dark-science-49-exode/"
natural_ketchup_id = "7ITzlMFL4Oo"
youtube_urls = [
    "www.youtube.com/watch?v=",
    "youtube.com/watch?v=",
    "youtu.be/",
    "youtube.com/embed/"
]


def test_youtube_id_parser():
    for prefix in ['', 'http://']:
        for url in youtube_urls:
            constructed_url = prefix + url + natural_ketchup_id
            parsed_id = youtube.get_youtube_id_from_url(constructed_url)
            assert_equals(natural_ketchup_id, parsed_id)


def test_youtube_embedder():
    url = youtube_urls[0] + natural_ketchup_id
    embed_dict = youtube.embed_youtube(url)

    assert_equals(url, embed_dict['url'])
    assert_is_not_none(embed_dict['images'])
    assert_in(natural_ketchup_id, embed_dict['object']['html'])


@patreon.test.manage()
def test_degenerate_embedly_cases():
    creator, session = get_new_logged_in_creator()

    # 1. Hit /post to get the post_id.
    post_id = get_blank_draft_id(session)

    # 2. Publish link post.
    data = post_fixtures.image_embed_data()
    data['embed']['url'] = comic_url
    data['embed']['subject'] = "name"
    data['embed']['image_url'] = None
    post = publish_post(post_id, session, data)

    # 3. Assert that we got an image.
    assert_has_embed(post.get('embed'))
    assert_equals(post.get('post_type'), 'image_embed')
    assert_is_not_none(post.get('image').get('url'))
