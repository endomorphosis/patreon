from nose.tools import *
import patreon
from patreon.util.unsorted import jsdecode


@patreon.services.caching.memcached()
def add_two_numbers(x, y):
    return x + y


@patreon.test.manage()
def test_memcache_handler():
    x = 1
    y = 2
    sum = x + y
    key = add_two_numbers.mc_key_for(x, y)

    assert_equals(None, patreon.services.memcache.get(key))
    assert_equals(sum, add_two_numbers(x, y))
    assert_equals(sum, jsdecode(patreon.services.memcache.get(key)))
