from nose.tools import assert_equals, assert_raises
import patreon
from patreon.exception.encryption_errors import *
from patreon.services import encryption
from patreon.services.encryption import decrypt, encrypt
import rsa
from tempfile import NamedTemporaryFile


@patreon.test.manage()
def test_encryption_decryption_cycles():
    data = '111-11-1111'

    encryption.public_key, encryption.private_key = rsa.newkeys(512)

    assert_equals(data, decrypt(encrypt(data)))


@patreon.test.manage()
def test_load_from_file():
    data = '111-11-1111'

    public_key, private_key = rsa.newkeys(512)

    public_key_file = NamedTemporaryFile()
    public_key_file.write(public_key.save_pkcs1())
    public_key_file.flush()

    private_key_file = NamedTemporaryFile()
    private_key_file.write(private_key.save_pkcs1())
    private_key_file.flush()

    encryption.load_private_key_from_file(private_key_file.name)
    encryption.load_public_key_from_file(public_key_file.name)

    assert_equals(data, decrypt(encrypt(data)))


@patreon.test.manage()
def test_wrong_decryption_key():
    encryption.public_key, _ = rsa.newkeys(512)
    _, encryption.private_key = rsa.newkeys(512)

    with assert_raises(DecryptionFailure):
        decrypt(encrypt('1'))


@patreon.test.manage()
def test_load_bad_public_key():
    public_key_file = NamedTemporaryFile()
    with assert_raises(InvalidPublicKey):
        encryption.load_public_key_from_file('aaa')
    with assert_raises(InvalidPublicKey):
        encryption.load_public_key_from_file(public_key_file.name)


@patreon.test.manage()
def test_load_bad_priv_key():
    private_key_file = NamedTemporaryFile()

    with assert_raises(InvalidPrivateKey):
        encryption.load_private_key_from_file('aaa')
    with assert_raises(InvalidPrivateKey):
        encryption.load_private_key_from_file(private_key_file.name)


@patreon.test.manage()
def test_encrypt_errors():
    encryption.public_key, encryption.private_key = rsa.newkeys(512)
    large_data = '111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111'
    encrypted_data = encrypt('1')

    with assert_raises(InvalidEncryptData):
        encrypt(large_data)

    with assert_raises(InvalidEncryptData):
        encrypt(encrypted_data)

    encryption.public_key, encryption.private_key = None, None
    with assert_raises(EncryptionError):
        encrypt('1')


@patreon.test.manage()
def test_decrypt_errors():
    encryption.public_key, encryption.private_key = rsa.newkeys(512)
    unencrypted_data = '1'

    with assert_raises(InvalidDecryptData):
        decrypt(unencrypted_data)