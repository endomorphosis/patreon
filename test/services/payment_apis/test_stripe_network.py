from unittest.mock import patch

from nose.tools import assert_raises
import patreon
from patreon.exception.payment_errors import TemporaryOutage
from patreon.services.payment_apis import StripeAPI
from requests.exceptions import ConnectTimeout


@patreon.test.manage()
@patch('requests.request', side_effect=ConnectTimeout)
def test_stripe_network_failure(mock):
    api = StripeAPI()
    with assert_raises(TemporaryOutage):
        api.charge_instrument('not-idempotent', 'not-an-id', 50)

    with assert_raises(TemporaryOutage):
        api.refund_transaction('not-idempotent', 'cus_not-an-id')

    with assert_raises(TemporaryOutage):
        api.refund_transaction_partial('not-idempotent', 'cus_not-an-id', 50)

    with assert_raises(TemporaryOutage):
        api.get_transaction_status('not-idempotent')

    with assert_raises(TemporaryOutage):
        api.add_instrument('not-a-token ')


