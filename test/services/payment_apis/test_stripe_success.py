from nose.plugins.attrib import attr
from nose.tools import *
import patreon
from patreon.services.payment_apis import StripeAPI
from test.fixtures.card_fixtures import create_stripe_card
from test.services.payment_apis import generate_idempotency_key


def assert_stripe_charge(amount, transaction_id):
    transaction = StripeAPI().get_transaction_status(transaction_id).raw_result

    assert_equals(transaction['amount'], amount)
    assert_true(transaction['paid'])
    assert_equals(transaction['status'], 'paid')


def assert_stripe_refund(amount, transaction_id):
    transaction = StripeAPI().get_transaction_status(transaction_id).raw_result
    assert_equals(transaction['amount_refunded'], amount)


@patreon.test.manage()
@attr(speed="toxic")
def test_stripe_okay():
    user_id = 1
    card_token = create_stripe_card(user_id)
    idempotence_key_charge = generate_idempotency_key()  # This COULD collide...
    idempotence_key_refund_partial = generate_idempotency_key()
    idempotence_key_refund_full = generate_idempotency_key()

    charge_amount = 500

    api = StripeAPI()

    # Test charge
    charge_transaction, _ = api.charge_instrument(idempotence_key_charge, card_token, charge_amount)
    assert_stripe_charge(charge_amount, charge_transaction)

    assert_equals(api.charge_instrument(idempotence_key_charge, card_token, charge_amount * 10).transaction_id, charge_transaction)
    assert_stripe_charge(charge_amount, charge_transaction)

    transaction_id, _ = api.refund_transaction_partial(idempotence_key_refund_partial, charge_transaction,
                                                       int(charge_amount / 10))
    assert_stripe_refund((charge_amount / 10), transaction_id)

    transaction_id, _ = api.refund_transaction_partial(idempotence_key_refund_partial, charge_transaction,
                                                       int(charge_amount))
    assert_stripe_refund((charge_amount / 10), transaction_id)

    transaction_id, _ = api.refund_transaction(idempotence_key_refund_full, charge_transaction)
    assert_stripe_refund(charge_amount, transaction_id)

    transaction_id, _ = api.refund_transaction(idempotence_key_refund_full, charge_transaction)
    assert_stripe_refund(charge_amount, transaction_id)
