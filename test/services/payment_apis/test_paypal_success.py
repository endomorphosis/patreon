import string
from unittest.mock import patch

from nose.tools import *
import patreon
from patreon.constants.paypal_api_constants import *
from patreon.exception.payment_errors import TemporaryOutage
from patreon.services.payment_apis import PayPalAPI
import random
from test.fixtures.card_fixtures import create_paypal_card
from test.services.payment_apis import generate_idempotency_key


def assert_paypal_charge(amount, instrument_id, mock, idempotence_key):
    mock.assert_called_with(CHARGE_ACTION, {
        AMOUNT_KEY: amount / 100,
        PAYMENT_ACTION_KEY: 'Sale',
        CURRENCY_KEY: CURRENCY,
        INSTRUMENT_ID_KEY: instrument_id,
        IDEMPOTENCE_KEY: idempotence_key
    })


def assert_paypal_refund_partial(amount, transaction_id, mock, idempotence_key):
    mock.assert_called_with(REFUND_ACTION, {
        TRANSACTION_ID_KEY: transaction_id,
        CURRENCY_KEY: CURRENCY,
        REFUND_TYPE_KEY: 'Partial',
        AMOUNT_KEY: amount / 100,
        IDEMPOTENCE_KEY: idempotence_key
    })

def assert_paypal_refund_full(amount, transaction_id, mock, idempotence_key):
    mock.assert_called_with(REFUND_ACTION, {
        TRANSACTION_ID_KEY: transaction_id,
        CURRENCY_KEY: CURRENCY,
        IDEMPOTENCE_KEY: idempotence_key
    })

@patreon.test.manage()
@patch.object(PayPalAPI, '_PayPalAPI__make_paypal_request',
              return_value={
                  TRANSACTION_ID_KEY: 'trans_id',
                  REFUND_TRANSACTION_KEY: 'trans_id',
                  FEE_AMOUNT_KEY: 1}
              )
def test_paypal_okay(mock):
    user_id = 1
    card_token = create_paypal_card(user_id)
    idempotence_key_charge = generate_idempotency_key()  # This COULD collide...
    idempotence_key_refund_partial = generate_idempotency_key()
    idempotence_key_refund_full = generate_idempotency_key()

    charge_amount = 500

    api = PayPalAPI()

    # Test charge
    charge_transaction, _ = api.charge_instrument(idempotence_key_charge, card_token, charge_amount)
    assert_paypal_charge(charge_amount, card_token, mock, idempotence_key_charge)

    assert_equals(api.charge_instrument(idempotence_key_charge, card_token, charge_amount * 10).transaction_id,
                  charge_transaction)
    assert_paypal_charge(charge_amount * 10, card_token, mock, idempotence_key_charge)

    transaction_id, _ = api.refund_transaction_partial(idempotence_key_refund_partial, charge_transaction,
                                                       int(charge_amount / 10))
    assert_paypal_refund_partial(int(charge_amount / 10), transaction_id, mock, idempotence_key_refund_partial)

    transaction_id, _ = api.refund_transaction_partial(idempotence_key_refund_partial, charge_transaction,
                                                       int(charge_amount))
    assert_paypal_refund_partial(int(charge_amount), transaction_id, mock, idempotence_key_refund_partial)

    idempotence_key_charge = ''.join(
        random.choice(string.ascii_uppercase) for i in range(12))  # This COULD collide...

    charge_transaction, _ = api.charge_instrument(idempotence_key_charge, card_token, charge_amount)
    assert_paypal_charge(charge_amount, card_token, mock, idempotence_key_charge)

    transaction_id, _ = api.refund_transaction(idempotence_key_refund_full, charge_transaction)
    assert_paypal_refund_full(charge_amount, transaction_id, mock, idempotence_key_refund_full)

    transaction_id, _ = api.refund_transaction(idempotence_key_refund_full, charge_transaction)
    assert_paypal_refund_full(charge_amount, transaction_id, mock, idempotence_key_refund_full)
