from nose.plugins.attrib import attr
from nose.tools import *
import patreon
from patreon.exception.payment_errors import ConfigError, AuthenticationError, InvalidAPIRequest, \
    ReusedIdempotencyKey
from patreon.services import db
from patreon.services.payment_apis import StripeAPI
from test.fixtures.card_fixtures import create_stripe_card
from test.services.payment_apis import generate_idempotency_key


def refundable_charge(api, card_id):
    return api.charge_instrument(generate_idempotency_key(), card_id, 200).transaction_id


@patreon.test.manage()
def test_stripe_bad_configs():
    original_config = patreon.config.stripe.copy()

    patreon.config.stripe = None

    with assert_raises(ConfigError):
        StripeAPI()

    patreon.config.stripe = {
        'cat': 1
    }

    with assert_raises(ConfigError):
        StripeAPI()

    patreon.config.stripe = original_config.copy()

    patreon.config.stripe['secret_key'] = 'the secret key is not cats'
    with assert_raises(AuthenticationError) as e:
        StripeAPI().get_transaction_status('ch_3FpvJlVQYk7IlS')

    patreon.config.stripe = original_config.copy()


@patreon.test.manage()
@attr(speed="slower")
def test_stripe_charge_failures():
    card_token = create_stripe_card(1)

    duplicate_idempotency_key = generate_idempotency_key()

    bad_card_id = 'not a card id'
    with assert_raises(InvalidAPIRequest) as assert_raises_context:
        StripeAPI().charge_instrument(generate_idempotency_key(), bad_card_id, 50)

    exception = assert_raises_context.exception
    assert_true('No such customer: ' + bad_card_id in str(exception.original_exception))

    with assert_raises(InvalidAPIRequest) as assert_raises_context:
        StripeAPI().charge_instrument(generate_idempotency_key(), card_token, 1)

    exception = assert_raises_context.exception

    assert_true('Amount must be at least 50 cents' in str(exception.original_exception))

    transaction_id, _ = StripeAPI().charge_instrument(duplicate_idempotency_key, card_token, 50)

    with assert_raises(ReusedIdempotencyKey) as assert_raises_context:
        StripeAPI().refund_transaction(duplicate_idempotency_key, transaction_id)

    exception = assert_raises_context.exception

    assert_true('Keys for idempotent requests can only be used for the same endpoint they were first used for'
                in str(exception.original_exception))

    assert(db.test_payment_events_dirty)


@patreon.test.manage()
@attr(speed="toxic")
def test_stripe_refund():
    card_token = create_stripe_card(1)
    api = StripeAPI()

    bad_transaction_id = 'cat'
    with assert_raises(InvalidAPIRequest) as assert_raises_context:
        api.refund_transaction(generate_idempotency_key(), bad_transaction_id)

    assert_true('No such payment: ' + bad_transaction_id in str(assert_raises_context.exception.original_exception))

    with assert_raises(InvalidAPIRequest) as assert_raises_context:
        api.refund_transaction_partial(generate_idempotency_key(), bad_transaction_id, 50)

    assert_true('No such payment: ' + bad_transaction_id in str(assert_raises_context.exception.original_exception))

    double_transaction_id = refundable_charge(api, card_token)

    with assert_raises(InvalidAPIRequest) as assert_raises_context:
        api.refund_transaction(generate_idempotency_key(), double_transaction_id)
        api.refund_transaction(generate_idempotency_key(), double_transaction_id)

    assert_true('Charge {} has already been refunded.'.format(double_transaction_id)
                in str(assert_raises_context.exception.original_exception))

    with assert_raises(InvalidAPIRequest) as assert_raises_context:
        api.refund_transaction_partial(generate_idempotency_key(), double_transaction_id, 50)

    assert_true('Charge {} has already been refunded.'.format(double_transaction_id)
                in str(assert_raises_context.exception.original_exception))

    double_transaction_id = refundable_charge(api, card_token)

    with assert_raises(InvalidAPIRequest) as assert_raises_context:
        api.refund_transaction_partial(generate_idempotency_key(), double_transaction_id, 150)
        api.refund_transaction(generate_idempotency_key(), double_transaction_id, 50)
        api.refund_transaction(generate_idempotency_key(), double_transaction_id, 50)

    assert_true('Charge {} has already been refunded.'.format(double_transaction_id)
                in str(assert_raises_context.exception.original_exception))


@patreon.test.manage()
def test_stripe_card():
    api = StripeAPI()

    bad_token = 'cat'
    with assert_raises(InvalidAPIRequest) as assert_raises_context:
        api.add_instrument(bad_token)

    exception = assert_raises_context.exception
    assert_true('No such token: ' + bad_token in str(exception.original_exception))


@patreon.test.manage()
def test_stripe_transaction_status():
    api = StripeAPI()

    bad_transaction_id = 'cat'
    with assert_raises(InvalidAPIRequest) as assert_raises_context:
        api.get_transaction_status(bad_transaction_id)

    assert_true('No such payment: ' + bad_transaction_id in str(assert_raises_context.exception.original_exception))
