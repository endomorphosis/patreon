from nose.plugins.attrib import attr
from nose.tools import *
import patreon
from patreon.exception.payment_errors import CardDeclineError
from patreon.services.payment_apis import StripeAPI
from test.fixtures.card_fixtures import create_stripe_card
from test.services.payment_apis import generate_idempotency_key


def check_decline(card_id):
    api = StripeAPI()
    with assert_raises(CardDeclineError) as exception:
        api.charge_instrument(generate_idempotency_key(), card_id, 50)


def check_creation_decline(card_number):
    with assert_raises(CardDeclineError):
        create_stripe_card(1, card_number)


@patreon.test.manage()
@attr(speed="toxic")
def test_declines_stripe():
    check_creation_decline('4000000000000002')  # Generic
    check_creation_decline('4000000000000127')  # Stolen/Fraudulent
    check_creation_decline('4000000000000069')  # Expired
    check_creation_decline('4000000000000119')  # Processing Error
    check_creation_decline('')  # Invalid card number

    decline_cards = [create_stripe_card(1, '4100000000000019'),  # Successful add, failure on charge
                     create_stripe_card(1, '4000000000000341')]  # Successful add, failure on charge

    for card_id in decline_cards:
        check_decline(card_id)
