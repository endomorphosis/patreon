from unittest.mock import patch

from nose.tools import assert_raises
import patreon
from patreon.exception.payment_errors import TemporaryOutage
from patreon.services.payment_apis import PayPalAPI
from requests.exceptions import ConnectTimeout


@patreon.test.manage()
@patch.object(PayPalAPI, '_PayPalAPI__make_paypal_request', side_effect=ConnectTimeout)
def test_paypal_network_failure(mock):
    api = PayPalAPI()
    with assert_raises(TemporaryOutage):
        api.charge_instrument('not-idempotent', 'B-not-an-id', 50)

    with assert_raises(TemporaryOutage):
        api.refund_transaction('not-idempotent', 'B-not-an-id')

    with assert_raises(TemporaryOutage):
        api.refund_transaction_partial('not-idempotent', 'B-not-an-id', 50)

    with assert_raises(TemporaryOutage):
        api.get_transaction_status('not-idempotent')

    with assert_raises(TemporaryOutage):
        api.add_instrument('not-a-token ')
