import string
import random


def generate_idempotency_key():
    return ''.join(random.choice(string.ascii_uppercase) for i in range(12))
