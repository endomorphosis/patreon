from unittest.mock import patch

from nose.tools import *
import patreon
from patreon.constants.paypal_api_constants import REFUND_ACTION, TRANSACTION_ID_KEY, CURRENCY_KEY, IDEMPOTENCE_KEY, \
    CURRENCY
from patreon.exception.payment_errors import ConfigError, AuthenticationError, InvalidAPIRequest, TemporaryOutage
from patreon.services.payment_apis import PayPalAPI
from test.fixtures.card_fixtures import create_paypal_card
from test.services.payment_apis import generate_idempotency_key


@patreon.test.manage()
def test_paypal_bad_configs():
    try:
        original_config = patreon.config.paypal.copy()

        patreon.config.paypal = None

        with assert_raises(ConfigError):
            PayPalAPI()

        patreon.config.paypal = {
            'cat': 1
        }

        with assert_raises(ConfigError):
            PayPalAPI()

        patreon.config.paypal = original_config.copy()
        patreon.config.paypal['api_url'] = 'https://api-3t.paypal.com/nvp'  # This is a live url, we're using test creds

        with assert_raises(AuthenticationError) as e:
            PayPalAPI().get_transaction_status('Not A Transaction ID')

        patreon.config.paypal = original_config.copy()

        patreon.config.paypal['password'] = 'the password is not cats'
        with assert_raises(AuthenticationError) as e:
            PayPalAPI().get_transaction_status('Not A Transaction ID')

        patreon.config.paypal = original_config.copy()
    except TemporaryOutage:
        pass


@patreon.test.manage()
def test_paypal_charge_failures():
    try:
        card_token = create_paypal_card(1)

        bad_card_id = 'not a card id'
        with assert_raises(InvalidAPIRequest) as assert_raises_context:
            PayPalAPI().charge_instrument(generate_idempotency_key(), bad_card_id, 50)

        exception = assert_raises_context.exception
        assert_equals('Billing Agreement Id or transaction Id is not valid', exception.original_exception.message)

        with assert_raises(InvalidAPIRequest) as assert_raises_context:
            PayPalAPI().charge_instrument(generate_idempotency_key(), 'B-notreal', 50)

        exception = assert_raises_context.exception
        assert_equals('Billing Agreement Id or transaction Id is not valid', exception.original_exception.message)

        with assert_raises(InvalidAPIRequest) as assert_raises_context:
            PayPalAPI().charge_instrument(generate_idempotency_key(), card_token, -1)

        exception = assert_raises_context.exception

        assert_equals('This transaction cannot be processed. The amount to be charged is zero.',
                      exception.original_exception.message)
    except TemporaryOutage:
        pass


def refundable_charge(api, card_id):
    return api.charge_instrument(generate_idempotency_key(), card_id, 200).transaction_id


@patreon.test.manage()
@patch.object(PayPalAPI, '_PayPalAPI__make_paypal_request')
def test_paypal_refund(mock):
    api = PayPalAPI()
    key = generate_idempotency_key()
    api.refund_transaction(key, 'trans_id')
    mock.assert_called_once_with(
        REFUND_ACTION,
        {
            TRANSACTION_ID_KEY: 'trans_id',
            CURRENCY_KEY: CURRENCY,
            IDEMPOTENCE_KEY: key
        }
    )


@patreon.test.manage()
def test_paypal_card():
    try:
        api = PayPalAPI()
        with assert_raises(InvalidAPIRequest) as assert_raises_context:
            api.add_instrument_start(100, 'Cat', 'Dog', '', 'http://google.com')

        assert_equals(assert_raises_context.exception.original_exception.message, 'ReturnURL is missing.')

        with assert_raises(InvalidAPIRequest) as assert_raises_context:
            api.add_instrument_start(100, 'Cat', 'Dog', 'http://google.com', '')

        assert_equals(assert_raises_context.exception.original_exception.message, 'CancelURL is missing.')
    except TemporaryOutage:
        pass


@patreon.test.manage()
def test_paypal_transaction_status():
    try:
        api = PayPalAPI()

        bad_transaction_id = 'cat'
        with assert_raises(InvalidAPIRequest) as assert_raises_context:
            api.get_transaction_status(bad_transaction_id)

        assert_equals('The transaction id is not valid', assert_raises_context.exception.original_exception.message)
    except TemporaryOutage:
        pass
