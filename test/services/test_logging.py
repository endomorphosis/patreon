import patreon
import json
from nose.tools import *
from patreon.services.logging.unsorted import sanitize_data

def test_sanitize_data():
	bad_data = {
		'data': {
			'oldPassword': 'SENSITIVEDATA',
			'newPassword': 'SENSITIVEDATA',
			'username': 'dont remove this'
		},
		'data2': [
			{ 'otherPassword': 'SENSITIVEDATA' },
			{ 'someOtherKey': 1000 }
		],
		'data3': {
			'TaxID': 'SENSITIVEDATA'
		}
	}
	assert_in('SENSITIVEDATA', json.dumps(bad_data))
	good_data = sanitize_data(bad_data)
	assert_not_in('SENSITIVEDATA', json.dumps(good_data))
	assert_equal(good_data['data']['username'], 'dont remove this')
	assert_equal(good_data['data2'][1]['someOtherKey'], 1000)
