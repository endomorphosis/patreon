from nose.tools import *

import patreon
from patreon.services.mail.templates import NotifyNewComment
from test.fixtures import user_fixtures, comment_fixtures
from test.mail import mailer
from test.test_helpers.mail_helpers import assert_no_mandrill_template_remains


@patreon.test.manage()
def test_should_render_html():
    test_user = user_fixtures.patron()
    test_creator = user_fixtures.creator()
    test_commenter = user_fixtures.patron()

    comment = comment_fixtures.insert_comment(
        test_commenter.user_id, 0, "A Sweet Caticorn Picture"
    )

    rendered_email = mailer().send_template_email(
        NotifyNewComment(test_user, test_creator, test_commenter, comment, is_reply=False)
    )['html']

    assert_in(comment.comment_text, rendered_email)
    assert_in(comment.canonical_url(), rendered_email)
    assert_in(test_user.email, rendered_email)
    assert_in(test_creator.full_name, rendered_email)
    assert_in(test_commenter.full_name, rendered_email)
    assert_in(test_commenter.thumb_url, rendered_email)
    assert_in(test_commenter.canonical_url(), rendered_email)

    assert_no_mandrill_template_remains(rendered_email)
