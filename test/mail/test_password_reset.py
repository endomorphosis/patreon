from nose.tools import assert_true
import patreon

from patreon.model.manager import forgot_password_mgr
from patreon.services.mail.templates import PasswordReset
from test.fixtures import user_fixtures
from test.mail import mailer
from test.test_helpers.mail_helpers import assert_no_mandrill_template_remains


@patreon.test.manage()
def test_should_render_html():
    test_user = user_fixtures.patron()

    token = forgot_password_mgr.generate_token()

    rendered_email = mailer().send_template_email(
        PasswordReset(test_user, token)
    )
    rendered_email = rendered_email['html']

    assert_true(token in rendered_email)

    assert_no_mandrill_template_remains(rendered_email)
