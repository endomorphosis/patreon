from nose.tools import assert_true

import patreon
from patreon.model.manager import campaign_mgr
from patreon.services.mail.templates import NotifyNewPost
from test.fixtures import user_fixtures, post_fixtures
from test.mail import mailer
from test.test_helpers.mail_helpers import assert_no_mandrill_template_remains


@patreon.test.manage()
def test_should_render_html():
    test_creator = user_fixtures.creator()
    test_user = user_fixtures.patron()

    creator_id = test_creator.user_id
    campaign_id = campaign_mgr.try_get_campaign_id_by_user_id_hack(creator_id)
    test_post = post_fixtures.create_post(campaign_id, creator_id)

    rendered_email = mailer().send_template_email(
        NotifyNewPost(test_creator, test_post, [test_user])
    )['html']

    assert_true(test_user.email in rendered_email)
    assert_true(test_creator.full_name in rendered_email)
    assert_true(test_post.activity_title in rendered_email)
    assert_true(test_post.activity_content in rendered_email)

    assert_no_mandrill_template_remains(rendered_email)
