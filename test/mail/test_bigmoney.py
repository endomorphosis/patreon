from nose.tools import assert_true

import patreon
from patreon.services.mail.templates import BigMoney
from test.fixtures import user_fixtures
from test.mail import mailer
from test.test_helpers.mail_helpers import assert_no_mandrill_template_remains


@patreon.test.manage()
def test_should_render_html():
    test_user = user_fixtures.patron()

    rendered_email = mailer().send_template_email(
        BigMoney(test_user)
    )['html']

    assert_true(test_user.email in rendered_email)

    assert_no_mandrill_template_remains(rendered_email)
