from nose.tools import assert_true

import patreon
from patreon.services.mail.templates.EmailChanged import EmailChanged
from test.fixtures import user_fixtures
from test.mail import mailer
from test.test_helpers.mail_helpers import assert_no_mandrill_template_remains


@patreon.test.manage()
def test_should_render_html():
    test_user = user_fixtures.patron()
    old_email = "oldemail@gmail.com"
    new_email = "newEmail@gmai.com"

    rendered_email = mailer().send_template_email(
        EmailChanged(test_user, old_email, new_email)
    )['html']

    assert_true(old_email in rendered_email)
    assert_true(new_email in rendered_email)

    assert_no_mandrill_template_remains(rendered_email)
