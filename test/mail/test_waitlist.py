from nose.tools import assert_true

import patreon
from patreon.services.mail.templates import Waitlist
from test.fixtures import user_fixtures
from test.mail import mailer
from test.test_helpers.mail_helpers import assert_no_mandrill_template_remains


@patreon.test.manage()
def test_should_render_html():
    test_user = user_fixtures.patron()
    test_creator = user_fixtures.creator()

    reward_level = "Really High Reward!"

    rendered_email = mailer().send_template_email(
        Waitlist(test_user, test_creator, reward_level)
    )['html']

    assert_true(test_creator.image_url in rendered_email)
    assert_true(reward_level in rendered_email)
    assert_true(test_user.email in rendered_email)
    assert_true(test_creator.full_name in rendered_email)
    assert_true(test_creator.canonical_url() in rendered_email)

    assert_no_mandrill_template_remains(rendered_email)
