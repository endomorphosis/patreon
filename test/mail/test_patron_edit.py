from nose.tools import assert_in

import patreon
from patreon.services.mail.templates import Patron
from patreon.util import format
from test.fixtures import user_fixtures
from test.mail import mailer
from test.test_helpers.mail_helpers import assert_no_mandrill_template_remains


@patreon.test.manage()
def test_should_render_html():
    test_user = user_fixtures.patron()
    test_creator = user_fixtures.creator()

    old_pledge_cents = 10
    pledge_cents = 1000
    rendered_email = mailer().send_template_email(
        Patron(test_creator, test_user, pledge_cents, old_pledge_cents)
    )['html']

    pledge_string = format.cents_as_dollar_string(pledge_cents)
    old_pledge_string = format.cents_as_dollar_string(pledge_cents)

    assert_in(pledge_string, rendered_email)
    assert_in(old_pledge_string, rendered_email)
    assert_in(test_creator.email, rendered_email)
    assert_in(test_user.first_name, rendered_email)
    assert_in(test_user.thumb_url, rendered_email)
    assert_in(test_user.canonical_url(), rendered_email)

    assert_no_mandrill_template_remains(rendered_email)
