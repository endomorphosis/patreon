from nose.tools import assert_true

import patreon
from patreon.services.mail.templates import Patron
from patreon.util import format
from test.fixtures import user_fixtures
from test.mail import mailer
from test.test_helpers.mail_helpers import assert_no_mandrill_template_remains


@patreon.test.manage()
def test_should_render_html():
    test_user = user_fixtures.patron()
    test_creator = user_fixtures.creator()

    pledge_cents = 1000

    rendered_email = mailer().send_template_email(
        Patron(test_creator, test_user, pledge_cents)
    )['html']

    dollar_string = format.cents_as_dollar_string(pledge_cents)

    assert_true(dollar_string in rendered_email)
    assert_true(test_creator.email in rendered_email)
    assert_true(test_user.first_name in rendered_email)
    assert_true(test_user.thumb_url in rendered_email)
    assert_true(test_user.canonical_url() in rendered_email)

    assert_no_mandrill_template_remains(rendered_email)
