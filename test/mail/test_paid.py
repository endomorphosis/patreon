import datetime
from nose.tools import assert_true

import patreon
from patreon.services.mail.templates import Paid
from test.fixtures import user_fixtures
from test.mail import mailer
from test.test_helpers.mail_helpers import assert_no_mandrill_template_remains


@patreon.test.manage()
def test_should_render_html():
    test_user = user_fixtures.patron()

    amount = "Several Billion Dollars"
    date = str(datetime.datetime.now())
    rendered_email = mailer().send_template_email(
        Paid(test_user, amount, date)
    )['html']

    assert_true(amount in rendered_email)
    assert_true(date in rendered_email)
    assert_true(test_user.email in rendered_email)

    assert_no_mandrill_template_remains(rendered_email)
