from nose.tools import assert_true

import patreon
from patreon.services.mail.templates import Register
from test.fixtures import user_fixtures
from test.mail import mailer
from test.test_helpers.mail_helpers import assert_no_mandrill_template_remains


@patreon.test.manage()
def test_should_render_html():
    test_user = user_fixtures.patron()

    activation_url = "http://patreon.com/notARealLink.php"

    rendered_email = mailer().send_template_email(
        Register(test_user.email, test_user.first_name, test_user.last_name, activation_url)
    )
    rendered_email = rendered_email['html']

    assert_true(activation_url in rendered_email)
    assert_true(test_user.email in rendered_email)
    assert_true(test_user.first_name in rendered_email)

    assert_no_mandrill_template_remains(rendered_email)
