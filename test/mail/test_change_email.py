from nose.tools import assert_true

import patreon
from patreon.services.mail.templates import ChangeEmail
from test.fixtures import user_fixtures
from test.mail import mailer
from test.test_helpers.mail_helpers import assert_no_mandrill_template_remains


@patreon.test.manage()
def test_should_render_html():
    test_user = user_fixtures.patron()

    new_email = "newEmail@gmail.com"
    validation_url = "https://patreon.com/notTheRealURL"

    rendered_email = mailer().send_template_email(
        ChangeEmail(test_user, new_email, validation_url)
    )['html']

    assert_true(validation_url in rendered_email)
    assert_true(new_email in rendered_email)

    assert_no_mandrill_template_remains(rendered_email)
