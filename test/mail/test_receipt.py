from nose.tools import *

import patreon
from patreon.model.manager import campaign_mgr
from patreon.services.mail.templates import Receipt
from patreon.util.format import format_cents_value_as_money
from test.fixtures import pledge_fixtures, user_fixtures
from test.mail import mailer
from test.test_helpers.mail_helpers import assert_no_mandrill_template_remains


@patreon.test.manage()
def test_should_render_html():
    patron = user_fixtures.patron()
    creator = user_fixtures.creator()
    campaign = campaign_mgr.try_get_campaign_by_user_id_hack(creator.user_id)

    amount = 1000
    pledge_amount = format_cents_value_as_money(amount)
    pledge = pledge_fixtures.create_pledge(patron.user_id, creator.user_id, amount)

    rendered_email = mailer().send_template_email(
        Receipt(patron, creator, campaign, pledge)
    )['html']

    assert_in(pledge_amount, rendered_email)
    assert_in(patron.email, rendered_email)
    assert_in(creator.full_name, rendered_email)

    assert_no_mandrill_template_remains(rendered_email)
