from nose.tools import assert_true

import patreon
from patreon.services.mail.templates import Refunded
from test.fixtures import user_fixtures
from test.mail import mailer
from test.test_helpers.mail_helpers import assert_no_mandrill_template_remains


@patreon.test.manage()
def test_should_render_html():
    test_user = user_fixtures.patron()
    test_creator = user_fixtures.creator()

    amount = "Several Billion Dollars"

    rendered_email = mailer().send_template_email(
        Refunded(test_user, test_creator, amount)
    )['html']

    assert_true(amount in rendered_email)
    assert_true(test_user.email in rendered_email)
    assert_true(test_creator.full_name in rendered_email)
    assert_true(test_creator.image_url in rendered_email)
    assert_true(test_creator.canonical_url() in rendered_email)

    assert_no_mandrill_template_remains(rendered_email)
