from nose.tools import assert_true

import patreon
from patreon.services.mail.templates import ConfirmEmail
from test.fixtures import user_fixtures
from test.mail import mailer
from test.test_helpers.mail_helpers import assert_no_mandrill_template_remains


@patreon.test.manage()
def test_should_render_html():
    test_user = user_fixtures.patron()

    confirm_url = "http://patreon.com/notARealLink.php"
    rendered_email = mailer().send_template_email(
        ConfirmEmail(test_user, confirm_url)
    )['html']

    assert_true(confirm_url in rendered_email)
    assert_true(test_user.email in rendered_email)

    assert_no_mandrill_template_remains(rendered_email)
