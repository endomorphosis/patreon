import time
from flask import request, session

import patreon
from nose.tools import *
from patreon.model.manager import feature_flag
from patreon.model.manager.csrf import calculate_csrf_signature
from test.fixtures.session_fixtures import create_unauthed_web_session, create_web_session
from test.fixtures.user_fixtures import patron


@patreon.test.manage()
def test_check_https():
    session = create_unauthed_web_session()
    resp = session.manual(url='/login', data=None, method='GET', headers={'X-Forwarded-Proto': 'http'})
    assert_equals(resp.status_code, 302)
    resp = session.manual(url='/login?blah%EF%BF%BD', data=None, method='GET', headers={'X-Forwarded-Proto': 'http'})
    assert_equals(resp.status_code, 302)


@patreon.test.manage()
def test_check_csrf():
    patron_user = patron()
    session = create_web_session(patron_user)

    # No CSRF
    resp = session.manual(url='/', data={}, method='POST')
    assert_equals(resp.status_code, 302)

    # Type and value error on time
    resp = session.manual(url='/', data={'CSRFToken[URI]': '1',
                                         'CSRFToken[token]': 'blah',
                                         'CSRFToken[time]': 'a'}, method='POST',
                          headers={'Content-Type': 'application/x-www-form-urlencoded'})
    assert_equals(resp.status_code, 302)
    resp = session.manual(url='/', data={'CSRFToken[URI]': '1',
                                         'CSRFToken[token]': 'blah',
                                         'CSRFToken[time]': '{}'}, method='POST',
                          headers={'Content-Type': 'application/x-www-form-urlencoded'})
    assert_equals(resp.status_code, 302)

    route = '/test'
    user_id = patron_user.UID
    token = 'blah'

    request_time = (time.time() - 60 * 60 * 24 * 2) * 1000
    csrf_token = calculate_csrf_signature(route, user_id, token, request_time)

    # Expired
    resp = session.manual(url='/', data={'CSRFToken[URI]': route,
                                         'CSRFToken[token]': csrf_token,
                                         'CSRFToken[time]': request_time}, method='POST',
                          headers={'Content-Type': 'application/x-www-form-urlencoded'})
    assert_equals(resp.status_code, 302)

    # Expired
    resp = session.manual(url='/', data={'CSRFToken[URI]': route,
                                         'CSRFToken[token]': csrf_token,
                                         'CSRFToken[time]': request_time}, method='POST',
                          headers={'Content-Type': 'application/x-www-form-urlencoded', 'Referer': route})

    request_time = time.time() * 1000
    csrf_token = calculate_csrf_signature(route, user_id, token, request_time)

    # Invalid referer
    resp = session.manual(url='/', data={'CSRFToken[URI]': route,
                                         'CSRFToken[token]': csrf_token,
                                         'CSRFToken[time]': request_time}, method='POST',
                          headers={'Content-Type': 'application/x-www-form-urlencoded', 'Referer': 'Bad'})
    assert_equals(resp.status_code, 302)

    request_time = time.time() * 1000
    csrf_token = 'a'
    # Invalid referer
    resp = session.manual(url='/',
                          data={'CSRFToken[URI]': route,
                                'CSRFToken[token]': csrf_token,
                                'CSRFToken[time]': request_time},
                          method='POST',
                          headers={'Content-Type': 'application/x-www-form-urlencoded', 'Referer': route})
    assert_equals(resp.status_code, 302)


@patreon.test.manage()
def test_ab_test_assignments():
    patron_user = patron()
    patron_session = create_web_session(patron_user)
    unauthed_session = create_unauthed_web_session()

    unauthed_session.get('/')
    patron_session.get('/')

    with unauthed_session:
        assert_is_not_none(request.ab_test_group_id)
        group_id = request.ab_test_group_id

    with unauthed_session:
        assert_is_not_none(request.ab_test_group_id)
        assert_equals(request.ab_test_group_id, group_id)

    with patron_session:
        assert_true(session.is_logged_in)
        assert_equals(session.user_id, patron_user.user_id)
        assert_is_not_none(request.ab_test_group_id)
        group_id = request.ab_test_group_id

    with patron_session:
        assert_true(session.is_logged_in)
        assert_equals(session.user_id, patron_user.user_id)
        assert_is_not_none(request.ab_test_group_id)
        assert_equals(request.ab_test_group_id, group_id)

    assert_equals(group_id, feature_flag.get_group_id_for_user(patron_user.user_id))
