"""
Extremely basic test to ensure that web_app loads.
"""

import patreon
from patreon.test import MockSession
from patreon.app.web import web_app
from patreon.services import db

from nose.tools import *

@patreon.test.manage()
def test_basic_loading():
    sess = MockSession(web_app)
    resp = sess.get('/about')
    assert_equal(resp.status_code, 200)
