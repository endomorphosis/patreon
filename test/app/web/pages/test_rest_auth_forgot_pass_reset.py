from unittest.mock import patch

import patreon
from nose.tools import *
from patreon.app.web.pages.rest_auth_forgot_pass_reset import handle_forgot_pass
from patreon.util import datetime
from patreon.model.manager.auth_mgr import check_password
from patreon.model.table import ForgotPassword
from patreon.util.unsorted import jsencode
from test.fixtures.session_fixtures import create_web_session, get_and_follow
from test.fixtures.user_fixtures import patron


@patreon.test.manage()
def test_errors():
    user_ids = [None, 'a', 'a']
    tokens = ['a', None, 'a']
    passwords = ['a', 'a', None]

    for user_id, token, password in zip(user_ids, tokens, passwords):
        resp, code = handle_forgot_pass(user_id, token, password)
        assert_equals(code, 400)
        assert_true(resp and 'matches the email' in resp)

    user_ids = ['1', patron().UID]
    tokens = ['1', '1']
    password = 'blah'

    for user_id, token in zip(user_ids, tokens):
        resp, code = handle_forgot_pass(user_id, token, password)
        assert_equals(code, 400)
        assert_true(resp and 'matches the email' not in resp)


@patreon.test.manage()
def test_okay():
    user = patron()
    token = "12564"
    password = "blah"
    ForgotPassword.insert({'UID': user.UID, 'SecretID': token, 'Created': datetime.datetime.now()})
    resp, code = handle_forgot_pass(user.UID, token, password)
    assert_equals(code, 200)
    assert_true(check_password(user.UID, password))


@patreon.test.manage()
@patch('patreon.app.web.pages.rest_auth_forgot_pass_reset.handle_forgot_pass', return_value=(jsencode({}), 200))
def test_request(mock):
    patron_user = patron()
    session = create_web_session(patron_user)

    resp = get_and_follow(session, '/REST/auth/forgot_password_reset')
    assert_equals(resp.status_code, 405)

    resp = session.post('/REST/auth/forgot_password_reset', {'user_id': '1', 'security_token': 'b', 'password': 'c'},
                        use_form=True)
    assert_equals(resp.headers['Content-Type'], 'application/json')
    mock.assert_called_with(1, 'b', 'c')
    resp = session.post('/REST/auth/forgot_password_reset', {'user_id': 'a', 'security_token': 'b', 'password': 'c'},
                        use_form=True)
    assert_equals(resp.headers['Content-Type'], 'application/json')
    mock.assert_called_with(None, 'b', 'c')

    assert_equals(mock.call_count, 2)
