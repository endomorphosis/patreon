from unittest.mock import patch

import patreon
from nose.tools import *
from patreon.app.web.pages.create4 import handle_create4_post
from patreon.model.manager import campaign_mgr, reward_mgr
from patreon.util.unsorted import jsencode
from test.fixtures.session_fixtures import create_web_session, create_unauthed_web_session
from test.fixtures.user_fixtures import creator, patron
from test.test_helpers.campaign_helpers import assert_thanks_data, assert_creator_has_n_rewards, assert_reward_is
from test.test_helpers.redirect_helpers import assert_redirects
from werkzeug.datastructures import MultiDict


@patreon.test.manage()
def test_update_campaign():
    creator_id = creator().user_id
    campaign_id = campaign_mgr.try_get_campaign_id_by_user_id_hack(creator_id)

    campaign_mgr.update_campaign(
        campaign_id,
        thanks_message='thanks_message',
        thanks_video_url=None,
        thanks_embed='thanks_embed'
    )
    assert_thanks_data(campaign_id, 'thanks_message', '', '')

    campaign_mgr.update_campaign(
        campaign_id,
        thanks_message='thanks_message',
        thanks_video_url='thanks_video_url',
        thanks_embed='thanks_embed'
    )
    assert_thanks_data(campaign_id, 'thanks_message', 'thanks_video_url', 'thanks_embed')


@patreon.test.manage()
def test_create_or_update_reward():
    creator_id = creator().user_id

    assert_creator_has_n_rewards(creator_id, 0)
    result = reward_mgr.create_or_update_reward(creator_id, '', 100, '1', True, '2')
    assert_true(result)
    assert_creator_has_n_rewards(creator_id, 1)

    reward = reward_mgr.find_by_creator_id(creator_id)[0]
    assert_reward_is(reward, 100, True, '1', 2)

    result = reward_mgr.create_or_update_reward(creator_id, reward.reward_id, 200, '3', False, '4')
    assert_true(result)
    assert_creator_has_n_rewards(creator_id, 1)
    reward = reward_mgr.find_by_creator_id(creator_id)[0]
    assert_reward_is(reward, 200, False, '3', 4)

    result = reward_mgr.create_or_update_reward(creator_id, 'blah', 200, '3', False, '4')
    assert_equals(False, result)

    creator_user = creator()

    result = reward_mgr.create_or_update_reward(creator_user, reward.reward_id, 200, '3', False, '4')
    assert_equals(False, result)


@patreon.test.manage()
def test_delete_missing_rewards():
    creator_id = creator().user_id

    reward_mgr.delete_missing_rewards(creator_id, [])
    reward_mgr.delete_missing_rewards(creator_id, ['9999'])

    assert_creator_has_n_rewards(creator_id, 0)
    result = reward_mgr.create_or_update_reward(creator_id, '', 100, '1', True, '2')
    assert_true(result)
    assert_creator_has_n_rewards(creator_id, 1)

    reward_mgr.delete_missing_rewards(creator_id, ['9999'])
    assert_creator_has_n_rewards(creator_id, 0)

    result = reward_mgr.create_or_update_reward(creator_id, '', 100, '1', True, '2')
    assert_true(result)
    assert_creator_has_n_rewards(creator_id, 1)
    reward = reward_mgr.find_by_creator_id(creator_id)[0]

    reward_mgr.delete_missing_rewards(creator_id, [str(reward.reward_id)])
    assert_creator_has_n_rewards(creator_id, 1)
    assert_equals(reward, reward_mgr.find_by_creator_id(creator_id)[0])


@patreon.test.manage()
def test_handle_errors():
    patron_id = patron().user_id
    creator_id = creator().user_id

    resp, code = handle_create4_post(patron_id, '', '', '', '', '', '', '', '')
    assert_equals(code, 400)

    resp, code = handle_create4_post(creator_id, ['a'], [], [], [], [], '', '', '')
    assert_equals(code, 400)

    with assert_redirects(to='/user?alert=1'):
        handle_create4_post(creator_id, ['a'], ['b'], ['c'], ['d'], ['e'], '', '', '')
    assert_creator_has_n_rewards(creator_id, 0)

    with assert_redirects(to='/create4'):
        handle_create4_post(creator_id, ['a'], ['1'], ['c'], ['1'], ['e'], '', '', '')


@patreon.test.manage()
def test_handle_okay():
    creator_id = creator().user_id

    with assert_redirects(to='/user?alert=1'):
        handle_create4_post(creator_id, [''], ['1'], ['b'], ['2'], ['3'], '', '', '')
    assert_creator_has_n_rewards(creator_id, 1)

    reward = reward_mgr.find_by_creator_id(creator_id)[0]

    with assert_redirects(to='/user?alert=1'):
        handle_create4_post(
            creator_id,
            ['', str(reward.reward_id)],
            ['1', '2'],
            ['c', 'f'],
            ['1', '0'],
            ['', '5'],
            '', '', ''
        )
    assert_creator_has_n_rewards(creator_id, 2)
    assert_reward_is(reward, 200, False, 'f', 5)

    with assert_redirects(to='/user?alert=1'):
        handle_create4_post(
            creator_id,
            ['', '', ''],
            ['1', '2', '3'],
            ['c', 'f', 'h'],
            ['1', '1', '2'],
            ['', '', ''],
            '', '', ''
        )

    assert_creator_has_n_rewards(creator_id, 3)


@patreon.test.manage()
@patch('patreon.app.web.pages.create4.handle_create4_post',
       return_value=(jsencode({}), 200))
def test_request(mock):
    patron_user = patron()
    session = create_web_session(patron_user)
    session_unauthed = create_unauthed_web_session()

    resp = session_unauthed.post('/create4', {}, use_form=True)
    assert_equals(resp.status_code, 401)

    data = MultiDict({
        'rid[]': 1,
        'rShipping[]': 4,
        'rDescription[]': 7,
        'rAmount[]': 10,
        'rLimit[]': 13,
        'videoUrl': 'a',
        'shareEmbed': 'b',
        'thanksMsg': 'c'
    })
    data.update({
        'rid[]': 2,
        'rShipping[]': 5,
        'rDescription[]': 8,
        'rAmount[]': 11,
        'rLimit[]': 14
    })
    data.update({
        'rid[]': 3,
        'rShipping[]': 6,
        'rDescription[]': 9,
        'rAmount[]': 12,
        'rLimit[]': 15
    })

    resp = session.post('/create4', data, use_form=True)
    assert_equals(200, resp.status_code)
    mock.assert_called_once_with(
        patron_user.user_id,
        ['1', '2', '3'],
        ['10', '11', '12'],
        ['7', '8', '9'],
        ['4', '5', '6'],
        ['13', '14', '15'],
        'a', 'b', 'c'
    )
