"""
Test that notifications is gated by user type.
"""

import patreon
from patreon.app.web import web_app
from test.fixtures.session_fixtures import create_web_session, create_unauthed_web_session
from test.fixtures.user_fixtures import creator, patron

from nose.tools import *

@patreon.test.manage()
def test_notifications_logged_out():
    sess = create_unauthed_web_session()
    resp = sess.get('/updates')
    assert_equal(resp.status_code, 302)
    assert_equal(resp.headers.get('Location'), '/login?ru=%2Fupdates')

@patreon.test.manage()
def test_notifications_patron():
    patron_user = patron()
    sess = create_web_session(patron_user)
    resp = sess.get('/updates')
    assert_equal(resp.status_code, 302)
    assert_equal(resp.headers.get('Location'), '/')

@patreon.test.manage()
def test_notifications_creator():
    creator_user = creator()
    sess = create_web_session(creator_user)
    resp = sess.get('/updates')
    assert_equal(resp.status_code, 200)
