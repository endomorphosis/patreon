from unittest.mock import patch
from nose.tools import *

import patreon
from patreon.app.web.pages.process_comment_delete import handle_delete_comment
from patreon.model.manager import campaign_mgr
from patreon.util.unsorted import jsdecode, jsencode
from test.fixtures.activity_fixtures import create_post
from test.fixtures.comment_fixtures import create_comment, insert_comment
from test.fixtures.session_fixtures import create_web_session, create_unauthed_web_session, get_and_follow
from test.fixtures.user_fixtures import patron, creator


@patreon.test.manage()
def test_errors():
    patron_id = patron().user_id
    other_patron_id = patron().user_id
    creator_id = creator().user_id
    campaign_id = campaign_mgr.try_get_campaign_id_by_user_id_hack(creator_id)
    post_id = create_post(patron_id, campaign_id).activity_id

    comment_okay = insert_comment(patron_id, post_id)
    comment_bad = insert_comment(patron_id, 999)

    resp, code = handle_delete_comment('notanid', comment_okay.comment_id)
    # Don't ruin their wedding - Sam Yam
    assert_equal(code, 401)
    assert_is_not_none(jsdecode(resp))
    assert_is_not_none(jsdecode(resp).get('error'))
    assert_equals('This route is restricted to logged in users.', jsdecode(resp)['error'])

    resp, code = handle_delete_comment(patron_id, 'notanid')
    assert_equal(code, 404)
    assert_is_not_none(jsdecode(resp))
    assert_is_not_none(jsdecode(resp).get('error'))
    assert_equals("Comment with id notanid was not found.", jsdecode(resp)['error'])

    resp, code = handle_delete_comment(other_patron_id, comment_okay.comment_id)
    assert_equal(code, 403)
    assert_is_not_none(jsdecode(resp))
    assert_is_not_none(jsdecode(resp).get('error'))
    assert_equals(
        'You do not have permission to delete comment with id ' + str(comment_okay.comment_id) + '.',
        jsdecode(resp)['error']
    )

    resp, code = handle_delete_comment(patron_id, comment_bad.comment_id)
    assert_equal(code, 404)
    assert_is_not_none(jsdecode(resp))
    assert_is_not_none(jsdecode(resp).get('error'))
    assert_equals('Post with id ' + str(comment_bad.post_id) + ' was not found.', jsdecode(resp)['error'])


@patreon.test.manage()
def test_okay():
    patron_id = patron().user_id
    creator_id = creator().user_id
    campaign_id = campaign_mgr.try_get_campaign_id_by_user_id_hack(creator_id)
    post_id = create_post(patron_id, campaign_id).activity_id

    comment_delete_single_id = create_comment(patron_id, post_id).comment_id
    comment_delete_creator_id = create_comment(patron_id, post_id).comment_id
    comment_delete_parent = create_comment(patron_id, post_id)
    comment_delete_parent_id = comment_delete_parent.comment_id

    create_comment(patron_id, post_id, thread_id=comment_delete_parent_id)
    assert_true(comment_delete_parent.has_children)

    resp, code = handle_delete_comment(patron_id, comment_delete_single_id)
    assert_equals(code, 200)
    assert_equals(jsencode(None), resp)

    resp, code = handle_delete_comment(creator_id, comment_delete_creator_id)
    assert_equals(code, 200)
    assert_equals(jsencode(None), resp)

    resp, code = handle_delete_comment(patron_id, comment_delete_parent_id)
    assert_equals(code, 200)
    assert_is_not_none(jsdecode(resp))
    assert_is_not_none(jsdecode(resp).get('CID'))
    assert_equals(jsdecode(resp)['CID'], comment_delete_parent_id)
    assert_equals(jsdecode(resp)['Comment'], '')


@patreon.test.manage()
@patch('patreon.app.web.pages.process_comment_delete.handle_delete_comment',
       return_value=(jsencode({}), 200))
def test_request(mock):
    patron_user = patron()
    session = create_web_session(patron_user)
    session_unauthed = create_unauthed_web_session()

    resp = get_and_follow(session, '/processCommentDelete')
    assert_equals(resp.status_code, 405)

    resp = session_unauthed.post('/processCommentDelete', {}, use_form=True)
    assert_equals(resp.status_code, 401)

    resp = session.post('/processCommentDelete', {'cid': '1'}, use_form=True)
    assert_equals(resp.headers['Content-Type'], 'application/json')
    mock.assert_called_with(patron_user.user_id, 1)

    resp = session.post('/processCommentDelete', {'cid': 'c'}, use_form=True)
    assert_equals(resp.headers['Content-Type'], 'application/json')
    mock.assert_called_with(patron_user.user_id, None)

    assert_equals(mock.call_count, 2)
