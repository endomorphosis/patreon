from unittest.mock import patch, DEFAULT

from nose.tools import *
import patreon
from patreon.model.manager import youtube_import_mgr
from patreon.model.table import UserExtra
from patreon.app.web.pages import process_youtube_import
from test.fixtures.activity_fixtures import create_post
from test.fixtures.session_fixtures import create_web_session
from test.fixtures.user_fixtures import creator


@patreon.test.manage()
def test_get_channel_id():
    corpus = [
        ('https://www.youtube.com/user/xoxofest', 'xoxofest'),
        ('https://www.youtube.com/user/vlogbrothers/videos', 'vlogbrothers'),
        ('https://www.youtube.com/channel/UCX6b17PVsYBQ0ip5gyeme-Q?q=1', 'UCX6b17PVsYBQ0ip5gyeme-Q'),
        ('https://www.youtube.com/blimeycow#riid', 'blimeycow'),
        ('https://www.google.com', None)
    ]

    for url, channel_id in corpus:
        assert_equals(channel_id, process_youtube_import.get_channel_id(url))


@patreon.test.manage()
def test_dont_import_youtube():
    creator_user = creator()
    session = create_web_session(creator_user)
    response = session.post(
        '/processYoutubeImport?no=1',
        {'YTUrl': 'https://www.youtube.com/user/PomplamooseMusic'},
        use_form=True
    )

    assert_equals(200, response.status_code)
    extra_info = UserExtra.get(creator_user.user_id)
    assert_equals(2, extra_info.is_youtube_flagged)


@patreon.test.manage()
def test_import_youtube_fresh():
    channel_data, playlist_page1, playlist_page2, video_details = get_test_data()

    creator_user = creator()
    session = create_web_session(creator_user)
    with patch.multiple('patreon.services.youtube',
                        get_user_channel_by_username=DEFAULT,
                        get_playlist_items=DEFAULT,
                        get_details_for_videos=DEFAULT) as values:
        # setup mocked functions
        def playlist_value(playlist_id, next_page_token=None):
            if next_page_token is None:
                return playlist_page1
            else:
                return playlist_page2

        values['get_user_channel_by_username'].return_value = channel_data
        values['get_playlist_items'].side_effect = playlist_value
        values['get_details_for_videos'].return_value = video_details

        response = session.post(
            '/processYoutubeImport?no=0',
            {'YTUrl': 'https://www.youtube.com/user/xoxofest'},
            use_form=True
        )

        assert_equals(200, response.status_code)
        extra_info = UserExtra.get(creator_user.user_id)
        assert_equals(1, extra_info.is_youtube_flagged)
        assert_equals(3, youtube_import_mgr.count_for_user(creator_user.user_id))

        # a second import won't add any more records, since they've already been added
        response = session.post(
            '/processYoutubeImport?no=0',
            {'YTUrl': 'https://www.youtube.com/user/xoxofest'},
            use_form=True
        )
        assert_equals(3, youtube_import_mgr.count_for_user(creator_user.user_id))


@patreon.test.manage()
def test_import_youtube_one_page():
    channel_data, playlist_page1, _, video_details = get_test_data()

    no_token_playlist = {}
    no_token_playlist.update(playlist_page1)
    # remove nextPageToken from the first page of results
    del no_token_playlist['nextPageToken']

    creator_user = creator()
    session = create_web_session(creator_user)
    with patch.multiple('patreon.services.youtube',
                        get_user_channel_by_username=DEFAULT,
                        get_playlist_items=DEFAULT,
                        get_details_for_videos=DEFAULT) as values:
        # setup mocked functions
        values['get_user_channel_by_username'].return_value = channel_data
        values['get_playlist_items'].return_value = no_token_playlist
        values['get_details_for_videos'].return_value = video_details

        response = session.post(
            '/processYoutubeImport?no=0',
            {'YTUrl': 'https://www.youtube.com/user/xoxofest'},
            use_form=True
        )

        assert_equals(200, response.status_code)
        extra_info = UserExtra.get(creator_user.user_id)
        assert_equals(1, extra_info.is_youtube_flagged)
        assert_equals(3, youtube_import_mgr.count_for_user(creator_user.user_id))

        # a second import won't add any more records, since they've already been added
        response = session.post(
            '/processYoutubeImport?no=0',
            {'YTUrl': 'https://www.youtube.com/user/xoxofest'},
            use_form=True
        )
        assert_equals(3, youtube_import_mgr.count_for_user(creator_user.user_id))


@patreon.test.manage()
def test_import_youtube_posted():
    channel_data, playlist_page1, playlist_page2, video_details = get_test_data()

    creator_user = creator()
    session = create_web_session(creator_user)

    video_id = playlist_page1['items'][0]['contentDetails']['videoId']
    create_post(
        creator_user.user_id,
        1,
        link_url='https://www.youtube.com/watch?v=' + video_id
    )

    with patch.multiple('patreon.services.youtube',
                        get_user_channel_by_username=DEFAULT,
                        get_playlist_items=DEFAULT,
                        get_details_for_videos=DEFAULT) as values:
        # setup mocked functions
        def playlist_value(playlist_id, next_page_token=None):
            if next_page_token is None:
                return playlist_page1
            else:
                return playlist_page2

        values['get_user_channel_by_username'].return_value = channel_data
        values['get_playlist_items'].side_effect = playlist_value
        values['get_details_for_videos'].return_value = video_details

        response = session.post(
            '/processYoutubeImport?no=0',
            {'YTUrl': 'https://www.youtube.com/user/xoxofest'},
            use_form=True
        )

        assert_equals(200, response.status_code)
        extra_info = UserExtra.get(creator_user.user_id)
        assert_equals(1, extra_info.is_youtube_flagged)
        assert_equals(2, youtube_import_mgr.count_for_user(creator_user.user_id))


@patreon.test.manage()
def test_import_empty_channel():
    creator_user = creator()
    session = create_web_session(creator_user)

    with patch.multiple('patreon.services.youtube',
                        get_user_channel_by_username=DEFAULT,
                        get_user_channel_by_channel_name=DEFAULT) as values:
        values['get_user_channel_by_username'].return_value = {'items': []}
        values['get_user_channel_by_channel_name'].return_value = {'items': []}

        response = session.post(
            '/processYoutubeImport?no=0',
            {'YTUrl': 'https://www.youtube.com/user/notrealuser'},
            use_form=True
        )

        assert_equals(200, response.status_code)
        extra_info = UserExtra.get(creator_user.user_id)
        # we still flag this user as "imported", even if they have nothing
        assert_equals(1, extra_info.is_youtube_flagged)
        assert_equals(0, youtube_import_mgr.count_for_user(creator_user.user_id))


@patreon.test.manage()
def test_pending_imports_gets_biggest_thumbnail():
    _, _, _, video_details = get_test_data()
    imports = process_youtube_import.make_pending_imports(
        1,
        video_details['items']
    )

    assert_equals(
        imports[0]['ImageUrl'],
        video_details['items'][0]['snippet']['thumbnails']['maxres']['url']
    )
    assert_equals(
        imports[1]['ImageUrl'],
        video_details['items'][1]['snippet']['thumbnails']['huge']['url']
    )
    assert_equals(
        imports[2]['ImageUrl'],
        video_details['items'][2]['snippet']['thumbnails']['default']['url']
    )


def get_test_data():
    channel_data = {
        'items': [{
            'contentDetails': {
                'relatedPlaylists': {'uploads': 'UUqMG_BBwxrhLG80Y3yuEu-Q'},
            }
        }]
    }
    playlist_page1 = {
        "nextPageToken": "CDIQAA",
        "items": [
            {"contentDetails": {"videoId": "R_79KQlN5H0"}},
            {"contentDetails": {"videoId": "WSL5qVL3Mng"}},
            {"contentDetails": {"videoId": "0IUHlxWrYBw"}}
        ]
    }
    playlist_page2 = {"items": []}
    video_details = {
        "items": [
            {
                "snippet": {
                    "description": "A look back at the third XOXO, held September 11-14 in Portland, Oregon\u2014a four-day arts and technology festival celebrating independent artists, hackers, and makers using the Internet to make a living doing what they love. For more information: http://xoxofest.com/\n\nVideo thumbnail by Ian Linkletter:\nhttps://www.flickr.com/photos/linkletter/15129946299/\n\nVideo production by Searle Video:\nhttp://www.searlevideo.com/\n\nMusic is Broke for Free's \"Something Elated,\" used very gratefully under a Creative Commons Attribution 3.0 license. Go buy all his music.\nhttp://freemusicarchive.org/music/Broke_For_Free/Something_EP/Broke_For_Free_-_Something_EP_-_05_Something_Elated",
                    "publishedAt": "2014-10-29T18:18:55.000Z",
                    "thumbnails": {
                        "default": {
                            "url": "https://i.ytimg.com/vi/R_79KQlN5H0/default.jpg",
                            "width": 100,
                            "height": 80
                        },
                        "maxres": {
                            "url": "https://i.ytimg.com/vi/R_79KQlN5H0/maxresdefault.jpg",
                            "width": 200,
                            "height": 160
                        }
                    },
                    "title": "Remembering XOXO 2014"
                },
                "id": "R_79KQlN5H0"
            },
            {
                "snippet": {
                    "description": "Paul Ford isn't easy to sum up, so we originally copped out and wrote the silly one-line bio we read before his talk. He's one of the most talented writers and programmers we know, frighteningly and frustratingly accomplished at both. He most recently created Tilde.club\u2014a Unix server that is definitely not a social network\u2014writes for The Message on Medium, and is working on a book about web pages for FSG while raising twins in Brooklyn, NY. He is a really great hugger.\n\nRecorded in September 2014 at XOXO, an arts and technology festival in Portland, Oregon celebrating independent artists using the Internet to make a living doing what they love. For more, visit http://xoxofest.com.\n\nVideo thumbnail by Ian Linkletter:\nhttps://www.flickr.com/photos/linkletter/15317253482/\n\nIntro Music: Broke for Free, \"Only Instrumental\"\nhttp://bit.ly/xo2014broke",
                    "publishedAt": "2014-10-27T17:46:29.000Z",
                    "thumbnails": {
                        "default": {
                            "url": "https://i.ytimg.com/vi/WSL5qVL3Mng/default.jpg",
                            "width": 100,
                            "height": 80
                        },
                        "huge": {
                            "url": "https://i.ytimg.com/vi/WSL5qVL3Mng/hugedefault.jpg",
                            "width": 200,
                            "height": 160
                        }
                    },
                    "title": "Paul Ford, Ftrain - XOXO Festival (2014)"
                },
                "id": "WSL5qVL3Mng"
            },
            {
                "snippet": {
                    "description": "Last year, Edna Piranha (aka Jen Fong-Adwent) launched Meatspace, a chatroom that snaps a two-second animated GIF of your face every time you post. No usernames, no registration, and no history\u2014messages last minutes before being deleted forever. The results were unexpected: a tight-knit community of virtual strangers turned real-life friends.\n\nRecorded in September 2014 at XOXO, an arts and technology festival in Portland, Oregon celebrating independent artists using the Internet to make a living doing what they love. For more, visit http://xoxofest.com.\n\nVideo thumbnail by Ian Linkletter:\nhttps://www.flickr.com/photos/linkletter/15317237092/\n\nIntro Music: Broke for Free, \"Only Instrumental\"\nhttp://bit.ly/xo2014broke",
                    "publishedAt": "2014-10-22T18:31:12.000Z",
                    "thumbnails": {
                        "default": {
                            "url": "https://i.ytimg.com/vi/0IUHlxWrYBw/default.jpg",
                            "width": 100,
                            "height": 80
                        },
                        "mini": {
                            "url": "https://i.ytimg.com/vi/0IUHlxWrYBw/bigdefault.jpg",
                            "width": 50,
                            "height": 40
                        }
                    },
                    "title": "Edna Piranha, Meatspace - XOXO Festival (2014)"
                },
                "id": "0IUHlxWrYBw"
            }
        ]
    }
    return channel_data, playlist_page1, playlist_page2, video_details
