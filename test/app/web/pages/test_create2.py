from unittest.mock import patch

import patreon
from nose.tools import *
from patreon.app.web.pages.create2 import handle_create2_post
from patreon.model.manager import campaign_mgr
from patreon.util.unsorted import jsencode
from test.fixtures.session_fixtures import create_web_session, create_unauthed_web_session
from test.fixtures.user_fixtures import patron, creator
from test.test_helpers.campaign_helpers import assert_campaign_data
from test.test_helpers.redirect_helpers import assert_redirects


@patreon.test.manage()
def test_update_campaign():
    creator_id = creator().user_id
    campaign_id = campaign_mgr.try_get_campaign_id_by_user_id_hack(creator_id)

    campaign_mgr.update_campaign(
        campaign_id,
        summary='summary',
        video_url='video_url',
        video_embed='video_embed'
    )
    assert_campaign_data(campaign_id, 'summary', 'video_url', 'video_embed')

    campaign_mgr.update_campaign(
        campaign_id,
        summary='not_summary',
        video_url=None,
        video_embed=None
    )
    assert_campaign_data(campaign_id, 'not_summary', 'video_url', 'video_embed')


@patreon.test.manage()
def test_handle_create2_post():
    patron_id = patron().user_id
    creator_id = creator().user_id
    campaign_id = campaign_mgr.try_get_campaign_id_by_user_id_hack(creator_id)

    with assert_redirects():
        handle_create2_post(creator_id, 'summary', 'video_url', 'video_embed')
    assert_campaign_data(campaign_id, 'summary', 'video_url', 'video_embed')

    with assert_redirects():
        handle_create2_post(creator_id, 'd', None, None)
    assert_campaign_data(campaign_id, 'd', 'video_url', 'video_embed')

    resp, code = handle_create2_post(patron_id, 'summary', 'video_url', 'video_embed')
    assert_equals(code, 400)


@patreon.test.manage()
@patch('patreon.app.web.pages.create2.handle_create2_post',
       return_value=(jsencode({}), 200))
def test_request(mock):
    creator_user = creator()
    session = create_web_session(creator_user)
    session_unauthed = create_unauthed_web_session()

    resp = session_unauthed.post('/create2', {}, use_form=True)
    assert_equals(resp.status_code, 401)

    data = {
        'about': 'summary',
        'videoUrl': 'video_url',
        'shareEmbed': 'video_embed'
    }

    resp = session.post('/create2', data, use_form=True)
    assert_equals(200, resp.status_code)
    mock.assert_called_once_with(creator_user.user_id, 'summary', 'video_url', 'video_embed')
