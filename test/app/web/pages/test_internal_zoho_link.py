from nose.tools import assert_equals, raises
import patreon
from patreon.app.web.pages.internal_zoho_link import handle_zoho
from patreon.exception.auth_errors import UnauthorizedAdminFunction
from patreon.model.manager import zoho_campaigns_mgr, user_mgr
from test.fixtures.session_fixtures import create_web_session
from test.fixtures.user_fixtures import patron
from test.test_helpers.user_helpers import get_new_logged_in_admin_creator


@patreon.test.manage()
def test_zoho_link_okay():
    admin_user, admin_session = get_new_logged_in_admin_creator()
    campaign_id = user_mgr.get(admin_user.user_id).campaigns[0].campaign_id

    response = admin_session.post('/internal/zoho_link', {
        'zoho_id': 1,
        'campaign_id': campaign_id
    }, use_form=True)

    assert_equals(response.status_code, 302)

    zoho_map = zoho_campaigns_mgr.get(campaign_id, None)
    assert_equals(zoho_map.zoho_id, '1')

    response = admin_session.post('/internal/zoho_link', {
        'zoho_id': 2,
        'campaign_id': campaign_id
    }, use_form=True)

    assert_equals(response.status_code, 302)

    zoho_map = zoho_campaigns_mgr.get(campaign_id, None)
    assert_equals(zoho_map.zoho_id, '2')

    with admin_session:
        handle_zoho(campaign_id, '3')
        zoho_map = zoho_campaigns_mgr.get(campaign_id, None)
        assert_equals(zoho_map.zoho_id, '3')


@patreon.test.manage()
@raises(UnauthorizedAdminFunction)
def test_zoho_link_fail():
    patron_user = patron()
    patron_session = create_web_session(patron_user)

    response = patron_session.post('/internal/zoho_link', {
        'zoho_id': 1,
        'campaign_id': 1
    }, use_form=True)

    assert_equals(response.status_code, 403)

    with patron_session:
        handle_zoho(1, 1)
