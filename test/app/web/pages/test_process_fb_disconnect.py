import patreon
from nose.tools import *
from patreon.model.manager import user_mgr
from test.fixtures.session_fixtures import create_web_session, create_unauthed_web_session
from test.fixtures.user_fixtures import patron


@patreon.test.manage()
def test_errors():
    session = create_unauthed_web_session()
    resp = session.post('/processFBDisconnect', {}, use_form=True)
    assert_equals(403, resp.status_code)


@patreon.test.manage()
def test_allow_delete_nonexistent():
    p = patron()

    assert_is_none(p.facebook_id)
    session = create_web_session(p)
    resp = session.post('/processFBDisconnect', {}, use_form=True)
    assert_equals(200, resp.status_code)


@patreon.test.manage()
def test_delete_facebook_id():
    fake_facebook_id = 100000
    p = patron()
    user_mgr.update_user(p.user_id, p.user_id, facebook_id=fake_facebook_id)
    assert_is_not_none(user_mgr.get_unique(facebook_id=fake_facebook_id))

    session = create_web_session(p)
    resp = session.post('/processFBDisconnect', {}, use_form=True)
    assert_equals(200, resp.status_code)

    assert_is_none(user_mgr.get_unique(facebook_id=fake_facebook_id))
