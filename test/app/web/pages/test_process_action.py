from unittest.mock import patch
from nose.plugins.attrib import attr
import patreon
from patreon.app.web.pages.process_action import handle_process_action
from patreon.util.unsorted import jsdecode, jsencode
from test.fixtures.activity_fixtures import create_post
from test.fixtures.session_fixtures import create_web_session, get_and_follow
from test.fixtures.session_fixtures import create_unauthed_web_session
from test.fixtures.user_fixtures import patron, creator
from nose.tools import *


@patreon.test.manage()
def test_errors():
    user = creator()
    activity_ids = [None, None, None, create_post(user.campaigns[0].campaign_id, patron().UID).activity_id]
    get_on = 1
    action_types = [None, None, None, 0]

    for activity_id, action_type in zip(activity_ids, action_types):
        resp, code = handle_process_action(user.UID, activity_id, get_on, action_type)
        assert_equals(code, 400)


@patreon.test.manage()
@attr(speed="slower")
def test_okay():
    create = creator()
    users = [patron(), patron(), patron(), patron(), patron()]
    activity = create_post(create.campaigns[0].campaign_id, patron().UID)
    action_type = 1

    like_count = 0
    for user in users:
        resp, code = handle_process_action(user.UID, activity.activity_id, 1, action_type)
        like_count += 1
        assert_equals(code, 200)
        assert_true(jsdecode(resp)['count'] == like_count)

    for user in users:
        resp, code = handle_process_action(user.UID, activity.activity_id, 0, action_type)
        like_count -= 1
        assert_equals(code, 200)
        assert_true(jsdecode(resp)['count'] == like_count)

    for user in users:
        resp, code = handle_process_action(user.UID, activity.activity_id, 1, action_type)
        like_count += 1
        assert_equals(code, 200)
        assert_true(jsdecode(resp)['count'] == like_count)

    on = True
    for user in users:
        resp, code = handle_process_action(user.UID, activity.activity_id, on, action_type)
        if not on:
            like_count -= 1
        assert_equals(code, 200)
        assert_true(jsdecode(resp)['count'] == like_count)
        on = not on


@patreon.test.manage()
@attr(speed="slower")
def test_okay2():
    create = creator()
    users = [patron(), patron(), patron(), patron(), patron()]
    activity = create_post(create.campaigns[0].campaign_id, patron().UID)
    action_type = 1

    like_count = 0

    on = True
    for user in users:
        resp, code = handle_process_action(user.UID, activity.activity_id, on, action_type)
        if on:
            like_count += 1
        assert_equals(code, 200)
        assert_true(jsdecode(resp)['count'] == like_count)
        on = not on

@patreon.test.manage()
@patch('patreon.app.web.pages.process_action.handle_process_action', return_value=(jsencode({}), 200))
def test_request(mock):
    patron_user = patron()
    session = create_web_session(patron_user)
    session_unauthed = create_unauthed_web_session()

    resp = get_and_follow(session, '/processAction')
    assert_equals(resp.status_code, 405)

    resp = session_unauthed.post('/processAction', {}, use_form=True)
    assert_equals(resp.status_code, 401)

    resp = session.post('/processAction?hid=1&on=2&ty=3', {}, use_form=True)
    assert_equals(resp.headers['Content-Type'], 'application/json')
    mock.assert_called_with(patron_user.UID, 1, 2, 3)

    resp = session.post('/processAction?hid=a&on=b&ty=c', {}, use_form=True)
    assert_equals(resp.headers['Content-Type'], 'application/json')
    mock.assert_called_with(patron_user.UID, None, None, None)

    assert_equals(mock.call_count, 2)
