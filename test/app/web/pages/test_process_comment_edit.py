from unittest.mock import patch
from nose.tools import *

import patreon
from patreon.app.web.pages.process_comment_edit import handle_process_comment_edit
from patreon.model.manager import campaign_mgr
from patreon.model.table import Comment
from patreon.util.unsorted import jsdecode, jsencode
from test.fixtures.activity_fixtures import create_post
from test.fixtures.comment_fixtures import create_comment
from test.fixtures.session_fixtures import create_web_session, \
    create_unauthed_web_session, get_and_follow
from test.fixtures.user_fixtures import patron, creator


@patreon.test.manage()
def test_errors():
    creator_id = creator().user_id
    campaign_id = campaign_mgr.try_get_campaign_id_by_user_id_hack(creator_id)
    patron_id = patron().user_id
    post_id = create_post(patron_id, campaign_id).activity_id
    comment_id = create_comment(creator_id, post_id).comment_id

    comment_ids = [comment_id, None, comment_id]
    user_ids = ['notanid', patron_id, patron_id]
    comment_texts = ['okay', 'okay', None]

    for user_id, comment_id, comment_text in zip(user_ids, comment_ids, comment_texts):
        resp, code = handle_process_comment_edit(user_id, comment_id, comment_text)
        assert_equal(code, 400)
        assert_true(jsdecode(resp))
        assert_true(jsdecode(resp).get('error'))
        assert_true(jsdecode(resp)['error'] == 'Invalid parameters')

    comment_text = 'doesnt matter'
    resp, code = handle_process_comment_edit(patron_id, comment_id, comment_text)
    assert_equals(code, 403)
    assert_true(jsdecode(resp))
    assert_true(jsdecode(resp).get('error'))
    assert_true(jsdecode(resp)['error'] == 'Not authorized')


@patreon.test.manage()
def test_okay():
    creator_id = creator().user_id
    campaign_id = campaign_mgr.try_get_campaign_id_by_user_id_hack(creator_id)
    post_id = create_post(creator_id, campaign_id).activity_id
    comment_id = create_comment(creator_id, post_id).comment_id

    comment_text = 'does matter'
    resp, code = handle_process_comment_edit(creator_id, comment_id, comment_text)
    assert_equals(code, 200)
    assert_true(jsdecode(resp))
    assert_true(jsdecode(resp).get('Comment'))
    assert_true(jsdecode(resp)['Comment'] == comment_text)

    assert_true(Comment.get(comment_id=comment_id).comment_text == comment_text)


@patreon.test.manage()
@patch('patreon.app.web.pages.process_comment_edit.handle_process_comment_edit',
       return_value=(jsencode({}), 200))
def test_request(mock):
    patron_user = patron()
    session = create_web_session(patron_user)
    session_unauthed = create_unauthed_web_session()

    resp = get_and_follow(session, '/processCommentEdit')
    assert_equals(resp.status_code, 405)

    resp = session_unauthed.post('/processCommentEdit', {}, use_form=True)
    assert_equals(resp.status_code, 401)

    resp = session.post('/processCommentEdit', {'cid': '1', 'new_message': 'new_message'}, use_form=True)
    assert_equals(resp.headers['Content-Type'], 'application/json')
    mock.assert_called_with(patron_user.user_id, 1, 'new_message')

    resp = session.post('/processCommentEdit', {'cid': 'c', 'new_message': 'new_message'}, use_form=True)
    assert_equals(resp.headers['Content-Type'], 'application/json')
    mock.assert_called_with(patron_user.user_id, None, 'new_message')

    assert_equals(mock.call_count, 2)
