from unittest.mock import patch
from nose.tools import *
from werkzeug.datastructures import MultiDict

import patreon
from patreon.model.manager import campaign_mgr, category_mgr, user_mgr
from test.fixtures.session_fixtures import create_web_session, create_unauthed_web_session
from test.fixtures.user_fixtures import patron, creator
from test.test_helpers.campaign_helpers import assert_user_has_n_categories, \
    assert_first_category_is, create_campaign
from test.test_helpers.mail_helpers import AssertSendsMail


@patreon.test.manage()
def test_add_category():
    valid_category_id = 4
    invalid_category_id = 'blah'

    user_id = patron().user_id
    assert_user_has_n_categories(user_id, 0)

    category_mgr.add_category(user_id, valid_category_id)
    assert_user_has_n_categories(user_id, 1)
    assert_first_category_is(user_id, valid_category_id)

    category_mgr.add_category(user_id, invalid_category_id)
    assert_user_has_n_categories(user_id, 1)
    assert_first_category_is(user_id, valid_category_id)

    category_mgr.add_category(user_id, None)
    assert_user_has_n_categories(user_id, 1)
    assert_first_category_is(user_id, valid_category_id)


@patreon.test.manage()
def test_update_campaign():
    creator_id = creator().user_id
    campaign_id = campaign_mgr.try_get_campaign_id_by_user_id_hack(creator_id)

    campaign_mgr.update_campaign(
        campaign_id=campaign_id,
        one_liner='1',
        pay_per_name='2',
        creation_name='3',
        is_monthly= True,
        is_nsfw=False,
        image_url=None,
        image_small_url=None
    )

    campaign = campaign_mgr.get(campaign_id)
    assert_equals(campaign.one_liner, '1')
    assert_equals(campaign.pay_per_name, 'month') # is_monthly = True
    assert_equals(campaign.creation_name, '3')
    assert_equals(campaign.is_monthly, True)
    assert_equals(campaign.is_nsfw, False)

    campaign_mgr.update_campaign(
        campaign_id=campaign_id,
        one_liner='a',
        pay_per_name=None,
        creation_name=None,
        is_monthly= None,
        is_nsfw=None,
        image_url=None,
        image_small_url=None
    )
    campaign = campaign_mgr.get(campaign_id)
    assert_equals(campaign.one_liner, 'a')
    assert_equals(campaign.pay_per_name, 'month') # is_monthly = True
    assert_equals(campaign.creation_name, '3')
    assert_equals(campaign.is_monthly, True)
    assert_equals(campaign.is_nsfw, False)


@patreon.test.manage()
def test_handle_create_post():
    creator_user = creator()
    patron_user = patron()

    campaign_mgr.create_or_update_campaign(
        user_id=creator_user.user_id,
        first_name="X",
        last_name="Y",
        one_liner="b",
        pay_per_name="c",
        creation_name="d",
        is_monthly=True,
        is_nsfw=False,
        image_url="e",
        image_small_url="f",
        category_ids_raw=[1, 2, 3]
    )

    campaign = campaign_mgr.try_get_campaign_by_user_id_hack(creator_user.user_id)
    assert_equals(creator_user.full_name, 'X Y')
    assert_equals(campaign.one_liner, 'b')
    assert_equals(campaign.pay_per_name, 'month') # is_monthly = True
    assert_equals(campaign.creation_name, 'd')
    assert_equals(campaign.is_monthly, True)
    assert_equals(campaign.is_nsfw, False)
    assert_equals(campaign.image_url, 'e')
    assert_equals(campaign.image_small_url, 'f')
    assert_equals(category_mgr.find_by_user_id(creator_user.user_id).count(), 3)

    assert_equals(0, len(patron_user.campaigns))
    campaign_mgr.create_or_update_campaign(
        user_id=patron_user.user_id,
        first_name="X",
        last_name="Y",
        one_liner="b",
        pay_per_name="c",
        creation_name="d",
        is_monthly=True,
        is_nsfw=False,
        image_url="e",
        image_small_url="f",
        category_ids_raw=[1, 2, 3]
    )

    patron_user = user_mgr.get_user(patron_user.user_id)
    campaign = campaign_mgr.try_get_campaign_by_user_id_hack(patron_user.user_id)
    assert_equals(patron_user.full_name, 'X Y')
    assert_equals(campaign.one_liner, 'b')
    assert_equals(campaign.pay_per_name, 'month') # is_monthly = True
    assert_equals(campaign.creation_name, 'd')
    assert_equals(campaign.is_monthly, True)
    assert_equals(campaign.is_nsfw, False)
    assert_equals(campaign.image_url, 'e')
    assert_equals(campaign.image_small_url, 'f')
    assert_equals(category_mgr.find_by_user_id(patron_user.user_id).count(), 3)

    campaign_mgr.create_or_update_campaign(
        user_id=patron_user.user_id,
        first_name="X",
        last_name="Y",
        one_liner="b",
        pay_per_name="c",
        creation_name="d",
        is_monthly=True,
        is_nsfw=False,
        image_url="e",
        image_small_url="f",
        category_ids_raw=[1, 2, 3]
    )

    patron_user = user_mgr.get_user(patron_user.user_id)
    campaign = campaign_mgr.try_get_campaign_by_user_id_hack(patron_user.user_id)
    assert_equals(patron_user.full_name, 'X Y')
    assert_equals(campaign.one_liner, 'b')
    assert_equals(campaign.pay_per_name, 'month') # is_monthly = True
    assert_equals(campaign.creation_name, 'd')
    assert_equals(campaign.is_monthly, True)
    assert_equals(campaign.is_nsfw, False)
    assert_equals(campaign.image_url, 'e')
    assert_equals(campaign.image_small_url, 'f')

    campaign_mgr.create_or_update_campaign(
        user_id=patron_user.user_id,
        first_name="X",
        last_name="Y",
        one_liner="b",
        pay_per_name="c",
        creation_name="d",
        is_monthly=True,
        is_nsfw=False,
        image_url="e",
        image_small_url="f",
        category_ids_raw=[1, 2, 3]
    )

    patron_user = user_mgr.get_user(patron_user.user_id)
    campaign = campaign_mgr.try_get_campaign_by_user_id_hack(patron_user.user_id)
    assert_equals(patron_user.full_name, 'X Y')
    assert_equals(campaign.one_liner, 'b')
    assert_equals(campaign.pay_per_name, 'month') # is_monthly = True
    assert_equals(campaign.creation_name, 'd')
    assert_equals(campaign.is_monthly, True)
    assert_equals(campaign.is_nsfw, False)
    assert_equals(campaign.image_url, 'e')
    assert_equals(campaign.image_small_url, 'f')


@patreon.test.manage()
@patch('patreon.model.manager.campaign_mgr.create_or_update_campaign')
def test_request(mock):
    patron_user = patron()
    session_unauthed = create_unauthed_web_session()
    resp = session_unauthed.post('/create', {}, use_form=True)
    assert_equals(resp.status_code, 401)

    data = MultiDict({
        'fullName': "Marcos Gaeta",
        'oneline': '2',
        'createSingle': '3',
        'create': '4',
        'monthCheck': None,
        'nsfwCheck': '1',
        'shareImgUrlPhoto': '5',
        'shareImgSmallUrlPhoto': '6'
    })
    data.update({
        'categories[]': '1'
    })
    data.update({
        'categories[]': '2'
    })
    data.update({
        'categories[]': '3'
    })

    session = create_web_session(patron_user)
    resp = session.post('/create', data, use_form=True)
    assert_equals(302, resp.status_code)
    mock.assert_called_once_with(
        patron_user.user_id, "Marcos", "Gaeta", '2', '3', '4', False, True,
        '5', '6', ['1', '2', '3']
    )


@patreon.test.manage()
def test_creator_welcome():
    patron_email = "patron@patreon.com"
    patron_id = patron(patron_email).user_id

    with AssertSendsMail(patron_email, 1):
        create_campaign(patron_id)

    with AssertSendsMail(patron_email, 0):
        create_campaign(patron_id)

    creator_email = "creator@patreon.com"
    creator_id = creator(creator_email).user_id

    with AssertSendsMail(creator_email, 0):
        create_campaign(creator_id)
