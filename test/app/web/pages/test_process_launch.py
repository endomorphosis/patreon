import patreon
from patreon.model.manager import campaign_mgr
from nose.tools import *
from test.fixtures.session_fixtures import create_web_session, create_unauthed_web_session
from test.fixtures.user_fixtures import patron, creator

@patreon.test.manage()
def test_unauthed_launch():
    session = create_unauthed_web_session()
    resp = session.post('/processUserSetup', {}, use_form=True)
    assert_equals(401, resp.status_code)


@patreon.test.manage()
def test_launch_from_existing_campaign():
    patron_user = patron()
    campaigns = campaign_mgr.get_campaigns_by_user_id(patron_user.user_id)
    assert_equals([], campaigns)

    session = create_web_session(patron_user)
    resp = session.post('/processUserSetup', {}, use_form=True)
    assert_equals(200, resp.status_code)

    campaigns = campaign_mgr.get_campaigns_by_user_id(patron_user.user_id)
    assert_equals(1, len(campaigns))
    assert_is_not_none(campaigns[0].published_at)


@patreon.test.manage()
def test_launch_from_no_campaign():
    creator_user = creator()
    campaigns = campaign_mgr.get_campaigns_by_user_id(creator_user.user_id)
    campaign_mgr.unpublish_campaign(campaigns[0].campaign_id)

    campaigns = campaign_mgr.get_campaigns_by_user_id(creator_user.user_id)
    assert_equals(1, len(campaigns))
    assert_is_none(campaigns[0].published_at)

    session = create_web_session(creator_user)
    resp = session.post('/processUserSetup', {}, use_form=True)
    assert_equals(200, resp.status_code)

    campaigns = campaign_mgr.get_campaigns_by_user_id(creator_user.user_id)
    assert_equals(1, len(campaigns))
    assert_is_not_none(campaigns[0].published_at)
