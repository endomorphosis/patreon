from nose.tools import *
import patreon
from patreon.model.manager import rewards_wait_mgr
from test.fixtures.session_fixtures import create_web_session, create_unauthed_web_session
from test.fixtures.reward_fixtures import create_reward
from test.fixtures.user_fixtures import patron, creator


@patreon.test.manage()
def test_add_reward_wait():
    patron_user = patron()
    session = create_web_session(patron_user)
    reward = create_reward(creator().user_id)
    if patron_user.user_id == reward.reward_id:
        reward = create_reward(creator().user_id)

    resp = session.post(
        '/processRewardWait?add=1&rid={0}'.format(reward.reward_id),
        {},
        use_form=True
    )
    assert_equals(200, resp.status_code)

    assert_is_not_none(rewards_wait_mgr.get(patron_user.user_id, reward.reward_id))


@patreon.test.manage()
def test_delete_nonexistent_reward_wait():
    patron_user = patron()
    session = create_web_session(patron_user)
    reward = create_reward(creator().user_id)
    if patron_user.user_id == reward.reward_id:
        reward = create_reward(creator().user_id)

    resp = session.post(
        '/processRewardWait?add=0&rid={0}'.format(reward.reward_id),
        {},
        use_form=True
    )
    assert_equals(200, resp.status_code)


@patreon.test.manage()
def test_delete_extant_reward_wait():
    patron_user = patron()
    session = create_web_session(patron_user)
    reward = create_reward(creator().user_id)
    if patron_user.user_id == reward.reward_id:
        reward = create_reward(creator().user_id)

    rewards_wait_mgr.add(patron_user.user_id, reward.reward_id)
    assert_is_not_none(rewards_wait_mgr.get(patron_user.user_id, reward.reward_id))

    resp = session.post(
        '/processRewardWait?add=0&rid={0}'.format(reward.reward_id),
        {},
        use_form=True
    )
    assert_equals(200, resp.status_code)

    assert_is_none(rewards_wait_mgr.get(patron_user.user_id, reward.reward_id))


@patreon.test.manage()
def test_failed_reward_wait():
    unauthed_session = create_unauthed_web_session()
    resp = unauthed_session.post('/processRewardWait?rid=1', {}, use_form=True)
    assert_equals(400, resp.status_code)

    p = patron()
    session = create_web_session(p)
    resp = session.post('/processRewardWait?add=1', {}, use_form=True)
    assert_equals(400, resp.status_code)

    session = create_web_session(p)
    resp = session.post('/processRewardWait?add=1&rid=garbage', {}, use_form=True)
    assert_equals(400, resp.status_code)
