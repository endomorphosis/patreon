from unittest.mock import patch
import patreon
from patreon.app.web.pages.rest_auth_forgot_pass import handle_rest_auth_forgot_pass
from nose.tools import *
from patreon.model.table import ForgotPassword
from patreon.util.unsorted import jsencode
from test.fixtures.session_fixtures import create_web_session, get_and_follow
from test.fixtures.user_fixtures import patron


@patreon.test.manage()
def test_errors():
    bad_pass = patron()
    bad_pass.update({'Password': None})
    emails = [None, 'notauser', patron(is_nuked=1).Email, bad_pass.Email]
    for email in emails:
        resp, code = handle_rest_auth_forgot_pass(email)
        assert_equals(code, 400)


@patreon.test.manage()
def test_okay():
    user = patron()
    resp = handle_rest_auth_forgot_pass(user.Email)
    assert_equals(resp, jsencode(True))
    token = ForgotPassword.query.filter_by(UID=user.UID).first().token
    assert_true(token)

@patreon.test.manage()
@patch('patreon.app.web.pages.rest_auth_forgot_pass.handle_rest_auth_forgot_pass', return_value=(jsencode({}), 200))
def test_request(mock):
    patron_user = patron()
    session = create_web_session(patron_user)

    resp = get_and_follow(session, '/REST/auth/forgot_password')
    assert_equals(resp.status_code, 405)

    resp = session.post('/REST/auth/forgot_password', {'email': 'a'}, use_form=True)
    assert_equals(resp.headers['Content-Type'], 'application/json')
    mock.assert_called_with('a')

    assert_equals(mock.call_count, 1)
