from unittest.mock import patch

import patreon
from patreon.app.web.pages.process_comment import handle_process_comment
from patreon.model.manager.comment_mgr import check_spam
from patreon.util.unsorted import jsdecode, jsencode
from test.fixtures.activity_fixtures import create_post
from test.fixtures.session_fixtures import create_web_session, \
    create_unauthed_web_session, get_and_follow
from test.fixtures.user_fixtures import patron, creator
from nose.tools import *


@patreon.test.manage()
def test_errors():
    patron_ = patron()
    creator_ = creator()
    post = create_post(patron_.user_id, creator_.campaigns[0].campaign_id)
    activity_ids = [None, 'notanid', post.activity_id]
    comment_texts = ['', '', None]
    user_id = patron_.user_id
    thread_id = None
    for post_id, comment_text in zip(activity_ids, comment_texts):
        resp, code = handle_process_comment(
            user_id, post_id, comment_text, thread_id
        )
        assert_equals(code, 400)


@patreon.test.manage()
@patch('patreon.services.spam.is_spam', return_value=0)
def test_okay(mock2):
    patron_ = patron()
    creator_ = creator()
    post = create_post(creator_.user_id, creator_.campaigns[0].campaign_id)

    # Patron comment without thread

    resp, code = handle_process_comment(
        patron_.user_id, post.activity_id, 'Cool stuff manfriend', None
    )
    assert_equals(code, 200)
    assert_true(jsdecode(resp))
    assert_true(jsdecode(resp)['CID'])
    comment_id = jsdecode(resp)['CID']

    # patron comment on thread

    resp, code = handle_process_comment(
        patron_.user_id, post.activity_id, 'Cooler stuff buckaroo',
        comment_id
    )
    assert_equals(code, 200)
    assert_true(jsdecode(resp))
    assert_true(jsdecode(resp)['CID'])

    # patron comment on thread 2

    patron_user2 = patron()
    resp, code = handle_process_comment(
        patron_user2.UID, post.activity_id, 'Lame stuff buckaroo',
        comment_id
    )
    assert_equals(code, 200)
    assert_true(jsdecode(resp))
    assert_true(jsdecode(resp)['CID'])

    # creator comment without thread

    resp, code = handle_process_comment(
        creator_.user_id, post.activity_id, 'Woah now', None
    )
    assert_equals(code, 200)
    assert_true(jsdecode(resp))
    assert_true(jsdecode(resp)['CID'])


@patreon.test.manage()
def test_spam():
    # this is an akismet test string
    spammer = patron(first_name="viagra-test-123", last_name=None)
    creator_ = creator()

    is_spam = check_spam(creator_, spammer, 'Cool steve likes this thing', 1)
    assert_equals(1, is_spam)


@patreon.test.manage()
@patch('patreon.app.web.pages.process_comment.handle_process_comment',
       return_value=(jsencode({}), 200))
def test_request(mock):
    patron_user = patron()
    session = create_web_session(patron_user)
    session_unauthed = create_unauthed_web_session()

    resp = get_and_follow(session, '/processComment')
    assert_equals(resp.status_code, 405)

    resp = session_unauthed.post('/processComment', {}, use_form=True)
    assert_equals(resp.status_code, 401)

    form = {'hid': '1', 'threadid': '2', 'inputComment': 'test'}
    resp = session.post('/processComment', form, use_form=True)
    assert_equals(resp.headers['Content-Type'], 'application/json')
    mock.assert_called_with(patron_user.UID, 1, 'test', 2)

    form = {'hid': 'a', 'threadid': 'b', 'inputComment': 'c'}
    resp = session.post('/processComment', form, use_form=True)
    assert_equals(resp.headers['Content-Type'], 'application/json')
    mock.assert_called_with(patron_user.UID, None, 'c', None)

    assert_equals(mock.call_count, 2)
