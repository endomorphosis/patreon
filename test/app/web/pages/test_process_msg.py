import patreon
from nose.tools import *
from patreon.model.manager import message_mgr
from test.fixtures.session_fixtures import create_web_session, create_unauthed_web_session
from test.fixtures.user_fixtures import patron


def _test_multiple_recipients(uids_key):
    sender = patron()
    recipient1 = patron()
    recipient2 = patron()
    message_content = 'HELLO!'

    assert_equals(0, message_mgr.get_unread(sender.user_id))
    assert_equals(0, message_mgr.get_unread(recipient1.user_id))
    assert_equals(0, message_mgr.get_unread(recipient2.user_id))

    session = create_web_session(sender)
    resp = session.post('/processMsg', {
        uids_key: [sender.user_id, recipient1.user_id, recipient2.user_id],
        'msg': message_content
    }, use_form=True)

    assert_equals(200, resp.status_code)

    # even though sender was also a recipient, we don't count self-sent messages
    assert_equals(0, message_mgr.get_unread(sender.user_id))
    assert_equals(1, message_mgr.get_unread(recipient1.user_id))
    assert_equals(1, message_mgr.get_unread(recipient2.user_id))

    messages = message_mgr.get_messages_and_clear_unread(recipient1.user_id)
    assert_equals(1, len(messages))
    assert_equals(sender.user_id, messages[0].sender_id)
    assert_equals(recipient1.user_id, messages[0].recipient_id)
    assert_equals(message_content, messages[0].content)


@patreon.test.manage()
def test_multiple_uids_recipients():
    _test_multiple_recipients('uids')


@patreon.test.manage()
def test_multiple_php_uids_recipients():
    _test_multiple_recipients('uids[]')


@patreon.test.manage()
def test_single_recipient():
    sender = patron()
    recipient = patron()
    message_content = 'HELLO!'

    assert_equals(0, message_mgr.get_unread(recipient.user_id))

    session = create_web_session(sender)
    resp = session.post('/processMsg', {
        'uid': recipient.user_id,
        'msg': message_content
    }, use_form=True)

    assert_equals(200, resp.status_code)
    assert_equals(1, message_mgr.get_unread(recipient.user_id))

    messages = message_mgr.get_messages_and_clear_unread(recipient.user_id)
    assert_equals(1, len(messages))
    assert_equals(sender.user_id, messages[0].sender_id)
    assert_equals(recipient.user_id, messages[0].recipient_id)
    assert_equals(message_content, messages[0].content)


@patreon.test.manage()
def test_missing_args():
    sender = patron()
    recipient = patron()
    message_content = 'HELLO!'

    session = create_web_session(sender)
    resp = session.post('/processMsg', {'msg': message_content}, use_form=True)
    assert_equals(400, resp.status_code)

    resp = session.post('/processMsg', {'uid': recipient.user_id}, use_form=True)
    assert_equals(400, resp.status_code)

    resp = session.post('/processMsg', {'uids': [recipient.user_id]}, use_form=True)
    assert_equals(400, resp.status_code)

    resp = session.post('/processMsg', {'uids[]': [recipient.user_id]}, use_form=True)
    assert_equals(400, resp.status_code)
