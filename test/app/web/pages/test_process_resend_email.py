from nose.tools import *
import patreon
from patreon.model.manager import user_mgr
from patreon.model.table import UnverifiedUser
from test.fixtures.session_fixtures import create_web_session, create_unauthed_web_session
from test.fixtures.user_fixtures import patron
from unittest.mock import patch


@patreon.test.manage()
def test_resend_unauthed():
    session = create_unauthed_web_session()
    resp = session.post('/processResendEmail', {}, use_form=True)
    assert_equals(403, resp.status_code)


@patreon.test.manage()
def test_resend():
    p = patron(email='test@example.com')
    session = create_web_session(p)

    previous_count = len(UnverifiedUser.query.filter_by(Email=p.email).all())
    resp = session.post('/processResendEmail', {}, use_form=True)
    assert_equals(200, resp.status_code)

    post_count = len(UnverifiedUser.query.filter_by(Email=p.email).all())
    assert_equals(post_count, 1 + previous_count)

    resp = session.post('/processResendEmail', {}, use_form=True)
    assert_equals(200, resp.status_code)

    post_count = len(UnverifiedUser.query.filter_by(Email=p.email).all())
    assert_equals(post_count, 2 + previous_count)
