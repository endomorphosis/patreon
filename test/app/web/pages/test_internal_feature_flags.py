from nose.plugins.attrib import attr
from nose.tools import *
import patreon
from patreon.model.manager import feature_flag
from patreon.util.unsorted import jsdecode
from test.fixtures.session_fixtures import create_web_session
from test.fixtures.user_fixtures import patron
from test.test_helpers.user_helpers import get_new_logged_in_admin_patron


@patreon.test.manage()
def test_permissions_failure():
    patron_user = patron()
    patron_session = create_web_session(patron_user)

    response = patron_session.get('/internal/feature_flags')
    assert_equals(response.status_code, 403)

    response = patron_session.get('/internal/user_feature_flags?identifier=1')
    assert_equals(response.status_code, 403)

    response = patron_session.post('/internal/user_feature_flags', {
        'user_id': 1,
        'feature_name': 'test_feature',
        'force_test_group': 'true'
    }, use_form=True)
    assert_equals(response.status_code, 403)


@patreon.test.manage()
def test_fetch_global_flags():
    admin_user, admin_session = get_new_logged_in_admin_patron()

    response = admin_session.get('/internal/feature_flags')
    assert_equals(response.status_code, 200)

@patreon.test.manage()
def test_change_global_flag_settings():
    admin_user, admin_session = get_new_logged_in_admin_patron()

    test_feature = 'test_feature'

    assert_equals(feature_flag.is_globally_disabled(test_feature), False)
    assert_equals(feature_flag.get_percentage_enabled(test_feature), 0.0)

    response = admin_session.post('/internal/feature_flags', {
        'feature_name': 'test_feature',
        'cutoff': '75',
        'is_disabled': 'true'
    }, use_form=True)
    assert_equals(response.status_code, 204)

    assert_equals(feature_flag.is_globally_disabled(test_feature), True)
    assert_equals(feature_flag.get_percentage_enabled(test_feature), 0.75)

    response = admin_session.post('/internal/feature_flags', {
        'feature_name': 'test_feature',
        'cutoff': '100',
        'is_disabled': 'false'
    }, use_form=True)
    assert_equals(response.status_code, 204)

    assert_equals(feature_flag.is_globally_disabled(test_feature), False)
    assert_equals(feature_flag.get_percentage_enabled(test_feature), 1.0)

@patreon.test.manage()
def test_user_flag_info_flow():
    admin_user, admin_session = get_new_logged_in_admin_patron()
    visible_flags = feature_flag.publicly_visible_feature_flags()

    response = admin_session.get('/internal/user_feature_flags?identifier=1')
    data = jsdecode(response.data.decode('utf-8'))
    assert_equals(data['user_id'], 1)
    assert_equals(len(data['flag_info']), len(visible_flags))
    assert_equals(data['flag_info'][0]['name'], visible_flags[0])
    assert_equals(
        data['flag_info'][0]['enabled'],
        feature_flag.is_enabled(visible_flags[0], user_id=1)
    )
    assert_equals(
        data['flag_info'][0]['forced'],
        feature_flag.is_user_enabled(visible_flags[0], 1)
    )

    # regardless of current state, force user into test group
    response = admin_session.post('/internal/user_feature_flags', {
        'feature_name': visible_flags[0],
        'user_id': '1',
        'force_test_group': 'true'
    }, use_form=True)
    assert_equals(response.status_code, 204)

    response = admin_session.get('/internal/user_feature_flags?identifier=1')
    data = jsdecode(response.data.decode('utf-8'))
    assert_equals(data['flag_info'][0]['name'], visible_flags[0])
    assert_equals(
        data['flag_info'][0]['enabled'],
        feature_flag.is_enabled(visible_flags[0], user_id=1)
    )
    assert_equals(
        data['flag_info'][0]['forced'],
        feature_flag.is_user_enabled(visible_flags[0], 1)
    )
    assert_equals(data['flag_info'][0]['forced'], True)

    # stop forcing user into test group
    response = admin_session.post('/internal/user_feature_flags', {
        'feature_name': visible_flags[0],
        'user_id': '1',
        'force_test_group': 'false'
    }, use_form=True)
    assert_equals(response.status_code, 204)

    response = admin_session.get('/internal/user_feature_flags?identifier=1')
    data = jsdecode(response.data.decode('utf-8'))
    assert_equals(data['flag_info'][0]['name'], visible_flags[0])
    assert_equals(
        data['flag_info'][0]['enabled'],
        feature_flag.is_enabled(visible_flags[0], user_id=1)
    )
    assert_equals(
        data['flag_info'][0]['forced'],
        feature_flag.is_user_enabled(visible_flags[0], 1)
    )
    assert_equals(data['flag_info'][0]['forced'], False)
