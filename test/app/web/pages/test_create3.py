from unittest.mock import patch
from werkzeug.datastructures import MultiDict

import patreon
from nose.tools import *
from patreon.app.web.pages.create3 import handle_create3_post
from patreon.model.manager import campaign_mgr, goal_mgr
from patreon.util.unsorted import jsencode
from test.fixtures.goal_fixtures import create_goal
from test.fixtures.session_fixtures import create_web_session, create_unauthed_web_session
from test.fixtures.user_fixtures import creator, patron
from test.test_helpers.campaign_helpers import assert_campaign_has_n_goals, assert_goal_is
from test.test_helpers.redirect_helpers import assert_redirects


@patreon.test.manage()
def test_delete_missing_goals():
    creator_id = creator().user_id
    campaign_id = campaign_mgr.try_get_campaign_id_by_user_id_hack(creator_id)

    assert_campaign_has_n_goals(campaign_id, 0)

    goal_ids = ['6']
    goal_mgr.delete_missing_goals(campaign_id, creator_id, goal_ids)
    assert_campaign_has_n_goals(campaign_id, 0)

    goal = create_goal(campaign_id)
    assert_not_equal(goal.goal_id, '6')
    goal_mgr.delete_missing_goals(campaign_id, creator_id, goal_ids)
    assert_campaign_has_n_goals(campaign_id, 0)

    goal_ids = [str(create_goal(campaign_id).goal_id)]
    goal_mgr.delete_missing_goals(campaign_id, creator_id, goal_ids)
    assert_campaign_has_n_goals(campaign_id, 1)


@patreon.test.manage()
def test_create_or_update_goal():
    creator_id = creator().user_id
    campaign_id = campaign_mgr.try_get_campaign_id_by_user_id_hack(creator_id)

    assert_campaign_has_n_goals(campaign_id, 0)
    result = goal_mgr.create_or_update_goal(
        campaign_id,  creator_id, '', '1', '2', 300
    )
    assert_true(result)
    assert_campaign_has_n_goals(campaign_id, 1)

    goal = goal_mgr.find_by_campaign_id(campaign_id)[0]
    assert_goal_is(goal, '1', '2', 300)

    result = goal_mgr.create_or_update_goal(
        campaign_id, creator_id, goal.goal_id, '3', '4', 400
    )
    assert_true(result)
    assert_campaign_has_n_goals(campaign_id, 1)

    goal = goal_mgr.find_by_campaign_id(campaign_id)[0]
    assert_goal_is(goal, '3', '4', 400)

    result = goal_mgr.create_or_update_goal(
        campaign_id, creator_id, 'abc', '3', '4', 400
    )
    assert_false(result)

    creator_user = creator()
    campaign = creator_user.campaigns[0]

    result = goal_mgr.create_or_update_goal(
        campaign, creator_id, goal.goal_id, '3', '4', 400
    )
    assert_false(result)


@patreon.test.manage()
def test_handle_errors():
    patron_id = patron().user_id
    creator_id = creator().user_id
    campaign_id = campaign_mgr.try_get_campaign_id_by_user_id_hack(creator_id)

    resp, code = handle_create3_post(patron_id, '', '', '', '')
    assert_equals(code, 400)

    with assert_redirects(to='/create3'):
        handle_create3_post(creator_id, ['a'], [], [], [])

    with assert_redirects(to='/create4'):
        handle_create3_post(creator_id, ['a'], ['b'], ['c'], ['d'])
    assert_campaign_has_n_goals(campaign_id, 0)

    with assert_redirects(to='/create3'):
        handle_create3_post(creator_id, ['a'], ['b'], ['c'], ['1'])


@patreon.test.manage()
def test_handle_okay():
    creator_id = creator().user_id
    campaign_id = campaign_mgr.try_get_campaign_id_by_user_id_hack(creator_id)

    with assert_redirects(to='/create4'):
        handle_create3_post(
            creator_id,
            [''],
            ['b'],
            ['c'],
            ['1']
        )
    assert_campaign_has_n_goals(campaign_id, 1)

    goal = goal_mgr.find_by_campaign_id(campaign_id)[0]

    with assert_redirects(to='/create4'):
        handle_create3_post(
            creator_id,
            ['', str(goal.goal_id)],
            ['b', 'e'],
            ['c', 'f'],
            ['1', '2']
        )
    assert_campaign_has_n_goals(campaign_id, 2)
    assert_equals(goal.goal_title, 'e')
    assert_equals(goal.goal_text, 'f')
    assert_equals(goal.amount_cents, 200)

    with assert_redirects(to='/create4'):
        handle_create3_post(
            creator_id,
            ['', '', ''],
            ['b', 'e', 'g'],
            ['c', 'f', 'h'],
            ['1', '1', '2']
        )
    assert_campaign_has_n_goals(campaign_id, 3)


@patreon.test.manage()
@patch('patreon.app.web.pages.create3.handle_create3_post',
       return_value=(jsencode({}), 200))
def test_request(mock):
    patron_user = patron()
    session = create_web_session(patron_user)
    session_unauthed = create_unauthed_web_session()

    resp = session_unauthed.post('/create3', {}, use_form=True)
    assert_equals(resp.status_code, 401)

    data = MultiDict({
        'gid[]': 1,
        'rTitle[]': 4,
        'rDescription[]': 7,
        'rAmount[]': 10
    })
    data.update({
        'gid[]': 2,
        'rTitle[]': 5,
        'rDescription[]': 8,
        'rAmount[]': 11
    })
    data.update({
        'gid[]': 3,
        'rTitle[]': 6,
        'rDescription[]': 9,
        'rAmount[]': 12
    })

    resp = session.post('/create3', data, use_form=True)
    assert_equals(200, resp.status_code)
    mock.assert_called_once_with(
        patron_user.user_id,
        ['1', '2', '3'],
        ['4', '5', '6'],
        ['7', '8', '9'],
        ['10', '11', '12']
    )
