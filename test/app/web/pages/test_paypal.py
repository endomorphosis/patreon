from nose.plugins.attrib import attr
from nose.tools import *
import patreon
from patreon.constants.cards import PAYPAL
from patreon.exception.payment_errors import PaypalException, TemporaryOutage
from patreon.model.manager import card_mgr
from patreon.model.table import PaypalToken
from patreon.services.payment_apis import PayPalAPI
from test.fixtures.session_fixtures import create_unauthed_web_session, create_web_session
from test.fixtures.user_fixtures import patron, creator
from unittest.mock import patch


@patreon.test.manage()
@attr(speed="slower")
def test_paypal_start():
    unauthed_session = create_unauthed_web_session()

    result = unauthed_session.get('/paypalStart')

    assert_equals(302, result.status_code)
    assert_in('/login', result.headers['Location'])
    assert_not_in('paypal.com', result.headers['Location'])

    patron_user = patron()

    patron_session = create_web_session(patron_user)
    amount = 1
    creator_user = creator()
    creator_id = creator_user.user_id

    response = patron_session.get('/paypalStart')

    assert_equals(response.status_code, 405)

    try:
        response = patron_session.post('/paypalStart', use_form=True, use_csrf=True, data={
            'cid': creator_id,
            'amt': amount,
            'cat': 'bat'
        })

        assert_equals(response.status_code, 302)

        assert_in(patreon.config.paypal['url'], response.headers['Location'])
        assert_in('webscr&cmd=_express-checkout&token=', response.headers['Location'])

        paypal_state = PaypalToken.query.filter_by(user_id=patron_user.user_id).first()
        assert_not_equals(None, paypal_state)

        assert_in(paypal_state.paypal_token, response.headers['Location'])

        assert_equals(paypal_state.post_info_json['cat'], 'bat')

        response = patron_session.post('/paypalStart', use_form=True, use_csrf=True, data={
            'rat': 'sat'
        })

        assert_equals(response.status_code, 302)

        assert_in(patreon.config.paypal['url'], response.headers['Location'])
        assert_in('webscr&cmd=_express-checkout&token=', response.headers['Location'])

        paypal_state = PaypalToken.query.filter_by(user_id=patron_user.user_id).first()
        assert_not_equals(None, paypal_state)

        assert_in(paypal_state.paypal_token, response.headers['Location'])

        assert_equals(paypal_state.post_info_json['rat'], 'sat')
    except PaypalException as e:
        assert_equals(TemporaryOutage, e.original_exception.__class__)

def assert_paypal_failed(response):
    assert_equals(response.status_code, 302)
    assert_in('paypal_failed=1', response.headers['Location'])

@patreon.test.manage()
def test_paypal_return_failures():
    unauthed_session = create_unauthed_web_session()

    result = unauthed_session.get('/paypalReturn')

    assert_equals(302, result.status_code)
    assert_in('/login', result.headers['Location'])
    assert_not_in('paypal.com', result.headers['Location'])

    patron_user = patron()

    patron_session = create_web_session(patron_user)

    assert_paypal_failed(patron_session.get('/paypalReturn'))
    assert_paypal_failed(patron_session.get('/paypalReturn?suc=1'))
    assert_paypal_failed(patron_session.get('/paypalReturn?suc=1&token=EC-blah'))

@patreon.test.manage()
@patch.object(PayPalAPI, 'add_instrument')
def test_paypal_return_success(mock):
    patron_user = patron()
    fake_express_checkout_token = 'EC-blah'
    fake_card_id = 'B-blah'

    mock.return_value = (fake_card_id, None, PAYPAL, None, None)

    form_data = {
            'sStreet': 1,
            'sStreet2': 2,
            'sCity': 3,
            'sState': 4,
            'sZip': 5,
            'sCountry': 6,
            'rid': 7,
            'amt': 8,
            'cid': 9,
            'pledge_cap': 10
        }

    patron_session = create_web_session(patron_user)
    card_mgr.save_paypal_state(patron_user.user_id, form_data, fake_express_checkout_token)
    response = patron_session.get('/paypalReturn?suc=1&token={}'.format(fake_express_checkout_token))
    mock.assert_called_once_with(fake_express_checkout_token)
    assert_equals(response.status_code, 302)

    get_params = {
        'street1': form_data.get('sStreet'),
        'street2': form_data.get('sStreet2'),
        'city': form_data.get('sCity'),
        'state': form_data.get('sState'),
        'zip': form_data.get('sZip'),
        'country': form_data.get('sCountry'),
        'rewardId': form_data.get('rid'),
        'amount': form_data.get('amt'),
        'u': form_data.get('cid'),
        'pledgeCap': form_data.get('pledge_cap'),
        'cardId': fake_card_id
    }

    for key in get_params.keys():
        value = get_params[key]
        assert_in(key + '=' + str(value), response.headers['Location'])

    card = card_mgr.get(fake_card_id)

    assert_not_equals(None, card)
    assert_equals(card.UID, patron_user.user_id)



