from unittest.mock import patch
import patreon
from patreon.app.web.pages.process_follow import handle_process_follow
from patreon.model.manager import follow_mgr
from patreon.util.unsorted import jsdecode, jsencode
from test.fixtures.session_fixtures import create_web_session, create_unauthed_web_session, get_and_follow
from test.fixtures.user_fixtures import patron
from nose.tools import *


@patreon.test.manage()
def test_errors():
    user_id = patron().UID
    follower_ids = [None, 'notanid']
    get_follow = 1
    for follower_id in follower_ids:
        resp, code = handle_process_follow(user_id, follower_id, get_follow)
        assert_equals(code, 400)


@patreon.test.manage()
def test_okay():
    user_id = patron().UID
    followed_id = patron().UID

    resp, code = handle_process_follow(user_id, followed_id, 1)
    assert_equals(code, 200)
    assert_true(jsdecode(resp)['is_following'])
    assert_true(follow_mgr.get(follower_id=user_id, followed_id=followed_id) is not None)

    resp, code = handle_process_follow(user_id, followed_id, 1)
    assert_equals(code, 200)
    assert_true(jsdecode(resp)['is_following'])
    assert_true(follow_mgr.get(follower_id=user_id, followed_id=followed_id) is not None)

    resp, code = handle_process_follow(user_id, followed_id, 0)
    assert_equals(code, 200)
    assert_true(not jsdecode(resp)['is_following'])
    assert_true(follow_mgr.get(follower_id=user_id, followed_id=followed_id) is None)

    resp, code = handle_process_follow(user_id, followed_id, 0)
    assert_equals(code, 200)
    assert_true(not jsdecode(resp)['is_following'])
    assert_true(follow_mgr.get(follower_id=user_id, followed_id=followed_id) is None)


@patreon.test.manage()
@patch('patreon.app.web.pages.process_follow.handle_process_follow', return_value=(jsencode({}), 200))
def test_request(mock):
    patron_user = patron()
    session = create_web_session(patron_user)
    session_unauthed = create_unauthed_web_session()

    resp = get_and_follow(session, '/processFollow')
    assert_equals(resp.status_code, 405)

    resp = session_unauthed.post('/processFollow', {}, use_form=True)
    assert_equals(resp.status_code, 401)

    resp = session.post('/processFollow?uid=1&follow=2', {'uid': '1', 'follow': '2'}, use_form=True)
    assert_equals(resp.headers['Content-Type'], 'application/json')
    mock.assert_called_with(patron_user.UID, 1, 2)

    resp = session.post('/processFollow?uid=a&follow=b', {'uid': 'a', 'follow': 'b'}, use_form=True)
    assert_equals(resp.headers['Content-Type'], 'application/json')
    mock.assert_called_with(patron_user.UID, None, None)

    assert_equals(mock.call_count, 2)
