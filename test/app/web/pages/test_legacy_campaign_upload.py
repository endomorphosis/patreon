from unittest.mock import patch

import patreon
from patreon.util.unsorted import jsdecode
from test import fixtures
from test.fixtures.session_fixtures import create_web_session
from test.fixtures.user_fixtures import creator
from nose.tools import *



@patreon.test.manage()
@patch('patreon.model.manager.campaign_mgr.upload_campaign_image', return_value=('large', 'small'))
def test_campaign_image(mock):
    creator_user = creator()
    session = create_web_session(creator_user)

    file = open(fixtures.file_directory + 'blank.jpg', 'rb')
    data = file.read()
    resp = session.post('/legacyCampaignUpload', data, use_form=True, use_csrf=False)
    resp = jsdecode(resp.data.decode('utf-8'))
    assert_equals(resp['success'], True)
    assert_equals(resp['image_url'], 'large')
    assert_equals(resp['image_small_url'], 'small')

