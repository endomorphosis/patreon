from unittest.mock import patch

import patreon
from patreon.util.unsorted import jsdecode
from test import fixtures
from test.fixtures.session_fixtures import create_web_session
from test.fixtures.user_fixtures import creator
from nose.tools import *



@patreon.test.manage()
@patch('patreon.model.manager.profile_mgr.upload_profile_image', return_value='large_image')
def test_campaign_image(mock):
    creator_user = creator()
    session = create_web_session(creator_user)

    with open(fixtures.file_directory + 'blank.jpg', 'rb') as file:
        data = file.read()

    resp = session.post('/legacyUserUpload', data, use_form=True, use_csrf=False)
    resp = jsdecode(resp.data.decode('utf-8'))

    assert_equals(resp['success'], True)
    assert_equals(resp['photo_key'], 'large_image')
