from unittest.mock import patch
from nose.plugins.attrib import attr
import patreon
from patreon.model.manager import user_mgr, settings_mgr, change_email_mgr, \
    auth_mgr, user_location_mgr
from test.fixtures.session_fixtures import create_web_session, get_and_follow, \
    create_unauthed_web_session
from test.fixtures.user_fixtures import patron
from nose.tools import *


def assert_location_count(user_id, count):
    assert_equals(user_location_mgr.find_by_user_id(user_id).count(), count)


@patreon.test.manage()
@attr(speed="slower")
def test_sanitize_and_update_user_params():
    patron_user = patron()
    patron(vanity='cats')

    user_mgr.sanitize_and_update_user_params(patron_user, vanity='test_user')
    user = user_mgr.get(patron_user.user_id)
    assert_equals('test_user', user.vanity)

    user_mgr.sanitize_and_update_user_params(patron_user, vanity='Username (Optional)')
    user = user_mgr.get(patron_user.user_id)
    assert_equals('test_user', user.vanity)

    user_mgr.sanitize_and_update_user_params(patron_user, facebook='1', twitter='@2', youtube='3')
    user = user_mgr.get(patron_user.user_id)
    assert_equals('http://1', user.facebook)
    assert_equals('2', user.twitter)
    assert_equals('http://3', user.youtube)

    user_mgr.sanitize_and_update_user_params(patron_user, youtube='4')
    user = user_mgr.get(patron_user.user_id)
    user_extra = settings_mgr.get_user_extra(patron_user.user_id)
    assert_equals(user.youtube, 'http://4')
    assert_equals(0, user_extra.FlagYoutube)

    assert_equals(0, len(change_email_mgr.find_by_user_id(patron_user.user_id)))
    ret_val = user_mgr.sanitize_and_update_user_params(patron_user, email='newemail@newuser.new')
    assert_in('emc=1', ret_val)
    assert_equals(1, len(change_email_mgr.find_by_user_id(patron_user.user_id)))

    ret_val = user_mgr.sanitize_and_update_user_params(patron_user, new_password='test1234')
    assert_true(auth_mgr.check_password(patron_user.user_id, 'password'))
    assert_in('passError=2', ret_val)

    ret_val = user_mgr.sanitize_and_update_user_params(
        patron_user, new_password='test1234', old_password='password1'
    )
    assert_true(auth_mgr.check_password(patron_user.user_id, 'password'))
    assert_in('passError=1', ret_val)

    ret_val = user_mgr.sanitize_and_update_user_params(
        patron_user, new_password='test1234', old_password='password'
    )
    assert_true(auth_mgr.check_password(patron_user.user_id, 'password'))
    assert_in('passError=1', ret_val)

    ret_val = user_mgr.sanitize_and_update_user_params(
        patron_user, new_password='test1234', old_password='password',
        new_password_confirmation='test12345'
    )
    assert_true(auth_mgr.check_password(patron_user.user_id, 'password'))
    assert_in('passError=1', ret_val)

    ret_val = user_mgr.sanitize_and_update_user_params(
        patron_user, new_password='test1234', old_password='password',
        new_password_confirmation='test1234'
    )
    assert_true(auth_mgr.check_password(patron_user.user_id, 'test1234'))
    assert_not_in('passError', ret_val)

    user_mgr.sanitize_and_update_user_params(patron_user, vanity='cats')
    user = user_mgr.get(patron_user.user_id)
    assert_true(user.vanity != 'cats')

    user_mgr.sanitize_and_update_user_params(patron_user)
    assert_location_count(patron_user.user_id, 0)

    user_location_mgr.add_user_location(patron_user.user_id, 1, 1, 1)
    assert_location_count(patron_user.user_id, 1)

    user_location_mgr.add_user_location(patron_user.user_id, 1, 1, 1)
    user_mgr.sanitize_and_update_user_params(patron_user, latitudes=[])
    assert_location_count(patron_user.user_id, 0)

    user_location_mgr.add_user_location(patron_user.user_id, 1, 1, 1)
    user_mgr.sanitize_and_update_user_params(patron_user, longitudes=[])
    assert_location_count(patron_user.user_id, 0)

    user_location_mgr.add_user_location(patron_user.user_id, 1, 1, 1)
    user_mgr.sanitize_and_update_user_params(patron_user, location_names=[])
    assert_location_count(patron_user.user_id, 0)

    user_location_mgr.add_user_location(patron_user.user_id, 1, 1, 1)
    user_mgr.sanitize_and_update_user_params(
        patron_user, latitudes=[1], longitudes=[1], location_names=[]
    )
    assert_location_count(patron_user.user_id, 1)

    user_mgr.sanitize_and_update_user_params(patron_user)
    assert_location_count(patron_user.user_id, 0)

    user_mgr.sanitize_and_update_user_params(
        patron_user, latitudes=[2, 1], longitudes=[3, 3],
        location_names=['test1', 'test2']
    )
    assert_location_count(patron_user.user_id, 2)

    user_mgr.sanitize_and_update_user_params(
        patron_user, latitudes=[1], longitudes=[1], location_names=['test']
    )
    assert_location_count(patron_user.user_id, 1)

    user_mgr.sanitize_and_update_user_params(patron_user)
    assert_location_count(patron_user.user_id, 0)


@patreon.test.manage()
@patch('patreon.model.manager.user_mgr.sanitize_and_update_user_params', return_value='')
def test_post_sect_one(mock):
    url = '/processSettings?sect=1'
    patron_user = patron()
    session = create_web_session(patron_user)
    session_unauthed = create_unauthed_web_session()

    resp = get_and_follow(session, url)
    assert_equals(resp.status_code, 405)

    resp = session_unauthed.post(url, {}, use_form=True)
    assert_equals(resp.status_code, 401)

    resp = session.post(url, {
        'uname': 1,
        'name': '2 3',
        'settingsAbout': 4,
        'settingsFacebook': 5,
        'settingsTwitter': 6,
        'settingsYoutube': 7,
        'passwordNR': 8,
        'confirmPasswordNR': 9,
        'oldPassword': 10,
        'email': 11
    }, use_form=True)
    assert_equals(302, resp.status_code)

    mock.assert_called_once_with(
        patron_user, vanity='1', fname='2', lname='3', about='4', facebook='5',
        twitter='6', youtube='7', new_password='8', old_password='10',
        new_password_confirmation='9', email= '11', latitudes=[],
        longitudes=[], location_names=[]
    )


@patreon.test.manage()
def test_post_bad_sect():
    patron_user = patron()
    session = create_web_session(patron_user)
    resp = session.post('/processSettings?sect=1000', {}, use_form=True)
    assert_equals(302, resp.status_code)


@patreon.test.manage()
@patch('patreon.model.manager.settings_mgr.update_settings', return_value='')
def test_post_sect_three(mock):
    url = '/processSettings?sect=3'
    patron_user = patron()
    session = create_web_session(patron_user)

    resp = session.post(url, {
        'patronUpdate': 1,
        'postUpdate': 2,
        'commentUpdate': 3,
        'emailUpdate': 4,
        'privacyUpdate': 5,
        'milesUpdate': 6,
    }, use_form=True)
    assert_equals(302, resp.status_code)
    mock.assert_called_once_with(
        user_id=patron_user.user_id, actor_id=patron_user.user_id, privacy='5',
        email_new_patron='1', email_new_comment='3', email_patron_post='2',
        email_update='4', email_goal='6', email_creator_ids=[],
        email_creator_paid=[], email_creator_post=[]
    )

@patreon.test.manage()
@patch('patreon.model.manager.profile_mgr._convert_profile_image_to_thumb',
       return_value='thumbnail_key')
def test_update_image(mock):
    patron_user = patron()
    session = create_web_session(patron_user)

    old_image = patron_user.image_url
    old_thumb = patron_user.thumb_url

    resp = session.post('/processSettings?sect=1', {
        'profileImgUrl': 'doesntmatter'
    }, use_form=True)

    patron_user = user_mgr.get(patron_user.user_id)
    assert_equals(patron_user.image_url, old_image)
    assert_equals(patron_user.thumb_url, old_thumb)

    resp = session.post('/processSettings?sect=1', {
        'photo_key': 'photo_key',
        'uploadThumb': 1
    }, use_form=True)

    patron_user = user_mgr.get(patron_user.user_id)
    assert_in("photo_key", patron_user.image_url)
    assert_in("thumbnail_key", patron_user.thumb_url)
    assert_in("thumbnail_key", patron_user.thumbnail_url)
