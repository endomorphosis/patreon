from nose.tools import *
from flask import session
import patreon
from patreon.app.web.pages.internal_impersonate import handle_impersonate
from patreon.exception.auth_errors import UnauthorizedAdminFunction
from test.fixtures.session_fixtures import create_web_session
from test.fixtures.user_fixtures import patron
from test.test_helpers.user_helpers import get_new_logged_in_admin_patron


@patreon.test.manage()
def test_get_impersonate():
    patron_user = patron()
    admin_user, admin_session = get_new_logged_in_admin_patron()

    patron_user_id = patron_user.user_id
    admin_user_id = admin_user.user_id

    admin_session.get('/internal/impersonate?user_id=' + str(patron_user_id))

    with admin_session:
        assert_equals(session.user_id, patron_user_id)
        handle_impersonate(admin_user_id)
        assert_equals(session.user_id, admin_user_id)
        assert_equals(session.original_user_id, admin_user_id)

    admin_session.get('/internal/impersonate')

    with admin_session:
        assert_equals(session.user_id, admin_user_id)

    admin_session.post('/internal/impersonate', {
        'identifier': patron_user.email
    }, use_form=True)

    with admin_session:
        assert_equals(session.user_id, patron_user_id)


@patreon.test.manage()
@raises(UnauthorizedAdminFunction)
def test_impersonate_fail():
    patron_user = patron()
    admin_user, admin_session = get_new_logged_in_admin_patron()

    patron_user_id = patron_user.user_id
    admin_user_id = admin_user.user_id

    patron_session = create_web_session(patron_user)
    response = patron_session.get('/internal/impersonate?user_id=' + str(admin_user_id))

    assert_equals(response.status_code, 403)

    with patron_session:
        assert_equals(session.user_id, patron_user_id)
        handle_impersonate(admin_user_id)

    response = patron_session.get('/internal/impersonate')

    assert_equals(response.status_code, 403)

    with patron_session:
        assert_equals(session.user_id, patron_user_id)

    response = patron_session.post('/internal/impersonate', {
        'identifier': admin_user.email
    }, use_form=True)

    assert_equals(response.status_code, 403)

    with admin_session:
        assert_equals(session.user_id, patron_user_id)
