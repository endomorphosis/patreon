from nose.tools import *
import patreon
from test.fixtures.session_fixtures import create_web_session
from test.fixtures.user_fixtures import creator


@patreon.test.manage()
def test_post_patron_done():
    creator_user = creator()
    creator_session = create_web_session(creator_user)

    response = creator_session.post('/bePatronDone', {
        'shareEmbed': 'embed123',
        'thanksMsg': 'thanks123'
    }, use_form=True)

    assert_equals(response.status_code, 200)

    data = response.data.decode('utf-8')
    assert_true('embed123' in data)
    assert_true('thanks123' in data)

