from nose.tools import *
import patreon
from patreon.app.web.pages.process_check_vanity import handle_process_check_vanity
from patreon.util.unsorted import jsdecode
from test.fixtures.user_fixtures import patron


@patreon.test.manage()
def test_handle_check_vanity():
    patron_user = patron(vanity='test_vanity')

    resp = jsdecode(handle_process_check_vanity('blah'))
    assert_equals('blah', resp)

    resp = jsdecode(handle_process_check_vanity('test_vanity'))
    assert_equals('0', resp)
