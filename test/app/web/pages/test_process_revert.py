from nose.tools import *
import patreon
from patreon.model.manager import campaign_mgr
from patreon.model.table import Campaign
from test.fixtures.session_fixtures import create_web_session, create_unauthed_web_session
from test.test_helpers.follow_helpers import assert_is_not_following, assert_is_following
from test.fixtures.follows_fixtures import follow_user
from test.fixtures.user_fixtures import creator, patron


@patreon.test.manage()
def test_revert_unauthed():
    session = create_unauthed_web_session()
    resp = session.post('/processRevert', {}, use_form=True)
    assert_equals(401, resp.status_code)


@patreon.test.manage()
def test_revert_campaign():
    user = creator()
    user_id = user.user_id
    follower_id = patron().user_id
    second_follower_id = patron().user_id

    follow_user(follower_id, user_id)
    follow_user(second_follower_id, user_id)
    assert_is_following(follower_id, user_id)
    assert_is_following(second_follower_id, user_id)
    
    campaign = user.campaigns[0]
    assert_is_not_none(campaign.published_at)
    assert_is_not_none(campaign.creation_name)

    session = create_web_session(user)
    resp = session.post('/processRevert', {}, use_form=True)
    assert_equals(200, resp.status_code)

    # repeated reverts do nothing, but don't fail
    resp = session.post('/processRevert', {}, use_form=True)
    assert_equals(200, resp.status_code)

    # remove follows when creator reverts to regular user
    assert_is_not_following(follower_id, user_id)
    assert_is_not_following(second_follower_id, user_id)

    reverted_campaign = campaign_mgr.get(campaign.campaign_id)
    assert_is_none(reverted_campaign.published_at)
    assert_is_none(reverted_campaign.creation_name)
