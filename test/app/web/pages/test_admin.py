from nose.plugins.attrib import attr
from nose.tools import assert_true
import patreon
from test.fixtures.session_fixtures import create_web_session
from test.fixtures.user_fixtures import patron
from test.test_helpers.user_helpers import get_new_logged_in_admin_patron


@patreon.test.manage()
@attr(speed="slower")
def test_admin_alert():
    patron_user = patron()
    patron_session = create_web_session(patron_user)
    admin_user, admin_session = get_new_logged_in_admin_patron()

    admin_response = admin_session.get('/')
    assert_true('Impersonate via vanity, email, or user id' in admin_response.data.decode('utf-8'))

    patron_response = patron_session.get('/')
    assert_true('Impersonate via vanity, email, or user id' not in patron_response.data.decode('utf-8'))

    admin_response = admin_session.get('/user?u=' + str(patron_user.user_id))
    assert_true('Become this user' in admin_response.data.decode('utf-8'))

    patron_response = patron_session.get('/user?u=' + str(admin_user.user_id))
    assert_true('Become this user' not in patron_response.data.decode('utf-8'))
    assert_true('View this creator\'s complete pledge history' not in patron_response.data.decode('utf-8'))
    assert_true('View logged in user\'s pledge history with this creator' not in patron_response.data.decode('utf-8'))
