from nose.tools import *
import patreon
from patreon.model.dbm import user_location_dbm
from patreon.model.manager import user_location_mgr
from test.fixtures.session_fixtures import create_web_session, create_unauthed_web_session
from test.fixtures.user_fixtures import patron


def _all_user_locations(user):
    return user_location_mgr.find_by_user_id(user.user_id).all()


@patreon.test.manage()
def test_delete_nothing():
    patron_user = patron()
    session = create_web_session(patron_user)
    resp = session.post('/processDeleteLocation?lat=10.0&lon=12.0', {}, use_form=True)
    # even if there is no location to delete, it returns a 200
    assert_equals(resp.status_code, 200)


@patreon.test.manage()
def test_delete_real_location():
    patron_user = patron()
    session = create_web_session(patron_user)
    lat = 37.7
    lon = -123.1
    assert_equals(0, len(_all_user_locations(patron_user)))
    user_location_mgr.insert(patron_user.user_id, lat, lon, 'Farallon Islands')
    assert_equals(1, len(_all_user_locations(patron_user)))
    resp = session.post(
        '/processDeleteLocation?lat={lat}&lon={lon}'.format(lat=lat, lon=lon),
        {},
        use_form=True
    )
    assert_equals(resp.status_code, 200)
    assert_equals(0, len(_all_user_locations(patron_user)))


@patreon.test.manage()
def test_failed_delete():
    session = create_unauthed_web_session()
    resp = session.post('/processDeleteLocation?lat=10.0&lon=12.0', {}, use_form=True)
    assert_equals(resp.status_code, 401)
