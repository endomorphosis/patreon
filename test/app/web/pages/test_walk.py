"""
Extremely basic test to ensure that web_app loads.
"""
import patreon
from patreon.model.manager import feature_flag
from patreon.model.table import TblUsersSetting
from patreon.services import timer
from patreon.services.db import db_session
from patreon.test import MockSession
from patreon.app.web import web_app

from nose.tools import *
from test.fixtures.activity_fixtures import create_post
from test.fixtures.comment_fixtures import insert_comment
from test.fixtures.fixtures_rewards_give import create_rewards_give
from test.fixtures.pledge_fixtures import create_pledge
from test.fixtures.reward_fixtures import create_reward
from test.fixtures.session_fixtures import create_web_session, get_and_follow
from test.fixtures.user_fixtures import patron, creator
from test.fixtures.address_fixtures import address, invalid_address

PORTED_PAGES = [
    '/'
    '/about',
    '/discover',
    '/featured',
    '/login',
    '/login?ru=/',
    '/signup?ru=/',
    '/press',
    '/signup',
    '/legal',
    '/CDSCreators',
    '/contact',
    '/channel',
    '/forgetPass',
    '/forgetPassReset',
    '/importYtPop',
    '/search',
    '/start_i',
    '/REST/auth/CSRFTicket',
    '/processCheckVanity',
    '/processCheckVanity?vanity=a',
    '/processCheckVanity?vanity=pc',
    '/careers',
    '/contentRemoved',
    '/create_start',
    '/create',
    '/create2',
    '/create3',
    '/create4',
    '/jobs',
    '/manageRewards',
    '/manageRewardsList',
    '/pending',
    '/settings',
    '/userMsg',
    '/userMsgAll'
]

USER_PAGES = [
    'user', 'NonPatronNonCreator', 'PatronNonCreator', 'NonPatronCreator',
    'PatronCreator', 'PatronizedCreator', 'PatronNonCreator2',
    'PrivateNonCreator', 'PrivateCreator', 'SuspendedUser'
]

VANITY_ENDPOINTS = [
    '/{}',
    '/{}?ty=p',
    '/{}?ty=a',
    '/{}?ty=c',
    '/{}?pat=1',
    '/{}?pat=1&ty=c',
    '/{}?pat=1&ty=a',
    '/{}?pat=1&ty=l',
    '/{}?rf=1',
    '/user?u=a',
    '/user?v={}',
    '/user?u=2134'
]

USER_ID_ENDPOINTS = [
    '/userNext?u={}&ty=c',
    '/userNext?u={}&ty=a',
    '/userNext?u={}&ty=p',
    '/patronNext?u={}&ty=c',
    '/patronNext?u={}&ty=a',
    '/patronNext?u={}&ty=l',
    '/bePatron?u={}',
    '/bePatronDone?u={}',
    '/bePatronConfirm?u={}'
]

ACTIVITIES = []


@patreon.test.manage()
def test_walk():
    # Create sessions for all logged in users.
    sessions = []

    MOCK_USERS = {
        # Non-Patron, Non-Creator
        'NonPatronNonCreator': patron(vanity='NonPatronNonCreator'),
        # Patron, Non-Creator
        'PatronNonCreator': patron(vanity='PatronNonCreator'),
        # Non-Patron, Creator
        'NonPatronCreator': creator(vanity='NonPatronCreator'),
        # Patron, Creator
        'PatronCreator': creator(vanity='PatronCreator'),
        # Patronized Creator
        'PatronizedCreator': creator(vanity='PatronizedCreator'),
        # Patron, Non Creator 2
        'PatronNonCreator2': patron(vanity='PatronNonCreator2')
    }

    FLAG_SETUPS = [
        disable_all_flags,
        enable_all_flags
    ]

    build_site_fixtures(MOCK_USERS)

    for key, user in MOCK_USERS.items():
        session = create_web_session(user)
        sessions.append(session)

    # Include one logged out user.
    sessions.append(MockSession(web_app))

    for page in USER_PAGES:
        for endpoint in VANITY_ENDPOINTS:
            PORTED_PAGES.append(endpoint.format(page))
        for endpoint in USER_ID_ENDPOINTS:
            if page in ['PrivateNonCreator', 'PrivateCreator', 'SuspendedUser', 'user']:
                continue
            user_id = MOCK_USERS[page].user_id
            PORTED_PAGES.append(endpoint.format(user_id))

    for activity in ACTIVITIES:
        PORTED_PAGES.append('/posts/{}'.format(activity.activity_id))

    PORTED_PAGES.append('/logout')

    timer.refresh()
    for flag_setup in FLAG_SETUPS:
        flag_setup()
        for path in PORTED_PAGES:
            for session in sessions:
                resp = get_and_follow(session, path)
                assert_equal(resp.status_code, 200)


def build_site_fixtures(users):
    create_pledge(
        users['PatronCreator'].user_id,
        users['PatronizedCreator'].user_id,
        address=address()
    )
    create_pledge(
        users['PatronNonCreator'].user_id,
        users['PatronizedCreator'].user_id,
        address=invalid_address()
    )
    create_pledge(
        users['PatronNonCreator2'].user_id,
        users['PatronizedCreator'].user_id
    )

    post_id = create_post(
        users['PatronizedCreator'].user_id,
        users['PatronizedCreator'].campaigns[0].campaign_id,
        is_paid=1,
        min_cents_pledged_to_view=1
    ).activity_id

    activity1 = create_post(
        users['PatronCreator'].user_id,
        users['PatronCreator'].campaigns[0].campaign_id
    )
    activity2 = create_post(
        users['NonPatronCreator'].user_id,
        users['NonPatronCreator'].campaigns[0].campaign_id
    )
    activity3 = create_post(
        users['PatronNonCreator'].user_id,
        users['PatronizedCreator'].campaigns[0].campaign_id,
        is_creation=0
    )
    activity4 = create_post(
        users['PatronCreator'].user_id,
        users['PatronizedCreator'].campaigns[0].campaign_id,
        is_creation=0,
        min_cents_pledged_to_view=1
    )

    reward = create_reward(users['PatronizedCreator'].user_id)
    create_rewards_give(
        users['PatronCreator'].user_id,
        post_id,
        reward.reward_id,
        users['PatronizedCreator'].user_id
    )

    parent_id = insert_comment(users['PatronNonCreator'].user_id, post_id).comment_id
    parent_id2 = insert_comment(users['NonPatronCreator'].user_id, post_id, thread_id=parent_id).comment_id
    insert_comment(users['PatronCreator'].user_id,          post_id)
    insert_comment(users['PatronizedCreator'].user_id,      post_id)
    insert_comment(users['NonPatronCreator'].user_id,       post_id)
    insert_comment(users['NonPatronNonCreator'].user_id,    post_id)
    insert_comment(users['PatronCreator'].user_id,          post_id, thread_id=parent_id)
    insert_comment(users['PatronizedCreator'].user_id,      post_id, thread_id=parent_id)
    insert_comment(users['NonPatronNonCreator'].user_id,    post_id, thread_id=parent_id2)

    users['PatronCreator'].campaigns[0].update({'image_url': 'blah'})

    # Private Non Creator
    prnc = patron(vanity='PrivateNonCreator')
    # Private Creator
    prc = creator(vanity='PrivateCreator')
    # Suspended User
    creator(vanity='SuspendedUser', is_suspended=1)

    TblUsersSetting.get(user_id=prnc.user_id).update({'Privacy': 1})
    TblUsersSetting.get(user_id=prc.user_id).update({'Privacy': 1})

    ACTIVITIES.append(activity1)
    ACTIVITIES.append(activity2)
    ACTIVITIES.append(activity3)
    ACTIVITIES.append(activity4)

    db_session.close()


def disable_all_flags():
    all_flags = feature_flag.publicly_visible_feature_flags()
    for flag in all_flags:
        feature_flag.set_global_disabled_state(flag, True)


def enable_all_flags():
    all_flags = feature_flag.publicly_visible_feature_flags()
    for flag in all_flags:
        feature_flag.set_global_disabled_state(flag, False)
        feature_flag.set_percentage_enabled(flag, 1.0)
