from nose.tools import *
import itertools


def get_relationship(resource_json, relationship_name):
    if "relationships" not in resource_json:
        return None
    relationships = resource_json["relationships"]
    if relationship_name not in relationships:
        return None
    return relationships[relationship_name]


def assert_relationship(resource_json, relationship_name, type_name, id=None):
    relationship = get_relationship(resource_json, relationship_name)
    assert_is_instance(relationship, dict)
    if type_name is None:
        assert_is_none(relationship["data"])
    elif type_name == list:
        assert_is_instance(relationship["data"], list)
    else:
        assert_is_instance(relationship["data"], dict)
        assert_equals(type_name, relationship["data"]["type"])
        if id is not None:
            assert_equals(id, relationship["data"]["id"])
    return relationship


def all_resources_in_document(document_json):
    resources = []

    if isinstance(document_json["data"], list):
        resources = itertools.chain(resources, document_json["data"])
    else:
        resources = itertools.chain(resources, [document_json["data"]])

    if "included" in document_json:
        resources = itertools.chain(resources, document_json["included"])

    return resources


def get_resource_by_type_and_id(document_json, resource_type, resource_id):
    for resource in all_resources_in_document(document_json):
        if resource["type"] == resource_type and resource["id"] == resource_id:
            return resource
    return None


def get_resource_by_relationship(document_json, relationship_json):
    if isinstance(relationship_json["data"], list):
        return [
            get_resource_by_type_and_id(document_json, rel["type"], rel["id"])
            for rel in relationship_json["data"]
        ]
    else:
        return get_resource_by_type_and_id(
            document_json,
            relationship_json["data"]["type"],
            relationship_json["data"]["id"]
        )
    for resource in all_resources_in_document(document_json):
        if resource["type"] == relationship_json["data"]["type"] and resource["id"] == relationship_json["data"]["id"]:
            return resource
    return None
