from nose.plugins.attrib import attr
from nose.tools import *
import patreon
from patreon.app.api.resources.campaigns import CampaignResource
from patreon.app.api.resources import ApiResource
from patreon.model.manager import campaign_mgr, reward_mgr
from patreon.model.table import RewardsGive
from patreon.model.type import Address
from test.fixtures.fixtures_rewards_give import create_rewards_give
from test.fixtures.session_fixtures import create_web_session
from test.fixtures.user_fixtures import creator, patron
from test.fixtures.reward_fixtures import create_reward
from test.fixtures.goal_fixtures import create_goal
from test.fixtures.pledge_fixtures import create_pledge
from test.app.api.resources.asserts import assert_relationship, get_resource_by_relationship


def _assert_json_has_default_reward_tiers(campaign_json):
    has_patron_only = has_everyone = False

    reward_relationships = assert_relationship(campaign_json["data"], "rewards", list)
    reward_resources = get_resource_by_relationship(campaign_json, reward_relationships)

    for reward_resource in reward_resources:
        assert_equals("reward", reward_resource['type'])
        if int(reward_resource['id']) == -1:
            has_everyone = True
        elif int(reward_resource['attributes']['amount_cents']) <= 1:
            has_patron_only = True

    assert_true(has_patron_only)
    assert_true(has_everyone)


def _assert_campaign_has_default_reward_tiers(campaign_id):
    campaign = campaign_mgr.get_campaign(campaign_id)

    campaign_json = CampaignResource(campaign).as_json_api_document(version=ApiResource.JSON_API_STABLE)
    _assert_json_has_default_reward_tiers(campaign_json)


@patreon.test.manage()
def test_reward_tiers():
    user_id = creator().user_id
    campaign_id = campaign_mgr.try_get_campaign_id_by_user_id_hack(user_id)

    # Rewards: []
    _assert_campaign_has_default_reward_tiers(campaign_id)

    # Rewards: [2]
    create_reward(user_id, 2)
    _assert_campaign_has_default_reward_tiers(campaign_id)

    # Rewards: [0, 2]
    create_reward(user_id, 0)
    _assert_campaign_has_default_reward_tiers(campaign_id)

    # Rewards: [0, 1, 2]
    reward_1 = create_reward(user_id, 1)
    _assert_campaign_has_default_reward_tiers(campaign_id)

    # Rewards: [1, 2]
    reward_mgr.delete(reward_1.reward_id, user_id)
    _assert_campaign_has_default_reward_tiers(campaign_id)


def _assert_campaign_has_outstanding_payment_amount(campaign_id, user_id, amount):
    campaign = campaign_mgr.get_campaign(campaign_id)
    campaign_json = CampaignResource(campaign, current_user_id=user_id) \
        .as_json_api_document(version=ApiResource.JSON_API_STABLE)

    assert_true('data' in campaign_json)
    assert_true('attributes' in campaign_json['data'])
    assert_true('outstanding_payment_amount_cents' in campaign_json['data']['attributes'])
    assert_equals(amount, campaign_json['data']['attributes']['outstanding_payment_amount_cents'])


@patreon.test.manage()
def test_pending_amounts():
    creator_user_id = creator().user_id
    patron_user_id = patron().user_id
    campaign_id = campaign_mgr.try_get_campaign_id_by_user_id_hack(creator_user_id)

    _assert_campaign_has_outstanding_payment_amount(campaign_id, patron_user_id, 0)

    create_rewards_give(UID=patron_user_id, CID=creator_user_id, HID=1, RID=1, Pledge=100, Status=0)
    _assert_campaign_has_outstanding_payment_amount(campaign_id, patron_user_id, 100)

    create_rewards_give(UID=patron_user_id, CID=creator_user_id, HID=2, RID=1, Pledge=200, Status=0)
    _assert_campaign_has_outstanding_payment_amount(campaign_id, patron_user_id, 300)

    RewardsGive.query.filter_by(UID=patron_user_id).update({'Status': 1})
    patreon.services.clear_request_cache()

    _assert_campaign_has_outstanding_payment_amount(campaign_id, patron_user_id, 0)

    create_rewards_give(UID=patron_user_id, CID=creator_user_id, HID=1, RID=1, Pledge=100, Status=0)
    _assert_campaign_has_outstanding_payment_amount(campaign_id, patron_user_id, 100)

    RewardsGive.query.filter_by(UID=patron_user_id).update({'Status': 2})
    patreon.services.clear_request_cache()

    _assert_campaign_has_outstanding_payment_amount(campaign_id, patron_user_id, 400)

    create_rewards_give(UID=patron_user_id, CID=creator_user_id, HID=1, RID=1, Pledge=100, Status=0)
    _assert_campaign_has_outstanding_payment_amount(campaign_id, patron_user_id, 500)

    RewardsGive.query.filter_by(UID=patron_user_id).update({'Status': 10})
    patreon.services.clear_request_cache()

    _assert_campaign_has_outstanding_payment_amount(campaign_id, patron_user_id, 0)


@patreon.test.manage()
def test_goals():
    user_id = creator().user_id
    campaign_id = campaign_mgr.try_get_campaign_id_by_user_id_hack(user_id)

    create_goal(campaign_id, 500, "Test Goal", "description")

    campaign = campaign_mgr.get_campaign(campaign_id)

    campaign_json = CampaignResource(campaign).as_json_api_document(version=ApiResource.JSON_API_STABLE)

    _assert_json_has_default_reward_tiers(campaign_json)

    goal_relationships = assert_relationship(campaign_json["data"], "goals", list)

    assert_equals(1, len(goal_relationships["data"]))
    goal_resource = get_resource_by_relationship(campaign_json, goal_relationships)[0]

    assert_equals(goal_resource['type'], 'goal')
    assert_equals(goal_resource['attributes']['title'], "Test Goal", "The title field is different from expected.")
    assert_equals(goal_resource['attributes']['description'], "description",
                  "The description field is different than expected.")
    assert_equals(goal_resource['attributes']['amount_cents'], 500, "The amount field is different than expected.")


@patreon.test.manage()
def test_current_user_pledge():
    creator_id = creator().user_id
    campaign_id = campaign_mgr.try_get_campaign_id_by_user_id_hack(creator_id)
    campaign = campaign_mgr.get_campaign(campaign_id)

    patron_id = patron().user_id

    # no pledge exists
    campaign_json = CampaignResource(
        campaign, includes=["current_user_pledge"], current_user_id=patron_id
    ).as_json_api_document(version=ApiResource.JSON_API_STABLE)

    assert_relationship(campaign_json["data"], "current_user_pledge", None)

    # pledge exists
    pledge = create_pledge(patron_id, creator_id)

    campaign_json = CampaignResource(
        campaign, includes=["current_user_pledge"], current_user_id=patron_id
    ).as_json_api_document(version=ApiResource.JSON_API_STABLE)

    pledge_relationship = assert_relationship(campaign_json["data"], "current_user_pledge", "pledge", pledge.pledge_id)
    pledge_resource = get_resource_by_relationship(campaign_json, pledge_relationship)
    assert_equals(pledge.amount, pledge_resource["attributes"]["amount_cents"])


@patreon.test.manage()
def test_current_user_pledge_with_address():
    creator_ = creator()
    creator_id = creator_.user_id

    # TODO: should not have to make the session this early, but otherwise it explodes on
    # sqlalchemy.orm.exc.DetachedInstanceError: Parent instance <Campaign ...> is not bound to a Session;
    # lazy load operation of attribute 'campaigns_users' cannot proceed
    mock_session = create_web_session(user=creator_)
    with mock_session:
        campaign_id = campaign_mgr.try_get_campaign_id_by_user_id_hack(creator_id)
        campaign = campaign_mgr.get_campaign(campaign_id)

        patron_id = patron().user_id

        address = Address("100 Patreon St", None, "99999", "San Francisco", "CA", "US")

        # pledge exists
        pledge = create_pledge(patron_id, creator_id, address=address)

        campaign_json = CampaignResource(
            campaign, includes=["current_user_pledge"], current_user_id=patron_id
        ).as_json_api_document(version=ApiResource.JSON_API_STABLE)

    pledge_relationship = assert_relationship(campaign_json["data"], "current_user_pledge", "pledge", pledge.pledge_id)
    pledge_resource = get_resource_by_relationship(campaign_json, pledge_relationship)
    address_relationship = assert_relationship(pledge_resource, "address", "address")
    address_resource = get_resource_by_relationship(campaign_json, address_relationship)
    assert_equals(address.address_line_1, address_resource["attributes"]["line_1"])
    assert_equals(address.address_line_2, address_resource["attributes"]["line_2"])
    assert_equals(address.address_zip, address_resource["attributes"]["postal_code"])
    assert_equals(address.city, address_resource["attributes"]["city"])
    assert_equals(address.state, address_resource["attributes"]["state"])
    assert_equals(address.country, address_resource["attributes"]["country"])


@patreon.test.manage()
@attr(speed='slower')
def test_current_user_pledge_without_address():
    creator_ = creator()
    creator_id = creator_.user_id

    # TODO: should not have to make the session this early, but otherwise it explodes on
    # sqlalchemy.orm.exc.DetachedInstanceError: Parent instance <Campaign ...> is not bound to a Session;
    # lazy load operation of attribute 'campaigns_users' cannot proceed
    mock_session = create_web_session(user=creator_)
    with mock_session:
        campaign_id = campaign_mgr.try_get_campaign_id_by_user_id_hack(creator_id)
        campaign = campaign_mgr.get_campaign(campaign_id)

        patron_id = patron().user_id

        # pledge exists
        pledge = create_pledge(patron_id, creator_id, address=None)

        campaign_json = CampaignResource(
            campaign, includes=["current_user_pledge"], current_user_id=patron_id
        ).as_json_api_document(version=ApiResource.JSON_API_STABLE)

    pledge_relationship = assert_relationship(campaign_json["data"], "current_user_pledge", "pledge", pledge.pledge_id)
    pledge_resource = get_resource_by_relationship(campaign_json, pledge_relationship)
    address_relationship = assert_relationship(pledge_resource, "address", None)
