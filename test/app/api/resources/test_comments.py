from nose.tools import *
import patreon
from patreon.app.api.resources.comments import CommentResource, \
    PostCommentsCollection, PostCommentsCollectionPaged
from patreon.model.manager import campaign_mgr
from patreon.model.table import Comment
from patreon.util.datetime import datetime
from test.fixtures.comment_fixtures import create_comment, insert_comment
from test.fixtures.activity_fixtures import create_post
from test.fixtures.user_fixtures import creator, patron
from test.test_helpers.comment_helpers import assert_page_comment_is, \
    assert_page_equals_comments

comment_text_1 = "This is my excellent comment."
comment_text_2 = "This is the parent comment."
comment_text_3 = "This is the original comment."
comment_text_4 = "This is my comment_update."


@patreon.test.manage()
def test_new_comment_from_json_api():
    user = patron()
    creator_id = creator().user_id
    campaign_id = campaign_mgr.try_get_campaign_id_by_user_id_hack(creator_id)
    post_id = create_post(creator_id, campaign_id).activity_id
    post_linkage = {'type': 'post', 'id': str(post_id)}
    comment_data = {
        'type': 'comment',
        'body': comment_text_1,
        'links': {
            'post': {'linkage': post_linkage}
        }
    }
    json_api_doc = {'data': comment_data}
    resource = CommentResource.new_from_json_api_document(json_api_doc, current_user=user)
    assert_is_instance(resource.model, Comment)
    assert_is_not_none(resource.model.comment_id)
    assert_equals(comment_text_1, resource.model.comment_text)
    assert_equals(user, resource.model.commenter)

    result_doc = resource.as_json_api_document()
    assert_equals(
        {'type': 'user', 'id': str(user.user_id)},
        result_doc['data']['links']['commenter']['linkage']
    )


@patreon.test.manage()
def test_new_comment_reply_from_json_api():
    user = patron()
    creator_id = creator().user_id
    campaign_id = campaign_mgr.try_get_campaign_id_by_user_id_hack(creator_id)
    post_id = create_post(creator_id, campaign_id).activity_id
    comment = create_comment(creator_id, post_id, comment_text_2)

    post_linkage = {'type': 'post', 'id': str(post_id)}
    parent_comment_linkage = {'type': 'comment', 'id': str(comment.comment_id)}
    comment_data = {
        'type': 'comment',
        'body': comment_text_1,
        'links': {
            'post': {'linkage': post_linkage},
            'parent': {'linkage': parent_comment_linkage}
        }
    }
    json_api_doc = {'data': comment_data}
    resource = CommentResource.new_from_json_api_document(json_api_doc, current_user=user)
    assert_is_instance(resource.model, Comment)
    assert_is_not_none(resource.model.comment_id)
    assert_equals(comment_text_1, resource.model.comment_text)
    assert_equals(user, resource.model.commenter)

    result_doc = resource.as_json_api_document()
    assert_equals(
        {'type': 'user', 'id': str(user.user_id)},
        result_doc['data']['links']['commenter']['linkage']
    )
    assert_equals(
        {'type': 'comment', 'id': str(user.user_id)},
        result_doc['data']['links']['parent']['linkage']
    )


@patreon.test.manage()
def test_update_comment_from_json_api():
    user = patron()
    creator_id = creator().user_id
    campaign_id = campaign_mgr.try_get_campaign_id_by_user_id_hack(creator_id)
    post_id = create_post(creator_id, campaign_id).activity_id
    comment = create_comment(user.user_id, post_id, comment_text_3)

    comment_data = {
        'type': 'comment',
        'id': str(comment.comment_id),
        'body': comment_text_4,
    }

    json_api_doc = {'data': comment_data}
    resource = CommentResource.update_from_json_api_document(json_api_doc, current_user=user)
    assert_is_instance(resource.model, Comment)
    assert_equals(comment.comment_id, resource.model.comment_id)

    result_doc = resource.as_json_api_document()
    assert_equals(comment_text_4, result_doc['data']['body'])


@patreon.test.manage()
def test_get_comments_by_post():
    user = patron()
    creator_id = creator().user_id
    campaign_id = campaign_mgr.try_get_campaign_id_by_user_id_hack(creator_id)
    post = create_post(creator_id, campaign_id)
    comments = [
        insert_comment(
            user.user_id,
            post.activity_id,
            comment_text='This is comment {}.'.format(i),
            created_at=datetime(year=2015, month=1, day=i),
            vote_sum=(5 if i == 5 else 0)
        ) for i in range(1, 11)
    ]


    page = PostCommentsCollection(post)
    page_created = PostCommentsCollection(post, sort='created')
    page_created_reversed = PostCommentsCollection(post, sort='-created')
    page_votes = PostCommentsCollection(post, sort='vote_sum')
    page_votes_reversed = PostCommentsCollection(post, sort='-vote_sum')

    assert_page_equals_comments(page, comments)
    assert_page_equals_comments(page_created, comments)
    assert_page_equals_comments(page_created_reversed, reversed(comments))
    assert_equals(page_votes.members()[-1].model.Comment, 'This is comment 5.')
    assert_equals(page_votes_reversed.members()[0].model.Comment, 'This is comment 5.')


@patreon.test.manage()
def test_get_paged_comments_by_post():
    user = patron()
    creator_id = creator().user_id
    campaign_id = campaign_mgr.try_get_campaign_id_by_user_id_hack(creator_id)
    post = create_post(creator_id, campaign_id)
    comments = [
        insert_comment(
            user.user_id,
            post.activity_id,
            comment_text='This is comment {}.'.format(i),
            created_at=datetime(year=2015, month=1, day=i)
        ) for i in range(1, 11)
    ]
    for i, comment in enumerate(comments):
        Comment.get(comment.comment_id).update({'vote_sum': i % 3})

    page1 = PostCommentsCollectionPaged(post, per_page=2, cursor=None, sort='created')
    assert_equals(len(page1.members()), 2)
    assert_page_comment_is(page1, 0, comments, 0)
    assert_page_comment_is(page1, 1, comments, 1)

    page2 = PostCommentsCollectionPaged(post, per_page=2, cursor=comments[1].CID, sort='created')
    assert_equals(len(page2.members()), 2)
    assert_page_comment_is(page2, 0, comments, 2)
    assert_page_comment_is(page2, 1, comments, 3)

    page3 = PostCommentsCollectionPaged(post, per_page=1000, cursor=comments[3].CID)
    assert_equals(len(page3.members()), 6)
    assert_page_comment_is(page3, 0, comments, 4)
    assert_page_comment_is(page3, 5, comments, 9)

    assert_equals(
        page1.pagination_urls()['next'],
        'https://{0}/posts/{1}/comments?page[cursor]={2}&page[count]=2&sort=created'.format(
            patreon.config.api_server,
            post.activity_id,
            comments[1].CID
        )
    )
    assert_equals(
        page1.pagination_urls()['first'],
        'https://{0}/posts/{1}/comments?page[count]=2&sort=created'.format(
            patreon.config.api_server,
            post.activity_id
        )
    )
    assert_equals(page1.pagination_urls()['first'], page2.pagination_urls()['first'])
    assert_equals(
        page3.pagination_urls()['first'],
        'https://{0}/posts/{1}/comments?page[count]=1000&sort=created'.format(
            patreon.config.api_server,
            post.activity_id
        )
    )

    page_backwards = PostCommentsCollectionPaged(post, per_page=2, cursor=None, sort='-created')
    assert_equals(len(page_backwards.members()), 2)
    assert_page_comment_is(page_backwards, 0, comments, 9)
    assert_page_comment_is(page_backwards, 1, comments, 8)
    assert_equals(
        page_backwards.pagination_urls()['first'],
        'https://{0}/posts/{1}/comments?page[count]=2&sort=-created'.format(
            patreon.config.api_server,
            post.activity_id
        )
    )

    sort_votes = PostCommentsCollectionPaged(post, per_page=4, offset=0, sort='-vote_sum,-created')
    assert_equals(len(sort_votes.members()), 4)
    assert_page_comment_is(sort_votes, 0, comments, 8)
    assert_page_comment_is(sort_votes, 1, comments, 5)
    assert_page_comment_is(sort_votes, 2, comments, 2)
    assert_page_comment_is(sort_votes, 3, comments, 7)
    assert_equals(
        sort_votes.pagination_urls()['first'],
        'https://{0}/posts/{1}/comments?page[count]=4&sort=-vote_sum,-created'.format(
            patreon.config.api_server,
            post.activity_id
        )
    )
    assert_equals(
        sort_votes.pagination_urls()['next'],
        'https://{0}/posts/{1}/comments?page[offset]=4&page[count]=4&sort=-vote_sum,-created'.format(
            patreon.config.api_server,
            post.activity_id
        )
    )

    sort_votes_page_2 = PostCommentsCollectionPaged(post, per_page=4, offset=4, sort='-vote_sum,-created')
    assert_equals(len(sort_votes.members()), 4)
    assert_page_comment_is(sort_votes_page_2, 0, comments, 4)
    assert_page_comment_is(sort_votes_page_2, 1, comments, 1)
    assert_page_comment_is(sort_votes_page_2, 2, comments, 9)
    assert_page_comment_is(sort_votes_page_2, 3, comments, 6)
    assert_equals(
        sort_votes_page_2.pagination_urls()['first'],
        'https://{0}/posts/{1}/comments?page[count]=4&sort=-vote_sum,-created'.format(
            patreon.config.api_server,
            post.activity_id
        )
    )
    assert_equals(
        sort_votes_page_2.pagination_urls()['next'],
        'https://{0}/posts/{1}/comments?page[offset]=8&page[count]=4&sort=-vote_sum,-created'.format(
            patreon.config.api_server,
            post.activity_id
        )
    )
