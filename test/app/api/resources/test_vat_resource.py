from nose.tools import *
import patreon
from patreon.app.api.resources.vat import VatResource


@patreon.test.manage()
def test_vat_resource():
    id = '6'
    data = {'test': '123'}
    vat_resource = VatResource(id, data).as_json_api_document()

    assert_in('data', vat_resource)
    assert_equals(vat_resource['data']['id'], id)
    assert_in('type', vat_resource['data'])
    assert_equals(vat_resource['data']['type'], 'vat_rate')
    assert_in('test', vat_resource['data'])
    assert_equals(vat_resource['data']['test'], '123')
