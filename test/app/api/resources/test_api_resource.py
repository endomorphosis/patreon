import patreon
from patreon.app.api.resources import ApiResource, ApiResourceNull, \
    ApiResourceCollection, ApiResourceReference
from nose.tools import *


class ExampleResource(ApiResource):
    def __init__(self, id):
        self._id = id

    def resource_type(self):
        return "example"

    def resource_id(self):
        return str(self._id)

    def as_json(self):
        return {
            "title": "an example"
        }


class LinkingResource(ExampleResource):
    def __init__(self, id, link):
        self._id = id
        self._link = link

    def resource_type(self):
        return "linking_example"

    def resource_id(self):
        return str(self._id)

    def as_json(self):
        return {
            "title": "an example of linking"
        }

    def linked_resources(self):
        return {
            "something": self._link
        }


@patreon.test.manage()
def test_api_resource():
    resource = ExampleResource(1)
    expected_json = {
        "data": {
            "type": "example",
            "id": "1",
            "title": "an example"
        }
    }
    assert_equal(expected_json, resource.as_json_api_document())


@patreon.test.manage()
def test_linked_resource():
    resource = LinkingResource(1, ExampleResource(2))
    expected_json = {
        "data": {
            "type": "linking_example",
            "id": "1",
            "title": "an example of linking",
            "links": {
                "something": {
                    "linkage": {"type": "example", "id": "2"}
                }
            }
        },
        "included": [
            {
                "type": "example",
                "id": "2",
                "title": "an example"
            }
        ]
    }
    assert_equal(expected_json, resource.as_json_api_document())


@patreon.test.manage()
def test_linked_null():
    resource = LinkingResource(1, ApiResourceNull())
    expected_json = {
        "data": {
            "type": "linking_example",
            "id": "1",
            "title": "an example of linking",
            "links": {
                "something": {
                    "linkage": None
                }
            }
        }
    }
    assert_equal(expected_json, resource.as_json_api_document())


@patreon.test.manage()
def test_linked_collection():
    resource = LinkingResource(1, ApiResourceCollection([ExampleResource(2), ExampleResource(3)]))
    expected_json = {
        "data": {
            "type": "linking_example",
            "id": "1",
            "title": "an example of linking",
            "links": {
                "something": {
                    "linkage": [{"type": "example", "id": "2"}, {"type": "example", "id": "3"}]
                }
            }
        },
        "included": [
            {
                "type": "example",
                "id": "2",
                "title": "an example"
            },
            {
                "type": "example",
                "id": "3",
                "title": "an example"
            },
        ]
    }
    assert_equal(expected_json, resource.as_json_api_document())


@patreon.test.manage()
def test_linked_heterogeneous_collection():
    resource = LinkingResource(1, ApiResourceCollection([ExampleResource(2), LinkingResource(2, ExampleResource(3))]))
    expected_json = {
        "data": {
            "type": "linking_example",
            "id": "1",
            "title": "an example of linking",
            "links": {
                "something": {
                    "linkage": [{
                        "type": "example",
                        "id": "2"
                    }, {
                        "type": "linking_example",
                        "id": "2"
                    }]
                }
            }
        },
        "included": [
            {
                "type": "example",
                "id": "2",
                "title": "an example"
            },
            {
                "type": "linking_example",
                "id": "2",
                "title": "an example of linking",
                "links": {"something": {"linkage": {"type": "example", "id": "3"}}}
            },
            {
                "type": "example",
                "id": "3",
                "title": "an example"
            },
        ]
    }
    assert_equal(expected_json, resource.as_json_api_document())


@patreon.test.manage()
def test_toplevel_collection():
    resource = ApiResourceCollection([ExampleResource(2), LinkingResource(2, ExampleResource(3))])
    expected_json = {
        "data": [
            {
                "type": "example",
                "id": "2",
                "title": "an example"
            },
            {
                "type": "linking_example",
                "id": "2",
                "title": "an example of linking",
                "links": {"something": {"linkage": {"type": "example", "id": "3"}}}
            },
        ],
        "included": [
            {
                "type": "example",
                "id": "3",
                "title": "an example"
            },
        ]
    }
    assert_equal.__self__.maxDiff = None
    assert_equal(expected_json, resource.as_json_api_document())


@patreon.test.manage()
def test_toplevel_collection_with_repeats():
    example_resource = ExampleResource(10)
    resource = ApiResourceCollection([
        example_resource,
        LinkingResource(2,
            ApiResourceCollection([
                example_resource,
                LinkingResource(3, example_resource)
            ])
        )
    ])
    expected_json = {
        "data": [
            {
                "type": "example",
                "id": "10",
                "title": "an example"
            },
            {
                "type": "linking_example",
                "id": "2",
                "title": "an example of linking",
                "links": {
                    "something": {"linkage": [
                        {"type": "example", "id": "10"},
                        {"type": "linking_example", "id": "3"}
                    ]}
                }
            },
        ],
        "included": [
            {
                "type": "linking_example",
                "id": "3",
                "title": "an example of linking",
                "links": {
                    "something": {"linkage": {"type": "example", "id": "10"}}
                }
            },
        ]
    }
    assert_equal(expected_json, resource.as_json_api_document())


@patreon.test.manage()
def test_link_to_unincluded_resource():
    resource = LinkingResource(1, ApiResourceReference("https://api.patreon.com/thing/10"))
    expected_json = {
        "data": {
            "type": "linking_example",
            "id": "1",
            "title": "an example of linking",
            "links": {
                "something": "https://api.patreon.com/thing/10"
            }
        }
    }
    assert_equal(expected_json, resource.as_json_api_document())


@patreon.test.manage()
def test_link_to_unincluded_resource_with_relationship_url():
    resource = LinkingResource(
        1,
        ApiResourceReference(
            "https://api.patreon.com/thing/10",
            "https://api.patreon.com/linking_example/1/something/"
        )
    )
    expected_json = {
        "data": {
            "type": "linking_example",
            "id": "1",
            "title": "an example of linking",
            "links": {
                "something": {
                    "resource": "https://api.patreon.com/thing/10",
                    "self": "https://api.patreon.com/linking_example/1/something/"
                }
            }
        }
    }
    assert_equal(expected_json, resource.as_json_api_document())
