import json
from nose.tools import *

import patreon
from patreon.app.api import make_error_response
from patreon.exception import APIException, ViewForbidden, ActionForbidden
from patreon.exception.auth_errors import InvalidUser
from patreon.exception.campaign_errors import HasNoCampaign
from patreon.exception.common_errors import DataMissing, LoginRequired
from patreon.exception.post_errors import PostViewForbidden, CannotPost, \
    BadPostType, EmbedMissing, InvalidImageFormat


def _get_error(exception):
    response = make_error_response(exception)
    response_string = response.response[0].decode("utf-8")
    response_dict = json.loads(response_string)
    return response_dict.get('errors')[0]


def _assert_error_equals(error, status, code_name, code, title, detail=None):
    assert_equal(error['status'], status)
    assert_equal(error['code_name'], code_name)
    assert_equal(error['code'], code)
    assert_equal(error['title'], title)
    assert_equal(error['detail'], detail or title)


@patreon.test.manage()
def test_api_exceptions():
    _assert_error_equals(
        _get_error(APIException()),
        '500',
        'APIException',
        1,
        "Internal Error."
    )

    _assert_error_equals(
        _get_error(ActionForbidden(None)),
        '403',
        'ActionForbidden',
        5,
        "You do not have permission to None this None."
    )

    _assert_error_equals(
        _get_error(ViewForbidden(None)),
        '403',
        'ViewForbidden',
        6,
        "You do not have permission to view this None."
    )

    _assert_error_equals(
        _get_error(PostViewForbidden(1)),
        '403',
        'PostViewForbidden',
        804,
        "You do not have permission to view this post.",
        "You do not have permission to view post with id 1."
    )

    _assert_error_equals(
        _get_error(HasNoCampaign()),
        '404',
        'HasNoCampaign',
        301,
        "Current user does not have a campaign."
    )

    _assert_error_equals(
        _get_error(CannotPost(1)),
        '403',
        'CannotPost',
        806,
        "You do not have permission to post to this campaign.",
        "You do not have permission to post to campaign with id 1."
    )

    _assert_error_equals(
        _get_error(DataMissing()),
        '400',
        'DataMissing',
        51,
        "Parameter 'data' is missing."
    )

    _assert_error_equals(
        _get_error(LoginRequired()),
        '401',
        'LoginRequired',
        53,
        "This route is restricted to logged in users."
    )

    _assert_error_equals(
        _get_error(BadPostType(None)),
        '400',
        'BadPostType',
        808,
        "Invalid value for parameter 'post_type'.",
        "Invalid parameter for 'post_type': None."
    )

    _assert_error_equals(
        _get_error(EmbedMissing()),
        '404',
        'EmbedMissing',
        800,
        "Invalid embed URL."
    )

    _assert_error_equals(
        _get_error(InvalidUser()),
        '403',
        'InvalidUser',
        100,
        "Incorrect email or password."
    )

    _assert_error_equals(
        _get_error(InvalidImageFormat('.wav')),
        '500',
        'InvalidImageFormat',
        802,
        "Could not process file with non-image format.",
        "Could not process file '.wav' with non-image format."
    )
