from unittest.mock import patch

from nose.plugins.attrib import attr
from nose.tools import *
import patreon
from patreon.app.api import api_app
from patreon.model.manager import campaign_mgr, auth_mgr, push_mgr
from patreon.services.aws import SNS
from patreon.test import MockSession
from patreon.util.unsorted import jsdecode
from test.fixtures.pledge_fixtures import create_pledge
from test.fixtures.user_fixtures import creator as create_creator
from test.test_helpers.user_helpers import get_new_logged_in_creator


@patreon.test.manage()
def test_get_settings():
    creator, session = get_new_logged_in_creator()

    response = session.get('/settings')

    assert_equals(response.status_code, 200)

    data = jsdecode(response.data.decode('utf-8')).get('data')
    # these tests will fail when default values here change
    assert_equals(data.get('email_about_all_new_comments'), True)
    assert_equals(data.get('email_about_milestone_goals'), True)
    assert_equals(data.get('email_about_patreon_updates'), True)
    assert_equals(data.get('push_about_all_new_comments'), True)
    assert_equals(data.get('push_about_milestone_goals'), True)
    assert_equals(data.get('push_about_patreon_updates'), True)
    assert_equals(data.get('pledges_are_private'), False)


@patreon.test.manage()
def test_patch_settings():
    creator, session = get_new_logged_in_creator()

    data = {
        'email_about_all_new_comments': False,
        'email_about_milestone_goals': False,
        'email_about_patreon_updates': False,
        'push_about_all_new_comments': False,
        'push_about_milestone_goals': False,
        'push_about_patreon_updates': False,
        'pledges_are_private': True,
    }
    response = session.patch('/settings', data=data)

    assert_equals(response.status_code, 200)

    data = jsdecode(response.data.decode('utf-8')).get('data')
    assert_equals(data.get('email_about_all_new_comments'), False)
    assert_equals(data.get('email_about_milestone_goals'), False)
    assert_equals(data.get('email_about_patreon_updates'), False)
    assert_equals(data.get('push_about_all_new_comments'), False)
    assert_equals(data.get('push_about_milestone_goals'), False)
    assert_equals(data.get('push_about_patreon_updates'), False)
    assert_equals(data.get('pledges_are_private'), True)


@patreon.test.manage()
def test_get_campaign_settings():
    creator, session = get_new_logged_in_creator()
    campaign_id = campaign_mgr.try_get_campaign_id_by_user_id_hack(creator.user_id)

    id_str = '{}-{}-campaign-settings'.format(creator.user_id, campaign_id)
    response = session.get('/campaign-settings/' + id_str)

    assert_equals(response.status_code, 200)

    data = jsdecode(response.data.decode('utf-8')).get('data')
    # these tests will fail when default values here change
    assert_equals(data.get('email_about_new_patrons'), True)
    assert_equals(data.get('email_about_new_patron_posts'), True)
    assert_equals(data.get('push_about_new_patrons'), True)
    assert_equals(data.get('push_about_new_patron_posts'), True)


@patreon.test.manage()
def test_patch_campaign_settings():
    creator, session = get_new_logged_in_creator()
    campaign_id = campaign_mgr.try_get_campaign_id_by_user_id_hack(creator.user_id)

    data = {
        'email_about_new_patrons': False,
        'email_about_new_patron_posts': False,
        'push_about_new_patrons': False,
        'push_about_new_patron_posts': False,
    }
    id_str = '{}-{}-campaign-settings'.format(creator.user_id, campaign_id)
    response = session.patch('/campaign-settings/' + id_str, data=data)

    assert_equals(response.status_code, 200)

    data = jsdecode(response.data.decode('utf-8')).get('data')
    assert_equals(data.get('email_about_new_patrons'), False)
    assert_equals(data.get('email_about_new_patron_posts'), False)
    assert_equals(data.get('push_about_new_patrons'), False)
    assert_equals(data.get('push_about_new_patron_posts'), False)


@patreon.test.manage()
def test_get_follow_settings():
    pledgee = create_creator()
    campaign_id = campaign_mgr.try_get_campaign_id_by_user_id_hack(pledgee.user_id)
    pledger, session = get_new_logged_in_creator()
    create_pledge(patron_id=pledger.user_id, creator_id=pledgee.user_id)

    id_str = '{}-{}-follow-settings'.format(pledger.user_id, campaign_id)
    response = session.get('/follow-settings/' + id_str)

    assert_equals(response.status_code, 200)

    data = jsdecode(response.data.decode('utf-8')).get('data')
    # these tests will fail when default values here change
    assert_equals(data.get('email_about_new_paid_posts'), True)
    assert_equals(data.get('email_about_new_posts'), True)
    assert_equals(data.get('email_about_new_comments'), True)
    assert_equals(data.get('push_about_new_paid_posts'), True)
    assert_equals(data.get('push_about_new_posts'), True)
    assert_equals(data.get('push_about_new_comments'), True)
    assert_equals(data.get('push_about_going_live'), True)


@patreon.test.manage()
def test_patch_follow_settings():
    pledgee = create_creator()
    campaign_id = campaign_mgr.try_get_campaign_id_by_user_id_hack(pledgee.user_id)
    pledger, session = get_new_logged_in_creator()
    create_pledge(patron_id=pledger.user_id, creator_id=pledgee.user_id)

    data = {
        'email_about_new_paid_posts': False,
        'email_about_new_posts': False,
        'email_about_new_comments': False,
        'push_about_new_paid_posts': False,
        'push_about_new_posts': False,
        'push_about_new_comments': False,
        'push_about_going_live': False
    }
    id_str = '{}-{}-follow-settings'.format(pledger.user_id, campaign_id)
    response = session.patch('/follow-settings/' + id_str, data=data)

    assert_equals(response.status_code, 200)

    data = jsdecode(response.data.decode('utf-8')).get('data')
    assert_equals(data.get('email_about_new_paid_posts'), False)
    assert_equals(data.get('email_about_new_posts'), False)
    assert_equals(data.get('email_about_new_comments'), False)
    assert_equals(data.get('push_about_new_paid_posts'), False)
    assert_equals(data.get('push_about_new_posts'), False)
    assert_equals(data.get('push_about_new_comments'), False)
    assert_equals(data.get('push_about_going_live'), False)


@patreon.test.manage()
@patch.object(SNS, 'create_platform_endpoint')
def test_post_push_info(create_platform_endpoint_mock):
    user, session = get_new_logged_in_creator()

    post_data = {
        'bundle_id': 'com.patreon.iphone.beta',
        'token': 'somerandomhexstringexistsrighthere1'
    }
    create_platform_endpoint_mock.return_value = {
        'CreatePlatformEndpointResponse': {
            'CreatePlatformEndpointResult': {
                'EndpointArn': {
                    'some_endpoint_arn_from_aws'
                }
            }
        }
    }
    response = session.post('/push-info', data=post_data)

    assert_equals(response.status_code, 201)

    create_platform_endpoint_mock.assert_called_with(
        platform_application_arn=SNS.arn_prefix() + 'app/APNS/Patreon-iPhone-Beta',
        token=post_data['token'],
        custom_user_data=str(user.user_id)
    )

    data = jsdecode(response.data.decode('utf-8')).get('data')
    # these tests will fail when default values here change
    assert_equals(data.get('bundle_id'), data['bundle_id'])
    # TODO: time?
    # assert_equals(data.get('created_at'), True)
    # assert_equals(data.get('edited_at'), True)
    user_linkage = data.get('links').get('user').get('linkage')
    assert_equals(user_linkage.get('type'), 'user')
    assert_equals(user_linkage.get('id'), str(user.user_id))


@patreon.test.manage()
@patch.object(SNS, 'create_platform_endpoint')
def test_delete_push_info(create_platform_endpoint_mock):
    create_platform_endpoint_mock.return_value = {
        'CreatePlatformEndpointResponse': {
            'CreatePlatformEndpointResult': {
                'EndpointArn': {
                    'some_endpoint_arn_from_aws'
                }
            }
        }
    }
    def create_push_info():
        user, session = get_new_logged_in_creator()
        bundle_id = 'com.patreon.iphone.beta',
        token = 'somerandomhexstringexistsrighthere1'
        push_mgr.update_or_create(user_id=user.user_id, bundle_id=bundle_id, token=token)
        payload = {
            'attributes': {
                'bundle_id': bundle_id,
                'token': token
            },
            'relationships': {
                'user': {
                    'data': {
                        'id': user.user_id,
                        'type': 'user'
                    }
                }
            }
        }
        return user, session, payload

    user, session, no_user_payload = create_push_info()
    no_user_payload.pop("relationships", None)
    response = session.delete('/push-info?json-api-version=1.0', data=no_user_payload)
    assert_equals(response.status_code, 204)

    user, session, payload = create_push_info()
    no_user_session = MockSession(api_app)
    response = no_user_session.delete('/push-info?json-api-version=1.0', data=payload)
    assert_equals(response.status_code, 204)

    user, session, no_user_payload = create_push_info()
    no_user_session = MockSession(api_app)
    no_user_payload.pop("relationships", None)
    response = no_user_session.delete('/push-info?json-api-version=1.0', data=no_user_payload)
    assert_equals(response.status_code, 401)

    user, session, payload = create_push_info()
    bunk_payload = {
        'attributes': {
            'bundle_id': 'nope',
            'token': 'not_happening'
        }
    }
    response = session.delete('/push-info?json-api-version=1.0', data=bunk_payload)
    assert_equals(response.status_code, 404)

@patreon.test.manage()
@attr(speed="slower")
def test_change_password():
    user, session = get_new_logged_in_creator()

    # missing old password and confirmation
    post_data = {'new_password': 'test1234'}
    response = session.post('/settings/change-password', data=post_data)
    assert_equals(response.status_code, 400)
    error_details = jsdecode(response.data.decode('utf-8')).get('errors')[0]
    assert_equals(error_details.get('code'), 1)
    assert_equals(error_details.get('title'), 'no old password for change password')
    assert_true(auth_mgr.check_password(user.user_id, 'password'))

    # bad old password, missing confirmation
    post_data = {'new_password': 'test1234',
                 'old_password': 'password1'}
    response = session.post('/settings/change-password', data=post_data)
    assert_equals(response.status_code, 400)
    error_details = jsdecode(response.data.decode('utf-8')).get('errors')[0]
    assert_equals(error_details.get('code'), 1)
    assert_equals(error_details.get('title'), 'missing field: new_password_confirmation')
    assert_true(auth_mgr.check_password(user.user_id, 'password'))

    # good old password, missing confirmation
    post_data = {'new_password': 'test1234',
                 'old_password': 'password'}
    response = session.post('/settings/change-password', data=post_data)
    assert_equals(response.status_code, 400)
    error_details = jsdecode(response.data.decode('utf-8')).get('errors')[0]
    assert_equals(error_details.get('code'), 1)
    assert_equals(error_details.get('title'), 'missing field: new_password_confirmation')
    assert_true(auth_mgr.check_password(user.user_id, 'password'))

    # good old password, mismatch confirmation
    post_data = {'new_password': 'test1234',
                 'new_password_confirmation': 'test12345',
                 'old_password': 'password'}
    response = session.post('/settings/change-password', data=post_data)
    assert_equals(response.status_code, 400)
    error_details = jsdecode(response.data.decode('utf-8')).get('errors')[0]
    assert_equals(error_details.get('code'), 1)
    assert_equals(error_details.get('title'), 'confirmation mismatch on password change')
    assert_true(auth_mgr.check_password(user.user_id, 'password'))

    # good old password, good confirmation
    post_data = {'new_password': 'test1234',
                 'new_password_confirmation': 'test1234',
                 'old_password': 'password'}
    response = session.post('/settings/change-password', data=post_data)
    assert_equals(response.status_code, 200)
    assert_true(auth_mgr.check_password(user.user_id, 'test1234'))
