from datetime import timezone

from nose.tools import *
import patreon
from test.test_helpers.user_helpers import get_new_logged_in_admin_api, get_new_logged_in_creator
from test.fixtures import pledge_fixtures, user_fixtures
from patreon.model.table import PaymentEvent


@patreon.test.manage()
def test_admin_payments_pledges_route():
    creator, session = get_new_logged_in_admin_api()

    creator_id = user_fixtures.creator().user_id
    patron_id = user_fixtures.patron().user_id

    pledge = pledge_fixtures.create_pledge(patron_id, creator_id, amount=1000)

    resp = session.get('/admin/payments/pledges?json-api-version=1.0')
    assert_equals(200, resp.status_code)

    assert_equals({
        'data': [
            {
                'type': 'pledge',
                'id': pledge.pledge_id,
                'attributes': {
                    'amount_cents': 1000,
                    'pledge_cap_cents': None,
                    'created_at': pledge.created_at.replace(tzinfo=timezone.utc).isoformat(),
                    'patron_pays_fees': pledge.PatronPaysFees
                },
                'relationships': {
                    'self': 'https://{}/pledges/{}'.format(patreon.config.api_server, pledge.pledge_id),
                    'address': {'data': None},
                    'patron': {'data': {'id': '3', 'type': 'user'}},
                    'pledge_vat_location': {'data': None},
                    'creator': {'data': {'id': '2', 'type': 'user'}},
                    'card': {'data': None},
                    'reward': {'data': None}
                }
            }
        ]
    }, resp.data_obj)


@patreon.test.manage()
def test_admin_payments_pledges_route_requires_admin():
    creator, session = get_new_logged_in_creator()

    response = session.get('/admin/payments/pledges?json-api-version=1.0')
    resource = response.data_obj
    assert_equals(403, response.status_code)
    assert_equals(resource['errors'][0]['title'], 'Not authorized to access route')


@patreon.test.manage()
def test_admin_payments_events_route():
    creator, session = get_new_logged_in_admin_api()

    PaymentEvent.insert({
        "type": "test",
        "subtype": "example",
        "json_data": {"data": [1, 2, 3]}
    })

    response = session.get('/admin/payments/events?json-api-version=1.0')

    assert_equals(200, response.status_code)

    resources = response.data_obj["data"]
    assert_equals(1, len(resources))

    assert_equals("payment_event", resources[0]["type"])
    assert_equals("test", resources[0]["attributes"]["payment_event_type"])
    assert_equals("example", resources[0]["attributes"]["payment_event_subtype"])
    assert_equals({"data": [1, 2, 3]}, resources[0]["attributes"]["data"])


@patreon.test.manage()
def test_admin_payments_events_route_requires_admin():
    creator, session = get_new_logged_in_creator()

    response = session.get('/admin/payments/events?json-api-version=1.0')
    resource = response.data_obj
    assert_equals(403, response.status_code)
    assert_equals(resource['errors'][0]['title'], 'Not authorized to access route')
