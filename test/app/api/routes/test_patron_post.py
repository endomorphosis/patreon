from nose.tools import *

import patreon
from patreon.model.manager import campaign_mgr
from test.fixtures import pledge_fixtures, user_fixtures
from test.fixtures.post_fixtures import image_embed_data, link_data, video_embed_data
from test.test_helpers import get_errors
from test.test_helpers.post_helpers import assert_has_embed, assert_has_post_file, \
    assert_has_thumbnail, assert_has_legacy_images, patron_post
from test.test_helpers.user_helpers import get_new_logged_in_patron


# TODO MARCOS test attachments

@patreon.test.manage()
def test_cannot_patron_post():
    # 1. Create creator.
    creator_id = user_fixtures.creator().user_id
    campaign_id = campaign_mgr.try_get_campaign_id_by_user_id_hack(creator_id)

    # 2. Login as a patron.
    patron, session = get_new_logged_in_patron()

    # 3. Prepare API call.
    endpoint = '/campaigns/' + str(campaign_id) + '/post'
    patron_post_data = {
        'post_type': 'text_only',
        'title': "Post Title",
        'content': "This is post content.",
    }

    # 4. Assert post fails.
    post_response = session.post(endpoint, patron_post_data)
    errors = get_errors(post_response)
    assert_equals(errors[0].get('code_name'), 'CannotPost')
    assert_equals(errors[0].get('code'), 806)


@patreon.test.manage()
def test_text_only():
    # 1. Create creator.
    creator_id = user_fixtures.creator().user_id
    campaign_id = campaign_mgr.try_get_campaign_id_by_user_id_hack(creator_id)

    # 2. Login as a pledging patron.
    patron, session = get_new_logged_in_patron()
    pledge_fixtures.create_pledge(patron.user_id, creator_id)

    # 3. Make a patron post.
    post = patron_post(campaign_id, session)

    # 4. Assert post succeeds.
    assert_equals(post.get('post_type'), 'text_only')
    assert_is_none(post.get('image'))
    assert_is_none(post.get('thumbnail'))
    assert_is_none(post.get('post_file'))
    assert_is_none(post.get('embed'))


@patreon.test.manage()
def test_image_embed():
    # 1. Create creator.
    creator_id = user_fixtures.creator().user_id
    campaign_id = campaign_mgr.try_get_campaign_id_by_user_id_hack(creator_id)

    # 2. Login as a pledging patron.
    patron, session = get_new_logged_in_patron()
    pledge_fixtures.create_pledge(patron.user_id, creator_id)

    # 3. Make a patron post.
    post = patron_post(campaign_id, session, image_embed_data())

    # 4. Assert post succeeds.
    assert_equals(post.get('post_type'), 'image_embed')
    assert_has_legacy_images(post.get('image'))
    assert_has_thumbnail(post.get('thumbnail'))
    assert_has_post_file(post.get('post_file'))
    assert_has_embed(post.get('embed'))


@patreon.test.manage()
def test_video_embed():
    # 1. Create creator.
    creator_id = user_fixtures.creator().user_id
    campaign_id = campaign_mgr.try_get_campaign_id_by_user_id_hack(creator_id)

    # 2. Login as a pledging patron.
    patron, session = get_new_logged_in_patron()
    pledge_fixtures.create_pledge(patron.user_id, creator_id)

    # 3. Make a patron post.
    post = patron_post(campaign_id, session, video_embed_data())

    # 4. Assert post succeeds.
    assert_equals(post.get('post_type'), 'video_embed')
    assert_is_none(post.get('image'))
    assert_is_none(post.get('thumbnail'))
    assert_is_none(post.get('post_file'))
    assert_has_embed(post.get('embed'))


@patreon.test.manage()
def test_link():
    # 1. Create creator.
    creator_id = user_fixtures.creator().user_id
    campaign_id = campaign_mgr.try_get_campaign_id_by_user_id_hack(creator_id)

    # 2. Login as a pledging patron.
    patron, session = get_new_logged_in_patron()
    pledge_fixtures.create_pledge(patron.user_id, creator_id)

    # 3. Make a patron post.
    post = patron_post(campaign_id, session, link_data())

    # 4. Assert post succeeds.
    assert_equals(post.get('post_type'), 'link')
    assert_is_none(post.get('image'))
    assert_is_none(post.get('thumbnail'))
    assert_is_none(post.get('post_file'))
    assert_has_embed(post.get('embed'))
