import patreon
from test.test_helpers.mail_helpers import AssertSendsMail
from test.test_helpers.user_helpers import get_new_logged_in_creator
from nose.tools import *

@patreon.test.manage()
def test_resend_verify_email():
    user, session = get_new_logged_in_creator()
    email = user.Email

    with AssertSendsMail(email, 1):
        response = session.post('/resend_verify_email', data={})

    assert_equals(response.status_code, 200)
