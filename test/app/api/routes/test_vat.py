from unittest.mock import patch

from nose.tools import *
import patreon
from patreon.util.unsorted import base64encode, jsdecode
from test.test_helpers.user_helpers import get_new_logged_in_patron


@patreon.test.manage()
@patch('patreon.app.api.routes.vat.get_ip')
def test_vat_endpoint(mock_function):
    us_ip = '128.101.101.101'
    gb_ip = '85.159.213.251'
    unknown_ip = '130.117.51.78'
    bad_ip = 'not_an_ip'
    patron_user, patron_session = get_new_logged_in_patron()

    mock_function.return_value = us_ip

    response = patron_session.get('/vat_rate')
    data = jsdecode(response.data.decode('utf-8'))['data']
    assert_equals(data['friendly_name'], 'United States')
    assert_equals(data['id'], base64encode(us_ip))
    assert_equals(data['iso_code'], 'US')
    assert_equals(data['type'], 'vat_rate')
    assert_equals(data['vat_rate'], None)

    mock_function.return_value = gb_ip

    response = patron_session.get('/vat_rate')
    data = jsdecode(response.data.decode('utf-8'))['data']
    assert_equals(data['friendly_name'], 'United Kingdom')
    assert_equals(data['id'], base64encode(gb_ip))
    assert_equals(data['iso_code'], 'GB')
    assert_equals(data['type'], 'vat_rate')
    assert_equals(data['vat_rate'], 2000)

    mock_function.return_value = unknown_ip

    response = patron_session.get('/vat_rate')
    data = jsdecode(response.data.decode('utf-8'))['data']
    assert_equals(data['friendly_name'], None)
    assert_equals(data['id'], base64encode(unknown_ip))
    assert_equals(data['iso_code'], None)
    assert_equals(data['type'], 'vat_rate')
    assert_equals(data['vat_rate'], None)

    mock_function.return_value = bad_ip

    response = patron_session.get('/vat_rate')
    data = jsdecode(response.data.decode('utf-8'))['data']
    assert_equals(data['friendly_name'], None)
    assert_equals(data['id'], base64encode(bad_ip))
    assert_equals(data['iso_code'], None)
    assert_equals(data['type'], 'vat_rate')
    assert_equals(data['vat_rate'], None)

