from flask import json
from nose.plugins.attrib import attr
from nose.tools import *

import patreon
from patreon.model.manager import campaign_mgr, activity_mgr, action_mgr, post_mgr
from test.fixtures import post_fixtures, pledge_fixtures
from test.test_helpers import get_data
from test.test_helpers.campaign_helpers import get_current_campaign_id
from test.test_helpers.post_helpers import get_drafts_for_campaign, \
    get_blank_draft_id, delete_draft, undelete_draft, is_blank_draft, save_draft, \
    publish_post, patron_post
from test.test_helpers.user_helpers import get_new_logged_in_creator, get_new_logged_in_patron
from test.test_helpers.attachment_helpers import add_attachment


@patreon.test.manage()
def test_delete_undelete_draft():
    # 1. Login as a creator
    creator, session = get_new_logged_in_creator()

    # 2. Hit /post to get the post_id.
    post_id = get_blank_draft_id(session)

    # 3. Save text_only post
    save_draft(post_id, session)

    # 4. Get current campaign_id.
    campaign_id = get_current_campaign_id(session)

    # 5. Delete draft.
    deleted_draft_id = delete_draft(post_id, session)
    assert_equals(post_id, deleted_draft_id)

    # 6. Get the creator's drafts.
    drafts_list = get_drafts_for_campaign(campaign_id, session)
    assert_equal(len(drafts_list), 0)

    # 7. Undelete draft.
    undeleted_draft = undelete_draft(post_id, session)
    undeleted_draft_id = int(undeleted_draft.get('id'))
    assert_equal(undeleted_draft_id, post_id)

    # 8. Get the creator's drafts.
    drafts_list_2 = get_drafts_for_campaign(campaign_id, session)
    assert_equal(len(drafts_list_2), 1)


@patreon.test.manage()
def test_there_are_no_drafts():
    # 1. Login as a creator
    creator, session = get_new_logged_in_creator()

    # 2. Get current campaign_id.
    campaign_id = get_current_campaign_id(session)

    # 3. Get the creator's drafts.
    drafts_list = get_drafts_for_campaign(campaign_id, session)
    assert_equal(drafts_list, [])

    # 4. Hit /post.
    new_post_response = session.get('/posts/new')
    new_post = new_post_response.data_obj['data']
    assert_true(is_blank_draft(new_post))

    # 5. Check that there are still no drafts.
    drafts_list_2 = get_drafts_for_campaign(campaign_id, session)
    assert_equal(drafts_list_2, [])


@patreon.test.manage()
def test_drafts_are_not_blank():
    def assert_id_unseen(draft_id):
        assert_not_in(draft_id, seen_draft_ids)
        seen_draft_ids.append(draft_id)

    # 1. Login as a creator
    creator, session = get_new_logged_in_creator()

    seen_draft_ids = []

    # 2. Hit /posts/new the first time.
    draft_1_id = get_blank_draft_id(session)
    assert_id_unseen(draft_1_id)

    # 3. Save draft with title.
    save_draft(draft_1_id, session)

    # 4. Hit /posts/new the second time.
    draft_2_id = get_blank_draft_id(session)
    assert_id_unseen(draft_2_id)

    # 5. Attach a file.
    add_attachment(draft_2_id, session)

    # 6. Hit /posts/new the third time.
    draft_3_id = get_blank_draft_id(session)
    assert_id_unseen(draft_3_id)

    # 3. Get the creator's drafts.
    campaign_id = get_current_campaign_id(session)
    drafts_list = get_drafts_for_campaign(campaign_id, session)
    assert_equal(len(drafts_list), 2)


@patreon.test.manage()
def test_edit_draft():
    # 1. Login as a creator
    creator, session = get_new_logged_in_creator()

    # 2. Hit /post to get the post_id.
    post_id = get_blank_draft_id(session)

    # 3. Save text_only post.
    save_draft(post_id, session)

    # 4. Save another text_only post.
    post = save_draft(post_id, session)

    assert_is_none(post.get('image'))
    assert_is_none(post.get('thumbnail'))
    assert_is_none(post.get('post_file'))
    assert_is_none(post.get('embed'))


@patreon.test.manage()
def test_edit_published_post():
    # 1. Login as a creator
    creator, session = get_new_logged_in_creator()

    # 2. Hit /post to get the post_id.
    post_id = get_blank_draft_id(session)

    # 3. Publish text_only post.
    publish_post(post_id, session)

    # 4. Edit published post.
    post_response_2 = session.post('/posts/' + str(post_id), post_fixtures.text_only_data())
    assert_equal(post_response_2.status_code, 200)


@patreon.test.manage()
def test_get_draft_by_id():
    # 1. Login as a creator
    creator, session = get_new_logged_in_creator()

    # 2. Get the post by ID
    post_id = get_blank_draft_id(session)
    response = session.get('/posts/' + str(post_id))
    assert_equals(response.status_code, 200)
    assert_equals(get_data(response).get('id'), str(post_id))


@patreon.test.manage()
def test_error_cases():
    creator, creator_session = get_new_logged_in_creator()
    patron, patron_session = get_new_logged_in_patron()
    campaign_id = campaign_mgr.try_get_campaign_id_by_user_id_hack(creator.user_id)

    response = patron_session.get('/posts/new')
    assert_equals(response.status_code, 404)

    post_id = get_blank_draft_id(creator_session)
    response = patron_session.get('/posts/' + str(post_id))
    assert_equals(response.status_code, 200)

    response = patron_session.get('/campaigns/' + str(campaign_id) + '/drafts')
    assert_equals(response.status_code, 403)

    activity_mgr.delete_activity(post_id)

    response = patron_session.post('/posts/' + str(post_id) + '/undelete', {})
    assert_equals(response.status_code, 403)


@patreon.test.manage()
def test_blank_drafts():
    # 1. Log in as a creator.
    creator, session = get_new_logged_in_creator()

    # 2. Hit /post to get the post_id.
    post_id_1 = get_blank_draft_id(session)

    # 3. Hit /post to get the post_id.
    post_id_2 = get_blank_draft_id(session)

    # 4. Assert they are the same draft.
    assert_equals(post_id_1, post_id_2)


@patreon.test.manage()
@attr(speed="slower")
def test_get_post():
    # Make creator & creator's post
    creator, creator_session = get_new_logged_in_creator()
    post_id = get_blank_draft_id(creator_session)
    publish_post(post_id, creator_session, post_fixtures.link_data())

    # Fetching post as creator returns complete post JSON
    response = creator_session.get('posts/{}?json-api-version=1.0'.format(post_id))
    assert_equal(response.status_code, 200)
    response_dict = json.loads(response.data)
    assert_equal(response_dict['data']['type'], 'post')
    assert_equal(response_dict['data']['id'], '1')
    assert_equal(response_dict['data']['attributes']['content'], 'This is post content.')

    # Make a patron.
    # Fetching post as a patron not pledging to creator returns incomplete post JSON
    patron, patron_session = get_new_logged_in_patron()
    response = patron_session.get('posts/{}?json-api-version=1.0'.format(post_id))
    assert_equal(response.status_code, 200)
    response_dict = json.loads(response.data)
    assert_equal(response_dict['data']['type'], 'post')
    assert_equal(response_dict['data']['id'], '1')
    assert_not_in('content', response_dict['data']['attributes'])

    # Make the patron pledge to the creator.
    # Now, fetching post should return complete post JSON
    post = post_mgr.get_post(post_id)
    pledge_fixtures.create_pledge(patron.user_id, creator.user_id, amount=post.min_cents_pledged_to_view)
    response = patron_session.get('posts/{}?json-api-version=1.0'.format(post_id))
    assert_equal(response.status_code, 200)
    response_dict = json.loads(response.data)
    assert_equal(response_dict['data']['type'], 'post')
    assert_equal(response_dict['data']['id'], '1')
    assert_equal(response_dict['data']['attributes']['content'], 'This is post content.')

    # Have the patron publish a post to the creator's community feed
    campaign_id = campaign_mgr.try_get_campaign_id_by_user_id_hack(creator.user_id)
    post_dict = patron_post(campaign_id, creator_session)
    post_id = post_dict['id']

    # The creator should be able to see this post
    response = creator_session.get('posts/{}?json-api-version=1.0'.format(post_id))
    assert_equal(response.status_code, 200)
    response_dict = json.loads(response.data)
    assert_equal(response_dict['data']['type'], 'post')
    assert_equal(response_dict['data']['id'], '2')
    assert_equal(response_dict['data']['attributes']['content'], 'This is post content.')

    # The patron should be able to see this post
    response = patron_session.get('posts/{}?json-api-version=1.0'.format(post_id))
    assert_equal(response.status_code, 200)
    response_dict = json.loads(response.data)
    assert_equal(response_dict['data']['type'], 'post')
    assert_equal(response_dict['data']['id'], '2')
    assert_equal(response_dict['data']['attributes']['content'], 'This is post content.')


@patreon.test.manage()
@attr(speed="toxic")
def test_get_post_likes():
    # Make creator & creator's post
    creator, creator_session = get_new_logged_in_creator()
    post_id = get_blank_draft_id(creator_session)
    publish_post(post_id, creator_session, post_fixtures.link_data())

    # Fetching the creator's post should show it has not yet been liked
    response = creator_session.get('posts/{}?json-api-version=1.0'.format(post_id))
    response_dict = json.loads(response.data)
    assert_equal(response_dict['data']['relationships']['likes']['data'], [])

    # Make a patron who does not pledge to the creator
    patron, patron_session = get_new_logged_in_patron()

    # Fetching the creator's post should show no info about whether it's been liked
    response = patron_session.get('posts/{}?json-api-version=1.0'.format(post_id))
    response_dict = json.loads(response.data)
    assert_not_in('likes', response_dict['data']['relationships'])

    # Make the patron pledge to the creator
    post = post_mgr.get_post(post_id)
    pledge_fixtures.create_pledge(patron.user_id, creator.user_id, amount=post.min_cents_pledged_to_view)

    # Now, fetching the creator's post (as the patron) should show info about whether it's been liked
    response = patron_session.get('posts/{}?json-api-version=1.0'.format(post_id))
    response_dict = json.loads(response.data)
    assert_equal(response_dict['data']['relationships']['likes']['data'], [])

    # Have the patron like the post
    action_mgr.like_activity(patron.user_id, post_id)

    # Now the post should have a like in its response, but still no next page link
    response = creator_session.get('posts/{}?json-api-version=1.0'.format(post_id))
    response_dict = json.loads(response.data)
    assert_equal(response_dict['data']['relationships']['likes']['data'], [{'type': 'like', 'id': '1-2-1'}])
    assert_not_in('next', response_dict['data']['relationships']['likes']['links'])

    # Have another patron like the post
    patron_2, _ = get_new_logged_in_patron()
    action_mgr.like_activity(patron_2.user_id, post_id)

    # Now the post should have two likes, still no next page link
    response = creator_session.get('posts/{}?json-api-version=1.0'.format(post_id))
    response_dict = json.loads(response.data)
    assert_equal(response_dict['data']['relationships']['likes']['data'], [{'type': 'like', 'id': '1-3-1'},
                                                                           {'type': 'like', 'id': '1-2-1'}])
    assert_not_in('next', response_dict['data']['relationships']['likes']['links'])

    # Have a third patron like the post
    patron_3, _ = get_new_logged_in_patron()
    action_mgr.like_activity(patron_3.user_id, post_id)

    # Now the post should have the most recent two likes, and a next page link
    response = creator_session.get('posts/{}?json-api-version=1.0'.format(post_id))
    response_dict = json.loads(response.data)
    assert_equal(response_dict['data']['relationships']['likes']['data'], [{'type': 'like', 'id': '1-4-1'},
                                                                           {'type': 'like', 'id': '1-3-1'}])
    assert_in('next', response_dict['data']['relationships']['likes']['links'])
