from flask import json
from nose.plugins.attrib import attr
from nose.tools import *
import patreon
from patreon.model.manager import action_mgr, campaign_mgr, activity_mgr, post_mgr
from test.fixtures import post_fixtures, pledge_fixtures
from test.fixtures.activity_fixtures import create_post
from test.test_helpers.user_helpers import get_new_logged_in_creator, get_new_logged_in_patron
from test.test_helpers.post_helpers import get_drafts_for_campaign, \
    get_blank_draft_id, delete_draft, undelete_draft, is_blank_draft, save_draft, \
    publish_post, patron_post


@patreon.test.manage()
def test_edit_like_route():
    creator, session = get_new_logged_in_creator()
    campaign_id = campaign_mgr.try_get_campaign_id_by_user_id_hack(creator.user_id)
    post = create_post(creator.user_id, campaign_id)

    resp = session.post('/likes', {
        'type': 'notlike'
    })
    assert_equals(resp.status_code, 400)
    resp = session.post('/likes', {
        'type': 'like',
        'links': {
            'post': {
                'linkage': {
                    'type': 'notpost',
                    'id': post.activity_id
                }
            }
        }
    })
    assert_equals(resp.status_code, 400)

    # Create Cases
    valid_data = {
        'type': 'like',
        'links': {
            'post': {
                'linkage': {
                    'type': 'post',
                    'id': post.activity_id
                }
            }
        }
    }
    assert_equals(action_mgr.get_like(user_id=creator.user_id, activity_id=post.activity_id), None)
    assert_equals(activity_mgr.get_activity(post.activity_id).like_count, 0)
    resp = session.post('/likes', valid_data)
    assert_equals(resp.status_code, 201)
    assert_equals(activity_mgr.get_activity(post.activity_id).like_count, 1)
    assert_true(action_mgr.get_like(user_id=creator.user_id, activity_id=post.activity_id) is not None)
    resp = session.post('/likes', valid_data)
    assert_equals(resp.status_code, 409)
    assert_equals(activity_mgr.get_activity(post.activity_id).like_count, 1)
    assert_true(action_mgr.get_like(user_id=creator.user_id, activity_id=post.activity_id) is not None)

    # Delete Cases
    resp = session.delete('/likes', data=valid_data)
    assert_equals(resp.status_code, 204)
    assert_equals(activity_mgr.get_activity(post.activity_id).like_count, 0)
    assert_equals(action_mgr.get_like(user_id=creator.user_id, activity_id=post.activity_id), None)

    resp = session.delete('/likes', data=valid_data)
    assert_equals(resp.status_code, 409)
    assert_equals(action_mgr.get_like(user_id=creator.user_id, activity_id=post.activity_id), None)
    assert_equals(activity_mgr.get_activity(post.activity_id).like_count, 0)


@patreon.test.manage()
@attr(speed="toxic")
def test_get_like_route():
    creator, creator_session = get_new_logged_in_creator()
    post_id = get_blank_draft_id(creator_session)
    publish_post(post_id, creator_session, post_fixtures.link_data())

    patron, _ = get_new_logged_in_patron()
    post = post_mgr.get_post(post_id)
    pledge_fixtures.create_pledge(patron.user_id, creator.user_id, amount=post.min_cents_pledged_to_view)
    action_mgr.like_activity(patron.user_id, post_id)
    patron_2, _ = get_new_logged_in_patron()
    pledge_fixtures.create_pledge(patron_2.user_id, creator.user_id, amount=post.min_cents_pledged_to_view)
    action_mgr.like_activity(patron_2.user_id, post_id)

    response = creator_session.get('posts/{}?json-api-version=1.0'.format(post_id))
    response_dict = json.loads(response.data)
    first_link = response_dict['data']['relationships']['likes']['links']['first']
    assert_not_in('next', response_dict['data']['relationships']['likes']['links'])

    patron_3, patron_3_session = get_new_logged_in_patron()
    response = patron_3_session.get(first_link[len('https://localhost/'):] + '&json-api-version=1.0')
    assert_equal(response.status_code, 403)

    pledge_fixtures.create_pledge(patron_3.user_id, creator.user_id, amount=post.min_cents_pledged_to_view)
    action_mgr.like_activity(patron_3.user_id, post_id)

    response = creator_session.get('posts/{}?json-api-version=1.0'.format(post_id))
    response_dict = json.loads(response.data)
    next_link = response_dict['data']['relationships']['likes']['links']['next']

    response = patron_3_session.get(first_link[len('https://localhost/'):] + '&json-api-version=1.0')
    response_dict = json.loads(response.data)
    data = response_dict['data']
    assert_equal(len(data), 2)
    assert_equal(data[0]['id'], '1-4-1')
    assert_equal(data[0]['type'], 'like')
    assert_equal(data[0]['relationships']['user']['data'], {'id': '4', 'type': 'user'})
    assert_equal(data[1]['id'], '1-3-1')
    assert_equal(data[1]['type'], 'like')
    assert_equal(data[1]['relationships']['user']['data'], {'id': '3', 'type': 'user'})

    action_route_next_link = response_dict['links']['next']
    assert_equal(next_link, action_route_next_link)

    response = patron_3_session.get(next_link[len('https://localhost/'):] + '&json-api-version=1.0')
    response_dict = json.loads(response.data)
    data = response_dict['data']
    assert_equal(len(data), 1)
    assert_equal(data[0]['id'], '1-2-1')
    assert_equal(data[0]['type'], 'like')
    assert_equal(data[0]['relationships']['user']['data'], {'id': '2', 'type': 'user'})
