import time
from unittest.mock import patch
from nose.plugins.attrib import attr

from nose.tools import *
import patreon
from patreon.constants.cards import STRIPE_PREFIX, PAYPAL_PREFIX
from patreon.model.manager import complete_pledge_mgr, new_pledge_mgr, pledge_mgr, campaign_mgr
from patreon.model.table import PledgesDelete
from test.fixtures.card_fixtures import create_card
from test.fixtures.reward_fixtures import create_reward
from test.fixtures.user_fixtures import creator
from test.test_helpers.pledge_helpers import assert_pledge_does_not_exist, \
    post_pledge, delete_pledge
from test.test_helpers.user_helpers import get_new_logged_in_patron

GB_IP = '85.159.213.251'
US_IP = '128.101.101.101'
card_id_1 = STRIPE_PREFIX + '1'
card_id_2 = STRIPE_PREFIX + '2'
card_id_3 = PAYPAL_PREFIX + '3'


@patreon.test.manage()
@patch('patreon.app.api.routes.pledge.get_ip', return_value=GB_IP)
def test_create_pledge_endpoint(mock_function):
    creator_id = creator().user_id
    campaign_id = campaign_mgr.try_get_campaign_id_by_user_id_hack(creator_id)
    new_reward_id = create_reward(creator_id, 100).reward_id
    patron_user, patron_session = get_new_logged_in_patron()
    patron_id = patron_user.user_id

    assert_pledge_does_not_exist(patron_id, creator_id, campaign_id)

    create_card(patron_id, card_id_1)

    post_pledge(patron_session, creator_id, new_reward_id, card_id_1)

    pledge = pledge_mgr.get(patron_id, creator_id)
    new_pledge = new_pledge_mgr.get_pledge_by_patron_and_campaign(patron_id, campaign_id)
    assert_is_not_none(pledge)
    assert_is_not_none(new_pledge)
    assert_equals(1000, pledge.amount)
    assert_equals(2000, pledge.pledge_cap)
    assert_equals(new_reward_id, pledge.reward_id)
    assert_equals(pledge.amount, new_pledge.amount_cents)
    assert_equals(pledge.pledge_cap, new_pledge.pledge_cap_amount_cents)
    assert_equals(pledge.reward_id, new_pledge.reward_tier_id)
    assert_equals(new_pledge.pledge_vat_location.geolocation_country, 'GB')

    # Check that patron pays fees value is set to 0 (False)
    # even though the field is not included in the dictionary
    assert_equals(pledge.PatronPaysFees, 0)
    assert_equals(new_pledge.patron_pays_fees, 0)


@patreon.test.manage()
@patch('patreon.app.api.routes.pledge.get_ip', return_value=GB_IP)
def test_create_pledge_endpoint_with_patron_pays_fees(mock_function):
    creator_id = creator().user_id
    campaign_id = campaign_mgr.try_get_campaign_id_by_user_id_hack(creator_id)
    new_reward_id = create_reward(creator_id, 100).reward_id
    patron_user, patron_session = get_new_logged_in_patron()
    patron_id = patron_user.user_id

    assert_pledge_does_not_exist(patron_id, creator_id, campaign_id)

    create_card(patron_id, card_id_1)

    post_pledge(patron_session, creator_id, new_reward_id, card_id_1, patron_pays_fees=1)

    pledge = pledge_mgr.get(patron_user.user_id, creator_id)
    new_pledge = new_pledge_mgr.get_pledge_by_patron_and_campaign(patron_id, campaign_id)
    assert_is_not_none(pledge)
    assert_is_not_none(new_pledge)
    assert_equals(1000, pledge.amount)
    assert_equals(2000, pledge.pledge_cap)
    assert_equals(new_reward_id, pledge.reward_id)
    assert_equals(pledge.amount, new_pledge.amount_cents)
    assert_equals(pledge.pledge_cap, new_pledge.pledge_cap_amount_cents)
    assert_equals(pledge.reward_id, new_pledge.reward_tier_id)
    assert_equals(new_pledge.pledge_vat_location.geolocation_country, 'GB')
    assert_equals(pledge.PatronPaysFees, 1)
    assert_equals(new_pledge.patron_pays_fees, 1)


@patreon.test.manage()
@patch('patreon.app.api.routes.pledge.get_ip', return_value=GB_IP)
@attr(speed="slower")
def test_update_pledge_endpoint(mock_function):
    # Create a pledge
    creator_id = creator().user_id
    campaign_id = campaign_mgr.try_get_campaign_id_by_user_id_hack(creator_id)
    new_reward_id = create_reward(creator_id, 200).reward_id
    patron_user, patron_session = get_new_logged_in_patron()
    patron_id = patron_user.user_id

    assert_pledge_does_not_exist(patron_id, creator_id, campaign_id)

    create_card(patron_id, card_id_1)
    create_card(patron_id, card_id_2)

    post_pledge(patron_session, creator_id, new_reward_id, card_id_1)

    time.sleep(1)  # right now things explode in tblHistory if you edit a pledge twice in one second :-(

    # Update the pledge
    post_pledge(
        patron_session,
        creator_id,
        new_reward_id,
        amount_cents=220,
        pledge_cap=440,
        card_id=card_id_2
    )

    # Get the pledge from the old and new pledge managers. Check that both were updated.
    pledge = pledge_mgr.get(patron_id, creator_id)
    new_pledge = new_pledge_mgr.get_pledge_by_patron_and_campaign(patron_id, campaign_id)
    assert_is_not_none(pledge)
    assert_is_not_none(new_pledge)
    assert_equals(220, pledge.amount)
    assert_equals(440, pledge.pledge_cap)
    assert_equals(new_reward_id, pledge.reward_id)
    assert_equals(pledge.amount, new_pledge.amount_cents)
    assert_equals(pledge.pledge_cap, new_pledge.pledge_cap_amount_cents)
    assert_equals(pledge.reward_id, new_pledge.reward_tier_id)
    assert_equals(new_pledge.pledge_vat_location.geolocation_country, 'GB')

    # Check that the new pledge manager has the deleted entry, representing the initial pledge.
    historic_pledges = new_pledge_mgr.historic_pledges_by_patron_and_campaign(patron_id, campaign_id).all()
    assert_equal(historic_pledges[1].deleted_at, new_pledge.created_at)

    # Check that patron pays fees value is set to 0 (False)
    # even though the field is not included in the dictionary
    assert_equals(pledge.PatronPaysFees, 0)
    assert_equals(new_pledge.patron_pays_fees, 0)


@patreon.test.manage()
@patch('patreon.app.api.routes.pledge.get_ip', return_value=GB_IP)
@attr(speed="slower")
def test_update_pledge_endpoint_with_patron_pays_fees(mock_function):
    # Create a pledge
    creator_id = creator().user_id
    campaign_id = campaign_mgr.try_get_campaign_id_by_user_id_hack(creator_id)
    new_reward_id = create_reward(creator_id, 200).reward_id
    patron_user, patron_session = get_new_logged_in_patron()
    patron_id = patron_user.user_id

    assert_pledge_does_not_exist(patron_id, creator_id, campaign_id)

    create_card(patron_id, card_id_1)
    create_card(patron_id, card_id_2)

    post_pledge(
        patron_session,
        creator_id,
        new_reward_id,
        card_id_1,
        amount_cents=220,
        pledge_cap=440
    )

    time.sleep(1)  # right now things explode in tblHistory if you edit a pledge twice in one second :-(

    # Update the pledge
    post_pledge(
        patron_session,
        creator_id,
        new_reward_id,
        card_id_1,
        amount_cents=220,
        pledge_cap=440,
        patron_pays_fees=1
    )

    # Get the pledge from the old and new pledge managers. Check that both were updated.
    pledge = pledge_mgr.get(patron_user.user_id, creator_id)
    new_pledge = new_pledge_mgr.get_pledge_by_patron_and_campaign(patron_id, campaign_id)
    assert_is_not_none(pledge)
    assert_is_not_none(new_pledge)
    assert_equals(220, pledge.amount)
    assert_equals(440, pledge.pledge_cap)
    assert_equals(new_reward_id, pledge.reward_id)
    assert_equals(pledge.amount, new_pledge.amount_cents)
    assert_equals(pledge.pledge_cap, new_pledge.pledge_cap_amount_cents)
    assert_equals(pledge.reward_id, new_pledge.reward_tier_id)
    assert_equals(new_pledge.pledge_vat_location.geolocation_country, 'GB')

    # Check that the new pledge manager has the deleted entry, representing the initial pledge.
    historic_pledges = new_pledge_mgr.historic_pledges_by_patron_and_campaign(patron_id, campaign_id).all()
    assert_equal(historic_pledges[1].deleted_at, new_pledge.created_at)

    # Check that patron pays fees value is set to 1
    assert_equals(pledge.PatronPaysFees, 1)
    assert_equals(new_pledge.patron_pays_fees, 1)


@patreon.test.manage()
@patch('patreon.app.api.routes.pledge.get_ip', return_value=GB_IP)
@attr(speed="slower")
def test_delete_pledge_okay(mock_function):
    creator_id = creator().user_id
    campaign_id = campaign_mgr.try_get_campaign_id_by_user_id_hack(creator_id)
    new_reward_id = create_reward(creator_id, 200).reward_id
    patron_user, patron_session = get_new_logged_in_patron()
    patron_id = patron_user.user_id

    assert_pledge_does_not_exist(patron_id, creator_id, campaign_id)

    create_card(patron_id, card_id_1)
    create_card(patron_id, card_id_2)
    create_card(patron_id, card_id_3)

    post_pledge(patron_session, creator_id, new_reward_id, card_id_1)

    # You're not pledging to this creator
    response = delete_pledge(patron_session, 666)
    assert_equals(response.status_code, 400)

    time.sleep(1)  # right now things explode in tblHistory if you edit a pledge twice in one second :-(

    # Update the pledge
    post_pledge(
        patron_session,
        creator_id,
        new_reward_id,
        amount_cents=220,
        pledge_cap=440,
        card_id=card_id_2
    )

    pledge = pledge_mgr.get(patron_id, creator_id)
    new_pledge = new_pledge_mgr.get_pledge_by_patron_and_campaign(patron_id, campaign_id)

    assert_is_not_none(pledge)
    assert_is_not_none(new_pledge)

    delete_pledge(patron_session, campaign_id)

    pledge = pledge_mgr.get(patron_id, creator_id)
    new_pledge = new_pledge_mgr.get_pledge_by_patron_and_campaign(patron_id, campaign_id)
    pledge_delete = PledgesDelete.get(user_id=patron_id, creator_id=creator_id)

    assert_is_none(pledge)
    assert_is_none(new_pledge)
    assert_is_not_none(pledge_delete)

    campaign = campaign_mgr.get(campaign_id)
    campaign.update({'is_nsfw': 1})

    response = post_pledge(
        patron_session,
        creator_id,
        new_reward_id,
        amount_cents=220,
        pledge_cap=440,
        card_id=card_id_3
    )

    assert_equals(response.status_code, 400)
    assert_in('NSFW', response.get_data().decode('utf-8'))


@patreon.test.manage()
@patch('patreon.app.api.routes.pledge.get_ip', return_value=GB_IP)
def test_complete_pledge_manager(mock_function):
    creator_id = creator().user_id
    campaign_id = campaign_mgr.try_get_campaign_id_by_user_id_hack(creator_id)
    new_reward_id = create_reward(creator_id, 100).reward_id
    patron_user, patron_session = get_new_logged_in_patron()
    patron_id = patron_user.user_id

    assert_pledge_does_not_exist(patron_id, creator_id, campaign_id)

    create_card(patron_id, card_id_1)

    post_pledge(patron_session, creator_id, new_reward_id, card_id_1)

    old_pledge, new_pledge = complete_pledge_mgr.get_pledge_by_patron_and_campaign_or_creator(
        patron_id=patron_id, creator_id=creator_id
    )
    new_pledge = new_pledge_mgr.get_pledge_by_patron_and_campaign(patron_id, campaign_id)
    assert_is_not_none(old_pledge)
    assert_is_not_none(new_pledge)
    assert_equals(old_pledge.amount, new_pledge.amount_cents)


@patreon.test.manage()
@patch('patreon.app.api.routes.pledge.get_ip', return_value=GB_IP)
@attr(speed="slower")
def test_delete_pledge_vat_bug(mock_function):
    creator_id = creator().user_id
    campaign_id = campaign_mgr.try_get_campaign_id_by_user_id_hack(creator_id)
    new_reward_id = create_reward(creator_id, 100).reward_id
    patron_user, patron_session = get_new_logged_in_patron()
    patron_id = patron_user.user_id

    assert_pledge_does_not_exist(patron_id, creator_id, campaign_id)

    create_card(patron_id, card_id_1)

    post_pledge(patron_session, creator_id, new_reward_id, card_id_1)

    delete_pledge(patron_session, campaign_id)

    assert_pledge_does_not_exist(patron_id, creator_id, campaign_id)

    mock_function.return_value = US_IP

    time.sleep(1)
    post_pledge(patron_session, creator_id, new_reward_id, card_id_1)

    delete_pledge(patron_session, campaign_id)

    assert_pledge_does_not_exist(patron_id, creator_id, campaign_id)
