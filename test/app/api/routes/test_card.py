from unittest.mock import patch

from nose.tools import *
import patreon
from patreon.exception.payment_errors import ExternalPaymentAPIException
from patreon.services.payment_apis import StripeAPI
from test.test_helpers.user_helpers import get_new_logged_in_patron


@patreon.test.manage()
@patch.object(StripeAPI, 'add_instrument')
def test_exceptions(mock):
    patron_user, patron_session = get_new_logged_in_patron()

    response = patron_session.post('/card', {
        'data': {
            'a': 'b'
        }
    })

    data = response.get_data().decode('utf-8')
    assert_equals(response.status_code, 400)
    assert_true("Stripe token is required." in data)

    mock.return_value = None

    response = patron_session.post('/card', {
        'data': {
            'stripe_token': 'tok_6'
        }
    })

    data = response.get_data().decode('utf-8')
    assert_equals(response.status_code, 400)
    assert_true("Unable to get customer information from token." in data)
    mock.assert_called_with('tok_6')

    mock.side_effect = ExternalPaymentAPIException('a', 'b')
    response = patron_session.post('/card', {
        'data': {
            'stripe_token': 'tok_7'
        }
    })

    data = response.get_data().decode('utf-8')
    assert_equals(response.status_code, 400)
    assert_true("stripe_exception" in data)
    mock.assert_called_with('tok_7')
