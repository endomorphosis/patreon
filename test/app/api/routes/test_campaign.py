import json
from nose.tools import *
import patreon
from patreon.model.manager import campaign_mgr
from test.fixtures.user_fixtures import creator
from test.test_helpers.user_helpers import get_new_logged_in_creator, \
    get_new_logged_in_patron, get_new_logged_in_admin_api


@patreon.test.manage()
def test_get_campaign_by_id():
    creator, session = get_new_logged_in_creator()

    campaign_id = campaign_mgr.try_get_campaign_id_by_user_id_hack(creator.user_id)

    resp = session.get('/campaigns/0')
    assert_equals(resp.status_code, 404)

    resp = session.get('/campaigns/' + str(campaign_id))
    assert_equals(resp.status_code, 200)
    assert_true(str(campaign_id) in resp.data.decode('utf-8'))


@patreon.test.manage()
def test_error_cases():
    patron, session = get_new_logged_in_patron()

    resp = session.get('/campaign/current_campaign')
    assert_equals(resp.status_code, 404)


@patreon.test.manage()
def test_get_featured_campaigns():
    patron, session = get_new_logged_in_patron()

    resp = session.get('/campaigns/featured')
    assert_equals(resp.status_code, 200)


@patreon.test.manage()
def test_approval_status():
    admin, session = get_new_logged_in_admin_api()

    creator_user = creator()
    campaign_id = creator_user.campaigns[0].campaign_id

    resp = session.get('/campaigns/approved')
    assert_equals(resp.status_code, 200)
    data = json.loads(resp.data.decode('utf-8'))
    assert_equals(0, len(data['data']))

    resp = session.put('/campaigns/{0}/qualified_status'.format(campaign_id), {
        'status': 'approved'
    })
    assert_equals(resp.status_code, 201)

    resp = session.get('/campaigns/{0}/qualified_status'.format(campaign_id))
    assert_equals(resp.status_code, 200)
    resp_json = json.loads(resp.data.decode('utf-8'))
    assert_equals('approved', resp_json['data']['status'])

    resp = session.get('/campaigns/approved')
    assert_equals(resp.status_code, 200)
    data = json.loads(resp.data.decode('utf-8'))
    assert_equals(1, len(data['data']))

    resp = session.get('/campaigns/disapproved')
    assert_equals(resp.status_code, 200)
    data = json.loads(resp.data.decode('utf-8'))
    assert_equals(0, len(data['data']))

    resp = session.put('/campaigns/{0}/qualified_status'.format(campaign_id), {
        'status': 'disapproved'
    })
    assert_equals(resp.status_code, 200)

    resp = session.get('/campaigns/disapproved')
    assert_equals(resp.status_code, 200)
    data = json.loads(resp.data.decode('utf-8'))
    assert_equals(1, len(data['data']))

    resp = session.delete('/campaigns/{0}/qualified_status'.format(campaign_id))
    assert_equals(resp.status_code, 204)

    resp = session.get('/campaigns/{0}/qualified_status'.format(campaign_id))
    assert_equals(resp.status_code, 200)
    resp_json = json.loads(resp.data.decode('utf-8'))
    assert_is_none(resp_json['data']['status'])
