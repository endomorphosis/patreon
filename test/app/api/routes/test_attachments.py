from nose.plugins.attrib import attr
from tempfile import NamedTemporaryFile
from nose.tools import *
import patreon
from patreon.model.manager import campaign_mgr
from patreon.model.type import TemporaryLocalFilename
from test.fixtures.activity_fixtures import create_post
from test.test_helpers.attachment_helpers import get_attachments, \
    add_attachment, assert_is_valid_attachment, delete_attachment, \
    add_attachment_fail, get_attachment_by_id
from test.test_helpers.post_helpers import get_blank_draft_id
from test.test_helpers.user_helpers import get_new_logged_in_creator, \
    get_new_logged_in_patron


@patreon.test.manage()
def test_create_delete_attachment():
    # 1. Login as a creator
    creator, session = get_new_logged_in_creator()

    # 2. Hit /post to get the post_id.
    post_id = get_blank_draft_id(session)

    # 3. Check that there are no attachments
    attachments_before = get_attachments(post_id, session)
    assert_equals(attachments_before, [])

    # 4. Attach a file.
    attachment = add_attachment(post_id, session)
    assert_is_valid_attachment(attachment)

    # 5. Check that the attachment is there.
    attachments_after_add = get_attachments(post_id, session)
    assert_equals(len(attachments_after_add), 1)

    # 6. Delete attachment.
    attachment_id = int(attachment.get('id'))
    deleted_attachment_id = delete_attachment(post_id, attachment_id, session)
    assert_equals(deleted_attachment_id, attachment_id)

    # 7. Check that attachments is empty.
    attachments_after_delete = get_attachments(post_id, session)
    assert_equals(attachments_after_delete, [])


@patreon.test.manage()
def test_large_attach():
    # 1. Login as a creator
    creator, session = get_new_logged_in_creator()

    # 2. Hit /post to get the post_id.
    post_id = get_blank_draft_id(session)

    with NamedTemporaryFile(suffix=".jpg", delete=False) as file, \
            TemporaryLocalFilename(file.name) as filename:
        # 3. Create good image.
        with open(filename, 'w') as file:
            file.seek(patreon.config.max_attachment_bytes - 1024)
            file.write("0")

        # 4. Add large attachment.
        response = add_attachment(post_id, session, filename)
        attachment_id = response.get('id')
        get_attachment_by_id(post_id, session, attachment_id)

        # 5. Create bad image.
        with open(filename, 'a') as file:
            for i in range(0, 1024):
                file.write("1")

        # 6. Add too large attachment.
        errors = add_attachment_fail(post_id, session, filename)
        assert_equals(errors[0].get('code_name'), 'FileTooLarge')
        assert_equals(errors[0].get('code'), 901)


@patreon.test.manage()
@attr(speed="slower")
def test_error_cases():
    creator, creator_session = get_new_logged_in_creator()
    patron, patron_session = get_new_logged_in_patron()
    campaign_id = campaign_mgr.try_get_campaign_id_by_user_id_hack(creator.user_id)

    post_id = create_post(
        creator.user_id, campaign_id, min_cents_pledged_to_view=1
    ).activity_id

    with NamedTemporaryFile(suffix=".jpg", delete=False) as file, \
            TemporaryLocalFilename(file.name) as filename:
        response = add_attachment(post_id, creator_session, filename)

    attachment_id = response.get('id')
    base_post_url = "/posts/" + str(post_id) + '/attachments'

    response = patron_session.get(base_post_url)
    assert_equals(403, response.status_code)

    with NamedTemporaryFile(suffix=".jpg", delete=False) as file, \
            TemporaryLocalFilename(file.name) as filename:
        response = patron_session.file(base_post_url, file=filename, filename=filename)
    assert_equals(403, response.status_code)

    response = patron_session.delete(base_post_url + '/' + str(attachment_id))
    assert_equals(403, response.status_code)

    response = patron_session.get(base_post_url + '/' + str(attachment_id))
    assert_equals(403, response.status_code)
