from nose.plugins.attrib import attr
from nose.tools import *
import patreon
from patreon.model.manager import campaign_mgr
from test.fixtures import comment_fixtures, pledge_fixtures, post_fixtures, user_fixtures
from test.test_helpers.user_helpers import get_new_logged_in_patron
from test.test_helpers.comment_helpers import make_comment, \
    make_images_comment, make_comment_fail, get_comment, sample_message, sample_key


@patreon.test.manage()
def test_get_comment():
    # 1. Create thread.
    creator_id = user_fixtures.creator().user_id
    campaign_id = campaign_mgr.try_get_campaign_id_by_user_id_hack(creator_id)
    post_id = post_fixtures.create_post(campaign_id, creator_id).activity_id
    comment_id = comment_fixtures.create_comment(creator_id, post_id).comment_id

    # 2. Login as a patron.
    patron, session = get_new_logged_in_patron()

    # 3. Create pledge.
    pledge_fixtures.create_pledge(patron.user_id, creator_id, amount=500)

    # 4. View comment.
    comment_response = get_comment(post_id, comment_id, session)
    assert_in(sample_message, comment_response['body'])


@patreon.test.manage()
def test_cannot_comment():
    # 1. Create thread.
    creator_id = user_fixtures.creator().user_id
    campaign_id = campaign_mgr.try_get_campaign_id_by_user_id_hack(creator_id)
    post_id = post_fixtures.create_post(campaign_id, creator_id).activity_id

    # 2. Login as a patron.
    patron, session = get_new_logged_in_patron()

    # 3. Make comment.
    error = make_comment_fail(post_id, session)[0]
    assert_equals(error['code_name'], 'PostViewForbidden')
    assert_equals(error['code'], 804)
    assert_equals(error['status'], '403')


@patreon.test.manage()
@attr(speed='slower')
def test_comment():
    # 1. Create thread.
    creator_id = user_fixtures.creator().user_id
    campaign_id = campaign_mgr.try_get_campaign_id_by_user_id_hack(creator_id)
    post_id = post_fixtures.create_post(campaign_id, creator_id).activity_id

    # 2. Login as a patron.
    patron, session = get_new_logged_in_patron()

    # 3. Create pledge.
    pledge_fixtures.create_pledge(patron.user_id, creator_id, amount=500)

    # 4. Make comment.
    comment_response = make_comment(post_id, session)
    assert_in(sample_message, comment_response['attributes']['body'])


@patreon.test.manage()
@attr(speed='slower')
def test_images_comment():
    # 1. Create thread.
    creator_id = user_fixtures.creator().user_id
    campaign_id = campaign_mgr.try_get_campaign_id_by_user_id_hack(creator_id)
    post_id = post_fixtures.create_post(campaign_id, creator_id).activity_id

    # 2. Login as a patron.
    patron, session = get_new_logged_in_patron()

    # 3. Create pledge.
    pledge_fixtures.create_pledge(patron.user_id, creator_id, amount=500)

    # 4. Make comment with images.
    comment_response = make_images_comment(post_id, session)
    assert_not_in(sample_key, comment_response['attributes']['body'])
