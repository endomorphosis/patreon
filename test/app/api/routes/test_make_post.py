from nose.tools import *
import patreon
from test.fixtures import post_fixtures
from test.test_helpers.post_helpers import assert_has_thumbnail, \
    assert_has_embed, assert_has_legacy_images, assert_has_post_file, \
    publish_post, get_blank_draft_id, upload_jpg, upload_mp3, \
    upload_thumbnail, upload_jpg_no_thumbnail
from test.test_helpers.user_helpers import get_new_logged_in_creator


@patreon.test.manage()
def test_thumbnail():
    # 1. Login as a creator
    creator, session = get_new_logged_in_creator()

    # 2. Hit /post to get the post_id.
    post_id = get_blank_draft_id(session)

    # 3. Save image and thumbnail.
    post = upload_thumbnail(post_id, session)

    assert_equals(post.get('post_type'), 'text_only')
    assert_has_thumbnail(post.get('thumbnail'))
    assert_is_none(post.get('post_file'))
    assert_is_none(post.get('embed'))


@patreon.test.manage()
def test_upload_image():
    # 1. Login as a creator
    creator, session = get_new_logged_in_creator()

    # 2. Hit /post to get the post_id.
    post_id = get_blank_draft_id(session)

    # 3. Save image and thumbnail.
    upload_jpg(post_id, session)

    # 4. Publish image post.
    post = publish_post(post_id, session, post_fixtures.image_file_data())

    assert_equals(post.get('post_type'), 'image_file')
    assert_has_legacy_images(post.get('image'))
    assert_has_thumbnail(post.get('thumbnail'))
    assert_has_post_file(post.get('post_file'))
    assert_is_none(post.get('embed'))


@patreon.test.manage()
def test_upload_image_no_thumbnail():
    # 1. Login as a creator
    creator, session = get_new_logged_in_creator()

    # 2. Hit /post to get the post_id.
    post_id = get_blank_draft_id(session)

    # 3. Save image and thumbnail.
    upload_jpg_no_thumbnail(post_id, session)

    # 4. Publish image post.
    post = publish_post(post_id, session, post_fixtures.image_file_data())

    assert_equals(post.get('post_type'), 'image_file')
    assert_has_legacy_images(post.get('image'))
    assert_has_thumbnail(post.get('thumbnail'))
    assert_has_post_file(post.get('post_file'))
    assert_is_none(post.get('embed'))


@patreon.test.manage()
def test_embed_image():
    # 1. Login as a creator
    creator, session = get_new_logged_in_creator()

    # 2. Hit /post to get the post_id.
    post_id = get_blank_draft_id(session)

    # 3. Publish image embed post.
    post = publish_post(post_id, session, post_fixtures.image_embed_data())

    assert_equals(post.get('post_type'), 'image_embed')
    assert_has_legacy_images(post.get('image'))
    assert_has_thumbnail(post.get('thumbnail'))
    assert_has_post_file(post.get('post_file'))
    assert_has_embed(post.get('embed'))


@patreon.test.manage()
def test_upload_audio():
    # 1. Login as a creator
    creator, session = get_new_logged_in_creator()

    # 2. Hit /post to get the post_id.
    post_id = get_blank_draft_id(session)

    # 3. Save audio and thumbnail.
    upload_mp3(post_id, session)

    # 4. Publish audio embed post.
    post = publish_post(post_id, session, post_fixtures.audio_file_data())

    assert_equals(post.get('post_type'), 'audio_file')
    assert_has_legacy_images(post.get('image'))
    assert_has_thumbnail(post.get('thumbnail'))
    assert_has_post_file(post.get('post_file'))
    assert_is_none(post.get('embed'))


@patreon.test.manage()
def test_video_embed():
    # 1. Login as a creator
    creator, session = get_new_logged_in_creator()

    # 2. Hit /post to get the post_id.
    post_id = get_blank_draft_id(session)

    # 3. Save thumbnail.
    upload_thumbnail(post_id, session)

    # 4. Publish video embed post.
    post = publish_post(post_id, session, post_fixtures.video_embed_data())

    assert_equals(post.get('post_type'), 'video_embed')
    assert_has_legacy_images(post.get('image'))
    assert_has_thumbnail(post.get('thumbnail'))
    assert_is_none(post.get('post_file'))
    assert_has_embed(post.get('embed'))


@patreon.test.manage()
def test_image_embed_no_thumbnail():
    # 1. Login as a creator
    creator, session = get_new_logged_in_creator()

    # 2. Hit /post to get the post_id.
    post_id = get_blank_draft_id(session)

    # 3. Publish image embed post.
    post = publish_post(post_id, session, post_fixtures.image_embed_data())

    assert_equals(post.get('post_type'), 'image_embed')
    assert_has_legacy_images(post.get('image'))
    assert_has_thumbnail(post.get('thumbnail'))
    assert_has_post_file(post.get('post_file'))
    assert_has_embed(post.get('embed'))



@patreon.test.manage()
def test_video_embed_no_thumbnail():
    # 1. Login as a creator
    creator, session = get_new_logged_in_creator()

    # 2. Hit /post to get the post_id.
    post_id = get_blank_draft_id(session)

    # 3. Publish video embed post.
    post = publish_post(post_id, session, post_fixtures.video_embed_data())

    assert_equals(post.get('post_type'), 'video_embed')
    assert_has_legacy_images(post.get('image'))
    assert_has_thumbnail(post.get('thumbnail'))
    assert_is_none(post.get('post_file'))
    assert_has_embed(post.get('embed'))


@patreon.test.manage()
def test_link():
    # 1. Login as a creator
    creator, session = get_new_logged_in_creator()

    # 2. Hit /post to get the post_id.
    post_id = get_blank_draft_id(session)

    # 3. Publish link post.
    post = publish_post(post_id, session, post_fixtures.link_data())

    assert_equals(post.get('post_type'), 'link')
    assert_has_legacy_images(post.get('image'))
    assert_has_thumbnail(post.get('thumbnail'))
    assert_is_none(post.get('post_file'))
    assert_has_embed(post.get('embed'))
