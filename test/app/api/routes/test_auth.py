from unittest.mock import patch
from nose.plugins.attrib import attr

from nose.tools import *
import patreon
from patreon.app.api import api_app
from patreon.model.manager import auth_mgr
from patreon.model.table import ForgotPassword
from patreon.services.db import db_session
from patreon.test import MockSession
from patreon.util.unsorted import jsdecode
from test.test_helpers.user_helpers import get_new_logged_in_creator
from test.fixtures.user_fixtures import patron


@patreon.test.manage()
@patch('patreon.services.facebook.me')
@attr(speed="toxic")
def test_login(mock):
    # Fixtures
    creator, session = get_new_logged_in_creator()
    creator2, session2 = get_new_logged_in_creator()
    password = 'password123'
    auth_mgr.change_password(creator.user_id, password)
    auth_mgr.change_password(creator2.user_id, '')
    mock.return_value = {
        'id': 0
    }

    # Error Cases
    resp = session.post('/login', {})
    assert_equals(400, resp.status_code)

    resp = session.post('/login', {
        'email': creator.email
    })
    assert_equals(403, resp.status_code)

    resp = session.post('/login', {
        'email': creator.email,
        'password': 'badpass'
    })
    assert_equals(403, resp.status_code)

    resp = session.post('/login', {
        'fb_access_token': 0
    })
    assert_equals(403, resp.status_code)

    resp = session.post('/login', {
        'email': creator2.email,
        'password': ''
    })
    assert_equals(403, resp.status_code)

    # Good Cases password
    resp = session.post('/login', {
        'email': creator.email,
        'password': password
    })
    assert_equals(200, resp.status_code)
    response_body = jsdecode(resp.data.decode('utf-8'))

    resp = session.get('/current_user')
    assert_equals(200, resp.status_code)
    data = jsdecode(resp.data.decode('utf-8')).get('data')
    assert_equals(data.get('email'), creator.email)

    # Good cases facebook
    facebook_id = '1234'
    creator.update({
        'FBID': facebook_id
    })
    mock.return_value = {
        'id': facebook_id
    }
    resp = session.post('/login', {
        'fb_access_token': 0
    })
    assert_equals(200, resp.status_code)

    resp = session.get('/current_user')
    assert_equals(200, resp.status_code)
    data = jsdecode(resp.data.decode('utf-8')).get('data')
    assert_equals(data.get('email'), creator.email)


@patreon.test.manage()
def test_forgot_password():
    empty_session = MockSession(api_app)

    normal_patron = patron()
    response = empty_session.post('/auth/forgot-password', data={'email': normal_patron.email})
    assert_equals(response.status_code, 200)
    forgot_password_results = ForgotPassword.query.filter_by(UID=normal_patron.user_id).all()
    assert_equal(len(forgot_password_results), 1)
    forgot_password_row = forgot_password_results[0]
    assert_is_not_none(forgot_password_row)
    assert_not_equal(len(forgot_password_row.SecretID), 0)


@patreon.test.manage()
def test_forgot_password_errors():
    empty_session = MockSession(api_app)

    response = empty_session.post('/auth/forgot-password', data={})
    assert_equals(response.status_code, 400)
    assert_equals(db_session.query(ForgotPassword).count(), 0)

    response = empty_session.post('/auth/forgot-password', data={'email': 'rando'})
    assert_equals(response.status_code, 400)
    assert_equals(db_session.query(ForgotPassword).count(), 0)

    nuked_patron = patron(is_nuked=1)
    response = empty_session.post('/auth/forgot-password', data={'email': nuked_patron.email})
    assert_equals(response.status_code, 400)
    assert_equals(db_session.query(ForgotPassword).count(), 0)

    passwordless_patron = patron()
    passwordless_patron.update({'Password': None})
    response = empty_session.post('/auth/forgot-password', data={'email': passwordless_patron.email})
    assert_equals(response.status_code, 400)
    assert_equals(db_session.query(ForgotPassword).count(), 0)
