from nose.tools import *
import patreon
from patreon.model.manager import payment_event_mgr
from patreon.util import datetime
from patreon.util.unsorted import jsdecode
from test.test_helpers.user_helpers import get_new_logged_in_admin_api


def __assert_paging_correct(result, expected_id):
    data = jsdecode(result.data.decode())

    assert_equals(len(data['data']), 1)
    element_id = data['data'][0]['id']
    next_url = data['links']['next']

    assert_in('page[cursor]=' + str(element_id), next_url)
    assert_equals(element_id, str(expected_id))

    return next_url

def __assert_empty_set(result):
    data = jsdecode(result.data.decode())
    assert_equals(len(data), 1)
    assert_equals(len(data['data']), 0)

@patreon.test.manage()
def test_payment_event_filtering():
    roughly_july_27_2015 = 1438040744

    id1 = payment_event_mgr.add_payment_event('type1', 'subtype1', {})[0]
    id2 = payment_event_mgr.add_payment_event('type1', 'subtype2', {})[0]
    id3 = payment_event_mgr.add_payment_event('type2', 'subtype3', {})[0]

    id4 = payment_event_mgr.add_payment_event('type3', 'subtype1', {},
                                              datetime.datetime.fromtimestamp(roughly_july_27_2015))[0]

    creator, session = get_new_logged_in_admin_api()

    # Test basic paging
    result = session.get('/admin/payments/events?json-api-version=1.0&page[size]=1')
    result = session.get(__assert_paging_correct(result, id3))
    result = session.get(__assert_paging_correct(result, id2))
    result = session.get(__assert_paging_correct(result, id1))
    result = session.get(__assert_paging_correct(result, id4))
    __assert_empty_set(result)

    # test time filtering
    result = session.get('/admin/payments/events?json-api-version=1.0&page[size]=1&filter[created_at][end]=1438040744')
    result = session.get(__assert_paging_correct(result, id4))
    __assert_empty_set(result)

    # test type filtering
    result = session.get('/admin/payments/events?json-api-version=1.0&page[size]=1&filter[payment_event_type]=type1')
    result = session.get(__assert_paging_correct(result, id2))
    result = session.get(__assert_paging_correct(result, id1))
    __assert_empty_set(result)
    result = session.get('/admin/payments/events?json-api-version=1.0&page[size]=1&filter[payment_event_type]=type2')
    result = session.get(__assert_paging_correct(result, id3))
    __assert_empty_set(result)
    result = session.get('/admin/payments/events?json-api-version=1.0&page[size]=1&filter[payment_event_type]=bad')
    __assert_empty_set(result)

    # test subtype filtering
    result = session.get('/admin/payments/events?json-api-version=1.0&page[size]=1&filter[payment_event_subtype]=subtype1')
    result = session.get(__assert_paging_correct(result, id1))
    result = session.get(__assert_paging_correct(result, id4))
    __assert_empty_set(result)
    result = session.get('/admin/payments/events?json-api-version=1.0&page[size]=1&filter[payment_event_subtype]=subtype2')
    result = session.get(__assert_paging_correct(result, id2))
    __assert_empty_set(result)
    result = session.get('/admin/payments/events?json-api-version=1.0&page[size]=1&filter[payment_event_subtype]=bad')
    __assert_empty_set(result)

    # test simulaneous type/subtype filtering
    result = session.get('/admin/payments/events?json-api-version=1.0&page[size]=1&filter[payment_event_type]=type1&filter[payment_event_subtype]=subtype2')
    result = session.get(__assert_paging_correct(result, id2))
    __assert_empty_set(result)
    result = session.get('/admin/payments/events?json-api-version=1.0&page[size]=1&filter[payment_event_type]=type2&filter[payment_event_subtype]=subtype3')
    result = session.get(__assert_paging_correct(result, id3))
    __assert_empty_set(result)
    result = session.get('/admin/payments/events?json-api-version=1.0&page[size]=1&filter[payment_event_type]=type2&filter[payment_event_subtype]=bad')
    __assert_empty_set(result)
    result = session.get('/admin/payments/events?json-api-version=1.0&page[size]=1&filter[payment_event_type]=bad&filter[payment_event_subtype]=subtype3')
    __assert_empty_set(result)

