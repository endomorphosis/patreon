from nose.tools import *

import patreon
from patreon.app.api import api_app, ApiResource
from patreon.app.api.resources.categories import CategoryResource
from patreon.model.type import Category
from patreon.test import MockSession
from test.test_helpers import get_data

version = ApiResource.JSON_API_DEFAULT_VERSION


@patreon.test.manage()
def test_categories_not_logged_in():
    session = MockSession(api_app)

    categories_response = session.get('/categories')
    categories_list = get_data(categories_response)

    assert_in(CategoryResource(Category.MUSIC).as_json_api_data(version), categories_list)
    assert_not_in(CategoryResource(Category.ALL).as_json_api_data(version), categories_list)


@patreon.test.manage()
def test_get_category_by_id():
    session = MockSession(api_app)

    categories_response = session.get('/categories/' + str(Category.MUSIC.category_id))
    category = get_data(categories_response)

    assert_equals(Category.MUSIC.category_id, int(category.get('id')))
    assert_equals(Category.MUSIC.name, category.get('name'))
