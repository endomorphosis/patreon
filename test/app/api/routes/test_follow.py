import time
from flask import json
from nose.plugins.attrib import attr
from nose.tools import *

import patreon
from patreon.model.manager import follow_mgr
from test.test_helpers.user_helpers import get_new_logged_in_creator, get_new_logged_in_patron


@patreon.test.manage()
@attr(speed='slower')
def test_create_follow():
    creator, creator_session = get_new_logged_in_creator()
    patron, patron_session = get_new_logged_in_patron()

    follow = follow_mgr.get(follower_id=patron.user_id, followed_id=creator.user_id)
    assert_is_none(follow)

    follow_data = {
        'data': {
            'type': 'follow',
            'relationships': {
                'follower': {'data': {'type': 'user', 'id': str(patron.user_id)}},
                'followed': {'data': {'type': 'user', 'id': str(creator.user_id)}}
            }
        }
    }
    follow_response = patron_session.post('/follow?json-api-version=1.0', follow_data, nest_json_in_data=False)
    response_dict = json.loads(follow_response.data)
    assert_equal(response_dict['data']['relationships']['follower'],
                 follow_data['data']['relationships']['follower'])
    assert_equal(response_dict['data']['relationships']['followed'],
                 follow_data['data']['relationships']['followed'])
    assert_equal(response_dict['data']['type'], 'follow')

    follow = follow_mgr.get(follower_id=patron.user_id, followed_id=creator.user_id)
    assert_is_not_none(follow)


@patreon.test.manage()
@attr(speed="slower")
def test_create_follow_errors():
    creator, creator_session = get_new_logged_in_creator()
    patron, patron_session = get_new_logged_in_patron()
    other_patron, _ = get_new_logged_in_patron()

    follow = follow_mgr.get(follower_id=patron.user_id, followed_id=creator.user_id)
    assert_is_none(follow)

    different_follower_data = {
        'data': {
            'type': 'follow',
            'relationships': {
                'follower': {'data': {'type': 'user', 'id': str(other_patron.user_id)}},
                'followed': {'data': {'type': 'user', 'id': str(creator.user_id)}}
            }
        }
    }
    different_follower_response = patron_session.post('/follow?json-api-version=1.0', different_follower_data,
                                                      nest_json_in_data=False)
    assert_equal(different_follower_response.status_code, 403)
    follow = follow_mgr.get(follower_id=patron.user_id, followed_id=creator.user_id)
    assert_is_none(follow)
    follow = follow_mgr.get(follower_id=other_patron.user_id, followed_id=creator.user_id)
    assert_is_none(follow)

    no_followed_data = {
        'data': {
            'type': 'follow',
            'relationships': {
                'follower': {'data': {'type': 'user', 'id': str(patron.user_id)}},
            }
        }
    }
    no_followed_repsonse = patron_session.post('/follow?json-api-version=1.0', no_followed_data,
                                               nest_json_in_data=False)
    assert_equal(no_followed_repsonse.status_code, 400)
    follows = follow_mgr.find_by_follower_id(patron.user_id)
    assert_equal(follows, [])

    follow_mgr.insert(follower_id=patron.user_id, followed_id=creator.user_id)
    extant_follow = follow_mgr.get(follower_id=patron.user_id, followed_id=creator.user_id)
    time.sleep(1)  # so that the following would have a different created_at date if it succeeded
    follow_already_exists_data = {
        'data': {
            'type': 'follow',
            'relationships': {
                'follower': {'data': {'type': 'user', 'id': str(patron.user_id)}},
                'followed': {'data': {'type': 'user', 'id': str(creator.user_id)}}
            }
        }
    }
    follow_already_exists_response = patron_session.post('/follow?json-api-version=1.0', follow_already_exists_data,
                                                         nest_json_in_data=False)
    assert_equal(follow_already_exists_response.status_code, 409)
    refetched_follow = follow_mgr.get(follower_id=patron.user_id, followed_id=creator.user_id)
    print(extant_follow, refetched_follow)
    assert_equal(extant_follow.created_at, refetched_follow.created_at)


@patreon.test.manage()
@attr(speed='slower')
def test_delete_follow():
    creator, creator_session = get_new_logged_in_creator()
    patron, patron_session = get_new_logged_in_patron()

    follow_mgr.insert(follower_id=patron.user_id, followed_id=creator.user_id)
    extant_follow = follow_mgr.get(follower_id=patron.user_id, followed_id=creator.user_id)

    follow_response = patron_session.delete('/follow/{}?json-api-version=1.0'.format(extant_follow.follow_id))
    response_dict = json.loads(follow_response.data)
    assert_equal(follow_response.status_code, 204)
    server = patreon.config.api_server
    assert_equal(follow_response.headers['Location'], 'https://{}/follows/{}'.format(server, extant_follow.follow_id))
    follow_data = {
        'data': {
            'type': 'follow',
            'relationships': {
                'follower': {'data': {'type': 'user', 'id': str(patron.user_id)}},
                'followed': {'data': {'type': 'user', 'id': str(creator.user_id)}}
            }
        }
    }
    assert_equal(response_dict['data']['relationships']['follower'],
                 follow_data['data']['relationships']['follower'])
    assert_equal(response_dict['data']['relationships']['followed'],
                 follow_data['data']['relationships']['followed'])
    assert_equal(response_dict['data']['type'], 'follow')

    follow = follow_mgr.get(follower_id=patron.user_id, followed_id=creator.user_id)
    assert_is_none(follow)


@patreon.test.manage()
def test_delete_follow_errors():
    patron, patron_session = get_new_logged_in_patron()

    follow_response = patron_session.delete('/follow/{}-1?json-api-version=1.0'.format(patron.user_id))
    assert_equal(follow_response.status_code, 404)

    follow_response = patron_session.delete('/follow/{}-1?json-api-version=1.0'.format(int(patron.user_id) + 1))
    assert_equal(follow_response.status_code, 403)
