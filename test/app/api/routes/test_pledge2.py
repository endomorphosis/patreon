from unittest.mock import patch
from nose.plugins.attrib import attr
from nose.tools import *

import patreon
from patreon.model.manager import campaign_mgr, complete_pledge_mgr, new_pledge_mgr, \
    payment_mgr, vat_mgr, payment_event_mgr, pledge_mgr, pledge_history_mgr, goal_mgr
from patreon.util.datetime import offset_from_time
from test.fixtures.goal_fixtures import create_goal
from test.test_helpers import get_errors
from test.test_helpers.pledge_helpers import post_pledge_2
from test.test_helpers.user_helpers import get_new_logged_in_patron
from test.fixtures.user_fixtures import creator, patron as patron_
from test.fixtures.reward_fixtures import create_reward
from test.fixtures.card_fixtures import create_card, create_stripe_card
from test.fixtures.pledge_fixtures import create_pledge
from test.fixtures import address_fixtures

GB_IP = '85.159.213.251'
US_IP = '128.101.101.101'


@patreon.test.manage()
@patch('patreon.app.api.routes.pledge.get_ip', return_value=GB_IP)
def test_create_pledge(mock_function):
    patron, session = get_new_logged_in_patron()
    patron_id = patron.user_id

    creator_id = creator().user_id
    campaign_id = campaign_mgr.try_get_campaign_id_by_user_id_hack(creator_id)

    amount_cents = 500
    pledge_cap_cents = 1000

    reward_id = create_reward(creator_id, 2).reward_id

    card_id = "cus_4YtOYR3mikFTnf"
    create_card(patron_id, card_id)

    resp = post_pledge_2(session, campaign_id, amount_cents, pledge_cap_cents, reward_id, card_id)
    assert_equals(201, resp.status_code)

    pledge = pledge_mgr.get(patron_id, creator_id)
    new_pledge = new_pledge_mgr.get_pledge_by_patron_and_campaign(patron_id, campaign_id)
    assert_is_not_none(pledge)
    assert_is_not_none(new_pledge)
    assert_equals(amount_cents, pledge.amount)
    assert_equals(pledge_cap_cents, pledge.pledge_cap)
    assert_equals(reward_id, pledge.reward_id)
    assert_equals(pledge.amount, new_pledge.amount_cents)
    assert_equals(pledge.pledge_cap, new_pledge.pledge_cap_amount_cents)
    assert_equals(pledge.reward_id, new_pledge.reward_tier_id)
    assert_equals('GB', new_pledge.pledge_vat_location.geolocation_country)
    assert_equals('GB', new_pledge.pledge_vat_location.selected_country)
    assert_equals(GB_IP, new_pledge.pledge_vat_location.ip_address)


@patreon.test.manage()
@patch('patreon.app.api.routes.pledge.get_ip', return_value=GB_IP)
def test_create_pledge_with_no_reward(mock_function):
    patron, session = get_new_logged_in_patron()
    patron_id = patron.user_id

    creator_id = creator().user_id
    campaign_id = campaign_mgr.try_get_campaign_id_by_user_id_hack(creator_id)

    amount_cents = 500
    pledge_cap_cents = 1000

    reward_id = "0"

    card_id = "cus_4YtOYR3mikFTnf"
    create_card(patron_id, card_id)

    resp = post_pledge_2(session, campaign_id, amount_cents, pledge_cap_cents, reward_id, card_id)
    assert_equals(201, resp.status_code)

    pledge = pledge_mgr.get(patron_id, creator_id)
    new_pledge = new_pledge_mgr.get_pledge_by_patron_and_campaign(patron_id, campaign_id)
    assert_is_not_none(pledge)
    assert_is_not_none(new_pledge)
    assert_equals(amount_cents, pledge.amount)
    assert_equals(pledge_cap_cents, pledge.pledge_cap)
    assert_equals(None, pledge.reward_id)
    assert_equals(pledge.amount, new_pledge.amount_cents)
    assert_equals(pledge.pledge_cap, new_pledge.pledge_cap_amount_cents)
    assert_equals(pledge.reward_id, new_pledge.reward_tier_id)
    assert_equals('GB', new_pledge.pledge_vat_location.geolocation_country)
    assert_equals('GB', new_pledge.pledge_vat_location.selected_country)
    assert_equals(GB_IP, new_pledge.pledge_vat_location.ip_address)


@patreon.test.manage()
@patch('patreon.app.api.routes.pledge.get_ip', return_value=GB_IP)
def test_invalid_card_for_pledge(mock_function):
    patron, session = get_new_logged_in_patron()
    patron_id = patron.user_id

    creator_id = creator().user_id
    campaign_id = campaign_mgr.try_get_campaign_id_by_user_id_hack(creator_id)

    amount_cents = 500
    pledge_cap_cents = 1000

    reward_id = create_reward(creator_id, 2).reward_id

    card_id = "bad_card_id"

    resp = post_pledge_2(session, campaign_id, amount_cents, pledge_cap_cents, reward_id, card_id)

    assert_equals("invalid_card", get_errors(resp)[0]["code_name"])
    assert_equals(504, get_errors(resp)[0]["code"])

    pledge = pledge_mgr.get(patron_id, creator_id)
    assert_is_none(pledge)


@patreon.test.manage()
@patch('patreon.app.api.routes.pledge.get_ip', return_value=GB_IP)
def test_invalid_pledge_cap(mock_function):
    patron, session = get_new_logged_in_patron()
    patron_id = patron.user_id

    creator_id = creator().user_id
    campaign_id = campaign_mgr.try_get_campaign_id_by_user_id_hack(creator_id)

    amount_cents = 500
    pledge_cap_cents = 600

    reward_id = create_reward(creator_id, 2).reward_id

    card_id = "cus_4YtOYR3mikFTnf"
    create_card(patron_id, card_id)

    resp = post_pledge_2(session, campaign_id, amount_cents, pledge_cap_cents, reward_id, card_id)

    assert_equals("invalid_pledge_cap", get_errors(resp)[0]["code_name"])
    assert_equals(503, get_errors(resp)[0]["code"])

    pledge = pledge_mgr.get(patron_id, creator_id)
    assert_is_none(pledge)


@patreon.test.manage()
@patch('patreon.app.api.routes.pledge.get_ip', return_value=GB_IP)
def test_mismatched_country_pledge(mock_function):
    patron, session = get_new_logged_in_patron()
    patron_id = patron.user_id

    creator_id = creator().user_id
    campaign_id = campaign_mgr.try_get_campaign_id_by_user_id_hack(creator_id)

    amount_cents = 500
    pledge_cap_cents = 1000

    reward_id = create_reward(creator_id, 2).reward_id

    card_id = "cus_4YtOYR3mikFTnf"
    create_card(patron_id, card_id)

    resp = post_pledge_2(session, campaign_id, amount_cents, pledge_cap_cents, reward_id, card_id, vat_country='FR')

    assert_equals("vat_country_mismatch", get_errors(resp)[0]["code_name"])
    assert_equals(505, get_errors(resp)[0]["code"])

    pledge = pledge_mgr.get(patron_id, creator_id)
    assert_is_none(pledge)


@patreon.test.manage()
@patch('patreon.app.api.routes.pledge.get_ip', return_value=GB_IP)
def test_reward_not_available_for_pledge(mock_function):
    patron_user, session = get_new_logged_in_patron()


    creator_id = creator().user_id
    campaign_id = campaign_mgr.try_get_campaign_id_by_user_id_hack(creator_id)

    amount_cents = 500
    pledge_cap_cents = 1000

    reward_id = create_reward(creator_id, 2, 1).reward_id
    # create a pledge that takes up the 1 available slot for this reward
    card_id = "cus_33333333333333"

    other_patron_id = patron_().user_id

    create_card(other_patron_id, card_id)
    create_pledge(other_patron_id, creator_id, 500, reward_id=reward_id, card_id="fixture")

    card_id = "cus_4YtOYR3mikFTnf"
    create_card(patron_user.user_id, card_id)

    resp = post_pledge_2(session, campaign_id, amount_cents, pledge_cap_cents, reward_id, card_id)

    assert_equals("reward_not_available", get_errors(resp)[0]["code_name"])
    assert_equals(501, get_errors(resp)[0]["code"])

    pledge = pledge_mgr.get(patron_user.user_id, creator_id)
    assert_is_none(pledge)


@patreon.test.manage()
@patch('patreon.app.api.routes.pledge.get_ip', return_value=GB_IP)
def test_insufficient_pledge_for_reward(mock_function):
    patron, session = get_new_logged_in_patron()
    patron_id = patron.user_id

    creator_id = creator().user_id
    campaign_id = campaign_mgr.try_get_campaign_id_by_user_id_hack(creator_id)

    amount_cents = 500
    pledge_cap_cents = 1000

    reward_id = create_reward(creator_id, 9999).reward_id

    card_id = "cus_4YtOYR3mikFTnf"
    create_card(patron_id, card_id)

    resp = post_pledge_2(session, campaign_id, amount_cents, pledge_cap_cents, reward_id, card_id)

    assert_equals("insufficient_pledge_for_reward", get_errors(resp)[0]["code_name"])
    assert_equals(502, get_errors(resp)[0]["code"])

    pledge = pledge_mgr.get(patron_id, creator_id)
    assert_is_none(pledge)


@patreon.test.manage()
@patch('patreon.app.api.routes.pledge.get_ip', return_value=GB_IP)
def test_big_money_required(mock_function):
    patron, session = get_new_logged_in_patron()
    patron_id = patron.user_id

    creator_id = creator().user_id
    campaign_id = campaign_mgr.try_get_campaign_id_by_user_id_hack(creator_id)

    amount_cents = pledge_cap_cents = 10000

    reward_id = create_reward(creator_id, 1).reward_id

    card_id = "cus_4YtOYR3mikFTnf"
    create_card(patron_id, card_id)

    resp = post_pledge_2(session, campaign_id, amount_cents, pledge_cap_cents, reward_id, card_id)

    assert_equals("big_money_escrow_required", get_errors(resp)[0]["code_name"])
    assert_equals(509, get_errors(resp)[0]["code"])

    pledge = pledge_mgr.get(patron_id, creator_id)
    assert_is_none(pledge)


@patreon.test.manage()
@patch('patreon.app.api.routes.pledge.get_ip', return_value=GB_IP)
@attr(speed="slower")
def test_big_money_accepted(mock_get_ip):
    patron, session = get_new_logged_in_patron()
    patron_id = patron.user_id

    creator_id = creator().user_id
    campaign_id = campaign_mgr.try_get_campaign_id_by_user_id_hack(creator_id)

    amount_cents = pledge_cap_cents = 10000

    reward_id = create_reward(creator_id, 1).reward_id

    card_id = create_stripe_card(patron_id)

    resp = post_pledge_2(session, campaign_id, amount_cents, pledge_cap_cents, reward_id, card_id, is_big_money=True)

    pledge = pledge_mgr.get(patron_id, creator_id)
    new_pledge = new_pledge_mgr.get_pledge_by_patron_and_campaign(patron_id, campaign_id)
    assert_is_not_none(pledge)
    assert_is_not_none(new_pledge)
    assert_equals(amount_cents, pledge.amount)
    assert_equals(pledge_cap_cents, pledge.pledge_cap)
    assert_equals(reward_id, pledge.reward_id)
    assert_equals(pledge.amount, new_pledge.amount_cents)
    assert_equals(pledge.pledge_cap, new_pledge.pledge_cap_amount_cents)
    assert_equals(pledge.reward_id, new_pledge.reward_tier_id)
    assert_equals('GB', new_pledge.pledge_vat_location.geolocation_country)
    assert_equals('GB', new_pledge.pledge_vat_location.selected_country)
    assert_equals(GB_IP, new_pledge.pledge_vat_location.ip_address)

    assert_equals(amount_cents + vat_mgr.calculate_vat_on_amount_for_country(amount_cents, 'GB'),
                  payment_mgr.get_credit_total_legacy(patron_id, True))

    events = payment_event_mgr.get_all_events_after_time(patreon.util.datetime.offset_from_time(minutes=-5))

    assert_equals(len(events), 2)
    first_event = events[0]
    second_event = events[1]

    assert_equals(first_event.type, 'pledge')
    assert_equals(first_event.subtype, 'vat')

    assert_equals(first_event.json_data['amount_cents'], amount_cents)
    assert_equals(first_event.json_data['user_id'], patron_id)
    assert_equals(first_event.json_data['campaign_id'], campaign_id)
    assert_equals(first_event.json_data['pledge_cap'], pledge_cap_cents)
    assert_equals(first_event.json_data['reward_id'], reward_id)
    assert_equals(first_event.json_data['vat_country'], 'GB')
    assert_equals(first_event.json_data['vat_amount'], vat_mgr.calculate_vat_on_amount_for_country(amount_cents, 'GB'))

    assert_equals(second_event.type, 'payment')
    assert_equals(second_event.subtype, 'big_money')

    assert_equals(second_event.json_data['amount_cents'],
                  amount_cents + vat_mgr.calculate_vat_on_amount_for_country(amount_cents, 'GB'))
    assert_equals(second_event.json_data['user_id'], patron_id)
    assert_equals(second_event.json_data['status'], 'succeeded')


@patreon.test.manage()
@patch('patreon.app.api.routes.pledge.get_ip', return_value=US_IP)
@attr(speed="slower")
def test_big_money_accepted_no_vat(mock_get_ip):
    patron, session = get_new_logged_in_patron()
    patron_id = patron.user_id

    creator_id = creator().user_id
    campaign_id = campaign_mgr.try_get_campaign_id_by_user_id_hack(creator_id)

    amount_cents = pledge_cap_cents = 10000

    reward_id = create_reward(creator_id, 1).reward_id

    card_id = create_stripe_card(patron_id)

    resp = post_pledge_2(
        session, campaign_id, amount_cents, pledge_cap_cents, reward_id,
        card_id, vat_country='US', is_big_money=True
    )

    pledge = pledge_mgr.get(patron_id, creator_id)
    new_pledge = new_pledge_mgr.get_pledge_by_patron_and_campaign(patron_id, campaign_id)
    assert_is_not_none(pledge)
    assert_is_not_none(new_pledge)
    assert_equals(amount_cents, pledge.amount)
    assert_equals(pledge_cap_cents, pledge.pledge_cap)
    assert_equals(reward_id, pledge.reward_id)
    assert_equals(pledge.amount, new_pledge.amount_cents)
    assert_equals(pledge.pledge_cap, new_pledge.pledge_cap_amount_cents)
    assert_equals(pledge.reward_id, new_pledge.reward_tier_id)
    assert_equals('US', new_pledge.pledge_vat_location.geolocation_country)
    assert_equals('US', new_pledge.pledge_vat_location.selected_country)
    assert_equals(US_IP, new_pledge.pledge_vat_location.ip_address)

    assert_equals(amount_cents, payment_mgr.get_credit_total_legacy(patron_id, True))


@patreon.test.manage()
@patch('patreon.app.api.routes.pledge.get_ip', return_value=US_IP)
@attr(speed="slower")
def test_maintain_limited_reward(mock_get_ip):
    patron, session = get_new_logged_in_patron()
    patron_id = patron.user_id

    creator_id = creator().user_id
    campaign_id = campaign_mgr.try_get_campaign_id_by_user_id_hack(creator_id)

    amount_cents = pledge_cap_cents = 10000

    reward_id = create_reward(creator_id, 1, UserLimit=1).reward_id

    card_id = create_stripe_card(patron_id)

    complete_pledge_mgr.create_or_update_pledge(
        patron_id, amount_cents, pledge_cap_cents, reward_id, card_id,
        campaign_id=campaign_id
    )
    pledge_history_mgr.delete_patron_history(patron_id)

    resp = post_pledge_2(
        session, campaign_id, amount_cents + 500, pledge_cap_cents + 500,
        reward_id, card_id, vat_country='US', is_big_money=True
    )

    assert_equals(resp.status_code, 201)

    pledge = pledge_mgr.get(patron_id, creator_id)
    new_pledge = new_pledge_mgr.get_pledge_by_patron_and_campaign(patron_id, campaign_id)
    assert_is_not_none(pledge)
    assert_is_not_none(new_pledge)
    assert_equals(new_pledge.amount_cents, amount_cents + 500)
    assert_equals(reward_id, new_pledge.reward_tier_id)


@patreon.test.manage()
@patch('patreon.app.api.routes.pledge.get_ip', return_value=US_IP)
def test_big_money_fails(mock_get_ip):
    patron, session = get_new_logged_in_patron()
    patron_id = patron.user_id

    creator_id = creator().user_id
    campaign_id = campaign_mgr.try_get_campaign_id_by_user_id_hack(creator_id)

    amount_cents = pledge_cap_cents = 10000

    reward_id = create_reward(creator_id, 1).reward_id

    card_id = 'cus_cat'
    create_card(patron_id, card_id)

    resp = post_pledge_2(
        session, campaign_id, amount_cents, pledge_cap_cents, reward_id,
        card_id, vat_country='US', is_big_money=True
    )
    assert_equals(resp.data_obj['errors'][0]['code_name'], 'generic_big_money_charge_error')
    assert_equals(resp.data_obj['errors'][0]['code'], 431)

    events = payment_event_mgr.get_events_of_type_after_time(
        offset_from_time(minutes=-5), 'payment'
    )

    assert_equals(len(events), 1)
    big_money_pledge = events[0]

    assert_equals(big_money_pledge.type, 'payment')
    assert_equals(big_money_pledge.subtype, 'big_money')

    assert_equals(big_money_pledge.json_data['amount_cents'], amount_cents)
    assert_equals(big_money_pledge.json_data['user_id'], patron_id)
    assert_equals(big_money_pledge.json_data['status'], 'failed')


@patreon.test.manage()
@patch('patreon.app.api.routes.pledge.get_ip', return_value=US_IP)
def test_address_required(mock_get_ip):
    patron, session = get_new_logged_in_patron()
    patron_id = patron.user_id

    creator_id = creator().user_id
    campaign_id = campaign_mgr.try_get_campaign_id_by_user_id_hack(creator_id)

    amount_cents = pledge_cap_cents = 1000

    reward_id = create_reward(creator_id, 1, Shipping=1).reward_id

    card_id = 'cus_cat'
    create_card(patron_id, card_id)

    resp = post_pledge_2(
        session, campaign_id, amount_cents, pledge_cap_cents, reward_id,
        card_id, vat_country='US'
    )
    assert_equals(resp.data_obj['errors'][0]['code_name'], 'shipping_address_required')
    assert_equals(resp.data_obj['errors'][0]['code'], 508)


@patreon.test.manage()
@patch('patreon.app.api.routes.pledge.get_ip', return_value=US_IP)
def test_address_accepted_when_required(mock_get_ip):
    patron, session = get_new_logged_in_patron()
    patron_id = patron.user_id

    creator_id = creator().user_id
    campaign_id = campaign_mgr.try_get_campaign_id_by_user_id_hack(creator_id)

    amount_cents = pledge_cap_cents = 1000

    reward_id = create_reward(creator_id, 1, Shipping=1).reward_id

    card_id = 'cus_cat'
    create_card(patron_id, card_id)

    resp = post_pledge_2(
        session, campaign_id, amount_cents, pledge_cap_cents, reward_id,
        card_id, vat_country='US', address=address_fixtures.patreon_address()
    )
    assert_equals(201, resp.status_code)


@patreon.test.manage()
@patch('patreon.app.api.routes.pledge.get_ip', return_value=US_IP)
def test_goal_reached(mock_get_ip):
    patron, session = get_new_logged_in_patron()
    patron_id = patron.user_id

    creator_id = creator().user_id
    campaign_id = campaign_mgr.try_get_campaign_id_by_user_id_hack(creator_id)

    amount_cents = pledge_cap_cents = 1000

    reward_id = create_reward(creator_id, 1, Shipping=1).reward_id
    goal_id = create_goal(campaign_id, amount_cents=100, goal_title="My first goal!").goal_id

    card_id = 'cus_cat'
    create_card(patron_id, card_id)

    resp = post_pledge_2(
        session, campaign_id, amount_cents, pledge_cap_cents, reward_id,
        card_id, vat_country='US', address=address_fixtures.patreon_address()
    )
    assert_equals(201, resp.status_code)

    assert_not_equals(None, goal_mgr.get(goal_id).reached_at)
