from nose.tools import *
import patreon
from test.test_helpers import get_data
from test.test_helpers.user_helpers import get_new_logged_in_creator


@patreon.test.manage()
def test_get_by_id():
    creator, session = get_new_logged_in_creator()
    response = session.get('/user/{}'.format(creator.user_id))
    assert_equal(response.status_code, 200)
    assert_equals(get_data(response).get('type'), 'user')
    assert_equals(get_data(response).get('id'), str(creator.user_id))

    response = session.get('/user/NONEXISTENT_ID')
    assert_equal(response.status_code, 404)


@patreon.test.manage()
def test_get_by_vanity():
    creator, session = get_new_logged_in_creator()
    response = session.get('/users?filter[vanity]={}'.format(creator.vanity))
    assert_equal(response.status_code, 200)
    assert_equals(get_data(response).get('type'), 'user')
    assert_equals(get_data(response).get('id'), str(creator.user_id))

    response = session.get('/users?filter[vanity]=NONEXISTENT_VANITY')
    assert_equal(response.status_code, 404)
