import io
from nose.tools import *
from patreon.model.manager import attachment_mgr
from test import fixtures
from test.test_helpers import get_data, get_errors


def get_attachments(post_id, session):
    get_attachments_response = session.get('/posts/' + str(post_id) + '/attachments')
    return get_data(get_attachments_response)


def _add_attachment(post_id, session, filename=None):
    # Default image.
    filename = filename or fixtures.file_directory + 'blank.jpg'
    with io.open(filename, 'rb') as file:
        return session.file(
            '/posts/' + str(post_id) + '/attachments',
            file,
            filename
        )


def add_attachment_fail(post_id, session, filename=None):
    response = _add_attachment(post_id, session, filename)
    return get_errors(response)


def add_attachment(post_id, session, filename=None):
    response = _add_attachment(post_id, session, filename)
    return get_data(response)


def get_attachment_by_id(post_id, session, attachment_id):
    response = session.get('/posts/' + str(post_id) + '/attachments/' + str(attachment_id))

    attachment = attachment_mgr.get_attachment_or_throw(post_id=post_id, attachment_id=attachment_id)
    attachment_response = get_data(response)

    assert_is_valid_attachment(attachment_response)
    assert_equals(attachment.url, attachment_response.get('url'))
    assert_equals(attachment.name, attachment_response.get('name'))


def delete_attachment(post_id, attachment_id, session):
    delete_attachment_response = session.delete(
        '/posts/' + str(post_id) + '/attachments/' + str(attachment_id)
    )
    data = get_data(delete_attachment_response)
    return int(data.get('id'))


def assert_is_valid_attachment(attachment_dict):
    assert_is_not_none(attachment_dict)
    assert_is_not_none(attachment_dict.get('url'))
    assert_is_not_none(attachment_dict.get('name'))


def assert_response_has_no_content(response):
    assert_not_equals(response.headers['Content-Type'], "application/octetstream")


def assert_response_has_content(response, filename):
    assert_equals(response.headers['Content-Type'], "application/octetstream")
    assert_equals(response.headers['Content-Disposition'], 'attachment; filename="{}"'.format(filename))
    assert_greater(response.headers['Content-Length'], '5')
