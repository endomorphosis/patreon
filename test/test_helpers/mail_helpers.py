from nose.tools import *
from patreon.services import mail_file_writer


def assert_no_mandrill_template_remains(email_html):
    assert_false('*|' in email_html)
    assert_false('|*' in email_html)


def assert_unread_count(recipient_email, expected_count):
    count = mail_file_writer.unread_count(recipient_email)
    assert_equals(count, expected_count)


class AssertSendsMail():
    def __init__(self, recipient_email, expected_count):
        self.recipient_email = recipient_email
        self.expected_count = expected_count

    def __enter__(self):
        self.start_count = mail_file_writer.unread_count(self.recipient_email)

    def __exit__(self, exc_type, exc_val, exc_tb):
        end_count = mail_file_writer.unread_count(self.recipient_email)
        assert_equals(end_count - self.start_count, self.expected_count)
