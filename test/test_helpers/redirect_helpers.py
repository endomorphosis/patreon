from contextlib import contextmanager
from nose.tools import *
from patreon.routing import Redirect
from werkzeug.wrappers import Response

@contextmanager
def assert_redirects(to=None):
    try:
        yield
    except Redirect as e:
        if to is not None:
            assert_equals(e.location, to)
        return
    raise AssertionError('Expected a redirect. No redirect happened.')
