from tempfile import NamedTemporaryFile
from PIL import Image, ImageDraw


def generate_image(width, height):
    with NamedTemporaryFile(suffix=".jpg", delete=False) as file:
        image = Image.new('RGB', (width, height)) # create the image
        draw = ImageDraw.Draw(image)
        draw.rectangle([0, 0, width, height], fill='white')
        draw.ellipse((20, 20, width, height), fill='orange', outline='orange')
        image.save(file.name)
        image.close()
        return file.name
