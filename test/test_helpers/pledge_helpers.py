from nose.tools import *
from patreon.model.manager import new_pledge_mgr, pledge_mgr


def _post_pledge(session, json):
    return session.post(
        '/pledges?json-api-version=1.0', json, nest_json_in_data=False
    )


def post_pledge(session, creator_id, reward_id, card_id, amount_cents=1000,
                pledge_cap=2000, vat_country='GB', patron_pays_fees=0, address=None):
    return _post_pledge(session, pledge_json_1(
        creator_id, reward_id, amount_cents, pledge_cap, vat_country,
        card_id, patron_pays_fees=patron_pays_fees, address=address
    ))


def post_pledge_2(session, campaign_id, amount_cents, pledge_cap_cents,
                  reward_id, card_id, vat_country='GB', is_big_money=False,
                  patron_pays_fees=0, address=None):
    return _post_pledge(session, pledge_json_2(
        campaign_id, amount_cents, pledge_cap_cents, reward_id, card_id,
        vat_country, is_big_money, patron_pays_fees, address
    ))


def delete_pledge(session, campaign_id):
    return session.delete('/pledges/{}?json-api-version=1.0'.format(campaign_id))


def assert_pledge_does_not_exist(patron_id, creator_id, campaign_id):
    pledge = pledge_mgr.get(patron_id, creator_id)
    new_pledge = new_pledge_mgr.get_pledge_by_patron_and_campaign(patron_id, campaign_id)

    assert_is_none(pledge)
    assert_is_none(new_pledge)


def _get_attributes_dict(amount, pledge_cap, vat_country, is_big_money, patron_pays_fees):
    attributes = {
        'amount_cents': amount,
        'pledge_cap_cents': pledge_cap,
        'vat_country': vat_country,
        'patron_pays_fees': patron_pays_fees
    }

    if is_big_money:
        attributes['accepted_big_money_charge'] = True

    return attributes


def _get_address_dict(address):
    return {
        'line_1': address.address_line_1,
        'line_2': address.address_line_2,
        'postal_code': address.address_zip,
        'city': address.city,
        'state': address.state,
        'country': address.country
    }


def pledge_json_1(user_id, RID, amount=1000, pledge_cap=2000, vat_country='GB',
                  card_id='1', is_big_money=False, patron_pays_fees=0,
                  address=None):
    json = {
        'data': {
            'type': 'pledge',
            'attributes': _get_attributes_dict(
                amount, pledge_cap, vat_country, is_big_money, patron_pays_fees
            ),
            'relationships': {
                'creator': {'data': {'type': 'user', 'id': user_id}},
                'reward': {'data': {'type': 'reward', 'id': RID}},
                'card': {'data': {'type': 'card', 'id': card_id}},
            },
        },
        'included': []
    }

    if address:
        json['data']['relationships']['address'] = {
            'data': {
                'type': 'address',
                'id': 'address-id'
            }
        }
        json['included'].append({
            'type': 'address',
            'id': "address-id",
            'attributes': _get_address_dict(address)
        })

    return json


def pledge_json_2(campaign_id, amount, pledge_cap, reward_id, card_id,
                  vat_country='GB', is_big_money=False, patron_pays_fees=0,
                  address=None):
    json = {
        'data': {
            'type': 'pledge',
            'attributes': _get_attributes_dict(
                amount, pledge_cap, vat_country, is_big_money, patron_pays_fees
            ),
            'relationships': {
                'campaign': {'data': {'type': 'campaign', 'id': str(campaign_id)}},
                'reward': {'data': {'type': 'reward', 'id': str(reward_id)}},
                'card': {'data': {'type': 'card', 'id': str(card_id)}},
                'address': {'data': {'type': 'address', 'id': "address-id"}}
            }
        },
        'included': []
    }

    if address:
        json['data']['relationships']['address'] = {
            'data': {
                'type': 'address',
                'id': 'address-id'
            }
        }
        json['included'].append({
            'type': 'address',
            'id': "address-id",
            'attributes': _get_address_dict(address)
        })

    return json

