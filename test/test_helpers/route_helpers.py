new_post        = "/posts/new"
post            = "/posts/{post_id}"
post_file       = "/posts/{post_id}/post_file"
thumbnail       = "/posts/{post_id}/thumbnail"
undelete_post   = "/posts/{post_id}/undelete"
campaign_drafts = "/campaigns/{campaign_id}/drafts"
campaign_posts  = "/campaigns/{campaign_id}/posts"
patron_post     = "/campaigns/{campaign_id}/post"


def new_post_url():
    return new_post


def post_url(post_id):
    return post.format(post_id=post_id)


def post_file_url(post_id):
    return post_file.format(post_id=post_id)

def thumbnail_url(post_id):
    return thumbnail.format(post_id=post_id)


def post_undelete_url(post_id):
    return undelete_post.format(post_id=post_id)


def patron_post_url(campaign_id):
    return patron_post.format(campaign_id=campaign_id)


def campaign_drafts_url(campaign_id):
    return campaign_drafts.format(campaign_id=campaign_id)


def campaign_posts_url(campaign_id):
    return campaign_posts.format(campaign_id=campaign_id)
