import io
from nose.tools import assert_equals
from patreon.model.table import Comment
from test import fixtures
from test.test_helpers import get_data, get_errors

sample_image = 'blank.jpg'
sample_key = 'om5J9qwoGZSpu2uPhWZ3'
sample_message = "!"


def structured_comments_to_ids(comment_tree_root):
    output = []
    for comment_tree in comment_tree_root:
        if isinstance(comment_tree, Comment):
            output.append(comment_tree.comment_id)
        elif isinstance(comment_tree, list):
            output.append(structured_comments_to_ids(comment_tree))
        elif comment_tree is None:
            pass
        else:
            raise ValueError("Invalid type in the comment list:", comment_tree)
    return output


def assert_page_comment_is(comment_page, page_i, comment_list, list_i):
    assert_equals(
        comment_page.members()[page_i].model.comment_text,
        comment_list[list_i].comment_text
    )


def assert_page_equals_comments(page, comments):
    """Turn a list of comment models into their comment text for easier comparison."""
    actual_comment_texts = [member.model.Comment for member in page.members()]
    expected_comment_texts = [comment.Comment for comment in comments]
    assert_equals(actual_comment_texts, expected_comment_texts)


def multipart_many_files(endpoint, data, session):
    filename = fixtures.file_directory + sample_image

    files_dict = {
        sample_image: io.open(filename, 'rb')
        for _ in range(2)
    }

    response = session.files(
        endpoint,
        files_dict,
        data=data
    )

    data = get_data(response)

    for _ in files_dict:
        files_dict[_].close()

    return data


def get_comment(post_id, comment_id, session, data=None):
    endpoint = "/posts/{post_id}/comments/{comment_id}".format(
        post_id=post_id, comment_id=comment_id
    )
    return get_data(session.get(endpoint))


def _make_comment(post_id, session, data=None):
    comment = {
        'attributes': {
            'body': sample_message
        },
        'relationships': {
            'post': {
                'data': {
                    'type': 'post',
                    'id': post_id
                }
            }
        }
    }
    if data:
        comment.update(data)

    return session.post('/comments?json-api-version=1.0', comment)


def make_comment_fail(post_id, session, data=None):
    return get_errors(_make_comment(post_id, session, data))


def make_comment(post_id, session, data=None):
    return get_data(_make_comment(post_id, session, data))


def make_images_comment(post_id, session, data=None):
    comment = {
        'attributes': {
            'body': sample_message + sample_key
        },
        'relationships': {
            'post': {
                'data': {
                    'type': 'post',
                    'id': post_id
                }
            }
        },
        'images': {
             sample_key: sample_image
        }
    }

    if data:
        comment.update(data)

    return multipart_many_files('/images_comments?json-api-version=1.0', comment, session)


def make_comment_on_thread(post_id, thread_id, session, data=None):
    if not data:
        data = {}
    data['thread_id'] = thread_id

    return make_comment(post_id, session, data)


def make_images_comment_on_thread(post_id, thread_id, session, data=None):
    if not data:
        data = {}
    data['thread_id'] = thread_id

    return make_images_comment(post_id, session, data)
