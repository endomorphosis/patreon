from nose.tools import *
from patreon.app.api import api_app
from patreon.test import MockSession
from test.fixtures import user_fixtures
from test.fixtures.session_fixtures import create_web_session


def get_new_logged_in_creator():
    creator = user_fixtures.creator()
    session = MockSession(api_app)

    login_response = session.post('/login', {
        'email': creator.email,
        'password': 'password'
    })
    assert_equal(login_response.status_code, 200)
    assert_false(session.is_admin())
    return creator, session


def get_new_logged_in_patron():
    patron = user_fixtures.patron()
    session = MockSession(api_app)

    login_response = session.post('/login', {
        'email': patron.email,
        'password': 'password'
    })
    assert_equal(login_response.status_code, 200)
    assert_false(session.is_admin())
    return patron, session


def get_new_logged_in_admin_api():
    patron = user_fixtures.patron()
    patron.update({
        'is_admin': 1
    })
    session = MockSession(api_app)

    login_response = session.post('/login', {
        'email': patron.email,
        'password': 'password'
    })
    assert_equal(login_response.status_code, 200)
    assert_true(session.is_admin())
    return patron, session


def get_new_logged_in_admin_patron():
    patron = user_fixtures.patron()
    patron.update({
        'is_admin': 1
    })

    session = create_web_session(patron)
    assert_true(session.is_admin())

    return patron, session

def get_new_logged_in_admin_creator():
    creator = user_fixtures.creator()
    creator.update({
        'is_admin': 1
    })

    session = create_web_session(creator)
    assert_true(session.is_admin())

    return creator, session

