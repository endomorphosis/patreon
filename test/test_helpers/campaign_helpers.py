from nose.tools import assert_equals
from patreon.model.manager import campaign_mgr, category_mgr, goal_mgr, reward_mgr
from test.test_helpers import get_data


def get_current_campaign_id(session):
    current_campaign_response = session.get('/campaign/current_campaign')
    campaign = get_data(current_campaign_response)
    return campaign.get('id')


def assert_user_has_n_categories(user_id, n):
    assert_equals(category_mgr.find_by_user_id(user_id).count(), n)


def assert_campaign_has_n_goals(campaign_id, n):
    assert_equals(len(goal_mgr.find_by_campaign_id(campaign_id)), n)


def assert_creator_has_n_rewards(creator_id, n):
    assert_equals(len(reward_mgr.find_by_creator_id(creator_id)), n)


def assert_first_category_is(user_id, category_id):
    category = category_mgr.find_by_user_id(user_id)[0]
    assert_equals(category.category_id, category_id)


def assert_goal_is(goal, title, text, amount):
    assert_equals(goal.goal_title, title)
    assert_equals(goal.goal_text, text)
    assert_equals(goal.amount_cents, amount)


def assert_reward_is(reward, amount, requires_shipping, description, user_limit):
    assert_equals(reward.amount, amount)
    assert_equals(reward.requires_shipping, requires_shipping)
    assert_equals(reward.description, description)
    assert_equals(reward.user_limit, user_limit)


def assert_campaign_data(campaign_id, summary, video_url, video_embed):
    campaign = campaign_mgr.get(campaign_id)

    assert_equals(campaign.summary, summary)
    assert_equals(campaign.main_video_url, video_url)
    assert_equals(campaign.main_video_embed, video_embed)


def assert_thanks_data(campaign_id, message, thanks_url, thanks_embed):
    campaign = campaign_mgr.get(campaign_id)

    assert_equals(campaign.thanks_msg, message)
    assert_equals(campaign.thanks_video_url, thanks_url)
    assert_equals(campaign.thanks_embed, thanks_embed)


def create_campaign(user_id, first_name="Marcos", last_name="Gaeta",
                    one_liner="Line", pay_per_name="Epoch",
                    creation_name="Thing", is_monthly=True, is_nsfw=False,
                    image_url="img.png", image_small_url="pic.png",
                    category_ids_raw=None):
    if not category_ids_raw:
        category_ids_raw = [1,2,3]

    campaign_mgr.create_or_update_campaign(
        user_id, first_name, last_name, one_liner, pay_per_name, creation_name,
        is_monthly, is_nsfw, image_url, image_small_url, category_ids_raw
    )
