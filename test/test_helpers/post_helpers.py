import io
import os
from nose.tools import *
from patreon.model.manager import make_post_mgr, post_mgr
from patreon.model.type import PostType
from test import fixtures
from test.fixtures.post_fixtures import text_only_data, text_only_patron_post_data
from test.test_helpers import get_data, assert_is_isoformat, image_helpers, route_helpers


def upload_mp3_no_thumbnail(post_id, session):
    filename = fixtures.file_directory + 'blank.mp3'
    with io.open(filename, 'rb') as file:
        upload_mp3_response = session.file(
            route_helpers.post_file_url(post_id),
            file,
            filename
        )
        return get_data(upload_mp3_response)


def upload_mp3(post_id, session):
    return [
        upload_mp3_no_thumbnail(post_id, session),
        upload_thumbnail(post_id, session)
    ]


def upload_jpg_no_thumbnail(post_id, session):
    filename = fixtures.file_directory + 'blank.jpg'
    with io.open(filename, 'rb') as file:
        upload_jpg_response = session.file(
            route_helpers.post_file_url(post_id),
            file,
            filename
        )
        return get_data(upload_jpg_response)


def upload_jpg(post_id, session):
    return [
        upload_jpg_no_thumbnail(post_id, session),
        upload_thumbnail(post_id, session)
    ]


def upload_thumbnail(post_id, session, top_offset=None):
    filename = image_helpers.generate_image(400, 400)
    with io.open(filename, 'rb') as file:
        upload_jpg_response = session.file(
            route_helpers.thumbnail_url(post_id),
            file,
            filename,
            data={ 'top_offset': top_offset }
        )
    os.remove(filename)
    return get_data(upload_jpg_response)


def save_draft(post_id, session, data=None):
    post = text_only_data()
    if data:
        post.update(data)
    post_response = session.post(route_helpers.post_url(post_id), post)
    return get_data(post_response)


def publish_post(post_id, session, data=None):
    if not data:
        data = {}
    data.update({ 'tags': { 'publish': True } })
    return save_draft(post_id, session, data)


def patron_post(campaign_id, session, data=None):
    post = text_only_patron_post_data()
    if data:
        post.update(data)
    post_response = session.post(
        route_helpers.patron_post_url(campaign_id),
        post
    )
    return get_data(post_response)


def edit_post(post_id, session, data):
    post_response = session.post(route_helpers.post_url(post_id), data)
    return get_data(post_response)


def edit_post_hack(post_id, user_id, post_type=None, is_paid=True,
                min_cents_pledged_to_view=None):
    data = text_only_data()
    if not post_type:
        post_type = PostType.TEXT_ONLY

    if min_cents_pledged_to_view is None:
        min_cents_pledged_to_view = data['min_cents_pledged_to_view']

    return make_post_mgr.save_post(
        post_id=post_id,
        user_id=user_id,
        post_type=post_type,
        title=data['title'],
        content_html=data['content'],
        thumbnail=None,
        embed_raw=None,
        min_cents_pledged_to_view=min_cents_pledged_to_view,
        category=data['category'],
        is_paid=is_paid,
        will_publish=True
    )


def get_drafts_for_campaign(campaign_id, session):
    get_drafts_response = session.get(route_helpers.campaign_drafts_url(campaign_id))
    return get_data(get_drafts_response)


def get_blank_draft_id(session):
    new_post_response = session.get(route_helpers.new_post_url())
    new_post = get_data(new_post_response)
    assert_true(is_blank_draft(new_post))
    return int(new_post.get('id'))


def delete_draft(post_id, session):
    delete_draft_response = session.delete(route_helpers.post_url(post_id))
    data = get_data(delete_draft_response)
    assert_is_isoformat(data.get('deleted_at'))
    return int(data.get('id'))


def undelete_draft(post_id, session):
    undelete_draft_response = session.post(route_helpers.post_undelete_url(post_id), {})
    return get_data(undelete_draft_response)


def is_blank_draft(post_dict):
    return (post_dict['title'] == None) \
        & (post_dict['content'] == None) \
        & (post_dict['image'] == None) \
        & (post_dict['thumbnail'] == None) \
        & (post_dict['embed'] == None) \
        & (post_dict['post_file'] == None) \
        & (post_dict['published_at'] == None) \
        & (post_dict['links']['attachments']['linkage'] == [])


def assert_has_legacy_images(images_dict):
    assert_is_not_none(images_dict)
    assert_is_not_none(images_dict.get('url'))
    assert_is_not_none(images_dict.get('large_url'))
    assert_is_not_none(images_dict.get('thumb_url'))


def assert_has_thumbnail(thumbnail_dict):
    assert_is_not_none(thumbnail_dict)
    assert_is_not_none(thumbnail_dict.get('url'))


def assert_has_post_file(post_file_dict):
    assert_is_not_none(post_file_dict)
    assert_is_not_none(post_file_dict.get('name'))
    assert_is_not_none(post_file_dict.get('url'))


def assert_has_embed(embed_dict):
    assert_is_not_none(embed_dict)
    assert_is_not_none(embed_dict.get('html'))
    assert_is_not_none(embed_dict.get('subject'))
    # assert_is_not_none(embed_dict.get('description'))
    assert_is_not_none(embed_dict.get('url'))
    assert_is_not_none(embed_dict.get('provider'))
    assert_is_not_none(embed_dict.get('provider_url'))


def assert_min_cents_pledge_to_view(post_id, min_cents_pledged_to_view):
    post = post_mgr.get_post(post_id)
    assert_equals(post.min_cents_pledged_to_view, min_cents_pledged_to_view)
