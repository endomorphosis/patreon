from nose.tools import assert_equal
from patreon.model.manager import reward_mgr


def assert_reward_counts_as_dict(creator_id, expected_rewards_dict):
    rewards = reward_mgr.find_by_creator_id(creator_id)
    rewards_dict = {
        reward.reward_id: reward.count
        for reward in rewards
    }
    assert_equal(expected_rewards_dict, rewards_dict)
