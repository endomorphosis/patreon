from nose.tools import *
import re

def is_status_code_ok(response):
    code = response.status_code
    return code >= 200 and code < 300


def get_errors(response):
    assert_is_not_none(response.data_obj)
    assert_false(is_status_code_ok(response))
    errors = response.data_obj.get('errors')
    assert_is_not_none(errors)
    return errors


def get_data(response):
    assert_is_not_none(response.data_obj)
    if not is_status_code_ok(response):
        errors = response.data_obj.get('errors')
        raise Exception(errors[0]['detail'] if errors else response.status_code)

    data = response.data_obj.get('data')
    assert_is_not_none(data)
    return data


def assert_is_isoformat(time_string):
    iso = re.compile(r'^\d{4}-\d{2}-\d{2}[ T]\d{2}:\d{2}:\d{2}[\.\d{6}]?$')
    assert_true(iso.search(str(time_string)))
