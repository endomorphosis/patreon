from nose.tools import *
from patreon.model.manager import follow_mgr


def assert_is_following(follower_id, followed_id):
    assert_is_not_none(follow_mgr.get(follower_id, followed_id))

def assert_is_not_following(follower_id, followed_id):
    assert_is_none(follow_mgr.get(follower_id, followed_id))


def assert_followed_are(follower_id, user_ids):
    follows = follow_mgr.find_by_follower_id(follower_id)
    assert_equal([_.followed_id for _ in follows], user_ids)


def assert_followers_are(followed_id, user_ids):
    follows = follow_mgr.find_by_followed_id(followed_id)
    assert_equal([_.follower_id for _ in follows], user_ids)
