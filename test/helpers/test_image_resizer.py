import os
from PIL import Image
from nose.tools import *

import patreon
from patreon.constants import image_sizes
from patreon.services.image_resizer import TopOffsetResizer
from patreon.model.manager import make_post_mgr
from patreon.model.type import PostType
from patreon.util.image_processing import resizer as resizer_
from test.test_helpers import image_helpers
from test.test_helpers.post_helpers import get_blank_draft_id, save_draft
from test.test_helpers.user_helpers import get_new_logged_in_creator

high_expected_sizes = {
    'old_single': (10, 400),
    'old_double': (10, 400),
    'medium': (10, 400),
    'large': (10, 400),
    'medium_2': (10, 400),
    'large_2': (10, 400)
}

wide_expected_sizes = {
    'old_single': (320, 8),
    'old_double': (400, 10),
    'medium': (400, 10),
    'large': (400, 10),
    'medium_2': (400, 10),
    'large_2': (400, 10)
}

thumbnail_expected_sizes = {
    'old_thumb': (75, 75),
    'square_small': (44, 44),
    'square_small_2': (88, 88),
    'square_large': (620, 620),
    'square_large_2': (1240, 1240),
    'huge': (600, 338),
    'small': (120, 68),
    'small_2': (240, 136),
    'large': (460, 259),
    'large_2': (920, 518)
}


def _get_resizes_and_assert(width, height, input_sizes, expected_sizes):
    image_tempfile_filename = image_helpers.generate_image(width, height)

    with TopOffsetResizer(image_tempfile_filename, input_sizes,
                          top_offset=0) as resizer:
        resizes_dict = resizer.resizes()

        for size in expected_sizes:
            filename = resizes_dict.get(size)
            im = Image.open(filename)
            width, height = im.size

            expected_width, expected_height = expected_sizes.get(size)
            assert_equal(expected_width, width)
            assert_equal(expected_height, height)

    os.remove(image_tempfile_filename)


def test_resize_tall_image():
    _get_resizes_and_assert(
        400, 10, image_sizes.POST_SIZES, wide_expected_sizes
    )


def test_resize_wide_image():
    _get_resizes_and_assert(
        10, 400, image_sizes.POST_SIZES, high_expected_sizes
    )


def test_resize_square_image():
    _get_resizes_and_assert(
        400, 400, image_sizes.POST_THUMB_SIZES, thumbnail_expected_sizes
    )


def test_calculate_offset():
    source_dimensions_list = [
        (1000,  1000),
        (100,   1000),
        (1000,  100),
        (640,   360)
    ]

    top_offset_list = [
        -1,
        0,
        279,
        280,
        10000,
        None
    ]

    expected_offsets = [
        0,  0,  401,    402,    402,    201,
        0,  0,  401,    402,   8682,   4341,
        0,  0,  0,      0,      0,      0,
        0,  0,  0,      0,      0,      0
    ]

    destination_width, destination_height = \
        image_sizes.POST_THUMB_LARGE_2.dimensions

    i = 0
    for source_dimensions in source_dimensions_list:
        source_width, source_height = source_dimensions
        for top_offset in top_offset_list:
            result = resizer_.calculate_top_offset(
                top_offset, source_width, source_height, destination_width,
                destination_height
            )
            assert_equals(expected_offsets[i], result)
            i += 1

@patreon.test.manage()
def test_handle_embed():
    # 1. Login as a creator
    creator, session = get_new_logged_in_creator()

    # 2. Hit /post to get the post_id.
    post_id = get_blank_draft_id(session)

    embed_raw = {
        "url": "https://vimeo.com/channels/staffpicks/126783354",
        "subject": "517292111_1280.jpg"
    }

    thumbnail = {
        "url": "http://i.vimeocdn.com/video/517292111_1280.jpg"
    }

    make_post_mgr._handle_embed(
        post_id,
        PostType.IMAGE_EMBED,
        embed_raw,
        thumbnail,
        None
    )

    # 4. Save another text_only post.
    save_draft(post_id, session)
