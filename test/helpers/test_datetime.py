import patreon
from nose.tools import *

import pytz
from datetime import date, timezone
import dateutil.parser

def test_datetime_at_midnight_on_date():
    today = date.today()
    utc_dt = patreon.util.datetime.datetime_at_midnight_on_date(today, pytz.timezone("UTC"))
    assert_equals(today, utc_dt.date())
    assert_equals(timezone.utc, utc_dt.tzinfo)
    assert_equals(0, utc_dt.time().hour)
    assert_equals(0, utc_dt.time().minute)
    assert_equals(0, utc_dt.time().second)

    jan_1_2015 = dateutil.parser.parse('2015-01-01T12:00:00').date()
    pst_dt = patreon.util.datetime.datetime_at_midnight_on_date(jan_1_2015, pytz.timezone("America/Los_Angeles"))
    assert_equals(jan_1_2015, pst_dt.date())
    assert_equals(timezone.utc, pst_dt.tzinfo)
    assert_equals(8, pst_dt.time().hour)
    assert_equals(0, pst_dt.time().minute)
    assert_equals(0, pst_dt.time().second)

def test_beginning_of_this_month():
    today = date.today()
    # there is some narrowly conceivable possibility that this test might fail
    # because the current time crossed a day boundary between the previous line
    # and the next.
    dt = patreon.util.datetime.beginning_of_this_month()
    assert_equals(today.year, dt.year)
    assert_equals(today.month, dt.month)
    assert_equals(1, dt.day)
    assert_equals(0, dt.time().hour)
    assert_equals(0, dt.time().minute)
    assert_equals(0, dt.time().second)
