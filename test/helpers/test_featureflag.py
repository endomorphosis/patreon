from nose.plugins.attrib import attr
from nose.tools import *
import patreon
from patreon.model.manager import feature_flag
from test.fixtures.session_fixtures import create_web_session
from test.fixtures.user_fixtures import patron


@patreon.test.manage()
@attr(speed="slower")
def test_feature_flag():
    feature = feature_flag.publicly_visible_feature_flags()[0]

    patrons = patron(), patron(), patron()
    session1, session2, session3 = [create_web_session(u) for u in patrons]
    patron_id1, patron_id2, patron_id3 = [patron.user_id for patron in patrons]

    session1.get('/')
    session2.get('/')
    session3.get('/')

    feature_flag.set_percentage_enabled(feature, 0.0)
    feature_flag.set_global_disabled_state(feature, False)

    assert_equal(feature_flag.is_enabled(feature, user_id=patron_id1), False)
    assert_equal(feature_flag.is_enabled(feature, user_id=patron_id2), False)
    assert_equal(feature_flag.is_enabled(feature, user_id=patron_id3), False)
    assert_equal(feature_flag.is_user_enabled(feature, patron_id1), False)

    feature_flag.set_user_enabled(feature, patron_id1)

    assert_equal(feature_flag.is_enabled(feature, user_id=patron_id1), True)
    assert_equal(feature_flag.is_enabled(feature, user_id=patron_id2), False)
    assert_equal(feature_flag.is_enabled(feature, user_id=patron_id3), False)
    assert_equal(feature_flag.is_user_enabled(feature, patron_id1), True)

    feature_flag.remove_user_enabled(feature, patron_id1)

    assert_equal(feature_flag.is_enabled(feature, user_id=patron_id1), False)
    assert_equal(feature_flag.is_enabled(feature, user_id=patron_id2), False)
    assert_equal(feature_flag.is_enabled(feature, user_id=patron_id3), False)
    assert_equal(feature_flag.is_user_enabled(feature, patron_id1), False)

    feature_flag.set_percentage_enabled(feature, 1.0)

    assert_equal(feature_flag.is_enabled(feature, user_id=patron_id1), True)
    assert_equal(feature_flag.is_enabled(feature, user_id=patron_id2), True)
    assert_equal(feature_flag.is_enabled(feature, user_id=patron_id3), True)
    assert_equal(feature_flag.is_user_enabled(feature, patron_id1), False)

    feature_flag.set_percentage_enabled(feature, 0.0)

    assert_equal(feature_flag.is_enabled(feature, user_id=patron_id1), False)
    assert_equal(feature_flag.is_enabled(feature, user_id=patron_id2), False)
    assert_equal(feature_flag.is_enabled(feature, user_id=patron_id3), False)
    assert_equal(feature_flag.is_user_enabled(feature, patron_id1), False)

    # TODO: Percentage testing will be inaccurate until we have a
    # better way of testing probabilistic things.


@patreon.test.manage()
def test_feature_flag_global_disabled():
    feature = feature_flag.publicly_visible_feature_flags()[0]

    patrons = patron(), patron()
    session1, session2 = [create_web_session(u) for u in patrons]
    patron_id1, patron_id2 = [patron.user_id for patron in patrons]

    feature_flag.set_percentage_enabled(feature, 0.0)
    feature_flag.set_global_disabled_state(feature, True)

    assert_equal(feature_flag.is_globally_disabled(feature), True)
    assert_equal(feature_flag.is_enabled(feature, user_id=patron_id1), False)
    assert_equal(feature_flag.is_enabled(feature, user_id=patron_id2), False)

    feature_flag.set_percentage_enabled(feature, 1.0)
    assert_equal(feature_flag.is_enabled(feature, user_id=patron_id1), False)
    assert_equal(feature_flag.is_enabled(feature, user_id=patron_id2), False)

    feature_flag.set_user_enabled(feature, session1.user_id)
    assert_equal(feature_flag.is_enabled(feature, user_id=patron_id1), False)
    assert_equal(feature_flag.is_enabled(feature, user_id=patron_id2), False)


@patreon.test.manage()
def test_feature_flag_enabled_features_for_user():
    session = create_web_session(patron())

    feature_flag.remove_user_enabled("dummy_test", session.user_id)
    feature_flag.remove_user_enabled("vatmoss", session.user_id)

    assert_equal([], feature_flag.enabled_features_for_user(session))
    assert_equal("", feature_flag.enabled_features_for_user_as_string(session))

    feature_flag.set_user_enabled("dummy_test", session.user_id)
    assert_equal(["dummy_test"], feature_flag.enabled_features_for_user(session))
    assert_equal("dummy_test", feature_flag.enabled_features_for_user_as_string(session))

    feature_flag.set_user_enabled("vatmoss", session.user_id)
    assert_equal(["vatmoss", "dummy_test"], feature_flag.enabled_features_for_user(session))
    assert_equal("vatmoss, dummy_test", feature_flag.enabled_features_for_user_as_string(session))


@patreon.test.manage()
def test_exception_list():
    feature = feature_flag.publicly_visible_feature_flags()[0]
    feature_flag.set_global_disabled_state(feature, False)
    feature_flag.set_percentage_enabled(feature, 0.999999)

    # excepted accounts shouldnt be in test groups at < 100%
    for user_id in feature_flag.EXCEPTED_ACCOUNTS:
        assert_false(feature_flag.is_enabled(feature, user_id=user_id))

    feature_flag.set_percentage_enabled(feature, 1.0)
    for user_id in feature_flag.EXCEPTED_ACCOUNTS:
        assert_true(feature_flag.is_enabled(feature, user_id=user_id))
