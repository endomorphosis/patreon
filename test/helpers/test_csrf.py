import calendar
import datetime
import patreon

from patreon.app.web import web_app
from patreon.model.manager.csrf import *
from nose.tools import *


def get_timestamp_delta(minutes=0, hours=0):
    timedelta = datetime.datetime.utcnow() - datetime.timedelta(minutes=minutes, hours=hours)
    return calendar.timegm(timedelta.timetuple()) * 1000


@patreon.test.manage()
def test_csrf_cycle():
    user_id = 1
    uri = '/'

    token = get_user_secret(user_id)
    ticket = get_csrf_ticket(uri, user_id, token)
    request_time = ticket['time']
    signature = ticket['token']
    assert_true(not is_invalid_csrf_token(user_id, token, request_time, uri, signature))
    token = get_user_secret(user_id)
    assert_true(is_invalid_csrf_token(user_id, token, request_time, uri, signature))


@patreon.test.manage()
def test_different_secrets():
    secret1 = get_user_secret('1')
    secret2 = get_user_secret('1')
    assert_true(secret1 != secret2)


@patreon.test.manage()
def test_referrer_check():
    referrer = 'https://patreon.com/test'
    uri1 = 'test'
    uri2 = 'https://patreon.com/test'
    uri3 = 'bar'
    assert_true(not is_invalid_referrer(referrer, uri1))
    assert_true(not is_invalid_referrer(referrer, uri2))
    assert_true(is_invalid_referrer(referrer, uri3))


@patreon.test.manage()
def test_expiration():
    datetime1 = get_timestamp_delta(minutes=15)
    datetime2 = get_timestamp_delta(hours=5)

    assert_true(not is_expired(datetime1))
    assert_true(is_expired(datetime2))


@patreon.test.manage()
def test_csrf_exceptions():
    uri1 = '/'
    uri2 = 'processSignup'
    uri3 = '/processSignup'

    assert_true(not is_web_csrf_protection_disabled(uri1))
    assert_true(is_web_csrf_protection_disabled(uri2))
    assert_true(is_web_csrf_protection_disabled(uri3))


@patreon.test.manage()
def test_csrf_logging():
    with web_app.test_request_context('/dummy', method='POST'):
        log_csrf_event('fake-reason', '', '', '')
