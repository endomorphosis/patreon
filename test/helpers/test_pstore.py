import patreon

from patreon.services import db
from patreon.services.pstore import PStore
from nose.tools import *

@patreon.test.manage()
def test_pstore():

    pstore1 = PStore(db)
    pstore2 = PStore(db)

    assert_equal(pstore1.get('hey'), None)
    assert_equal(pstore2.get('hey'), None)
    pstore1.set('hey', 'listen')

    # Test values are updated on .update().
    assert_equal(pstore1.get('hey'), 'listen')
    assert_equal(pstore2.get('hey'), None)
    pstore2.update()
    assert_equal(pstore2.get('hey'), 'listen')

    # Test values are updated on .set().
    pstore2.set('hey', 'is for horses')
    assert_equal(pstore1.get('hey'), 'listen')
    pstore1.set('hay', 'is more accurately for horses')
    assert_equal(pstore1.get('hey'), 'is for horses')

    pstore2.set('nothing', None)
    assert_equal(pstore2.get('hay'), 'is more accurately for horses')

    # Make sure new pstores are up to date as well.
    pstore3 = PStore(db)
    assert_equal(pstore3.get('hey'), 'is for horses')
    assert_equal(pstore3.get('hay'), 'is more accurately for horses')