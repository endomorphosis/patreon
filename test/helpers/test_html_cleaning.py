import itertools
import patreon
from patreon.util.html_cleaner import angular_escape
from nose.tools import *

@patreon.test.manage()
def test_angular_escaping():
    # Base cases
    case0 = "blah"
    case1 = "{blah}"
    case2 = "{blah"
    case3 = "blah}"
    case4 = "{{blah"
    case5 = "blah}}"
    case6 = "{{blah}}"

    assert_equal(angular_escape(case0), case0)
    assert_equal(angular_escape(case1), case1)
    assert_equal(angular_escape(case2), case2)
    assert_equal(angular_escape(case3), case3)
    assert_equal(angular_escape(case4), case0)
    assert_equal(angular_escape(case5), case0)
    assert_equal(angular_escape(case6), case0)


@patreon.test.manage()
def test_angular_escaping_encoded():
    ampersands = ['&', '&amp;', '&#26;', '&#38;']
    lclubs = ['#123;', '#x7b;', 'lclub;']
    rclubs = ['#125;', '#x7d;', 'rclub;']

    lclub_perm = []
    rclub_perm = []

    for ampersand in ampersands:
        for lclub in lclubs:
            lclub_perm.append(ampersand+lclub)
        for rclub in rclubs:
            rclub_perm.append(ampersand+rclub)

    lclub_doubles = list(itertools.product(lclub_perm, lclub_perm))
    rclub_doubles = list(itertools.product(rclub_perm, rclub_perm))

    for lclub in lclub_doubles:
        assert_equal(angular_escape(lclub[0] + lclub[1]), '')

    for rclub in rclub_doubles:
        assert_equal(angular_escape(rclub[0] + rclub[1]), '')


