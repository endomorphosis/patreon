from nose.tools import assert_equals
import patreon
from patreon.util.unsorted import split_name


def _assert_splits_to(name, expected_first, expected_last):
    actual_first, actual_last = split_name(name)
    assert_equals(expected_first, actual_first)
    assert_equals(expected_last, actual_last)


@patreon.test.manage()
def test_split_name():
    _assert_splits_to(None,                 "", "")
    _assert_splits_to("",                   "", "")
    _assert_splits_to("Marcos",             "Marcos", "")
    _assert_splits_to(" Marcos",            "Marcos", "")
    _assert_splits_to("Marcos ",            "Marcos", "")
    _assert_splits_to("Marcos Gaeta",       "Marcos", "Gaeta")
    _assert_splits_to(" Marcos Gaeta",      "Marcos", "Gaeta")
    _assert_splits_to("Marcos Gaeta ",      "Marcos", "Gaeta")
    _assert_splits_to("Marcos The Gaeta",   "Marcos", "The Gaeta")
