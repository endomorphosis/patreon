import patreon

from nose.tools import *
from unittest import mock

def mock_row(id, number):
    row = mock.Mock()
    row.id = id
    row.number = number
    return row

@patreon.test.manage()
def test_order_by_list_of_ids():
    objects = [
                mock_row(1,3),
                mock_row(2,2),
                mock_row(3,1)
            ]
    ordered_by_id = patreon.util.model.order_by_list_of_ids([3,1,2], objects)
    assert_equals([objects[2], objects[0], objects[1]], ordered_by_id)

    ordered_by_number = patreon.util.model.order_by_list_of_ids([3,1,2], objects, "number")
    assert_equals([objects[0], objects[2], objects[1]], ordered_by_number)
