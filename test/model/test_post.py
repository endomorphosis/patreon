from nose.plugins.attrib import attr
from nose.tools import *
import patreon
from patreon.model.manager import activity_mgr, rewards_give_mgr, campaign_mgr
from test.fixtures.pledge_fixtures import create_pledge
from test.fixtures.post_fixtures import create_post, image_embed_data, create_patron_post
from test.fixtures.user_fixtures import creator, patron
from test.test_helpers.mail_helpers import AssertSendsMail
from test.test_helpers.post_helpers import get_blank_draft_id, publish_post, \
    assert_has_embed, edit_post_hack, assert_min_cents_pledge_to_view
from test.test_helpers.user_helpers import get_new_logged_in_creator


@patreon.test.manage()
def test_delete_post():
    # 1. Create creator.
    creator_id = creator().user_id

    # 2. Create pledging patrons.
    patron_count = 2
    for _ in range(patron_count):
        patron_id = patron().user_id
        create_pledge(patron_id=patron_id, creator_id=creator_id)

    # 3. Publish a post.
    campaign_id = campaign_mgr.try_get_campaign_id_by_user_id_hack(creator_id)
    post = create_post(campaign_id, creator_id)
    post_id = post.activity_id

    # 4. There are two pending bills.
    bills = rewards_give_mgr.get_bills_for_post(post_id)
    assert_equals(len(bills), patron_count)

    # 5. Delete the published post.
    activity_mgr.delete_activity(post_id)

    # 6. There are no pending bills.
    bills = rewards_give_mgr.get_bills_for_post(post_id)
    assert_equals(len(bills), 0)


@patreon.test.manage()
@attr(speed="toxic")
def test_degenerate_embedly_cases():
    creator, session = get_new_logged_in_creator()

    difficult_urls = [
        "http://nightattack.tv/episode/67",
        "http://jurytalks.com/blog/2015/6/3/interview-w-doxie",
        "http://carasantamaria.com/podcast/jaclyn-glenn",
        "http://dresdencodak.com/2015/06/01/dark-science-47/",
        "http://carlolson.tv/dcp-162-jonah-guelzo-sound-designer-mixer-editor/",
        "http://www.multiplenerdgasm.com/117/",
        "http://www.beerarmy.org/beerarmyengaged/team-punisher",
        "https://www.fimfiction.net/story/268320/1/unconventional-meetups"
        "/pruning-in-the-spring",
        "https://www.fimfiction.net/story/40907/13/in-which-masterweaver-just"
        "-makes-stuff-up/this-chapter-is-to-catch-the-characters-up-with-the-"
        "plot-which-totally-exists"
    ]

    for url in difficult_urls:
        # 1. Hit /post to get the post_id.
        post_id = get_blank_draft_id(session)

        # 2. Publish link post.
        data = image_embed_data()
        data['embed']['url'] = url
        data['embed']['subject'] = "name"
        data['embed']['image_url'] = None
        post = publish_post(post_id, session, data)

        # 3. Assert that we got an image.
        assert_has_embed(post.get('embed'))


@patreon.test.manage()
def test_notify_new_post():
    # 1. Create creator.
    creator_email = "creator@patreon.com"
    creator_id = creator(creator_email).user_id
    campaign_id = campaign_mgr.try_get_campaign_id_by_user_id_hack(creator_id)

    # 2. Create patron.
    patron_email = "patron@patreon.com"
    patron_id = patron(patron_email).user_id

    # 3. Pledge to creator.
    create_pledge(patron_id, creator_id, amount=1000)

    # 4. Patron post.
    with AssertSendsMail(creator_email, 1):
        create_patron_post(campaign_id, patron_id)

    # 5. Patron post as creator.
    with AssertSendsMail(patron_email, 1):
        create_patron_post(campaign_id, creator_id)

    # 6. Creator Post.
    with AssertSendsMail(patron_email, 1):
        create_post(campaign_id, creator_id)


@patreon.test.manage()
def test_notify_patrons_on_can_view():
    # 1. Create creator.
    creator_id = creator().user_id
    campaign_id = campaign_mgr.try_get_campaign_id_by_user_id_hack(creator_id)

    # 2. Create patron with low pledge.
    patron_low_email = "patron_low@patreon.com"
    low_pledge = 100
    patron_low_id = patron(patron_low_email).user_id
    create_pledge(patron_low_id, creator_id, low_pledge)

    # 3. Create patron with high pledge.
    patron_high_email = "patron_high@patreon.com"
    high_pledge = 1000
    patron_high_id = patron(patron_high_email).user_id
    create_pledge(patron_high_id, creator_id, high_pledge)

    # 4. Patron post as creator.
    with AssertSendsMail(patron_low_email, 0), \
         AssertSendsMail(patron_high_email, 1):
        post_id = create_post(
            campaign_id, creator_id, min_cents_pledged_to_view=high_pledge
        ).activity_id

    assert_min_cents_pledge_to_view(post_id, high_pledge)

    # 5. Edit post.
    with AssertSendsMail(patron_low_email, 1), \
         AssertSendsMail(patron_high_email, 0):
        edit_post_hack(post_id, creator_id, min_cents_pledged_to_view=low_pledge)

    assert_min_cents_pledge_to_view(post_id, low_pledge)
