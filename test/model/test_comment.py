from nose.plugins.attrib import attr
import patreon
from patreon.model.manager import campaign_mgr, comment_mgr
from patreon.services.db import test_dirty_table
from patreon.util import datetime
from test.fixtures import comment_fixtures, pledge_fixtures
from test.fixtures.user_fixtures import creator, patron
from test.fixtures.post_fixtures import create_post
from test.test_helpers.comment_helpers import structured_comments_to_ids
from test.test_helpers.mail_helpers import AssertSendsMail
from nose.tools import *


@patreon.test.manage()
def test_structured_comments():
    user_id = 1
    post_id = 10

    # HACK that ticks the clock forward after each comment
    def insert_comment(comment_text, thread_id=None):
        insert_comment.current_time += datetime.timedelta(1)
        return comment_fixtures.insert_comment(
            user_id, post_id, comment_text, thread_id,
            insert_comment.current_time
        ).comment_id
    insert_comment.current_time = datetime.datetime.now()

    comment_a       = insert_comment('a')
    comment_aa      = insert_comment('aa',      comment_a)
    comment_aaa     = insert_comment('aaa',     comment_aa)
    comment_aaaa    = insert_comment('aaaa',    comment_aaa)
    comment_ab      = insert_comment('ab',      comment_a)
    comment_b       = insert_comment('b')
    comment_ba      = insert_comment('ba',      comment_b)
    comment_bb      = insert_comment('bb',      comment_b)

    _, structure_nest_recent = comment_mgr.get_structured_comments_for_post(post_id)
    structure = structured_comments_to_ids(structure_nest_recent)
    # Structure should match visual representation of nested comments.
    assert_equals(comment_a, 1)
    assert_equals(structure, [
        [comment_b, [
            [comment_bb],
            [comment_ba]
        ]],
        [comment_a, [
            [comment_ab],
            [comment_aa, [
                [comment_aaa, [
                    [comment_aaaa]
                ]]
            ]]
        ]]
    ])

    _, structure_flat_recent = comment_mgr.get_structured_comments_for_post(post_id, flattened=True)
    structure = structured_comments_to_ids(structure_flat_recent)
    # All comments above depth 2 are flattened to depth 2.
    assert_equals(structure, [
        [comment_b, [
            [comment_ba],
            [comment_bb]]],
        [comment_a, [
            [comment_aa],
            [comment_aaa],
            [comment_aaaa],
            [comment_ab]]]
    ])


@patreon.test.manage()
@attr(speed="slower")
def test_comments_send_mail():
    creator_email = "creator@patreon.com"
    patron_email = "patron@patreon.com"

    # 1. Create creator.
    creator_id = creator(creator_email).user_id
    campaign_id = campaign_mgr.try_get_campaign_id_by_user_id_hack(creator_id)

    # 2. Create patrons.
    patron_id_1 = patron(patron_email).user_id
    # min_cents_pledged_to_view for create_post() below is 500, so these must be high enough to be allowed to comment
    pledge_fixtures.create_pledge(patron_id_1, creator_id, amount=600)
    patron_id_2 = patron().user_id
    pledge_fixtures.create_pledge(patron_id_2, creator_id, amount=700)

    # 3. Patron comment, patron reply.
    post_id_1 = create_post(campaign_id, creator_id).activity_id
    with AssertSendsMail(creator_email, 1):
        comment_id_1 = comment_mgr.save_comment(
            patron_id_1, post_id_1, "1"
        ).comment_id
    with AssertSendsMail(creator_email, 1):
        with AssertSendsMail(patron_email, 1):
            comment_mgr.save_comment(
                patron_id_2, post_id_1, "2", comment_id_1
            )

    # 4. Patron comment, creator reply.
    post_id_2 = create_post(campaign_id, creator_id).activity_id
    with AssertSendsMail(creator_email, 1):
        comment_id_2 = comment_mgr.save_comment(
            patron_id_1, post_id_2, "3"
        ).comment_id
    with AssertSendsMail(patron_email, 1):
        comment_mgr.save_comment(
            creator_id, post_id_2, "4", comment_id_2
        )

    # 5. Creator comment, patron reply.
    post_id_3 = create_post(campaign_id, creator_id).activity_id
    with AssertSendsMail(creator_email, 0):
        comment_id_3 = comment_mgr.save_comment(
            creator_id, post_id_3, "5"
        ).comment_id
    with AssertSendsMail(creator_email, 1):
        comment_mgr.save_comment(
            patron_id_1, post_id_3, "6", comment_id_3
        )

    # 6. Reply Chain.
    post_id_4 = create_post(campaign_id, creator_id).activity_id
    comment_id_4 = comment_mgr.save_comment(
        patron_id_1, post_id_4, "7"
    ).comment_id
    comment_id_5 = comment_mgr.save_comment(
        patron_id_2, post_id_4, "8", comment_id_4
    ).comment_id
    with AssertSendsMail(creator_email, 0):
        with AssertSendsMail(patron_email, 0):
            comment_mgr.save_comment(
                creator_id, post_id_4, "9", comment_id_5
            )
