import patreon
from patreon.util import datetime
from test.fixtures.activity_fixtures import create_post
from test.fixtures.campaign_fixtures import create_campaign
from test.fixtures.user_fixtures import patron
from nose.tools import *


@patreon.test.manage()
def test_instance():
    patron_user1 = patron()
    patron_user2 = patron()
    nsfw_campaign = create_campaign(
        patron_user1.UID, is_nsfw=1, published_at=datetime.datetime.now()
    )
    normal_campaign = create_campaign(
        patron_user2.UID, published_at=datetime.datetime.now()
    )

    nsfw_activity = create_post(patron_user1.UID, nsfw_campaign.campaign_id)
    patron_activity = create_post(
        patron_user1.UID, normal_campaign.campaign_id,
        min_cents_pledged_to_view=1
    )
    activity_activity = create_post(
        patron_user1.UID, normal_campaign.campaign_id
    )
    unpublished_activity = create_post(
        patron_user2.UID, normal_campaign.campaign_id,
        created_at=datetime.datetime.now()
    )
    normal_activity = create_post(patron_user2.UID, normal_campaign.campaign_id)

    assert_true(not nsfw_activity.is_searchable)
    assert_true(not patron_activity.is_searchable)
    assert_true(not activity_activity.is_searchable)
    assert_true(not unpublished_activity.is_searchable)
    assert_true(normal_activity.is_searchable)


@patreon.test.manage()
def test_search_dict():
    patron_user1 = patron()
    patron_user2 = patron(vanity='&quot;')

    campaign_unencoded = create_campaign(
        patron_user1.UID, published_at=datetime.datetime.now(), summary='"',
        creation_name='"', pay_per_name='"'
    )
    campaign_encoded = create_campaign(
        patron_user2.UID, published_at=datetime.datetime.now(),
        summary='&quot;', creation_name='&quot;', pay_per_name='&quot'
    )
    unencoded_activity = create_post(
        patron_user1.UID, campaign_unencoded.campaign_id, activity_content='"',
        activity_title='"'
    )
    encoded_activity = create_post(
        patron_user1.UID, campaign_encoded.campaign_id,
        activity_content='&quot;', activity_title='&quot;'
    )
    search_dict = unencoded_activity.as_search_dict()
    assert_equals(search_dict['title'], '"')
    assert_equals(search_dict['description'], '"')

    search_dict = encoded_activity.as_search_dict()
    assert_equals(search_dict['title'], '"')
    assert_equals(search_dict['description'], '"')
