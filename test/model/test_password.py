from nose.tools import *
import patreon
from patreon.model.manager.auth_mgr import change_password, check_password, \
    check_admin_password, deprecated_verify_hash_hybrid, \
    deprecated_verify_hash_md5, hash_password, verify_hash_bcrypt
from patreon.model.manager.user_mgr import register_user
from patreon.model.table import User


@patreon.test.manage()
def test_hash_formats():
    bcrypt_hash = "$2a$12$pnpcjwh57nNYkdOhv4t1zeF93r0LgqtdRKcSpSJI9SVw4he4eXo3K"
    md5_hash = "569a2fa50089f25214665070ef1d5280"
    hybrid_hash = "$2a$12$9fmD4959RWt4D/iIzPIQyei/z1n29FxpzVgLndoWwUFaa4qzVdzlS"
    password = "test"
    wrong_password = "test2"

    assert_true(verify_hash_bcrypt(password, bcrypt_hash))
    assert_false(verify_hash_bcrypt(wrong_password, bcrypt_hash))
    assert_true(deprecated_verify_hash_md5(password, md5_hash))
    assert_false(deprecated_verify_hash_md5(wrong_password, md5_hash))
    assert_true(deprecated_verify_hash_hybrid(password, hybrid_hash))
    assert_false(deprecated_verify_hash_hybrid(wrong_password, hybrid_hash))


@patreon.test.manage()
def test_hash_password():
    password = 'test'
    bcrypt_hash = hash_password(password)
    assert_true(verify_hash_bcrypt(password, bcrypt_hash))


@patreon.test.manage()
def test_change_password():
    password = 'password'
    email = 'test@a.com'
    register_user(email, password, 'test', 'test', 'test')
    user_id = User.get_unique(email=email).user_id
    password = 'test'
    change_password(user_id, password)
    password_hash = User.get(user_id=user_id).Password
    assert_true(verify_hash_bcrypt(password, password_hash))


@patreon.test.manage()
def test_check_password():
    password = 'password'
    email = 'test@a.com'
    register_user(email, password, 'test', 'test', 'test')
    user_id = User.get_unique(email=email).user_id
    assert_true(check_password(user_id, password))
    assert_false(check_password('100000', password))
    assert_false(check_password(user_id, 'notpassword'))


@patreon.test.manage()
def test_check_password_rehash():
    md5_hash = "569a2fa50089f25214665070ef1d5280"
    hybrid_hash = "$2a$12$9fmD4959RWt4D/iIzPIQyei/z1n29FxpzVgLndoWwUFaa4qzVdzlS"
    password = "test"
    email = 'test@a.com'
    register_user(email, password, 'test', 'test', 'test')
    user_id = User.get_unique(email=email).user_id

    def test_one_hash(one_password_hash):
        User.get(user_id=user_id).update({'Password': one_password_hash})
        assert_true(check_password(user_id, password))
        password_hash = User.get(user_id=user_id).Password
        assert_true(verify_hash_bcrypt(password, password_hash))

    test_one_hash(md5_hash)
    test_one_hash(hybrid_hash)


@patreon.test.manage()
def test_admin_login_fail():
    password = 'not the admin pass'
    assert_false(check_admin_password(password))
