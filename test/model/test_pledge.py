import time
from nose.tools import *

import patreon
from patreon.model.manager import address_mgr, complete_pledge_mgr, pledge_mgr, reward_mgr
from test.fixtures.address_fixtures import patreon_address
from test.fixtures.reward_fixtures import create_reward
from test.fixtures.user_fixtures import creator, patron_with_credit_card
from test.test_helpers.reward_helpers import assert_reward_counts_as_dict


@patreon.test.manage()
def test_get_pledge():
    creator_id = creator().user_id
    patron, card_id = patron_with_credit_card()
    patron_id = patron.user_id

    reward_low_cents = 100
    reward_high_cents = 500
    reward_shipping_cents = 1000

    reward_low_id = create_reward(creator_id, reward_low_cents).reward_id
    reward_high_id = create_reward(creator_id, reward_high_cents, UserLimit=1).reward_id
    reward_shipping_id = create_reward(creator_id, reward_shipping_cents, Shipping=1).reward_id

    rewards = reward_mgr.find_by_creator_id(creator_id)
    assert_equal(len(rewards), 3)

    assert_reward_counts_as_dict(
        creator_id, {
            reward_low_id: 0,
            reward_high_id: 0,
            reward_shipping_id: 0
        }
    )

    # 1. Pledge to campaign with lower reward tier.
    complete_pledge_mgr.create_or_update_pledge(
        patron_id=patron_id,
        amount=reward_low_cents,
        pledge_cap=reward_low_cents,
        reward_id=reward_low_id,
        card_id=card_id,
        creator_id=creator_id
    )

    assert_reward_counts_as_dict(
        creator_id, {
            reward_low_id: 1,
            reward_high_id: 0,
            reward_shipping_id: 0
        }
    )

    highest_reward_id = reward_mgr.find_highest_reward_by_amount(creator_id, reward_high_cents).reward_id
    assert_equal(highest_reward_id, reward_high_id)

    time.sleep(1)  # right now things explode in tblHistory if you edit a pledge twice in one second :-(

    # 2. Pledge to campaign with high reward tier.
    complete_pledge_mgr.create_or_update_pledge(
        patron_id=patron_id,
        amount=reward_high_cents,
        pledge_cap=reward_high_cents,
        reward_id=reward_high_id,
        card_id=card_id,
        creator_id=creator_id
    )

    assert_reward_counts_as_dict(
        creator_id, {
            reward_low_id: 0,
            reward_high_id: 1,
            reward_shipping_id: 0
        }
    )

    highest_reward_id = reward_mgr.find_highest_reward_by_amount(creator_id, reward_high_cents).reward_id
    assert_equal(highest_reward_id, reward_low_id)

    time.sleep(1)  # right now things explode in tblHistory if you edit a pledge twice in one second :-(

    # 3. Pledge to campaign with shipping-required reward tier.
    complete_pledge_mgr.create_or_update_pledge(
        patron_id=patron_id,
        amount=reward_shipping_cents,
        pledge_cap=reward_shipping_cents,
        reward_id=reward_shipping_id,
        card_id=card_id,
        creator_id=creator_id,
        address=patreon_address()
    )

    assert_reward_counts_as_dict(
        creator_id, {
            reward_low_id: 0,
            reward_high_id: 0,
            reward_shipping_id: 1
        }
    )

    pledge = pledge_mgr.get(patron_id=patron_id, creator_id=creator_id)
    addressee = patron.full_name

    # Assert that the old_address saves correctly.
    assert_equals(pledge.address.street, patreon_address().street)
    assert_equals(pledge.address.address_zip, patreon_address().address_zip)

    # Assert that the new_address saves correctly.
    assert_equals(pledge.shipping_address.addressee, addressee)
    assert_equals(pledge.shipping_address.street, patreon_address().street)
    assert_equals(pledge.shipping_address.postal_code, patreon_address().address_zip)

    # Assert that the new_address saves to the user correctly.
    addresses = address_mgr.get_addresses_by_user_id(patron_id)
    assert_equals(len(addresses), 1)
    patron_address = addresses[0]

    assert_equals(patron_address.addressee, addressee)
    assert_equals(patron_address.street, patreon_address().street)
    assert_equals(patron_address.postal_code, patreon_address().address_zip)
