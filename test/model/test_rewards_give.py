import patreon

import datetime
from patreon.model.manager import rewards_give_mgr
from test.fixtures.campaign_fixtures import create_campaign
from test.fixtures import user_fixtures, fixtures_rewards_give, activity_fixtures
from nose.tools import *

@patreon.test.manage()
def test_get_most_recent():
    creator = user_fixtures.creator()
    patron = user_fixtures.patron()
    post = activity_fixtures.create_post(user_id = creator.user_id,
                                         campaign_id = creator.campaigns[0].campaign_id)

    invoice = fixtures_rewards_give.create_rewards_give(UID = patron.user_id,
                                                        HID = post.activity_id,
                                                        RID = 0,
                                                        CID = creator.user_id)

    yesterday = datetime.datetime.now() - datetime.timedelta(days = 1)
    old_invoice = fixtures_rewards_give.create_rewards_give(UID = patron.user_id,
                                                            HID = post.activity_id,
                                                            RID = 0, CID = creator.user_id,
                                                            Created = yesterday)

    result = rewards_give_mgr.get_most_recent_invoice_for_patron_and_creator(patron_id = patron.user_id,
                                                                             creator_id = creator.user_id)

    assert_equals(invoice, result)
