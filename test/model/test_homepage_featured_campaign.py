from nose.tools import *
import patreon
from patreon.model.manager import homepage_featured_campaign_mgr
from test.fixtures.campaign_fixtures import create_campaign
from test.fixtures.pledge_fixtures import create_pledge
from test.fixtures.user_fixtures import creator, patron


@patreon.test.manage()
def test_get_qualified_campaigns():
    qualified = homepage_featured_campaign_mgr.get_qualified_campaigns(
        patron_cutoff=1
    )
    assert_equals(0, len(qualified))

    creator_user = creator()
    creator_id = creator_user.user_id
    campaign = creator_user.campaigns[0]
    create_pledge(patron().user_id, creator_id, 150000)

    qualified = homepage_featured_campaign_mgr.get_qualified_campaigns(
        patron_cutoff=1
    )
    assert_equals(1, len(qualified))
    assert_equals(campaign.campaign_id, qualified[0].campaign_id)
    assert_equals(campaign.published_at, qualified[0].published_at)

    homepage_featured_campaign_mgr.approve_campaign(campaign)
    qualified = homepage_featured_campaign_mgr.get_qualified_campaigns(
        patron_cutoff=1
    )
    assert_equals(0, len(qualified))


@patreon.test.manage()
def test_get_campaigns_by_status():
    approved = homepage_featured_campaign_mgr.get_approved_campaigns()
    assert_equals(0, len(approved))

    campaign = create_campaign(creator().user_id)

    status = homepage_featured_campaign_mgr.get_campaign_status(
        campaign.campaign_id
    )
    assert_is_none(status)

    homepage_featured_campaign_mgr.approve_campaign(campaign)
    approved = homepage_featured_campaign_mgr.get_approved_campaigns()
    assert_equals(1, len(approved))
    assert_equals(campaign.campaign_id, approved[0].campaign_id)
    assert_equals(campaign.published_at, approved[0].published_at)
    status = homepage_featured_campaign_mgr.get_campaign_status(
        campaign.campaign_id
    )
    assert_equals(1, status['approval_status'])

    homepage_featured_campaign_mgr.disapprove_campaign(campaign)
    approved = homepage_featured_campaign_mgr.get_approved_campaigns()
    assert_equals(0, len(approved))
    status = homepage_featured_campaign_mgr.get_campaign_status(
        campaign.campaign_id
    )
    assert_equals(0, status['approval_status'])

    homepage_featured_campaign_mgr.reset_campaign_status(campaign.campaign_id)
    approved = homepage_featured_campaign_mgr.get_approved_campaigns()
    assert_equals(0, len(approved))
    status = homepage_featured_campaign_mgr.get_campaign_status(
        campaign.campaign_id
    )
    assert_is_none(status)


@patreon.test.manage()
def test_get_disapproved_campaigns():
    disapproved = homepage_featured_campaign_mgr.get_disapproved_campaigns()
    assert_equals(0, len(disapproved))

    campaign = create_campaign(creator().user_id)
    homepage_featured_campaign_mgr.approve_campaign(campaign)
    disapproved = homepage_featured_campaign_mgr.get_disapproved_campaigns()
    assert_equals(0, len(disapproved))

    homepage_featured_campaign_mgr.disapprove_campaign(campaign)
    disapproved = homepage_featured_campaign_mgr.get_disapproved_campaigns()
    assert_equals(1, len(disapproved))
    assert_equals(campaign.campaign_id, disapproved[0].campaign_id)
    assert_equals(campaign.published_at, disapproved[0].published_at)

    homepage_featured_campaign_mgr.reset_campaign_status(campaign.campaign_id)
    disapproved = homepage_featured_campaign_mgr.get_disapproved_campaigns()
    assert_equals(0, len(disapproved))
