import patreon
from patreon.util import datetime
from patreon.model.manager import campaign_mgr
from test.fixtures.campaign_fixtures import create_campaign
from test.fixtures.user_fixtures import patron
from nose.tools import *


@patreon.test.manage()
def test_instance():
    patron_user1 = patron()
    patron_user2 = patron()
    patron_user3 = patron()
    nsfw_campaign = create_campaign(patron_user1.UID, is_nsfw=1)
    unpublished_campaign = create_campaign(patron_user2.UID, created_at=datetime.datetime.now())
    normal_campaign = create_campaign(patron_user3.UID)

    assert_true(not nsfw_campaign.is_searchable)
    assert_true(not unpublished_campaign.is_searchable)
    assert_true(normal_campaign.is_searchable)


@patreon.test.manage()
def test_static_context():
    assert_equals(0, len(campaign_mgr.find_searchable().all()))

    patron_user1 = patron()
    patron_user2 = patron()
    patron_user3 = patron()
    patron_user4 = patron()

    # nsfw, not searchable
    create_campaign(patron_user1.UID, is_nsfw=1)
    assert_equals(0, len(campaign_mgr.find_searchable().all()))

    # normal, searchable
    create_campaign(patron_user3.UID)
    assert_equals(1, len(campaign_mgr.find_searchable().all()))

    # unpublished, unsearchable
    create_campaign(patron_user2.UID, created_at=datetime.datetime.now())
    assert_equals(1, len(campaign_mgr.find_searchable().all()))

    # normal, searchable
    create_campaign(patron_user4.UID)
    assert_equals(2, len(campaign_mgr.find_searchable().all()))


@patreon.test.manage()
def test_search_dict():
    patron_user1 = patron()
    patron_user2 = patron(vanity='&quot;')

    campaign = create_campaign(patron_user1.UID, published_at=datetime.datetime.now(), summary='"',
                               creation_name='"', pay_per_name='"')
    search_dict = campaign.as_search_dict()
    assert_true(search_dict)
    assert_true(search_dict['creator_name'])
    assert_true(search_dict['creation_name'])
    assert_true(search_dict['summary'])
    assert_true(search_dict['vanity'])
    assert_true(search_dict['pay_per_name'])

    campaign = create_campaign(patron_user2.UID, published_at=datetime.datetime.now(), summary='&quot;',
                               creation_name='&quot;', pay_per_name='&quot')
    search_dict = campaign.as_search_dict()
    assert_true(search_dict)
    assert_equals(search_dict['creation_name'], '"')
    assert_equals(search_dict['summary'], '"')
    assert_equals(search_dict['vanity'], '"')
    assert_equals(search_dict['pay_per_name'], '"')
