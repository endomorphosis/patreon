import datetime
from nose.plugins.attrib import attr
import patreon

from nose.tools import *
from patreon.model.manager import user_mgr, settings_mgr, auth_mgr, user_location_mgr
from patreon.model.table import ChangeEmail, User, CampaignsUsers
from patreon.services.db import db_session
import random
from test import register_user_helper, user_location_helper


@patreon.test.manage()
def test_register_user_okay():
    user = register_user_helper()

    email = user['email']
    user_id = user['user_id']
    password = user['password']
    token = user['token']

    assert_true(user_mgr.get_unverified_user(verification=token) is not None)
    assert_true(user_mgr.get_user_unique(email=email) is not None)

    assert_true(settings_mgr.get_old_settings(user_id=user_id) is not None)
    assert_true(settings_mgr.get_settings(user_id=user_id) is not None)
    assert_true(settings_mgr.get_user_extra(user_id=user_id) is not None)

    assert_true(auth_mgr.check_password(user_id, password))
    assert_true(user_mgr.confirm_user(token) is not None)

    register_user_helper({'email': 'facedude@gac.com', 'facebook_id': 10})
    assert_true(user_mgr.get_user_unique(facebook_id=10) is not None)
    assert_true(user_mgr.get_user_unique(facebook_id=10).Password is None)


@patreon.test.manage()
def test_register_user_errors():
    assert_true(user_mgr.register_user(None, "", "", "", "") is None)
    assert_true(user_mgr.register_user('a@b.com', None, "", "", "") is None)
    assert_true(user_mgr.register_user('notanemail', "", "", "", "") is None)

    user = register_user_helper()
    assert_true(register_user_helper({'email': user['email']}).get('token') is None)
    new_user = register_user_helper({'email': 'cooldude53@gmail.com', 'vanity': user['vanity']})
    new_email = new_user['email']
    new_vanity = db_session.query(User.Vanity).filter_by(Email=new_email).first().Vanity
    assert_true(new_vanity is None)
    register_user_helper({'email': 'facedude@gac.com', 'facebook_id': 10})
    user = register_user_helper({'email': 'facedude@gac3.com', 'facebook_id': 10})
    assert_true(user['token'] is None)


@patreon.test.manage()
def test_get_user_by_email_and_password():
    user = register_user_helper()
    user_id = user['user_id']
    email = user['email']
    password = user['password']
    assert_true(auth_mgr.get_user_by_email_and_password(email, password) == user_id)
    assert_true(auth_mgr.get_user_by_email_and_password('noemail@gmail.com', password) is None)
    assert_true(auth_mgr.get_user_by_email_and_password(email, 'nopassword') is None)
    assert_true(auth_mgr.get_user_by_email_and_password(None, password) is None)
    assert_true(auth_mgr.get_user_by_email_and_password(email, None) is None)


@patreon.test.manage()
def test_confirm_user():
    user = register_user_helper()

    assert_true(user_mgr.confirm_user(user['token']))
    assert_true(user_mgr.confirm_user('notoken') is None)


@patreon.test.manage()
def test_resend_email():
    assert_true(user_mgr.resend_confirm(None) is None)
    assert_true(user_mgr.resend_confirm('notauser') is None)

    user = register_user_helper()
    user_id = user['user_id']
    token = user_mgr.resend_confirm(user_id)
    assert_true(user_mgr.confirm_user(token))
    assert_equals(user_mgr.confirm_user(user['token']), None)


@patreon.test.manage()
def test_setup_update_email():
    assert_true(user_mgr.setup_update_email(None, 'not none') is None)
    assert_true(user_mgr.setup_update_email('not none', None) is None)
    assert_true(user_mgr.setup_update_email('1', 'invalid_email') is None)
    assert_true(user_mgr.setup_update_email('1', 'valid@email.com') is None)

    user = register_user_helper()

    email = user['email']
    user_id = user['user_id']

    user_mgr.setup_update_email(user_id, email)
    result = db_session.query(ChangeEmail.Verification, ChangeEmail.Email) \
        .filter_by(UID=user_id).first()
    assert_equals(None, result)

    token = user_mgr.setup_update_email(user_id, 'aaa@bbb.com')
    db_token, db_email = db_session.query(ChangeEmail.Verification, ChangeEmail.Email) \
        .filter_by(UID=user_id).first()

    assert_true(token == db_token)
    assert_true(db_email == 'aaa@bbb.com')


@patreon.test.manage()
def test_update_email():
    assert_true(user_mgr.update_email(None) is None)

    user = register_user_helper()
    assert_true(user['token'] is not None)
    user_id = user['user_id']

    new_email = 'coolcrazydude@cat.com'
    token = user_mgr.setup_update_email(user_id, new_email)

    assert_true(user_mgr.update_email(token) is not None)
    email, status = db_session.query(User.Email, User.Status).filter_by(UID=user_id).first()
    assert_true(email == new_email)
    assert_true(status == 1)


@patreon.test.manage()
@attr(speed="slower")
def test_update_user():
    assert_is_none(user_mgr.update_user(None, None, "", "", "", "", "", "", "", "", "", "", "", ""))

    user = register_user_helper()
    user_id = user['user_id']
    new_vanity = "crazy"
    new_first_name = "steve"
    new_last_name = "bob"
    new_about = "How are you friend"
    new_youtube = "no YOU tube"
    new_twitter = "tweeter"
    new_facebook = "myspace.com/tom"
    new_fbid = 6
    new_gender = 1
    new_password = "frank"
    new_image_url = "eye"
    new_thumb_url = "nose"

    new_user = user_mgr.update_user(
        user_id, user_id, new_vanity, new_first_name, new_last_name, new_about,
        new_youtube,new_twitter, new_facebook, new_image_url, new_thumb_url,
        new_fbid, new_gender, new_password
    )

    assert_true(new_user is not None)

    assert_true(new_user.UID == user_id)
    assert_true(new_user.Vanity == new_vanity)
    assert_true(new_user.FName == new_first_name)
    assert_true(new_user.LName == new_last_name)
    assert_true(new_user.About == new_about)
    assert_true(new_user.Youtube == new_youtube)
    assert_true(new_user.Twitter == new_twitter)
    assert_true(new_user.Facebook == new_facebook)
    assert_true(new_user.FBID == new_fbid)
    assert_true(new_user.Gender == new_gender)
    assert_true(auth_mgr.check_password(user_id, new_password))
    assert_true(new_user.ThumbUrl == new_thumb_url)
    assert_true(new_user.ImageUrl == new_image_url)

    new_new_password = "steve"

    # checks changing only one property
    new_user = user_mgr.update_user(user_id, user_id, password=new_new_password)

    assert_true(new_user.UID == user_id)
    assert_true(new_user.Vanity == new_vanity)
    assert_true(new_user.FName == new_first_name)
    assert_true(new_user.LName == new_last_name)
    assert_true(new_user.About == new_about)
    assert_true(new_user.Youtube == new_youtube)
    assert_true(new_user.Twitter == new_twitter)
    assert_true(new_user.Facebook == new_facebook)
    assert_true(new_user.FBID == new_fbid)
    assert_true(new_user.Gender == new_gender)
    assert_true(not auth_mgr.check_password(user_id, new_password))
    assert_true(auth_mgr.check_password(user_id, new_new_password))
    assert_true(new_user.ThumbUrl == new_thumb_url)
    assert_true(new_user.ImageUrl == new_image_url)

    new_new_password = "eve"

    # checks changing properties to non-truthy values
    new_user = user_mgr.update_user(user_id, user_id, "", "", "", "", "", "", "", "", "", 0, 0, new_new_password)

    assert_true(new_user.UID == user_id)
    assert_true(new_user.Vanity == "")
    assert_true(new_user.FName == "")
    assert_true(new_user.LName == "")
    assert_true(new_user.About == "")
    assert_true(new_user.Youtube == "")
    assert_true(new_user.Twitter == "")
    assert_true(new_user.Facebook == "")
    assert_true(new_user.FBID == 0)
    assert_true(new_user.Gender == 0)
    assert_true(not auth_mgr.check_password(user_id, new_password))
    assert_true(auth_mgr.check_password(user_id, new_new_password))
    assert_true(new_user.ThumbUrl == "")
    assert_true(new_user.ImageUrl == "")


@patreon.test.manage()
def test_insert_locations():
    assert_true(user_location_mgr.insert(None, 1, 1, "Yes") is None)
    assert_true(user_location_mgr.insert(1, None, 1, "Yes") is None)
    assert_true(user_location_mgr.insert(1, 1, None, "Yes") is None)
    assert_true(user_location_mgr.insert(1, "notlat", 1, "yes") is None)
    assert_true(user_location_mgr.insert(1, 1, "notlong", "yes") is None)

    user = register_user_helper()
    user_id = user['user_id']

    assert_true(len(user_location_helper(user_id)) == 1)
    assert_true(len(user_location_helper(user_id)) == 2)
    assert_true(len(user_location_helper(user_id)) == 3)


@patreon.test.manage()
def test_delete_by_location():
    assert_true(user_location_mgr.delete_by_location(None, 1, 1) is None)
    assert_true(user_location_mgr.delete_by_location(1, None, 1) is None)
    assert_true(user_location_mgr.delete_by_location(1, 1, None) is None)
    assert_true(user_location_mgr.delete_by_location(1, "notlat", 1) is None)
    assert_true(user_location_mgr.delete_by_location(1, 1, "notlong") is None)

    user = register_user_helper()
    user_id = user['user_id']
    lat = round(random.uniform(-10, 10), 10)
    long = round(random.uniform(-10, 10), 10)
    location_name = "Patreon HQ"

    assert_true(len(user_location_mgr.insert(user_id, lat, long, location_name)) == 1)
    assert_true(len(user_location_mgr.delete_by_location(user_id, lat, long)) == 0)


@patreon.test.manage()
def test_remove_all_locations():
    assert_true(user_location_mgr.delete_by_user_id(None) is None)

    user = register_user_helper()
    user_id = user['user_id']

    assert_true(len(user_location_helper(user_id)) == 1)
    assert_true(len(user_location_helper(user_id)) == 2)
    assert_true(len(user_location_helper(user_id)) == 3)
    assert_true(len(user_location_mgr.delete_by_user_id(user_id)) == 0)


@patreon.test.manage()
def test_update_user_extra():
    assert_true(settings_mgr.update_user_extra(None, "", "", "", "", "", "") is None)

    user = register_user_helper()
    user_id = user['user_id']
    paypal_email = 'blah@blah.com'
    auto_pay = 1
    pay_method = 1
    stripe_recipient_id = "1"
    stripe_last_four = 9999
    stripe_name = "Steve Bob"

    user_extra = settings_mgr.update_user_extra(
        user_id, paypal_email, auto_pay, pay_method, stripe_recipient_id,
        stripe_last_four, stripe_name
    )

    assert_true(user_extra.UID == user_id)
    assert_true(user_extra.PaypalEmail == paypal_email)
    assert_true(user_extra.PayMethod == pay_method)
    assert_true(user_extra.AutoPay == auto_pay)
    assert_true(user_extra.StripeRecipientID == stripe_recipient_id)
    assert_true(user_extra.StripeLastFour == stripe_last_four)
    assert_true(user_extra.StripeName == stripe_name)

    user_extra = settings_mgr.update_user_extra(user_id, stripe_name="steve_bob")

    assert_true(user_extra.UID == user_id)
    assert_true(user_extra.PaypalEmail == paypal_email)
    assert_true(user_extra.PayMethod == pay_method)
    assert_true(user_extra.AutoPay == auto_pay)
    assert_true(user_extra.StripeRecipientID == stripe_recipient_id)
    assert_true(user_extra.StripeLastFour == stripe_last_four)
    assert_true(user_extra.StripeName == "steve_bob")


@patreon.test.manage()
def test_remove_fb():
    assert_true(user_mgr.remove_fb(None) is None)

    user = register_user_helper()
    user_id = user['user_id']
    result = user_mgr.update_user(user_id, user_id, facebook_id=777)
    assert_true(result.FBID == 777)
    result = user_mgr.remove_fb(user_id)
    assert_true(result.FBID is None)


@patreon.test.manage()
def test_login_user_fb():
    assert_true(auth_mgr.login_user_fb(None) is None)
    assert_true(auth_mgr.login_user_fb(1) is None)

    user = register_user_helper()
    user_id = user['user_id']
    result = user_mgr.update_user(user_id, user_id, facebook_id=777)
    assert_true(result.FBID == 777)
    assert_true(auth_mgr.login_user_fb(777) == user_id)


@patreon.test.manage()
def test_fb_merge_user_one_account():
    assert_true(user_mgr.merge_user_fb(None, 1) is None)
    assert_true(user_mgr.merge_user_fb(1, None) is None)
    password_user = register_user_helper()
    facebook_id = 1

    result = user_mgr.merge_user_fb(password_user['user_id'], facebook_id)
    assert_true(result.FBID == facebook_id)
    assert_true(user_mgr.merge_user_fb(password_user['user_id'], facebook_id) is None)


@patreon.test.manage()
def test_update_user_settings():
    assert_true(settings_mgr.update_user_settings(None, 1, 1) is None)

    user = register_user_helper()
    user_id = user['user_id']

    result = settings_mgr.update_user_settings(user_id, 1, 1)
    assert_true(result.Privacy == 1)
    assert_true(result.FBImport == 1)

    result = settings_mgr.update_user_settings(user_id, None, 0)
    assert_true(result.Privacy == 1)
    assert_true(result.FBImport == 0)

    result = settings_mgr.update_user_settings(user_id, None, None)
    assert_true(result.Privacy == 1)
    assert_true(result.FBImport == 0)


@patreon.test.manage()
def test_update_campaign_settings():
    assert_true(settings_mgr.update_campaign_settings(None, 1, 1) is None)

    user = register_user_helper()
    user_id = user['user_id']

    campaigns_user_dict = {'user_id': user_id, 'campaign_id': 1, 'joined_at': datetime.datetime.now()}
    CampaignsUsers.insert(campaigns_user_dict)

    result = settings_mgr.update_campaign_settings(user_id, 1, 1)
    assert_true(result.should_email_on_new_patron == 1)
    assert_true(result.should_email_on_patron_post == 1)

    result = settings_mgr.update_campaign_settings(user_id, None, 0)
    assert_true(result.should_email_on_new_patron == 1)
    assert_true(result.should_email_on_patron_post == 0)

    result = settings_mgr.update_campaign_settings(user_id, None, None)
    assert_true(result.should_email_on_new_patron == 1)
    assert_true(result.should_email_on_patron_post == 0)


@patreon.test.manage()
def test_update_patron_settings():
    assert_true(settings_mgr.update_patron_settings(None, 1, 1, 1) is None)

    user = register_user_helper()
    user_id = user['user_id']

    result = settings_mgr.update_patron_settings(user_id, 0, 0, 0)
    assert_true(result.email_new_comment == 0)
    assert_true(result.email_patreon == 0)
    assert_true(result.email_goal == 0)

    result = settings_mgr.update_patron_settings(user_id, None, 0, 1)
    assert_true(result.email_new_comment == 0)
    assert_true(result.email_patreon == 0)
    assert_true(result.email_goal == 1)

    result = settings_mgr.update_patron_settings(user_id, None, None, None)
    assert_true(result.email_new_comment == 0)
    assert_true(result.email_patreon == 0)
    assert_true(result.email_goal == 1)


@patreon.test.manage()
def test_update_user_settings():
    assert_true(settings_mgr.update_settings(None, None) is None)
    user = register_user_helper()
    user_id = user['user_id']

    assert_true(settings_mgr.update_settings(user_id, user_id, privacy=1) is not None)
    assert_true(settings_mgr.get_old_settings(user_id=user_id).Privacy == 1)
