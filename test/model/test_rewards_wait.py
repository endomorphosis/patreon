from nose.tools import *
import patreon
from patreon.model.manager import rewards_wait_mgr
from test.fixtures.reward_fixtures import create_reward
from test.fixtures.user_fixtures import patron, creator


@patreon.test.manage()
def test_reward_wait_access():
    person = patron()
    reward = create_reward(creator().user_id)
    if person.user_id == reward.reward_id:
        reward = create_reward(creator().user_id)

    assert_is_none(rewards_wait_mgr.get(person.user_id, reward.reward_id))

    rewards_wait_mgr.add(person.user_id, reward.reward_id)
    assert_is_not_none(rewards_wait_mgr.get(person.user_id, reward.reward_id))

    rewards_wait_mgr.delete(person.user_id, reward.reward_id)
    assert_is_none(rewards_wait_mgr.get(person.user_id, reward.reward_id))


@patreon.test.manage()
def test_delete_nonexistent_reward_wait():
    person = patron()
    reward = create_reward(creator().user_id)
    if person.user_id == reward.reward_id:
        reward = create_reward(creator().user_id)

    # failing to delete a RewardWait should be silent
    rewards_wait_mgr.delete(person.user_id, reward.reward_id)


@patreon.test.manage()
def test_add_duplicate_reward_wait():
    person = patron()
    reward = create_reward(creator().user_id)
    if person.user_id == reward.reward_id:
        reward = create_reward(creator().user_id)

    primary_key = rewards_wait_mgr.add(person.user_id, reward.reward_id)
    assert_equals(primary_key, [reward.reward_id, person.user_id])
    # no duplicate error is raised on second attempt
    primary_key = rewards_wait_mgr.add(person.user_id, reward.reward_id)
    assert_equals(primary_key, [reward.reward_id, person.user_id])
