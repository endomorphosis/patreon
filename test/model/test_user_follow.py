import patreon
from test.fixtures.follows_fixtures import follow_user, unfollow_user
from test.fixtures.user_fixtures import patron
from test.test_helpers.follow_helpers import assert_is_not_following, \
    assert_followed_are, assert_followers_are


@patreon.test.manage()
def test_follow():
    alice_id = patron().user_id
    bob_id = patron().user_id

    assert_is_not_following(alice_id, bob_id)
    assert_is_not_following(bob_id, alice_id)

    follow_user(alice_id, bob_id)

    assert_followed_are(alice_id,   [bob_id])
    assert_followed_are(bob_id,     [])
    assert_followers_are(alice_id,  [])
    assert_followers_are(bob_id,    [alice_id])

    unfollow_user(alice_id, bob_id)

    assert_is_not_following(alice_id, bob_id)
    assert_is_not_following(bob_id, alice_id)
