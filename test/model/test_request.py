from flask import request
import patreon
from patreon.exception.auth_errors import UnauthorizedAdminFunction
from patreon.model.request.route_wrappers import require_admin
from nose.tools import *
from test.fixtures.session_fixtures import create_web_session, create_unauthed_web_session
from test.test_helpers.user_helpers import get_new_logged_in_admin_patron


# in a request + adminin a request + not an admin, not in a request

@require_admin
def add_two_numbers(x, y):
    return x + y


@patreon.test.manage()
def test_require_admin_as_admin():
    patron_user, patron_session = get_new_logged_in_admin_patron()

    with patron_session as test_request_context:
        assert_equals(add_two_numbers(2, 2), 4)


@patreon.test.manage()
@raises(UnauthorizedAdminFunction)
def test_require_admin_as_nonadmin():
    patron_session = create_web_session()

    with patron_session as test_request_context:
        add_two_numbers(2, 2)


@patreon.test.manage()
@raises(UnauthorizedAdminFunction)
def test_require_admin_as_anauthenticated_user():
    patron_session = create_unauthed_web_session()

    with patron_session as test_request_context:
        add_two_numbers(2, 2)


@patreon.test.manage()
def test_require_admin_without_session():
    assert_equals(add_two_numbers(2, 2), 4)


@patreon.test.manage()
def test_get_dictionary_from_params():
    patron_user, patron_session = get_new_logged_in_admin_patron()

    with patron_session as test_request_context:
        request.args = {
            'cat[dog]': '1',
            'cat': '2',
            'dog': '3',
            'dog[bog]': '4',
            'cat[cat]': '5',
            'cat[cat[rat]]': '6'
        }
        output_dictionary = request.dictionary_from_get('cat')

        assert_equals(len(output_dictionary.keys()), 3)
        assert_equals(output_dictionary['dog'], '1')
        assert_equals(output_dictionary['cat'], '5')
        assert_equals(output_dictionary['cat[rat]'], '6')

    with patron_session as test_request_context:
        request.form = {
            'cat[dog]': '1',
            'cat': '2',
            'dog': '3',
            'dog[cat]': '4',
            'cat[cat]': '5',
            'cat[cat[rat]]': '6'
        }
        output_dictionary = request.dictionary_from_post('cat')

        assert_equals(len(output_dictionary.keys()), 3)
        assert_equals(output_dictionary['dog'], '1')
        assert_equals(output_dictionary['cat'], '5')
        assert_equals(output_dictionary['cat[rat]'], '6')
