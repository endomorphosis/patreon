import patreon

from nose.tools import *
from test import register_user_helper
from patreon.model.manager import presence_mgr


@patreon.test.manage()
def test_presence():
    user_id = register_user_helper().get('user_id')
    presences = presence_mgr.find_by_user_id(user_id)
    assert_equals([presence.room for presence in presences], [])

    # Test new presence
    presence_mgr.update_presence(user_id, 'global')
    presences = presence_mgr.find_by_user_id(user_id)
    assert_equals([presence.room for presence in presences], ['global'])

    # Test update presence
    presence_mgr.update_presence(user_id, 'global')
    presences = presence_mgr.find_by_user_id(user_id)
    assert_equals([presence.room for presence in presences], ['global'])
