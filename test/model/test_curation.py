from nose.tools import *

import patreon
from patreon.model.type import Category
from patreon.model.manager import curation_mgr, campaign_mgr, activity_mgr
from test.fixtures import post_fixtures, user_fixtures

ALL = Category.ALL.category_id
FRONT = Category.FRONT_PAGE.category_id
GAMES = Category.GAMES.category_id
ANIMATION = Category.ANIMATION.category_id


def _get_all_curation_post_ids():
    return [ post.activity_id for post in curation_mgr.find_all_posts() ]


def _get_category_post_ids(category_id):
    return [
        curation.post_id
        for curation in curation_mgr.find_by_single_category(category_id)
    ]


@patreon.test.manage()
def test_get_curation():
    # 2. Create posts.
    creator_id = user_fixtures.creator().user_id
    campaign_id = campaign_mgr.try_get_campaign_id_by_user_id_hack(creator_id)

    games_id_1 = post_fixtures.create_post(campaign_id, creator_id).activity_id
    games_id_2 = post_fixtures.create_post(campaign_id, creator_id).activity_id
    animation_id = post_fixtures.create_post(campaign_id, creator_id).activity_id

    # 3. Assert featured contents are empty.
    assert_equal(_get_all_curation_post_ids(), [])
    assert_equal(_get_category_post_ids(GAMES), [])
    assert_equal(_get_category_post_ids(ANIMATION), [])
    assert_equal(_get_category_post_ids(ALL), [])

    # 4. Add three curations with different weights.
    curation_mgr.set_curated_post(games_id_1, GAMES, 3)
    curation_mgr.set_curated_post(games_id_2, GAMES, 1)
    curation_mgr.set_curated_post(animation_id, ANIMATION, 2)

    # 5. Assert featured contents.
    assert_equal(_get_all_curation_post_ids(), [games_id_1, animation_id, games_id_2])
    assert_equal(_get_category_post_ids(GAMES), [games_id_1, games_id_2])
    assert_equal(_get_category_post_ids(ANIMATION), [animation_id])
    assert_equal(_get_category_post_ids(ALL), [games_id_1, animation_id, games_id_2])

    # 6. Remove games_curation_2 entirely from featured list.
    curation_mgr.remove_curated_post(games_id_2)

    # 7. Assert featured contents have not games_2.
    assert_equal(_get_all_curation_post_ids(), [games_id_1, animation_id])
    assert_equal(_get_category_post_ids(GAMES), [games_id_1])
    assert_equal(_get_category_post_ids(ANIMATION), [animation_id])
    assert_equal(_get_category_post_ids(ALL), [games_id_1, animation_id])

    # 8. Assert that deleting posts does not break featured ALL.
    activity_mgr.delete_activity(games_id_1)
    assert_equal(_get_all_curation_post_ids(), [animation_id])



@patreon.test.manage()
def test_index():
    # 1. Create posts.
    creator_id = user_fixtures.creator().user_id
    campaign_id = campaign_mgr.try_get_campaign_id_by_user_id_hack(creator_id)

    post_1 = post_fixtures.create_post(campaign_id, creator_id).activity_id
    post_2 = post_fixtures.create_post(campaign_id, creator_id).activity_id
    post_3 = post_fixtures.create_post(campaign_id, creator_id).activity_id

    curation_mgr.set_curated_post(post_1, GAMES, 1)
    curation_mgr.set_curated_post(post_2, FRONT, 1)

    assert_equal(_get_category_post_ids(None), [post_1, post_2])
    assert_equal(_get_category_post_ids(ALL), [post_1, post_2])
    assert_equal(_get_category_post_ids(FRONT), [post_2])
