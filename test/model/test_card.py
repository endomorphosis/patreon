from nose.tools import *

import patreon
from patreon.constants.cards import VISA
from patreon.model.dbm.card_info_dbm import get_cards, save_card
from test import register_user_helper


@patreon.test.manage()
def test_save_card():
    user_id = register_user_helper().get('user_id')
    card1 = {
        'RID': 'cus_123',
        'Name': 'Test Visa',
        'Type': VISA,
        'Number': '1234'
    }
    assert_equal(get_cards(user_id), {})
    save_card(user_id, card1)

    cards = get_cards(user_id)
    assert_equal(len(cards), 1)
    card = cards['cus_123']
    assert_equal(card['TypeNum'], VISA)
    assert_equal(card['Type'], 'Visa')

    card2 = {
        'RID': 'cus_234',
        'Type': 'PayPal',
        'Number': '2345',
        'Name': 'Test PayPal',
        'Country': 'USA',
        'Street': '123 Sesame Street',
        'Street2': '#Grover',
        'City': 'New York',
        'State': 'NY',
        'Zip': '12345',
        'ExpirationDate': '11/16',
    }

    save_card(user_id, card2)
    cards = get_cards(user_id)
    assert_equal(len(cards), 2)
