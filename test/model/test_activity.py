import patreon
from patreon.model.manager import activity_mgr, campaign_mgr
from test.fixtures import user_fixtures, fixtures_rewards_give, activity_fixtures
from nose.tools import *


@patreon.test.manage()
def test_is_blocked_due_to_failed_payment():
    creator_id = user_fixtures.creator().user_id
    campaign_id = campaign_mgr.try_get_campaign_id_by_user_id_hack(creator_id)
    patron_id = user_fixtures.patron().user_id
    post_id = activity_fixtures.create_post(
        user_id=creator_id,
        campaign_id=campaign_id,
        is_paid=True
    ).activity_id

    invoice = fixtures_rewards_give.create_rewards_give(
        UID=patron_id,
        HID=post_id,
        RID=0,
        CID=creator_id
    )

    invoice.update({'Status': patreon.globalvars.get('REWARD_DECLINED')})

    is_blocked = activity_mgr.is_blocked_due_to_failed_payment(post_id, patron_id)
    assert_true(is_blocked)

