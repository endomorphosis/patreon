import patreon
from patreon.model.manager import new_pledge_mgr

from nose.tools import *


@patreon.test.manage()
def test_make_pledge():
    user_id = 1
    campaign_id = 100
    amount_cents = 123
    amount_cents2 = 222

    pledge = new_pledge_mgr.make_or_update_pledge(user_id, campaign_id, amount_cents)
    assert_equal(amount_cents, pledge.amount_cents)

    pledge = new_pledge_mgr.get_pledge_by_patron_and_campaign(user_id, campaign_id)
    assert_equal(amount_cents, pledge.amount_cents)

    pledge = new_pledge_mgr.make_or_update_pledge(user_id, campaign_id, amount_cents2)
    assert_equal(amount_cents2, pledge.amount_cents)

    pledge_history = [
        pledge for pledge in new_pledge_mgr.historic_pledges_by_patron_and_campaign(user_id, campaign_id)
    ]

    assert_equal(2, len(pledge_history))

    assert_equal(amount_cents2, pledge_history[0].amount_cents)
    assert_is_none(pledge_history[0].deleted_at)

    assert_equal(amount_cents, pledge_history[1].amount_cents)
    assert_is_not_none(pledge_history[1].deleted_at)
