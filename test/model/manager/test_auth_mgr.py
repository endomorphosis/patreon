from nose.tools import assert_true
import patreon
from patreon.model.manager import auth_mgr
import pyotp
from test.fixtures.user_fixtures import patron


@patreon.test.manage()
def test_setup_and_pass():
    patron_user = patron()
    secret = pyotp.random_base32()
    auth_mgr.setup_two_factor_auth(patron_user, secret)
    totp = pyotp.TOTP(secret)
    assert_true(auth_mgr.verify_two_factor_code(secret, totp.at(0), for_time=0))
