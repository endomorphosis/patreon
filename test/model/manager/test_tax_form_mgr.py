from nose.tools import *
import patreon
from patreon.model.manager.tax_form_mgr import *
from patreon.model.table import W9TaxForm
from patreon.services import encryption
from patreon.services.encryption import decrypt
import rsa


def w9_data():
    return {
        'user_id': '1',
        'full_name': '2',
        'birth_date': '3',
        'address': '4',
        'city': '5',
        'us_tax_id': '6',
        'signature': '7',
        'country': '8',
        'business_name': '9',
        'state': '10',
        'zip': '11',
        'tax_class': '12'
    }


def w8_data(optional=False):
    required = {
        'user_id': '1',
        'full_name': '2',
        'birth_date': '3',
        'address': '4',
        'city': '5',
        'us_tax_id': '6',
        'signature': '7',
        'country': '8',
        'tax_id': '9',
        'country_citizen': '10'
    }
    if not optional:
        return required

    optional = {
        'reference_id': '11',
        'treaty_article': '12',
        'treaty_percent': '13',
        'treaty_income': '14',
        'treaty_reason': '15',
        'acting_capacity': '17'
    }
    required.update(optional)
    return required


@patreon.test.manage()
def test_w9_inserts_updates():
    user_id = 1
    assert_is_none(W9TaxForm.get(user_id))
    insert_or_update_w9(**w9_data())

    tax_form = W9TaxForm.get(user_id)
    ssn = tax_form.USTaxID

    assert_is_not_none(tax_form)

    assert_equals(tax_form.full_name, '2')
    assert_equals(tax_form.tax_class, '12')

    data = w9_data()

    for key in data.keys():
        data[key] = None

    data['user_id'] = 1
    data['full_name'] = 'cat'

    insert_or_update_w9(**data)

    assert_equals(tax_form.full_name, 'cat')
    assert_equals(tax_form.USTaxID, ssn)
    assert_true(encryption.looks_encrypted(ssn))

    assert_equals(type(get(user_id)), W9TaxForm)


@patreon.test.manage()
def test_w8_inserts_updates():
    user_id = 1
    assert_is_none(W8TaxForm.get(user_id))
    insert_or_update_w8(**w8_data())

    tax_form = W8TaxForm.get(user_id)

    assert_is_not_none(tax_form)

    assert_equals(tax_form.full_name, '2')
    assert_equals(tax_form.TreatyPercent, None)

    tax_form.delete()

    insert_or_update_w8(**w8_data(optional=True))

    tax_form = W8TaxForm.get(user_id)
    ssn = tax_form.USTaxID

    assert_is_not_none(tax_form)

    assert_equals(tax_form.full_name, '2')
    assert_equals(tax_form.TreatyPercent, '13')

    data = w8_data()

    for key in data.keys():
        data[key] = None

    data['user_id'] = 1
    data['full_name'] = 'cat'

    insert_or_update_w8(**data)

    assert_equals(tax_form.full_name, 'cat')
    assert_equals(tax_form.USTaxID, ssn)
    assert_true(encryption.looks_encrypted(ssn))

    assert_equals(type(get(user_id)), W8TaxForm)


@patreon.test.manage()
def test_encryption_decryption():
    encryption.public_key, encryption.private_key = rsa.newkeys(512)

    user_id = 1
    assert_is_none(W8TaxForm.get(user_id))
    insert_or_update_w8(**w8_data())

    tax_form = W8TaxForm.get(user_id)

    assert_is_not_none(tax_form)

    assert_equals('6', decrypt(tax_form.USTaxID))

