import datetime

from nose.tools import assert_equals, assert_is_not_none

import patreon
from patreon.model.manager import stats_mgr
from patreon.model.table import Stats
from patreon.services.db import db_session


def format_date(date):
    return date.strftime('%Y-%m-%d')


@patreon.test.manage()
def test_create_or_update():
    now = datetime.date.today()
    s = Stats(Key='foo', Date=now, Value=1.1)

    db_session.add(s)
    db_session.flush()

    # Query's Value hasn't changed, so it shouldn't update.
    _s = stats_mgr.create_or_update(s.Key, s.Date, s.Value)
    assert_equals(s.Key, _s.Key)
    assert_equals(format_date(s.Date), format_date(_s.Date))
    assert_equals(s.Value, _s.Value)

    # Update an existing value.
    _s = stats_mgr.create_or_update(s.Key, s.Date, 10.0)
    assert_equals(s.Key, _s.Key)
    assert_equals(format_date(s.Date), format_date(_s.Date))
    assert_equals(_s.Value, 10.0)

    # Create a new entry.
    _s = stats_mgr.create_or_update('bar', now, 5.5)
    added_stat = db_session.query(Stats).filter(Stats.Key == 'bar').first()
    assert_is_not_none(added_stat)
    assert_equals(added_stat.Key, 'bar')
    assert_equals(format_date(added_stat.Date), format_date(now))
    assert_equals(added_stat.Value, 5.5)

    # Make sure we're not adding additional rows when w're supposed to be
    # updating existing rows' values.
    assert_equals(db_session.query(Stats).count(), 2)
