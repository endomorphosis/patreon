from unittest.mock import patch
import time

from nose.plugins.attrib import attr
from nose.tools import *
import patreon
from patreon.model.manager import campaign_mgr, pledge_mgr, new_pledge_mgr, \
    complete_pledge_mgr, pledge_helper
from test.fixtures.card_fixtures import create_stripe_card
from test.fixtures.reward_fixtures import create_reward
from test.fixtures.user_fixtures import creator, patron
from test.test_helpers.user_helpers import get_new_logged_in_patron


def assert_campaign_creator(campaign_id, creator_id):
    campaign_id_2 = campaign_mgr.try_get_campaign_id_by_user_id_hack(creator_id)
    assert_equals(campaign_id, campaign_id_2)


# Test create
@patreon.test.manage()
def test_create_with_creator_id():
    creator_id = creator().user_id
    new_reward_id = create_reward(creator_id, 200).reward_id
    patron_user, patron_session = get_new_logged_in_patron()
    patron_id = patron_user.user_id

    # Create a pledge
    old_pledge, new_pledge = complete_pledge_mgr.create_or_update_pledge(
        patron_id=patron_id,
        amount=200,
        pledge_cap=600,
        reward_id=new_reward_id,
        card_id=3,
        creator_id=creator_id
    )

    # Check that the returned pledges have expected values
    assert_equals(old_pledge.patron_id, patron_id)
    assert_equals(new_pledge.user_id, patron_id)

    assert_equals(old_pledge.amount, 200)
    assert_equals(old_pledge.amount, new_pledge.amount_cents)

    assert_equals(old_pledge.pledge_cap, 600)
    assert_equals(old_pledge.pledge_cap, new_pledge.pledge_cap_amount_cents)

    assert_equals(old_pledge.reward_id, new_reward_id)
    assert_equals(new_pledge.reward_tier_id, new_reward_id)

    assert_equals(old_pledge.card_id, "3")
    # New pledge tables do not include the card #

    assert_equals(old_pledge.creator_id, creator_id)
    assert_campaign_creator(new_pledge.campaign_id, creator_id)

    # Check that the default for PatronPaysFees were set to default of false (0)
    assert_equals(old_pledge.PatronPaysFees, 0)
    assert_equals(new_pledge.patron_pays_fees, 0)


@patreon.test.manage()
def test_create_with_patron_pays_fee_on():
    creator_id = creator().user_id
    new_reward_id = create_reward(creator_id, 200).reward_id
    patron_user, patron_session = get_new_logged_in_patron()
    patron_id = patron_user.user_id

    # Create a pledge, where the patron will pay the fees
    # Create a pledge
    old_pledge, new_pledge = complete_pledge_mgr.create_or_update_pledge(
        patron_id=patron_id,
        amount=200,
        pledge_cap=600,
        reward_id=new_reward_id,
        card_id=3,
        creator_id=creator_id,
        patron_pays_pledge_fees=1
    )

    # Check that the returned pledges have expected values
    assert_equals(old_pledge.patron_id, patron_id)
    assert_equals(new_pledge.user_id, patron_id)

    assert_equals(old_pledge.amount, 200)
    assert_equals(old_pledge.amount, new_pledge.amount_cents)

    assert_equals(old_pledge.pledge_cap, 600)
    assert_equals(old_pledge.pledge_cap, new_pledge.pledge_cap_amount_cents)

    assert_equals(old_pledge.reward_id, new_reward_id)
    assert_equals(new_pledge.reward_tier_id, new_reward_id)

    assert_equals(old_pledge.card_id, "3")
    # New pledge tables do not include the card #

    assert_equals(old_pledge.creator_id, creator_id)
    assert_campaign_creator(new_pledge.campaign_id, creator_id)

    # Check the patron pays the fees
    assert_equals(old_pledge.PatronPaysFees, 1)
    assert_equals(new_pledge.patron_pays_fees, 1)


@patreon.test.manage()
def test_create_with_campaign_id():
    creator_id = creator().user_id
    campaign_id = campaign_mgr.try_get_campaign_id_by_user_id_hack(creator_id)
    new_reward_id = create_reward(creator_id, 200).reward_id
    patron_user, patron_session = get_new_logged_in_patron()
    patron_id = patron_user.user_id

    # Create a pledge
    old_pledge, new_pledge = complete_pledge_mgr.create_or_update_pledge(
        patron_id=patron_id,
        amount=200,
        pledge_cap=600,
        reward_id=new_reward_id,
        card_id=3,
        campaign_id=campaign_id
    )

    # Check that the returned pledges have expected values
    assert_equals(old_pledge.patron_id, patron_id)
    assert_equals(new_pledge.user_id, patron_id)

    assert_equals(old_pledge.amount, 200)
    assert_equals(old_pledge.amount, new_pledge.amount_cents)

    assert_equals(old_pledge.pledge_cap, 600)
    assert_equals(old_pledge.pledge_cap, new_pledge.pledge_cap_amount_cents)

    assert_equals(old_pledge.reward_id, new_reward_id)
    assert_equals(new_pledge.reward_tier_id, new_reward_id)

    assert_equals(old_pledge.card_id, "3")
    # New pledge tables do not include the card #

    assert_equals(old_pledge.creator_id, creator_id)
    assert_campaign_creator(new_pledge.campaign_id, creator_id)


@patreon.test.manage()
def test_get_pledge_by_campaign_and_creator():
    creator_id = creator().user_id
    new_reward_id = create_reward(creator_id, 200).reward_id
    patron_user, patron_session = get_new_logged_in_patron()
    patron_id = patron_user.user_id

    # Create a pledge
    old_pledge, new_pledge = complete_pledge_mgr.create_or_update_pledge(
        patron_id=patron_id,
        amount=200,
        pledge_cap=600,
        reward_id=new_reward_id,
        card_id=3,
        creator_id=creator_id)

    # Check that the returned pledges have expected values
    assert_equals(old_pledge.patron_id, patron_id)
    assert_equals(new_pledge.user_id, patron_id)

    assert_equals(old_pledge.amount, 200)
    assert_equals(old_pledge.amount, new_pledge.amount_cents)

    assert_equals(old_pledge.pledge_cap, 600)
    assert_equals(old_pledge.pledge_cap, new_pledge.pledge_cap_amount_cents)

    assert_equals(old_pledge.reward_id, new_reward_id)
    assert_equals(new_pledge.reward_tier_id, new_reward_id)

    assert_equals(old_pledge.card_id, "3")
    # New pledge tables do not include the card #

    assert_equals(old_pledge.creator_id, creator_id)
    assert_campaign_creator(new_pledge.campaign_id, creator_id)

    # Get the pledge by patron_id and campaign_id

    old_pledge_by_campaign, new_pledge_by_campaign = \
        complete_pledge_mgr.get_pledge_by_patron_and_campaign_or_creator(
            patron_id=patron_id,
            campaign_id=campaign_mgr.try_get_campaign_id_by_user_id_hack(creator_id)
        )

    assert_equals(old_pledge, old_pledge_by_campaign)
    assert_equals(new_pledge, new_pledge_by_campaign)


    # Get the pledge by patron_id and creator_id
    old_pledge_by_creator, new_pledge_by_creator = \
        complete_pledge_mgr.get_pledge_by_patron_and_campaign_or_creator(
            patron_id=patron_id,
            creator_id=creator_id
        )

    assert_equals(old_pledge, old_pledge_by_creator)
    assert_equals(new_pledge, new_pledge_by_creator)


@patreon.test.manage()
@attr(speed="slower")
def test_update_pledge():
    creator_id = creator().user_id
    new_reward_id = create_reward(creator_id, 200).reward_id
    patron_user, patron_session = get_new_logged_in_patron()
    patron_id = patron_user.user_id

    # Create a pledge
    old_pledge, new_pledge = complete_pledge_mgr.create_or_update_pledge(
        patron_id=patron_id,
        amount=200,
        pledge_cap=600,
        reward_id=new_reward_id,
        card_id=3,
        creator_id=creator_id)

    time.sleep(2)  # right now things explode in tblHistory if you edit a pledge twice in one second :-(


    # Check that the returned pledges have expected values
    assert_equals(old_pledge.patron_id, patron_id)
    assert_equals(new_pledge.user_id, patron_id)

    assert_equals(old_pledge.amount, 200)
    assert_equals(old_pledge.amount, new_pledge.amount_cents)

    assert_equals(old_pledge.pledge_cap, 600)
    assert_equals(old_pledge.pledge_cap, new_pledge.pledge_cap_amount_cents)

    assert_equals(old_pledge.reward_id, new_reward_id)
    assert_equals(new_pledge.reward_tier_id, new_reward_id)

    assert_equals(old_pledge.card_id, "3")
    # New pledge tables do not include the card #

    assert_equals(old_pledge.creator_id, creator_id)
    assert_campaign_creator(new_pledge.campaign_id, creator_id)

    # Update the pledge
    updated_old_pledge, updated_new_pledge = \
        complete_pledge_mgr.create_or_update_pledge(
            patron_id=patron_id,
            amount=300,
            pledge_cap=900,
            reward_id=new_reward_id,
            card_id=4,
            creator_id=creator_id
        )

    # Check that the returned pledges have expected values
    assert_equals(updated_old_pledge.patron_id, patron_id)
    assert_equals(updated_new_pledge.user_id, patron_id)

    assert_equals(updated_old_pledge.amount, 300)
    assert_equals(updated_new_pledge.amount_cents, 300)

    assert_equals(updated_old_pledge.pledge_cap, 900)
    assert_equals(updated_new_pledge.pledge_cap_amount_cents, 900)

    assert_equals(updated_old_pledge.reward_id, new_reward_id)
    assert_equals(updated_new_pledge.reward_tier_id, new_reward_id)

    assert_equals(updated_old_pledge.card_id, 4)
    # New pledge tables do not include the card #

    assert_equals(updated_old_pledge.creator_id, creator_id)
    assert_campaign_creator(new_pledge.campaign_id, creator_id)

    # Check that "get" retrieves the most updated pledges
    retrieved_old_pledge, retrieved_new_pledge = complete_pledge_mgr.get_pledge_by_patron_and_campaign_or_creator(
        patron_id=patron_id,
        creator_id=creator_id
    )

    assert_equals(updated_old_pledge, retrieved_old_pledge)
    assert_equals(updated_new_pledge, retrieved_new_pledge)


@patreon.test.manage()
@attr(speed="slower")
def test_delete_pledge():
    creator_id = creator().user_id
    campaign_id = campaign_mgr.try_get_campaign_id_by_user_id_hack(creator_id)
    new_reward_id = create_reward(creator_id, 200).reward_id
    patron_user, patron_session = get_new_logged_in_patron()
    patron_id = patron_user.user_id

    # Create a pledge
    old_pledge, new_pledge = complete_pledge_mgr.create_or_update_pledge(
        patron_id=patron_id,
        amount=200,
        pledge_cap=600,
        reward_id=new_reward_id,
        card_id=3,
        creator_id=creator_id)

    time.sleep(1)  # right now things explode in tblHistory if you edit a pledge twice in one second :-(

    # Check that the returned pledges have expected values
    assert_equals(old_pledge.patron_id, patron_id)
    assert_equals(new_pledge.user_id, patron_id)

    assert_equals(old_pledge.amount, 200)
    assert_equals(old_pledge.amount, new_pledge.amount_cents)

    assert_equals(old_pledge.pledge_cap, 600)
    assert_equals(old_pledge.pledge_cap, new_pledge.pledge_cap_amount_cents)

    assert_equals(old_pledge.reward_id, new_reward_id)
    assert_equals(new_pledge.reward_tier_id, new_reward_id)

    assert_equals(old_pledge.card_id, "3")
    # New pledge tables do not include the card #

    assert_equals(old_pledge.creator_id, creator_id)
    assert_equals(new_pledge.campaign_id, campaign_id)

    # Delete the pledge
    # complete_pledge_mgr.delete_pledge(patron_id=patron_id, campaign_id=2)

    pledge_mgr.delete_pledge(patron_id, campaign_id)
    new_pledge_mgr.delete_pledge(patron_id, campaign_id)

    time.sleep(1)  # right now things explode in tblHistory if you edit a pledge twice in one second :-(

    del_old_pledge = pledge_mgr.get(patron_id, creator_id)
    del_new_pledge = new_pledge_mgr.get_pledge_by_patron_and_campaign(patron_id, campaign_id)

    assert_is_none(del_old_pledge)
    assert_is_none(del_new_pledge)


@patreon.test.manage()
def test_NamedTupleExperiment():
    """
    Test that decorater method to_legacy_and_new_pledge_tuple
    works correctly, given a toy method returnTuple
    :return:
    """

    @pledge_helper.to_legacy_and_new_pledge_tuple
    def returnTuple():
        return 1, 2

    val = returnTuple()

    assert_equals(val.legacy, 1)
    assert_equals(val.new, 2)


class fake_exception(Exception):
    pass


@patreon.test.manage()
@patch('patreon.model.manager.new_pledge_mgr.make_or_update_pledge')
def test_transaction_handling_create_or_update(mock):
    mock.side_effect = fake_exception

    creator_id = creator().user_id
    patron_id = patron().user_id
    reward_id = create_reward(creator_id, 100).reward_id
    card = create_stripe_card(patron_id)

    # This exception should never be thrown by that code
    assert_is_none(pledge_mgr.get(patron_id, creator_id))

    with assert_raises(fake_exception):
        complete_pledge_mgr.create_or_update_pledge(
            patron_id, 1000, 1000, reward_id, card,
            creator_id=creator_id
        )

    assert_is_none(pledge_mgr.get(patron_id, creator_id))


@patreon.test.manage()
@patch('patreon.model.manager.pledge_mgr.delete_pledge')
def test_transaction_handling_delete(mock):
    mock.side_effect = fake_exception

    creator_id = creator().user_id
    campaign_id = campaign_mgr.try_get_campaign_id_by_user_id_hack(creator_id)
    patron_id = patron().user_id
    reward_id = create_reward(creator_id, 100).reward_id
    card = create_stripe_card(patron_id)

    # This exception should never be thrown by that code
    assert_is_none(pledge_mgr.get(patron_id, creator_id))

    complete_pledge_mgr.create_or_update_pledge(
        patron_id, 1000, 1000, reward_id, card,
        creator_id=creator_id
    )

    assert_is_not_none(pledge_mgr.get(patron_id, creator_id))

    with assert_raises(fake_exception):
        complete_pledge_mgr.delete_pledge(patron_id, creator_id=creator_id)

    assert_is_not_none(
        new_pledge_mgr.get_pledge_by_patron_and_campaign(
            patron_id,
            campaign_id
        )
    )
