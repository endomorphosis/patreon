from nose.tools import *
import patreon
from patreon.constants.transaction_types import TxnTypes
from patreon.model.manager import payment_mgr
from patreon.model.table.transactions import Txn

__author__ = 'zennenga'


@patreon.test.manage()
def test_basic_cases():
    user_id = 1
    transaction_id = payment_mgr.create_and_process_transaction_and_obligation(TxnTypes.credit_adjustment,
                                                                               user_id=user_id,
                                                                               amount_cents=10)
    transaction = Txn.get(transaction_id)
    assert_equals(10, payment_mgr.get_credit_total(user_id))
    assert_equals(2, len(transaction.ledger_entries))

    transaction_id = payment_mgr.create_and_process_transaction_and_obligation(TxnTypes.credit_adjustment,
                                                                               user_id=user_id,
                                                                               amount_cents=10)
    transaction = Txn.get(transaction_id)
    assert_equals(20, payment_mgr.get_credit_total(user_id))
    assert_equals(2, len(transaction.ledger_entries))

    transaction_id = payment_mgr.create_and_process_transaction_and_obligation(TxnTypes.credit_adjustment,
                                                                               user_id=user_id,
                                                                               amount_cents=0)
    transaction = Txn.get(transaction_id)
    assert_equals(20, payment_mgr.get_credit_total(user_id))
    assert_equals(0, len(transaction.ledger_entries))

    transaction_id = payment_mgr.create_and_process_transaction_and_obligation(TxnTypes.credit_adjustment,
                                                                               user_id=user_id,
                                                                               amount_cents=-10)
    transaction = Txn.get(transaction_id)
    assert_equals(10, payment_mgr.get_credit_total(user_id))
    assert_equals(2, len(transaction.ledger_entries))

    transaction_id = payment_mgr.create_and_process_transaction_and_obligation(TxnTypes.credit_adjustment,
                                                                               user_id=user_id,
                                                                               amount_cents=-10)
    transaction = Txn.get(transaction_id)
    assert_equals(0, payment_mgr.get_credit_total(user_id))
    assert_equals(2, len(transaction.ledger_entries))


@patreon.test.manage()
def test_deducting_more_than_exist():
    user_id = 1
    payment_mgr.add_credit(user_id, 10)
    assert_equals(payment_mgr.get_credit_total(user_id), 10)

    payment_mgr.add_credit(user_id, 0)
    assert_equals(payment_mgr.get_credit_total(user_id), 10)

    payment_mgr.remove_credit(user_id, 0)
    assert_equals(payment_mgr.get_credit_total(user_id), 10)

    payment_mgr.remove_credit(user_id, 20)
    assert_equals(payment_mgr.get_credit_total(user_id), 0)
