from nose.tools import assert_equals
import patreon
from patreon.model.manager import vat_mgr


@patreon.test.manage()
def test_calculate_vat():
    vat_amount = vat_mgr.calculate_vat_on_amount_for_country(100, "GB")  # UK is 20%
    assert_equals(vat_amount, 20)

    vat_amount = vat_mgr.calculate_vat_on_amount_for_country(110, "GB")  # UK is 20%
    assert_equals(vat_amount, 22)

    # rounding down
    vat_amount = vat_mgr.calculate_vat_on_amount_for_country(111, "GB")  # UK is 20%
    assert_equals(vat_amount, 22)

    # rounding up
    vat_amount = vat_mgr.calculate_vat_on_amount_for_country(113, "GB")  # UK is 20%
    assert_equals(vat_amount, 23)

    # rounding up from exactly a half-cent
    vat_amount = vat_mgr.calculate_vat_on_amount_for_country(102, "HR")  # Croatia is 25%
    assert_equals(vat_amount, 26)

    vat_amount = vat_mgr.calculate_vat_on_amount_for_country(100, "US")  # USA is not EU
    assert_equals(vat_amount, 0)

    vat_amount = vat_mgr.calculate_vat_on_amount_for_country(100, None)  # No country specified, no vat
    assert_equals(vat_amount, 0)


@patreon.test.manage()
def test_geoip_data():
    assert_equals(vat_mgr.get_country_by_ip_address('128.101.101.101'), 'US')


@patreon.test.manage()
def test_geoip_vpn_address():
    assert_equals(vat_mgr.get_country_by_ip_address('172.31.13.103'), '??')


@patreon.test.manage()
def test_iso_name():
    assert_equals('United States', vat_mgr.get_friendly_name_by_country_code('US'))
