import patreon
from patreon.constants.money import BIG_MONEY_CENTS
from patreon.exception.payment_errors import *
from patreon.model.manager import payment_event_mgr, payment_mgr
from patreon.services.payment_apis import StripeAPI, PayPalAPI
from nose.tools import *
from patreon import util
from test.fixtures.card_fixtures import create_card, create_stripe_card


def _assert_exception_event_log(api, low_level_exception_class_name):
    bad_id = 'Not-A-Transaction-ID'
    with assert_raises(InvalidAPIRequest):
        api.get_transaction_status(bad_id)

    timedelta = util.datetime.offset_from_time(minutes=-5)

    events = payment_event_mgr.get_all_events_after_time(timedelta)

    assert_equals(1, len(events))

    event_data = events[0].json_data

    assert_equals('InvalidAPIRequest', event_data['type'])
    assert_equals(low_level_exception_class_name, event_data['original_exception']['type'])

    events = payment_event_mgr.get_events_of_type_after_time(timedelta, 'exception')
    assert_equals(1, len(events))

    events = payment_event_mgr.get_events_of_type_after_time(timedelta, 'payments')
    assert_equals(0, len(events))


@patreon.test.manage()
def test_stripe_payment_event():
    api = StripeAPI()

    _assert_exception_event_log(api, 'InvalidRequestError')


@patreon.test.manage()
def test_paypal_payment_event():
    try:
        api = PayPalAPI()

        _assert_exception_event_log(api, 'GenericPaypalAPIError')
    except (ConnectionError, TemporaryOutage):
        pass


@patreon.test.manage()
def test_big_money_card_not_found_event():
    with assert_raises(CardNotFound):
        payment_mgr.charge_big_money(1, "bad_card", BIG_MONEY_CENTS)

    timedelta = util.datetime.offset_from_time(minutes=-5)

    events = payment_event_mgr.get_events_of_type_after_time(timedelta, "exception")

    assert_equals(1, len(events))

    event_data = events[0].json_data

    assert_equals('CardNotFound', event_data['type'])
    assert_false('original_exception' in event_data)


@patreon.test.manage()
def test_big_money_bad_card_event():
    card_id = 'cus_bad_customer'
    create_card(1, card_id)

    with assert_raises(BigMoneyChargeError):
        payment_mgr.charge_big_money(1, card_id, BIG_MONEY_CENTS)

    timedelta = util.datetime.offset_from_time(minutes=-5)

    events = payment_event_mgr.get_events_of_type_after_time(timedelta, "exception")

    assert_equals(2, len(events))

    first_event_data = events[1].json_data
    assert_equals('InvalidAPIRequest', first_event_data['type'])
    assert_equals('InvalidRequestError', first_event_data['original_exception']['type'])

    second_event_data = events[0].json_data
    assert_equals('BigMoneyChargeError', second_event_data['type'])
    assert_equals(first_event_data, second_event_data['original_exception'])

    events = payment_event_mgr.get_events_of_type_after_time(timedelta, "payment")

    assert_equals(1, len(events))
    assert_equals('payment', events[0].type)
    assert_equals('big_money', events[0].subtype)

    first_event_data = events[0].json_data
    assert_equals(1, first_event_data['user_id'])
    assert_equals(BIG_MONEY_CENTS, first_event_data['amount_cents'])
    assert_equals('failed', first_event_data['status'])


@patreon.test.manage()
def test_big_money_success_event():
    card_id = create_stripe_card(1)

    payment_mgr.charge_big_money(1, card_id, BIG_MONEY_CENTS)

    timedelta = util.datetime.offset_from_time(minutes=-5)

    events = payment_event_mgr.get_events_of_type_after_time(timedelta, "exception")

    assert_equals(0, len(events))

    events = payment_event_mgr.get_events_of_type_after_time(timedelta, "payment")

    assert_equals(1, len(events))
    assert_equals('payment', events[0].type)
    assert_equals('big_money', events[0].subtype)

    first_event_data = events[0].json_data
    assert_equals(1, first_event_data['user_id'])
    assert_equals(BIG_MONEY_CENTS, first_event_data['amount_cents'])
    assert_equals('succeeded', first_event_data['status'])


@patreon.test.manage()
def test_json_data_filtering():
    payment_event_mgr.add_big_money_event(1, 1, True)
    payment_event_mgr.add_big_money_event(2, 1, True)
    payment_event_mgr.add_big_money_event(2, 1, True)
    payment_event_mgr.add_big_money_event(3, 1, True)
    payment_event_mgr.add_exception_event(Exception())

    events_1 = payment_event_mgr.get_user_events(1)
    events_2 = payment_event_mgr.get_user_events(2)

    assert_equals(1, len(events_1))
    assert_equals(2, len(events_2))

    assert_equals(1, events_1[0].user_id)
    assert_equals(2, events_2[0].user_id)
    assert_equals(2, events_2[1].user_id)

    assert_equals(None, events_1[0].get_data_member('cat'))
