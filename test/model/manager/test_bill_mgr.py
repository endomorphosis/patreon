import time
from nose.plugins.attrib import attr
import sqlalchemy

from patreon.exception.db_errors import BadEnumType
from nose.tools import *
import patreon
from patreon.model.manager import campaign_mgr, complete_pledge_mgr, bill_mgr, rewards_give_mgr
from patreon.model.type import BillType
from patreon.util import datetime
from test.fixtures import post_fixtures
from test.fixtures.reward_fixtures import create_reward
from test.fixtures.user_fixtures import creator
from test.test_helpers.user_helpers import get_new_logged_in_patron, get_new_logged_in_creator


@patreon.test.manage()
def test_create_and_get_bill_by_patron():
    creator_id = creator().user_id
    campaign_id = campaign_mgr.try_get_campaign_id_by_user_id_hack(creator_id)
    new_reward_id = create_reward(creator_id, 200).reward_id
    patron_user, patron_session = get_new_logged_in_patron()

    # Create a pledge
    old_pledge, new_pledge = complete_pledge_mgr.create_or_update_pledge(
        patron_id=patron_user.user_id,
        amount=200,
        pledge_cap=600,
        reward_id=new_reward_id,
        card_id=3,
        campaign_id=campaign_id
    )

    # Create a bill
    bill_mgr.create_bill(
        user_id=patron_user.user_id,
        campaign_id=campaign_id,
        pledge_id=new_pledge.pledge_id,
        vat_charge_id=None,
        amount_cents=200,
        bill_type=BillType.monthly,
        post_id=1,
        billing_cycle=None,
        created_at=datetime.datetime.now()
    )

    bill_mgr.clear_post_cache(1)

    # Get bill
    retrieved_bills = bill_mgr.get_bills_for_patron(patron_user.user_id)

    assert_equals(len(retrieved_bills), 1)  # Check that only 1 bill was retrieved

    assert_equals(retrieved_bills[0].user_id, patron_user.user_id)
    assert_equals(retrieved_bills[0].campaign_id, campaign_id)
    assert_equals(retrieved_bills[0].amount_cents, 200)
    assert_equals(retrieved_bills[0].type, BillType.monthly)
    assert_equals(retrieved_bills[0].deleted_at, None)


@patreon.test.manage()
def test_create_and_get_bill_by_post():
    creator_id = creator().user_id
    new_reward_id = create_reward(creator_id, 200).reward_id
    patron_user, patron_session = get_new_logged_in_patron()
    campaign_id = campaign_mgr.try_get_campaign_id_by_user_id_hack(creator_id)

    # Create a pledge
    old_pledge, new_pledge = complete_pledge_mgr.create_or_update_pledge(
        patron_id=patron_user.user_id,
        amount=200,
        pledge_cap=600,
        reward_id=new_reward_id,
        card_id=3,
        campaign_id=campaign_id
    )

    # Create a bill
    bill_mgr.create_bill(
        user_id=patron_user.user_id,
        campaign_id=campaign_id,
        pledge_id=new_pledge.pledge_id,
        vat_charge_id=None,
        amount_cents=200,
        bill_type=BillType.monthly,
        post_id=1,
        billing_cycle=None
    )

    retrieved_bills = bill_mgr.get_bills_for_post(post_id=1)

    assert_equals(len(retrieved_bills), 1)  # Check that only 1 bill was retrieved

    assert_equals(retrieved_bills[0].user_id, patron_user.user_id)
    assert_equals(retrieved_bills[0].campaign_id, campaign_id)
    assert_equals(retrieved_bills[0].amount_cents, 200)
    assert_equals(retrieved_bills[0].type, BillType.monthly)


@patreon.test.manage()
def test_create_and_get_bill_by_campaign():
    creator_id = creator().user_id
    new_reward_id = create_reward(creator_id, 200).reward_id
    patron_user, patron_session = get_new_logged_in_patron()
    campaign_id = campaign_mgr.try_get_campaign_id_by_user_id_hack(creator_id)

    # Create a pledge
    old_pledge, new_pledge = complete_pledge_mgr.create_or_update_pledge(
        patron_id=patron_user.user_id,
        amount=200,
        pledge_cap=600,
        reward_id=new_reward_id,
        card_id=3,
        campaign_id=campaign_id
    )

    # Create a bill
    bill_mgr.create_bill(
        user_id=patron_user.user_id,
        campaign_id=campaign_id,
        pledge_id=new_pledge.pledge_id,
        vat_charge_id=None,
        amount_cents=200,
        bill_type=BillType.monthly,
        post_id=1,
        billing_cycle=None,
        created_at=datetime.datetime.now()
    )

    retrieved_bills = bill_mgr.get_bills_for_campaign(campaign_id)

    assert_equals(len(retrieved_bills), 1)  # Check that only 1 bill was retrieved

    assert_equals(retrieved_bills[0].user_id, patron_user.user_id)
    assert_equals(retrieved_bills[0].campaign_id, campaign_id)
    assert_equals(retrieved_bills[0].amount_cents, 200)
    assert_equals(retrieved_bills[0].type, BillType.monthly)


@patreon.test.manage()
@attr(speed="slower")
def test_create_and_delete_bill_by_post():
    creator_id = creator().user_id
    new_reward_id = create_reward(creator_id, 200).reward_id
    patron_user, patron_session = get_new_logged_in_patron()
    campaign_id = campaign_mgr.try_get_campaign_id_by_user_id_hack(creator_id)

    # Create a pledge
    old_pledge, new_pledge = complete_pledge_mgr.create_or_update_pledge(
        patron_id=patron_user.user_id,
        amount=200,
        pledge_cap=600,
        reward_id=new_reward_id,
        card_id=3,
        campaign_id=campaign_id
    )

    # Create a bill
    bill_id = bill_mgr.create_bill(
        user_id=patron_user.user_id,
        campaign_id=campaign_id,
        pledge_id=new_pledge.pledge_id,
        vat_charge_id=None,
        amount_cents=200,
        bill_type=BillType.monthly,
        post_id=1,
        billing_cycle=None,
        created_at=datetime.datetime.now()
    )

    # Delete the bill
    bill_mgr.delete_by_post_id(1)

    time.sleep(1)  # Wait a second so we can make sure the bill was deleted in the past

    # Retrieve the bill
    retrieved_bills = bill_mgr.get_bills_for_campaign(campaign_id)

    assert_equals(len(retrieved_bills), 1)  # Check that only 1 bill was retrieved

    # Check that it has been marked as deleted
    assert_is_not_none(retrieved_bills)
    assert_less(retrieved_bills[0].deleted_at, datetime.datetime.now())

    # Check that the other fields are what we expect
    assert_equals(retrieved_bills[0].user_id, patron_user.user_id)
    assert_equals(retrieved_bills[0].campaign_id, campaign_id)
    assert_equals(retrieved_bills[0].amount_cents, 200)
    assert_equals(retrieved_bills[0].type, BillType.monthly)


@patreon.test.manage()
@attr(speed="slower")
def test_create_and_delete_bill_by_campaign():
    creator_id = creator().user_id
    new_reward_id = create_reward(creator_id, 200).reward_id
    patron_user, patron_session = get_new_logged_in_patron()
    campaign_id = campaign_mgr.try_get_campaign_id_by_user_id_hack(creator_id)

    # Create a pledge
    old_pledge, new_pledge = complete_pledge_mgr.create_or_update_pledge(
        patron_id=patron_user.user_id,
        amount=200,
        pledge_cap=600,
        reward_id=new_reward_id,
        card_id=3,
        campaign_id=campaign_id
    )

    # Create a bill
    bill_id = bill_mgr.create_bill(
        user_id=patron_user.user_id,
        campaign_id=campaign_id,
        pledge_id=new_pledge.pledge_id,
        vat_charge_id=None,
        amount_cents=200,
        bill_type=BillType.per_post,
        post_id=1,
        billing_cycle=None,
        created_at=datetime.datetime.now()
    )

    # Delete the bill
    bill_mgr.delete_by_user_id_campaign_id(patron_user.user_id, campaign_id)

    time.sleep(1)  # Wait a second so we can make sure the bill was deleted in the past

    # Retrieve the bill
    retrieved_bills = bill_mgr.get_bills_for_campaign(campaign_id)

    # Check that it has been marked as deleted
    assert_is_not_none(retrieved_bills)
    assert_less(retrieved_bills[0].deleted_at, datetime.datetime.now())

    # Check that the other fields are what we expect
    assert_equals(len(retrieved_bills), 1)  # Check that only 1 bill was retrieved
    assert_equals(retrieved_bills[0].user_id, patron_user.user_id)
    assert_equals(retrieved_bills[0].campaign_id, campaign_id)
    assert_equals(retrieved_bills[0].amount_cents, 200)
    assert_equals(retrieved_bills[0].type, BillType.per_post)


# Note: This test checks that an error is thrown if the bill_type isn't allowed by the column.
# It's commented out because an error (from mysql?) is thrown despite the except clause.
@patreon.test.manage()
def test_create_bill_with_bad_type():
    creator_id = creator().user_id
    new_reward_id = create_reward(creator_id, 200).reward_id
    patron_user, patron_session = get_new_logged_in_patron()
    campaign_id = campaign_mgr.try_get_campaign_id_by_user_id_hack(creator_id)

    # Create a pledge
    old_pledge, new_pledge = complete_pledge_mgr.create_or_update_pledge(
        patron_id=patron_user.user_id,
        amount=200,
        pledge_cap=600,
        reward_id=new_reward_id,
        card_id=3,
        campaign_id=campaign_id
    )

    # Create a bill
    with assert_raises(sqlalchemy.exc.StatementError) as e:
        bill_mgr.create_bill(
            user_id=patron_user.user_id,
            campaign_id=campaign_id,
            pledge_id=new_pledge.pledge_id,
            vat_charge_id=None,
            amount_cents=200,
            bill_type="unknown_type",
            post_id=1,
            billing_cycle=None,
            created_at=datetime.datetime.now()
        )
    # Check that the exception is what we expect
    exception = e.exception
    assert_in("unknown_type", str(exception))
    assert_in("Bills.type", str(exception))


# Test make_post_mgr._create_bill_and_notify_patrons,
# which now modifies the rewards_give and bill tables.
@patreon.test.manage()
@attr(speed="slower")
def test_create_bill_and_notify_patrons_helper():
    # 1. Login as a creator
    creator_user, session = get_new_logged_in_creator()
    creator_id = creator_user.user_id
    campaign_id = campaign_mgr.try_get_campaign_id_by_user_id_hack(creator_id)
    new_reward_id = create_reward(creator_id, 200).reward_id

    # 2. Log in as patron
    patron_user, patron_session = get_new_logged_in_patron()

    # 3. The patron pledges to the creator, per-post
    complete_pledge_mgr.create_or_update_pledge(
        patron_id=patron_user.user_id,
        amount=400,
        pledge_cap=800,
        reward_id=new_reward_id,
        card_id=3,
        campaign_id=campaign_id
    )

    # 4. Create the post - the bill is also created
    post = post_fixtures.create_post(campaign_id, creator_id)
    post_id = post.activity_id

    # 5. Retrieve the bill for rewards_give and bill tables.
    bill_retrieved = bill_mgr.get_bills_for_campaign(campaign_id)
    rewards_give_retrieved = rewards_give_mgr.get_bills_for_post(post_id)

    # Check that 1 bill was retrieved and it has the expected fields
    assert_equals(len(bill_retrieved), 1)

    assert_equals(bill_retrieved[0].amount_cents, rewards_give_retrieved[0].amount)
    assert_equals(bill_retrieved[0].campaign_id, campaign_id)
    assert_equals(rewards_give_retrieved[0].creator_id, creator_id)
    assert_equals(rewards_give_retrieved[0].reward_id, new_reward_id)
