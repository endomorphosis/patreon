from nose.tools import *
import patreon
from patreon.constants.account_types import SystemAccounts
from patreon.model.manager.account_mgr import touch_system_account, \
    touch_user_credit_account_id
from patreon.services.db import db_session


@patreon.test.manage()
def test_create_account_unique():
    id1 = touch_system_account(SystemAccounts.EXTERNAL_TRANSACTION_FEE_SINK)
    db_session.commit()
    id2 = touch_system_account(SystemAccounts.EXTERNAL_TRANSACTION_FEE_SINK)
    db_session.commit()
    id3 = touch_system_account(SystemAccounts.EXTERNAL_TRANSACTION_FEE_SINK)
    db_session.commit()
    assert_equals(id1, id2)
    assert_equals(id1, id3)

    id1 = touch_user_credit_account_id(1)
    db_session.commit()
    id2 = touch_user_credit_account_id(1)
    db_session.commit()
    id3 = touch_user_credit_account_id(1)
    db_session.commit()
    assert_equals(id1, id2)
    assert_equals(id1, id3)
