import datetime
from unittest.mock import patch

from nose.tools import *
import patreon
from patreon.constants.transaction_types import TxnTypes
from patreon.exception.payment_errors import *
from patreon.model.manager import payment_mgr, credit_purchase_mgr
from patreon.model.table.ledger_entries import LedgerEntry
from patreon.model.table.transactions import CreditPurchaseTxn
from patreon.services.payment_apis import StripeAPI
from test.fixtures.card_fixtures import create_stripe_card

__author__ = 'zennenga'


@patreon.test.manage()
@patch.object(StripeAPI, 'charge_instrument')
def test_credit_purchase_basic_codepaths(mock):
    user_id = 1
    card_token = 'cus_cat'
    amount_cents = 100
    campaign_id = 1
    mock.return_value = ('1', 2)

    # Base case, new credit purchase succeeds
    transaction_id = payment_mgr.create_and_process_transaction_and_obligation(TxnTypes.credit_purchase,
                                                                               card_token,
                                                                               amount_cents,
                                                                               user_id,
                                                                               campaign_id)
    transaction = CreditPurchaseTxn.get(transaction_id)
    assert_equals(amount_cents, payment_mgr.get_credit_total(user_id))
    assert_true(transaction.is_successful)
    assert_true(transaction.credit_purchase.is_successful)
    assert_true(transaction.most_recent_external_payment.is_successful)
    external_transaction_id = transaction.most_recent_external_payment.external_id
    assert_equals(external_transaction_id, '1')
    assert_equals(transaction.most_recent_external_payment.fee_cents, 2)

    assert_equals(transaction_id, payment_mgr.process_transaction(transaction_id))
    assert_equals(amount_cents, payment_mgr.get_credit_total(user_id))
    assert_false(transaction.is_processing)
    LedgerEntry.query.filter_by(txn_id=transaction_id).delete()
    assert_equals(0, payment_mgr.get_credit_total(user_id))

    # Transcation has no status, external payment marked as successful
    # Can occur due to crashes
    # Recovery involves marking transaction successful, adding ledger entries
    transaction.update({
        'succeeded_at': None,
        'failed_at': None
    })

    assert_equals(transaction_id, payment_mgr.process_transaction(transaction_id))
    assert_equals(amount_cents, payment_mgr.get_credit_total(user_id))
    LedgerEntry.query.filter_by(txn_id=transaction_id).delete()
    assert_equals(0, payment_mgr.get_credit_total(user_id))
    assert_false(transaction.is_processing)

    # Transaction marked as failed, payment claims success
    # This implies the transaction failed after the external payment succeeded
    # Recovery involves inserting ledger entries and marking all relevant objects as successful
    transaction.update({
        'succeeded_at': None,
        'failed_at': datetime.datetime.now()
    })

    assert_equals(transaction_id, payment_mgr.process_transaction(transaction_id))
    assert_equals(amount_cents, payment_mgr.get_credit_total(user_id))
    assert_true(transaction.is_successful)
    assert_true(transaction.credit_purchase.is_successful)
    assert_true(transaction.most_recent_external_payment.is_successful)
    LedgerEntry.query.filter_by(txn_id=transaction_id).delete()
    assert_equals(0, payment_mgr.get_credit_total(user_id))
    assert_false(transaction.is_processing)

    # Transaction not marked as any status, external payment marked as failed
    # This is a crash case, should not happen during normal code execution
    # Recovery involves marking the transaction and credit purchase as failed
    transaction.update({
        'succeeded_at': None,
        'failed_at': None
    })

    transaction.most_recent_external_payment.update({
        'succeeded_at': None,
        'failed_at': datetime.datetime.now()
    })

    with assert_raises(ExternalPaymentAlreadyFailed):
        payment_mgr.process_transaction(transaction_id)

    assert_true(transaction.is_failed)
    assert_true(transaction.credit_purchase.is_failed)
    assert_true(transaction.most_recent_external_payment.is_failed)
    assert_equals(0, payment_mgr.get_credit_total(user_id))
    assert_false(transaction.is_processing)

    # Transaction is re-run after being marked as failed (in all necessary places)
    transaction.update({
        'succeeded_at': None,
        'failed_at': datetime.datetime.now()
    })

    transaction.most_recent_external_payment.update({
        'succeeded_at': None,
        'failed_at': datetime.datetime.now()
    })

    with assert_raises(TransactionAlreadyFailed):
        payment_mgr.process_transaction(transaction_id)

    assert_true(transaction.is_failed)
    assert_true(transaction.credit_purchase.is_failed)
    assert_true(transaction.most_recent_external_payment.is_failed)
    assert_equals(0, payment_mgr.get_credit_total(user_id))
    assert_false(transaction.is_processing)

    # Basic case - Transaction is marked as processing, don't try to process on top of it
    transaction.update({
        'succeeded_at': None,
        'failed_at': None,
        'began_processing_at': datetime.datetime.now()
    })

    with assert_raises(TransactionInProcess):
        payment_mgr.process_transaction(transaction_id)

    assert_true(transaction.is_processing)
    assert_equals(0, payment_mgr.get_credit_total(user_id))

    # Most important assertion - The card is only ever charged ONCE.
    assert_equals(mock.call_count, 1)


class test_exception(Exception):
    pass


@patreon.test.manage()
@patch.object(StripeAPI, 'charge_instrument')
@patch('patreon.model.manager.credit_purchase_mgr.mark_transaction_as_processing')
def test_fail_first_block(mock, mock2):
    user_id = 1
    card_token = create_stripe_card(user_id)
    amount_cents = 100
    campaign_id = 1
    mock.side_effect = test_exception
    mock2.return_value = ('1', 2)
    transaction_id = credit_purchase_mgr.initialize(card_token, amount_cents, user_id, campaign_id)

    with assert_raises(test_exception):
        payment_mgr.process_transaction(transaction_id)

    transaction = CreditPurchaseTxn.get(transaction_id)
    assert_false(
        transaction.is_failed or transaction.is_successful or transaction.is_processing or transaction.is_pending)


@patreon.test.manage()
@patch.object(StripeAPI, 'charge_instrument')
@patch('patreon.model.manager.credit_purchase_mgr.execute')
def test_fail_payment_state(mock, mock2):
    user_id = 1
    card_token = create_stripe_card(user_id)
    amount_cents = 100
    campaign_id = 1
    mock.side_effect = test_exception
    mock2.return_value = ('1', 2)
    transaction_id = credit_purchase_mgr.initialize(card_token, amount_cents, user_id, campaign_id)

    with assert_raises(test_exception):
        payment_mgr.process_transaction(transaction_id)

    transaction = CreditPurchaseTxn.get(transaction_id)
    assert_true(transaction.is_failed)
    assert_true(transaction.most_recent_external_payment.is_failed)
    assert_false(transaction.is_processing)


@patreon.test.manage()
@patch.object(StripeAPI, 'charge_instrument')
@patch('patreon.model.manager.ledger_entry_mgr.assert_transaction_ledger_sane')
def test_fail_ledger(mock, mock2):
    user_id = 1
    card_token = create_stripe_card(user_id)
    amount_cents = 100
    campaign_id = 1
    mock.side_effect = test_exception
    mock2.return_value = ('1', 2)
    transaction_id = credit_purchase_mgr.initialize(card_token, amount_cents, user_id, campaign_id)

    with assert_raises(test_exception):
        payment_mgr.process_transaction(transaction_id)

    transaction = CreditPurchaseTxn.get(transaction_id)
    assert_true(transaction.is_failed)
    assert_true(transaction.most_recent_external_payment.is_successful)
    assert_false(transaction.is_processing)


@patreon.test.manage()
@patch.object(StripeAPI, 'charge_instrument')
@patch('patreon.model.manager.ledger_entry_mgr.assert_transaction_ledger_sane')
@patch('patreon.model.manager.credit_purchase_mgr.mark_transaction_as_failed')
def test_fail_marking_failed(mock, mock3, mock2):
    user_id = 1
    card_token = create_stripe_card(user_id)
    amount_cents = 100
    campaign_id = 1
    mock.side_effect = test_exception
    mock3.side_effect = test_exception
    mock2.return_value = ('1', 2)
    transaction_id = credit_purchase_mgr.initialize(card_token, amount_cents, user_id, campaign_id)

    with assert_raises(test_exception):
        payment_mgr.process_transaction(transaction_id)

    transaction = CreditPurchaseTxn.get(transaction_id)
    assert_false(transaction.is_failed)
    assert_true(transaction.most_recent_external_payment.is_failed)
    assert_false(transaction.is_processing)
