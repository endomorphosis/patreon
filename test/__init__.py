import random

from patreon.model.manager import user_location_mgr, user_mgr
from patreon.model.table import User
from patreon.util.random import randomstring
from . import fixtures


def register_user_helper(update_dict={}):
    user_info_dict = {
        'email': randomstring() + '@' + randomstring() + '.com',
        'password': randomstring(),
        'vanity': randomstring().capitalize(),
        'first_name': randomstring().capitalize(),
        'last_name': randomstring().capitalize(),
        'facebook_id': None}

    user_info_dict.update(update_dict)

    token = user_mgr.register_user(
        user_info_dict['email'], user_info_dict['password'],
        user_info_dict['vanity'], user_info_dict['first_name'],
        user_info_dict['last_name'], user_info_dict['facebook_id']
    )

    if user_info_dict['facebook_id'] is None:
        user_id = User.get_unique(email=user_info_dict['email']).UID
    else:
        user_id = User.get_unique(facebook_id=user_info_dict['facebook_id']).UID

    user_info_dict['token'] = token
    user_info_dict['user_id'] = user_id
    return user_info_dict


def user_location_helper(user_id):
    lat = round(random.uniform(-10, 10), 10)
    long = round(random.uniform(-10, 10), 10)
    location_name = "Patreon HQ"
    return user_location_mgr.add_user_location(user_id, lat, long, location_name)
