from nose.tools import *
from patreon.util import format


def test_format_money():
    assert_equals("1", format.format_money(1.00))
    assert_equals("0.50", format.format_money(0.5, 2))
    assert_equals("0.50", format.format_money("0.50", 2))
