from flask import session
import patreon

from nose.tools import *
from patreon.model.table import Session
from patreon.services import db
from patreon.test import asserttest
from test.fixtures.session_fixtures import create_web_session


@asserttest()
def create_session():
    session_id = '1'
    user_id = 2
    csrf_token = 'csrf_token'
    csrf_token_expiration = '2015-02-10 11:46:41'
    is_admin = 1
    extra_data = '{}'

    db.query("""
                INSERT INTO sessions_new
                  (session_token, user_id, csrf_token, csrf_token_expires_at, is_admin, extra_data_json,
                  created_at, expires_at)
                VALUES
                  (%s, %s, %s, %s, %s, %s, NOW(), (NOW() + INTERVAL %s SECOND))
            """, [session_id, user_id, csrf_token, csrf_token_expiration, is_admin, extra_data,
                  patreon.globalvars.get('SESSION_LIFETIME')])

    db.test_dirty_table.add(Session)

    return {'session_id': session_id,
            'user_id': user_id,
            'csrf_token': csrf_token,
            'csrf_token_expires_at': csrf_token_expiration,
            'is_admin': is_admin,
            'extra_data_json': extra_data}


@patreon.test.manage()
def test_get_session():
    session_data = create_session()
    session_interface = patreon.model.request.session.PatreonSessionInterface()
    session_instance = session_interface.get_session(session_data.get('session_id'), True)

    assert_equals(session_instance.session_id, session_data.get('session_id'))
    assert_equals(session_instance.user_id, session_data.get('user_id'))
    assert_equals(session_instance.csrf_token, session_data.get('csrf_token'))
    assert_equals(str(session_instance.csrf_token_expiration), session_data.get('csrf_token_expires_at'))
    assert_equals(session_instance.is_admin, session_data.get('is_admin') == 1)
    assert_equals(session_instance.extra_data, session_data.get('extra_data_json'))


@patreon.test.manage()
def test_set_and_get_session():
    session_data = create_session()
    session_interface = patreon.model.request.session.PatreonSessionInterface()
    session_instance = session_interface.get_session(session_data.get('session_id'), True)

    session_instance.session_id = 'new_id'
    session_instance.user_id = 3
    session_instance.csrf_token = 'new_csrf_token'
    session_instance.csrf_token_expiration = '2015-02-10 11:46:45'
    session_instance.is_admin = 0
    session_instance.extra_data = '"valid_json"'

    session_interface.save_session(None, session_instance, None)
    new_session = session_interface.get_session(session_instance.session_id)

    assert_equals(new_session.user_id, 3)
    assert_equals(new_session.csrf_token, 'new_csrf_token')
    assert_equals(str(new_session.csrf_token_expiration), '2015-02-10 11:46:45')
    assert_equals(new_session.is_admin, False)
    assert_equals(new_session.extra_data, '"valid_json"')


@patreon.test.manage()
def test_delete_session():
    session_data = create_session()
    session_interface = patreon.model.request.session.PatreonSessionInterface()
    session_instance = session_interface.get_session(session_data.get('session_id'), True)
    session_interface.delete_session(session_instance)
    new_session = session_interface.get_session(session_data.get('session_id'), True)

    assert_not_equals(session_instance.user_id, 0)
    assert_equals(new_session.user_id, 0)


@patreon.test.manage()
def test_extra_data_members():
    patron_session = create_web_session()

    with patron_session:
        first_original_user_id = session.original_user_id
        session.original_user_id = 5
        assert_equals(session.original_user_id, 5)
        assert_not_equals(session.original_user_id, first_original_user_id)


@patreon.test.manage()
def test_blank_session():
    session_id = 'patreonsessionid'
    session_interface = patreon.model.request.session.PatreonSessionInterface()
    session_instance = session_interface.get_session(session_id, True)
    assert_equals(session_instance.session_id, session_id)
