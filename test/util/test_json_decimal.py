from nose.tools import *

from flask.json import dumps
from decimal import Decimal


def test_json():
    decimal_json = dumps({ "decimal": Decimal(1) })
    assert_equals('{"decimal": 1}', decimal_json)
