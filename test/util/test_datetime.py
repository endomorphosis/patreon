from nose.tools import *
from patreon.util.datetime import datetime_at_midnight_on_date
import datetime
import pytz

def test_midnight_on_date():
    dt = datetime_at_midnight_on_date(datetime.date(2015,1,1), pytz.timezone("US/Pacific"))
    assert_equal("2015-01-01 08:00:00+00:00", str(dt))

    dt = datetime_at_midnight_on_date(datetime.date(2015,6,1), pytz.timezone("US/Pacific"))
    assert_equal("2015-06-01 07:00:00+00:00", str(dt))

def test_midnight_in_dst_transition():
    dt = datetime_at_midnight_on_date(datetime.date(2014,10,19), pytz.timezone("America/Sao_Paulo"))
    assert_equal("2014-10-19 03:00:00+00:00", str(dt))
