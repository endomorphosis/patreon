import patreon

from nose.tools import *
from patreon.util.unsorted import url_relative_to_full, dollar_string_to_cents, append_get_var, \
    build_url_from_parameters


@patreon.test.manage()
def test_relative_url_to_full():
    host = patreon.config.main_server
    cases = [
        ('//example.com', 'https://example.com'),
        ('//example.com/pathname', 'https://example.com/pathname'),
        ('//example.com?hey=listen', 'https://example.com?hey=listen'),
        ('/pathname', 'https://' + host + '/pathname'),
        ('/pathname?hey=listen', 'https://' + host + '/pathname?hey=listen'),
        ('http://example.com/pathname', 'http://example.com/pathname'),
        ('https://example.com/pathname', 'https://example.com/pathname'),
        ('https://example.com/pathname?hey=listen', 'https://example.com/pathname?hey=listen'),
    ]
    for start, expected in cases:
        assert_equal(url_relative_to_full(start), expected)


@patreon.test.manage()
def test_dollar_string_to_cents():
    assert_equals(300, dollar_string_to_cents('$3'))
    assert_equals(500, dollar_string_to_cents('5'))
    assert_equals(250000, dollar_string_to_cents('2,500'))
    assert_equals(None, dollar_string_to_cents('NaN'))
    assert_equals(None, dollar_string_to_cents('bad'))


# Just for you jesse
@patreon.test.manage()
def test_append_get_var():
    empty = ''
    no_get_param = '/'
    has_get_param = '/?rat=hat'

    new_get_param = 'cat=hat'

    assert_equals(append_get_var(empty, new_get_param), '?cat=hat')
    assert_equals(append_get_var(no_get_param, new_get_param), '/?cat=hat')
    assert_equals(append_get_var(has_get_param, new_get_param), '/?rat=hat&cat=hat')


@patreon.test.manage()
def test_build_url_from_parameters():
    params = {
        'cat': 'hat',
        'dog': {
            'mat': {
                'gnat': 'bat'
            },
            'dat': 'mat'
        }
    }
    url = build_url_from_parameters('/', params)
    assert_in('/?', url)
    assert_in('dog[mat][gnat]=bat', url)
    assert_in('cat=hat', url)
    assert_in('dog[dat]=mat', url)
