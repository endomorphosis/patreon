from nose.tools import *
from patreon.util import collections


def test_zip_lists_to_dicts_fail():
    params = ['a', 'b', 'c', 'd']
    lists = [
        [1, 2, 3, 4],
        [5, 6, 7, 8],
        [9, 10, 11, 12],
        [13, 14, 15]
    ]

    dicts = collections.zip_lists_to_dicts(params, lists)

    assert_is_none(dicts)


def test_zip_lists_to_dicts():
    params_1_2 = ['a']
    lists_1_2 = [[1, 2]]
    expected_dicts_1_2 = [
        {'a': 1},
        {'a': 2}
    ]

    params_4_4 = ['a', 'b', 'c', 'd']
    lists_4_4 = [
        [1, 2, 3, 4],
        [5, 6, 7, 8],
        [9, 10, 11, 12],
        [13, 14, 15, 16]
    ]
    expected_dicts_4_4 = [
        {'a': 1, 'b': 5, 'c': 9, 'd': 13},
        {'a': 2, 'b': 6, 'c': 10, 'd': 14},
        {'a': 3, 'b': 7, 'c': 11, 'd': 15},
        {'a': 4, 'b': 8, 'c': 12, 'd': 16}
    ]

    dicts = collections.zip_lists_to_dicts(params_1_2, lists_1_2)
    assert_equals(dicts, expected_dicts_1_2)

    dicts = collections.zip_lists_to_dicts(params_4_4, lists_4_4)
    assert_equals(dicts, expected_dicts_4_4)
