from nose.tools import *

from patreon.util.unsorted import insert_links_in_text


def test_insert_links_in_trivial_text():
    text1 = ""
    text2 = "abcdef"

    # Assert no change.
    assert_equal(text1, insert_links_in_text(text1))
    assert_equal(text2, insert_links_in_text(text2))


def test_insert_links_in_html():
    html1 = """<a href="http://something.com">Words</a>"""

    # Assert no change.
    assert_equal(html1, insert_links_in_text(html1))


def test_insert_links_in_text():
    link1 = "http://something.com"
    link2 = "https://username:password@something.com/HEY?listen&collaborate=listen#iceisback"

    text_with_links = "blah blah blah {0} blah blah".format(link1)
    text_with_sadistic_link = "blah {0} blah".format(link2)

    link_html_template = '''<a target="_blank" href="{0}">{0}</a>'''
    assert_in(link_html_template.format(link1), insert_links_in_text(text_with_links))
    assert_in(link_html_template.format(link2), insert_links_in_text(text_with_sadistic_link))
