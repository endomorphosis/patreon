import patreon

from nose.tools import *
from patreon.test import asserttest
from patreon.services import ratelimit

import time
import datetime
import uuid

@patreon.test.manage()
def test_ratelimit():
    if patreon.config.memcached:
        user_id = str(uuid.uuid4())
        for _ in range(10):
            assert_false(ratelimit.throttle(user_id, 'action', 10, 3600))
            assert_false(ratelimit.overlimit(user_id, 'action', 10, 3600))
        assert_true(ratelimit.throttle(user_id, 'action', 10, 3600))
        assert_true(ratelimit.overlimit(user_id, 'action', 10, 3600))