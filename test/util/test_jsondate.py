import patreon

from nose.tools import *
from patreon.test import asserttest
from patreon.util import jsondate

import time
import datetime

@patreon.test.manage()
def test_jsondate():
    future_seconds = (time.time() * 100)

    assert_equal(jsondate.diff(seconds=1), 1000)
    assert_greater(jsondate.now(), future_seconds)
    assert_greater(jsondate.diff(seconds=100) + jsondate.now(), jsondate.now())

    assert_greater(jsondate.today(), future_seconds)
    assert_greater(jsondate.today(hour=12, minute=32, second=17), future_seconds)

    assert_greater(jsondate.from_unix(time.time()), future_seconds)
    assert_greater(jsondate.from_datetime(datetime.datetime.now()), future_seconds)

    n = datetime.datetime.now()
    assert_greater(jsondate.coerce(n), future_seconds)
    assert_greater(jsondate.coerce(time.time()), future_seconds)

    n = jsondate.now()
    assert_equal(jsondate.coerce(n), n)