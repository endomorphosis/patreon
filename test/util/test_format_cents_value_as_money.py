from nose.tools import *
from patreon.util import format


def test_format_cents_as_money():
    assert_equals("1", format.format_cents_value_as_money(100))
    assert_equals("1.01", format.format_cents_value_as_money(101))
    assert_equals("1.50", format.format_cents_value_as_money(150))
    assert_equals("1.99", format.format_cents_value_as_money(199))
    assert_equals("2", format.format_cents_value_as_money(200))
    assert_equals("500", format.format_cents_value_as_money(50000))
    assert_equals("1,000", format.format_cents_value_as_money(100000))
    assert_equals("10,000", format.format_cents_value_as_money(1000000))
    assert_equals("100,000", format.format_cents_value_as_money(10000000))
    assert_equals("1,000,000", format.format_cents_value_as_money(100000000))
    assert_equals("1,000.99", format.format_cents_value_as_money(100099))
    assert_equals("10,000.99", format.format_cents_value_as_money(1000099))
    assert_equals("100,000.99", format.format_cents_value_as_money(10000099))
    assert_equals("1,000,000.99", format.format_cents_value_as_money(100000099))
    assert_equals("20,000,000,000,000,000", format.format_cents_value_as_money(2000000000000000000))
    assert_equals("20,000,000,000,000,000.01", format.format_cents_value_as_money(2000000000000000001))
