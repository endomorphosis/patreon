var frisby = require('frisby');
var config = require('./lib/config');
var auth = require('./lib/auth');

var createCommentTest = function () {
    frisby.create('Get new draft')
        .get(config.BASE_URL_INSECURE + 'categories?api_key=' + config.API_KEY)
        .inspectJSON()
        .expectStatus(200)
        .expectHeader('Content-Type', 'application/vnd.api+json')
        .expectJSON('data', {
            categories: {
                "0": "Front Page",
                "1": "Video & Film",
                "2": "Music",
                "3": "Writing",
                "4": "Comics",
                "5": "Drawing & Painting",
                "6": "Animation",
                "7": "Podcasts",
                "8": "Games",
                "9": "Photography",
                "10": "Comedy",
                "11": "Science",
                "12": "Education",
                "13": "Crafts & DIY",
                "14": "Dance & Theater",
                "99": "All"
            }
        })
        .toss();
};

createCommentTest();
