var frisby = require('frisby');
var config = require('./lib/config');
var auth = require('./lib/auth');

var testerCampaignID = "162460";

var getCurrentCampaignTest = function () {
    frisby.create('Get current campaign')
        .get(config.BASE_URL_INSECURE + 'campaign/current_campaign?api_key=' + config.API_KEY)
        .expectStatus(200)
        .expectHeader('Content-Type', 'application/vnd.api+json')
        .expectJSON('data', {
            type: 'campaign'
        })
        .expectJSONTypes('data', {
            id: String,
            summary: (function(val) { expect(val).toBeTypeOrNull(String); }),
            creation_name: (function(val) { expect(val).toBeTypeOrNull(String); }),
            pay_per_name: (function(val) { expect(val).toBeTypeOrNull(String); }),
            one_liner: (function(val) { expect(val).toBeTypeOrNull(String); }),
            main_video_embed: (function(val) { expect(val).toBeTypeOrNull(String); }),
            main_video_url: (function(val) { expect(val).toBeTypeOrNull(String); }),
            image_small_url: (function(val) { expect(val).toBeTypeOrNull(String); }),
            image_url: (function(val) { expect(val).toBeTypeOrNull(String); }),
            thanks_video_url: (function(val) { expect(val).toBeTypeOrNull(String); }),
            thanks_embed: (function(val) { expect(val).toBeTypeOrNull(String); }),
            thanks_msg: (function(val) { expect(val).toBeTypeOrNull(String); }),
            is_monthly: Boolean,
            is_nsfw: Boolean,
            created_at: String,
            published_at: (function(val) { expect(val).toBeTypeOrNull(String); })
        })
        .toss();
};

var getCurrentCampaignShouldFailWhenNotCreatorTest = function () {
    frisby.create('Get current campaign when not creator')
        .get(config.BASE_URL_INSECURE + 'campaign/current_campaign?api_key=' + config.API_KEY)
        .expectStatus(200)
        .expectHeader('Content-Type', 'application/vnd.api+json')
        .expectJSON({
            errors: [{
                error_code: 400,
                message: 'current user does not have a campaign.'
            }]
        })
        .toss();
};

var createCampaignTest = null;

var deleteCampaignTest = null;

module.exports.getCurrentCampaignShouldFailWhenNotCreatorTest =
    getCurrentCampaignShouldFailWhenNotCreatorTest;
module.exports.getCurrentCampaignTest = getCurrentCampaignTest;
module.exports.createCampaignTest = createCampaignTest;
module.exports.deleteCampaignTest = deleteCampaignTest;

//auth.authAndRunTest(getCurrentCampaignShouldFailWhenNotCreatorTest);
auth.authAndRunTest(getCurrentCampaignTest);
