var frisby = require('frisby');
var config = require('./lib/config');
var auth = require('./lib/auth');
var campaign = require('./campaign_api_spec');

var testerCampaignID = "162460";

var emptyDraftTest = function () {
    frisby.create('Get new draft')
        .get(config.BASE_URL_INSECURE + 'post?api_key=' + config.API_KEY)
        .expectStatus(200)
        .expectHeader('Content-Type', 'application/vnd.api+json')
        .expectJSON('data', {
            category: null,
            cents_pledged_at_creation: 0,
            comment_count: 0,
            content: null,
            deleted_at: null,
            embed: {
                description: null,
                domain: null,
                html: null,
                subject: null,
                url: null
            },
            image_height: 0,
            image_large_url: null,
            image_url: null,
            image_width: 0,
            is_creation: true,
            is_paid: false,
            like_count: 0,
            min_cents_pledged_to_view: 0,
            post_type: "text_only",
            published_at: null,
            thumbnail: null,
            title: null,
            type: "activity"
        })
        .expectJSONTypes('data', {
            campaign_id: String,
            created_at: String,
            edited_at: String,
            id: String,
            user_id: String
        })
        .toss();
};

var thereAreNoDraftsTest = function () {
    frisby.create('There are no drafts')
        .get(
            config.BASE_URL_INSECURE + 'campaign/' + testerCampaignID
            + '/drafts?api_key=' + config.API_KEY
        )
        .expectStatus(200)
        .expectHeader('Content-Type', 'application/vnd.api+json')
        .expectJSON('data', [])
        .toss();
};

auth.authAndRunTest(emptyDraftTest);
auth.authAndRunTest(thereAreNoDraftsTest);

module.exports.emptyDraftTest = emptyDraftTest;
module.exports.thereAreNoDraftsTest = thereAreNoDraftsTest;
