var frisby = require('frisby');
var auth = require('./lib/auth');
var config = require('./lib/config');

var creationID = "1488604";
var commentText = "This is a test comment.";
var newCommentText = "This is an edited comment.";

var createCommentTest = function () {
    frisby.create('Create Comment')
        .post(config.BASE_URL_INSECURE + 'processComment',
        {
            "hid": creationID,
            "inputComment": commentText,
            "returnData": false,
            "notABot": true,
            "threadid": 0,
            "CSRFToken": auth.csrfTicket
        })
        .expectStatus(200)
        .expectHeader('Content-Type', 'application/json')
        .expectJSON(
        {
            "HID": creationID,
            "Comment": commentText,
            "ThreadID": null,
            "DeletedAt": null,
            "EditedAt": null
        })
        .expectJSONTypes({
            "CID": String,
            "UID": String,
            "Created": String
        })
        .afterJSON(function (json) {
            editCommentTest(json.CID);
        })
        .toss();
};

var editCommentTest = function (commentID) {
    frisby.create('Edit Comment')
        .post(config.BASE_URL_INSECURE + 'processCommentEdit', {
            "cid": commentID,
            "new_message": newCommentText,
            "CSRFToken": auth.csrfTicket
        })
        .expectStatus(200)
        .expectHeader('Content-Type', 'application/json')
        .expectJSON(
        {
            "CID": commentID,
            "HID": creationID,
            "Comment": newCommentText,
            "ThreadID": null,
            "DeletedAt": null
        })
        .expectJSONTypes({
            "UID": String,
            "Created": String,
            "EditedAt": String
        })
        .afterJSON(function () {
            deleteCommentTest(commentID)
        })
        .toss();
};

var deleteCommentTest = function (commentID) {
    frisby.create('Delete Comment')
        .post(config.BASE_URL_INSECURE + 'processCommentDelete', {
            "cid": commentID,
            "CSRFToken": auth.csrfTicket
        })
        .expectStatus(200)
        .expectHeader('Content-Type', 'application/json')
        .expectJSON({
            "HID": creationID
        })
        .toss();
};

auth.authAndRunTest(createCommentTest);
