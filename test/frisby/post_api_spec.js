var frisby = require('frisby');
var config = require('./lib/config');
var auth = require('./lib/auth');
var campaign = require('./campaign_api_spec');


var saveImageTest = function () {
    frisby.create('upload an image')
        .post(config.BASE_URL_INSECURE + 'post/' + post_id, {
            data: {
                title: "You are amazing",
                content: "<p>Share your thoughts!</p>",
                embed: {
                    html: null
                },
                tags: {
                    publish: false
                },
                metadata: {
                    file_name: "ravagesofwarpromo-1.jpg"
                },
                thumbnail: {
                    top_offset: 0
                },
                post_type: "image",
                is_paid: false,
                category: -1,
                min_cents_pledged_to_view: null,
                upload:  {
                    source:  "data:image/jpeg;base64,/9j/4QAYRXhpZgAASUkqAAgAAAAAAAAAAAAAAP/sABFEdWNreQAB…AwADAAMAAwADAAMAAwADAAMAAwADAAMAAwADAAMAAwADAAMAAwADAAMAAwADAAMAAwADAB/9k="
                }
            }
        }, { json: true })
        .expectStatus(200)
        .expectHeader('Content-Type', 'application/vnd.api+json')
        .expectJSON('data', {
            category: null,
            cents_pledged_at_creation: 0,
            comment_count: 0,
            content: null,
            deleted_at: null,
            embed: {
                description: null,
                domain: null,
                html: null,
                subject: null,
                url: null
            },
            image_height: 0,
            image_large_url: null,
            image_url: null,
            image_width: 0,
            is_creation: true,
            is_paid: false,
            like_count: 0,
            min_cents_pledged_to_view: 0,
            post_type: "text_only",
            published_at: null,
            thumbnail: null,
            title: null,
            type: "activity"
        })
        .expectJSONTypes('data', {
            campaign_id: String,
            created_at: String,
            edited_at: String,
            id: String,
            user_id: String
        })
        .toss();
};

auth.authAndRunTest(saveImageTest);

