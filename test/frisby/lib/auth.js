var frisby = require('frisby');
var config = require('./config');

var testerEmail = "testLogin@patreon.com";
var testerPassword = "PatTestPi1johpe";

var authAndRunTest = function (callback) {
    frisby.create('Login')
        .post(config.BASE_URL + 'login', {
            data: {
                "email": testerEmail,
                "password": testerPassword
            }
        }, { json: true })
        .expectStatus(200)
        .after(function (body, res) {
            var setCookie = res.headers['set-cookie'];
            var cookie = '';
            if (Array.isArray(setCookie)) {
                for (var i = 0, len = setCookie.length; i < len; i++) {
                    cookie += setCookie[i].split(';')[0];
                    if (i < len - 1) {
                        cookie += '; ';
                    }
                }
            } else {
                //hack to handle no cookie being passed back
                expect(false).toMatch(true);
            }
            frisby.globalSetup({
                request: {
                    headers: {
                        'Cookie': cookie
                    }
                }
            });
            console.log(cookie);
            callback();
        })
        .toss();
};

module.exports.authAndRunTest = authAndRunTest;
