var frisby = require('frisby');
frisby.create('Ensure we are dealing with a teapot')
    .get('http://httpbin.org/status/418')
    .expectStatus(418)
    .toss();
frisby.create('Ensure response has a proper JSON Content-Type header')
    .get('http://httpbin.org/get')
    .expectHeader('Content-Type', 'application/json')
    .toss();