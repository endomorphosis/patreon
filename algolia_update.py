import time
import patreon
from patreon.helpers.algolia import Algolia


def get_post_updates_from_queue(number_elements=10):
    messages = patreon.services.aws.sqs.get('post_search_update_queue', number_elements)
    if messages is None:
        return []
    return [message.get('post_id') for message in messages]


def get_campaign_updates_from_queue(number_elements=10):
    messages = patreon.services.aws.sqs.get('campaign_search_update_queue', number_elements)
    if messages is None:
        return []
    return [message.get('campaign_id') for message in messages]


client = Algolia("27QC0IB0ER", 'eeb13fc8f23b4d3c3a7571d7f3f77475')

while True:
    post_ids = get_post_updates_from_queue()
    if len(post_ids) > 0:
        client.update_posts_by_ids(post_ids)
        print('Updating posts: ' + str(post_ids).strip('[]'))

    campaign_ids = get_campaign_updates_from_queue()
    if len(campaign_ids) > 0:
        client.update_campaigns_by_ids(campaign_ids)
        print('Updating campaigns: ' + str(campaign_ids).strip('[]'))

    if post_ids == [] and campaign_ids == []:
        print('Both queues empty! Sleeping!')
        time.sleep(5)
