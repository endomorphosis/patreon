from patreon import config
from patreon import pstore
from patreon.services import aws

import requests
import sys

SLACK_CHANNEL = 'engineering'
DEPLOY_KEY = 'webclient_deployed_sha'

if len(sys.argv) != 2 or len(sys.argv[1]) != 40:
    print('Usage: python3 deploy_webclient.py <SHA_TO_DEPLOY>')
    sys.exit(0)

sha = sys.argv[1]

# Sanity check that the assets have been built
s3_bucket = aws.S3Bucket('patreon-webclient-assets')
appjs = sha + '/js/app.js'
appcss = sha + '/css/app.css'
if not s3_bucket.exists(appjs) or not s3_bucket.exists(appcss):
    print('This SHA is not yet available in S3. Please ensure it is green on webclient master.')
    sys.exit(1)

# Deploy!
previous_sha = pstore.get(DEPLOY_KEY)
pstore.set(DEPLOY_KEY, sha)

# Check if it worked
if pstore.get(DEPLOY_KEY) != sha:
    print('Error setting SHA in pstore! Deploy aborted.')
    sys.exit(1)

if config.stage == 'production':
    # Notify Slack
    message = '''
Previous webclient revision was {0}
Deployed webclient to revision {1}
URL: https://github.com/Patreon/patreon_webclient/commit/{1}
    '''.format(previous_sha, sha)
    api_key = config.slack_api_key
    url = ("https://patreon.slack.com/services/hooks/slackbot"
           "?token={0}&channel=%23{1}").format(api_key, SLACK_CHANNEL)
    requests.post(url, data=message)

    # Run the smoke tests
    api_key = config.circle_smoketests_api_key
    url = ("https://circleci.com/api/v1/project/Patreon/"
           "patreon_smoketests/tree/master?circle-token={}").format(api_key)
    requests.post(url)

print('Done!')
