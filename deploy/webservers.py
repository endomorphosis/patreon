python_http_routes = [
    # Place http urls that you want to go through Python here. Ex:
    # /about
]

python_https_routes = [
    # Place https urls that you want to go through Python here. Ex:
    # /processPatron
]

# Set this to the root director of PHP for Apache.
php_root = None

# Set this to the route to static resources for Nginx.
nginx_static_root = None