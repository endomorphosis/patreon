#!/usr/bin/env python3

try:
    import webservers
except ImportError:
    webservers = None

import sys
import re
import subprocess
import jinja2
import os

modpath = os.path.dirname(__file__)

def get_php_root():
    if webservers and webservers.php_root:
        return webservers.php_root
    else:
        python_repo_path = os.path.join(modpath, '..')
        php_repo_path = os.path.join(python_repo_path, '../patreon_php')
        default = os.path.realpath(php_repo_path)

        res = input("Please input the location of your PHP codebase: [default: {}] ".format(default))
        res = res.strip()
        return res or default


def get_nginx_prefix():
    output = subprocess.check_output(['nginx', '-V'], stderr=subprocess.STDOUT)
    matches = re.search(r"prefix=([\/\._\-0-9a-zA-Z]+)\W", output.decode('ascii'))
    if matches:
        return matches.groups(0)[0]
    else:
        raise ValueError("Unable to determine the nginx install directory. Is nginx installed?")


def open_nginx_config_file():
    prefix = get_nginx_prefix()
    filename = os.path.join(prefix, 'nginx.patreon.conf')
    if os.path.exists(filename):
        answer = input("nginx.patreon.conf already exists at {}. Overwrite? [y/N] ".format(filename))
        if answer.lower() in ('y', 'yes'):
            return filename
        else:
            raise ValueError("Not overwriting existing configuration.")
    else:
        return filename


def get_template():
    template_file = os.path.join(modpath, 'nginx.conf.template')
    with open(template_file) as fp:
        return jinja2.Template(fp.read())


def prepare_directory():
    prefix = get_nginx_prefix()
    logs_dir = os.path.join(prefix, 'logs/')
    cert_file = os.path.join(modpath, 'localhost.cert')
    key_file = os.path.join(modpath, 'localhost.key')
    subprocess.check_output('mkdir -p {}'.format(logs_dir), shell=True)
    subprocess.check_output('cp {0} {1} {2}/'.format(cert_file, key_file, prefix), shell=True)


def main():
    php_root      = get_php_root()
    config_file   = open_nginx_config_file()
    template      = get_template()
    conf_contents = template.render(php_root=php_root)

    prepare_directory()

    with open(config_file, 'w') as fp:
        fp.write(conf_contents)
        print("To start nginx, run: sudo nginx -c nginx.patreon.conf")


if __name__ == '__main__':
    if sys.version_info < (3, 0, 0):
        print("Please run this script under Python 3.")
    else:
        main()
