import argparse

import patreon

if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('--port', default=5000, type=int)
    parser.add_argument('--main_server', default=None)
    parser.add_argument('--api_server', default=None)
    args = parser.parse_args()

    if args.main_server:
        patreon.config.main_server = args.main_server
        patreon.globalvars.define('MAIN_SERVER', patreon.config.main_server)
    if args.api_server:
        patreon.config.api_server = args.api_server
        patreon.globalvars.define('API_SERVER', patreon.config.api_server)

    from patreon.app.web import web_app

    web_app.debug = patreon.config.debug
    web_app.run('0.0.0.0', port=args.port, use_reloader=False)

else:
    from patreon.app.web import web_app