import json as jsonlib
import jinja2


# TODO: having to do this globally is gross, but doing it non-globally on the jinja2.Environment instance didn't work...
def pretty_json(json):
    return jsonlib.dumps(json, sort_keys=True, indent=4, separators=(',', ': '))


jinja2.filters.FILTERS['pretty_json'] = pretty_json


class DocPage(object):
    def __init__(self, endpoints_json=None, resources_json=None):
        self._endpoints_json = endpoints_json
        self._resources_json = resources_json

    def render(self):
        env = jinja2.Environment(
            loader=jinja2.FileSystemLoader('documentation/src')
        )
        template_name = self.template()
        template = env.get_template(template_name)
        html = template.render(self.data())
        return jinja2.Markup(html)

    def set_endpoints_json(self, json):
        self._endpoints_json = json

    def get_endpoints_json(self, filename='documentation/output/endpoints.json'):
        if not self._endpoints_json:
            self._endpoints_json = self.json_from_file(filename)
        return self._endpoints_json

    def set_resources_json(self, json):
        self._resources_json = json

    def get_resources_json(self, filename='documentation/output/resources.json'):
        if not self._resources_json:
            self._resources_json = self.json_from_file(filename)
        return self._resources_json

    @staticmethod
    def json_from_file(filename):
        import tokenize

        with tokenize.open(filename) as f:
            json_str = f.read()
        return jsonlib.loads(json_str)

    def data(self):
        json = self.get_endpoints_json()
        json.update(self.get_resources_json())
        return json

    def template(self):
        return "doc_template.html"


def generate_html_doc(endpoints_json=None, resources_json=None):
    return DocPage(
        endpoints_json=endpoints_json,
        resources_json=resources_json
    ).render()


def generate_and_write_html_doc(endpoints_json=None, resources_json=None):
    rendered = generate_html_doc(endpoints_json, resources_json)
    with open('documentation/output/documentation.html', 'w+') as outfile:
        outfile.write(rendered)
