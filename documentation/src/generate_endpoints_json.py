from ast import *
import fnmatch
import json as jsonlib
import tokenize
import os

from documentation.src.util import find_subnodes_of_type
from patreon.constants.http_codes import NOT_AUTHORIZED_403


def make_endpoints_json(filename):
    with tokenize.open(filename) as f:
        fstr = f.read()
    top_node = parse(fstr, mode='exec')
    endpoints = extract_endpoints(top_node)

    json_arr = []
    for endpoint in endpoints:
        path = extract_path_tuple(endpoint)
        if not path:
            continue

        route_path, methods_str = path

        json_arr.append({
            'route': {
                'path': route_path,
                'methods': methods_str
            },
            'details': {
                'other_decorators': extract_other_decorators(endpoint),
                'inputs': extract_inputs(endpoint),
                'return_resource': extract_return_resource(endpoint),
                'errors': extract_errors(endpoint)
            }
        })

    return json_arr


def extract_endpoints(top_node):
    # one top-level 'body' node
    body_node = [node for node in iter_fields(top_node)][0]
    # routes are top-level functions
    return [
        node for node in body_node[1]
        if isinstance(node, FunctionDef)
    ]


# PATH

def extract_path_tuple(endpoint):
    route_decorators = [
        decorator for decorator in endpoint.decorator_list
        if is_route_decorator(decorator)
    ]
    if not route_decorators:
        return None

    route_decorator = route_decorators[0]

    route_path = '/'
    route_args_nodes = [
        arg for arg in route_decorator.args
        if isinstance(arg, Str) and arg.s.startswith('/')
    ]
    if route_args_nodes:
        route_args_node = route_args_nodes[0]
        route_path = route_args_node.s

    route_methods = ['GET']
    route_methods_nodes = [
        kw for kw in route_decorator.keywords
        if kw.arg == 'methods'
    ]
    if route_methods_nodes:
        route_methods_node = route_methods_nodes[0]
        route_methods = [node.s for node in route_methods_node.value.elts]

    methods_str = ', '.join(route_methods)
    return route_path, methods_str


def is_route_decorator(decorator_node):
    return (
        isinstance(decorator_node, Call)
        and isinstance(decorator_node.func, Attribute)
        and decorator_node.func.attr == 'route'
    )


# DECORATORS

def extract_other_decorators(endpoint):
    return [
        decorator.id for decorator in endpoint.decorator_list
        if not is_route_decorator(decorator)
        and isinstance(decorator, Call)
        and isinstance(decorator.func, Attribute)
    ]
    # TODO: more, non-Name decorators?


# INPUTS

def extract_inputs(endpoint):
    # TODO: look at all dict lookups of the result of get_data
    # TODO: look at get_data getting passed to an APIResource object for parsing
    pass


# OUTPUTS

def extract_return_resource(endpoint):
    return_nodes = find_subnodes_of_type(endpoint, Return)
    resources = []
    for node in return_nodes:
        added = False
        if isinstance(node.value, Call):
            if isinstance(node.value.func, Attribute):
                if isinstance(node.value.func.value, Call):
                    if isinstance(node.value.func.value.func, Name):
                        added = True
                        resources += [node.value.func.value.func.id]
        if not added:
            resources += ['UNKNOWN']
            # TODO: other return node types
    return list(set(resources))


## POSSIBLE ERRORS

def extract_errors(endpoint):
    abort_nodes = find_aborts(endpoint)
    aborts = []
    for node in abort_nodes:
        status_code = NOT_AUTHORIZED_403
        error_type = 'generic-error'
        error_message = 'A runtime check failed.'
        if node.args and not isinstance(node.args[0], Name):
            status_code = node.args[0].n
            if len(node.args) > 1:
                error_type = node.args[1].s
            if len(node.args) > 2:
                error_message = node.args[2]
        aborts += [(status_code, error_type, error_message)]
    return aborts


def find_aborts(node):
    all_calls = find_subnodes_of_type(node, Call)
    return [
        call_node
        for call_node in all_calls
        if isinstance(call_node.func, Name)
        and call_node.func.id == 'abort'
    ]


def generate_endpoints_json(input_path='patreon/app/api/routes', endpoint_filenames=None):
    if not endpoint_filenames:
        endpoint_filenames = []
        blacklist = ['__init__', 'api_docs', 'xdomain']
        for root, dirnames, filenames in os.walk(input_path):
            for filename in fnmatch.filter(filenames, '*.py'):
                skip = False
                for skip_name in blacklist:
                    if filename.startswith(skip_name):
                        skip = True
                if not skip:
                    endpoint_filenames.append(os.path.join(root, filename))

    json_arr = []
    for filename in endpoint_filenames:
        json_arr += make_endpoints_json(filename)
    json_arr.sort(key=lambda endpoint_dict: endpoint_dict['route']['path'])

    return {'endpoints': json_arr}


def generate_and_write_endpoints_json(input_path='patreon/app/api/routes',
                                      endpoint_filenames=None,
                                      output_path='documentation/output/endpoints.json'):
    json = generate_endpoints_json(input_path, endpoint_filenames)
    with open(output_path, 'w+') as outfile:
        json_str = jsonlib.dumps(json, sort_keys=True, indent=4, separators=(',', ': '))
        outfile.write(json_str)
