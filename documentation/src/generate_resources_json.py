from ast import *
import json as jsonlib

from documentation.src.util import *


def make_resource_json(filename):
    import tokenize

    with tokenize.open(filename) as f:
        fstr = f.read()
    top_node = parse(fstr, mode='exec')

    resources = extract_resources(top_node)

    json_arr = []
    for resource in resources:
        json = {}
        json['python_class_name'] = resource.name
        json['type'] = extract_type(resource)
        json['blueprint'] = extract_blueprint(resource)
        if isinstance(json['blueprint'], dict):
            json['blueprint']['type'] = json['type']

        json['includes'] = {'allowed': {}}
        allowed_includes_as_info = sorted(extract_allowed_includes(resource),
                                          key=lambda include: include.type)
        for include_info in allowed_includes_as_info:
            json['includes']['allowed'].update(include_info.to_dict())
        json['includes']['default'] = extract_default_includes(resource)

        json_arr.append(json)
    return json_arr


def extract_resources(top_node):
    # one top-level 'body' node
    body_node = [node for node in iter_fields(top_node)][0]
    # resources are top-level Classes
    resources = [node for node in body_node[1] if isinstance(node, ClassDef)]
    return [resource for resource in resources
            if resource.bases \
            and resource.bases[0].id in ['BlueprintAPIResource', 'ApiResource']]


def find_function_by_name(resource, name):
    functions = [node for node in resource.body if isinstance(node, FunctionDef)]
    name_matches = list(filter(lambda node: node.name == name, functions))
    if name_matches:
        return name_matches[0]


def extract_type(resource):
    type_function = find_function_by_name(resource, 'resource_type')
    return type_function.body[0].value.s if type_function else 'UNKNOWN'


def extract_raw_blueprint(resource):
    type_function = find_function_by_name(resource, 'blueprint')
    if type_function:
        ast_dict = type_function.body[0].value
        return blueprint_from_ast_dict(ast_dict)
    return {}


def extract_blueprint(resource):
    raw_blueprint = extract_raw_blueprint(resource)
    if raw_blueprint:
        if 'links' in raw_blueprint:
            del raw_blueprint['links']
        return raw_blueprint
    return 'UNKNOWN'


def blueprint_from_ast_dict(ast_dict):
    blueprint = {}
    for i, key in enumerate(ast_dict.keys):
        val = ast_dict.values[i]
        if isinstance(val, Str):
            val_str = val.s
            if val_str.startswith('date:'):
                value = '[Date (ISO-8601)]'
            elif val_str.startswith('bool:'):
                value = '[Boolean]'
            elif key.s == 'id':
                value = '[Unique ID String]'
            elif key.s != 'type':
                value = '[String]'
            else:
                value = val_str
            blueprint[key.s] = value
        elif isinstance(val, Dict):
            blueprint[key.s] = blueprint_from_ast_dict(val)
    return blueprint


def extract_default_includes(resource):
    default_includes_function = find_function_by_name(resource, 'default_includes')
    if default_includes_function:
        return [item.s for item in default_includes_function.body[0].value.elts]
    return []


def extract_allowed_includes(resource):
    all_includes = set(extract_blueprint_includes(resource))
    all_includes |= set(extract_manual_includes(resource))
    return all_includes


def extract_blueprint_includes(resource):
    raw_blueprint = extract_raw_blueprint(resource)
    if 'links' in raw_blueprint:
        return [IncludeInfo(link, raw_blueprint['links'][link]['type'])
                for link in raw_blueprint['links']]
    return []


def extract_manual_includes(resource):
    linked_resources_function = find_function_by_name(resource, 'linked_resources')
    if linked_resources_function:
        subscripts = find_subnodes_of_type(linked_resources_function, Subscript)
        return [IncludeInfo(subscript.slice.value.s) for subscript in subscripts]
    return []


class IncludeInfo(object):
    def __init__(self, name, type_='UNKNOWN'):
        self.name = name
        self.type = type_

    def __hash__(self):
        return self.name.__hash__()

    def __eq__(self, other):
        return type(other) == type(self) and self.name == other.name

    def to_dict(self):
        return {self.name: self.type}


def generate_resources_json(input_path='patreon/app/api/resources', resource_filenames=None):
    if not resource_filenames:
        import os, fnmatch

        resource_filenames = []
        blacklist = ['__init__', 'api_resource', 'api_resource_collection',
                     'api_resource_null', 'api_resource_reference',
                     'blueprint_api_resource', 'blueprint_class_map']
        for root, dirnames, filenames in os.walk(input_path):
            for filename in fnmatch.filter(filenames, '*.py'):
                skip = False
                for skip_name in blacklist:
                    if filename.startswith(skip_name):
                        skip = True
                if not skip:
                    resource_filenames.append(os.path.join(root, filename))

    json_arr = []
    for filename in resource_filenames:
        json_arr += make_resource_json(filename)
    json_arr.sort(key=lambda resource_dict: resource_dict['type'])

    return {'resources': json_arr}


def generate_and_write_resources_json(input_path='patreon/app/api/resources', resource_filenames=None,
                                      output_path='documentation/output/resources.json'):
    json = generate_resources_json(input_path, resource_filenames)
    with open(output_path, 'w+') as outfile:
        json_str = jsonlib.dumps(json, sort_keys=True, indent=4, separators=(',', ': '))
        outfile.write(json_str)
