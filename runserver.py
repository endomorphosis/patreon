import ssl
import argparse

from os import path
from flask import send_from_directory
import patreon

def setup_apps(web_app, api_app):
    web_app.debug = patreon.config.debug
    api_app.debug = patreon.config.debug

    # Add additional routes to simulate the static resources that would
    # normally be served with nginx.
    # TODO(postport): This should go away with the PHP codebase.
    if patreon.config.php_repo:

        php_public = path.join(patreon.config.php_repo, 'public/')

        @web_app.route('/scripts/<path:filename>')
        def scripts_files(filename):
            return send_from_directory(path.join(php_public, 'scripts'), filename)

        @web_app.route('/css/<path:filename>')
        def css_files(filename):
            return send_from_directory(path.join(php_public, 'css'), filename)

        @web_app.route('/js/<path:filename>')
        def js_files(filename):
            return send_from_directory(path.join(php_public, 'js'), filename)

        @web_app.route('/images/<path:filename>')
        def image_files(filename):
            return send_from_directory(path.join(php_public, 'images'), filename)

        @web_app.route('/_generated/<path:filename>')
        def generated_files(filename):
            if ".js?" in filename:
                # Fix autodetection not handling query params for app.js?v=100
                mimetype = "application/javascript"
            else:
                # Use autodetection
                mimetype = None
            return send_from_directory(path.join(php_public, '_generated'), filename, mimetype=mimetype)

    if patreon.config.debug:
        import sqltap.wsgi

        web_app.wsgi_app = sqltap.wsgi.SQLTapMiddleware(web_app.wsgi_app)
        api_app.wsgi_app = sqltap.wsgi.SQLTapMiddleware(api_app.wsgi_app)

# Actually run the server here.
if __name__ == '__main__':

    parser = argparse.ArgumentParser()
    parser.add_argument('--port', default=5000, type=int)
    parser.add_argument('--main_server', default=None)
    parser.add_argument('--api_server', default=None)
    parser.add_argument('--with_ssl', action='store_true', default=False)
    parser.add_argument('--api', action='store_true', default=False)
    args = parser.parse_args()

    if args.with_ssl:
        context = ssl.SSLContext(ssl.PROTOCOL_TLSv1)
        context.load_cert_chain('ssl/server.crt', 'ssl/server.key')
    else:
        context = None

    if args.main_server:
        patreon.config.main_server = args.main_server
        patreon.globalvars.define('MAIN_SERVER', patreon.config.main_server)
    if args.api_server:
        patreon.config.api_server = args.api_server
        patreon.globalvars.define('API_SERVER', patreon.config.api_server)

    from patreon.app.web import web_app
    from patreon.app.api import api_app

    setup_apps(web_app, api_app)

    if args.api:
        api_app.run(host='0.0.0.0', port=5001, ssl_context=context)
    else:
        web_app.run(host='0.0.0.0', port=5000, ssl_context=context)

else:
    from patreon.app.web import web_app
    from patreon.app.api import api_app

    setup_apps(web_app, api_app)